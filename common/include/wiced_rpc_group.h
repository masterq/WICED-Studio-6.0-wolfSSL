/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/**
 * @file
 * @brief WICED RPC Bluetooth groups definitions
 */

#pragma once

#include "hci_control_api.h"
#include <stdint.h>


/**
 * WICED RPC Group codes
 */
enum wiced_rpc_group {
    WICED_RPC_GROUP_BT_GENERAL         = HCI_CONTROL_GROUP_DEVICE,
    WICED_RPC_GROUP_BT_LE              = HCI_CONTROL_GROUP_LE,
    WICED_RPC_GROUP_BT_GATT            = HCI_CONTROL_GROUP_GATT,
    WICED_RPC_GROUP_BT_HF              = HCI_CONTROL_GROUP_HF,
    WICED_RPC_GROUP_BT_SPP             = HCI_CONTROL_GROUP_SPP,
    WICED_RPC_GROUP_BT_AUDIO           = HCI_CONTROL_GROUP_AUDIO,
    WICED_RPC_GROUP_BT_HIDD            = HCI_CONTROL_GROUP_HIDD,
    WICED_RPC_GROUP_BT_AVRC_TARGET     = HCI_CONTROL_GROUP_AVRC_TARGET,
    WICED_RPC_GROUP_BT_TEST            = HCI_CONTROL_GROUP_TEST,
    WICED_RPC_GROUP_BT_AIO             = HCI_CONTROL_GROUP_AIO,
    WICED_RPC_GROUP_BT_TIME            = HCI_CONTROL_GROUP_TIME,
    WICED_RPC_GROUP_BT_ANCS            = HCI_CONTROL_GROUP_ANCS,
    WICED_RPC_GROUP_BT_ALERT           = HCI_CONTROL_GROUP_ALERT,
    WICED_RPC_GROUP_BT_IAP2            = HCI_CONTROL_GROUP_IAP2,
    WICED_RPC_GROUP_BT_AG              = HCI_CONTROL_GROUP_AG,
    WICED_RPC_GROUP_BT_LN              = HCI_CONTROL_GROUP_LN,
    WICED_RPC_GROUP_BT_BSG             = HCI_CONTROL_GROUP_BSG,
    WICED_RPC_GROUP_BT_AVRC_CONTROLLER = HCI_CONTROL_GROUP_AVRC_CONTROLLER,
    WICED_RPC_GROUP_BT_AMS             = HCI_CONTROL_GROUP_AMS,
    WICED_RPC_GROUP_BT_HIDH            = HCI_CONTROL_GROUP_HIDH,
    WICED_RPC_GROUP_BT_AUDIO_SINK      = HCI_CONTROL_GROUP_AUDIO_SINK,
    WICED_RPC_GROUP_BT_PBC             = HCI_CONTROL_GROUP_PBC,
    WICED_RPC_GROUP_BT_MESH            = HCI_CONTROL_GROUP_MESH,
    WICED_RPC_GROUP_ZB_GENERAL         = HCI_CONTROL_GROUP_ZB_FIRST + 0x00u,
    WICED_RPC_GROUP_ZB_GATEWAY         = HCI_CONTROL_GROUP_ZB_FIRST + 0x01u,
    WICED_RPC_GROUP_ZB_ZCL             = HCI_CONTROL_GROUP_ZB_FIRST + 0x0au,
    WICED_RPC_GROUP_ZB_ZCL_GENERAL     = HCI_CONTROL_GROUP_ZB_FIRST + 0x0bu,
    WICED_RPC_GROUP_ZB_QT              = HCI_CONTROL_GROUP_ZB_FIRST + 0x10u,
    WICED_RPC_GROUP_ZB_BDB             = HCI_CONTROL_GROUP_ZB_FIRST + 0x1eu,
    WICED_RPC_GROUP_ZB_VENDOR          = HCI_CONTROL_GROUP_ZB_FIRST + 0x1fu,
    WICED_RPC_GROUP_MISC               = HCI_CONTROL_GROUP_MISC,
};
typedef uint8_t wiced_rpc_group_t;
