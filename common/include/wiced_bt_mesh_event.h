/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */


/** @file
*
* Mesh Model definitions
*
*/
#ifndef __MESH_EVENT_H__
#define __MESH_EVENT_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @addtogroup  wiced_bt_mesh_core          Mesh Core Library API
 * @ingroup     wiced_bt_mesh
 *
 * Mesh Core library of the WICED SDK provide a simple method for an application to integrate 
 * Bluetooth Mesh functionality.  
 *
 * @{
 */

/**
 * Mesh event structure is exchanged between the app and the mesh models library
 */
typedef struct
{
    uint8_t         element_idx;    /**< Element Index of the source or destination of the message */
    uint16_t        company_id;     /**< Company ID for the Model ID */
    uint16_t        model_id;       /**< Model ID */
    uint16_t        opcode;         /**< Opcode of the message to be transmitted or that has been received */
    uint8_t         ttl;            /**< Time to leave to be used when transmitting or as received from the peer */
    uint16_t        src;            /**< Address of the source mesh node */
    uint16_t        dst;            /**< Address of the destination mesh node */
    uint8_t         app_key_idx;    /**< Application key index used to decrypt when message was received or which should be used to encrypt to send the message */
    wiced_bool_t    credential_flag;/**< Value of the Friendship Credential Flag */
    uint8_t         period;         /**< 2 bits - the resolution(0 - 100ms, 1 - 1sec, 2 - 10sec, 3 - 10min); 6 bits - The number of steps(0 - disabled) */
    uint8_t         retransmit;     /**< 3 bits - Number of retransmissions for each message; 5 bits - Number of 50-millisecond steps - 1 between retransmissions
                                             Use 0x41 if it isn't got from publication. It means 3 transmittions with 100 ms interval */
    wiced_bool_t    reply;          /**< If TRUE the reply is expected */
} wiced_bt_mesh_event_t;

/**
 * \brief Create message reply mesh event from the received mesh event
 * \details This function doesn't create but just updates received mesh event to be used to send message back to the originator.
 * After application or a models library calls Mesh Models Library or Mesh Core library passing the pointer to the mesh event, it loses the ownership of the event and should not
 * use it again.
 *
 * @param       p_event information about the message received.
 *
 * @return      Pointer to the input p_event updated for response
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_create_reply_event(wiced_bt_mesh_event_t *p_event);

/**
 * \brief Create the copy of the existing mesh event
 * \details This function creates new mesh event with same content as input mesh event.
 *
 * @param       p_event mesh event to copy.
 *
 * @return      Pointer to the created mesh event
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_copy_event(wiced_bt_mesh_event_t *p_event);

/**
 * \brief Create mesh event for an unsolicited message.
 * \details In case dst is 0 the function takes all information from the model's publication or fails if publication is not configured for the specified model.
 * In case non-0 dst the function uses specified dst and app_key_idx and fills all other fields with default values.
 * In special case with company_id equals to 0xffff the function creates a message event with default ttl and 0x41 retransmit.
 *
 * @param       element_index   Element index.
 * @param       company_id      Company ID.
 * @param       model_id        Model ID.
 * @param       dst             Destination address. If parameter is 0, the function finds publication and take uses its fields for the mesh event.
 * @param       app_key_idx     Global app key index. The parameter is ignored if dst is equal to 0. The 0xffff means device key.
 *
 * @return      p_event Pointer to a newly allocated mesh event.
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_create_event(uint8_t element_index, uint16_t company_id, uint16_t model_id, uint16_t dst, uint16_t app_key_idx);

/**
 * \brief Release mesh event
 * \details The application should call this function when it receives the mesh event in the callback and the reply value is set to 0.  
 *
 * @param       p_event information for the message.
 *
 * @return      None
 */
void wiced_bt_mesh_release_event(wiced_bt_mesh_event_t *p_event);

/* @} wiced_bt_mesh_core */

#ifdef __cplusplus
}
#endif

#endif /* __MESH_EVENT_H__ */
