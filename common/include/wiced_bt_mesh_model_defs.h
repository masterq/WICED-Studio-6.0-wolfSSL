/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */


/** @file
*
* Mesh Model definitions
*
*/
#ifndef __MESH_MODEL_DEFS_H__
#define __MESH_MODEL_DEFS_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @anchor MESH_COMPANY_ID
 * @name Mesh company IDs
 * \details Definitions of the company IDs
 * @{
 */
#define MESH_COMPANY_ID_BT_SIG              0x0000
#define MESH_COMPANY_ID_CYPRESS             0x0131
/** @} MESH_COMPANY_ID */

/**
* @anchor BT_MESH_GATT_UUID
* @name UUIDs for services and characteristics
* \details The following is the list of service and characteristic UUIDs that a mesh node can support.
* @{
*/
#define WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING                    0x1827
#define WICED_BT_MESH_CORE_UUID_SERVICE_PROXY                           0x1828
#define WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROVISIONING_DATA_IN     0x2ADB
#define WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROVISIONING_DATA_OUT    0x2ADC
#define WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROXY_DATA_IN            0x2ADD
#define WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROXY_DATA_OUT           0x2ADE
/** @} BT_MESH_GATT_UUID */

/**
 * @anchor MESH_ELEM_LOC
 * @name Element location
 * \details Element location (defined in GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers)
 * @{
 */
#define MESH_ELEM_LOC_UNKNOWN                                    0x0000
#define MESH_ELEM_LOC_FIRST                                      0x0001
#define MESH_ELEM_LOC_TWO_HUNDRED_AND_FIFTY_FIFTH                0x00ff
#define MESH_ELEM_LOC_FRONT                                      0x0100
#define MESH_ELEM_LOC_BACK                                       0x0101
#define MESH_ELEM_LOC_TOP                                        0x0102
#define MESH_ELEM_LOC_BOTTOM                                     0x0103
#define MESH_ELEM_LOC_UPPER                                      0x0104
#define MESH_ELEM_LOC_LOWER                                      0x0105
#define MESH_ELEM_LOC_MAIN                                       0x0106
#define MESH_ELEM_LOC_BACKUP                                     0x0107
#define MESH_ELEM_LOC_AUXILIARY                                  0x0108
#define MESH_ELEM_LOC_SUPPLEMENTARY                              0x0109
#define MESH_ELEM_LOC_FLASH                                      0x010a
#define MESH_ELEM_LOC_INSIDE                                     0x010b
#define MESH_ELEM_LOC_OUTSIDE                                    0x010c
#define MESH_ELEM_LOC_LEFT                                       0x010d
#define MESH_ELEM_LOC_RIGHT                                      0x010e
#define MESH_ELEM_LOC_INTERNAL                                   0x010f
#define MESH_ELEM_LOC_EXTERNAL                                   0x0110
/** @} MESH_ELEM_LOC */

/**
 * @anchor WICED_BT_MESH_MODEL_ID
 * @name Model identifiers
 * \details The following is the list of Blueoth SIG defined Model IDs that a mesh node can support.
 * @{
 */
#define WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV                          0x0000
#define WICED_BT_MESH_CORE_MODEL_ID_CONFIG_CLNT                         0x0001
#define WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV                          0x0002
#define WICED_BT_MESH_CORE_MODEL_ID_HEALTH_CLNT                         0x0003

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV                   0x1000
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT                  0x1001

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV                   0x1002
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_CLNT                  0x1003

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV                   0x1004
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT                  0x1005

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV               0x1006
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV         0x1007
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT              0x1008

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_SRV             0x1009
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_SETUP_SRV       0x100A
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_CLNT            0x100B

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_SRV                 0x100C
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_CLNT                0x100D

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_SRV                0x100E
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_SETUP_SRV          0x100F
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_CLNT               0x1010

#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ADMIN_PROPERTY_SRV          0x1011
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_MANUFACT_PROPERTY_SRV       0x1012
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_USER_PROPERTY_SRV           0x1013
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_CLIENT_PROPERTY_SRV         0x1014
#define WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT               0x1015

#define WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV                          0x1100
#define WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SETUP_SRV                    0x1101
#define WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT                         0x1102

#define WICED_BT_MESH_CORE_MODEL_ID_TIME_SRV                            0x1200
#define WICED_BT_MESH_CORE_MODEL_ID_TIME_SETUP_SRV                      0x1201
#define WICED_BT_MESH_CORE_MODEL_ID_TIME_CLNT                           0x1202
#define WICED_BT_MESH_CORE_MODEL_ID_SCENE_SRV                           0x1203
#define WICED_BT_MESH_CORE_MODEL_ID_SCENE_SETUP_SRV                     0x1204
#define WICED_BT_MESH_CORE_MODEL_ID_SCENE_CLNT                          0x1205
#define WICED_BT_MESH_CORE_MODEL_ID_SCHEDULER_SRV                       0x1206
#define WICED_BT_MESH_CORE_MODEL_ID_SCHEDULER_SETUP_SRV                 0x1207
#define WICED_BT_MESH_CORE_MODEL_ID_SCHEDULER_CLNT                      0x1208

#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV                 0x1300
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV           0x1301
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT                0x1302

#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SRV                       0x1303
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SETUP_SRV                 0x1304
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_CLNT                      0x1305
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_TEMPERATURE_SRV           0x1306

#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SRV                       0x1307
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SETUP_SRV                 0x1308
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_CLNT                      0x1309
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_HUE_SRV                   0x130A
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SATURATION_SRV            0x130B

#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SRV                       0x130C
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV                 0x130D
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_CLNT                      0x130E

#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SRV                        0x130F
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SETUP_SRV                  0x1310
#define WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_CLNT                       0x1311

/** @} WICED_BT_MESH_MODEL_ID */

/**
 * @anchor WICED_BT_MESH_MODEL_OPCODE
 * @name Mesh model opcodes
 * \details The following is the list of the Mesh model opcodes.
 * @{
 */

// Opcodes of the foundation models (Configuration and Health)
#define WICED_BT_MESH_CORE_CMD_APPKEY_ADD                                       0x00
#define WICED_BT_MESH_CORE_CMD_APPKEY_DELETE                                    0x8000
#define WICED_BT_MESH_CORE_CMD_APPKEY_GET                                       0x8001
#define WICED_BT_MESH_CORE_CMD_APPKEY_LIST                                      0x8002
#define WICED_BT_MESH_CORE_CMD_APPKEY_STATUS                                    0x8003
#define WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE                                    0x01

#define WICED_BT_MESH_CORE_CMD_ATTENTION_GET                                    0x8004
#define WICED_BT_MESH_CORE_CMD_ATTENTION_SET                                    0x8005
#define WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED                            0x8006
#define WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS                                 0x8007

#define WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET                           0x8008
#define WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS                        0x02

#define WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET                                0x8009
#define WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET                                0x800a
#define WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS                             0x800b

#define WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET                           0x800c
#define WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET                           0x800d
#define WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS                        0x800e

#define WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET                                0x800f
#define WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET                                0x8010
#define WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS                             0x8011

#define WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET                            0x8012
#define WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET                            0x8013
#define WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS                         0x8014

#define WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET                     0x8015
#define WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET                     0x8016
#define WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_STATUS                  0x8017

#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET                     0x8018
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET                     0x03
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_STATUS                  0x8019
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET           0x801a

#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD                    0x801b
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE                 0x801c
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL             0x801d
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE              0x801e
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS                 0x801f
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD          0x8020
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE       0x8021
#define WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE    0x8022

#define WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET                      0x8023
#define WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET                      0x8024
#define WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS                   0x8025

#define WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET                                 0x8026
#define WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET                                 0x8027
#define WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_STATUS                              0x8028

#define WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET                0x8029
#define WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_LIST               0x802a
#define WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET             0x802b
#define WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_LIST            0x802c

#define WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET                      0x802D
#define WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_STATUS                   0x802E
#define WICED_BT_MESH_CORE_CMD_HEALTH_CURRENT_STATUS                            0x04
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR                               0x802F
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED                       0x8030
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET                                 0x8031
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_STATUS                              0x05
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST                                0x8032
#define WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED                        0x8033
#define WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET                                0x8034
#define WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET                                0x8035
#define WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED                        0x8036
#define WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS                             0x8037

#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET                        0x8038
#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET                        0x8039
#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_STATUS                     0x06
#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET                       0x803A
#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET                       0x803B
#define WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_STATUS                    0x803C

#define WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND                                   0x803D
#define WICED_BT_MESH_CORE_CMD_MODEL_APP_STATUS                                 0x803E
#define WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND                                 0x803F

#define WICED_BT_MESH_CORE_CMD_NETKEY_ADD                                       0x8040
#define WICED_BT_MESH_CORE_CMD_NETKEY_DELETE                                    0x8041
#define WICED_BT_MESH_CORE_CMD_NETKEY_GET                                       0x8042
#define WICED_BT_MESH_CORE_CMD_NETKEY_LIST                                      0x8043
#define WICED_BT_MESH_CORE_CMD_NETKEY_STATUS                                    0x8044
#define WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE                                    0x8045

#define WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET                                0x8046
#define WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET                                0x8047
#define WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_STATUS                             0x8048

#define WICED_BT_MESH_CORE_CMD_NODE_RESET                                       0x8049
#define WICED_BT_MESH_CORE_CMD_NODE_RESET_STATUS                                0x804A

#define WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET                                0x804B
#define WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_LIST                               0x804C
#define WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET                             0x804D
#define WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_LIST                            0x804E

// Generic OnOff opcodes
#define WICED_BT_MESH_OPCODE_GEN_ONOFF_GET                                      0x8201
#define WICED_BT_MESH_OPCODE_GEN_ONOFF_SET                                      0x8202
#define WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED                              0x8203
#define WICED_BT_MESH_OPCODE_GEN_ONOFF_STATUS                                   0x8204

// Generic Level
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_GET                                      0x8205
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_SET                                      0x8206
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_SET_UNACKED                              0x8207
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_STATUS                                   0x8208
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET                                0x8209
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET_UNACKED                        0x820A
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET                                 0x820B
#define WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET_UNACKED                         0x820C

// Generic Default Transition Time opcodes
#define WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_GET                               0x820d
#define WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET                               0x820e
#define WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET_UNACKED                       0x820f
#define WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_STATUS                            0x8210

// Generic Power OnOff
#define WICED_BT_MESH_OPCODE_GEN_ONPOWERUP_GET                                  0x8211
#define WICED_BT_MESH_OPCODE_GEN_ONPOWERUP_STATUS                               0x8212

// Generic Power OnOff Setup
#define WICED_BT_MESH_OPCODE_GEN_ONPOWERUP_SET                                  0x8213
#define WICED_BT_MESH_OPCODE_GEN_ONPOWERUP_SET_UNACKED                          0x8214

// Generic Power Level
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_GET                                0x8215
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_SET                                0x8216
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_SET_UNACKED                        0x8217
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_STATUS                             0x8218
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_LAST_GET                           0x8219
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_LAST_STATUS                        0x821A
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_DEFAULT_GET                        0x821B
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_DEFAULT_STATUS                     0x821C
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_RANGE_GET                          0x821D
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_RANGE_STATUS                       0x821E

// Generic Power Level Setup
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_DEFAULT_SET                        0x821F
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_DEFAULT_SET_UNACKED                0x8220
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_RANGE_SET                          0x8221
#define WICED_BT_MESH_OPCODE_GEN_POWER_LEVEL_RANGE_SET_UNACKED                  0x8222

// Generic Battery
#define WICED_BT_MESH_OPCODE_GEN_BATTERY_GET                                    0x8223
#define WICED_BT_MESH_OPCODE_GEN_BATTERY_STATUS                                 0x8224

// Generic Location
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_GLOBAL_GET                            0x8225
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_GLOBAL_STATUS                         0x40
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_LOCAL_GET                             0x8226
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_LOCAL_STATUS                          0x8227

// Generic Location Setup
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_GLOBAL_SET                            0x41
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_GLOBAL_SET_UNACKED                    0x42
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_LOCAL_SET                             0x8228
#define WICED_BT_MESH_OPCODE_GEN_LOCATION_LOCAL_SET_UNACKED                     0x8229

// Generic Manufacturer Property
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTIES_GET                    0x822A
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTIES_STATUS                 0x43
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTY_GET                      0x822B
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTY_SET                      0x44
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTY_SET_UNACKED              0x45
#define WICED_BT_MESH_OPCODE_GEN_MANUFACTURER_PROPERTY_STATUS                   0x46

// Generic Admin Property
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTIES_GET                           0x822C
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTIES_STATUS                        0x47
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTY_GET                             0x822D
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTY_SET                             0x48
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTY_SET_UNACKED                     0x49
#define WICED_BT_MESH_OPCODE_GEN_ADMIN_PROPERTY_STATUS                          0x4A

// Generic User Property
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTIES_GET                            0x822E
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTIES_STATUS                         0x4B
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTY_GET                              0x822F
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTY_SET                              0x4C
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTY_SET_UNACKED                      0x4D
#define WICED_BT_MESH_OPCODE_GEN_USER_PROPERTY_STATUS                           0x4E

// Generic Client Property
#define WICED_BT_MESH_OPCODE_GEN_CLIENT_PROPERTIES_GET                          0x4F
#define WICED_BT_MESH_OPCODE_GEN_CLIENT_PROPERTIES_STATUS                       0x50

// Sensor
#define WICED_BT_MESH_OPCODE_SENSOR_DESCRIPTOR_GET                              0x8230
#define WICED_BT_MESH_OPCODE_SENSOR_DESCRIPTOR_STATUS                           0x51
#define WICED_BT_MESH_OPCODE_SENSOR_GET                                         0x8231
#define WICED_BT_MESH_OPCODE_SENSOR_STATUS                                      0x52
#define WICED_BT_MESH_OPCODE_SENSOR_COLUMN_GET                                  0x8232
#define WICED_BT_MESH_OPCODE_SENSOR_COLUMN_STATUS                               0x53
#define WICED_BT_MESH_OPCODE_SENSOR_SERIES_GET                                  0x8233
#define WICED_BT_MESH_OPCODE_SENSOR_SERIES_STATUS                               0x54

// Sensor Setup
#define WICED_BT_MESH_OPCODE_SENSOR_CADENCE_GET                                 0x8234
#define WICED_BT_MESH_OPCODE_SENSOR_CADENCE_SET                                 0x55
#define WICED_BT_MESH_OPCODE_SENSOR_CADENCE_SET_UNACKED                         0x56
#define WICED_BT_MESH_OPCODE_SENSOR_CADENCE_STATUS                              0x57
#define WICED_BT_MESH_OPCODE_SENSOR_SETTINGS_GET                                0x8235
#define WICED_BT_MESH_OPCODE_SENSOR_SETTINGS_STATUS                             0x58
#define WICED_BT_MESH_OPCODE_SENSOR_SETTING_GET                                 0x8236
#define WICED_BT_MESH_OPCODE_SENSOR_SETTING_SET                                 0x59
#define WICED_BT_MESH_OPCODE_SENSOR_SETTING_SET_UNACKED                         0x5A
#define WICED_BT_MESH_OPCODE_SENSOR_SETTING_STATUS                              0x5B

// Time
#define WICED_BT_MESH_OPCODE_TIME_GET                                           0x8237
#define WICED_BT_MESH_OPCODE_TIME_SET                                           0x5C
#define WICED_BT_MESH_OPCODE_TIME_STATUS                                        0x5D
#define WICED_BT_MESH_OPCODE_TIME_ROLE_GET                                      0x8238
#define WICED_BT_MESH_OPCODE_TIME_ROLE_SET                                      0x8239
#define WICED_BT_MESH_OPCODE_TIME_ROLE_STATUS                                   0x823A
#define WICED_BT_MESH_OPCODE_TIME_ZONE_GET                                      0x823B
#define WICED_BT_MESH_OPCODE_TIME_ZONE_SET                                      0x823C
#define WICED_BT_MESH_OPCODE_TIME_ZONE_STATUS                                   0x823D
#define WICED_BT_MESH_OPCODE_TAI_UTC_DELTA_GET                                  0x823E
#define WICED_BT_MESH_OPCODE_TAI_UTC_DELTA_SET                                  0x823F
#define WICED_BT_MESH_OPCODE_TAI_UTC_DELTA_STATUS                               0x8240

// Scene
#define WICED_BT_MESH_OPCODE_SCENE_GET                                          0x8241
#define WICED_BT_MESH_OPCODE_SCENE_RECALL                                       0x8242
#define WICED_BT_MESH_OPCODE_SCENE_RECALL_UNACKED                               0x8243
#define WICED_BT_MESH_OPCODE_SCENE_STATUS                                       0x5E
#define WICED_BT_MESH_OPCODE_SCENE_REGISTER_GET                                 0x8244
#define WICED_BT_MESH_OPCODE_SCENE_REGISTER_STATUS                              0x8245

// Scene Setup
#define WICED_BT_MESH_OPCODE_SCENE_STORE                                        0x8246
#define WICED_BT_MESH_OPCODE_SCENE_STORE_UNACKED                                0x8247
#define WICED_BT_MESH_OPCODE_SCENE_DELETE                                       0x829E
#define WICED_BT_MESH_OPCODE_SCENE_DELETE_UNACKED                               0x829F

// Scheduler
#define WICED_BT_MESH_OPCODE_SCHEDULER_ACTION_GET                               0x8248
#define WICED_BT_MESH_OPCODE_SCHEDULER_ACTION_STATUS                            0x5F
#define WICED_BT_MESH_OPCODE_SCHEDULER_GET                                      0x8249
#define WICED_BT_MESH_OPCODE_SCHEDULER_STATUS                                   0x824A

// Scheduler Setup
#define WICED_BT_MESH_OPCODE_SCHEDULER_ACTION_SET                               0x60
#define WICED_BT_MESH_OPCODE_SCHEDULER_ACTION_SET_UNACKED                       0x61

// Light Lightness
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_GET                                0x824B
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_SET                                0x824C
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_SET_UNACKED                        0x824D
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_STATUS                             0x824E
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LINEAR_GET                         0x824F
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LINEAR_SET                         0x8250
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LINEAR_SET_UNACKED                 0x8251
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LINEAR_STATUS                      0x8252
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LAST_GET                           0x8253
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_LAST_STATUS                        0x8254
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_DEFAULT_GET                        0x8255
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_DEFAULT_STATUS                     0x8256
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_RANGE_GET                          0x8257
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_RANGE_STATUS                       0x8258

// Light Lightness Setup
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_DEFAULT_SET                        0x8259
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_DEFAULT_SET_UNACKED                0x825A
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_RANGE_SET                          0x825B
#define WICED_BT_MESH_OPCODE_LIGHT_LIGHTNESS_RANGE_SET_UNACKED                  0x825C

// Light CTL
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_GET                                      0x825D
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_SET                                      0x825E
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_SET_UNACKED                              0x825F
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_STATUS                                   0x8260
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_GET                          0x8261
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_RANGE_GET                    0x8262
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_RANGE_STATUS                 0x8263
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_SET                          0x8264
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_SET_UNACKED                  0x8265
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_STATUS                       0x8266
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_DEFAULT_GET                              0x8267
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_DEFAULT_STATUS                           0x8268

// Light CTL Setup
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_DEFAULT_SET                              0x8269
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_DEFAULT_SET_UNACKED                      0x826A
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_RANGE_SET                    0x826B
#define WICED_BT_MESH_OPCODE_LIGHT_CTL_TEMPERATURE_RANGE_SET_UNACKED            0x826C

// Light HSL
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_GET                                      0x826D
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_HUE_GET                                  0x826E
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_HUE_SET                                  0x826F
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_HUE_SET_UNACKED                          0x8270
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_HUE_STATUS                               0x8271
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SATURATION_GET                           0x8272
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SATURATION_SET                           0x8273
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SATURATION_SET_UNACKED                   0x8274
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SATURATION_STATUS                        0x8275
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SET                                      0x8276
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_SET_UNACKED                              0x8277
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_STATUS                                   0x8278
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_TARGET_GET                               0x8279
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_TARGET_STATUS                            0x827A
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_DEFAULT_GET                              0x827B
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_DEFAULT_STATUS                           0x827C
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_RANGE_GET                                0x827D
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_RANGE_STATUS                             0x827E

// Light HSL Setup
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_DEFAULT_SET                              0x827F
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_DEFAULT_SET_UNACKED                      0x8280
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_RANGE_SET                                0x8281
#define WICED_BT_MESH_OPCODE_LIGHT_HSL_RANGE_SET_UNACKED                        0x8282

// Light xyL
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_GET                                      0x8283
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_SET                                      0x8284
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_SET_UNACKED                              0x8285
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_STATUS                                   0x8286
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_TARGET_GET                               0x8287
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_TARGET_STATUS                            0x8288
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_DEFAULT_GET                              0x8289
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_DEFAULT_STATUS                           0x828A
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_RANGE_GET                                0x828B
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_RANGE_STATUS                             0x828C

// Light xyL Setup
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_DEFAULT_SET                              0x828D
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_DEFAULT_SET_UNACKED                      0x828E
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_RANGE_SET                                0x828F
#define WICED_BT_MESH_OPCODE_LIGHT_XYL_RANGE_SET_UNACKED                        0x8290

// Light Control
#define WICED_BT_MESH_OPCODE_LIGHT_LC_MODE_GET                                  0x8291
#define WICED_BT_MESH_OPCODE_LIGHT_LC_MODE_SET                                  0x8292
#define WICED_BT_MESH_OPCODE_LIGHT_LC_MODE_SET_UNACKED                          0x8293
#define WICED_BT_MESH_OPCODE_LIGHT_LC_MODE_STATUS                               0x8294
#define WICED_BT_MESH_OPCODE_LIGHT_LC_OM_GET                                    0x8295
#define WICED_BT_MESH_OPCODE_LIGHT_LC_OM_SET                                    0x8296
#define WICED_BT_MESH_OPCODE_LIGHT_LC_OM_SET_UNACKED                            0x8297
#define WICED_BT_MESH_OPCODE_LIGHT_LC_OM_STATUS                                 0x8298
#define WICED_BT_MESH_OPCODE_LIGHT_LC_LIGHT_ONOFF_GET                           0x8299
#define WICED_BT_MESH_OPCODE_LIGHT_LC_LIGHT_ONOFF_SET                           0x829A
#define WICED_BT_MESH_OPCODE_LIGHT_LC_LIGHT_ONOFF_SET_UNACKED                   0x829B
#define WICED_BT_MESH_OPCODE_LIGHT_LC_LIGHT_ONOFF_STATUS                        0x829C
#define WICED_BT_MESH_OPCODE_LIGHT_LC_PROPERTY_GET                              0x829D
#define WICED_BT_MESH_OPCODE_LIGHT_LC_PROPERTY_SET                              0x62
#define WICED_BT_MESH_OPCODE_LIGHT_LC_PROPERTY_SET_UNACKED                      0x63
#define WICED_BT_MESH_OPCODE_LIGHT_LC_PROPERTY_STATUS                           0x64

#define WICED_BT_MESH_OPCODE_UNKNOWN                                            0xFFFF

//General purpose state update code
#define WICED_BT_MODEL_STATE_UPDATE                                             0xFFFFFF
#define WICED_BT_MODEL_MAX_OPCODE_LEN                                           0x03

/** @} WICED_BT_MESH_MODEL_OPCODE */

#define WICED_BT_MESH_LOCATION_GLOBAL_LATITUDE_UNKNOWN                          0x8000000
#define WICED_BT_MESH_LOCATION_GLOBAL_LONGITUDE_UNKNOWN                         0x8000000

/**
 * @anchor WICED_BT_MESH_ON_POWER_UP_STATE
 * @name On Power Up State definitions
 * @{
 */
#define WICED_BT_MESH_ON_POWER_UP_STATE_OFF                             0x00 /// Off. After being powered up, the element is in an off state.
#define WICED_BT_MESH_ON_POWER_UP_STATE_DEFAULT                         0x01 /// Default. After being powered up, the element is in an On state and uses default state values.
#define WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE                         0x02 /// Restore. If a transition was in progress when powered down, the element restores the target state when powered up. Otherwise the element restores the state it was in when powered down.
/** @} WICED_BT_MESH_ON_POWER_UP_STATE */

/**
 * @anchor WICED_BT_MESH_STATUS_CODE
 * @name Status code definitions
 * @{
 */
#define WICED_BT_MESH_STATUS_SUCCESS                                    0x00 /// Command successfully processed
#define WICED_BT_MESH_STATUS_CANNOT_SET_RANGE_MIN                       0x01 /// The provided value for Range Min cannot be set
#define WICED_BT_MESH_STATUS_CANNOT_SET_RANGE_MAX                       0x02 /// The provided value for Range Max cannot be set
/** @} WICED_BT_MESH_STATUS_CODE */

/**
 * @anchor WICED_BT_MESH_SCENE_STATUS_CODE
 * @name Scene operation Status code definitions
 * @{
 */
#define WICED_BT_MESH_SCENE_STATUS_SUCCESS                              0x00 /// Command successfully processed
#define WICED_BT_MESH_SCENE_STATUS_REGISTER_FULL                        0x01 /// Scene register full
#define WICED_BT_MESH_SCENE_STATUS_NOT_FOUND                            0x02 /// Scene not found
/** @} WICED_BT_MESH_SCENE_STATUS_CODE */

#define MESH_DEFAULT_TRANSITION_TIME_IN_MS                              0    /// Default transition time

/**
* @anchor WICED_BT_MESH_PROPERTY
* @name Mesh Device Properties as defined in the Mesh Device Property sepcification
* @{
*/
#define WICED_BT_MESH_PROPERTY_AVERAGE_AMBIENT_TEMPERATURE_IN_A_PERIOD_OF_DAY           0x0001
#define WICED_BT_MESH_PROPERTY_AVERAGE_INPUT_CURRENT                                    0x0002
#define WICED_BT_MESH_PROPERTY_AVERAGE_INPUT_VOLTAGE                                    0x0003
#define WICED_BT_MESH_PROPERTY_AVERAGE_OUTPUT_CURRENT                                   0x0004
#define WICED_BT_MESH_PROPERTY_AVERAGE_OUTPUT_VOLTAGE                                   0x0005
#define WICED_BT_MESH_PROPERTY_CENTER_BEAM_INTENSITY_AT_FULL_POWER                      0x0006
#define WICED_BT_MESH_PROPERTY_CHROMATICALLY_TOLERANCE                                  0x0007
#define WICED_BT_MESH_PROPERTY_COLOR_RENDERING_INDEX_R9                                 0x0008
#define WICED_BT_MESH_PROPERTY_COLOR_RENDERING_INDEX_RA                                 0x0009
#define WICED_BT_MESH_PROPERTY_DEVICE_APPEARANCE                                        0x000A
#define WICED_BT_MESH_PROPERTY_DEVICE_COUNTRY_OF_ORIGIN                                 0x000B
#define WICED_BT_MESH_PROPERTY_DEVICE_DATE_OF_MANUFACTURE                               0x000C
#define WICED_BT_MESH_PROPERTY_DEVICE_ENERGY_USE_SINCE_TURN_ON                          0x000D
#define WICED_BT_MESH_PROPERTY_DEVICE_FIRMWARE_REVISION                                 0x000E
#define WICED_BT_MESH_PROPERTY_DEVICE_GLOBAL_TRADE_ITEM_NUMBER                          0x000F
#define WICED_BT_MESH_PROPERTY_DEVICE_HARDWARE_REVISION                                 0x0010
#define WICED_BT_MESH_PROPERTY_DEVICE_MANUFACTURER_NAME                                 0x0011
#define WICED_BT_MESH_PROPERTY_DEVICE_MODEL_NUMBER                                      0x0012
#define WICED_BT_MESH_PROPERTY_DEVICE_OPERATING_TEMPERATURE_RANGE_SPECIFICATION         0x0013
#define WICED_BT_MESH_PROPERTY_DEVICE_OPERATING_TEMPERATURE_STATISTICAL_VALUES          0x0014
#define WICED_BT_MESH_PROPERTY_DEVICE_OVER_TEMPERATURE_EVENT_STATISTICS                 0x0015
#define WICED_BT_MESH_PROPERTY_DEVICE_POWER_RANGE_SPECIFICATION                         0x0016
#define WICED_BT_MESH_PROPERTY_DEVICE_RUNTIME_SINCE_TURN_ON                             0x0017
#define WICED_BT_MESH_PROPERTY_DEVICE_RUNTIME_WARRANTY                                  0x0018
#define WICED_BT_MESH_PROPERTY_DEVICE_SERIAL_NUMBER                                     0x0019
#define WICED_BT_MESH_PROPERTY_DEVICE_SOFTWARE_REVISION                                 0x001A
#define WICED_BT_MESH_PROPERTY_DEVICE_UNDER_TEMPERATURE_EVENT_STATISTICS                0x001B
#define WICED_BT_MESH_PROPERTY_INDOOR_AMBIENT_TEMPERATURE_STATISTICAL_VALUES            0x001C
#define WICED_BT_MESH_PROPERTY_INITIAL_CIE 1931 CHROMATICITY_COORDINATES                0x001D
#define WICED_BT_MESH_PROPERTY_INITIAL_CORRELATED COLOR_TEMPERATURE                     0x001E
#define WICED_BT_MESH_PROPERTY_INITIAL_LUMINOUS_FLUX                                    0x001F
#define WICED_BT_MESH_PROPERTY_INITIAL_PLANCKIAN_DISTANCE                               0x0020
#define WICED_BT_MESH_PROPERTY_INPUT_CURRENT_RANGE_SPECIFICATION                        0x0021
#define WICED_BT_MESH_PROPERTY_INPUT_CURRENT_STATISTICS                                 0x0022
#define WICED_BT_MESH_PROPERTY_INPUT_OVER_CURRENT_EVENT_STATISTICS                      0x0023
#define WICED_BT_MESH_PROPERTY_INPUT_OVER_RIPPLE_VOLTAGE_EVENT_STATISTICS               0x0024
#define WICED_BT_MESH_PROPERTY_INPUT_OVER_VOLTAGE_EVENT_STATISTICS                      0x0025
#define WICED_BT_MESH_PROPERTY_INPUT_UNDER_CURRENT_EVENT_STATISTICS                     0x0026
#define WICED_BT_MESH_PROPERTY_INPUT_UNDER_VOLTAGE_EVENT_STATISTICS                     0x0027
#define WICED_BT_MESH_PROPERTY_INPUT_VOLTAGE_RANGE_SPECIFICATION                        0x0028
#define WICED_BT_MESH_PROPERTY_INPUT_VOLTAGE_RIPPLE_SPECIFICATION                       0x0029
#define WICED_BT_MESH_PROPERTY_INPUT_VOLTAGE_STATISTICS                                 0x002A
#define WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_ON                                     0x002B
#define WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_PROLONG                                0x002C
#define WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_STANDBY                                0x002D
#define WICED_BT_MESH_PROPERTY_LIGHTNESS_ON                                             0x002E
#define WICED_BT_MESH_PROPERTY_LIGHTNESS_PROLONG                                        0x002F
#define WICED_BT_MESH_PROPERTY_LIGHTNESS_STANDBY                                        0x0030
#define WICED_BT_MESH_PROPERTY_REGULATOR_ACCURACY                                       0x0031
#define WICED_BT_MESH_PROPERTY_REGULATOR_KID                                            0x0032
#define WICED_BT_MESH_PROPERTY_REGULATOR_KIU                                            0x0033
#define WICED_BT_MESH_PROPERTY_REGULATOR_KPD                                            0x0034
#define WICED_BT_MESH_PROPERTY_REGULATOR_KPU                                            0x0035
#define WICED_BT_MESH_PROPERTY_TIME_FADE                                                0x0036
#define WICED_BT_MESH_PROPERTY_TIME_FADE_ON                                             0x0037
#define WICED_BT_MESH_PROPERTY_TIME_FADE_STANDBY_AUTO                                   0x0038
#define WICED_BT_MESH_PROPERTY_TIME_FADE_STANDBY_MANUAL                                 0x0039
#define WICED_BT_MESH_PROPERTY_TIME_OCCUPANCY_DELAY                                     0x003A
#define WICED_BT_MESH_PROPERTY_TIME_PROLONG                                             0x003B
#define WICED_BT_MESH_PROPERTY_TIME_RUN_ON                                              0x003C
#define WICED_BT_MESH_PROPERTY_LUMEN_MAINTENANCE_FACTOR                                 0x003D
#define WICED_BT_MESH_PROPERTY_LUMINOUS_EFFICICACY                                      0x003E
#define WICED_BT_MESH_PROPERTY_LUMINOUS_ENERGY_SINCE_TURN_ON                            0x003F
#define WICED_BT_MESH_PROPERTY_LUMINOUS_EXPOSURE                                        0x0040
#define WICED_BT_MESH_PROPERTY_LUMINOUS_FLUX_RANGE                                      0x0041
#define WICED_BT_MESH_PROPERTY_MOTION_SENSED                                            0x0042
#define WICED_BT_MESH_PROPERTY_MOTION_THRESHOLD                                         0x0043
#define WICED_BT_MESH_PROPERTY_OPEN_CIRCUIT_EVENT_STATISTICS                            0x0044
#define WICED_BT_MESH_PROPERTY_OUTDOOR_STATISTICAL_VALUES                               0x0045
#define WICED_BT_MESH_PROPERTY_OUTPUT_CURRENT_RANGE                                     0x0046
#define WICED_BT_MESH_PROPERTY_OUTPUT_CURRENT_STATISTICS                                0x0047
#define WICED_BT_MESH_PROPERTY_OUTPUT_RIPPLE_VOLTAGE_SPECIFICATION                      0x0048
#define WICED_BT_MESH_PROPERTY_OUTPUT_VOLTAGE_RANGE                                     0x0049
#define WICED_BT_MESH_PROPERTY_OUTPUT_VOLTAGE_STATISTICS                                0x004A
#define WICED_BT_MESH_PROPERTY_OVER_OUTPUT_RIPPLE_VOLTAGE_EVENT_STATISTICS              0x004B
#define WICED_BT_MESH_PROPERTY_PEOPLE_COUNT                                             0x004C
#define WICED_BT_MESH_PROPERTY_PRESENCE_DETECTED                                        0x004D
#define WICED_BT_MESH_PROPERTY_PRESENT_AMBIENT_LIGHT_LEVEL                              0x004E
#define WICED_BT_MESH_PROPERTY_PRESENT_AMBIENT_TEMPERATURE                              0x004F
#define WICED_BT_MESH_PROPERTY_PRESENT_CIE 1931 CHROMATICITY_COORDINATES                0x0050
#define WICED_BT_MESH_PROPERTY_PRESENT_CORRELATED COLOR_TEMPERATURE                     0x0051
#define WICED_BT_MESH_PROPERTY_PRESENT_DEVICE_INPUT_POWER                               0x0052
#define WICED_BT_MESH_PROPERTY_PRESENT_DEVICE_OPERATING_EFFICIENCY                      0x0053
#define WICED_BT_MESH_PROPERTY_PRESENT_DEVICE_OPERATING_TEMPERATURE                     0x0054
#define WICED_BT_MESH_PROPERTY_PRESENT_ILLUMINANCE                                      0x0055
#define WICED_BT_MESH_PROPERTY_PRESENT_INDOOR_AMBIENT_TEMPERATURE                       0x0056
#define WICED_BT_MESH_PROPERTY_PRESENT_INPUT_CURRENT                                    0x0057
#define WICED_BT_MESH_PROPERTY_PRESENT_INPUT_RIPPLE_VOLTAGE                             0x0058
#define WICED_BT_MESH_PROPERTY_PRESENT_INPUT_VOLTAGE                                    0x0059
#define WICED_BT_MESH_PROPERTY_PRESENT_LUMINOUS_FLUX                                    0x005A
#define WICED_BT_MESH_PROPERTY_PRESENT_OUTDOOR_AMBIENT_TEMPERATURE                      0x005B
#define WICED_BT_MESH_PROPERTY_PRESENT_OUTPUT_CURRENT                                   0x005C
#define WICED_BT_MESH_PROPERTY_PRESENT_OUTPUT_VOLTAGE                                   0x005D
#define WICED_BT_MESH_PROPERTY_PRESENT_PLANCKIAN_DISTANCE                               0x005E
#define WICED_BT_MESH_PROPERTY_PRESENT_RELATIVE_OUTPUT_RIPPLE_VOLTAGE                   0x005F
#define WICED_BT_MESH_PROPERTY_RELATIVE_DEVICE_ENERGY_USE_IN_A_PERIOD_OF_DAY            0x0060
#define WICED_BT_MESH_PROPERTY_RELATIVE_DEVICE_RUNTIME_IN_A_GENERIC_LEVEL_RANGE         0x0061
#define WICED_BT_MESH_PROPERTY_RELATIVE_EXPOSURE_TIME_IN_AN_ILLUMINANCE RANGE           0x0062
#define WICED_BT_MESH_PROPERTY_RELATIVE_RUNTIME_IN_A_CORRELATED COLOR_TEMPERATURE_RANGE 0x0063
#define WICED_BT_MESH_PROPERTY_RELATIVE_RUNTIME_IN_A_DEVICE_OPERATING_TEMPERATURE_RANGE 0x0064
#define WICED_BT_MESH_PROPERTY_RELATIVE_RUNTIME_IN_AN_INPUT_CURRENT_RANGE               0x0065
#define WICED_BT_MESH_PROPERTY_RELATIVE_RUNTIME_IN_AN_INPUT_VOLTAGE_RANGE               0x0066
#define WICED_BT_MESH_PROPERTY_SHORT_CIRCUIT_EVENT_STATISTICS                           0x0067
#define WICED_BT_MESH_PROPERTY_TIME_SINCE_MOTION_SENSED                                 0x0068
#define WICED_BT_MESH_PROPERTY_TIME_SINCE_PRESENCE_DETECTED                             0x0069
#define WICED_BT_MESH_PROPERTY_TOTAL_DEVICE_ENERGY_USE                                  0x006A
#define WICED_BT_MESH_PROPERTY_TOTAL_DEVICE_Off ON_CYCLES                               0x006B
#define WICED_BT_MESH_PROPERTY_TOTAL_DEVICE_POWER_ON_CYCLES                             0x006C
#define WICED_BT_MESH_PROPERTY_TOTAL_DEVICE_POWER_ON_TIME                               0x006D
#define WICED_BT_MESH_PROPERTY_TOTAL_DEVICE_RUNTIME                                     0x006E
#define WICED_BT_MESH_PROPERTY_TOTAL_LIGHT_EXPOSURE_TIME                                0x006F
#define WICED_BT_MESH_PROPERTY_TOTAL_LUMINOUS_ENERGY                                    0x0070
/** @} WICED_BT_MESH_PROPERTY */

// Only values of 0x00 through 0x3E shall be used to specify the value of the Transition Number of Steps field.
#define WICED_BT_MESH_VALID_TRANSITION_TIME(x) (((x) & 0x3f) != 0x3f)

/// Macros to convert light lightness linear to actual and actual to linear
#define LIGHT_LIGHTNESS_LINEAR_FROM_ACTUAL(x)         (((uint32_t)(x) * (x) + 32767) / 65535)
#define LIGHT_LIGHTNESS_ACTUAL_FROM_LINEAR(x)         (utl_sqrt((uint32_t)65535 * (x)))

/// Color Temperature range as defined in Light CTL model
#define WICED_BT_MESH_LIGHT_CTL_TEMPERATURE_MIN       0x0320
#define WICED_BT_MESH_LIGHT_CTL_TEMPERATURE_MAX       0x4e20

///Time Roles defines the role of a node in propagation of time information in a mesh network.
#define WICED_BT_MESH_TIME_ROLE_NONE                  0x00 /**< The element does not participate in propagation of time information*/
#define WICED_BT_MESH_TIME_ROLE_MESH_TIME_AUTHORITY   0x01 /**< The element publishes Time Status messages but does not process received Time Status messages*/
#define WICED_BT_MESH_TIME_ROLE_MESH_TIME_RELAY       0x02 /**< The element processes received and publishes Time Status messages*/
#define WICED_BT_MESH_TIME_ROLE_MESH_TIME_CLIENT      0x03 /**< The element does not publish but processes received Time Status messages*/

#define WICED_BT_MESH_SCHEDULER_MAX_EVENTS            16   /**< Maximum events that can be scheduled on the node */

/**
 * @anchor MESH_BEACON_STATES
 * @name Mesh Beacon States
 * @{
 */
#define WICED_BT_MESH_BEACON_NOT_BROADCASTING           0x00 ///< The node is not broadcasting a Secure Network beacon
#define WICED_BT_MESH_BEACON_BROADCASTING               0x01 ///< The node is broadcasting a Secure Network beacon
/** @} MESH_BEACON_STATES */

/**
 * @anchor MESH_GATT_PROXY_STATES
 * @name Mesh GATT Proxy States
 * @{
 */
#define WICED_BT_MESH_GATT_PROXY_SERVICE_DISABLED       0x00 ///< The Mesh Proxy Service is running, Proxy feature is disabled
#define WICED_BT_MESH_GATT_PROXY_SERVICE_ENABLED        0x01 ///< The Mesh Proxy Service is running, Proxy feature is enabled
#define WICED_BT_MESH_GATT_PROXY_SERVICE_NOT_SUPPORTED  0x02 ///< The Mesh Proxy Service is not supported, Proxy feature is not supported
/** @} MESH_GATT_PROXY_STATES */

/**
 * @anchor MESH_FRIEND_STATES
 * @name Mesh Friend States
 * @{
 */
#define WICED_BT_MESH_FRIEND_FEATURE_DISABLED           0x00 ///< The node supports Friend feature that is disabled
#define WICED_BT_MESH_FRIEND_FEATURE_ENABLED            0x01 ///< The node supports Friend feature that is enabled
#define WICED_BT_MESH_FRIEND_FEATURE_NOT_SUPPORTED      0x02 ///< The Friend feature is not supported
/** @} MESH_GATT_PROXY_STATES */

/**
 * @anchor MESH_KEY_REFRESH_PHASES
 * @name Mesh Friend States
 * @{
 */
#define WICED_BT_MESH_KEY_REFRESH_PHASE_NORMAL          0x00 ///< Normal operation; Key Refresh procedure is not active
#define WICED_BT_MESH_KEY_REFRESH_PHASE_FIRST           0x01 ///< First phase of Key Refresh procedure
#define WICED_BT_MESH_KEY_REFRESH_PHASE_SECOND          0x02 ///< Second phase of Key Refresh procedure
/** @} MESH_KEY_REFRESH_PHASES */

/**
 * @anchor MESH_IDENTITY_STATES
 * @name Mesh Node Identity For a Subnet States
 * @{
 */
#define WICED_BT_MESH_GATT_NODE_IDENTITY_STOPPED        0x00 ///< Node Identity for a subnet is stopped
#define WICED_BT_MESH_GATT_NODE_IDENTITY_RUNNING        0x01 ///< Node Identity for a subnet is running
#define WICED_BT_MESH_GATT_NODE_IDENTITY_NOT_SUPPORTED  0x02 ///< Node Identity is not supported
/** @} MESH_IDENTITY_STATES */

#ifdef __cplusplus
}
#endif

#endif /* __FND_H__ */
