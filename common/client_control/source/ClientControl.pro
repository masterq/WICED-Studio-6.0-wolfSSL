#-------------------------------------------------
#
# Project created by QtCreator 2016-11-07T14:38:11
#
#-------------------------------------------------

QT += core gui
QT += serialport
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClientControl
TEMPLATE = app

win32|win64 {
    DESTDIR = $$PWD/../Windows
}

macx {
    DESTDIR = $$PWD/../OSX
}

SOURCES += main.cpp\
        mainwindow.cpp \
        audio_src.cpp \
        device_manager.cpp \
        handsfree.cpp \
        pbap_client.cpp \
        spp.cpp \
        audio_gateway.cpp \
        hid_device.cpp \
        hid_host.cpp \
        home_kit.cpp \
        avrc_tg.cpp \
        avrc_ct.cpp \
        iap2.cpp \
        fw_download.cpp \
        gatt.cpp \
        audio_snk.cpp \
        bt_serial_gatt.cpp \ 
        mesh.cpp \
        battery_client.cpp \
        findme_locator.cpp \
        demo.cpp

unix:!macx {
    SOURCES += serial_port_linux.cpp
    SOURCES += btspy_ux.cpp
}

win32|win64 {
    SOURCES += btspy_win32.cpp
    SOURCES += serial_port_win32.cpp
}

macx {
    SOURCES += serial_port_mac.cpp
    SOURCES += btspy_ux.cpp
}


HEADERS  += mainwindow.h \
            fw_download.h \
            avrc.h \
            serial_port.h \
            app_include.h

FORMS    += mainwindow.ui

INCLUDEPATH += ../../include

# ws2_32.lib and winmm.lib path might need to be adjusted on user PC, for example
# C:\WINDDK\7600.16385.0\lib\win7\i386\ws2_32.lib
# -L"Windows" -lws2_32
win32: LIBS += -lQt5Network ..\Windows\ws2_32.lib ..\Windows\winmm.lib

unix:!macx {
LIBS += -lasound -lrt
DEFINES += PCM_ALSA
}

RESOURCES     = resources.qrc

RC_ICONS = CY_Logo.ico

ICON = CY_Logo.icns

DISTFILES += \
    ../README.txt
