/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/*
 * Sample MCU application for BLE or BR/EDR HID Device using WICED HCI protocol.
 */

#include "app_include.h"

// Initialize app
void MainWindow::InitBLEHIDD()
{
    m_pairing_mode_active = FALSE;
    m_b_is_hidd = false;
    ui->cbBLEHIDInterupt->clear();
    ui->cbBLEHIDReport->clear();

    ui->cbBLEHIDInterupt->addItem("Control Channel", HCI_CONTROL_HID_REPORT_CHANNEL_CONTROL);
    ui->cbBLEHIDInterupt->addItem("Interrupt Channel", HCI_CONTROL_HID_REPORT_CHANNEL_INTERRUPT);

    ui->cbBLEHIDReport->addItem("Other", HCI_CONTROL_HID_REPORT_TYPE_OTHER);
    ui->cbBLEHIDReport->addItem("Input", HCI_CONTROL_HID_REPORT_TYPE_INPUT);
    ui->cbBLEHIDReport->addItem("Output", HCI_CONTROL_HID_REPORT_TYPE_OUTPUT);
    ui->cbBLEHIDReport->addItem("Feature", HCI_CONTROL_HID_REPORT_TYPE_FEATURE);

    ui->cbBLEHIDInterupt->setCurrentIndex(1);
    ui->cbBLEHIDReport->setCurrentIndex(1);
}

// Connect to peer device
void MainWindow::on_btnBLEHIDConnect_clicked()
{
    Log("Sending HID Connect Command");

    SendWicedCommand(HCI_CONTROL_HID_COMMAND_CONNECT, NULL, 0);
}

// Disconnect from peer device
void MainWindow::on_btnBLEHIDDisconnect_clicked()
{
    Log("Sending HID Disconnect Command");

    SendWicedCommand(HCI_CONTROL_HIDD_COMMAND_DISCONNECT, NULL, 0);
}

// Send HID report
void MainWindow::on_btnBLEHIDSendReport_clicked()
{
   char szLog[80] = { 0 };
   BYTE  cmd[60];

   QVariant v1 = ui->cbBLEHIDInterupt->currentData();
   cmd[0] = (BYTE)v1.toUInt();

   QVariant v2 = ui->cbBLEHIDReport->currentData();
   cmd[1] = (BYTE)v2.toUInt();

   QString str = ui->lineEditBLEHIDSendText->text();

   int num_digits = GetHexValue(&cmd[2], 50, str);

   for (int i = 0; i < num_digits; i++)
       sprintf(&szLog[strlen(szLog)], "%02x ", cmd[i + 2]);

   Log("Sending HID Report: channel %d, report %d, %s",  cmd[0], cmd[1], szLog);

   SendWicedCommand(HCI_CONTROL_HID_COMMAND_SEND_REPORT, cmd, 2 + num_digits);
}

// Enter pairing mode
void MainWindow::on_btnBLEHIDPairingMode_clicked()
{
    BYTE    cmd[1];
    if (!m_pairing_mode_active)
    {
        ui->btnBLEHIDPairingMode->setText("Exit Pairing Mode");
        m_pairing_mode_active = TRUE;
        cmd[0] = 1;
    }
    else
    {
        ui->btnBLEHIDPairingMode->setText("Enter Pairing Mode");
        m_pairing_mode_active = FALSE;
        cmd[0] = 0;
    }
    SendWicedCommand(HCI_CONTROL_HID_COMMAND_ACCEPT_PAIRING, cmd, 1);
}

// Send HID report key
void MainWindow::on_btnBLEHIDSendKey_clicked()
{
    char buf[100] = { 0 };
    QString str = ui->cbBLEHIDKey->currentText();
    strcpy(buf, str.toStdString().c_str());    
    BYTE  cmd[60] = { 0 };
    cmd[0] = HCI_CONTROL_HID_REPORT_CHANNEL_INTERRUPT;
    cmd[1] = HCI_CONTROL_HID_REPORT_TYPE_INPUT;
    cmd[2] = 1; // report id
    cmd[3] = ui->cbBLEHIDCapLock->isChecked() ? 0x20 : 0x00 |
        ui->cbBLEHIDCtrl->isChecked() ? 0x80 : 0x00 |
        ui->cbBLEHIDAlt->isChecked() ? 0x40 : 0x00;

    for (int i = 0; (i < 6) && (buf[i] != 0); i++)
    {
        if ((buf[i] >= '1') && (buf[i] <= '9'))
            cmd[5 + i] = 0x1e + buf[i] - '1';
        if (buf[i] == '0')
            cmd[5 + i] = 0x27;
        if ((buf[i] >= 'a') && (buf[i] <= 'z'))
            cmd[5 + i] = 0x04 + buf[i] - 'a';
    }
    char trace[256];
    sprintf(trace, "Send HID Report Type:%d ID:%d %02x %02x %02x %02x %02x %02x %02x %02x", cmd[1], cmd[2], cmd[3], cmd[4], cmd[5], cmd[6], cmd[7], cmd[8], cmd[9], cmd[10]);
    Log(trace);
    SendWicedCommand(HCI_CONTROL_HID_COMMAND_SEND_REPORT, cmd, 11);

    if (ui->cbBLEHIDBtnUp->isChecked())
    {
        for (int i = 0; i < 7; i++)
            cmd[4 + i] = 0;
        SendWicedCommand(HCI_CONTROL_HID_COMMAND_SEND_REPORT, cmd, 11);
    }
}

void MainWindow::on_cbBLEHIDCapLock_clicked()
{
    BYTE  cmd[60] = { 0 };
    cmd[0] = HCI_CONTROL_HID_REPORT_CHANNEL_INTERRUPT;
    cmd[1] = HCI_CONTROL_HID_REPORT_TYPE_INPUT;
    cmd[2] = 1; // report id
    cmd[3] = ui->cbBLEHIDCapLock->isChecked() ? 0x20 : 0x00 |
        ui->cbBLEHIDCtrl->isChecked() ? 0x80 : 0x00 |
        ui->cbBLEHIDAlt->isChecked() ? 0x40 : 0x00;
    for (int i = 0; i < 7; i++)
        cmd[4 + i] = 0;
    SendWicedCommand(HCI_CONTROL_HID_COMMAND_SEND_REPORT, cmd, 11);
}

void MainWindow::on_btnBLEHIDDVirtualUnplug_clicked()
{
    Log("Sending HIDD Virtual Unplug Command");

    SendWicedCommand(HCI_CONTROL_HIDD_COMMAND_VIRTUAL_UNPLUG, NULL, 0);
}


void MainWindow::on_cbBLEHIDCtrl_clicked()
{
    on_cbBLEHIDCapLock_clicked();
}

void MainWindow::on_cbBLEHIDAlt_clicked()
{
    on_cbBLEHIDCapLock_clicked();
}

// Handle WICED HCI events for BLE/BR HID device
void MainWindow::onHandleWicedEventBLEHIDD(unsigned int opcode, unsigned char *p_data, unsigned int len)
{
    char   trace[1024];

    switch (opcode)
    {
        case HCI_CONTROL_EVENT_DEVICE_STARTED:
            m_pairing_mode_active = FALSE;
            ui->btnBLEHIDPairingMode->setText("Enter Pairing Mode");
            break;

        case HCI_CONTROL_HID_EVENT_OPENED:
            Log("HID connection opened");
            ui->btnBLEHIDPairingMode->setText("Enter Pairing Mode");
            m_pairing_mode_active = FALSE;
            break;
        case HCI_CONTROL_LE_EVENT_ADVERTISEMENT_STATE:
            sprintf(trace, "Advertisement state:%d", p_data[0]);
            Log(trace);
            if (p_data[0] == 0)
            {
                ui->btnBLEHIDPairingMode->setText("Enter Pairing Mode");
                m_pairing_mode_active = FALSE;
            }
            break;
        case HCI_CONTROL_HID_EVENT_VIRTUAL_CABLE_UNPLUGGED:
            Log("HID Virtual Cable Unplugged");
            break;
        case HCI_CONTROL_HID_EVENT_DATA:
            sprintf(trace, "Recv HID Report type:%d ", p_data[0]);
            for (uint i = 0; i < len - 1; i++)
                sprintf(&trace[strlen(trace)], "%02x ", p_data[i + 1]);
            Log(trace);
            break;
        case HCI_CONTROL_HID_EVENT_CLOSED:
            sprintf(trace, "HID Connection Down reason::%d ", p_data[0]);
            Log(trace);
            break;
    }
}
