/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/*
 * Sample MCU application for AVRC Controller (AVRC-CT), AMS and ANCS using WICED HCI protocol.
 */


#include "app_include.h"
#include "avrc.h"

tAPP_AVRC_META_ATTRIB     repeat_ct;
tAPP_AVRC_META_ATTRIB     shuffle_ct;

static const char *szAvrcPlayStatus[] = { "Stopped", "Playing", "Paused", "Fwd Seek", "Rev Seek" };
const char *repeatStr[] = { "Off", "Single", "All", "Group"};
const char *shuffleStr[] = { "Off", "All", "Group"};

// Initialize app
void MainWindow::InitAVRCCT()
{
    // setup signals/slots
    connect(ui->btnCTPlay, SIGNAL(clicked()), this, SLOT(onCTPlay()));
    connect(ui->btnCTStop, SIGNAL(clicked()), this, SLOT(onCTStop()));
    connect(ui->btnCTPause, SIGNAL(clicked()), this, SLOT(onCTPause()));
    connect(ui->btnCTPrev, SIGNAL(clicked()), this, SLOT(onCTPrevious()));
    connect(ui->btnCTNext, SIGNAL(clicked()), this, SLOT(onCTNext()));
    connect(ui->btnCTSeekBack, SIGNAL(clicked()), this, SLOT(onCTSkipBackward()));
    connect(ui->btnCTSeekFwd, SIGNAL(clicked()), this, SLOT(onCTSkipForward()));
    connect(ui->btnCTMute, SIGNAL(clicked()), this, SLOT(onCTMute()));
    connect(ui->btnCTVolumeUp, SIGNAL(clicked()), this, SLOT(onCTVolumeUp()));
    connect(ui->btnCTVolumeDown, SIGNAL(clicked()), this, SLOT(onCTVolumeDown()));
    connect(ui->cbCTRepeat, SIGNAL(currentIndexChanged(int)), this, SLOT(onCTRepeatMode(int)));
    connect(ui->cbCTShuffle, SIGNAL(currentIndexChanged(int)), this, SLOT(onCTShuffleMode(int)));

    connect(ui->cbCTVolume, SIGNAL(currentIndexChanged(int)), this, SLOT(cbCTVolumeChanged(int)));
    connect(ui->btnCTConnect, SIGNAL(clicked()), this, SLOT(onCTConnect()));
    connect(ui->btnCTDisconnect, SIGNAL(clicked()), this, SLOT(onCTDisconnect()));
    connect(ui->btnCTANCSPositive, SIGNAL(clicked()), this, SLOT(OnBnClickedAncsPositive()));
    connect(ui->btnCTANCSNegative, SIGNAL(clicked()), this, SLOT(OnBnClickedAncsNegative()));

    ui->btnCTPlay->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->btnCTPrev->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    ui->btnCTStop->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    ui->btnCTPause->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    ui->btnCTNext->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    ui->btnCTSeekFwd->setIcon(style()->standardIcon(QStyle::SP_MediaSeekForward));
    ui->btnCTSeekBack->setIcon(style()->standardIcon(QStyle::SP_MediaSeekBackward));
    ui->btnCTMute->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));


    // Table for Music media player elements
    ui->tblMetaPlayerCT->horizontalHeader()->setStretchLastSection(true);
    ui->tblMetaPlayerCT->verticalHeader()->setVisible(true);
    ui->tblMetaPlayerCT->setRowCount(7);
    ui->tblMetaPlayerCT->setColumnCount(2);
    ui->tblMetaPlayerCT->setColumnWidth(0,120);
    ui->tblMetaPlayerCT->setColumnWidth(1,330);

    QStringList list;

    m_volMute = true;

    list.clear();
    list<<"Media Element"<<"Value";
    ui->tblMetaPlayerCT->setHorizontalHeaderLabels(list);

    // Mute is not available in stack
    ui->btnCTMute->setVisible(false);

    ui->cbCTRepeat->clear();
    ui->cbCTRepeat->addItem(repeatStr[APP_AVRC_PLAYER_VAL_OFF-1], APP_AVRC_PLAYER_VAL_OFF);
    ui->cbCTRepeat->addItem(repeatStr[APP_AVRC_PLAYER_VAL_SINGLE_REPEAT-1], APP_AVRC_PLAYER_VAL_SINGLE_REPEAT);
    ui->cbCTRepeat->addItem(repeatStr[APP_AVRC_PLAYER_VAL_ALL_REPEAT-1], APP_AVRC_PLAYER_VAL_ALL_REPEAT);
    //ui->cbCTRepeat->addItem(repeatStr[APP_AVRC_PLAYER_VAL_GROUP_REPEAT-1], APP_AVRC_PLAYER_VAL_GROUP_REPEAT);

    ui->cbCTShuffle->clear();
    ui->cbCTShuffle->addItem(shuffleStr[APP_AVRC_PLAYER_VAL_OFF-1], APP_AVRC_PLAYER_VAL_OFF);
    ui->cbCTShuffle->addItem(shuffleStr[APP_AVRC_PLAYER_VAL_ALL_SHUFFLE-1], APP_AVRC_PLAYER_VAL_ALL_SHUFFLE);
    //ui->cbCTShuffle->addItem(shuffleStr[APP_AVRC_PLAYER_VAL_GROUP_SHUFFLE-1], APP_AVRC_PLAYER_VAL_GROUP_SHUFFLE);

    m_current_volume_pct = 50;
    ui->cbCTVolume->clear();
    for(int i = 0; i < 101; i++)
    {
        char s[100];
        sprintf(s, "%d", i);
        ui->cbCTVolume->addItem(s);
    }
    ui->cbTGVolume->setCurrentIndex(m_current_volume_pct);

}

// Connect to peer device
void MainWindow::onCTConnect()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    if (m_CommPort == NULL)
        return;

    if (!m_bPortOpen)
    {
         return;
    }

    CBtDevice * pDev =(CBtDevice *)GetSelectedDevice();

    if (pDev == NULL)
        return;

    for (int i = 0; i < 6; i++)
        cmd[commandBytes++] = pDev->m_address[5 - i];

    Log("Sending  AVRC CT Connect Command, BDA: %02x:%02x:%02x:%02x:%02x:%02x",
           cmd[0], cmd[1], cmd[2], cmd[3], cmd[4], cmd[5]);

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_CONNECT, cmd, commandBytes);
}

// Disconnect from peer device
void MainWindow::onCTDisconnect()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if (NULL == pDev)
        return;

    cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
    cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;

    Log("Sending AVRC CT Disconnect Command, Handle: 0x%04x", pDev->m_avrc_handle);
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_DISCONNECT, cmd, commandBytes);

    pDev->m_avrc_handle = NULL_HANDLE;
    pDev->m_conn_type &= ~CONNECTION_TYPE_AVRC;
}

// Send play command
void MainWindow::onCTPlay()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_PLAY, cmd, commandBytes);
}

// Send stop command
void MainWindow::onCTStop()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_STOP, cmd, commandBytes);
}

// Send pause command
void MainWindow::onCTPause()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_PAUSE, cmd, commandBytes);
}

// Send next command
void MainWindow::onCTNext()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_NEXT_TRACK, cmd, commandBytes);
}

// Send previous command
void MainWindow::onCTPrevious()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_PREVIOUS_TRACK, cmd, commandBytes);
}

// Send vol up command
void MainWindow::onCTVolumeUp()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_VOLUME_UP, cmd, commandBytes);
}

// Send vol down command
void MainWindow::onCTVolumeDown()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_VOLUME_DOWN, cmd, commandBytes);
}

// Send mute command
void MainWindow::onCTMute()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    m_volMute = !m_volMute;

    ui->btnCTMute->setIcon(style()->standardIcon(m_volMute ? QStyle::SP_MediaVolumeMuted : QStyle::SP_MediaVolume));
}

// Change repeat option
void MainWindow::onCTRepeatMode(int index)
{
    if (m_CommPort == NULL)
        return;

    if(index == (repeat_ct.curr_value -1))
        return;

    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    QVariant val = ui->cbCTRepeat->itemData(index);

    repeat_ct.curr_value = val.toUInt();
    cmd[commandBytes++] = (BYTE)repeat_ct.curr_value;

    Log("Sending  AVRCP controller Repeat setting change : %d", repeat_ct.curr_value);

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_SET_REPEAT_MODE, cmd, commandBytes);

}

// Change shuffle option
void MainWindow::onCTShuffleMode(int index)
{
    if (m_CommPort == NULL)
        return;

    if(index == (shuffle_ct.curr_value -1))
        return;

    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    QVariant val = ui->cbCTShuffle->itemData(index);
    shuffle_ct.curr_value = val.toUInt();
    cmd[commandBytes++] = (BYTE)shuffle_ct.curr_value;

    Log("Sending AVRCP controller Shuffle setting change  : %d", shuffle_ct.curr_value);
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_SET_SHUFFLE_MODE, cmd, commandBytes);


}

// Change volume
void MainWindow::cbCTVolumeChanged(int index)
{
    if (m_CommPort == NULL)
        return;

    if(m_current_volume_pct == index)
        return;

    m_current_volume_pct = index;

    BYTE    cmd[60];
    int     commandBytes = 0;
    int handle = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
        handle = pDev->m_avrc_handle;

    cmd[commandBytes++] = handle & 0xff;
    cmd[commandBytes++] = (handle >> 8) & 0xff;

    cmd[commandBytes++] = m_current_volume_pct+1;

    Log("Sending Audio volume. Handle: 0x%04x", handle);
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_VOLUME_LEVEL, cmd, commandBytes);

}

// Send skip forward command
void MainWindow::onCTSkipForward()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_BEGIN_FAST_FORWARD, cmd, commandBytes);
    QThread::sleep(2);
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_END_FAST_FORWARD, cmd, commandBytes);
}

// Send skip backward command
void MainWindow::onCTSkipBackward()
{
    BYTE    cmd[60];
    int     commandBytes = 0;

    CBtDevice * pDev =(CBtDevice *)GetConnectedAVRCDevice();
    if(pDev)
    {
        cmd[commandBytes++] = pDev->m_avrc_handle & 0xff;
        cmd[commandBytes++] = (pDev->m_avrc_handle >> 8) & 0xff;
    }

    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_BEGIN_REWIND, cmd, commandBytes);
    QThread::sleep(2);
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_END_REWIND, cmd, commandBytes);
}

// Handle WICED HCI events
void MainWindow::onHandleWicedEventAVRCCT(unsigned int opcode, unsigned char *p_data, unsigned int len)
{
    switch (HCI_CONTROL_GROUP(opcode))
    {
    case HCI_CONTROL_GROUP_DEVICE:
        HandleDeviceEventsAVRCCT(opcode, p_data, len);
        break;

    case HCI_CONTROL_GROUP_AVRC_CONTROLLER:
        HandleAVRCControllerEvents(opcode, p_data, len);
        break;

    }
}

// Handle WICED HCI local device events
void MainWindow::HandleDeviceEventsAVRCCT(DWORD opcode, LPBYTE p_data, DWORD len)
{
    switch (opcode)
    {
        case HCI_CONTROL_EVENT_DEVICE_STARTED:
            break;
    }    
}

// Handle WICED HCI events for AVRC CT, AMS, ANCS
void MainWindow::HandleAVRCControllerEvents(DWORD opcode, BYTE *p_data, DWORD len)
{
    BYTE     bda[6];
    UINT16 handle;
    uint16_t maxStrLen;
    BYTE     status;
    CBtDevice *device;

    switch (opcode)
    {
     // connected with peer
     case HCI_CONTROL_AVRC_CONTROLLER_EVENT_CONNECTED:
    {
        if(len >= 9)
        {
            for (int i = 0; i < 6; i++)
                bda[5 - i] = p_data[i];
            p_data += 6;

            status = *p_data++;

            handle = p_data[0] + (p_data[1] << 8);

            // find device in the list with received address and save the connection handle
            if ((device = FindInList(bda,ui->cbDeviceList)) == NULL)
                device = AddDeviceToList(bda, ui->cbDeviceList, NULL);

            device->m_avrc_handle = handle;
            device->m_conn_type |= CONNECTION_TYPE_AVRC;

            Log("AVRCP Controller connected %02x:%02x:%02x:%02x:%02x:%02x handle %04x",
                bda[0], bda[1], bda[2], bda[3], bda[4], bda[5], handle);
        }
        else
        {
            Log("AVRCP Controller connection failed");
        }
    }

    break;

     // diconnected from peer
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_DISCONNECTED:
    {
        handle = p_data[0] | (p_data[1] << 8);
        Log("AVRCP Controller disconnected handle %d", handle);
        device = FindInList(CONNECTION_TYPE_AVRC, handle, ui->cbDeviceList);
        if (device && (device->m_avrc_handle == handle))
        {
            device->m_avrc_handle = NULL_HANDLE;
            device->m_conn_type &= ~CONNECTION_TYPE_AVRC;
        }
    }
        break;

        // Peer changed player
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_PLAYER_CHANGE:
    {
        char player[100];
        int cnt = (len-2) < 100 ? (len-2) : 100;
        memset(player, 0, 100);
        //snprintf(player, cnt, (char *)&p_data[2]);
        snprintf(player, cnt, "%s", (char *)&p_data[2]);
        Log("HCI_CONTROL_AVRC_CONTROLLER_EVENT_PLAYER_CHANGE %s", player);
    }
        break;

        // Peer indicated its available settings
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_SETTING_AVAILABLE:
        {
        Log("AVRCP Controller Player settings available handle: %04x, settings: 0x%x 0x%x 0x%x 0x%x",
                        p_data[0] + (p_data[1] << 8), p_data[2], p_data[3], p_data[4], p_data[5]);

            BYTE *p_setting = &p_data[1];
            const int MAX_RC_APP_SETTING_VALUE   = 4;

            int k;

            for (int i=1; i<=MAX_RC_APP_SETTING_VALUE; i++)
            {
                if (p_setting[i])
                {
                    int value = (int) p_setting[i];

                    if(i == 2)
                    {
                        ui->cbCTRepeat->clear();
                        k = 1;
                        while(value && k < 5)
                        {
                            value -= 1 << k;
                            ui->cbCTRepeat->addItem(repeatStr[k-1], k);
                            k++;
                        }
                    }

                    if(i == 3)
                    {
                        ui->cbCTShuffle->clear();
                        k = 1;
                        while(value && k < 4)
                        {
                            value -= 1 << k;
                            ui->cbCTShuffle->addItem(shuffleStr[k-1], k);
                            k++;
                        }
                    }
                }
            }
        }

        break;

        // Peer changed settings
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_SETTING_CHANGE:               /* AVRCP Controller Player setting changed */
        Log("AVRCP Controller Player setting change handle: %04x ",
                        p_data[0] + (p_data[1] << 8));

        for (int i = 0, j = 3; i < p_data[2]; i++)
        {
            Log("    attr: %02x setting: %02x ", p_data[j], p_data[j + 1]);

            switch (p_data[j++])
            {
            case 2: // Repeat Mode
                repeat_ct.curr_value = p_data[j++];
                Log("Repeat %s", repeatStr[repeat_ct.curr_value-1]);
                ui->cbCTRepeat->setCurrentIndex(repeat_ct.curr_value-1);
             break;

            case 3: // Shuffle Mode
                shuffle_ct.curr_value = p_data[j++];
                Log("Shuffle %s", shuffleStr[shuffle_ct.curr_value-1]);
                ui->cbCTShuffle->setCurrentIndex(shuffle_ct.curr_value-1);
                break;
            }
        }

        break;

        // Peer changed play status
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_PLAY_STATUS:
        Log("AVRCP Controller Play status handle: %04x status:%d (%s)",
            p_data[0] + (p_data[1] << 8), p_data[2], szAvrcPlayStatus[p_data[2]]);
        break;

        // Peer indicated current playing track info
    case HCI_CONTROL_AVRC_CONTROLLER_EVENT_CURRENT_TRACK_INFO:           /* AVRCP Controller Track Info event */
      {
        Log("AVRCP Controller Track Info handle: %04x status: %d Attr: %d",
            p_data[0] + (p_data[1] << 8), p_data[2], p_data[3]);

        maxStrLen = p_data[4] + (p_data[5] << 8);

        if (maxStrLen > 60)
            maxStrLen = 60;

        QString mediaName;
        QString mediaType;
        QTableWidgetItem *colType;
        QTableWidgetItem *colVal;
        char name[256] = {0};
        memset(name, 0, 256);

        switch (p_data[3])
        {
        case 1: /* Title */
            mediaType = "Title";
            break;
        case 2: /* Artist */
            mediaType = "Artist";
            break;
        case 3: /* Album */
            mediaType = "Album";
            break;
        case 4: /* track number */
            mediaType = "Track Num";
            break;
        case 5: /* number of tracks */
            mediaType = "Total Tracks";
            break;
        case 6: /* Genre */
            mediaType = "Genre";
            break;
        case 7: /* playing time/track duration*/
            mediaType = "Duration (sec)";
            break;
        default:
            /* Unhandled */
            break;
        }

        if(!mediaType.length())
            break;

        if(maxStrLen)
        {
            memcpy(name, &p_data[6],  maxStrLen);
        }
        else
            sprintf(name, "%s", "<not available>");

        Log("%s - %s", mediaType.toLocal8Bit().data(), name);

        mediaName = name;

        colType = new QTableWidgetItem(mediaType);
        colVal = new QTableWidgetItem(mediaName);

        ui->tblMetaPlayerCT->setItem(p_data[3] -1,0, colType);
        ui->tblMetaPlayerCT->setItem(p_data[3] -1,1, colVal);


    }
        break;

    default:
        /* Unhandled */
        break;
    }

}

// Send ANCS action command for acknowledgement
void MainWindow::OnBnClickedAncsPositive()
{
    BYTE command[5] = { 0, 0, 0, 0, 0 };
    command[0] = m_notification_uid & 0xff;
    command[1] = (m_notification_uid >> 8) & 0xff;
    command[2] = (m_notification_uid >> 16) & 0xff;
    command[3] = (m_notification_uid >> 24) & 0xff;
    SendWicedCommand(HCI_CONTROL_ANCS_COMMAND_ACTION, command, 5);
}

// Send ANCS action command for rejection
void MainWindow::OnBnClickedAncsNegative()
{
    BYTE command[5] = { 0, 0, 0, 0, 1 };
    command[0] = m_notification_uid & 0xff;
    command[1] = (m_notification_uid >> 8) & 0xff;
    command[2] = (m_notification_uid >> 16) & 0xff;
    command[3] = (m_notification_uid >> 24) & 0xff;
    SendWicedCommand(HCI_CONTROL_ANCS_COMMAND_ACTION, command, 5);
}

// Simulate button press on stero headphone
// (implementation is embedded application dependent)
void MainWindow::on_btnAVRCTBtnPress_clicked()
{
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_BUTTON_PRESS, NULL, 0);
}

// Simulate long button press on stero headphone (press and hold)
// (implementation is embedded application dependent)
void MainWindow::on_btnAVRCTLongBtnPress_clicked()
{
    SendWicedCommand(HCI_CONTROL_AVRC_CONTROLLER_COMMAND_LONG_BUTTON_PRESS, NULL, 0);
}
