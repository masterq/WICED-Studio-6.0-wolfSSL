/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Provisioning implementation.
 */
#include <stdio.h> 
#include <string.h> 

#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_ovl.h"
#include "core_aes_ccm.h"

#include "mesh_util.h"

#include "ecdh.h"

#include "pb_transport.h"
#include "provisioning_int.h"
#include "low_power.h"
#include "friend.h"
#include "mesh_discovery.h"
#include "foundation_int.h"
#include "key_refresh.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__PROVISIONING_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__PROVISIONING_C
#include "mesh_trace.h"

/* possible state of the connection - values of the BpCb::state */
#define BP_STATE_OPENING                        1
#define BP_STATE_OPENED                         2
// states for the case when we are provisioning node 
#define PB_STATE_PROVISIONING_WAIT_CAPABILITIES 3
#define PB_STATE_PROVISIONING_SENT_CAPABILITIES 4
#define PB_STATE_PROVISIONING_RECEIVED_START    5
#define PB_STATE_PROVISIONING_SENT_PUBLIC_KEY   6   // is not used with OOB public key
#define PB_STATE_PROVISIONING_OOB               7   // is not used if no input OOB and no output OOB
#define PB_STATE_PROVISIONING_WAIT_CONF         8
#define PB_STATE_PROVISIONING_SENT_CONFORMATION 9
#define PB_STATE_PROVISIONING_SENT_RANDOM       10
#define PB_STATE_PROVISIONING_SENT_COMPLETE     11
#define PB_STATE_PROVISIONING_SENT_FAILED       12

// states for the case when we are provisioner 
#define PB_STATE_PROVISIONER_SENT_INVITE        13
#define PB_STATE_PROVISIONER_WAIT_MODE          14
#define PB_STATE_PROVISIONER_SENT_START         15
#define PB_STATE_PROVISIONER_REQUESTED_PUB_KEY  16  // is not used with OOB public key
#define PB_STATE_PROVISIONER_SENT_PUBLIC_KEY    17
#define PB_STATE_PROVISIONER_OOB                18  // is not used if no input OOB and no output OOB and no static OOB
#define PB_STATE_PROVISIONER_WAIT_INPUT_COMPL   19
#define PB_STATE_PROVISIONER_SENT_CONFORMATION  20
#define PB_STATE_PROVISIONER_SENT_RANDOM        21
#define PB_STATE_PROVISIONER_SENT_DATA          22

#define PB_MAX_CONNECTIONS      1

// array of controll blocks of the active connections
PbCb* pb_cb[PB_MAX_CONNECTIONS];
static uint8_t                                          pb_cb_cnt = 0;    // number of items in the array pb_cb

static wiced_bt_mesh_provision_started_cb_t             wiced_bt_mesh_provision_started_cb = NULL;
static wiced_bt_mesh_provision_end_cb_t                 wiced_bt_mesh_provision_end_cb = NULL;
static wiced_bt_mesh_provision_get_oob_cb_t             wiced_bt_mesh_provision_get_oob_cb = NULL;
static wiced_bt_mesh_provision_on_capabilities_cb_t     wiced_bt_mesh_provision_on_capabilities_cb = NULL;
static wiced_bt_mesh_provision_get_capabilities_cb_t    wiced_bt_mesh_provision_get_capabilities_cb = NULL;
static wiced_bt_mesh_provision_gatt_send_cb_t           wiced_bt_mesh_provision_gatt_send_cb = NULL;
static wiced_bt_mesh_provision_end_cb_t                 wiced_bt_mesh_provision_end_hci_cb = NULL;


#ifdef _DEB_USE_STATIC_ALLOC_PROV
static PbCb     s_pb_cb;
#endif

// private and public keys of the node for ACDH authentication
uint8_t                       pb_priv_key[WICED_BT_MESH_PROVISION_PRIV_KEY_LEN];
uint8_t                       pb_public_key[WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN];

// ------------------ static functions
/* Finds connection controll block in the array pb_cb. Returns index of the found control block or -1 if not found. */
static int pb_find_conn_(uint32_t conn_id)
{
    uint8_t i;
    for (i = 0; i < pb_cb_cnt; i++)
    {
        if (conn_id == pb_cb[i]->link_id)
            break;
    }
    return i < pb_cb_cnt ? i : -1;
}

/* Allocates new connection controll block in the array pb_cb. Returns index of the allocated control block or -1 on error. */
static int pb_alloc_conn_(uint32_t conn_id, uint8_t pb_type)
{
    int conn_idx = -1;
    if (pb_cb_cnt < PB_MAX_CONNECTIONS)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV
        if (pb_cb_cnt == 0)
            pb_cb[pb_cb_cnt] = &s_pb_cb;
        else
#endif
        pb_cb[pb_cb_cnt] = (PbCb*)wiced_memory_allocate(sizeof(PbCb));
        if (pb_cb[pb_cb_cnt] != NULL)
        {
            conn_idx = pb_cb_cnt++;
            memset(pb_cb[conn_idx], 0, sizeof(PbCb));
            pb_cb[conn_idx]->link_id = conn_id;
            pb_cb[conn_idx]->pb_type = pb_type;
            
            TRACE1(TRACE_WARNING, "pb_alloc_conn_: id:%x\n", conn_id);
            TRACE3(TRACE_WARNING, " idx:%d type:%d len:%d\n", conn_idx, pb_type, sizeof(PbCb));
        }
        else
        {
            TRACE1(TRACE_WARNING, "pb_alloc_conn_: wiced_memory_allocate failed len:%d\n", sizeof(PbCb));
        }
    }
    else
    {
        TRACE0(TRACE_INFO, "pb_alloc_conn_: no available conn\n");
    }
    return conn_idx;
}

/* Deletes connection controll block in the array pb_cb. */
void pb_del_conn_(uint32_t conn_idx)
{
    TRACE2(TRACE_WARNING, "pb_del_conn_: idx:%d cnt:%d\n", conn_idx, pb_cb_cnt);

    if (conn_idx < pb_cb_cnt)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV
        if (pb_cb_cnt != 1)
#endif
        wiced_memory_free(pb_cb[conn_idx]);
        pb_cb_cnt--;
        while (conn_idx++ < pb_cb_cnt)
        {
            pb_cb[conn_idx - 1] = pb_cb[conn_idx];
        }
    }
}

/**
* Called by app to send provisioning packet on specific link.
*
* Parameters:
*   conn_idx:   Connection index
*   packet:     Packet to send
*   packet_len: Length of the packet to send
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - error
*
*/
static wiced_bool_t wiced_bt_mesh_provision_send_packet_(uint8_t conn_idx, const uint8_t* packet, uint16_t packet_len)
{
    // For GATT send message using proxy fragmentation
    if (pb_cb[conn_idx]->pb_type == PB_TYPE_GATT)
    {
        split_proxy(PROXY_PDU_TYPE_PROVIS, packet, packet_len, wiced_bt_mesh_core_gatt_send_max_len);
        pb_adv_packet_sent(pb_cb[conn_idx]->link_id);
        return WICED_TRUE;
    }
    return pb_adv_send_packet(pb_cb[conn_idx]->link_id, packet, packet_len);
}

static void wiced_bt_mesh_provision_started(uint32_t conn_id)
{
    // stop beaconing if we are not provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
    {
#ifndef MESH_CONTROLLER
        mesh_discovery_stop();
#endif
    }

    if (wiced_bt_mesh_provision_started_cb)
        wiced_bt_mesh_provision_started_cb(conn_id);
}

// assigns additional callback for WICED HCI event
void wiced_bt_mesh_provision_set_end_hci_callback(wiced_bt_mesh_provision_end_cb_t end_hci_callback)
{
    wiced_bt_mesh_provision_end_hci_cb = end_hci_callback;
}


static void wiced_bt_mesh_provision_end(wiced_bool_t provisioner, uint32_t conn_id, uint8_t result)
{
    // If we are not provisioner then start or reset device depending on result
    if (!provisioner)
    {
        if (result == WICED_BT_MESH_PROVISION_RESULT_SUCCESS)
        {
#ifndef MESH_CONTROLLER
            mesh_discovery_stop();
            mesh_start_mesh();
#ifndef _DEB_DONT_REBOOT_AFTER_PROVIS
            // we need to reboot if we are provisioned via GATT
            mesh_core_reboot(1000);
#endif
#endif
        }
        // on error reset node
        else
            wiced_bt_mesh_core_init(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
    }
    // call registered callback function
    if (wiced_bt_mesh_provision_end_cb)
    {
        wiced_bt_mesh_provision_end_cb(conn_id, result);
    }
    if (wiced_bt_mesh_provision_end_hci_cb)
    {
        wiced_bt_mesh_provision_end_hci_cb(conn_id, result);
    }
}

// sends message with conformation
wiced_bool_t pb_provisioner_send_conf_(uint32_t conn_idx)
{
    uint8_t msg[1 + WICED_BT_MESH_PROVISION_CONFORMATION_LEN];
    msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_CONFIRMATION;
    memcpy(&msg[1], pb_cb[conn_idx]->conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
    pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_CONFORMATION;
    return wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
}

static wiced_bool_t pb_provisioner_authenticate_(uint32_t conn_idx, uint32_t conn_id)
{
    wiced_bool_t res = WICED_FALSE;
    // prepare state for the case of using OOB because callback wiced_bt_mesh_provision_get_oob_cb can call wiced_bt_mesh_provision_set_oob()
    // On error caller will delete this control block at all
    pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_OOB;
    // if we configured to use output, input or static OOB then request it from application
    switch (pb_cb[conn_idx]->start.auth_method)
    {
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_OUTPUT:
        if(wiced_bt_mesh_provision_get_oob_cb)
            res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_OUTPUT, pb_cb[conn_idx]->start.auth_size, pb_cb[conn_idx]->start.auth_action);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_INPUT:
        if(wiced_bt_mesh_provision_get_oob_cb)
        res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_INPUT, pb_cb[conn_idx]->start.auth_size, pb_cb[conn_idx]->start.auth_action);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_STATIC:
        if(wiced_bt_mesh_provision_get_oob_cb)
            res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_STATIC, pb_cb[conn_idx]->start.auth_size, 0);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_NO:
        // we configured not to use OOB
        // calculate authentication (session_key and conformation) and send message with conformation
        pb_cb[conn_idx]->oob_value_len = 0;
        pb_calc_authentication(conn_idx, NULL, WICED_TRUE);
        res = pb_provisioner_send_conf_(conn_idx);
        break;
    }
    return res;
}

static wiced_bool_t pb_provisioning_authenticate_(uint32_t conn_idx, uint32_t conn_id)
{
    wiced_bool_t res = WICED_FALSE;
    // prepare state for the case of using OOB because callback wiced_bt_mesh_provision_get_oob_cb can call wiced_bt_mesh_provision_set_oob()
    // On error caller will delete this control block at all
    pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_OOB;
    // if we configured to use output, input or static OOB then request it from application
    switch (pb_cb[conn_idx]->start.auth_method)
    {
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_OUTPUT:
        if(wiced_bt_mesh_provision_get_oob_cb)
            if(wiced_bt_mesh_provision_get_oob_cb)
                res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_OUTPUT, pb_cb[conn_idx]->start.auth_size, pb_cb[conn_idx]->start.auth_action);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_INPUT:
        if(wiced_bt_mesh_provision_get_oob_cb)
            res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_INPUT, pb_cb[conn_idx]->start.auth_size, pb_cb[conn_idx]->start.auth_action);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_STATIC:
        if(wiced_bt_mesh_provision_get_oob_cb)
            res = wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_GET_STATIC, pb_cb[conn_idx]->start.auth_size, 0);
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_NO:
        // we configured to not to use OOB
        // calculate authentication (session_key and conformation) and wait for conformation from the provisioner
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_WAIT_CONF;
        pb_calc_authentication(conn_idx, NULL, WICED_FALSE);
        res = WICED_TRUE;
        break;
    }

    return res;
}

//------------------------------
/**
 * \brief Initializes provisioning layer.
 * \details Called by provisioning and provisioner app to initialize provisioning layer for both PB_ADV and PB_GATT.
 *
 * @param[in]   priv_key                :Private key(32 bytes) to use for ECDH authentication.
 * @param[in]   started_cb              :Callback function to be called on provisioning start
 * @param[in]   end_cb                  :Callback function to be called on provisioning end
 * @param[in]   on_capabilities_cb_t    :Callback function to be called on Capabilities
 * @param[in]   get_capabilities_cb_t   :Callback function to be called on Capabilities get
 * @param[in]   get_oob_cb              :Callback function to be called on OOB get
 * @param[in]   gatt_send_cb            :Callback function to send provisioning packet ofer GATT
 *
 * @return   WICED_TRUE/WICED_FALSE - success/failed.
 */
wiced_bool_t wiced_bt_mesh_provision_init(
    uint8_t                                         *priv_key,
    wiced_bt_mesh_provision_started_cb_t            started_cb,
    wiced_bt_mesh_provision_end_cb_t                end_cb,
    wiced_bt_mesh_provision_on_capabilities_cb_t    on_capabilities_cb,
    wiced_bt_mesh_provision_get_capabilities_cb_t   get_capabilities_cb,
    wiced_bt_mesh_provision_get_oob_cb_t            get_oob_cb,
    wiced_bt_mesh_provision_gatt_send_cb_t          gatt_send_cb)
{
    TRACE2(TRACE_INFO, "wiced_bt_mesh_provision_init: pb_cb_cnt:%d gatt_send_cb:%x priv_key: pub_key:\n", pb_cb_cnt, (uint32_t)gatt_send_cb);

    // delete all allocated control blocks
    while (pb_cb_cnt > 0)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV
        if (pb_cb_cnt == 1)
            pb_cb_cnt--;
        else
#endif
        wiced_memory_free(pb_cb[--pb_cb_cnt]);
    }

    wiced_bt_mesh_provision_started_cb = started_cb;
    wiced_bt_mesh_provision_end_cb = end_cb;
    wiced_bt_mesh_provision_get_oob_cb = get_oob_cb;
    wiced_bt_mesh_provision_on_capabilities_cb = on_capabilities_cb;
    wiced_bt_mesh_provision_get_capabilities_cb = get_capabilities_cb;
    wiced_bt_mesh_provision_gatt_send_cb = gatt_send_cb;

    ecdh_init_curve();

    // remember private_key
    memcpy(pb_priv_key, priv_key, WICED_BT_MESH_PROVISION_PRIV_KEY_LEN);
#ifdef HARDCODED_PRIV_KEY
    {
        uint8_t buf_provisioner[WICED_BT_MESH_PROVISION_PRIV_KEY_LEN] = { HARDCODED_PRIV_KEY_PROVISIONER };
        uint8_t buf_device[WICED_BT_MESH_PROVISION_PRIV_KEY_LEN] = { HARDCODED_PRIV_KEY_DEVICE };
        // if we are not provisioned then use HARDCODED_PRIV_KEY_DEVICE. Otherwise use HARDCODED_PRIV_KEY_PROVISIONER
        if(node_nv_data.node_id == MESH_NODE_ID_INVALID)
            memcpy(pb_priv_key, buf_device, sizeof(buf_device));
        else
            memcpy(pb_priv_key, buf_provisioner, sizeof(buf_provisioner));
    }
#endif
    TRACEN(TRACE_DEBUG, (char*)pb_priv_key, WICED_BT_MESH_PROVISION_PRIV_KEY_LEN);
    // create public_key
    ecdh_create_pub_key(pb_priv_key, pb_public_key);
    TRACEN(TRACE_DEBUG, (char*)pb_public_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
    // initialize PB_ADV transport layer
    pb_adv_init();
    return WICED_TRUE;
}

/**
* Implemented by provisioning layer.
* Called by provisioner app to start provisioning.
*
* Parameters:
*   conn_id:            Connection ID of the provisioning connection
*   node_id:            Node address(ID) to assign to provisioning node
*   uuid:               Node UUID to start provisioning. NULL uuid means use PB_GATT. Otherwise use PB_ADV
*   identify_duration:  Identify Duration to pass in the Invite PDU to the provisioning device
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*/
wiced_bool_t wiced_bt_mesh_provision_start(
    uint32_t                            conn_id,
    uint16_t                            node_id,
    uint8_t                             *uuid,
    uint8_t                             identify_duration)
{
    int conn_idx;
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_start: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " idx:%d node_id:%x [uuid:]\n", conn_idx, node_id);
    if (uuid)
        TRACEN(TRACE_INFO, (char*)uuid, MESH_DEVICE_UUID_LEN);
    // We can be provisioner only if we are provisioned or node_id is unassigned or non-unicast
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID
        || node_id == MESH_NODE_ID_INVALID || (node_id & MESH_NON_UNICAST_MASK) != 0)
        return WICED_FALSE;
    // return WICED_FALSE if connection exists already
    if (conn_idx >= 0)
        return WICED_FALSE;
    // allocate controll block
    conn_idx = pb_alloc_conn_(conn_id, uuid != NULL ? PB_TYPE_ADV : PB_TYPE_GATT);
    if (conn_idx < 0)
        return WICED_FALSE;

    // remember address of the provisioning device
    pb_cb[conn_idx]->node_id = node_id;

    pb_cb[conn_idx]->invite_identify = identify_duration;

    // if it is PB_ADV
    if (pb_cb[conn_idx]->pb_type == PB_TYPE_ADV)
    {
        //send link_open message
        if (!pb_adv_link_open(conn_id, uuid))
        {
            TRACE0(TRACE_INFO, "wiced_bt_mesh_provision_start: pb_adv_link_open failed\n");
            pb_del_conn_(conn_idx);
            //pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
            return WICED_FALSE;
        }

        // set OPENING state
        pb_cb[conn_idx]->state = BP_STATE_OPENING;
    }
    else
    {
        // it is PB_GATT
        uint8_t byte[2] = { WICED_BT_MESH_PROVISION_PDU_TYPE_INVITE, pb_cb[conn_idx]->invite_identify };
        // Initiate provisioning - send Invite packet
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_INVITE;
        if (!wiced_bt_mesh_provision_send_packet_(conn_idx, byte, 2))
        {
            pb_del_conn_(conn_idx);
            return WICED_FALSE;
        }
    }
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_start: exits state:%d\n", pb_cb[conn_idx]->state);
    return WICED_TRUE;
}

/**
 * \brief Stops provisioning.
 * \details This function is called by provisioner app to stop already started provisioning.
 * It doesn't notify application with end callback.
 *
 * @param[in]   conn_id             Connection ID of the provisioning connection
 *
 * @return   WICED_TRUE/WICED_FALSE - success/failed.
 */
wiced_bool_t wiced_bt_mesh_provision_stop(
    uint32_t                            conn_id)
{
    int             conn_idx;
    uint8_t         pb_type;

    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_start: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%d state:%d\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    // ignore if connection doesn't exist
    if (conn_idx < 0)
        return WICED_FALSE;
    // Close connection and delete controll block. Don't notify application because it initiated that action.
    pb_type = pb_cb[conn_idx]->pb_type;
    pb_del_conn_(conn_idx);
    if (pb_type == PB_TYPE_ADV)
        pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    return WICED_TRUE;
}

/**
* Called by transport layer to confirm link establishment for provisioning of the device.
*
* Parameters:
*   conn_id:    Connection ID for provisioning
*
* Return:   WICED_TRUE - succeeded
*           WICED_FALSE - failed
*/
wiced_bool_t pb_adv_link_opened(uint32_t conn_id)
{
    int conn_idx;
    uint8_t byte[2] = { WICED_BT_MESH_PROVISION_PDU_TYPE_INVITE, 0 };
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "pb_adv_link_opened: conn_id:%x\n", conn_id);
    TRACE3(TRACE_INFO, " conn_idx:%d state:%d pb_cb_cnt:%d\n", conn_idx, conn_idx < 0 ? -1 : pb_cb[conn_idx]->state, pb_cb_cnt);
    // if connection doesn't exist then probably we are provisioning node
    if (conn_idx < 0)
    {
        // ignore if we provisioned already
        if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
            return WICED_FALSE;
        // only one connection is allowed
        if (pb_cb_cnt > 0)
        {
            return WICED_FALSE;
        }
        // allocate controll block
        conn_idx = pb_alloc_conn_(conn_id, PB_TYPE_ADV);
        if (conn_idx < 0)
        {
            return WICED_FALSE;
        }
        pb_cb[conn_idx]->state = BP_STATE_OPENED;
        wiced_bt_mesh_provision_started(conn_id);
        return WICED_TRUE;
    }

    byte[1] = pb_cb[conn_idx]->invite_identify;

    // Make sure we are in the OPENING state
    if (pb_cb[conn_idx]->state != BP_STATE_OPENING)
    {
        pb_del_conn_(conn_idx);
        pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        // We are provisioner(WICED_TRUE)
        wiced_bt_mesh_provision_end(WICED_TRUE, conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        return WICED_FALSE;
    }

    // We received OPEN conformation. Then we are initiator of the link establishment and want to start provisioning
    pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_INVITE;
    if (!wiced_bt_mesh_provision_send_packet_(conn_idx, byte, 2))
    {
        pb_del_conn_(conn_idx);
        pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        // We are provisioner(WICED_TRUE)
        wiced_bt_mesh_provision_end(WICED_TRUE, conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        return WICED_FALSE;
    }
    wiced_bt_mesh_provision_started(conn_id);
    return WICED_TRUE;
}

/**
* Called by transport layer to notify link closing.
*
* Parameters:
*   conn_id:    Connection ID for provisioning
*   reason:     Reason of the closing. See spec for possible values.
*
* Return:   None
*
*/
void pb_adv_link_closed(uint32_t conn_id, uint8_t reason)
{
    int conn_idx;
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "pb_adv_link_closed: conn_id:%x\n", conn_id);
    TRACE3(TRACE_INFO, " reason:%d conn_idx:%d state:%x\n", reason, conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    // ignore if connection doesn't exist
    if (conn_idx >= 0)
    {
        wiced_bool_t provisioner = pb_cb[conn_idx]->state >= PB_STATE_PROVISIONER_SENT_INVITE
            || pb_cb[conn_idx]->state == BP_STATE_OPENING;
        // if we sent complete and waiting for ack then we can consider ouself as provisioned
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONING_SENT_COMPLETE)
        {
            // we are done with provisioning.
            // save unicast address received in the PDU_TYPE_DATA in the NVM
            mesh_set_dev_addr(pb_cb[conn_idx]->node_id);
            // initialize Low Power and Friend Features
            if((fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER) != 0)
                low_power_init(NULL);
            if ((fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND) != 0)
                friend_init(NULL);
        }
        pb_del_conn_(conn_idx);
        wiced_bt_mesh_provision_end(provisioner, conn_id, reason);
    }
}

/**
* Called by transport layer on ack from the peer device. On error transport layer will call pb_adv_link_closed()
* but not previous pb_adv_send_packet() and pb_adv_packet_sent().
*
* Parameters:
*   conn_id:    Connection ID for provisioning
*
* Return:   None
*
*/
void pb_adv_packet_sent(uint32_t conn_id)
{
    uint8_t         pb_type;
    int             conn_idx;
    wiced_bool_t    res = WICED_FALSE;

    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "pb_adv_packet_sent: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%d state:%d\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    // ignore if connection doesn't exist
    if (conn_idx < 0)
    {
        return;
    }
    pb_type = pb_cb[conn_idx]->pb_type;

    switch(pb_cb[conn_idx]->state)
    {
    // ------------- for provisioner
    // if it is conformation on message START
    case PB_STATE_PROVISIONER_SENT_START:
        // if we configured to not support OOB public key
        if (pb_cb[conn_idx]->start.public_key_oob_available == WICED_BT_MESH_PROVISION_START_PUB_KEY_NO)
        {
            // Send our public key to provisioner node
            uint8_t msg[1 + WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN];
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_PUBLIC_KEY;
            memcpy(&msg[1], pb_public_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_PUBLIC_KEY;
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, sizeof(msg)))
                break;
            res = WICED_TRUE;
            break;
        }
        // We configured to support OOB public key.
        // ask app(provisioner) to get public key of the provisioning node via OOB
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_REQUESTED_PUB_KEY;
        if (!wiced_bt_mesh_provision_get_oob_cb)
            break;
        if (!wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_PUB_KEY, 0, 0))
            break;
        res = WICED_TRUE;
        break;
    case PB_STATE_PROVISIONER_SENT_PUBLIC_KEY:
        // if we configured  to not support OOB public key
        if (pb_cb[conn_idx]->start.public_key_oob_available == WICED_BT_MESH_PROVISION_START_PUB_KEY_NO)
        {
            // just ignore this conformation. We will proceed on receiving peer public key
            res = WICED_TRUE;
            break;
        }
        // We got peer public key over OOB
        // Start authentication
        res = pb_provisioner_authenticate_(conn_idx, conn_id);
        break;
    // just ignore following conformations with success. We will proceed on receiving message from provisioning node
    case PB_STATE_PROVISIONER_SENT_INVITE:
    case PB_STATE_PROVISIONER_SENT_CONFORMATION:
    case PB_STATE_PROVISIONER_SENT_RANDOM:
    case PB_STATE_PROVISIONER_SENT_DATA:
        res = WICED_TRUE;
        break;
    // ------------- for provisioning node
    case PB_STATE_PROVISIONING_SENT_PUBLIC_KEY:
        // Start authentication
        res = pb_provisioning_authenticate_(conn_idx, conn_id);
        break;
    case PB_STATE_PROVISIONING_SENT_COMPLETE:
        // we are done with provisioning.
        // save unicast address received in the PDU_TYPE_DATA in the NVM
        if (MESH_RESULT_SUCCESS != mesh_set_dev_addr(pb_cb[conn_idx]->node_id))
            break;
        // initialize Low Power and Friend Features
        if ((fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER) != 0)
            low_power_init(NULL);
        if ((fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND) != 0)
            friend_init(NULL);

        pb_del_conn_(conn_idx);
        if (pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_SUCCESS);
        // We are provisioning device(WICED_FALSE)
        wiced_bt_mesh_provision_end(WICED_FALSE, conn_id, WICED_BT_MESH_PROVISION_RESULT_SUCCESS);
        res = WICED_TRUE;
        break;
    // just ignore following conformations with success. We will proceed on receiving message from provisioner
    case PB_STATE_PROVISIONING_SENT_CAPABILITIES:
    case PB_STATE_PROVISIONING_SENT_CONFORMATION:
    case PB_STATE_PROVISIONING_SENT_RANDOM:
        res = WICED_TRUE;
        break;
    }
    // on error or on unexpected conformation close connection, delete controll block and notify application
    if (!res)
    {
        wiced_bool_t provisioner = pb_cb[conn_idx]->state >= PB_STATE_PROVISIONER_SENT_INVITE;
        pb_del_conn_(conn_idx);
        if(pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        wiced_bt_mesh_provision_end(provisioner, conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    }
}

static wiced_bool_t check_capabilities(const wiced_bt_mesh_provision_capabilities_t* cap)
{
    // check capabilities
    if (cap->elements_num == 0)
        return WICED_FALSE;
    
    if (BE2TOUINT16(cap->algorithms) != WICED_BT_MESH_PROVISION_ALG_FIPS_P256_ELLIPTIC_CURVE)
        return WICED_FALSE;

    if ((cap->pub_key_type & WICED_BT_MESH_PROVISION_CAPS_PUB_KEY_TYPE_PROHIBITED) != 0)
        return WICED_FALSE;
    
    if ((cap->static_oob_type & WICED_BT_MESH_PROVISION_CAPS_STATIC_OOB_TYPE_PROHIBITED) != 0)
        return WICED_FALSE;

    if (cap->output_oob_size > WICED_BT_MESH_PROVISION_OUT_OOB_MAX_SIZE)
        return WICED_FALSE;

    if (cap->input_oob_size > WICED_BT_MESH_PROVISION_IN_OOB_MAX_SIZE)
        return WICED_FALSE;

    return WICED_TRUE;
}

static wiced_bool_t check_start(const wiced_bt_mesh_provision_capabilities_t* cap, const wiced_bt_mesh_provision_start_t* start)
{
    int oob_cnt = 0;
    if (start->algorithm == WICED_BT_MESH_PROVISION_START_ALG_FIPS_P256)
    {
        if (BE2TOUINT16(cap->algorithms) != WICED_BT_MESH_PROVISION_ALG_FIPS_P256_ELLIPTIC_CURVE)
            return WICED_FALSE;
    }
    else
        return WICED_FALSE;
    
    if (start->public_key_oob_available == WICED_BT_MESH_PROVISION_START_PUB_KEY_USED)
    {
        if (cap->pub_key_type != WICED_BT_MESH_PROVISION_CAPS_PUB_KEY_TYPE_AVAILABLE)
            return WICED_FALSE;
    }
    else if (start->public_key_oob_available != WICED_BT_MESH_PROVISION_START_PUB_KEY_NO)
        return WICED_FALSE;

    switch (start->auth_method)
    {
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_NO:
        if (start->auth_action != 0 || start->auth_size != 0)
            return WICED_FALSE;
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_STATIC:
        if (start->auth_action != 0 || start->auth_size != 0
            || (cap->static_oob_type & WICED_BT_MESH_PROVISION_CAPS_STATIC_OOB_TYPE_AVAILABLE) == 0)
                return WICED_FALSE;
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_OUTPUT:
        if (start->auth_size == 0 || start->auth_size > WICED_BT_MESH_PROVISION_OUT_OOB_MAX_SIZE
            || (((uint16_t)1 << start->auth_action) & BE2TOUINT16(cap->output_oob_action)) == 0
            || start->auth_size > cap->output_oob_size)
                return WICED_FALSE;
        break;
    case WICED_BT_MESH_PROVISION_START_AUTH_METHOD_INPUT:
        if (start->auth_size == 0 || start->auth_size > WICED_BT_MESH_PROVISION_IN_OOB_MAX_SIZE
            || (((uint16_t)1 << start->auth_action) & BE2TOUINT16(cap->input_oob_action)) == 0
            || start->auth_size > cap->input_oob_size)
                return WICED_FALSE;
        break;
    default:
        return WICED_FALSE;
    }
    return WICED_TRUE;
}

/**
* Called by transport layer on receiving and assembling of the packet from the provisioning device.
*
* Parameters:
*   conn_id:    Connection ID for provisioning
*   packet:     Received packet
*   packet_len  Length of the received packet
*
* Return:   None
*
*/
void pb_transport_received_packet(uint32_t conn_id, const uint8_t* packet, uint16_t packet_len)
{
    uint8_t         pb_type;
    wiced_bool_t    bRes = WICED_FALSE;
    uint8_t         failed_result = WICED_BT_MESH_PROVISION_RESULT_FAILED;
    int             conn_idx;
    uint8_t         msg[2 + sizeof(wiced_bt_mesh_provision_capabilities_t)+sizeof(wiced_bt_mesh_provision_start_t)+(WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN * 2)];
    conn_idx = pb_find_conn_(conn_id);

    TRACE1(TRACE_INFO, "pb_transport_received_packet: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%x state:%x\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    TRACEN(TRACE_INFO, (char*)packet, packet_len);
    // ignore if connection doesn't exist
    if (conn_idx < 0 || packet_len == 0)
    {
        return;
    }

    pb_type = pb_cb[conn_idx]->pb_type;

    // first byte is message type
    packet_len--;
    switch (*packet++)
    {
    // ------------ for provisioner
    case WICED_BT_MESH_PROVISION_PDU_TYPE_CAPABILITIES:
        if (packet_len != sizeof(wiced_bt_mesh_provision_capabilities_t)
            || pb_cb[conn_idx]->state != PB_STATE_PROVISIONER_SENT_INVITE)
            break;
        // check capabilities
        if (!check_capabilities((wiced_bt_mesh_provision_capabilities_t*)packet))
            break;
        // remember capabilities
        memcpy(&pb_cb[conn_idx]->capabilities, packet, sizeof(wiced_bt_mesh_provision_capabilities_t));
        // request start parameters and peer public key from application.
        // Set state before callback because callback can call wiced_bt_mesh_provision_set_mode()
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_WAIT_MODE;
        if (!wiced_bt_mesh_provision_on_capabilities_cb)
            break;
        if (!wiced_bt_mesh_provision_on_capabilities_cb(conn_id, (wiced_bt_mesh_provision_capabilities_t*)packet))
            break;
        bRes = WICED_TRUE;
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_PUBLIC_KEY:
        // public_key(64bytes)
        if (packet_len != WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN)
            break;
        // if we are provisioner
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONER_SENT_PUBLIC_KEY)
        {
            // We don't have peer public key over OOB and received it
            memcpy(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, packet, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
            // Start authentication
            bRes = pb_provisioner_authenticate_(conn_idx, conn_id);
            break;
        }
        // if we are provisioning node
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONING_RECEIVED_START)
        {
            // remember peer public key 
            memcpy(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, packet, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
            // if we configured not to use OOB public key then send our public key to provisioner
            if (pb_cb[conn_idx]->start.public_key_oob_available == WICED_BT_MESH_PROVISION_START_PUB_KEY_NO)
            {
                msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_PUBLIC_KEY;
                memcpy(&msg[1], pb_public_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
                pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_PUBLIC_KEY;
                if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN))
                    break;
                bRes = WICED_TRUE;
                break;
            }
            // we are configured to use OOB public key. Start authentication
            bRes = pb_provisioning_authenticate_(conn_idx, conn_id);
            break;
        }
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_INPUT_COMPLETE:
        // Input Complete doesn't have data
        if (packet_len != 0 || pb_cb[conn_idx]->state != PB_STATE_PROVISIONER_WAIT_INPUT_COMPL)
            break;
        // stop displaying input OOB
        if (!wiced_bt_mesh_provision_get_oob_cb)
            break;
        if (!wiced_bt_mesh_provision_get_oob_cb(conn_id, WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_STOP, 0, 0))
            break;
        // calculate authentication (session_key and conformation) and send message with conformation
        pb_calc_authentication(conn_idx, NULL, WICED_TRUE);
        bRes = pb_provisioner_send_conf_(conn_idx);
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_CONFIRMATION:
        // conformation(16bytes)
        if (packet_len != WICED_BT_MESH_PROVISION_CONFORMATION_LEN)
            break;
        // if we are provisioner then remember conformation and send random
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONER_SENT_CONFORMATION)
        {
            memcpy(pb_cb[conn_idx]->conformation, packet, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_RANDOM;
            memcpy(&msg[1], pb_cb[conn_idx]->random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_RANDOM;
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_RANDOM_LEN))
                break;
            bRes = WICED_TRUE;
            break;
        }
        // we are provisioning node
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONING_WAIT_CONF)
        {
            // Send Conformation
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_CONFIRMATION;
            memcpy(&msg[1], pb_cb[conn_idx]->conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
            // remember received conformation after taking our conformation nad before wiced_bt_mesh_provision_send_packet_
            // because wiced_bt_mesh_provision_send_packet_ will use the same buffer as receiver and it will corrupt packet
            memcpy(pb_cb[conn_idx]->conformation, packet, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_CONFORMATION;
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_CONFORMATION_LEN))
                break;
            bRes = WICED_TRUE;
            break;
        }
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_RANDOM:
        // random(16bytes)
        if (packet_len != WICED_BT_MESH_PROVISION_RANDOM_LEN)
            break;
        // if we are provisioner
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONER_SENT_RANDOM)
        {
            // check device conformation- calculate device conformation and compare it with received conformation
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_DATA;
            if (!pb_check_conformation(conn_idx, packet, WICED_TRUE))
                break;
            // we passed authentication.
            // create message with provisioning data
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_DATA;
            // send data
            if (!pb_create_provisioning_data(conn_idx, &msg[1]))
                break;
#ifdef MESH_CONTROLLER
            // In windows controller it is good place to calculate and save DevKey
            //calculate DevKey = k1(ECDHSecret, ProvisioningSalt, �prdk�)
            mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prdk", 4,
                &msg[1 + WICED_BT_MESH_PROVISION_DATA_LEN + WICED_BT_MESH_PROVISION_MIC_LEN]);
            // save it in the NVM
            if (MESH_RESULT_SUCCESS != mesh_set_dev_key(&msg[1 + WICED_BT_MESH_PROVISION_DATA_LEN + WICED_BT_MESH_PROVISION_MIC_LEN]))
                break;
#endif
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_DATA_LEN + WICED_BT_MESH_PROVISION_MIC_LEN))
                break;
            bRes = WICED_TRUE;
            break;
        }
        // we are provisioning node
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONING_SENT_CONFORMATION)
        {
            // check provisioner conformation- calculate provisioner conformation and compare it with received conformation
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_RANDOM;
            if (!pb_check_conformation(conn_idx, packet, WICED_FALSE))
                break;
            // send random
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_RANDOM;
            memcpy(&msg[1], pb_cb[conn_idx]->random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + WICED_BT_MESH_PROVISION_RANDOM_LEN))
                break;
            bRes = WICED_TRUE;
            break;
        }
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_COMPLETE:
        // Complete doesn't have data
        if (packet_len != 0 || pb_cb[conn_idx]->state != PB_STATE_PROVISIONER_SENT_DATA)
            break;
        // we are done.
        // Close connection, delete controll block and notify application
        pb_del_conn_(conn_idx);
        if (pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_SUCCESS);
        // That PDU can be received only by provisioner(WICED_TRUE)
        wiced_bt_mesh_provision_end(WICED_TRUE, conn_id, WICED_BT_MESH_PROVISION_RESULT_SUCCESS);
        bRes = WICED_TRUE;
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_FAILED:
        // it can be received at any state on provisioner
        if (packet_len != 1 || pb_cb[conn_idx]->state < PB_STATE_PROVISIONER_SENT_INVITE)
            break;
        // notify app about failed message using 0x80 offset
        failed_result = *packet | 0x80;
        break;
    // --------------------- for provisioning node
    case WICED_BT_MESH_PROVISION_PDU_TYPE_INVITE:
        // Invite doesn't have data
        if (packet_len != 1 || pb_cb[conn_idx]->state != BP_STATE_OPENED)
            break;
        pb_cb[conn_idx]->invite_identify = *packet;
        // request capabilities from the app. Set state before call because wiced_bt_mesh_provision_get_capabilities_cb_t can be called from inside of wiced_bt_mesh_provision_get_capabilities
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_WAIT_CAPABILITIES;
        if (!wiced_bt_mesh_provision_get_capabilities_cb)
            break;
        if (!wiced_bt_mesh_provision_get_capabilities_cb(conn_id))
            break;
        bRes = WICED_TRUE;
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_START:
        if (packet_len != sizeof(wiced_bt_mesh_provision_start_t) || pb_cb[conn_idx]->state != PB_STATE_PROVISIONING_SENT_CAPABILITIES)
            break;
        // check if received Start params fits in our capabilities
        if (!check_start(&pb_cb[conn_idx]->capabilities, (wiced_bt_mesh_provision_start_t*)packet))
            break;
        // remember Start parameters
        memcpy(&pb_cb[conn_idx]->start, packet, sizeof(wiced_bt_mesh_provision_start_t));
        // we don't need to send any messgae here. We have to receive public key
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_RECEIVED_START;
        bRes = WICED_TRUE;
        break;
    case WICED_BT_MESH_PROVISION_PDU_TYPE_DATA:
        // net_key(16bytes) || NetKeyIdx(2bytes) || <KR, IV Update(1 byte)> ||IVindex(4bytes) || unicast_address(2bytes)
        if (packet_len != (WICED_BT_MESH_PROVISION_DATA_LEN + WICED_BT_MESH_PROVISION_MIC_LEN)
            || pb_cb[conn_idx]->state != PB_STATE_PROVISIONING_SENT_RANDOM)
                break;
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_COMPLETE;
        // create The nonce of session = 13 LSB octets of Nonce = k1(ECDHSecret, ProvisioningSalt, �prsn�)
        mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prsn", 4, msg);
        // decrypt and authenticate data: encData, MICdata = AES-CCM (SessionKey, Nonce, Data)
        if (!ccm_crypt(WICED_FALSE, pb_cb[conn_idx]->session_key, &msg[MESH_KEY_LEN - MESH_NONCE_LEN],
            (uint8_t*)packet, WICED_BT_MESH_PROVISION_DATA_LEN, WICED_BT_MESH_PROVISION_MIC_LEN, NULL))
        {
            break;
        }
        // make sure the received address is valid unicast address
        pb_cb[conn_idx]->node_id = BE2TOUINT16(&packet[MESH_KEY_LEN + 2 + 1 + MESH_IV_INDEX_LEN]);
        if (pb_cb[conn_idx]->node_id == MESH_NODE_ID_INVALID || (pb_cb[conn_idx]->node_id & MESH_NON_UNICAST_MASK) != 0)
            break;
        //calculate DevKey = k1(ECDHSecret, ProvisioningSalt, �prdk�)
        mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prdk", 4, msg);
        //TRACE0(TRACE_CRITICAL, "dev_key:\n");
#if (TRACE_COMPILE_LEVEL <= 3)
        wiced_printf(NULL, 0, "dev_key:****************************\n\n");
#endif
        TRACEN(TRACE_CRITICAL, (char*)msg, MESH_KEY_LEN);
        // save it in the NVM
        if (MESH_RESULT_SUCCESS != mesh_set_dev_key(msg))
            break;
        // save net key and other related values in the NVM
        if (MESH_RESULT_SUCCESS != mesh_set_net_key(0, &packet[0], BE2TOUINT16(&packet[MESH_KEY_LEN])))
            break;
        // save IV index in the NVM
        if (MESH_RESULT_SUCCESS != mesh_set_iv_index(&packet[MESH_KEY_LEN + 2 + 1]))
            break;
#ifndef MESH_CONTROLLER
        // if IV-UPDATE flag is set device to the transition starte to the next iv_index
        if (packet[MESH_KEY_LEN + 2] & WICED_BT_MESH_PROVISION_FLAG_IV_UPDATE)
            iv_updt_set_active_state();
#endif
        // if KR flag is set then set net_key to the KR phase 2
        if (packet[MESH_KEY_LEN + 2] & WICED_BT_MESH_PROVISION_FLAG_KR)
            key_refresh_set_phase2(BE2TOUINT16(&packet[MESH_KEY_LEN]));
#ifndef MESH_CONTROLLER
        app_cache_clear(WICED_TRUE);
        app_cache_clear(WICED_FALSE);
#endif
        // we passed all provionning steps. Send Complete packet
        msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_COMPLETE;
        if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1))
            break;
        bRes = WICED_TRUE;
        break;
    default:
        // it is invalid PDU on provisioning device side then send failed message
        if (pb_cb[conn_idx]->state >= PB_STATE_PROVISIONING_WAIT_CAPABILITIES
            && pb_cb[conn_idx]->state <= PB_STATE_PROVISIONING_SENT_COMPLETE)
        {
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_FAILED;
            msg[1] = WICED_BT_MESH_PROVISION_FAILED_PDU_ERROR_INVALID_PDU;
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_FAILED;
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 2))
                break;
            bRes = WICED_TRUE;
        }
        break;
    }
    // it is error on provisioning device side then send failed message
    if (!bRes && pb_cb[conn_idx]->state >= PB_STATE_PROVISIONING_WAIT_CAPABILITIES
        && pb_cb[conn_idx]->state <= PB_STATE_PROVISIONING_SENT_COMPLETE)
    {
        msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_FAILED;
        msg[1] = WICED_BT_MESH_PROVISION_FAILED_PDU_ERROR_WRONG_FORMAT;
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_FAILED;
        bRes = wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 2);
    }

    // on error close connection, delete controll block and notify application
    if (!bRes)
    {
        wiced_bool_t provisioner = pb_cb[conn_idx]->state >= PB_STATE_PROVISIONER_SENT_INVITE;
        pb_del_conn_(conn_idx);
        if (pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        wiced_bt_mesh_provision_end(provisioner, conn_id, failed_result);
    }
}

/**
* Implemented by provisioner layer.
* Provisioner app should call this function with decision how to do provisioning
* on base of the capabilities of provisioning node received on wiced_bt_mesh_provision_on_capabilities_cb.
*
* Parameters:
*   conn_id:        Connection ID of the provisioning connection
*   start:          Application decision how to do provisioning
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*/
wiced_bool_t wiced_bt_mesh_provision_set_mode(
    uint32_t                                conn_id,
    const wiced_bt_mesh_provision_start_t   *start)
{
    wiced_bool_t bRes = WICED_FALSE;
    int conn_idx;
    uint8_t msg[1 + sizeof(wiced_bt_mesh_provision_start_t)];
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_set_mode: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%d state:%d start:\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    TRACEN(TRACE_INFO, (char*)start, sizeof(wiced_bt_mesh_provision_start_t));
    // ignore if connection doesn't exist
    if (conn_idx < 0)
    {
        return WICED_FALSE;
    }
    do
    {
        if (pb_cb[conn_idx]->state != PB_STATE_PROVISIONER_WAIT_MODE)
            break;
        // check if Start params fits in capabilities
        if (!check_start(&pb_cb[conn_idx]->capabilities, start))
            break;
        // remember mode
        memcpy(&pb_cb[conn_idx]->start, start, sizeof(wiced_bt_mesh_provision_start_t));
        // create responce packet
        msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_START;
        memcpy(&msg[1], &pb_cb[conn_idx]->start, sizeof(wiced_bt_mesh_provision_start_t));
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_START;
        if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + sizeof(wiced_bt_mesh_provision_start_t)))
            break;
        bRes = WICED_TRUE;
        break;
    } while (WICED_FALSE);
    // on error close connection, delete controll block and notify application
    if (!bRes)
    {
        uint8_t     pb_type = pb_cb[conn_idx]->pb_type;
        wiced_bool_t provisioner = pb_cb[conn_idx]->state >= PB_STATE_PROVISIONER_SENT_INVITE;
        pb_del_conn_(conn_idx);
        if(pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        wiced_bt_mesh_provision_end(provisioner, conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    }
    return bRes;
}


/**
* Implemented by provisioning layer.
* Called by provisioning app set capabilities of provisioning node.
*
* Parameters:
*   conn_id:        Connection ID of the provisioning connection
*   capabilities:   Capabilities of the provisioning node
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*/
wiced_bool_t wiced_bt_mesh_provision_set_capabilities(
    uint32_t                              conn_id,
    const wiced_bt_mesh_provision_capabilities_t   *capabilities)
{
    wiced_bool_t bRes = WICED_FALSE;
    int conn_idx;
    uint8_t msg[1 + sizeof(wiced_bt_mesh_provision_capabilities_t)];
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_set_capabilities: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%d state:%d capabilities:\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    TRACEN(TRACE_INFO, (char*)capabilities, sizeof(wiced_bt_mesh_provision_capabilities_t));
    // ignore if connection doesn't exist
    if (conn_idx < 0)
    {
        return WICED_FALSE;
    }
    do
    {
        if (pb_cb[conn_idx]->state != PB_STATE_PROVISIONING_WAIT_CAPABILITIES)
            break;
        // check capabilities
        if (!check_capabilities(capabilities))
            break;
        memcpy(&pb_cb[conn_idx]->capabilities, capabilities, sizeof(wiced_bt_mesh_provision_capabilities_t));
        // create responce packet
        msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_CAPABILITIES;
        memcpy(&msg[1], capabilities, sizeof(wiced_bt_mesh_provision_capabilities_t));
        pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_SENT_CAPABILITIES;
        if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1 + sizeof(wiced_bt_mesh_provision_capabilities_t)))
            break;
        bRes = WICED_TRUE;
    } while (WICED_FALSE);
    // on error close connection, delete controll block and notify application
    if (!bRes)
    {
        uint8_t     pb_type = pb_cb[conn_idx]->pb_type;
        wiced_bool_t provisioner = pb_cb[conn_idx]->state >= PB_STATE_PROVISIONER_SENT_INVITE;
        pb_del_conn_(conn_idx);
        if (pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
        wiced_bt_mesh_provision_end(provisioner, conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    }
    return bRes;
}


/**
* Implemented by provisioning layer.
* Called by provisioner and provisioning application as a responce on wiced_bt_mesh_provision_get_oob_cb
*
* Parameters:
*   conn_id:    Connection ID of the provisioning connection
*   value:      Requested OOB value
*   value_len:  Length of the requested OOB value
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*/
wiced_bool_t wiced_bt_mesh_provision_set_oob(
    uint32_t  conn_id,
    uint8_t   *value,
    uint8_t   value_len)
{
    wiced_bool_t bRes = WICED_FALSE;
    int conn_idx;
    uint8_t msg[1 + WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN];
    conn_idx = pb_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_set_oob: conn_id:%x\n", conn_id);
    TRACE2(TRACE_INFO, " conn_idx:%d state:%d\n", conn_idx, conn_idx >= 0 ? pb_cb[conn_idx]->state : -1);
    TRACEN(TRACE_INFO, (char*)value, value_len);
    // ignore if connection doesn't exist or value is empty
    if (conn_idx < 0 || value_len == 0 || value == NULL)
        return WICED_FALSE;
    do
    {
        if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONER_REQUESTED_PUB_KEY)
        {
            if (value_len != WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN)
                break;
            // save peer public key for future
            memcpy(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, value, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
            // Send our public key to the provisioning node
            msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_PUBLIC_KEY;
            memcpy(&msg[1], pb_public_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
            pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_SENT_PUBLIC_KEY;
            if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, sizeof(msg)))
                break;
            bRes = WICED_TRUE;
        }
        // if provisioner is waiting for requested OOB value
        else if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONER_OOB)
        {
            // for output and static OOB calculate and send conformation
            if ((pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_OUTPUT
                && value_len <= pb_cb[conn_idx]->start.auth_size)
                || (pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_STATIC && value_len <= WICED_BT_MESH_PROVISION_STATIC_OOB_MAX_SIZE))
            {
                // remember OOB value. We will need it to check received conformation
                memcpy(pb_cb[conn_idx]->oob_value, value, value_len);
                pb_cb[conn_idx]->oob_value_len = value_len;
                // calculate authentication (session_key and conformation) and send message with conformation
                pb_calc_authentication(conn_idx, NULL, WICED_TRUE);
                bRes = pb_provisioner_send_conf_(conn_idx);
            }
            // for input OOB case just save OOB value and wait for Input complete
            else if (pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_INPUT
                && value_len <= pb_cb[conn_idx]->start.auth_size)
            {
                memcpy(pb_cb[conn_idx]->oob_value, value, value_len);
                pb_cb[conn_idx]->oob_value_len = value_len;
                pb_cb[conn_idx]->state = PB_STATE_PROVISIONER_WAIT_INPUT_COMPL;
                bRes = WICED_TRUE;
            }
        }
        // if provisioning node is waiting for requested OOB value
        else if (pb_cb[conn_idx]->state == PB_STATE_PROVISIONING_OOB)
        {
            // for output and static OOB
            if (pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_OUTPUT
                && value_len <= pb_cb[conn_idx]->start.auth_size
                || (pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_STATIC && value_len <= WICED_BT_MESH_PROVISION_STATIC_OOB_MAX_SIZE))
            {
                // remember OOB value
                memcpy(pb_cb[conn_idx]->oob_value, value, value_len);
                pb_cb[conn_idx]->oob_value_len = value_len;
                // calculate authentication (session_key and conformation) and wait for conformation from the provisioner
                pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_WAIT_CONF;
                pb_calc_authentication(conn_idx, NULL, WICED_FALSE);
                bRes = WICED_TRUE;
            }
            // for input OOB case
            else if (pb_cb[conn_idx]->start.auth_method == WICED_BT_MESH_PROVISION_START_AUTH_METHOD_INPUT
                && value_len <= pb_cb[conn_idx]->start.auth_size)
            {
                // remember OOB value
                memcpy(pb_cb[conn_idx]->oob_value, value, value_len);
                pb_cb[conn_idx]->oob_value_len = value_len;
                // send Input complete and wait for conformation from the provisioner
                msg[0] = WICED_BT_MESH_PROVISION_PDU_TYPE_INPUT_COMPLETE;
                pb_cb[conn_idx]->state = PB_STATE_PROVISIONING_WAIT_CONF;
                // calculate authentication (session_key and conformation)
                pb_calc_authentication(conn_idx, NULL, WICED_FALSE);
                if (!wiced_bt_mesh_provision_send_packet_(conn_idx, msg, 1))
                    break;
                bRes = WICED_TRUE;
            }
        }
    } while (WICED_FALSE);
    // on error close connection and delete controll block. Don't notify application because we return WICED_FALSE
    if (!bRes)
    {
        uint8_t     pb_type = pb_cb[conn_idx]->pb_type;
        pb_del_conn_(conn_idx);
        if (pb_type == PB_TYPE_ADV)
            pb_adv_link_close(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    }
    return bRes;
}

/**
* \brief Processes received PB_GATT packets
*
* @param[in]   conn_id             :Connection ID. It is needed in case of few simultaneous provisioning via GATT.
* @param[in]   packet              :Packet to process.
* @param[in]   packet_len          :Length of the packet to process
*
* @return      None
*
*/
void wiced_bt_mesh_provision_gatt_packet(uint32_t conn_id, const uint8_t *packet, uint8_t packet_len)
{
    int conn_idx;
    // assemble packet from the fragments
    if (MESH_HANDLE_PROV_RES_SUCCEEDED != handle_proxy_frag(packet, packet_len))
        return;
    // ignore wrong PDU type
    if (ctx.proxy_data.type != PROXY_PDU_TYPE_PROVIS)
        return;
    // check if connection exists already
    conn_idx = pb_find_conn_(conn_id);

    TRACE2(TRACE_INFO, "wiced_bt_mesh_provision_gatt_packet: conn_id:%x conn_idx:%x\n", conn_id, conn_idx);
    TRACE3(TRACE_INFO, " conn_idx:%d state:%d pb_cb_cnt:%d\n", conn_idx, conn_idx < 0 ? -1 : pb_cb[conn_idx]->state, pb_cb_cnt);

    // if it is first packet on that connection
    if (conn_idx < 0)
    {
        // ignore it if it is not invite (first packet from provisioner)
        if (ctx.proxy_data.len != 2 || ctx.proxy_data.data[0] != WICED_BT_MESH_PROVISION_PDU_TYPE_INVITE)
            return;
        // ignore if we are provisioned already
        if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
            return;
        // only one connection is allowed
        if (pb_cb_cnt > 0)
            return;
        // allocate controll block
        conn_idx = pb_alloc_conn_(conn_id, PB_TYPE_GATT);
        if (conn_idx < 0)
            return;
        // update state and notify app about provisioning start
        pb_cb[conn_idx]->state = BP_STATE_OPENED;
        wiced_bt_mesh_provision_started(conn_id);
    }
    // now we can handle provisioning PDU
    pb_transport_received_packet(conn_id, ctx.proxy_data.data, ctx.proxy_data.len);
}

// callback function for split_proxy in wiced_bt_mesh_provision_send_packet_ to send packet via GATT
void pb_transport_gatt_split_cb(const uint8_t* hdr, uint32_t _hdr_len, const uint8_t* data, uint32_t data_len)
{
    uint8_t buf[MESH_MAX_GATT_NOTIFICATION_LEN]; //length should be bigger then possible packet lenght
    if (_hdr_len)
        memcpy(buf, hdr, _hdr_len);
    if (data_len)
        memcpy(&buf[_hdr_len], data, data_len);
    if (wiced_bt_mesh_provision_gatt_send_cb)
        wiced_bt_mesh_provision_gatt_send_cb(buf, _hdr_len + data_len);
}

