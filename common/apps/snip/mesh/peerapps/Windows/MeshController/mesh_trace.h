/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
*
* Mesh trace definitions
*
*/
#ifndef __MESH_TRACE_H__
#define __MESH_TRACE_H__

int wiced_printf(char * buffer, int len, char * fmt_str, ...);
void ble_trace0(const char *p_str);
void ble_trace1(const char *fmt_str, UINT32 p1);
void ble_trace2(const char *fmt_str, UINT32 p1, UINT32 p2);
void ble_trace3(const char *fmt_str, UINT32 p1, UINT32 p2, UINT32 p3);
void ble_trace4(const char *fmt_str, UINT32 p1, UINT32 p2, UINT32 p3, UINT32 p4);
void ble_tracen(const char *p_str, UINT32 len);

// get rid of TRACE... definitions in afx.h
#undef TRACE0
#undef TRACE1
#undef TRACE2
#undef TRACE3

#define TRACE0(l, s)                    TRACE0_##l(s)
#define TRACE1(l, s, p1)                TRACE1_##l(s, p1)
#define TRACE2(l, s, p1, p2)            TRACE2_##l(s, p1, p2)
#define TRACE3(l, s, p1, p2, p3)        TRACE3_##l(s, p1, p2, p3)
#define TRACE4(l, s, p1, p2, p3, p4)    TRACE4_##l(s, p1, p2, p3, p4)
#define TRACEN(l, p, len)               TRACEN_##l(p, len)

#if (TRACE_COMPILE_LEVEL == 0)
#define TRACE0_TRACE_DEBUG(s)                    ble_trace0(s)
#define TRACE1_TRACE_DEBUG(s, p1)                ble_trace1(s, p1)
#define TRACE2_TRACE_DEBUG(s, p1, p2)            ble_trace2(s, p1, p2)
#define TRACE3_TRACE_DEBUG(s, p1, p2, p3)        ble_trace3(s, p1, p2, p3)
#define TRACE4_TRACE_DEBUG(s, p1, p2, p3, p4)    ble_trace4(s, p1, p2, p3, p4)
#define TRACEN_TRACE_DEBUG(p, len)               ble_tracen(p, len)
#else
#define TRACE0_TRACE_DEBUG(s)
#define TRACE1_TRACE_DEBUG(s, p1)
#define TRACE2_TRACE_DEBUG(s, p1, p2)
#define TRACE3_TRACE_DEBUG(s, p1, p2, p3)
#define TRACE4_TRACE_DEBUG(s, p1, p2, p3, p4)
#define TRACEN_TRACE_DEBUG(p, len)
#endif

#if (TRACE_COMPILE_LEVEL <= 1)
#define TRACE0_TRACE_INFO(s)                     ble_trace0(s)
#define TRACE1_TRACE_INFO(s, p1)                 ble_trace1(s, p1)
#define TRACE2_TRACE_INFO(s, p1, p2)             ble_trace2(s, p1, p2)
#define TRACE3_TRACE_INFO(s, p1, p2, p3)         ble_trace3(s, p1, p2, p3)
#define TRACE4_TRACE_INFO(s, p1, p2, p3, p4)     ble_trace4(s, p1, p2, p3, p4)
#define TRACEN_TRACE_INFO(p, len)                ble_tracen(p, len)
#else
#define TRACE0_TRACE_INFO(s)
#define TRACE1_TRACE_INFO(s, p1)
#define TRACE2_TRACE_INFO(s, p1, p2)
#define TRACE3_TRACE_INFO(s, p1, p2, p3)
#define TRACE4_TRACE_INFO(s, p1, p2, p3, p4)
#define TRACEN_TRACE_INFO(p, len)
#endif

#if (TRACE_COMPILE_LEVEL <= 2)
#define TRACE0_TRACE_WARNING(s)                  ble_trace0(s)
#define TRACE1_TRACE_WARNING(s, p1)              ble_trace1(s, p1)
#define TRACE2_TRACE_WARNING(s, p1, p2)          ble_trace2(s, p1, p2)
#define TRACE3_TRACE_WARNING(s, p1, p2, p3)      ble_trace3(s, p1, p2, p3)
#define TRACE4_TRACE_WARNING(s, p1, p2, p3, p4)  ble_trace4(s, p1, p2, p3, p4)
#define TRACEN_TRACE_WARNING(p, len)             ble_tracen(p, len)
#else
#define TRACE0_TRACE_WARNING(s)
#define TRACE1_TRACE_WARNING(s, p1)
#define TRACE2_TRACE_WARNING(s, p1, p2)
#define TRACE3_TRACE_WARNING(s, p1, p2, p3)
#define TRACE4_TRACE_WARNING(s, p1, p2, p3, p4)
#define TRACEN_TRACE_WARNING(p, len)
#endif

#if (TRACE_COMPILE_LEVEL <= 3)
#define TRACE0_TRACE_CRITICAL(s)                 ble_trace0(s)
#define TRACE1_TRACE_CRITICAL(s, p1)             ble_trace1(s, p1)
#define TRACE2_TRACE_CRITICAL(s, p1, p2)         ble_trace2(s, p1, p2)
#define TRACE3_TRACE_CRITICAL(s, p1, p2, p3)     ble_trace3(s, p1, p2, p3)
#define TRACE4_TRACE_CRITICAL(s, p1, p2, p3, p4) ble_trace4(s, p1, p2, p3, p4)
#define TRACEN_TRACE_CRITICAL(p, len)            ble_tracen(p, len)
#else
#define TRACE0_TRACE_CRITICAL(s)
#define TRACE1_TRACE_CRITICAL(s, p1)
#define TRACE2_TRACE_CRITICAL(s, p1, p2)
#define TRACE3_TRACE_CRITICAL(s, p1, p2, p3)
#define TRACE4_TRACE_CRITICAL(s, p1, p2, p3, p4)
#define TRACEN_TRACE_CRITICAL(p, len)
#endif

#endif /* __MESH_TRACE_H__ */
