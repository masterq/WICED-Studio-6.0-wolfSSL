#pragma once
#include "stdafx.h"
#include "BtInterface.h"
#include "MeshController.h"

extern "C"
{
#include "core_aes_ccm.h"
}

BOOL CBtInterface::SendWsUpgradeCommand(BTW_GATT_VALUE *pValue)
{
    ods("+%S\n", __FUNCTIONW__);
    pValue->len = mesh_ota_fw_upgrade_crypt(WICED_TRUE, pValue->value, pValue->len, pValue->value);
    if (pValue->len == 0)
        return FALSE;
    return WriteCharacteristic(&guidSvcWSUpgrade, &guidCharWSUpgradeControlPoint, FALSE, pValue);
}

BOOL CBtInterface::SendWsUpgradeCommand(BYTE Command)
{
    BTW_GATT_VALUE value = { 1, Command };
    return SendWsUpgradeCommand(&value);
}

BOOL CBtInterface::SendWsUpgradeCommand(BYTE Command, USHORT sParam)
{
    BTW_GATT_VALUE value = { 3, Command, (BYTE)(sParam & 0xff), (BYTE)((sParam >> 8) & 0xff) };
    return SendWsUpgradeCommand(&value);
}

BOOL CBtInterface::SendWsUpgradeCommand(BYTE Command, ULONG lParam)
{
    BTW_GATT_VALUE value = { 5, Command, lParam & 0xff, (lParam >> 8) & 0xff, (lParam >> 16) & 0xff, (lParam >> 24) & 0xff };
    return SendWsUpgradeCommand(&value);
}

BOOL CBtInterface::SendWsUpgradeData(BYTE *Data, DWORD len)
{
    ods("+%S\n", __FUNCTIONW__);
    BTW_GATT_VALUE value;

    if (len > (sizeof(value.value) - 4))
    {
        ods("-%S data too long\n", __FUNCTIONW__);
        return (FALSE);
    }

    value.len = mesh_ota_fw_upgrade_crypt(WICED_TRUE, Data, (uint16_t)len, value.value);
    if (value.len == 0)
        return FALSE;

    return WriteCharacteristic(&guidSvcWSUpgrade, &guidCharWSUpgradeData, FALSE, &value);
}

