
// MeshControllerDlg.cpp : implementation file
//

#include "stdafx.h"
#include <setupapi.h>
#include "MeshController.h"
#include "MeshControllerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

GUID GUID_BLUETOOTHLE_DEVICE_INTERFACE = {0x781aee18, 0x7733, 0x4ce4, {0xad, 0xd0, 0x91, 0xf4, 0x1c, 0x67, 0xb5, 0x92}};

HANDLE OpenBleDevice(BLUETOOTH_ADDRESS *btha)
{
    HDEVINFO                            hardwareDeviceInfo;
    SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
    ULONG                               predictedLength = 0;
    ULONG                               requiredLength = 0, bytes=0;
	WCHAR								szBda[13] = {0};
	HANDLE								hDevice = INVALID_HANDLE_VALUE;
	DWORD								err;

    if ((hardwareDeviceInfo = SetupDiGetClassDevs ((LPGUID)&GUID_BLUETOOTHLE_DEVICE_INTERFACE,
			NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE))) == INVALID_HANDLE_VALUE)
    {
        ods("SetupDiGetClassDevs failed: %x\n", GetLastError());
        return hDevice;
    }

    deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

	// Enumerate devices of LE_DEVICE interface class
    for (int i = 0; ; i++) 
	{
        if (!SetupDiEnumDeviceInterfaces (hardwareDeviceInfo, 0, 
				(LPGUID)&GUID_BLUETOOTHLE_DEVICE_INTERFACE, i, &deviceInterfaceData)) 
		{
			if ((err = GetLastError()) == ERROR_NO_MORE_ITEMS) 
				ods ("OpenBleDevice device not found\n");
			else
				ods ("OpenBleDevice:ERROR SetupDiEnumDeviceInterfaces failed:%d\n", err);
			break;
		}
        SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfo, &deviceInterfaceData,
				NULL, 0, &requiredLength, NULL);

		if ((err = GetLastError()) != ERROR_INSUFFICIENT_BUFFER) 
		{
            ods("OpenBleDevice:ERROR SetupDiGetDeviceInterfaceDetail failed %d\n", err);
            break;
        }

        predictedLength = requiredLength;

        deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA) new CHAR[predictedLength + 2];
        if (deviceInterfaceDetailData == NULL) 
		{
            ods("OpenBleDevice:ERROR Couldn't allocate %d bytes for device interface details.\n", predictedLength);
			break;
        }
		RtlZeroMemory (deviceInterfaceDetailData, predictedLength + 2);
        deviceInterfaceDetailData->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
        if (!SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfo, &deviceInterfaceData,
			deviceInterfaceDetailData, predictedLength, &requiredLength, NULL)) 
		{
            ods("OpenBleDevice:ERROR SetupDiGetDeviceInterfaceDetail\n");
			delete deviceInterfaceDetailData;
			break;
        }

		_wcsupr_s (deviceInterfaceDetailData->DevicePath, wcslen(deviceInterfaceDetailData->DevicePath) + 1);
		BdaToString (szBda, btha);
		if (wcsstr (deviceInterfaceDetailData->DevicePath, szBda) != NULL)
		{
			ods("Opening interface:%S\n", deviceInterfaceDetailData->DevicePath);

			hDevice = CreateFile ( deviceInterfaceDetailData->DevicePath,
                GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

			delete deviceInterfaceDetailData;
			break;
		}
		delete deviceInterfaceDetailData;
	}
    SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
	return hDevice;
}

HANDLE OpenBleService(BLUETOOTH_ADDRESS *btha, GUID *guid)
{
    HDEVINFO                            hardwareDeviceInfo;
    SP_DEVICE_INTERFACE_DATA            deviceInterfaceData;
    PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
    ULONG                               predictedLength = 0;
    ULONG                               requiredLength = 0, bytes=0;
	WCHAR								szBda[13] = {0};
	HANDLE								hService = INVALID_HANDLE_VALUE;
	DWORD								err;

    if ((hardwareDeviceInfo = SetupDiGetClassDevs ((LPGUID)guid,
			NULL, NULL, (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE))) == INVALID_HANDLE_VALUE)
    {
        ods("OpenBleServiceSetupDiGetClassDevs failed: %x\n", GetLastError());
        return hService;
    }

    deviceInterfaceData.cbSize = sizeof (SP_DEVICE_INTERFACE_DATA);

	// Enumerate devices of LE_DEVICE interface class
    for (int i = 0; ; i++) 
	{
        if (!SetupDiEnumDeviceInterfaces (hardwareDeviceInfo, 0, guid, i, &deviceInterfaceData)) 
		{
			if ((err = GetLastError()) == ERROR_NO_MORE_ITEMS) 
				ods("OpenBleService device not found\n");
			else
				ods ("OpenBleService:ERROR SetupDiEnumDeviceInterfaces failed:%d\n", err);
			break;
		}
        SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfo, &deviceInterfaceData,
				NULL, 0, &requiredLength, NULL);

		if ((err = GetLastError()) != ERROR_INSUFFICIENT_BUFFER) 
		{
            ods("OpenBleService:ERROR SetupDiGetDeviceInterfaceDetail failed %d\n", err);
            break;
        }

        predictedLength = requiredLength;

        deviceInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA) new CHAR[predictedLength + 2];
        if (deviceInterfaceDetailData == NULL) 
		{
            ods("OpenBleService:ERROR Couldn't allocate %d bytes for device interface details.\n", predictedLength);
			break;
        }
		RtlZeroMemory (deviceInterfaceDetailData, predictedLength + 2);
        deviceInterfaceDetailData->cbSize = sizeof (SP_DEVICE_INTERFACE_DETAIL_DATA);
        if (!SetupDiGetDeviceInterfaceDetail (hardwareDeviceInfo, &deviceInterfaceData,
			deviceInterfaceDetailData, predictedLength, &requiredLength, NULL)) 
		{
            ods("OpenBleService :ERROR SetupDiGetDeviceInterfaceDetail\n");
			delete deviceInterfaceDetailData;
			break;
        }

		_wcsupr_s (deviceInterfaceDetailData->DevicePath, wcslen(deviceInterfaceDetailData->DevicePath) + 1);
		BdaToString (szBda, btha);
		if (wcsstr (deviceInterfaceDetailData->DevicePath, szBda) != NULL)
		{
			ods("OpenBleService Opening interface:%S\n", deviceInterfaceDetailData->DevicePath);

			hService = CreateFile ( deviceInterfaceDetailData->DevicePath,
                GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);

			if(hService == INVALID_HANDLE_VALUE)
				ods("OpenBleService (hService == INVALID_HANDLE_VALUE) GetLastError() = %d\n", GetLastError());

			delete deviceInterfaceDetailData;
			break;
		}
		delete deviceInterfaceDetailData;
	}
    SetupDiDestroyDeviceInfoList (hardwareDeviceInfo);
	return hService;
}

VOID CBtWin8Interface::PostNotification(BTW_GATT_VALUE *pValue, BLUETOOTH_GATT_VALUE_CHANGED_EVENT *pEvent, PVOID EventOutParameter, PVOID Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;

    for (int i = 0; i < num_chars; i++)
    {
        if (m_pchar[i].AttributeHandle == pEvent->ChangedAttributeHandle)
        {
            USHORT inx = 0;
            for (int j = 0; j < i; j++)
            {
                if (IsBthLEUuidMatch(m_pchar[i].CharacteristicUuid, m_pchar[j].CharacteristicUuid))
                {
                    inx++;
                }
            }
            ods("PostNotification: CharacteristicUuid:%x\n", m_pchar[i].CharacteristicUuid.Value.ShortUuid);
            pDlg->PostMessage(WM_USER + (m_pchar[i].CharacteristicUuid.Value.ShortUuid & 0xff), (WPARAM)inx, (LPARAM)pValue);
        }
    }
}

VOID WINAPI NotificationCallback(BTH_LE_GATT_EVENT_TYPE EventType, PVOID EventOutParameter, PVOID Context)
{
    ods ("EventType:%d\n", EventType);

    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    CBtWin8Interface *pBtInterface = (CBtWin8Interface *)(pDlg->m_btInterface);

    BTW_GATT_VALUE *p = (BTW_GATT_VALUE *)malloc (sizeof (BTW_GATT_VALUE));
    if (!p)
        return;

    BLUETOOTH_GATT_VALUE_CHANGED_EVENT *pEvent = (BLUETOOTH_GATT_VALUE_CHANGED_EVENT *)EventOutParameter;
    p->len = (USHORT)pEvent->CharacteristicValue->DataSize;
    memcpy (p->value, pEvent->CharacteristicValue->Data, pEvent->CharacteristicValue->DataSize);

    pBtInterface->PostNotification(p, pEvent, EventOutParameter, Context);
}


CBtWin8Interface::CBtWin8Interface (BLUETOOTH_ADDRESS *bth, HMODULE hLib, LPVOID NotificationContext) 
    : CBtInterface(bth, hLib, NotificationContext, TRUE) 
    , m_hDevice(INVALID_HANDLE_VALUE)
    , m_pEventHandle(INVALID_HANDLE_VALUE)
    , m_pEventHandle2(INVALID_HANDLE_VALUE)
    , m_pEventHandle3(INVALID_HANDLE_VALUE)
    , m_pEventHandle4(INVALID_HANDLE_VALUE)
{
}

BOOL CBtWin8Interface::FindChar(const GUID *p_guid, PBTH_LE_GATT_CHARACTERISTIC pOutChar)
{
    for (int i = 0; i < num_chars; i++)
    {
        _BTH_LE_UUID leuuid;
        leuuid.IsShortUuid = FALSE;
        leuuid.Value.LongUuid = *p_guid;
        if (IsBthLEUuidMatch(m_pchar[i].CharacteristicUuid, leuuid))
        {
            *pOutChar = m_pchar[i];
            return TRUE;
        }
    }
    return FALSE;
}

BOOL CBtWin8Interface::Init()
{
    BTH_LE_GATT_SERVICE service = { 0 }, service2 = { 0 }, service3 = { 0 }, service4 = { 0 };

    if ((m_hDevice = OpenBleDevice(&m_bth)) == INVALID_HANDLE_VALUE)
        return FALSE;

    num_chars = 0;

	USHORT num = 0, num2 = 0, num3 = 0, num4 = 0;
	HRESULT hr;

    FP_BluetoothGATTGetServices pBluetoothGATTGetServices = (FP_BluetoothGATTGetServices)GetProcAddress(m_hLib, "BluetoothGATTGetServices");
	hr = pBluetoothGATTGetServices(m_hDevice, 0, NULL, &num, BLUETOOTH_GATT_FLAG_NONE);
    if (num == 0)
        return FALSE;

    PBTH_LE_GATT_SERVICE pServices = (PBTH_LE_GATT_SERVICE) malloc(sizeof(BTH_LE_GATT_SERVICE) * num);
    if (pServices == NULL) 
        return FALSE;

    RtlZeroMemory(pServices, sizeof(BTH_LE_GATT_SERVICE) * num);
    
    hr = pBluetoothGATTGetServices(m_hDevice, num, pServices, &num, BLUETOOTH_GATT_FLAG_NONE);
    if (S_OK != hr) 
    {
        ods("BluetoothGATTGetServices - Actual Data %x", hr);
        return FALSE;
    }

    // search for Automation service.
    _BTH_LE_UUID leuuid;
    leuuid.IsShortUuid = TRUE;
    leuuid.Value.ShortUuid = WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING;
    _BTH_LE_UUID leuuid2;
    leuuid2.IsShortUuid = TRUE;
    leuuid2.Value.ShortUuid = WICED_BT_MESH_CORE_UUID_SERVICE_PROXY;
    _BTH_LE_UUID leuuid3;
    leuuid3.IsShortUuid = TRUE;
    leuuid3.Value.ShortUuid = WICED_BT_MESH_CORE_UUID_SERVICE_COMMAND;
    _BTH_LE_UUID leuuid4;
    leuuid4.IsShortUuid = FALSE;
    leuuid4.Value.LongUuid = guidSvcWSUpgrade;

    for (int i = 0; i < num; i++)
    {
        if (IsBthLEUuidMatch(pServices[i].ServiceUuid, leuuid))
            service = pServices[i];
        if (IsBthLEUuidMatch(pServices[i].ServiceUuid, leuuid2))
            service2 = pServices[i];
        if (IsBthLEUuidMatch(pServices[i].ServiceUuid, leuuid3))
            service3 = pServices[i];
        if (IsBthLEUuidMatch(pServices[i].ServiceUuid, leuuid4))
            service4 = pServices[i];
    }

    FP_BluetoothGATTGetCharacteristics pBluetoothGATTGetCharacteristics = (FP_BluetoothGATTGetCharacteristics)GetProcAddress(m_hLib, "BluetoothGATTGetCharacteristics");
    FP_BluetoothGATTGetDescriptors pBluetoothGATTGetDescriptors = (FP_BluetoothGATTGetDescriptors)GetProcAddress(m_hLib, "BluetoothGATTGetDescriptors");

    // Go through all characteristics of the Location services to find Config and Notify
    num = 0;
    if(service.ServiceUuid.Value.ShortUuid)
	    pBluetoothGATTGetCharacteristics(m_hDevice, &service, 0, NULL, &num, BLUETOOTH_GATT_FLAG_NONE);
    num2 = 0;
    if (service2.ServiceUuid.Value.ShortUuid)
        pBluetoothGATTGetCharacteristics(m_hDevice, &service2, 0, NULL, &num2, BLUETOOTH_GATT_FLAG_NONE);
    num3 = 0;
    if (service3.ServiceUuid.Value.ShortUuid)
        pBluetoothGATTGetCharacteristics(m_hDevice, &service3, 0, NULL, &num3, BLUETOOTH_GATT_FLAG_NONE);
    num4 = 0;
    if (service4.ServiceUuid.Value.ShortUuid)
        pBluetoothGATTGetCharacteristics(m_hDevice, &service4, 0, NULL, &num4, BLUETOOTH_GATT_FLAG_NONE);
    if (num != 0 || num2 != 0 || num3 != 0 || num4 != 0)
	{
        if ((m_pchar = new BTH_LE_GATT_CHARACTERISTIC[num + num2 + num3 + num4]) == NULL)
            return FALSE;

        if ((num != 0 && pBluetoothGATTGetCharacteristics(m_hDevice, &service, num, m_pchar, &num, BLUETOOTH_GATT_FLAG_NONE) != S_OK)
            || (num2 != 0 && pBluetoothGATTGetCharacteristics(m_hDevice, &service2, num2, &m_pchar[num], &num2, BLUETOOTH_GATT_FLAG_NONE) != S_OK)
            || (num3 != 0 && pBluetoothGATTGetCharacteristics(m_hDevice, &service3, num3, &m_pchar[num + num2], &num3, BLUETOOTH_GATT_FLAG_NONE) != S_OK)
            || (num4 != 0 && pBluetoothGATTGetCharacteristics(m_hDevice, &service4, num4, &m_pchar[num + num2 + num3], &num4, BLUETOOTH_GATT_FLAG_NONE) != S_OK))
        {
            ods("LoadCharacteristics hr:0x%x\n", hr);
		    delete [] m_pchar;
            m_pchar = NULL;
            return FALSE;
	    }
    }
    num_chars = num + num2 + num3 + num4;
    pnum_descr = new USHORT[num_chars];
    m_ppdescr = new PBTH_LE_GATT_DESCRIPTOR[num_chars];

    for (int i = 0; i < num_chars; i++)
    {
        pBluetoothGATTGetDescriptors(m_hDevice, &m_pchar[i], 0, NULL, &pnum_descr[i], BLUETOOTH_GATT_FLAG_NONE);
	    if (pnum_descr[i] != 0)
	    {
	        if ((m_ppdescr[i] = new BTH_LE_GATT_DESCRIPTOR[pnum_descr[i]]) == NULL)
		        break;

            if ((pBluetoothGATTGetDescriptors(m_hDevice, &m_pchar[i], pnum_descr[i], m_ppdescr[i], &pnum_descr[i], BLUETOOTH_GATT_FLAG_NONE)) != S_OK)
	        {
                ods("LoadDescriptors\n");
		        delete [] m_ppdescr[i];
		        break;
	        }
        }
    }
    free (pServices);
    return TRUE;
}

BOOL CBtWin8Interface::RegisterNotification(const GUID *p_guidServ, const GUID *p_guidChar)
{
    BTH_LE_GATT_CHARACTERISTIC bleChar;
    HRESULT hr = 0;

    BLUETOOTH_GATT_EVENT_HANDLE *pEventHandle = NULL;;
    if (*p_guidServ == guidSvcMeshProvisioning)
        pEventHandle = &m_pEventHandle;
    else if (*p_guidServ == guidSvcMeshProxy)
        pEventHandle = &m_pEventHandle2;
    else if (*p_guidServ == guidSvcMeshCommand)
        pEventHandle = &m_pEventHandle3;
    else if (*p_guidServ == guidSvcWSUpgrade)
        pEventHandle = &m_pEventHandle4;

    if(pEventHandle == NULL || *pEventHandle != INVALID_HANDLE_VALUE)
    {
        ods("BluetoothGATTRegisterEvent already registered\n");
        return TRUE;
    }

    if (!FindChar(p_guidChar, &bleChar))
        return FALSE;

    // register to receive notification
    HANDLE hService = OpenBleService(&m_bth, (GUID*)p_guidServ);
    if (hService == INVALID_HANDLE_VALUE)
        return FALSE;

    PBLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION p;
    if ((p = (PBLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION)malloc (sizeof (BLUETOOTH_GATT_VALUE_CHANGED_EVENT_REGISTRATION) + sizeof (BTH_LE_GATT_CHARACTERISTIC))) == NULL)
        return FALSE;

    p->NumCharacteristics = 1;
    p->Characteristics[0] = bleChar; 

    FP_BluetoothGATTRegisterEvent pReg = (FP_BluetoothGATTRegisterEvent)GetProcAddress(m_hLib, "BluetoothGATTRegisterEvent");
    if (pReg)
    {
        hr = pReg(hService, CharacteristicValueChangedEvent, p, &NotificationCallback, m_NotificationContext, pEventHandle, BLUETOOTH_GATT_FLAG_NONE);
        ods ("BluetoothGATTRegisterEvent hr:0x%x\n", hr);
    }

    free (p);
    CloseHandle(hService);
    return (hr == 0);
}

PBTH_LE_GATT_DESCRIPTOR CBtWin8Interface::FindDescr(const GUID *p_guidChar, USHORT uuidDescr)
{
    for (int i = 0; i < num_chars; i++)
    {
        _BTH_LE_UUID leuuid;
        leuuid.IsShortUuid = FALSE;
        leuuid.Value.LongUuid = *p_guidChar;
        if (IsBthLEUuidMatch(m_pchar[i].CharacteristicUuid, leuuid))
        {
            for (int j = 0; j < pnum_descr[i]; j++)
            {
                _BTH_LE_UUID descuuid;
                descuuid.IsShortUuid = TRUE;
                descuuid.Value.ShortUuid = uuidDescr;
                   
                if (IsBthLEUuidMatch(m_ppdescr[i][j].DescriptorUuid, descuuid))
                {
                    return &m_ppdescr[i][j];
                }
            }
        }
    }
    return NULL;
}

BOOL CBtWin8Interface::SetDescriptorValue(const GUID *p_guidServ, const GUID *p_guidChar, USHORT uuidDescr, BTW_GATT_VALUE *pValue)
{
    PBTH_LE_GATT_DESCRIPTOR pDescr = FindDescr(p_guidChar, uuidDescr);
    if (pDescr == NULL)
    {
        ods ("SetDescriptorValue bad UUID:0x%x\n", p_guidChar->Data1);
        return FALSE;
    }
    HRESULT hr = E_FAIL;
    HANDLE hService = OpenBleService(&m_bth, (GUID*)p_guidServ);
    if (hService == INVALID_HANDLE_VALUE)
        return FALSE;

    FP_BluetoothGATTSetDescriptorValue pSetDescr = (FP_BluetoothGATTSetDescriptorValue)GetProcAddress(m_hLib, "BluetoothGATTSetDescriptorValue");
    if (pSetDescr)
    {
        BTH_LE_GATT_DESCRIPTOR_VALUE DescriptorValue;

        RtlZeroMemory(&DescriptorValue, sizeof(DescriptorValue));

        if (uuidDescr == BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG)
        {
            DescriptorValue.DescriptorType = ClientCharacteristicConfiguration;
            DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToNotification = ((pValue->value[0] & 1) != 0);
            DescriptorValue.ClientCharacteristicConfiguration.IsSubscribeToIndication   = ((pValue->value[0] & 2) != 0);
        }
        else
        {
            ASSERT (FALSE);
        }
        HRESULT hr = pSetDescr(hService, pDescr, &DescriptorValue, BLUETOOTH_GATT_FLAG_NONE);
        ods ("BluetoothGATTSetDescriptorValue hr:0x%x\n", hr);
    }
    CloseHandle(hService);
    return hr == S_OK;
}

BOOL CBtWin8Interface::WriteCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar, BOOL without_resp, BTW_GATT_VALUE *pValue)
{
    BTH_LE_GATT_CHARACTERISTIC bleChar;

    if (!FindChar(p_guidChar, &bleChar))
        return FALSE;

    FP_BluetoothGATTSetCharacteristicValue pWriteChar = (FP_BluetoothGATTSetCharacteristicValue)GetProcAddress(m_hLib, "BluetoothGATTSetCharacteristicValue");
    if (pWriteChar == NULL)
        return FALSE;

    HANDLE hService = OpenBleService(&m_bth, (GUID*)p_guidServ);
    if (hService == INVALID_HANDLE_VALUE)
        return FALSE;

    ULONG size = sizeof(BTH_LE_GATT_CHARACTERISTIC_VALUE) + pValue->len - 1;
    LPBYTE buf = new BYTE[size];
    PBTH_LE_GATT_CHARACTERISTIC_VALUE Value = (PBTH_LE_GATT_CHARACTERISTIC_VALUE)buf;

    Value->DataSize = pValue->len;
    memcpy (Value->Data, pValue->value, pValue->len);

    HRESULT hr = pWriteChar(hService, &bleChar, Value, NULL, without_resp ? BLUETOOTH_GATT_FLAG_WRITE_WITHOUT_RESPONSE : 0);
    delete[] buf;

    CloseHandle (hService);
    return hr == S_OK;
}

CBtWin8Interface::~CBtWin8Interface()
{
    FP_BluetoothGATTUnregisterEvent pDereg = (FP_BluetoothGATTUnregisterEvent)GetProcAddress(m_hLib, "BluetoothGATTUnregisterEvent");
    if (pDereg)
    {
        if (m_pEventHandle != INVALID_HANDLE_VALUE)
            pDereg(m_pEventHandle, BLUETOOTH_GATT_FLAG_NONE);
        if (m_pEventHandle2 != INVALID_HANDLE_VALUE)
            pDereg(m_pEventHandle2, BLUETOOTH_GATT_FLAG_NONE);
        if (m_pEventHandle3 != INVALID_HANDLE_VALUE)
            pDereg(m_pEventHandle3, BLUETOOTH_GATT_FLAG_NONE);
        if (m_pEventHandle4 != INVALID_HANDLE_VALUE)
            pDereg(m_pEventHandle4, BLUETOOTH_GATT_FLAG_NONE);
    }
    if (m_hDevice != INVALID_HANDLE_VALUE)
        CloseHandle (m_hDevice);

    for (int i = 0; i < num_chars; i++)
        if (pnum_descr[i] != 0)
            delete[] m_ppdescr[i];
    delete[]pnum_descr;
    delete[]m_ppdescr;
    delete[]m_pchar;
}

