/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Foundation Model implementation
 *
*/
#include <string.h>
#include <stdio.h>
#include "platform.h"

#include "mesh_core.h"
#include "mesh_util.h"
#include "foundation_int.h"
#include "foundation.h"
#include "access_layer.h"
#include "core_aes_ccm.h"
#include "mesh_util.h"
#include "key_refresh.h"
#include "health.h"
#include "wiced_bt_mesh_model_defs.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__FOUNDATION_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__FOUNDATION_C
#include "mesh_trace.h"

wiced_bt_mesh_core_received_msg_callback_t  foundation_received_msg_callback = NULL;
// friend_idx for the received message
int     foundation_rcv_friend_idx = -2;

// Returns WICED_FALSE if it has been stopped by callback function by return FND_SCAN_MODELS_CB_NEXT_STOP
wiced_bool_t foundation_scan_models_(foundation_scan_models_cb_ cb, void* context)
{
    tFND_NVM_ELEMENT    *p_nvm_elem;
    uint8_t             idx_element, idx_model;
    int                 cb_ret;

    TRACE1(TRACE_DEBUG, "foundation_scan_models_: elements_num:%x\n", fnd_static_config_.elements_num);

    if (fnd_config_ == NULL)
        return WICED_FALSE;

    // go through all elements
    for (idx_element = 0, p_nvm_elem = fnd_config_->elements;   // first element
        idx_element < fnd_static_config_.elements_num;                // till end
        idx_element++, p_nvm_elem = (tFND_NVM_ELEMENT*)((uint8_t*)&p_nvm_elem->models[0] + p_nvm_elem->models_num * sizeof(tFND_NVM_MODEL)))  // go to the next element
    {
        TRACE2(TRACE_DEBUG, "  idx_element:%x p_nvm_elem->models_num:%x\n", idx_element, p_nvm_elem->models_num);
        // go through all models of the current element
        for (idx_model = 0; idx_model < p_nvm_elem->models_num; idx_model++)
        {
            TRACE2(TRACE_DEBUG, "   company_id:%x model_id:%x\n", p_nvm_elem->models[idx_model].company_id, p_nvm_elem->models[idx_model].model_id);
            cb_ret = cb(p_nvm_elem, &p_nvm_elem->models[idx_model], context);
            if (cb_ret != FND_SCAN_MODELS_CB_NEXT_MODEL)
                break;
        }
        if (cb_ret == FND_SCAN_MODELS_CB_NEXT_STOP)
            break;
    }

    TRACE1(TRACE_INFO, "foundation_scan_models_: returns cb_ret:%d\n", cb_ret);
    return cb_ret != FND_SCAN_MODELS_CB_NEXT_STOP;
}

/**
* Assign Company ID, Vendor ID and Product ID to the mesh node
* creates configuration block, saves it in the NVM and initializes all models.
* Application should call it on startup in non-provisioned mode
*
* Parameters:
* @param[in]   config           :Configuration data
* @param[in]   callback        :Callback function to be called by the Core at received message subscribed by any Model
* @param[in]   fault_test_cb   :Callback function to be called to invoke a self test procedure of an Element
* @param[in]   attention_cb    :Callback function to be called to attract human attention
*
* Return:   WICED_TRUE - succeeded. WICED_FALSE - failed;
*/
wiced_bool_t foundation_init(wiced_bt_mesh_core_config_t *config, wiced_bt_mesh_core_received_msg_callback_t callback,
    wiced_bt_mesh_core_health_fault_test_cb_t fault_test_cb, wiced_bt_mesh_core_attention_cb_t attention_cb)
{
    wiced_bool_t        res;
    tFND_INIT_MODEL     models[32];
    uint8_t             idx, models_num;

    TRACE1(TRACE_DEBUG, "foundation_init: config:%x\n", (uint32_t)config);

    foundation_received_msg_callback = callback;

    if (config == NULL)
        return WICED_FALSE;

    // Try to read configuration from NVM
    fnd_config_len_ = foundation_get_config_size_(config);
    TRACE1(TRACE_INFO, "foundation_init: fnd_config_len_:%d\n", fnd_config_len_);
    fnd_config_ = (tFND_NVM_CONFIG*)wiced_memory_permanent_allocate(fnd_config_len_);

    res = foundation_init_int_(config);

#ifndef MESH_CONTROLLER
    if(res)
        res = foundation_read_config_();

    // On error reset configuration
    if (!res)
    {
        if(foundation_init_int_(config))
            res = foundation_write_config_();
    }
#endif
    if (res)
    {
        // Get all models. It also checks if eny element contains two same Models.
        models_num = foundation_get_init_models_(models, sizeof(models) / sizeof(models[0]));
        for (idx = 0; idx < models_num; idx++)
        {
            if (models[idx].model_id == WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV)
            {
                // Make sure there is only one Model and it is in the Primary Element
                if (models[idx].elements_num != 1 || models[idx].elements_idx[0] != 0)
                {
                    TRACE0(TRACE_WARNING, "foundation_init: CONFIG_SRV is wrong\n");
                    res = WICED_FALSE;
                    break;
                }
                // we don't need to initialize Configuration Model. It is initialized in that call already
                continue;
            }
        }
    }

    if (res)
    {
        // Initialize heartbeat publications
        foundation_heartbeat_publication_init_();
    }

    health_init(fault_test_cb, attention_cb);

    TRACE1(TRACE_DEBUG, "foundation_init: returns %d\n", res);
    return res;
}

typedef struct
{
    uint16_t                        company_id;
    uint16_t                        model_id;
    uint16_t                        dst;
    uint16_t                        src;
    uint8_t                         app_key_idx;
    uint8_t                         ttl;
    uint16_t                        opcode;
    const uint8_t                   *params;
    uint16_t                        params_len;
    wiced_bt_mesh_event_t           *p_event;
} tFND_GET_MODELS_CB_DATA;

static int foundation_handle_cb_(tFND_NVM_ELEMENT *p_elem, tFND_NVM_MODEL *p_model, tFND_GET_MODELS_CB_DATA* p_data)
{
    // Skip Configuration Server Model
    if(p_model->company_id == 0
        && (p_model->model_id == WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV))
    {
        TRACE0(TRACE_DEBUG, "foundation_handle_cb_: ignore config server model\n");
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    }

    // Ignore models with wrong company id
    if (p_model->company_id != p_data->company_id)
    {
        TRACE0(TRACE_DEBUG, "foundation_handle_cb_: ignore model with wrong company id\n");
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    }

    // If that message opcode is for known model ID then call callback only for related model
    if (p_data->model_id != 0xffff
        && (p_model->company_id != p_data->company_id || p_model->model_id != p_data->model_id))
    {
        TRACE1(TRACE_DEBUG, "foundation_handle_cb_: ignore other model for know Model ID:%x\n", p_data->model_id);
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    }

    // go to next element if address is unicast and it is not address of the current element
    if ((p_data->dst & MESH_NON_UNICAST_MASK) == 0
        && p_data->dst != node_nv_data.node_id + p_elem->idx)
    {
        TRACE1(TRACE_DEBUG, "foundation_handle_cb_: ignore unicast addr:%x\n", p_data->dst);
        return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
    }

    // if company id is not requested then go to next model
    if (p_model->company_id != p_data->company_id)
    {
        TRACE1(TRACE_DEBUG, "foundation_handle_cb_: ignore company_id:%x\n", p_model->company_id);
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    }

    // Messages with destination to fixed group address shall be processed by the primary element
    if (p_data->dst >= MESH_FIXED_GROUP_MIN && p_elem->idx != 0)
    {
        TRACE0(TRACE_DEBUG, "foundation_handle_cb_: stop group addr only on non-primary element\n");
        return FND_SCAN_MODELS_CB_NEXT_STOP;
    }

    // Device key may be used only in Configuration Model and Time Setup Server and some messages of the Time Client (note Configuration Server doesn't come here)
    if (p_data->app_key_idx == 0xff)
    {
#ifndef _DEB_ENABLE_DEV_KEY_FOR_ALL_MODELS
        if (p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_CONFIG_CLNT
            && p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV
            && p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_TIME_SETUP_SRV
            && (p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_TIME_CLNT
                || (p_data->opcode != WICED_BT_MESH_OPCODE_TIME_STATUS
                    && p_data->opcode != WICED_BT_MESH_OPCODE_TIME_ROLE_STATUS
                    && p_data->opcode != WICED_BT_MESH_OPCODE_TIME_ZONE_STATUS
                    && p_data->opcode != WICED_BT_MESH_OPCODE_TAI_UTC_DELTA_STATUS)))
        {
            TRACE2(TRACE_DEBUG, "foundation_handle_cb_: ignore dev key with Model:%x opcode:%x\n", p_model->model_id, p_data->opcode);
            return FND_SCAN_MODELS_CB_NEXT_MODEL;
        }
#endif
    }
    else
    {
#ifndef MESH_CONTROLLER
#ifndef _DEB_IGNORE_MODEL_TO_APP_BINDING
        // Make sure application key is bound to that model
        if (!foundation_find_model_app_(p_model, node_nv_app_key[p_data->app_key_idx].global_idx, NULL))
        {
            TRACE1(TRACE_DEBUG, "foundation_handle_cb_: no model app binding. global_app_key_idx:%x\n", node_nv_app_key[p_data->app_key_idx].global_idx);
            return FND_SCAN_MODELS_CB_NEXT_MODEL;
        }
#endif
#endif
    }

#ifndef MESH_CONTROLLER
    // if the address is not unicast then make sure it is in the model's subscriptions list
    if (p_data->dst & MESH_NON_UNICAST_MASK)
    {
        uint8_t     status = FND_STATUS_SUCCESS;
        uint32_t    idx = foundation_find_subs_(p_model, p_data->dst, &status);
        if (status != FND_STATUS_SUCCESS)
        {
            TRACE0(TRACE_DEBUG, "foundation_handle_cb_: ignore subs\n");
            return FND_SCAN_MODELS_CB_NEXT_MODEL;
        }
    }
#endif

    if (p_model->company_id == 0
        && (p_model->model_id == WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV))
    {
        if (health_handle(p_elem->idx, p_data->src, p_data->app_key_idx, p_data->ttl, p_data->opcode, p_data->params, p_data->params_len))
            return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
    }

    if (p_data->p_event == NULL)
    {
        if ((p_data->p_event = (wiced_bt_mesh_event_t*)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_event_t))) == NULL)
            return FND_SCAN_MODELS_CB_NEXT_STOP;

        memset(p_data->p_event, 0, sizeof(wiced_bt_mesh_event_t));
        p_data->p_event->opcode = p_data->opcode;
        p_data->p_event->ttl = p_data->ttl;
        p_data->p_event->src = p_data->src;
        p_data->p_event->dst = p_data->dst;
        p_data->p_event->app_key_idx = p_data->app_key_idx;
        p_data->p_event->reply = WICED_FALSE;
        p_data->p_event->credential_flag = foundation_rcv_friend_idx != -2;

    }
    p_data->p_event->company_id = p_model->company_id;
    p_data->p_event->model_id = p_model->model_id;
    p_data->p_event->element_idx = p_elem->idx;

    // call callback and go to the next element on success
#ifndef MESH_CONTROLLER
    TRACE1(TRACE_DEBUG, "foundation_handle_cb_: calling received_msg_callback... stack_pointer:%x\n", mesh_core_get_current_stack_pointer());
#endif
    if (foundation_received_msg_callback(p_data->p_event, p_data->params, p_data->params_len))
    {
        p_data->p_event = NULL;
        return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
    }

    // go to the next model
    return FND_SCAN_MODELS_CB_NEXT_MODEL;
}

/**
* Process data packet addressed to this mesh node
*
* Parameters:
*   dst_id:         Destination address of the message.
*   src_id:         Address of the message source.
*   app_key_idx:    app key index. 0xff means dev key.
*   ttl:            TTL of the received packet
*   opcode:         opcode of the payload with bit MESH_APP_PAYLOAD_OP_LONG
*   op_company_id:  Manufacturer ID. 0 means it is not manufacturer specific
*   params:         parameters of the app payload
*   params_len:     length of the parameters of the app payload
*   friend_idx:     -2 - network sec material;-1 - LPN sec material; >=0 - sec material of friend index
*
* Return:   None
*
*/
void foundation_handle(uint16_t dst_id, uint16_t src_id, uint8_t app_key_idx, uint8_t ttl, uint16_t opcode, uint16_t op_company_id, const uint8_t *params, uint16_t params_len, int friend_idx)
{
    tFND_GET_MODELS_CB_DATA     data;
    data.model_id = 0xffff; // unknown Model ID
    data.p_event = NULL;
    // remember friend_idx of the received message. We will use it in answer
    foundation_rcv_friend_idx = friend_idx;

    // if opcode belongs to SIG Model
    if (op_company_id == 0)
    {
        wiced_bool_t    update_beacon;
        data.model_id = foundation_opcode_to_model_(opcode);
        // if opcode belongs to the Configurartion Server Models
        if (data.model_id == WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV)
        {
            // The Configuration Model should be in a primary element
            if (dst_id != node_nv_data.node_id)
                return;
            // All messages of the Configuration Model should be encrypted by device key
            if (app_key_idx != 0xff)
                return;
            if (foundation_handle_1_(src_id, ttl, opcode, params, params_len))
                return;
            if (foundation_handle_2_(src_id, ttl, opcode, params, params_len))
                return;
            if (key_refresh_handle(src_id, ttl, opcode, params, params_len, &update_beacon))
            {
#ifndef MESH_CONTROLLER
                if (update_beacon)
                    mesh_update_beacon();
#endif
                return;
            }
            return;
        }
    }

    if (foundation_received_msg_callback == NULL)
    {
        TRACE0(TRACE_DEBUG, "foundation_handle: no callback\n");
        return;
    }
    // Go through each models of each element except Configuration Server
    data.company_id     = op_company_id;
    data.dst            = dst_id;
    data.src            = src_id;
    data.app_key_idx    = app_key_idx;
    data.ttl            = ttl;
    data.opcode         = opcode;
    data.params         = params;
    data.params_len     = params_len;

    foundation_scan_models_((foundation_scan_models_cb_)foundation_handle_cb_, &data);
    if (data.p_event)
    {
        wiced_bt_free_buffer(data.p_event);
    }
}

typedef struct
{
    uint16_t  subs[32];
    uint8_t   subs_cnt;
} tFND_GET_SUBS_NUM_CB_DATA;

static int foundation_get_subs_num_cb_(tFND_NVM_ELEMENT *p_elem, tFND_NVM_MODEL *p_model, tFND_GET_SUBS_NUM_CB_DATA* p_data)
{
    int ret = FND_SCAN_MODELS_CB_NEXT_MODEL;
    uint8_t i, j;
    for (i = 0; i < p_model->subs_num; i++)
    {
        for (j = 0; j < p_data->subs_cnt; j++)
        {
            if (p_data->subs[j] == p_model->subs_list[i])
                break;
        }
        if (j < p_data->subs_cnt)
            continue;
        p_data->subs[p_data->subs_cnt] = p_model->subs_list[i];
        if (++p_data->subs_cnt >= sizeof(p_data->subs) / sizeof(p_data->subs[0]))
        {
            ret = FND_SCAN_MODELS_CB_NEXT_STOP;
            break;
        }
    }
    return ret;
}

/**
* Returns number of subscriptions and subscriptions themselves.
*
* Parameters:
*   subslist:   buffer to receive subscription list. can be NULL if size == 0.
*   size:       size of the buffer. Can be 0
*
* Return:   Number of addresses written to buffer. If size is 0 then returns the number of all subscriptions on device.
*/
uint8_t foundation_get_subs_num(uint16_t *subslist, uint8_t size)
{
    uint8_t i;
    tFND_GET_SUBS_NUM_CB_DATA data = { 0 };
    if (subslist == NULL && size != 0)
    {
        return 0;
    }
    foundation_scan_models_((foundation_scan_models_cb_)foundation_get_subs_num_cb_, &data);
    if (subslist)
    {
        for (i = 0; i < data.subs_cnt && i < size; i++)
        {
            subslist[i] = data.subs[i];
        }
        data.subs_cnt = i;

    }
    return data.subs_cnt;
}

