/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
/** @file
*
* Generic model OnOff implementation.
*/
#include "wiced_bt_ble.h"
#include "wiced_bt_mesh_core.h"
#include "wiced_bt_trace.h"
#include "mesh_model_utils.h"

static uint32_t     mesh_app_fine_timeout_ms;
static uint8_t      model_handle_cntr = 0;

wiced_mesh_model_handler_list_t model_handler_list = {.max_handles = WICED_BT_MESH_MODELS_MAX_UNIQUE_MODELS_ON_NODE, .registered_handles_cnt = 0};

/**
* Converts the given encoded transition time value in to the
* units of SMART_mesh_app_fine_timeout_ms.
*
* Parameters:
*   trans_time    :Encoded transition time value as defined in
*                model spec
*   Return        :value in number of fine timer ticks. Returns
*                zero for invalid value
*/
uint32_t get_transtime_in_finetimer_ticks(uint8_t trans_time)
{

    uint32_t   ret;

    ret = trans_time & GENDEFTRANSTIME_STEPS_MASK;

    //check valid values as per the mesh_spec
    if(ret == GENDEFTRANSTIME_STEPS_UNKNOWN || ret == 0)
        return 0;

    switch(trans_time & GENDEFTRANSTIME_STEP_RESOLUTION_MASK)
    {
    case GENDEFTRANSTIME_STEP_RESOLUTION_100MS:
        ret = (ret * 100)/mesh_app_fine_timeout_ms;
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_1S:
        ret = (ret * 1000)/mesh_app_fine_timeout_ms;
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_10S:
        ret = (ret * 10 * 1000)/mesh_app_fine_timeout_ms;
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_10M:
        ret = (ret * 10 * 60 * 1000)/mesh_app_fine_timeout_ms;
        break;
    };

    return ret;
}


/**
* Converts the given encoded transition time value in to the
* units of mesh_app_fine_timeout_ms.
*
* Parameters:
*   trans_time    :Encoded transition time value as defined in model spec
*   Return        :value in number of fine timer ticks. Returns zero for invalid value
*/
uint32_t get_transtime_in_ms(uint8_t trans_time)
{

    uint32_t   ret;

    ret = trans_time & GENDEFTRANSTIME_STEPS_MASK;

    //check valid values as per the mesh_spec
    if(ret == GENDEFTRANSTIME_STEPS_UNKNOWN || ret == 0)
        return 0;

    switch(trans_time & GENDEFTRANSTIME_STEP_RESOLUTION_MASK)
    {
    case GENDEFTRANSTIME_STEP_RESOLUTION_100MS:
        ret = (ret * 100);
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_1S:
        ret = (ret * 1000);
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_10S:
        ret = (ret * 10 * 1000);
        break;
    case GENDEFTRANSTIME_STEP_RESOLUTION_10M:
        ret = (ret * 10 * 60 * 1000);
        break;
    };

    return ret;
}

/**
* Converts the given opcode to the corresponding byte stream
* Note: It does not check the validity of the opcode.
*/
uint8_t* mesh_opcode_to_stream(uint8_t *p, uint32_t op)
{
    if (op <= 0xff)
    {
        *p++ = (uint8_t)op;
    }
    else if( op <= 0xffff)
    {
        *p++ = (uint8_t) ((op & 0x00FF00) >> 8);
        *p++ = (uint8_t) (op & 0x0000FF);
    }
    else
    {
        *p++ = (uint8_t) ((op & 0xFF0000) >> 16);
        *p++ = (uint8_t) ((op & 0x00FF00) >> 8);
        *p++ = (uint8_t) (op & 0x0000FF);
    }

    return p;
}


/**
* Converts the given transition time in milliseconds to correctly encoded value
* as per the mesh spec
* Parameters:
*   time_ms        : Transition time in milliseconds. Note, final transition time accuracy
*                   will depend upon the length of the time.
*
*   Return        : Encoded transition time
*/
uint8_t mesh_time_ms_to_transtime(uint32_t time_ms)
{
    uint8_t time_onoff;
    if (time_ms <= GENDEFTRANSTIME_STEPS_MAX * 100)
        time_onoff = (uint8_t)(time_ms / 100) | (uint8_t)GENDEFTRANSTIME_STEP_RESOLUTION_100MS;
    else if (time_ms <= GENDEFTRANSTIME_STEPS_MAX * 1000)
        time_onoff = (uint8_t)(time_ms / 1000) | (uint8_t)GENDEFTRANSTIME_STEP_RESOLUTION_1S;
    else if (time_ms <= GENDEFTRANSTIME_STEPS_MAX * 10000)
        time_onoff = (uint8_t)(time_ms / 10000) | (uint8_t)GENDEFTRANSTIME_STEP_RESOLUTION_10S;
    else
        time_onoff = (uint8_t)(time_ms / 600000) | (uint8_t)GENDEFTRANSTIME_STEP_RESOLUTION_10M;
    return time_onoff;
}

/**
 * \brief Sets the timeout value used by the App fine timer, in milliseconds. This is required for the
 * mesh models to perform timed transitions correctly.
 *
 * @param[in]      app_fine_timeout_ms : Fine timer interval in milliseconds
 *
 * @return         None
 */
void mesh_set_mesh_app_fine_timeout_ms(uint32_t app_fine_timeout_ms)
{
    mesh_app_fine_timeout_ms = app_fine_timeout_ms;
}

void state_not_bound(void *value_param)
{
    WICED_BT_TRACE("ERR: binding cb: UNUSED");
}

/**
 * \brief Initializes the internal model handler registry.
 *
 * @param       None
 *
 * @return      None
 */
void mesh_sig_model_handle_register_init(void)
{
    uint8_t    i;

    model_handler_list.registered_handles_cnt = 0;
    for(i=0; i<WICED_BT_MESH_MODELS_MAX_UNIQUE_MODELS_ON_NODE; i++)
       {
           model_handler_list.cb_list[i].model_id  = 0;
           model_handler_list.cb_list[i].model_handle = NULL;
       }

}

/**
 * Registers the given SIG model handler into the list of handlers to be called during opcode processing
 */
wiced_bool_t mesh_sig_model_handle_register(wiced_bt_mesh_model_handle_cb_t model_handle, uint16_t model_id)
{
uint8_t    i;

    for(i=0; i<model_handler_list.registered_handles_cnt; i++)
    {
        // If model handle is already registered, do not re-register it
        if(model_handler_list.cb_list[i].model_id == model_id)
            return WICED_FALSE;
    }

    if(model_handler_list.registered_handles_cnt < model_handler_list.max_handles)
    {
        model_handler_list.cb_list[model_handler_list.registered_handles_cnt].model_id     = model_id;
        model_handler_list.cb_list[model_handler_list.registered_handles_cnt].model_handle = model_handle;
        model_handler_list.registered_handles_cnt++;
    }

    return WICED_TRUE;
}

/**
* \brief Handles the SIG model messages for the instantiated models. This function should be called inside a common callback which is called by the foundation layer for functioning of the model.
*
* Parameters:
* @param[in]    element_idx        : Element Index for which the message has been received
* @param[in]    company_id         : Company ID of the Model subscribed for that message. 0 value means SIG model
* @param[in]    model              : Model ID of the Model subscribed for that message
* @param[in]    src_id             : Address of the message source.
* @param[in]    app_key_idx        : Application key index. 0xff means Device key.
* @param[in]    ttl                : TTL of the received message.
* @param[in]    opcode             : Opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
* @param[in]    params             : Parameters of the application payload.
* @param[in]    params_len         : Length (in Bytes) of the parameters of the application payload.
*
* @return       none
 */
wiced_bool_t mesh_sig_model_handle(uint8_t element_idx, uint16_t company_id, uint16_t model_id, uint16_t src_id, uint8_t app_key_idx, uint8_t ttl, uint16_t opcode, const uint8_t *params, uint16_t params_len)
{
wiced_bool_t ret = WICED_FALSE;

    if(company_id != 0)
        return ret;

    // Process through all registered model handles
    for(model_handle_cntr=0; model_handle_cntr<model_handler_list.registered_handles_cnt; model_handle_cntr++)
    {

        // Check model id, if doesn't match go to next
        if(model_handler_list.cb_list[model_handle_cntr].model_id != model_id)
            continue;

        WICED_BT_TRACE("* mesh_sig_model_handle: opcode %x  model_id %x \n", opcode, model_handler_list.cb_list[model_handle_cntr].model_id);

        // Call the model's handler
        ret = model_handler_list.cb_list[model_handle_cntr].model_handle(element_idx, src_id, app_key_idx, ttl, opcode, params, params_len);

        WICED_BT_TRACE("  ret: %d \n", ret);

        return ret;
    }

    return ret;
}

