/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
/** @file
*
* Low Power feature implementation.
*/

//#define _DEB_USE_MIA_ENTERHIDOFF
#define _DEB_DONT_DO_RECEIVEDELAY
#define _DEB_DONT_DO_IV_RECOVER

#include <stdio.h> 
#include <string.h> 

#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_aes_ccm.h"

#include "friendship.h"
#include "low_power.h"
#include "transport_layer.h"
#include "foundation.h"
#include "foundation_int.h"
#include "wiced_bt_mesh_core.h"
#include "key_refresh.h"

#ifndef MESH_CONTROLLER

#if MESH_CHIP == 20703
#elif MESH_CHIP == 20719
#else
#include "wiced_sleep.h"
#endif
#include "wiced_hal_gpio.h"
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
#include "clock_timer.h"
#endif
#endif
#include "wiced_timer.h"
#include "friend.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__LOW_POWER_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__LOW_POWER_C
#include "mesh_trace.h"

//#define _DEB_HARDCODED_RECEIVE_WINDOW   500

// All states of the LPN
#define LPN_STATE_DISABLED                      0
#define LPN_STATE_IDLE                          1
#define LPN_STATE_REQUEST_SENT                  2
#define LPN_STATE_LISTEN_OFFER                  3
#define LPN_STATE_IVI_RECOVER                   4
#define LPN_STATE_ESTABLISHMENT_POLL_SENT       5
#define LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE   6
#define LPN_STATE_ESTABLISHED                   7
#define LPN_STATE_SUBSLIST_ADD_SENT             8
#define LPN_STATE_LISTEN_SUBSLIST_ADD_CONF      9
#define LPN_STATE_POLL_SENT                     10
#define LPN_STATE_LISTEN_RESPONSE               11
#define LPN_STATE_POLL_TIMEOUT                  12

// Repeats on sending Request if no acceptable offer received during FRIENDSHIP_OFFER_LISTEN_INTERVAL
#define LPN_REQUEST_REPEATS                         3
// Repeats on sending Poll after offer received till update received
#define LPN_ESTABLISHMENT_POLL_REPEATS              6
// Friendshp establishments repeats
#define LPN_ESTABLISHMENT_REPEATS                   6
// In establishment LPN should listen for Update during that time
#define LPN_ESTABLISHMENT_UPDATE_LISTEN_INTERVAL    300
// If the Low Power node does not receive a response within the ReceiveWindow, it should resend the Friend Poll message
#define LPN_POLL_REPEATS                            3

#pragma pack(1)
#ifndef PACKED
#define PACKED
#endif

// structure to save the state of the Low Power node in the NVM
typedef PACKED struct
{
    uint16_t                lpn_counter;                        // The node shall keep a Low Power node counter (LPNCounter) initialized to 0 - Number of Friend Request messages that the Low Power node has sent
    uint16_t                addr;                               // Unicast address of friend node. MESH_NODE_ID_INVALID if friendship is not established yet.
    uint8_t                 receive_window;                     // ReceiveWindow is the time that the Low Power node listens for a response. It is received in the FriendOffer message from the friend
    uint8_t                 net_key_idx;                        // local network key index. It is initialised by 0 and always stays 0 but probably we will need it at KR
    uint8_t                 kr_phase;                           // phase of the Key Refresh
    MeshSecMaterial         sec_material;                       // security material which is comprised of the NID, the Encryption Key, and the Privacy Key.
    MeshSecMaterial         sec_material_kr;                    // security material for KR phases.
    uint8_t                 fsn;                                // Friend Sequence Number to be sent in next Poll. Can be 0 or 1. It is used to acknowledge receipt of previous messages from the Friend node to the Low Power node
    uint16_t                prev_addr;                          // Previous Friend�s unicast address
} tLPN_NVM_STATE;

typedef PACKED struct
{
    wiced_timer_t           delay_timer;                        // timer for Low Power Mode
    uint8_t                 state;                              // Low Power state - can be any of LPN_STATE_XXX
#ifndef MESH_CONTROLLER
#ifndef _DEB_USE_MIA_ENTERHIDOFF
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
    OSAPI_TIMER             sleep_wakeup_timer;                 // wake up timer
#endif
#endif
#endif
    wiced_bool_t            enable_sleep;                       // WICED_TRUE if sleep is enabled(it is command got-to-sleep)

    uint16_t                offer_addr;                         // Unicast address of the friend node we accepted offer from.
    uint8_t                 repeats;                            // Remaining Poll or Request repeats counter
    uint8_t                 establishments_repeats;             // Remaining Establishment counter
    uint8_t                 rssi;                               // RSSI of the received offer
    uint16_t                friend_counter;                     // FriendCounter received from Friend Offer message

    uint8_t                 trans_num;
    wiced_bool_t            subs_add;                           // TRUE/FALSE - add/remove - valid is subs_addr != 0
    uint16_t                subs_addr;                          // 0 means we are not sending single subs add/remove

    tLPN_NVM_STATE          nvm;                                // persistent state members
} tLPN_STATE;

#pragma pack()

tLPN_STATE                                      lpn_state = { 0 };
static wiced_bt_mesh_core_config_low_power_t    lpn_config = { 0 };     // configuration got from low_power_init()

#define LPN_NUM_ELEMENTS_MIN       0x01

#ifdef _DEB_USE_MIA_ENTERHIDOFF
// from fw2_20735B0\fw2\mpaf\drivers\miadriver.h
typedef enum
{
    HID_OFF_TIMED_WAKE_CLK_SRC_32KHZ,
    HID_OFF_TIMED_WAKE_CLK_SRC_128KHZ
} MiaTimedWakeRefClock;

void mia_enterHidOff(uint32_t wakeupTime, MiaTimedWakeRefClock ref);
#endif

static void low_power_handle_friend_offer_(wiced_bool_t lpn_sec, uint8_t ttl, uint16_t arc, uint16_t dst, uint8_t *params, uint16_t params_len);
static void low_power_handle_friend_update_(wiced_bool_t lpn_sec, uint8_t ttl, uint16_t src, uint16_t dst, uint8_t *params, uint16_t params_len);
static void low_power_handle_friend_subslist_conf_(uint8_t ttl, uint8_t *params, uint16_t params_len);

static void low_power_delay_timer_callback_(uint32_t arg);
static wiced_bool_t low_power_send_request_(void);
static wiced_bool_t low_power_send_poll_(uint16_t dst, uint8_t state);
static void low_power_sleep_(uint32_t wakeupTime);
static wiced_bool_t low_power_send_subslist_add_(wiced_bool_t retransmit);
static void low_power_lost_friend_(void);
static void low_power_ivi_updated_(wiced_bool_t res);
static void low_power_send_subs_update_msg_(wiced_bool_t add, uint8_t subs_cnt, uint16_t *p_subs, wiced_bool_t retransmit);

/**
* Local function to write current configuration in the NVM.
*
* Parameters:   None
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
static wiced_bool_t low_power_write_config_int_(void)
{
    wiced_bool_t            ret = WICED_TRUE;

    TRACE0(TRACE_INFO, "low_power_write_config_int_:\n");

    // write state
    if (sizeof(tLPN_NVM_STATE) != mesh_write_node_info(MESH_NVM_IDX_FRND_STATE, (uint8_t*)&lpn_state.nvm, sizeof(tLPN_NVM_STATE), NULL))
    {
        TRACE0(TRACE_WARNING, "low_power_write_config_int_: mesh_write_node_info failed for lpn_state\n");
        return WICED_FALSE;
    }
    return ret;
}

/**
* Writes current configuration in the NVM. It must be called on on any data change.
*
* Parameters:   None
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
wiced_bool_t low_power_write_config(void)
{
    // exit with error if LPN is disabled or we are not provisioned
    if (lpn_state.state == LPN_STATE_DISABLED
        || node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return WICED_FALSE;
    return low_power_write_config_int_();
}

static wiced_result_t low_power_start_timer_(wiced_timer_t* p_timer, uint32_t timeout)
{
    TRACE1(TRACE_INFO, "low_power_start_timer_: timeout:%x\n", timeout);
    return wiced_start_timer(p_timer, timeout);
}

#ifndef MESH_CONTROLLER
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
static uint32_t low_power_sleep_hanlder_(wiced_sleep_poll_type_t type)
{
    if (type == WICED_SLEEP_POOL_TIME_TO_SLEEP)
        return WICED_SLEEP_MAX_TIME_TO_SLEEP;
    if (type != WICED_SLEEP_POOL_SLEEP || !lpn_state.enable_sleep)
        return WICED_SLEEP_NOT_ALLOWED;
    //if(wiced_hidd_is_transport_detection_polling_on() || !wiced_bt_aon_driver_is_SDS_allowed())
    //    return WICED_SLEEP_NOT_ALLOWED;
    return WICED_SLEEP_ALLOWED;
}
#endif
#endif

/**
* Initializes Low Power feature and related data. It must be called on device startup.
* Stack calls that function with NULL parameter on device provision and on device reset
* to adjusts Low Power feature in accordance with changed provisioned state of the device.

*
* Parameters:
*   p_config:               Low Power Configuration data got from the wiced_bt_mesh_core_init() or NULL if called on provision starte change.
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*/
wiced_bool_t low_power_init(wiced_bt_mesh_core_config_low_power_t *p_config)
{
#ifndef MESH_CONTROLLER
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
    wiced_sleep_config_t    sleep_config;
#endif
    wiced_result_t          result;
#endif

    // clear all variables
    memset(&lpn_state, 0, sizeof(lpn_state));
    lpn_state.state = LPN_STATE_DISABLED;

    if (p_config)
    {
        TRACE4(TRACE_INFO, "low_power_init: rssi_factor:%d receive_window_factor:%d min_cache_size_log:%d receive_delay:%d\n", p_config->rssi_factor, p_config->receive_window_factor, p_config->min_cache_size_log, p_config->receive_delay);
        TRACE1(TRACE_INFO, " node_id:%x\n", node_nv_data.node_id);
        TRACE1(TRACE_INFO, " poll_timeout:%x\n", p_config->poll_timeout);

        memset(&lpn_config, 0, sizeof(lpn_config));

        // Exit on invalid params
        if (p_config->rssi_factor > FRIENDSHIP_RSSI_FACTOR_MAX
            || p_config->receive_window_factor > FRIENDSHIP_RECEIVE_WINDOW_FACTOR_MAX
            || p_config->min_cache_size_log < FRIENDSHIP_MIN_CACHE_SIZE_LOG_MIN
            || p_config->min_cache_size_log > FRIENDSHIP_MIN_CACHE_SIZE_LOG_MAX
            || p_config->receive_delay < FRIENDSHIP_RECEIVE_DELAY_MIN
            || p_config->poll_timeout < FRIENDSHIP_POLL_TIMEOUT_MIN
            || p_config->poll_timeout > FRIENDSHIP_POLL_TIMEOUT_MAX)
        {
            TRACE0(TRACE_INFO, "low_power_init: invalid argument\n");
            return WICED_FALSE;
        }

#ifndef MESH_CONTROLLER
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
        // configure to sleep if node is idle
        sleep_config.sleep_mode = WICED_SLEEP_MODE_HID_OFF;;
        sleep_config.device_wake_mode = WICED_SLEEP_WAKE_ACTIVE_HIGH;
        sleep_config.device_wake_source = WICED_SLEEP_WAKE_SOURCE_GPIO;
        sleep_config.device_wake_gpio_num = WICED_P04;
        sleep_config.host_wake_mode = WICED_SLEEP_WAKE_ACTIVE_HIGH;
        sleep_config.sleep_permit_handler = low_power_sleep_hanlder_;
        result = wiced_sleep_configure(&sleep_config);
        if (result != WICED_BT_SUCCESS)
        {
            TRACE1(TRACE_INFO, "low_power_init: wiced_sleep_configure failed result:%x\n", result);
            return WICED_FALSE;
        }
#endif
#endif

        // save configuration
        lpn_config = *p_config;
    }
    else
    {
        TRACE0(TRACE_INFO, "low_power_init: p_config == NULL\n");
    }

    // if node isn't provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
    {
        // Leave Low Power feature disabled (lpn_state.state == LPN_STATE_DISABLED)
        // and clear NVM data. Sign of the non-provisioned or non-established state is lpn_state.nvm.addr == 0
        low_power_write_config_int_();
        // Return success
        return WICED_TRUE;
    }

    // read state
    if (sizeof(tLPN_NVM_STATE) != mesh_read_node_info(MESH_NVM_IDX_FRND_STATE, (uint8_t*)&lpn_state.nvm, sizeof(tLPN_NVM_STATE), NULL))
    {
        TRACE0(TRACE_WARNING, "low_power_init: mesh_read_node_info failed for lpn_state\n");
        // Clear NVM data to make it unprovisioned and return error (WICED_FALSE)
        low_power_write_config_int_();
        return WICED_FALSE;
    }
    TRACE1(TRACE_WARNING, "low_power_init: addr:%x\n", lpn_state.nvm.addr);

    // Node is provisioned and we have red NVM data successfully
    // if friendship isn't established then start with idle state. Otherwise start with established state
    lpn_state.state = (lpn_state.nvm.addr == MESH_NODE_ID_INVALID) ? LPN_STATE_IDLE : LPN_STATE_ESTABLISHED;

    // initialize timer
    wiced_init_timer(&lpn_state.delay_timer, low_power_delay_timer_callback_, 0, WICED_MILLI_SECONDS_TIMER);
#ifndef _DEB_LOW_POWER_DONT_START_ON_INIT
    // start LPN logic via 100ms timer to let app to finish initialisation and to let provisioning to end all messages
    low_power_start_timer_(&lpn_state.delay_timer, 2000);
#endif

    return WICED_TRUE;
}

#ifdef _DEB_LOW_POWER_DONT_START_ON_INIT
void low_power_start(void)
{
    TRACE1(TRACE_INFO, "low_power_start: state:%x\n", lpn_state.state);
    if (lpn_state.state == LPN_STATE_DISABLED)
        return;
    if (lpn_state.state == LPN_STATE_IVI_RECOVER)
        lpn_state.state = LPN_STATE_IDLE;
    low_power_start_timer_(&lpn_state.delay_timer, 2000);
}
#endif

/**
* Does net decryption of the mesh packet trying LPN security material depending on device type and state
*
* Parameters:
*   adv_data:       Received packet.
*                   Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits))
*                   || SEQ(3bytes) || SRC(2bytes) || DST(2bytes)
*                   || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
*   adv_len:        Length of the received packet.
*   msg:            Buffer for decrypted packet. Should be suficiently big (ADV_LEN_MAX).
*   p_iv_index:     IV index
*   p_net_key_idx:  Buffer for the index of the net key
*
* Return:           WICED_TRUE/WICED_FALSE succeeded/failed
*
*/
wiced_bool_t low_power_net_decrypt(const BYTE* adv_data, BYTE adv_len, BYTE* msg, uint8_t* p_iv_index, uint8_t* p_net_key_idx)
{
    wiced_bool_t res = WICED_TRUE;
    // fail if friendship is not established
    if (lpn_state.state <= LPN_STATE_LISTEN_OFFER)
        res = WICED_FALSE;
    // succeed if decryption with main/old key succeeds
    else if (mesh_sec_material_decrypt(WICED_FALSE, &lpn_state.nvm.sec_material, adv_data, adv_len, msg, p_iv_index))
        *p_net_key_idx = lpn_state.nvm.net_key_idx;
    // fail if we are not in the KR phase
    else if(lpn_state.nvm.kr_phase == KEY_REFRESH_PHASE_STATE_NONE)
         res = WICED_FALSE;
    // succeed if decryption with new key succeeds
    else if (mesh_sec_material_decrypt(WICED_FALSE, &lpn_state.nvm.sec_material_kr, adv_data, adv_len, msg, p_iv_index))
    *p_net_key_idx = lpn_state.nvm.net_key_idx;
    else
        res = WICED_FALSE;
    TRACE3(TRACE_INFO, "low_power_net_decrypt: state:%d returns res:%d net_key_idx:%x\n", lpn_state.state, res, *p_net_key_idx);
    return res;
}

static MeshSecMaterial *low_power_get_sec_material_int_(void)
{
    // In phase 2 the node shall only transmit messages using the new keys
    return lpn_state.nvm.kr_phase == KEY_REFRESH_PHASE_STATE_2 ? &lpn_state.nvm.sec_material_kr : &lpn_state.nvm.sec_material;
}

/**
* Returns friendship secure material if state is OK
*
* Return:           Pointer to secure material
*
*/
MeshSecMaterial *low_power_get_sec_material(void)
{
    MeshSecMaterial *ret = NULL;
    TRACE4(TRACE_INFO, "low_power_get_sec_material: state:%x kr_phase:%d addr:%x offer_addr:%x\n", lpn_state.state, lpn_state.nvm.kr_phase, lpn_state.nvm.addr, lpn_state.offer_addr);
    if (lpn_state.state >= LPN_STATE_ESTABLISHED)
        ret = low_power_get_sec_material_int_();
    return ret;
}

static void low_power_accept_offer(void)
{
    // calculate friendship secure material (use lpn_counter-1 because we increased it after we sent friend request)
    TRACE3(TRACE_INFO, "low_power_accept_offer: offer_addr:%x lpn_counter:%x friend_counter:%x\n", lpn_state.offer_addr, lpn_state.nvm.lpn_counter, lpn_state.friend_counter);
    calc_net_security_friend(node_nv_data.node_id, lpn_state.offer_addr, lpn_state.nvm.lpn_counter - 1, lpn_state.friend_counter,
        node_nv_net_key[lpn_state.nvm.net_key_idx].key, &lpn_state.nvm.sec_material.nid, lpn_state.nvm.sec_material.encr_key,
        lpn_state.nvm.sec_material.privacy_key);

    lpn_state.nvm.kr_phase = KEY_REFRESH_PHASE_STATE_NONE;

    // Set persistently 0 to Friend Sequence Number befor first Poll
    lpn_state.nvm.fsn = 0;
    low_power_write_config_int_();

    // send poll - it will use friendship secure material
    lpn_state.repeats = LPN_ESTABLISHMENT_POLL_REPEATS;
    low_power_send_poll_(lpn_state.offer_addr, LPN_STATE_ESTABLISHMENT_POLL_SENT);
}

void low_power_kr_set_phase_1(uint8_t *new_key)
{
    TRACE4(TRACE_INFO, "low_power_kr_set_phase_1: kr_phase:%d offer_addr:%x lpn_counter:%x friend_counter:%x\n", lpn_state.nvm.kr_phase, lpn_state.offer_addr, lpn_state.nvm.lpn_counter, lpn_state.friend_counter);
    // calculate friendship secure material (use lpn_counter-1 because we increased it after we sent friend request)
    calc_net_security_friend(node_nv_data.node_id, lpn_state.offer_addr, lpn_state.nvm.lpn_counter - 1, lpn_state.friend_counter,
        new_key, &lpn_state.nvm.sec_material_kr.nid, lpn_state.nvm.sec_material_kr.encr_key,
        lpn_state.nvm.sec_material_kr.privacy_key);
    lpn_state.nvm.kr_phase = KEY_REFRESH_PHASE_STATE_1;
    low_power_write_config_int_();
}

void low_power_kr_set_phase_2(void)
{
    TRACE1(TRACE_INFO, "low_power_kr_set_phase_2: kr_phase:%d\n", lpn_state.nvm.kr_phase);
    lpn_state.nvm.kr_phase = KEY_REFRESH_PHASE_STATE_2;
    low_power_write_config_int_();
}

void low_power_kr_finish(void)
{
    TRACE1(TRACE_INFO, "low_power_kr_finish: kr_phase:%d\n", lpn_state.nvm.kr_phase);
    lpn_state.nvm.sec_material = lpn_state.nvm.sec_material_kr;
    lpn_state.nvm.kr_phase = KEY_REFRESH_PHASE_STATE_NONE;
    low_power_write_config_int_();
}

/**
* Delay timer callback function for function wiced_init_timer()
*
* Parameters:
*   arg:    not used
*
* Return:   None
*
*/
static void low_power_delay_timer_callback_(uint32_t arg)
{
    TRACE2(TRACE_INFO, "low_power_delay_timer_callback_: node_id:%x friend:%x\n", node_nv_data.node_id, lpn_state.nvm.addr);
    // just exit if we are not provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return;
    // Print all variables depending on the friendship establishement state
    if (lpn_state.nvm.addr == MESH_NODE_ID_INVALID)
    {
        TRACE4(TRACE_INFO, " state:%d repeats:%d establishments_repeats:%d rssi:%x\n", lpn_state.state, lpn_state.repeats, lpn_state.establishments_repeats, lpn_state.rssi);
        TRACE2(TRACE_INFO, " lpn_counter:%x offer_addr:%x\n", lpn_state.nvm.lpn_counter, lpn_state.offer_addr);
        TRACE1(TRACE_INFO, " friend_counter:%d \n", lpn_state.friend_counter);
    }
    else
    {
        TRACE3(TRACE_INFO, " state:%d repeats:%d lpn_counter:%x\n", lpn_state.state, lpn_state.repeats, lpn_state.nvm.lpn_counter);
        TRACE4(TRACE_INFO, " receive_delay:%d receive_window:%d net_key_idx:%d fsn:%d\n", lpn_config.receive_delay, lpn_state.nvm.receive_window, lpn_state.nvm.net_key_idx, lpn_state.nvm.fsn);
        TRACE1(TRACE_INFO, " prev_addr:%x\n", lpn_state.nvm.prev_addr);
    }

    switch (lpn_state.state)
    {
    case LPN_STATE_DISABLED:
        break;
    case LPN_STATE_IDLE:
        // start friendship establishment - send FriendRequest message
        lpn_state.repeats = LPN_REQUEST_REPEATS;
        lpn_state.establishments_repeats = LPN_ESTABLISHMENT_REPEATS;
        low_power_send_request_();
        break;
    case LPN_STATE_REQUEST_SENT:
        // Offer Listen Delay is passed. Start litenning for FRIENDSHIP_OFFER_LISTEN_INTERVAL
        lpn_state.state = LPN_STATE_LISTEN_OFFER;
        low_power_start_timer_(&lpn_state.delay_timer, FRIENDSHIP_OFFER_LISTEN_INTERVAL);
        break;
    case LPN_STATE_LISTEN_OFFER:
        // if acceptable offer is received during FRIENDSHIP_OFFER_LISTEN_INTERVAL then send Poll
        if (lpn_state.offer_addr != MESH_NODE_ID_INVALID)
        {
            low_power_accept_offer();
            break;
        }
        // we didn't get any acceptable Offer
        // If request repeats is not exhosted then send FriendRequest again
        if (lpn_state.repeats-- > 1)
        {
            low_power_send_request_();
            break;
        }
        lpn_state.state = LPN_STATE_IDLE;
#ifndef _DEB_DONT_DO_IV_RECOVER
#ifndef MESH_CONTROLLER
        // No more LPN_REQUEST_REPEATS repeats. Initiate the IV Index Recovery procedure.
        if (ivi_recovery_start(low_power_ivi_updated_))
        {
            lpn_state.state = LPN_STATE_IVI_RECOVER;
            break;
        }
#endif
#endif
        // On error Go to sleep mode to try to establish friendship on wake up
        low_power_sleep_(lpn_config.poll_timeout * 50);
        break;
    case LPN_STATE_ESTABLISHMENT_POLL_SENT:
        // Receive Delay is passed. Start ReceiveWindow timer for listenning for Update
        lpn_state.state = LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE;
#ifdef _DEB_HARDCODED_RECEIVE_WINDOW
        low_power_start_timer_(&lpn_state.delay_timer, _DEB_HARDCODED_RECEIVE_WINDOW);
#else
        low_power_start_timer_(&lpn_state.delay_timer, lpn_state.nvm.receive_window);
#endif
        break;
    case LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE:
        // We didn't receive Update during LPN_ESTABLISHMENT_UPDATE_LISTEN_INTERVAL
        // if poll repeats is not exhosted then send poll again
        if (lpn_state.repeats-- > 1)
        {
            low_power_send_poll_(lpn_state.offer_addr, LPN_STATE_ESTABLISHMENT_POLL_SENT);
            break;
        }

        // If establishments repeats is not exhosted then start establishment again
        if (lpn_state.establishments_repeats-- > 1)
        {
            lpn_state.repeats = LPN_REQUEST_REPEATS;
            low_power_send_request_();
            break;
        }
        lpn_state.state = LPN_STATE_IDLE;
#ifndef _DEB_DONT_DO_IV_RECOVER
#ifndef MESH_CONTROLLER
        // No more establishments repeats. Initiate the IV Index Recovery procedure.
        if (ivi_recovery_start(low_power_ivi_updated_))
        {
            lpn_state.state = LPN_STATE_IVI_RECOVER;
        }
#endif
#endif
        // On error Go to sleep mode to try to establish friendship on wake up
        low_power_sleep_(lpn_config.poll_timeout * 50);
        break;
    case LPN_STATE_ESTABLISHED:
        // we are in the friendship established state. We are here on startup or poll timeout.
        // Send Poll
        lpn_state.repeats = LPN_POLL_REPEATS;
        low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
        break;
    case LPN_STATE_SUBSLIST_ADD_SENT:
        // listen for Subscription List Confirm
        lpn_state.state = LPN_STATE_LISTEN_SUBSLIST_ADD_CONF;
#ifdef _DEB_HARDCODED_RECEIVE_WINDOW
        low_power_start_timer_(&lpn_state.delay_timer, _DEB_HARDCODED_RECEIVE_WINDOW);
#else
        low_power_start_timer_(&lpn_state.delay_timer, lpn_state.nvm.receive_window);
#endif
        break;
    case LPN_STATE_LISTEN_SUBSLIST_ADD_CONF:
        // we didn't receive Subscription Add Conformation.
        // if we have remaining retransmittions then send that message again
        if (lpn_state.repeats-- > 1)
        {
            // if it was single subs add/remove
            if (lpn_state.subs_addr != 0)    
                low_power_send_subs_update_msg_(lpn_state.subs_add, 1, &lpn_state.subs_addr, WICED_TRUE);
            else if(!low_power_send_subslist_add_(WICED_TRUE))
            {
                // no subscriptions to send. Then we are done and can start polling.
                lpn_state.repeats = LPN_POLL_REPEATS;
                low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
            }
            break;
        }
        // We lost the friend. Start friendship establishment
        low_power_lost_friend_();
        break;
    case LPN_STATE_POLL_SENT:
        // Receive Delay is passed. Start ReceiveWindow timer for listenning for Update or Packet
        lpn_state.state = LPN_STATE_LISTEN_RESPONSE;
#ifdef _DEB_HARDCODED_RECEIVE_WINDOW
        low_power_start_timer_(&lpn_state.delay_timer, _DEB_HARDCODED_RECEIVE_WINDOW);
#else
        low_power_start_timer_(&lpn_state.delay_timer, lpn_state.nvm.receive_window);
#endif
        break;
    case LPN_STATE_LISTEN_RESPONSE:
        // No response is received during ReceiveWindow
        // If repeat count is not exhosted then send Poll again
        if (lpn_state.repeats-- > 1)
        {
            low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
            break;
        }
        // We lost the friend. Start friendship establishment
        low_power_lost_friend_();
        break;
    case LPN_STATE_POLL_TIMEOUT:
        // we are in the friendship established state. Send Poll
        lpn_state.repeats = LPN_POLL_REPEATS;
        low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
        break;
    }
}

/**
* Process controll message.
*
* Parameters:
*   op:             OP code.
*   params:         parameters of the payload
*   params_len:     length of the parameters of the payload
*   src:            source address
*   dst:            destination address
*   ttl:            TTL of the received packet
*   lpn_sec:        WICED_TRUE - LPN security material is used; WICED_FALSE - network sec material
*
* Return:   WICED_TRUE - message handled; WICED_FALSE - try other handler.
*
*/
wiced_bool_t low_power_handle_ctrl(uint8_t op, uint8_t* params, uint16_t params_len, uint16_t src, uint16_t dst, uint8_t ttl, wiced_bool_t lpn_sec)
{
    wiced_bool_t ret = WICED_TRUE;

    TRACE4(TRACE_INFO, "low_power_handle_ctrl: op:%d ttl:%d lpn_sec:%d state:%d \n", op, ttl, lpn_sec, lpn_state.state);
    TRACE2(TRACE_INFO, " params_len:%d lpn.addr:%x\n", params_len, lpn_state.nvm.addr);
    TRACE2(TRACE_INFO, " src:%x dst:%x\n", src, dst);

    switch (op)
    {
    case MESH_TL_CM_OP_FRIEND_OFFER:
        low_power_handle_friend_offer_(lpn_sec, ttl, src, dst, params, params_len);
        break;

    case MESH_TL_CM_OP_FRIEND_UPDATE:
        low_power_handle_friend_update_(lpn_sec, ttl, src, dst, params, params_len);
        break;

    case MESH_TL_CM_OP_FRIEND_SUBSLIST_CONF:
        low_power_handle_friend_subslist_conf_(ttl, params, params_len);
        break;

    default:
        ret = WICED_FALSE;
        break;
    }
    return ret;
}

/**
* Creates Friend Poll message. It is sent by a Low Power node to ask the Friend node to send any messages that it has cached for the Low Power node.
*
* Parameters:
*   fsn:    Friend Sequence Number used to acknowledge receipt of previous messages from the Friend node to the Low Power node
*   buf:    Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t low_power_crt_friend_poll(wiced_bool_t fsn, uint8_t *buf)
{
    uint16_t len = 0;
    buf[len++] = fsn ? FRIENDSHIP_FRIEND_POLL_FSN_FLAG : 0x00;
    TRACE0(TRACE_INFO, "low_power_crt_friend_poll:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Request message. It is broadcasted by a Low Power node to start to find a friend.
*
* Parameters:
*   rssi_factor:            The RSSIFactor field is used in the Friend Offer Delay calculation. Can be between 0-3. RSSIFactor = (value + 1) * 0.5;
*   receive_window_factor:  The ReceiveWindowFactor field is used in the Friend Offer Delay calculation. Can be between 0-3. ReceiveWindowFactor = (value + 1) * 0.5;
*   min_cache_size_log:     The minimum number of Lower Transport PDUs that the Friend node can store in its Friend Cache. Can be between 1-7. N = 2 ** value;
*   receive_delay:          ReceiveDelay. Can be between 0x0A�0xFF
*   poll_timeout:           PollTimeout in units of 100 milliseconds. Can be between 0x00000A�0x34BBFF
*   buf:                    Buffer for created message. It should have suficient size to fit all message
*   previous_address:       Previous Friend�s unicast address
*   num_elements:           Number of Elements in the Low Power node. can be 0x01-0xff
*   lpn_counter:            Number of Friend Request messages that the Low Power node has sent
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t low_power_crt_friend_request(uint8_t rssi_factor, uint8_t receive_window_factor, uint8_t min_cache_size_log, uint8_t receive_delay, uint32_t poll_timeout,
                                         uint16_t previous_address, uint8_t num_elements, uint16_t lpn_counter, uint8_t *buf)
{
    uint16_t len;
    
    // Exit on invalid params
    if (rssi_factor > FRIENDSHIP_RSSI_FACTOR_MAX
        || receive_window_factor > FRIENDSHIP_RECEIVE_WINDOW_FACTOR_MAX
        || min_cache_size_log < FRIENDSHIP_MIN_CACHE_SIZE_LOG_MIN
        || min_cache_size_log > FRIENDSHIP_MIN_CACHE_SIZE_LOG_MAX
        || receive_delay < FRIENDSHIP_RECEIVE_DELAY_MIN
        || poll_timeout < FRIENDSHIP_POLL_TIMEOUT_MIN
        || poll_timeout > FRIENDSHIP_POLL_TIMEOUT_MAX
        || num_elements < LPN_NUM_ELEMENTS_MIN
        || (previous_address != MESH_NODE_ID_INVALID
            && (previous_address & MESH_NON_UNICAST_MASK) != 0))
    {
        return 0;
    }

    len = 0;
    buf[len] = FRIENDSHIP_CRITERIA_FROM_RSSI_FACTOR(rssi_factor);
    buf[len] |= FRIENDSHIP_CRITERIA_FROM_RECEIVE_WINDOW_FACTOR(receive_window_factor);
    buf[len] |= FRIENDSHIP_CRITERIA_FROM_MIN_CACHE_SIZE_LOG(min_cache_size_log);
    len++;
    buf[len++] = receive_delay;
    UINT32TOBE3(buf + len, poll_timeout);
    len += 3;
    UINT16TOBE2(buf + len, previous_address);
    len += 2;
    buf[len++] = num_elements;
    UINT16TOBE2(buf + len, lpn_counter);
    len += 2;

    TRACE0(TRACE_INFO, "low_power_crt_friend_request:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Subscription List Add message. It is sent to indicate the list of group addresses and virtual addresses
* for which messages are to be cached.
*
* Parameters:
*   trans_num:      The number for identifying a transaction
*   addr_num:       Number of addresses to add
*   addr:           Addresses to add
*   buf:            Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t low_power_crt_friend_subs_list_add(uint8_t trans_num, uint8_t addr_num, uint16_t *addr, uint8_t *buf)
{
    uint16_t len = 0;
    buf[len++] = trans_num;
    if (addr_num == 0 || addr_num > 100)
        return 0;
    while (addr_num--)
    {
        UINT16TOBE2(buf + len, *addr);
        len += 2;
        addr++;
    }

    TRACE0(TRACE_INFO, "low_power_crt_friend_subs_list_add:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Subscription List Remove message. It is sent to indicate the group addresses and virtual addresses
* to remove from the Friend Subscription List.
*
* Parameters:
*   trans_num:      The number for identifying a transaction
*   addr_num:       Number of addresses to add
*   addr:           Addresses to add
*   buf:            Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t low_power_crt_friend_subs_list_remove(uint8_t trans_num, uint8_t addr_num, uint16_t *addr, uint8_t *buf)
{
    uint16_t len = 0;
    buf[len++] = trans_num;
    if (addr_num == 0 || addr_num > 100)
        return 0;
    while (addr_num--)
    {
        UINT16TOBE2(buf + len, *addr);
        len += 2;
        addr++;
    }

    TRACE0(TRACE_INFO, "low_power_crt_friend_subs_list_remove:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

static void low_power_handle_friend_offer_(wiced_bool_t lpn_sec, uint8_t ttl, uint16_t src, uint16_t dst, uint8_t *params, uint16_t params_len)
{
    uint8_t     receive_window, cache_size, subscription_list_size, rssi, subs_num;
    uint16_t    friend_counter;
    // <ReceiveWindow(1 byte)><CacheSize(1 byte)><SubscriptionListSize(1 byte)><RSSI(1 byte)><FriendCounter(2 bytes)>
    // ignore message if network PDU was not encrypted by network secured material
    if (lpn_sec
        || ttl != 0                                             // or TTL is not 0
        || lpn_state.state != LPN_STATE_LISTEN_OFFER            // or we don't expect Offer
        || dst != node_nv_data.node_id                          // or DST is not our address
        || params_len != 6)                                     // or length is wrong
    {
        TRACE0(TRACE_INFO, "low_power_handle_friend_offer_: invalid state or params\n");
        return;
    }

    subscription_list_size = params[2];
    // ignore that offer if our subscriptions do not fit to it
    subs_num = foundation_get_subs_num(NULL, 0);
    if (subscription_list_size < subs_num)
    {
        TRACE2(TRACE_INFO, "low_power_handle_friend_offer_: ignore: unsuficient subs num:%d. Need:%d\n", subscription_list_size, subs_num);
        return;
    }

    receive_window = params[0];
    if (receive_window < FRIEND_RECEIVE_WINDOW_MIN) //RFU
    {
        TRACE1(TRACE_INFO, "low_power_handle_friend_offer_: ignore: too small receive_window:%x\n", receive_window);
        return;
    }
    cache_size = params[1];
    rssi = params[3];
    friend_counter = BE2TOUINT16(params + 4);

    // if it is not first offer then check if it is better then already accepted one
    if (lpn_state.offer_addr != MESH_NODE_ID_INVALID)
    {
        // ignore offer if old one has better RSSI
        if (lpn_state.rssi > rssi)
        {
            TRACE2(TRACE_INFO, "low_power_handle_friend_offer_: ignore: RSSI:%x less then RSSI of other offer:%x\n", rssi, lpn_state.rssi);
            return;
        }
    }
    // stop timer
    wiced_stop_timer(&lpn_state.delay_timer);
    // Remember that offer
    lpn_state.offer_addr = src;
    lpn_state.rssi = rssi;
    lpn_state.nvm.receive_window = receive_window;
    lpn_state.friend_counter = friend_counter;
    TRACE4(TRACE_INFO, "low_power_handle_friend_offer_: accept: RSSI:%x receive_window:%x cache_size:%d friend_counter:%x\n", rssi, receive_window, cache_size, friend_counter);
    low_power_accept_offer();
}

static void low_power_handle_friend_update_(wiced_bool_t lpn_sec, uint8_t ttl, uint16_t src, uint16_t dst, uint8_t *params, uint16_t params_len)
{
    wiced_bool_t    establishment;
#ifndef MESH_CONTROLLER
    wiced_bool_t    update_beacon;
#endif

    // <RFU(6 bits)><IV Update(1 bit)><Key Refresh(1 bit)><IV index(4 bytes)>

    // ignore that message if network PDU was not encrypted by friendship secured material
    if (!lpn_sec
        || (lpn_state.state != LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE    // or we don't expect Update 
            && lpn_state.state != LPN_STATE_LISTEN_RESPONSE)            // and we don't expect message from friend cache
        || dst != node_nv_data.node_id                                  // or DST is not our address
        || ttl != 0                                                     // or TTL is not 0
        || params_len != 6                                              // or length is wrong
        || params[5] > 1)                                               // MD field can be 0 or 1
    {
        TRACE0(TRACE_INFO, "low_power_handle_friend_update_: invalid state or params\n");
        return;
    }

    // stop timer
    wiced_stop_timer(&lpn_state.delay_timer);

    establishment = lpn_state.state == LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE;
    // update state before execution of any logic
    lpn_state.state = LPN_STATE_ESTABLISHED;

    // if it is update for friendship establishment then friendship is established
    if (establishment)
    {
        lpn_state.nvm.prev_addr = lpn_state.nvm.addr;
        lpn_state.nvm.addr = lpn_state.offer_addr;
        low_power_write_config_int_();
    }
    else
    {
        // It is responce on Poll
        // toggle the Friend Sequence Number and remember it in the NVM
        lpn_state.nvm.fsn ^= FRIENDSHIP_FRIEND_POLL_FSN_FLAG;
        low_power_write_config_int_();
    }

#ifndef MESH_CONTROLLER
    // execute IV Update logic
    update_beacon = iv_updt_handle((params[0] & FRIENDSHIP_FRIEND_UPDATE_IV_UPDATE_FLAG) != 0, BE4TOUINT32(params + 1), mesh_core_iv_updt_test_mode);
    // execute Key Refresh logic
    if (key_refresh_handle_beacon((params[0] & FRIENDSHIP_FRIEND_UPDATE_KEY_REFRESH_FLAG) != 0, lpn_state.nvm.net_key_idx))
        update_beacon = WICED_TRUE;
    // Update proxy server advertisement and network secure beacon if needed
    if (update_beacon)
        mesh_update_beacon();
#endif
    // if it is update for friendship establishment then add subscription list to the Friend
    if (establishment)
    {
        // send subscriptions starting from transaction 0
        lpn_state.trans_num = 0;
        if (low_power_send_subslist_add_(WICED_FALSE))
        {
            // we sent subscriptions. Then we are done.
            return;
        }
    }

    // it was not friendship establishment or we don't have subscriptions to send to Friend.
    // send poll if friend has more data
    if (params[5])
    {
        lpn_state.repeats = LPN_POLL_REPEATS;
        low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
        return;
    }
    // Friend doesn't have data. Go to sleep to send poll after PollTimeout
    lpn_state.state = LPN_STATE_POLL_TIMEOUT;
    lpn_state.subs_addr = 0;    // we are not sending single add/remove
    //timeout should be less then poll timeout to prevent friendship termination    lpn_state.state = LPN_STATE_POLL_TIMEOUT;
    low_power_sleep_(lpn_config.poll_timeout * 50);
    TRACE2(TRACE_WARNING, "low_power_handle_friend_update_: returns. state:%d poll_timeout:%d\n", lpn_state.state, lpn_config.poll_timeout);
    return;
}

static void low_power_handle_friend_subslist_conf_(uint8_t ttl, uint8_t *params, uint16_t params_len)
{
    // <TransactionNumber(1 byte)>
    // ignore that message if it is not expected
    if (lpn_state.state != LPN_STATE_LISTEN_SUBSLIST_ADD_CONF
        || ttl != 0                                             // or TTL is not 0
        || params_len != 1                                      // or it has wrong length
        || lpn_state.trans_num != params[0])                    // or wrong transaction number
    {
        TRACE2(TRACE_WARNING, "low_power_handle_friend_subslist_conf_: wrong state or params. transaction number:%x expected:%x\n", params[0], lpn_state.trans_num);
        return;
    }
    TRACE2(TRACE_INFO, "low_power_handle_friend_subslist_conf_: subs_add:%d subs_addr:%x\n", lpn_state.subs_add, lpn_state.subs_addr);

    wiced_stop_timer(&lpn_state.delay_timer);
    // send subscriptions starting from next transaction
    lpn_state.trans_num++;

    if (lpn_state.subs_addr != 0    // if it was wingle subs add/remove then we are done
        || !low_power_send_subslist_add_(WICED_FALSE))
    {
        lpn_state.subs_addr = 0;
        // we don't have subscriptions to send to Friend. Start polling
        lpn_state.repeats = LPN_POLL_REPEATS;
        low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
    }
}

static wiced_bool_t low_power_send_request_(void)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t         buf[16];
    uint16_t        len;

    // Reset received offer
    lpn_state.offer_addr = MESH_NODE_ID_INVALID;

#ifdef _DEB_NO_OFFER_LISTEN_DELAY
    lpn_state.state = LPN_STATE_LISTEN_OFFER;
#else
    lpn_state.state = LPN_STATE_REQUEST_SENT;
#endif

    // create Upper transport PDU 
    len = low_power_crt_friend_request(lpn_config.rssi_factor, lpn_config.receive_window_factor, lpn_config.min_cache_size_log, lpn_config.receive_delay, lpn_config.poll_timeout,
        lpn_state.nvm.prev_addr, fnd_static_config_.elements_num, lpn_state.nvm.lpn_counter, buf);
    if (len)
    {
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0, 3, 1, 0, 0, 0);
#endif
        // send broadcast controll message with 0 TTL and main net_key and master secure material 
        if (transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_REQUEST, buf, len, lpn_state.nvm.net_key_idx, NULL, node_nv_data.node_id, MESH_FIXED_GROUP_ALL_FRIENDS, 0))
        {
            // After each Friend Request message is sent, LPNCounter shall be incremented by 1. The LPNCounter may wrap.
            lpn_state.nvm.lpn_counter++;
            low_power_write_config_int_();
            ret = WICED_TRUE;
        }
    }
#ifdef _DEB_NO_OFFER_LISTEN_DELAY
    low_power_start_timer_(&lpn_state.delay_timer, FRIENDSHIP_OFFER_LISTEN_INTERVAL);
#else
    low_power_start_timer_(&lpn_state.delay_timer, FRIENDSHIP_OFFER_LISTEN_DELAY);
#endif
    TRACE1(TRACE_WARNING, "low_power_send_request_: returns %d\n", ret);
    return ret;
}

static wiced_bool_t low_power_send_poll_(uint16_t dst, uint8_t state)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t         buf[16];
    uint16_t        len;

    // set state before sending poll
#ifdef _DEB_DONT_DO_RECEIVEDELAY
    state = state == LPN_STATE_ESTABLISHMENT_POLL_SENT ? LPN_STATE_ESTABLISHMENT_LISTEN_UPDATE : LPN_STATE_LISTEN_RESPONSE;
#endif
    lpn_state.state = state;
    // create Upper transport PDU 
    len = low_power_crt_friend_poll(lpn_state.nvm.fsn, buf);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 3, 1, 0, 0, 0);
#endif
    // send unicast controll message with 0 TTL and main net_key and friendship secure material
    if (transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_POLL, buf, len, lpn_state.nvm.net_key_idx, low_power_get_sec_material_int_(), node_nv_data.node_id, dst, 0))
        ret = WICED_TRUE;
#ifdef _DEB_DONT_DO_RECEIVEDELAY
    low_power_start_timer_(&lpn_state.delay_timer, lpn_state.nvm.receive_window);
    TRACE3(TRACE_WARNING, "low_power_send_poll_: returns %d state:%d receive_window:%d\n", ret, state, lpn_state.nvm.receive_window);
#else
    low_power_start_timer_(&lpn_state.delay_timer, lpn_config.receive_delay);
    TRACE3(TRACE_WARNING, "low_power_send_poll_: returns %d state:%d receive_delay:%d\n", ret, state, lpn_config.receive_delay);
#endif
    return ret;
}

void low_power_sleep_wakeup_(INT32 args, UINT32 overTimeInUs)
{
    TRACE0(TRACE_WARNING, "low_power_sleep_wakeup_:\n");
}

static void low_power_sleep_(uint32_t wakeupTimeMs)
{
#ifndef MESH_CONTROLLER
    TRACE1(TRACE_WARNING, "low_power_sleep_: wakeupTimeMs:%d ********************************\n", wakeupTimeMs);
#ifdef _DEB_USE_MIA_ENTERHIDOFF
    mia_enterHidOff(wakeupTimeMs, HID_OFF_TIMED_WAKE_CLK_SRC_128KHZ);
#else
#ifndef _DEB_LOW_POWER_DISABLE_SLEEP
    lpn_state.enable_sleep = WICED_TRUE;
    osapi_createTimer(&lpn_state.sleep_wakeup_timer, low_power_sleep_wakeup_, 0);
    osapi_setTimerWakeupSource(&lpn_state.sleep_wakeup_timer, 1);
    osapi_activateTimer(&lpn_state.sleep_wakeup_timer, wakeupTimeMs * 1000);  // in microseconds
#endif
#endif
    // if we didn't get to sleep mode for some reason (in debug mode?) then send poll again after PollTimeout
    low_power_start_timer_(&lpn_state.delay_timer, wakeupTimeMs);
#endif
}

/**
* Callback function to be called at the end of IVI recovery procedure started on ivi_recovery_start().
*
* Parameters:
*   res:        WICED_TRUE/WICED_FALSE - IVI recovery succeeded/failed
*
* Return:   None
*/
static void low_power_ivi_updated_(wiced_bool_t res)
{
    // If we are in the IVI recover state 
    if (lpn_state.state == LPN_STATE_IVI_RECOVER)
    {
        // on success start friendship establishment again
        if (res)
        {
            // start with idle state
            lpn_state.state = LPN_STATE_IDLE;
            // start LPN logic via timer start just to handle all states in one place
            low_power_start_timer_(&lpn_state.delay_timer, 100);
        }
        else
        {
            // On error Go to sleep mode to try to establish friendship on wake up
            low_power_sleep_(lpn_config.poll_timeout * 50);
        }
    }
}

static void low_power_send_subs_update_msg_(wiced_bool_t add, uint8_t subs_cnt, uint16_t *p_subs, wiced_bool_t retransmit)
{
    uint8_t         buf[MESH_LOWER_TRANSPORT_PDU_MAX_LEN];
    uint16_t        len;
    // create and send unicast controll message with 0 TTL and main net_key
    len = low_power_crt_friend_subs_list_add(lpn_state.trans_num, subs_cnt, p_subs, buf);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 3, 1, 0, 0, 0);
#endif
    transport_layer_send_ctrl(add ? MESH_TL_CM_OP_FRIEND_SUBSLIST_ADD : MESH_TL_CM_OP_FRIEND_SUBSLIST_REMOVE,
        buf, len, lpn_state.nvm.net_key_idx, low_power_get_sec_material_int_(), node_nv_data.node_id, lpn_state.nvm.addr, 0);

    // set repeats counter if it is not retransmitting request
    if (!retransmit)
    {
        lpn_state.repeats = LPN_REQUEST_REPEATS;
    }
#ifdef _DEB_DONT_DO_RECEIVEDELAY
    lpn_state.state = LPN_STATE_LISTEN_SUBSLIST_ADD_CONF;
    low_power_start_timer_(&lpn_state.delay_timer, lpn_state.nvm.receive_window);
#else
    lpn_state.state = LPN_STATE_SUBSLIST_ADD_SENT;
    low_power_start_timer_(&lpn_state.delay_timer, lpn_config.receive_delay);
#endif
}

static wiced_bool_t low_power_send_subslist_add_(wiced_bool_t retransmit)
{
    uint16_t        subs_list[FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST];
    uint8_t         subs_cnt, subs_added, subs_to_add;

    subs_cnt = foundation_get_subs_num(subs_list, FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST);

    TRACE2(TRACE_WARNING, "low_power_send_subslist_add_: subs_cnt:%d trans_num:%d\n", subs_cnt, lpn_state.trans_num);

    // already added subscriptions
    subs_added = lpn_state.trans_num * FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST_MSG;
    //if we don't have more subscriptions to add then exit with WICED_FALSE
    if (subs_cnt <= subs_added)
    {
        return WICED_FALSE;
    }

    // Calc subscriptibtions to add in that transaction
    subs_to_add = subs_cnt - subs_added;
    if (subs_to_add > FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST_MSG)
        subs_to_add = FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST_MSG;

    lpn_state.subs_addr = 0;    // we are not sending single add/remove
    low_power_send_subs_update_msg_(WICED_TRUE, subs_to_add, &subs_list[subs_added], retransmit);

    return WICED_TRUE;
}

static void low_power_lost_friend_(void)
{
    // reset and save state
    lpn_state.state = LPN_STATE_IDLE;
    lpn_state.nvm.addr = MESH_NODE_ID_INVALID;
    low_power_write_config_int_();
    // start LPN logic via timer start just to handle all states in one place
    low_power_start_timer_(&lpn_state.delay_timer, 100);
}

/**
* Called by transport layer when bearer is idle (no active advertisments)
* Goes to sleep if LPN is idle
*
* Parameters:   None
*
* Return:   None
*/
void low_power_sleep_if_idle(void)
{
    TRACE1(TRACE_INFO, "low_power_sleep_if_idle: state:%d\n", lpn_state.state);
    if (lpn_state.state == LPN_STATE_ESTABLISHED)
        low_power_sleep_(lpn_config.poll_timeout * 50);
}

/**
* Called by transport layer when it received unsegmented access message or segment of the access message.
*
* Parameters:   None
*
* Return:   TRUE - it is expected and can be passed to access layer.
*           FALSE - it is unxepeted and should be dropped.
*/
wiced_bool_t low_power_on_msg_from_queue(void)
{
    TRACE1(TRACE_INFO, "low_power_on_msg_from_queue: state:%d\n", lpn_state.state);
    // Reject unexpected message
    if (lpn_state.state != LPN_STATE_LISTEN_RESPONSE)
        return WICED_FALSE;
    //toggle the Friend Sequence Number and remember it in the NVM
    lpn_state.nvm.fsn ^= FRIENDSHIP_FRIEND_POLL_FSN_FLAG;
    low_power_write_config_int_();
    return WICED_TRUE;
}

void low_power_on_after_handle_msg_from_queue(void)
{
    TRACE1(TRACE_INFO, "low_power_on_after_handle_msg_from_queue: state:%d\n", lpn_state.state);
    lpn_state.repeats = LPN_POLL_REPEATS;
    low_power_send_poll_(lpn_state.nvm.addr, LPN_STATE_POLL_SENT);
}

/**
* Sends Friend Clear message which terminates the friendship.
*
* Parameters:   None
*
* Return:   None
*/
void low_power_send_friend_clear(void)
{
    uint8_t                 buf[16];
    uint16_t                len;
    TRACE1(TRACE_INFO, "low_power_send_friend_clear: state:%d\n", lpn_state.state);
    // Ignore message if friendship isn't established
    if (lpn_state.state < LPN_STATE_ESTABLISHED)
        return;
    len = friend_crt_clear(node_nv_data.node_id, lpn_state.nvm.lpn_counter, buf);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 3, 1, 0, 0, 0);
#endif
    // use master secure material
    transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_CLEAR, buf, len, lpn_state.nvm.net_key_idx, NULL, node_nv_data.node_id, lpn_state.nvm.addr, MESH_MAX_TTL);
}

wiced_bool_t low_power_send_subslist_update(wiced_bool_t add, uint16_t addr)
{
    TRACE3(TRACE_WARNING, "low_power_send_subslist_update: state:%d add:%d addr:%x\n", lpn_state.state, add, addr);
    if (lpn_state.state != LPN_STATE_POLL_TIMEOUT
        || addr == MESH_NODE_ID_INVALID || addr == MESH_FIXED_GROUP_ALL_NODES || (addr & MESH_NON_UNICAST_MASK) == 0)
            return WICED_FALSE;

    lpn_state.subs_addr = addr;    // we are sending single add/remove
    lpn_state.subs_add = add;
    low_power_send_subs_update_msg_(lpn_state.subs_add, 1, &lpn_state.subs_addr, WICED_FALSE);

    return WICED_TRUE;
}

