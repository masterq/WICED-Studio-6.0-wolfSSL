/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Network layer implementation.
 */
#include <stdio.h> 
#include <string.h> 
#include "platform.h"

#include "mesh_core.h"
#include "core_aes_ccm.h"
#include "core_ovl.h"
#include "mesh_util.h"
#include "transport_layer.h"
#include "key_refresh.h"
#include "network_layer_int.h"
#include "low_power.h"
#include "friend.h"
#include "network_layer.h"
#include "foundation.h"
#include "foundation_int.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__NETWORK_LAYER_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__NETWORK_LAYER_C
#include "mesh_trace.h"

static wiced_bool_t send_filter_status_(uint8_t net_key_idx)
{
    uint8_t         opcode = WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_STATUS;
    uint8_t         params[3];

    TRACE3(TRACE_DEBUG, "send_filter_status_: type:%d cnt:%d net_key_idx:%x\n", ctx.proxy_out_flt.type, ctx.proxy_out_flt.cnt, net_key_idx);

    params[0] = ctx.proxy_out_flt.type;
    UINT16TOBE2(&params[1], (uint16_t)ctx.proxy_out_flt.cnt);

#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0xff, 0, 0, 0, 0, 0);
#endif
    // use master secure material
    return network_layer_send(&opcode, 1, params, (uint8_t)sizeof(params), net_key_idx, NULL, WICED_TRUE, node_nv_data.node_id, MESH_NODE_ID_INVALID, 0, 0xffffffff, NULL, NULL);
}

// adds address to the filter
static wiced_bool_t add_filter(uint16_t addr)
{
    wiced_bool_t ret = WICED_TRUE;

    TRACE2(TRACE_DEBUG, "add_filter: type:%d addr:%x filter_list:\n", ctx.proxy_out_flt.type, addr);
    TRACEN(TRACE_DEBUG, (char*)ctx.proxy_out_flt.addr, ctx.proxy_out_flt.cnt * 2);

    // ignore address if it is in the filter already
    if (mesh_find_filter_addr_(addr, NULL))
        return WICED_TRUE;

    // make sure filter has room to add new address
    if (ctx.proxy_out_flt.cnt >= sizeof(ctx.proxy_out_flt.addr) / sizeof(ctx.proxy_out_flt.addr[0]))
    {
        TRACE0(TRACE_DEBUG, "add_filter: failed - no room\n");
        return WICED_FALSE;
    }

    // add that address to the filter
    ctx.proxy_out_flt.addr[ctx.proxy_out_flt.cnt++] = addr;

    return WICED_TRUE;
}

// deletes address from the filter
static void del_filter(uint16_t addr)
{
    uint8_t idx;

    TRACE2(TRACE_DEBUG, "del_filter: type:%d addr:%x filter_list:\n", ctx.proxy_out_flt.type, addr);
    TRACEN(TRACE_DEBUG, (char*)ctx.proxy_out_flt.addr, ctx.proxy_out_flt.cnt * 2);

    // ignore address if it is not in the filter
    if (!mesh_find_filter_addr_(addr, &idx))
        return;

    // remove that address from the filter
    ctx.proxy_out_flt.cnt--;
    if (idx < ctx.proxy_out_flt.cnt)
        memcpy(&ctx.proxy_out_flt.addr[idx], &ctx.proxy_out_flt.addr[idx + 1], (ctx.proxy_out_flt.cnt - idx) * sizeof(ctx.proxy_out_flt.addr[0]));
}

static wiced_bool_t handle_config_msg_(uint8_t opcode, const uint8_t *params, uint32_t params_len, uint8_t net_key_idx)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t         i;

    TRACE2(TRACE_DEBUG, "handle_config_msg_: opcode:%x net_key_idx:%x params:\n", opcode, net_key_idx);
    TRACEN(TRACE_DEBUG, (char*)params, params_len);

    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_SET_TYPE:
        if (params_len != 1 || (*params != MESH_FLT_TYPE_WHITE_LIST && *params != MESH_FLT_TYPE_BLACK_LIST))
            break;
        // set the proxy filter type as requested in the message parameter, and clear the proxy filter list.
        ctx.proxy_out_flt.type = *params;
        ctx.proxy_out_flt.cnt = 0;
        send_filter_status_(net_key_idx);
        ret = WICED_TRUE;
        break;
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_ADD_ADDR:
        if (params_len < 2 || (params_len % 2) != 0)
            break;
        // go through each address in the parameters
        for (i = 0; i < params_len; i += 2)
        {
            if (!add_filter(BE2TOUINT16(&params[i])))
                break;
        }
        // on success send status
        if (i < params_len)
            break;
        send_filter_status_(net_key_idx);
        ret = WICED_TRUE;
        break;
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_DEL_ADDR:
        if (params_len < 2 || (params_len % 2) != 0)
            break;
        // go through each address in the parameters
        for (i = 0; i < params_len; i += 2)
        {
            del_filter(BE2TOUINT16(&params[i]));
            break;
        }
        send_filter_status_(net_key_idx);
        ret = WICED_TRUE;
        break;
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_STATUS:
        if (params_len != 3)
            break;
        //foundation_received_msg_callback(0, 0xffff, 0xffff, node_nv_data.node_id, 0xff, 0, opcode, params, params_len);
        {
            wiced_bt_mesh_event_t   *p_event;
            if ((p_event = (wiced_bt_mesh_event_t*)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_event_t))) != NULL)
            {
                p_event->element_idx = 0;
                p_event->company_id = 0xffff;
                p_event->model_id = 0xffff;
                p_event->opcode = opcode;
                p_event->ttl = 0xff;
                p_event->src = node_nv_data.node_id;
                p_event->dst = 0;
                p_event->app_key_idx = net_key_idx;
                p_event->reply = WICED_FALSE;
                if (!foundation_received_msg_callback(p_event, params, params_len))
                    wiced_bt_free_buffer(p_event);
            }
        }
        ret = WICED_TRUE;
        break;
    }

    TRACE1(TRACE_DEBUG, "handle_config_msg_: returns %d\n", ret);
    return ret;
}

/**
* Handles mesh packet received via GATT or ADV
*
* Parameters:
*   msg_data:   Packet to handle.
*   msg_len:    Length of the packet to handle
*   rssi:       RSSI of the received packet. #BTM_INQ_RES_IGNORE_RSSI if not valid.
*   type:       Message type. Can be any of NETWORK_LAYER_MSG_TYPE_XXX
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*
*/
wiced_bool_t network_layer_handle(const uint8_t *msg_data, uint32_t msg_len, uint8_t rssi, uint32_t type)
{
    uint8_t         res;
    uint8_t         net_key_idx;
    uint8_t         out_msg[ADV_LEN_MAX + 2];
    uint8_t         *msg = &out_msg[2];
    uint8_t         iv_index[MESH_IV_INDEX_LEN];
    uint8_t         mic_len;
    uint16_t        dst_id;
    uint16_t        src_id;
    wiced_bool_t    ctl;
    uint8_t         ttl;
    int             friend_idx = -2;    //-2 - none;-1 - LPN; >=0 - friend index
    uint32_t        seq;


    if(!network_layer_handle_check_len_(msg_data, msg_len))
        return WICED_FALSE;

    if (!mesh_net_decrypt(type == NETWORK_LAYER_MSG_TYPE_PROXY ? WICED_TRUE : WICED_FALSE, msg_data, (uint8_t)msg_len, msg, iv_index, &net_key_idx))
    {
        // Only ADV bearer can be secured by friend sec material
        if (type != NETWORK_LAYER_MSG_TYPE_ADV)
            return WICED_FALSE;
        else
        {
            friend_idx = friend_net_decrypt(msg_data, (uint8_t)msg_len, msg, iv_index, &net_key_idx);
            if (friend_idx < 0)
            {
                if (!low_power_net_decrypt(msg_data, (uint8_t)msg_len, msg, iv_index, &net_key_idx))
                    return WICED_FALSE;
                else
                    friend_idx = -1;
            }
        }
    }

    TRACE4(TRACE_INFO, "network_layer_handle: rssi:%x type:%d net_key_idx:%d friend_idx:%d net enc msg: dec msg:\n", rssi, type, net_key_idx, friend_idx);
    TRACEN(TRACE_INFO, (char*)msg_data, msg_len);

    // Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits)) || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
    src_id = BE2TOUINT16(msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_PKT_SEQ_LEN);
    // Drop packet with unassigned SRC(0) or if we originated it
    if (src_id == MESH_NODE_ID_INVALID
        || (src_id >= node_nv_data.node_id && src_id < (node_nv_data.node_id + fnd_static_config_.elements_num))
        || (src_id & MESH_NON_UNICAST_MASK) != 0)                                       // or src is non-unicast
    {
        TRACE1(TRACE_INFO, " Ignore invalid src:%x\n", src_id);
        return 0;
    }
    seq = BE3TOUINT32(msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN);
#ifndef MESH_CONTROLLER
    // check the cache if we didn't receive yet the packet from that SRC with the same SEQ
    if (!auth_cache_valid(src_id, seq, iv_index[MESH_IV_INDEX_LEN - 1]))
    {
        // drop that packet
        return 0;
    }
#endif

    res = network_layer_handle_parse_(msg, msg_len, type == NETWORK_LAYER_MSG_TYPE_GATT || type == NETWORK_LAYER_MSG_TYPE_PROXY, friend_idx, &mic_len, src_id, &dst_id, &ctl, &ttl);
    TRACEN(TRACE_INFO, (char*)msg, msg_len - mic_len);

    // upon receiving a valid Mesh message from the Proxy Client
    if (type == NETWORK_LAYER_MSG_TYPE_GATT || type == NETWORK_LAYER_MSG_TYPE_PROXY)
    {
        // If the proxy filter is a white list filter
        if (ctx.proxy_out_flt.type == MESH_FLT_TYPE_WHITE_LIST)
            // add the unicast address contained in the SRC field of the message to the white list
            add_filter(src_id);
        // If the proxy filter is a black list filter
        else
            // remove the unicast address contained in the SRC field of the message from the black list.
            del_filter(src_id);
    }

    // If it is proxy configuration message
    if (type == NETWORK_LAYER_MSG_TYPE_PROXY)
    {
        msg += MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN;
        msg_len -= MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN + mic_len;
        // check requirenments on ctl, ttl and dst
        if (!ctl || ttl != 0 || dst_id != MESH_NODE_ID_INVALID || msg_len == 0)
        {
            TRACE4(TRACE_DEBUG, "network_layer_handle: invalid proxy config message. ctl:%x ttl:%x msg_len:%d dst_id:%x\n", ctl, ttl, msg_len, dst_id);
            return WICED_FALSE;
        }
        handle_config_msg_(*msg, msg + 1, msg_len - 1, net_key_idx);
        return WICED_TRUE;
    }

    // Drop packet if dst is Proxibited
    if (dst_id == MESH_NODE_ID_INVALID)
    {
        TRACE0(TRACE_INFO, "network_layer_handle: Ignore non-assigned dst\n");
        return 0;
    }

    // retransmit if it should be retransmitted
    if ((res & 1) != 0)
    {
        uint8_t         out_msg2[ADV_LEN_MAX + 2];
        uint8_t         *msg2 = &out_msg2[2];

        memcpy(msg2, msg, msg_len - mic_len);
        msg2[MESH_IVI_NID_LEN] = (ctl ? MESH_CTL_MASK : 0) | (ttl - 1);

        // The IV Index used when retransmitting the message shall be the same as the IV Index when it was received.
        if (mesh_ccm_encrypt_net(&node_nv_net_key[net_key_idx].sec_material, MESH_NONCE_TYPE_NET, iv_index, msg2, msg_len - mic_len))
        {
#ifndef MESH_CONTROLLER
            mesh_set_adv_params(0xff, (node_nv_data.state_relay_retrans >> 5) + 1, ((uint16_t)(node_nv_data.state_relay_retrans & 0x1f) + 1) * 16, 0, 0, 0);
#endif
            mesh_send_message(dst_id, msg2, (uint8_t)msg_len, type == NETWORK_LAYER_MSG_TYPE_ADV ? WICED_FALSE : WICED_TRUE, NULL, NULL);
        }
    }

    // Pass it to transport layer if message is targeted to our device or friend LPN
    if ((res & 0x6) != 0)
    {
        transport_layer_handle(ctl, msg, (uint8_t)(msg_len - mic_len), iv_index, net_key_idx, ttl, rssi, res, friend_idx);
    }

    TRACE0(TRACE_INFO, "network_layer_handle: exits\n");
    return WICED_TRUE;
}

/**
* Creates mesh packet, encrypts/authenticates with network encr_key.
* Called by transport layer to sends transport payload.
* Network layer should implement that function
*
* Parameters:
*   ctrl_field:         Transport control field
*   ctrl_field_len:     Length of the Transport control field
*   app_payload:        Application payload or segment to send.
*   app_payload_len:    Length of the application payload or segment to send
*   net_key_idx:        Index if the network key
*   p_frnd_sec:         Use that friendship secure material. On NULL use master secure material.
*   ctl:                CTL flag value.
*   dst:                Destination address.
*   ttl:                Message TTL.
*   seq:                SEQ for the transmitting message. 0xffffffff means use own SEQ(node_nv_data.sequence_number)
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:            Mesh event to be used in complete_callback
*
* Return:   WICED_TRUE if send started successfully. WICED_FALSE - failed to start sending.
*/
wiced_bool_t network_layer_send(const uint8_t* ctrl_field, uint8_t ctrl_field_len, const uint8_t* app_payload, uint8_t app_payload_len,
    uint8_t net_key_idx, MeshSecMaterial *p_frnd_sec, wiced_bool_t ctl, uint16_t src, uint16_t dst, uint8_t ttl, uint32_t seq,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    wiced_bool_t              ret;
    MeshSecMaterial           *p_sec_material;
    uint8_t                   *msg;
    uint32_t                  msg_len;
    uint8_t                   out_msg[ADV_LEN_MAX + 2];
    uint8_t                   nonce_type;
    uint8_t                   dst_type = 0;
    uint8_t                   net_iv_index[MESH_IV_INDEX_LEN];
    uint8_t                   *p_net_iv_index;

    TRACE4(TRACE_DEBUG,"network_layer_send: net_key_idx:%x ctl:%x ttl:%x p_frnd_sec!=NULL:%d\n", net_key_idx, ctl, ttl, p_frnd_sec!=NULL);
    TRACE2(TRACE_DEBUG, " src:%x dst:%x\n", src, dst);
    TRACE1(TRACE_DEBUG, " seq:%x\n", seq);
    TRACE1(TRACE_DEBUG, " p_event:%x\n", (uint32_t)p_event);
    TRACE1(TRACE_DEBUG, " complete_callback:%x\n", (uint32_t)complete_callback);

    if (ctrl_field_len < MESH_TLCP_MIN_LEN || ctrl_field_len > MESH_TLCP_MAX_LEN
        || app_payload_len > MESH_LOWER_TRANSPORT_PDU_MAX_LEN)
    {
        TRACE2(TRACE_INFO, " ignore wrong packet. ctrl_len:% pdu_len:%d\n", ctrl_field_len, app_payload_len);
        return WICED_FALSE;
    }

    if (net_key_idx >= MESH_NET_KEY_MAX_NUM || (node_nv_data.net_key_bitmask & (1 << net_key_idx)) == 0)
    {
        TRACE0(TRACE_INFO, " no net key_id\n");
        return WICED_FALSE;
    }

    // if that network key is in the phase 2
    if ((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) == (KEY_REFRESH_PHASE_STATE_2 << MESH_GL_KEY_IDX_KR_PHASE_SHIFT))
    {
        // use new key
        uint8_t new_key_idx;
        if (find_netkey_((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK) | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &new_key_idx))
            net_key_idx = new_key_idx;
    }
    
    // if friendship or master sec material
    if (p_frnd_sec)
        p_sec_material= p_frnd_sec;
    else
        p_sec_material = &node_nv_net_key[net_key_idx].sec_material;

    // leave two bytes for header
    msg = &out_msg[2];
    network_layer_send_prepare_(ctrl_field, ctrl_field_len, app_payload, app_payload_len, p_sec_material, ctl, src, dst, ttl, seq, msg, &msg_len);

#ifndef MESH_CONTROLLER
    //Check if this message is targeted to our device only (no retransmittions and no friend LPNs)
    if (dst >= node_nv_data.node_id && dst < (node_nv_data.node_id + fnd_static_config_.elements_num))
    {
        dst_type = 2;
    }
    // If dst is unicast
    else if ((dst & MESH_NON_UNICAST_MASK) == 0)
    {
        // if it is not under friend sec and not control message from ourself and dst is our friend LPN then message goes to LPN cache
        if (p_frnd_sec == NULL && !(ctl && src == node_nv_data.node_id) && friend_find_lpn(dst, NULL))
            dst_type |= 4;
        // otherwise transmit
        else
            dst_type |= 1;
    }
    else
    {
        // dst isn't unicast
        // retransmit
        dst_type |= 1;
        // check if it is targeted to our friend LPN
        if (friend_does_go_to_lpn(src, dst))
            dst_type |= 4;
        // it is targeted to our device if it is broadcast message
        if (dst == MESH_FIXED_GROUP_ALL_NODES
            || (dst == MESH_FIXED_GROUP_ALL_PROXIES && node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)  // or all_proxies and proxy is on
            || (dst == MESH_FIXED_GROUP_ALL_FRIENDS && friend_get_state() == FRIEND_STATE_ENABLED)           // or all friends and friend feature is enabled
            || (dst == MESH_FIXED_GROUP_ALL_RELAYS && node_nv_data.state_relay == FND_STATE_RELAY_ON)        // or all relays and relay feature is enabled
            || ((dst & MESH_NON_UNICAST_MASK) != 0 && network_layer_find_subs_(dst)))                     // or non-unicast address (group or virtual) and we have subscription for it
        {
            dst_type |= 2;
        }
    }
#else
    dst_type = 1;
#endif
    TRACE3(TRACE_WARNING, " elements_num:%d returns:%x features:%x\n", fnd_static_config_.elements_num, dst_type, fnd_static_config_.features);

    if(node_nv_data.iv_update_state != MESH_IV_UPDT_STATE_ACTIVE)
        p_net_iv_index = node_nv_data.net_iv_index;
    else
    {
        UINT32 iv_index = BE4TOUINT32(node_nv_data.net_iv_index) - 1;
        UINT32TOBE4(net_iv_index, iv_index);
        p_net_iv_index = net_iv_index;
    }

    // Pass it to transport layer if message is targeted to our device or friend LPN
    if ((dst_type & 0x6) != 0)
    {
        transport_layer_handle(ctl, msg, (uint8_t)msg_len, p_net_iv_index, net_key_idx, ttl, BTM_INQ_RES_IGNORE_RSSI, dst_type, -2);
    }

    // Don't send if it is not targeted to other devices
    if ((dst_type & 0x1) == 0)
    {
        return WICED_TRUE;
    }

    // 0 dst and TRUE in ctl and 0 in ttl means it is Proxy Configuration Message
    nonce_type = (dst == MESH_NODE_ID_INVALID && ctl && ttl == 0 && p_frnd_sec == NULL) ? MESH_NONCE_TYPE_PROXY : MESH_NONCE_TYPE_NET;

    if (!mesh_ccm_encrypt_net(p_sec_material, nonce_type, p_net_iv_index, msg, msg_len))
    {
        return WICED_FALSE;
    }
    msg_len += ctl ? MESH_MIC_LEN2 : MESH_MIC_LEN;

    TRACE0(TRACE_INFO, "network_layer_send: encrypted msg\n");
    TRACEN(TRACE_INFO, (char*)msg, msg_len);

    ret = mesh_send_message(dst, msg, (uint8_t)msg_len, WICED_FALSE, complete_callback, p_event);
    TRACE1(TRACE_DEBUG, "network_layer_send: returns %d\n", ret);
    return ret;
}


/**
* Creates mesh packet, encrypts/authenticates with network encr_key.
* Called by transport layer to sends transport payload.
* Network layer should implement that function
*
* Parameters:
*   ctrl_field:         Transport control field four bytes length. Before calling network_layer_send for each segment
*                       copy 5 bits of the segment index to the LSB bits of the second byte ctrl_field[1]
*   app_payload:        Application payload to send.
*   app_payload_len:    Length of the application payload to send includin MIC
*   segLst:             Last sent segment. Start sending from next segment
*   blockAck:           Bits of acknowledged segments
*   net_key_idx:        Index if the network key
*   p_frnd_sec:         friendship secure material to use. NULL means use master secure material.
*   ctl:                CTL flag value.
*   src:                Source address (address of originating element)
*   dst:                Destination address.
*   ttl:                Message TTL.
*   complete_callback:  Callback function to be called at the end of all retransmissions.
*   p_event:            Mesh event to be used in complete_callback
*
* Return:       Last sent segment
*/
uint8_t network_layer_send_segments(uint8_t* ctrl_field, uint8_t* app_payload, uint16_t app_payload_len, uint8_t segLst, uint32_t blockAck,
    uint8_t net_key_idx, MeshSecMaterial *p_frnd_sec, wiced_bool_t ctl, uint16_t src, uint16_t dst, uint8_t ttl,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    uint8_t   ret = 0xff;
    uint8_t   seg, segO, segN = (app_payload_len - 1) / MESH_MAX_APP_PAYLOAD_SEGMENT_LEN;
    uint32_t  seg_mask;

    TRACE1(TRACE_DEBUG, "network_layer_send_segments: blockAck:%x\n", blockAck);
    TRACE4(TRACE_DEBUG, " segLst:%d net_key_idx:%x ctl:%d ttl:%d\n", segLst, net_key_idx, ctl, ttl);
    TRACE2(TRACE_DEBUG, " src:%x dst:%x ctrl, payload\n", src, dst);
    TRACEN(TRACE_DEBUG, (char*)ctrl_field, 4);
    TRACEN(TRACE_DEBUG, (char*)app_payload, app_payload_len);

    // go through each segment
    for (seg = 0; seg <= segN; seg++)
    {
        segO = (seg + segLst + 1) % (segN + 1);
        // skip already acknowledged segments
        seg_mask = 1 << segO;
        if ((blockAck & seg_mask) != 0)
            continue;

        // update SegO in the control field
        ctrl_field[2] = (ctrl_field[2] & 0xfc) | (segO >> 3);
        ctrl_field[3] = (ctrl_field[3] & 0x1f) | (segO << 5);
        TRACE2(TRACE_INFO, "network_layer_send_segments: segO:%d segN:%d\n", segO, segN);

        // send segment and stop sending (exit loop) on error
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0, 0, 0, 0, dst, seg_mask);
#endif
        if (!network_layer_send(ctrl_field, 4,
            &app_payload[segO * MESH_MAX_APP_PAYLOAD_SEGMENT_LEN],
            segO < segN ? MESH_MAX_APP_PAYLOAD_SEGMENT_LEN : ((app_payload_len - 1) % MESH_MAX_APP_PAYLOAD_SEGMENT_LEN) + 1,
            net_key_idx, p_frnd_sec, ctl, src, dst, ttl, 0xffffffff, complete_callback, p_event))
            break;
        ret = segO;
    }
    if (ret == 0xff)
        ret = segLst;
    TRACE1(TRACE_DEBUG, "network_layer_send_segments: returns %d\n", ret);
    return ret;
}


