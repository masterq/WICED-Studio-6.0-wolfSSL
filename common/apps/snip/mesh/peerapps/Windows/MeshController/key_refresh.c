/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
/** @file
 *
 * Mesh key refresh procedure implementation.
 */
#include <stdio.h> 
#include <string.h> 

#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_ovl.h"
#include "core_aes_ccm.h"

#include "mesh_util.h"

#include "key_refresh.h"
#include "access_layer.h"

#include "foundation_int.h"
#include "foundation.h"
#include "transport_layer.h"
#include "low_power.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__KEY_REFRESH_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__KEY_REFRESH_C
#include "mesh_trace.h"

static void key_refresh_handle_phase_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id);
static void key_refresh_handle_phase_set_(const uint8_t *params, uint16_t params_len, uint16_t src_id, wiced_bool_t *update_beacon);
void key_refresh_send_phase_status(uint8_t status, uint16_t idx, uint8_t phase, uint16_t src_id);

// returns WICED_TRUE if beacon has to be updated
static wiced_bool_t key_refresh_start_phase_2_(uint8_t old_net_key_idx, wiced_bool_t *p_prohibited)
{
    wiced_bool_t    update_beacon = WICED_FALSE;
    wiced_bool_t    prohibited = WICED_FALSE;
    uint8_t         phase;
    TRACE2(TRACE_INFO, "key_refresh_start_phase_2_: old_net_key_idx:%x old-global_idx:%x\n", old_net_key_idx, node_nv_net_key[old_net_key_idx].global_idx);
    phase = (node_nv_net_key[old_net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT;
    // if old key is in the phase 1 (just has been updated) then start phase 2
    if (phase == KEY_REFRESH_PHASE_STATE_1)
    {
        // just update phase in the old key, save it in the NVM and update the beacon
        node_nv_net_key[old_net_key_idx].global_idx = (node_nv_net_key[old_net_key_idx].global_idx & ~MESH_GL_KEY_IDX_KR_PHASE_MASK) | (KEY_REFRESH_PHASE_STATE_2 << MESH_GL_KEY_IDX_KR_PHASE_SHIFT);
        saveNodeNvNetKey(old_net_key_idx);
        low_power_kr_set_phase_2();
        update_beacon = WICED_TRUE;
    }
    else if(phase != KEY_REFRESH_PHASE_STATE_2)
        prohibited = WICED_TRUE;

    if (p_prohibited)
        *p_prohibited = prohibited;

    return update_beacon;
}

// returns WICED_TRUE if beacon has to be updated
static wiced_bool_t key_refresh_finish_(uint8_t old_net_key_idx, uint8_t new_net_key_idx)
{
    wiced_bool_t update_beacon = WICED_FALSE;
    uint8_t new_app_idx, old_app_idx, phase;
    TRACE3(TRACE_INFO, "key_refresh_finish_: new_net_key_idx:%x old_net_key_idx:%x old-global_idx:%x\n", new_net_key_idx, old_net_key_idx, node_nv_net_key[old_net_key_idx].global_idx);
    phase = (node_nv_net_key[old_net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT;
    // if old key is in the phase 2 or 1 then finish key refresh
    if (phase == KEY_REFRESH_PHASE_STATE_2 || phase == KEY_REFRESH_PHASE_STATE_1)
    {
        // copy new key to the old key location just to keep old local indexes
        memcpy(&node_nv_net_key[old_net_key_idx], &node_nv_net_key[new_net_key_idx], sizeof(node_nv_net_key[0]));
        // clean all KR flags
        node_nv_net_key[old_net_key_idx].global_idx &= MESH_GL_KEY_IDX_MASK;
        // save updated old key
        saveNodeNvNetKey(old_net_key_idx);

        low_power_kr_finish();

        // delete new key
        node_nv_data.net_key_bitmask &= ~(1 << new_net_key_idx);
        // go through each new app key
        for (new_app_idx = 0; new_app_idx < MESH_APP_KEY_MAX_NUM; new_app_idx++)
        {
            // ignore nonexistent key
            if ((node_nv_data.app_key_bitmask & (1 << new_app_idx)) == 0)
                continue;
            // we go through only new keys
            if ((node_nv_app_key[new_app_idx].global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG) == 0)
                continue;
            // ignore app key related to different net key
            if (node_nv_app_key[new_app_idx].global_net_key_idx != node_nv_net_key[old_net_key_idx].global_idx)
                continue;
            // find old app key related to that new one
            if (foundation_find_appkey_(node_nv_app_key[new_app_idx].global_idx & MESH_GL_KEY_IDX_MASK, &old_app_idx, NULL, NULL))
            {
                TRACE3(TRACE_INFO, " found new_app_idx:%x old_app_idx:%x old-global_idx:%x\n", new_net_key_idx, old_net_key_idx, node_nv_app_key[old_app_idx].global_idx);
                // copy new key to the old key location just to keep old local indexes
                memcpy(&node_nv_app_key[old_app_idx], &node_nv_app_key[new_app_idx], sizeof(node_nv_app_key[0]));
                // clean all KR flags
                node_nv_app_key[old_app_idx].global_idx &= MESH_GL_KEY_IDX_MASK;
                // save updated old key
                saveNodeNvAppKey(old_app_idx);
            }
            else
            {
                TRACE1(TRACE_INFO, " not found old app_key for new_app_idx:%x\n", new_net_key_idx);
            }
            // in any case delete new key even if old key isn't found
            node_nv_data.app_key_bitmask &= ~(1 << new_app_idx);
        }
        // save key bitmask in the NVM to save all deleted keys
        mesh_save_node_nv_data();

        // update net_key_idx in the transport layer controll blocks to use changed key index
        transport_layer_handle_kr_finish(new_net_key_idx, old_net_key_idx);

        update_beacon = WICED_TRUE;
    }
    return update_beacon;
}

/**
* Executes Key Refresh logic depending on the KR flag of the beacon (or friend update message) and current status of the network key
*
* Parameters:
*   kr:             Key Refresh flag
*   net_key_idx:    index of the network key
*
*   Return: WICED_TRUE - needs to update secure beacon
*/
wiced_bool_t key_refresh_handle_beacon(wiced_bool_t kr, uint8_t net_key_idx)
{
    wiced_bool_t ret = WICED_FALSE;
    uint8_t old_key_idx;
    uint16_t global_idx;

    global_idx = node_nv_net_key[net_key_idx].global_idx;

#ifdef _DEB_BEACON_EXTENDED_TRACE
    TRACE3(TRACE_DEBUG, "key_refresh_handle_beacon: kr:%d net_key_idx=%d global_idx=%d\n", kr, net_key_idx, global_idx);
#endif

    // ignore call if its key isn't new key of the active key refresh procedure
    if ((global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG) == 0)
        return WICED_FALSE;

    // find old net key
    if (!find_netkey_(global_idx & MESH_GL_KEY_IDX_MASK, &old_key_idx))
    {
        TRACE0(TRACE_INFO, "key_refresh_handle_beacon: no old key\n");
        return WICED_FALSE;
    }

    // if KR bit is set then start phase 2 on the old key if it is in the phase 1
    if (kr)
    {
        ret = key_refresh_start_phase_2_(old_key_idx, NULL);
    }
    // if KR bit is reset then finish KR if it is in the phase 2
    else
    {
        ret = key_refresh_finish_(old_key_idx, net_key_idx);
    }
    return ret;
}

/**
* Handles Key Refresh (KR) related packets
*
* Parameters:
*   src_id:         Address of the message source.
*   ttl:            TTL of the received packet
*   opcode:         opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*   params:         parameters of the app payload
*   params_len:     length of the parameters of the app payload
*   update_beacon:  variable to receive flag meaning beacon has to be updated
*
* Return:   WICED_TRUE - message handled; WICED_FALSE - try other handle.
*/
wiced_bool_t key_refresh_handle(uint16_t src_id, uint8_t ttl, uint16_t opcode, const uint8_t *params, uint16_t params_len, wiced_bool_t *update_beacon)
{
    wiced_bool_t    ret = WICED_TRUE;
    TRACE2(TRACE_INFO, "key_refresh_handle: ttl=%x src_id=%x\n", ttl, src_id);
    TRACE1(TRACE_INFO, " opcode=%x ***************************\n", opcode);
    if (params_len)
        TRACEN(TRACE_INFO, (char*)params, params_len);
    *update_beacon = WICED_FALSE;
    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET:
        key_refresh_handle_phase_get_(params, params_len, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET:
        key_refresh_handle_phase_set_(params, params_len, src_id, update_beacon);
        break;
    default:
        ret = WICED_FALSE;
    }
    return ret;
}

/**
* Checks key refresh timeouts and makes needed actions.
* It must be called by application once in each second
*
* Parameters:   None
*
* Return: None
*/
void key_refresh_timer()
{
/*
    if (p_node_kr)
    {
        // on timeout abort node KR procedure
        if (p_node_kr->to != 0 && --p_node_kr->to == 0)
        {
            TRACE0(TRACE_INFO, "key_refresh_timer: aborted\n");
        }
    }
*/
}

// message: NETKEY_IDX(2bytes)
static void key_refresh_handle_phase_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id)
{
    uint8_t idx, status, phase;
    uint16_t global_netkey_idx;
    // just ignore if length is wrong
    if (params_len != 2)
        return;
    global_netkey_idx = LE2TOUINT16(params);
    // find that netkey in the local array and make sure it is not a new KR key
    if (!find_netkey_(global_netkey_idx, &idx)
        || (node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG) != 0)
    {
        status = FND_STATUS_INVALID_NETKEY;
        phase = KEY_REFRESH_PHASE_STATE_NONE;
    }
    else
    {
        status = FND_STATUS_SUCCESS;
        phase = (node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT;
    }
    // send kr status message
    key_refresh_send_phase_status(status, global_netkey_idx, phase, src_id);
}

// message: NETKEY_IDX(2bytes), PHASE(1byte)
static void key_refresh_handle_phase_set_(const uint8_t *params, uint16_t params_len, uint16_t src_id, wiced_bool_t *update_beacon)
{
    uint8_t idx, status, phase, new_idx;
    uint16_t global_netkey_idx;
    wiced_bool_t    prohibited = WICED_FALSE;
    *update_beacon = WICED_FALSE;

    // just ignore if length is wrong
    if (params_len != 3)
        return;
    // get parameters
    global_netkey_idx = LE2TOUINT16(params);
    phase = params[2];

    // find that netkey in the local array (it will not find new KR key)
    if (!find_netkey_(global_netkey_idx, &idx))
    {
        status = FND_STATUS_INVALID_NETKEY;
        phase = KEY_REFRESH_PHASE_STATE_NONE;
    }
    else
    {
        // if it is command to start phase 2
        if (phase == KEY_REFRESH_PHASE_STATE_2)
        {
            *update_beacon = key_refresh_start_phase_2_(idx, &prohibited);
        }
        // if it is the command to finish key refresh procedure
        else if (phase == KEY_REFRESH_PHASE_STATE_3)
        {
            // set phase 2 is not prohibited for any state
            prohibited = WICED_FALSE;

            if (find_netkey_(global_netkey_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &new_idx))
            {
                *update_beacon = key_refresh_finish_(idx, new_idx);
            }
        }
        else
        {
            prohibited = WICED_TRUE;
        }

        status = FND_STATUS_SUCCESS;
        phase = (node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT;
    }
    // send kr status message
    if(!prohibited)
        key_refresh_send_phase_status(status, global_netkey_idx, phase, src_id);
}

void key_refresh_send_phase_status(uint8_t status, uint16_t idx, uint8_t phase, uint16_t src_id)
{
    uint8_t   buf[2 + 1 + 2 + 1];
    uint16_t len;
    TRACE3(TRACE_INFO, "key_refresh_send_phase_status: status:%x phase:%x idx:%d\n", status, phase, idx);
    len = key_refresh_crt_key_refresh_phase_status_msg(status, idx, phase, buf);
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, len, 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

/**
* Creates the application payload for message key refresh phase get
*
* Parameters:
*   net_idx:    netkey index to get key refresh state
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t key_refresh_crt_key_refresh_phase_get_msg(uint16_t net_idx, uint8_t *buf)
{
    uint8_t *p;
    p = op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET);
    UINT16TOLE2(p, net_idx);
    p += 2;
    TRACE0(TRACE_INFO, "key_refresh_crt_key_refresh_phase_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, p - buf);
    return (uint16_t)(p - buf);
}

/**
* Creates the application payload for message key refresh phase set
*
* Parameters:
*   net_idx:    netkey index to set key refresh phase
*   phase:      phase to set
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t key_refresh_crt_key_refresh_phase_set_msg(uint16_t net_idx, uint8_t phase, uint8_t *buf)
{
    uint8_t *p;
    p = op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET);
    UINT16TOLE2(p, net_idx);
    p += 2;
    *p++ = phase;
    TRACE0(TRACE_INFO, "key_refresh_crt_key_refresh_phase_set_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, p - buf);
    return (uint16_t)(p - buf);
}

/**
* Creates the application payload for message key refresh phase status
*
* Parameters:
*   status:     Status to send
*   net_idx:    netkey index
*   phase:      phase
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t key_refresh_crt_key_refresh_phase_status_msg(uint8_t status, uint16_t net_idx, uint8_t phase, uint8_t *buf)
{
    uint8_t *p;
    p = op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_STATUS);
    *p++ = status;
    UINT16TOLE2(p, net_idx);
    p += 2;
    *p++ = phase;
    TRACE0(TRACE_INFO, "key_refresh_crt_key_refresh_phase_status_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, p - buf);
    return (uint16_t)(p - buf);
}

/**
* Checks if Key Refresh procedure is in the phase 2.
*
* Parameters:
*   net_key_idx:    Local index of the network key
*
* Return: WICED_TRUE if Key Refresh procedure is in the phase 2. Otherwise returns WICED_FALSE
*/
wiced_bool_t key_refresh_is_phase2(uint8_t net_key_idx)
{
    return ((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT) == KEY_REFRESH_PHASE_STATE_2 ? WICED_TRUE : WICED_FALSE;
}

/**
* Set net_key to the phase 2.
*
* Parameters:
*   net_key_idx:    Local index of the network key
*
* Return: WICED_TRUE - success. WICED_FALSE - failed
*/
wiced_bool_t key_refresh_set_phase2(uint8_t net_key_idx)
{
    wiced_bool_t ret = WICED_FALSE;
    if (node_nv_data.net_key_bitmask & (1 << net_key_idx))
    {
        // just update phase in the key and save it in the NVM
        node_nv_net_key[net_key_idx].global_idx = (node_nv_net_key[net_key_idx].global_idx & ~MESH_GL_KEY_IDX_KR_PHASE_MASK) | (KEY_REFRESH_PHASE_STATE_2 << MESH_GL_KEY_IDX_KR_PHASE_SHIFT);
        saveNodeNvNetKey(net_key_idx);
        low_power_kr_set_phase_2();
        ret = WICED_TRUE;
    }
    TRACE1(TRACE_INFO, "key_refresh_set_phase2: net_key_idx:%x\n", net_key_idx);
    return ret;
}
