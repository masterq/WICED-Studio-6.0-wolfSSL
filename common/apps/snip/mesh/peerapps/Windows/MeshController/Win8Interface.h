
// MeshControllerDlg.h : header file
//

#pragma once
#include "BtInterface.h"

class CBtWin8Interface : public CBtInterface
{
public:
    CBtWin8Interface (BLUETOOTH_ADDRESS *bth, HMODULE hLib, LPVOID NotificationContext);
    virtual ~CBtWin8Interface();

    BOOL Init();
    BOOL SetDescriptorValue(const GUID *p_guidServ, const GUID *p_guidChar, USHORT uuidDescr, BTW_GATT_VALUE *pValue);
    BOOL WriteCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar, BOOL without_resp, BTW_GATT_VALUE *pValue);

    BOOL FindChar(const GUID *p_guid, PBTH_LE_GATT_CHARACTERISTIC pOutChar);
    PBTH_LE_GATT_DESCRIPTOR FindDescr(const GUID *p_guidChar, USHORT uuidDescr);

    BOOL RegisterNotification(const GUID *p_guidServ, const GUID *p_guidChar);
    void PostNotification(BTW_GATT_VALUE *pValue, BLUETOOTH_GATT_VALUE_CHANGED_EVENT *pEvent, PVOID EventOutParameter, PVOID Context);

    BOOL m_bConnected;
private:
    HANDLE m_hDevice;

    //BTH_LE_GATT_SERVICE m_service;

    USHORT num_chars;
    BTH_LE_GATT_CHARACTERISTIC *m_pchar;

    USHORT *pnum_descr;
    BTH_LE_GATT_DESCRIPTOR **m_ppdescr;

    BLUETOOTH_GATT_EVENT_HANDLE m_pEventHandle, m_pEventHandle2, m_pEventHandle3, m_pEventHandle4;
};

