/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Transport layer implementation.
 */
#include <stdio.h> 
#include <string.h> 
#include "platform.h"
#include "mesh_core.h"
#include "core_ovl.h"
#include "mesh_util.h"
#include "access_layer.h"
#include "network_layer.h"
#include "foundation.h"
#include "low_power.h"
#include "friend.h"

#include "transport_layer.h"
#include "wiced_timer.h"
#ifndef MESH_CONTROLLER
#include "iv_updt.h"
#endif

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__TRANSPORT_LAYER_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__TRANSPORT_LAYER_C
#include "mesh_trace.h"

#define TL_SEQZERO_MASK     0x1FFF
#define TL_MAX_CBS 8
#ifndef MESH_CONTROLLER
#define TL_TRANS_TO_MS      400
#define TL_RETRANS          6
#else
#define TL_TRANS_TO_MS      1000
#define TL_RETRANS          3
#endif
#define TL_ACK_TO_MS        250
#define TL_INCOMPL_TO_MS    25000 //15000       //For receiver: incomplete timer in ms (abort if no messages during this time) 

#define TL_CP_SZMIC_MASK          0x80    // SZMIC flag

#define TL_CP_OBO_MASK          0x80    // OBO flag in octet 1 of the Segment Ack Message

#define TL_CB_FLAGS_OBO         0x01    //message receiving on behalf of a Low Power node

typedef struct
{
    uint16_t  seqZero;
    uint16_t  node_id;                  // sender: destination address of the message
                                        // receiver: source address of the message
    uint16_t  src;                      // sender: Source address. i.e. address of the element originating the AL message
                                        // resceiver: destination address of the message
    uint32_t  blockAck;
    uint16_t  len;                    // length of the all application payload
    uint16_t  to;                     // sender: remaining retransmit timeout in ms
                                      // receiver: remaining acknowledgement timeout
    uint16_t  retrans_or_incompl_to;  //sender: remaining retransmit counter
                                      //receiver: incomplete timer in ms (abort if no messages during this time)
    uint8_t   ctrl_or_0;              // sender: (!= 0) first byte of the message.
                                      // receiver: 0
    uint8_t   lst_sent_seq;           // sender: Last sent segment. 0xff means no one segment sent yet. We use it for optimisation of the retransmitions
    wiced_bt_mesh_core_send_complete_callback_t complete_callback;  // only for sender. Callback to be called at the end of message sending
    wiced_bt_mesh_event_t *p_event;     // only for sender. Mesh event related to the sending message for complete_callback
    uint8_t   net_key_idx;
    MeshSecMaterial *p_frnd_sec;        // friendship secure material to use. NULL means use master secure material
    uint8_t   ttl;                    //sender: TTL field
    uint8_t   szmic;                  // WICED_TRUE/WICED_FALSE means long(8bytes)/short(4bytes) access layer MIC
    uint8_t   ctrl_msg;               // sender: WICED_TRUE/WICED_FALSE means controll/access message
    uint8_t   flags;                    // some flags of the message. Can be any combination of TL_CB_FLAGS_XXX
    uint8_t   msg[1];                 // application payload //MESH_MAX_APP_PAYLOAD_LEN
} tl_cb_t;

// Static functions dforward declarartion
static void transport_layer_send_segments_(tl_cb_t *p_cb);
static wiced_bool_t transport_layer_send_ack_(tl_cb_t *p_cb);
static wiced_bool_t transport_layer_on_seg_(wiced_bool_t ctl, uint8_t flags, wiced_bool_t szmic, uint8_t segO, uint8_t segN, uint16_t seqZero,
    const uint8_t* payload, uint16_t payload_len, const uint8_t* iv_index, uint8_t net_key_idx,
    const uint8_t* seq_src_dst, uint8_t ttl, uint8_t res, int friend_idx);
static wiced_bool_t transport_layer_on_seg_ack_(uint8_t obo, uint16_t seqZero, uint16_t src, uint32_t blockAck);
static wiced_bool_t transport_layer_handle_control_message_(uint8_t op, uint8_t* payload, uint16_t payload_len, const uint8_t* iv_index, uint8_t net_key_idx, const uint8_t* seq_src_dst, uint8_t ttl, uint8_t rssi, int friend_idx);
static void transport_layer_del_cb_(uint32_t cb_idx);


tl_cb_t *tl_cb[TL_MAX_CBS];
uint8_t tl_cb_cnt = 0;

#ifdef _DEB_USE_STATIC_ALLOC_TL
static uint8_t s_tl_cb[sizeof(tl_cb_t)+MESH_MAX_APP_PAYLOAD_LEN + MESH_MIC_LEN2];
#endif

static wiced_timer_t    transport_layer_timer;  // timer for transport layer
static wiced_bool_t     transport_layer_timer_initialized = WICED_FALSE;
static wiced_bool_t     transport_layer_timer_started = WICED_FALSE;

static void transport_layer_timer_callback(uint32_t arg);

static void transport_layer_timer_start(void)
{
    TRACE2(TRACE_INFO, "transport_layer_timer_start: initialized:%d started:%d\n", transport_layer_timer_initialized, transport_layer_timer_started);
    if (!transport_layer_timer_initialized)
    {
        transport_layer_timer_initialized = WICED_TRUE;
        wiced_init_timer(&transport_layer_timer, transport_layer_timer_callback, 0, WICED_MILLI_SECONDS_PERIODIC_TIMER);
    }
    if (!transport_layer_timer_started)
    {
        transport_layer_timer_started = WICED_TRUE;
        wiced_start_timer(&transport_layer_timer, 100);
    }
}

static void transport_layer_timer_stop(void)
{
    TRACE1(TRACE_INFO, "transport_layer_timer_stop: started:%d\n", transport_layer_timer_started);
    if (transport_layer_timer_started)
    {
        transport_layer_timer_started = WICED_FALSE;
        wiced_stop_timer(&transport_layer_timer);
    }
}

/**
* Resets current ticks counter if resets is WICED_TRUE and returns miliseconds past since previously reset ticks counter
* On windows application should implement it
*
* Parameters:
*   reset:      WICED_TRUE - resets counter after returning the value
*
* Return:   None
*/
static uint16_t transport_layer_get_ticks_(wiced_bool_t reset)
{
    uint16_t ticks;
    static UINT64 time = 0;
    UINT64 current_time = wiced_bt_mesh_core_get_tick_count();
    // miliseconds past since previous reset
    ticks = (uint16_t)(current_time - time);
    // Resets current ticks counter if resets is WICED_TRUE
    if (reset)
        time = current_time;
    //TRACE2(TRACE_DEBUG, "get_ticks: reset:%d ticks:%d\n", reset, ticks);
    return ticks;
}

/**
* Finds control block by seqZero and peer node_id
*
* Parameters:
*   seqZero:    seqZero value
*   node_id:    peer node address
*
* Return:   index of the found control bloc. On error returns negative value (<0)
*/
static int transport_layer_find_cb_(uint16_t seqZero, uint16_t node_id)
{
    uint8_t i;
    for (i = 0; i < tl_cb_cnt; i++)
    {
        if (seqZero == tl_cb[i]->seqZero && node_id == tl_cb[i]->node_id)
            break;
    }
    return i < tl_cb_cnt ? i : -1;
}

/**
* Allocates new control block for seqZero and peer node_id
*
* Parameters:
*   seqZero:        seqZero value
*   node_id:        peer node address
*   payload_len:    payload length for buffer
*
* Return:   index of the found control bloc. On error returns negative value (<0)
*/
static int transport_layer_alloc_cb_(uint16_t seqZero, uint16_t node_id, uint16_t payload_len)
{
    int cb_idx = -1;
    if (tl_cb_cnt >= TL_MAX_CBS)
    {
        TRACE0(TRACE_INFO, "transport_layer_alloc_cb_: no available conn\n");
        transport_layer_del_cb_(0);
    }
#ifdef _DEB_USE_STATIC_ALLOC_TL
    //??? Lets forget allocated block in that mode to get rid of the one minute delay in block deletion
    cb_idx = 0;
    tl_cb_cnt = 1;
    tl_cb[cb_idx] = (tl_cb_t*)s_tl_cb;
#else
    // if it is for receiving segments
    if (seqZero & 0x8000)
    {
        // find receiving (0x8000) cb for the same src 
        for (cb_idx = 0; cb_idx < tl_cb_cnt; cb_idx++)
        {
            if ((tl_cb[cb_idx]->seqZero & 0x8000) != 0 && tl_cb[cb_idx]->node_id == node_id)
                break;
        }
        if (cb_idx >= tl_cb_cnt)
            cb_idx = -1;
    }
    // if no existing cb then allocate new one
    if (cb_idx < 0)
    {
        tl_cb[tl_cb_cnt] = (tl_cb_t*)wiced_memory_allocate(sizeof(tl_cb_t) + payload_len);
        if (tl_cb[tl_cb_cnt] != NULL)
            cb_idx = tl_cb_cnt++;
    }
#endif
    if (cb_idx >= 0)
    {
        memset(tl_cb[cb_idx], 0, sizeof(tl_cb_t));
        tl_cb[cb_idx]->seqZero = seqZero;
        tl_cb[cb_idx]->node_id = node_id;
        tl_cb[cb_idx]->len = payload_len;
        TRACE2(TRACE_WARNING, "transport_layer_alloc_cb_: idx:%d seqZero:%x\n", cb_idx, seqZero);
        // on first allocation reset ticks counter
        if (tl_cb_cnt == 1)
            transport_layer_get_ticks_(WICED_TRUE);

        // start transport periodic timer to handle segmentation logic. //ToDo - get rid of periodic timer
        transport_layer_timer_start();
    }
    else
    {
        TRACE1(TRACE_WARNING, "transport_layer_alloc_cb_: wiced_memory_allocate(%d) failed\n", payload_len);
    }
    return cb_idx;
}

/**
* Deletes control block
*
* Parameters:
*   cb_idx:     index of the control block to delete
*
* Return:   None
*/
static void transport_layer_del_cb_(uint32_t cb_idx)
{
    wiced_bt_mesh_core_send_complete_callback_t complete_callback = NULL;
    wiced_bt_mesh_event_t   *p_event = NULL;
    wiced_bool_t stopped_sending = WICED_FALSE;
    TRACE2(TRACE_WARNING, "transport_layer_del_cb_: idx:%d cnt:%d\n", cb_idx, tl_cb_cnt);
    if (cb_idx < tl_cb_cnt)
    {
        complete_callback = tl_cb[cb_idx]->complete_callback;
        p_event = tl_cb[cb_idx]->p_event;

        // detect if it is sending side stopped sending that message
        stopped_sending = tl_cb[cb_idx]->ctrl_or_0 ? WICED_TRUE : WICED_FALSE;
        TRACE1(TRACE_DEBUG, " stopped_sending:%d\n", stopped_sending);

#ifdef _DEB_USE_STATIC_ALLOC_TL
        if (cb_idx != 0)
            return;
#else
        wiced_memory_free(tl_cb[cb_idx]);
#endif
        tl_cb_cnt--;
        while (cb_idx++ < tl_cb_cnt)
        {
            tl_cb[cb_idx - 1] = tl_cb[cb_idx];
        }

        // if we stopped sending that message and there are other sending messages then we didn't stop sending
        if (stopped_sending)
            if (transport_layer_is_sending_segments())
                stopped_sending = WICED_FALSE;
    }
    // Stop transport periodic timer and exit if there is no any pending activity (no controll blocks)
    if (tl_cb_cnt == 0)
        transport_layer_timer_stop();
    
    // If callback is not NULL then call it.
    if (complete_callback)
    {
        TRACE1(TRACE_DEBUG, "transport_layer_del_cb_: calling callback:%x\n", (uint32_t)complete_callback);
        complete_callback(p_event);
    }
#ifndef MESH_CONTROLLER
    // if we stopped sending segments then notify iv_update logic to stop pending IV UPDATE procedure
    if (stopped_sending)
        iv_updt_segments_sent();
#endif
}

#ifndef MESH_CONTROLLER
/**
* Checks the cache if it is old message and updates cache
*
* Parameters:
*   iv_index:       IV index
*   src_id:         SRC field of received message
*   seq:            SEG field of received message
*
* Return: WICED_TRUE if we didn't receive yet the packet from that SRC with the same or bigger SEQ.
*
*/
static wiced_bool_t transport_layer_cache_valid_(const uint8_t* iv_index, uint16_t src_id, uint32_t seq)
{
    wiced_bool_t ret = WICED_FALSE;
    int cache_idx, empty_cache_idx = -1;
    // Use separate cache for each IV INDEX (current and previous)(see section 3.3.4.)
    // If LSB of the current IV index is not equal to the LSB of the selected IV index then we use previous IV index
    wiced_bool_t prev_iv_idx = node_nv_data.net_iv_index[MESH_IV_INDEX_LEN - 1] != iv_index[MESH_IV_INDEX_LEN - 1];
    MeshAppCache *p_app_cache = prev_iv_idx ? ctx.app_cache_prev : ctx.app_cache;
    // find cache item for SRC
    for (cache_idx = 0; cache_idx < MAX_APP_CACHE_SRC_NUM; cache_idx++)
    {
        if (src_id == p_app_cache[cache_idx].src)
        {
            break;
        }
        if (p_app_cache[cache_idx].src == 0 && empty_cache_idx < 0)
            empty_cache_idx = cache_idx;
    }
    // if we found cache item for that src_id then return WICED_TRUE or WICED_FALSE
    // return WICED_TRUE (valid) if SEQ of the received packet more then SEQ of found packet
    if (cache_idx < MAX_APP_CACHE_SRC_NUM)
    {
        TRACE3(TRACE_WARNING, "transport_layer_cache_valid_: found idx:%d prev_iv_idx:%d src:%x\n", cache_idx, prev_iv_idx, src_id);
        TRACE1(TRACE_WARNING, " seq:%x\n", seq);
        TRACE1(TRACE_WARNING, " cache_seq:%x\n", p_app_cache[cache_idx].seq);

        if (seq > p_app_cache[cache_idx].seq)
        {
            p_app_cache[cache_idx].seq = seq;
            ret = WICED_TRUE;
        }
    }
    else if (empty_cache_idx < 0)
    {
        // src is not found
        // if there is no room for new device then return WICED_FALSE 
        TRACE2(TRACE_INFO, "transport_layer_cache_valid_: not room for new device prev_iv_idx:%d src:%x\n", prev_iv_idx, src_id);
        TRACE1(TRACE_INFO, " seq:%x\n", seq);
    }
    else
    {
        // remember src_id and seq for new device and return WICED_TRUE
        TRACE3(TRACE_WARNING, "transport_layer_cache_valid_: new device idx:%d prev_iv_idx:%d src:%x\n", empty_cache_idx, prev_iv_idx, src_id);
        TRACE1(TRACE_WARNING, " seq:%x\n", seq);

        p_app_cache[empty_cache_idx].src = src_id;
        p_app_cache[empty_cache_idx].seq = seq;
        p_app_cache[empty_cache_idx].seq_auth = 0xffffffff;
        ret = WICED_TRUE;
    }
    // on success save changed app cache into NVRAM
    if (ret)
    {
        app_cache_save(prev_iv_idx);
    }
    return ret;
}

static wiced_bool_t transport_layer_cache_seq_auth(const uint8_t* iv_index, uint16_t src_id, uint32_t seq_auth, wiced_bool_t fst)
{
    wiced_bool_t    ret = WICED_FALSE;
    int             cache_idx;
    uint32_t        cache_seq_auth;

    // Use separate cache for each IV INDEX (current and previous)(see section 3.3.4.)
    // If LSB of the current IV index is not equal to the LSB of the selected IV index then we use previous IV index
    wiced_bool_t prev_iv_idx = node_nv_data.net_iv_index[MESH_IV_INDEX_LEN - 1] != iv_index[MESH_IV_INDEX_LEN - 1];
    MeshAppCache *p_app_cache = prev_iv_idx ? ctx.app_cache_prev : ctx.app_cache;
    
    TRACE3(TRACE_INFO, "transport_layer_cache_seq_auth: fst:%d prev_iv_idx:%d src:%x\n", fst, prev_iv_idx, src_id);
    TRACE1(TRACE_INFO, " seq_auth:%x\n", seq_auth);

    // find cache item for SRC
    for (cache_idx = 0; cache_idx < MAX_APP_CACHE_SRC_NUM; cache_idx++)
    {
        if (src_id == p_app_cache[cache_idx].src)
            break;
    }
    // if we found cache item for that src_id
    if (cache_idx < MAX_APP_CACHE_SRC_NUM)
    {
        cache_seq_auth = p_app_cache[cache_idx].seq_auth;
        TRACE1(TRACE_INFO, " cache_seq_auth:%x\n", cache_seq_auth);
        // if it is first segment - not under receiving
        if (fst)
        {
            // if no seg has been received for that SRC or just received seq_auth more than last received seq_auth 
            if (cache_seq_auth == 0xffffffff || seq_auth > cache_seq_auth )
            {
                // update SeqAuth and return TRUE
                p_app_cache[cache_idx].seq_auth = seq_auth;
                app_cache_save(prev_iv_idx);
                ret = WICED_TRUE;
            }
        }
        else
        {
            // we are receiving segments from that SRC
            // if it is same SeqAuth then return TRUE
            if (seq_auth == p_app_cache[cache_idx].seq_auth)
                ret = WICED_TRUE;
        }
    }
    return ret;
}
#endif

/**
* Handles transport payload
*
* Parameters:
*   ctl:            CTL flag from network PDU
*   msg:            all network message. This function may change the data in the payload.
*   msg_len:        Length of the network message
*   iv_index:       IV index
*   net_key_idx:    Index of the network key
*   ttl:            TTL of the received packet
*   rssi:           RSSI of the received packet. #BTM_INQ_RES_IGNORE_RSSI if not valid.
*   res:            bits: 0x01 - should be retransmitted; 0x02 - targeted to our device; 0x04 - targeted to our friend LPN
*   friend_idx:     -2 - network sec material;-1 - LPN sec material; >=0 - sec material of friend index
*
* Return:   WICED_TRUE if handled successfully. WICED_FALSE - failed to handle.
*/
wiced_bool_t transport_layer_handle(wiced_bool_t ctl, uint8_t* msg, uint8_t msg_len, const uint8_t* iv_index, uint8_t net_key_idx, uint8_t ttl, uint8_t rssi, uint8_t res, int friend_idx)
{
    wiced_bool_t    ret = WICED_FALSE;
    
    // pointer on the place in the packet with SEQ || SRC || DST
    uint8_t* seq_src_dst = msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN;
    // Transport payload. This function may change the data in the payload.
    uint8_t* payload = msg + (MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN);
    // Length of the transport payload
    uint8_t payload_len = msg_len - (MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN);
    
    uint16_t  src = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN);
    uint16_t  dst = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN);

    TRACE4(TRACE_DEBUG, "transport_layer_handle: ctl:%d net_key_idx:%x ttl:%x friend_idx:%d\n", ctl, net_key_idx, ttl, friend_idx);
    TRACEN(TRACE_DEBUG, payload, payload_len);

#ifndef MESH_CONTROLLER
    // ignore it if we already handled the message with the same or bigger seq from that SRC and update cache on success
    if (!transport_layer_cache_valid_(iv_index, src, BE3TOUINT32(seq_src_dst)))
        return WICED_FALSE;
#endif

    // if it is segmented message
    if (payload[0] & TL_CP_SEG_MASK)
    {
        // access message:   SEG[1](1bit) || AKF(1bit) || AID(6bits) || SZMIC(1bit)  || SeqZero(13bits) || SeqO(5bits) || SeqN(5bits) || PDU(1-12 bytes)
        // controll message: SEG[1](1bit) || OP(7bits)               || RFU[1](1bit) || SeqZero(13bits) || SeqO(5bits) || SeqN(5bits) || SegmentM(1-8 bytes)
        if (payload_len >= 4 + 1)
        {
            // If we are LPN and it is message from friend's queue then we have to check if this message is expected (it also updates Low Power's FSN)
            wiced_bool_t msg_from_queue = friend_idx == -1 && low_power_on_msg_from_queue();
            if (friend_idx != -1 || msg_from_queue)
            {
                ret = transport_layer_on_seg_(ctl,
                    payload[0],
                    (payload[1] & TL_CP_SZMIC_MASK) != 0,                               // SZMIC
                    ((payload[2] & 0x03) << 3) + (payload[3] >> 5),                     // SegO
                    payload[3] & 0x1f,                                                  // SegN
                    transport_layer_get_seq_zero_from_pdu(payload),                     // SeqZero
                    payload + 4, payload_len - 4,                                       // SegmentM
                    iv_index, net_key_idx, seq_src_dst, ttl, res, friend_idx);
                if (msg_from_queue)
                    low_power_on_after_handle_msg_from_queue();
            }
        }
    }
    // if it is unsegmented access message
    else if (!ctl)
    {
        // unsegmented access message: SEG[0](1bit) || AKF(1bit) || AID(6bits) || PDU(5-15 bytes)
        if (payload_len >= 1 + 5 && payload_len <= 1 + 15)
        {
            if ((res & 4) != 0 && ttl > 1)
            {
                friend_cache_put_msg(ctl, src, dst, payload, payload_len, ttl - 1, seq_src_dst, iv_index);
            }
            if ((res & 2) != 0)
            {
                // If we are LPN and it is message from friend's queue then we have to check if this message is expected (it also updates Low Power's FSN)
                wiced_bool_t msg_from_queue = friend_idx == -1 && low_power_on_msg_from_queue();
                if (friend_idx != -1 || msg_from_queue)
                {
                    ret = access_layer_handle(
                        (payload[0] & TL_CP_AKF_MASK) != 0,     // AKF
                        payload[0] & TL_CP_AID_MASK,            // AID
                        WICED_FALSE,                            // SZMIC - TransMIC shall be a 32-bit value, as if the SZMIC field has the value 0
                        payload + 1, payload_len - 1,           // encrypted app payload
                        iv_index, net_key_idx, seq_src_dst,
                        ttl, friend_idx);
                    if (msg_from_queue)
                        low_power_on_after_handle_msg_from_queue();
                }
            }
        }
    }
    // if it is segment acknowledge
    else if ((payload[0] & TL_CP_OPCODE_MASK) == 0)
    {
        // segment acknowledge message: SEG[0](1bit) || 0(7bits) || OBO(1bit) || SeqZero(13bits) || RFU(2bits) || BlockAck(32bits)
        if (payload_len == 7)
        {
            // if it is ack to our friend LPN
            if ((res & 4) != 0)
            {
                if(ttl > 1)
                    ret = friend_cache_put_msg(ctl, src, dst, payload, payload_len, ttl - 1, seq_src_dst, iv_index);
            }
            else
            {
                // If we are LPN and it is message from friend's queue then we have to check if this message is expected (it also updates Low Power's FSN)
                wiced_bool_t msg_from_queue = friend_idx == -1 && low_power_on_msg_from_queue();
                if (friend_idx != -1 || msg_from_queue)
                {
                    ret = transport_layer_on_seg_ack_(
                        (payload[1] & TL_CP_OBO_MASK) != 0,                             // OBO
                        transport_layer_get_seq_zero_from_pdu(payload),                 // SeqZero
                        src,                                                            // SRC
                        BE4TOUINT32(payload + 3));                                      // BlockAck (4 bytes)
                    if (msg_from_queue)
                        low_power_on_after_handle_msg_from_queue();
                }
            }
        }
    }
    else
    {
        // unsegmented control message: SEG[0](1bit) || Opcode(7bits) || Params(0-11) 
        if (payload_len >= 1 && payload_len <= 12)
        {
            if ((res & 4) != 0)
            {
                ret = friend_cache_put_msg(ctl, src, dst, payload, payload_len, ttl - 1, seq_src_dst, iv_index);
            }
            if ((res & 2) != 0)
            {
                ret = transport_layer_handle_control_message_(
                    payload[0] & TL_CP_OPCODE_MASK,                                 // Opcode
                    payload + 1, payload_len - 1,                                   // Parameters
                    iv_index, net_key_idx, seq_src_dst, ttl, rssi, friend_idx);
            }
        }
    }

#ifndef MESH_CONTROLLER
    // if it is access message from friend
    if (ctl && friend_idx == -1)
    {
        // we are here because that message has been handled by Low Power layer
        // if no active advertisments then tell to Low Power logic to go to sleep if it is idle
        if (mesh_is_adv_idle())
            low_power_sleep_if_idle();
    }
#endif

    TRACE1(TRACE_DEBUG, "transport_layer_handle: return %d\n", ret);
    return ret;
}

/**
* It will bw called when there is no any active advertisment
* Should be implemented by any layer
*
* Parameters:
*   repeat_times:   None
*
*   Return:         Nobe
*/
void mesh_on_adv_idle(void)
{
    // tell to Low Power logic to go to sleep if it is idle
    low_power_sleep_if_idle();
}

wiced_bool_t transport_layer_send_seg_msg_(wiced_bool_t ctrl_msg, uint8_t ctrl, const uint8_t* payload, uint16_t payload_len, wiced_bool_t szmic,
    uint8_t net_key_idx, MeshSecMaterial *p_frnd_sec, uint16_t src, uint16_t dst, uint8_t ttl,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    wiced_bool_t    ret = WICED_FALSE;
    int     cb_idx;
    uint16_t  seqZero;
    tl_cb_t *p_cb;

    // we need to send segmented message
    ctrl |= TL_CP_SEG_MASK;

    // Try find control block and fail if it is found (it should not happen)
    seqZero = (uint16_t)(node_nv_data.sequence_number & TL_SEQZERO_MASK);
    cb_idx = transport_layer_find_cb_(seqZero, dst);
    if (cb_idx >= 0)
    {
        TRACE1(TRACE_INFO, "transport_layer_send_seg_msg_: already exists. sequence_number:%x\n", node_nv_data.sequence_number);
        return WICED_FALSE;
    }
    // allocate control block and fill it
    cb_idx = transport_layer_alloc_cb_(seqZero, dst, payload_len);
    if (cb_idx < 0)
    {
        return WICED_FALSE;
    }
    p_cb = tl_cb[cb_idx];
    p_cb->src = src;
    p_cb->node_id = dst;
    p_cb->net_key_idx = net_key_idx;
    p_cb->p_frnd_sec = p_frnd_sec;
    memcpy(p_cb->msg, payload, payload_len);
    p_cb->len = payload_len;
    p_cb->ctrl_or_0 = ctrl;
    p_cb->szmic = (uint8_t)szmic;
    p_cb->ctrl_msg = ctrl_msg ? 0x01 : 0x00;
    p_cb->lst_sent_seq = 0xff;
    p_cb->complete_callback = complete_callback;
    p_cb->p_event = p_event;

    if (ttl == MESH_USE_DEFAULT_TTL)
#ifndef MESH_CONTROLLER
        ttl = ctx.default_ttl;
#else
        ttl = MESH_DEFAULT_TTL;
#endif
    else if (ttl > MESH_TTL_MASK)
        ttl = MESH_TTL_MASK;
    p_cb->ttl = ttl;

#ifndef MESH_CONTROLLER
    // if destination of that message is gatt_connected controller then don't retransmit that message
    if (ctx.conn_id && ctx.connected_dev_id == dst)
        p_cb->retrans_or_incompl_to = 1;
    else
#endif
        p_cb->retrans_or_incompl_to = TL_RETRANS;
    p_cb->blockAck = 0;

    // Send all segments
    transport_layer_send_segments_(p_cb);
    TRACE0(TRACE_DEBUG, "transport_layer_send_seg_msg_: returns\n");
    return WICED_TRUE;
}

/**
* Sends controll message doing segmentation if needed
*
* Parameters:
*   opcode:         0x00 = Segment Acknowledgement
*                   0x01 to 0x3F = Opcode of the Transport Control message
*   params:         Parameters for the Transport Control message
*   params_len:     Length of the Parameters for the Transport Control message
*   net_key_idx:    Index if the network key.
*   p_frnd_sec:     Use that friendship secure material. On NULL use master secure material.
*   src:            Source address (address of originating element)
*   dst:            Destination address.
*   ttl:            Message TTL.
*
* Return:   WICED_TRUE if send started successfully. WICED_FALSE - failed to start send.
*/
wiced_bool_t transport_layer_send_ctrl(uint8_t opcode, const uint8_t* params, uint16_t params_len, uint8_t net_key_idx,
    MeshSecMaterial *p_frnd_sec, uint16_t src, uint16_t dst, uint8_t ttl)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t   ctrl;
    TRACE3(TRACE_INFO, "transport_layer_send_ctrl: net_key_idx:%x ttl:%x dst:%04x\n", net_key_idx, ttl, dst);
    TRACE4(TRACE_INFO, " p_frnd_sec!=NULL:%d opcode:%x params_len:%d params:%x ...\n", p_frnd_sec!=NULL, opcode, params_len, params[0]);
    TRACEN(TRACE_INFO, (char*)params, params_len);

    if (params_len > MESH_MAX_APP_PAYLOAD_LEN + MESH_MIC_LEN)
    {
        TRACE0(TRACE_INFO, "transport_layer_send_ctrl: too big payload\n");
        return WICED_FALSE;
    }

    if (net_key_idx >= MESH_NET_KEY_MAX_NUM || (node_nv_data.net_key_bitmask & (1 << net_key_idx)) == 0)
    {
        TRACE0(TRACE_INFO, "transport_layer_send_ctrl: no net_key_idx\n");
        return WICED_FALSE;
    }
    // at first try to find new net key if that key is in the key refresh procedure
    if ((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) != 0)
    {
        if (!find_netkey_((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK) | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &net_key_idx))
        {
            TRACE0(TRACE_INFO, "transport_layer_send_ctrl: no new net_key_idx\n");
            return WICED_FALSE;
        }
        TRACE1(TRACE_INFO, "transport_layer_send_ctrl: new net_key_idx:%d\n", net_key_idx);
    }

    // First byte of the Transport PDU
    ctrl = opcode & TL_CP_OPCODE_MASK;

    // If payload is short enough then we send unsegmented message
    if (params_len <= MESH_MAX_UNSEGMENTED_APP_PAYLOAD_LEN)
    {
        return network_layer_send(&ctrl, 1, params, (uint8_t)params_len, net_key_idx, p_frnd_sec, WICED_TRUE, src, dst, ttl, 0xffffffff, NULL, NULL);
    }

    return transport_layer_send_seg_msg_(WICED_TRUE, ctrl, params, params_len, WICED_FALSE, net_key_idx, p_frnd_sec, src, dst, ttl, NULL, NULL);
}

/**
* Sends application payload (access message) doing segmentation if needed
*
* Parameters:
*   payload:            Application payload to send.
*   payload_len:        Length of the application payload to send includin MIC
*   szmic:              WICED_TRUE/WICED_FALSE meabs send long(8bytes)/short(4bytes) MIC
*   app_key_idx:        Index if the application key. Value 0xff means device key
*   p_frnd_sec:         friendship secure material to use. NULL means use master secure material
*   src:                Source address (address of originating element)
*   dst:                Destination address.
*   ttl:                Message TTL.
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:            Mesh event to be used in complete_callback
*
* Return:   WICED_TRUE if send started successfully. WICED_FALSE - failed to start send.
*/
wiced_bool_t transport_layer_send(const uint8_t* payload, uint16_t payload_len, wiced_bool_t szmic,
    uint8_t app_key_idx, MeshSecMaterial *p_frnd_sec, uint16_t src, uint16_t dst, uint8_t ttl,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t   net_key_idx;
    uint8_t   ctrl;
    TRACE3(TRACE_INFO, "transport_layer_send: app_key_idx:%x ttl:%x p_frnd_sec!=NULL:%d\n", app_key_idx, ttl, p_frnd_sec!=NULL);
    TRACE2(TRACE_INFO, " src:%x dst:%x\n", src, dst);
    TRACEN(TRACE_INFO, (char*)payload, payload_len);

    if (payload_len > MESH_MAX_APP_PAYLOAD_LEN + MESH_MIC_LEN)
    {
        TRACE0(TRACE_INFO, " too big payload\n");
        return WICED_FALSE;
    }

    // if device key should be used
    if (app_key_idx == 0xff)
    {
        // Don't drop access message encrypted by device key with non-unicast  DST because some PTS tests request to send such packet
//        // drop access message encrypted by device key with non-unicast destination address
//        if ((dst & MESH_NON_UNICAST_MASK) != 0)
//        {
//            TRACE0(TRACE_INFO, " Ignore packet with non-unicast dst encrypted by device key\n");
//            return WICED_FALSE;
//        }
        // use first net_key
        for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
        {
            if (node_nv_data.net_key_bitmask & (1 << net_key_idx))
                break;
        }
        if(net_key_idx >= MESH_NET_KEY_MAX_NUM)
        {
            TRACE0(TRACE_INFO, " no net_key\n");
            return WICED_FALSE;
        }
    }
    else
    {
        if (app_key_idx >= MESH_APP_KEY_MAX_NUM || (node_nv_data.app_key_bitmask & (1 << app_key_idx)) == 0)
        {
            TRACE0(TRACE_INFO, "transport_layer_send: no app key_idx\n");
            return WICED_FALSE;
        }
        // at first try to find new net key if that key is in the key refresh procedure
        if (!find_netkey_(node_nv_app_key[app_key_idx].global_net_key_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &net_key_idx)
            && !find_netkey_(node_nv_app_key[app_key_idx].global_net_key_idx, &net_key_idx))
        {
            TRACE0(TRACE_INFO, "transport_layer_send: no net key_idx\n");
            return WICED_FALSE;
        }
        TRACE1(TRACE_INFO, "transport_layer_send: net key_idx:%d\n", net_key_idx);
    }

    // First byte of the Transport PDU
    ctrl = (app_key_idx == 0xff) ? 0 : (TL_CP_AKF_MASK | (node_nv_app_key[app_key_idx].aid & TL_CP_AID_MASK));

    // If payload is short enough then we send unsegmented message with short MIC
    if (payload_len <= (MESH_MAX_UNSEGMENTED_APP_PAYLOAD_LEN + MESH_MIC_LEN))
    {
#ifndef MESH_CONTROLLER
        uint8_t delay = 0;
        // if random delay is requested (for reply)
        if ((uint32_t)p_event == 0xffffffff)
            delay = 0xff;
        // if it is reply then use 0xff which means random delay 20-50 ms
        else if (p_event != NULL)
            delay = p_event->reply ? 0xff : 0;
        mesh_set_adv_params(delay, 0, 0, 0, 0, 0);
#endif
        if ((uint32_t)p_event == 0xffffffff)
            p_event = NULL;
        return network_layer_send(&ctrl, 1, payload, (uint8_t)payload_len, net_key_idx, p_frnd_sec, WICED_FALSE, src, dst, ttl, 0xffffffff, complete_callback, p_event);
    }
    // for segmented messages we don't need random delay. So if p_event == 0xffffffff then pass 0
    if ((uint32_t)p_event == 0xffffffff)
        p_event = NULL;
    return transport_layer_send_seg_msg_(WICED_FALSE, ctrl, payload, payload_len, szmic, net_key_idx, p_frnd_sec, src, dst, ttl, complete_callback, p_event);
}

void transport_layer_send_complete_cb(wiced_bt_mesh_event_t *p_event)
{
    TRACE1(TRACE_DEBUG, "transport_layer_send_complete_cb: p_event:%x\n", (uint32_t)p_event);
    // we don't need to do anything here. We will call access layer callback or release mesh event at the end of sending all segments
    // or at the cancel send transport_layer_cancel_send()
}

/**
* Sends all unacknowledged segments of the message
*
* Parameters:
*   p_cb:   Message control block
*
* Return:   None
*/
static void transport_layer_send_segments_(tl_cb_t *p_cb)
{
    uint8_t   segN = (p_cb->len - 1) / MESH_MAX_APP_PAYLOAD_SEGMENT_LEN;
    // Segmented access message: SEQ(1) AKF(1bit) || AID(6bits) || SZMIC(1bit) || SeqZero(13bits) || SeqO(5bits) || SegN(5bits) || Segment(1-12 bytes)
    uint8_t   ctrl_field[4] = { p_cb->ctrl_or_0,
        (p_cb->szmic ? TL_CP_SZMIC_MASK : 0) | (uint8_t)(p_cb->seqZero >> 6),
        (uint8_t)(p_cb->seqZero << 2), segN };

    // decrement retransmitions
    if (p_cb->retrans_or_incompl_to)
        p_cb->retrans_or_incompl_to--;

    p_cb->lst_sent_seq = network_layer_send_segments(ctrl_field, p_cb->msg, p_cb->len, p_cb->lst_sent_seq, p_cb->blockAck, p_cb->net_key_idx, p_cb->p_frnd_sec, p_cb->ctrl_msg ? WICED_TRUE : WICED_FALSE, p_cb->src, p_cb->node_id, p_cb->ttl, transport_layer_send_complete_cb, p_cb->p_event);
    // Start a segment transmission timer.
    p_cb->to = TL_TRANS_TO_MS + transport_layer_get_ticks_(WICED_FALSE);
}

/**
* Handles transport segment acknowledge
*
* Parameters:
*   obo:            OBO flag
*   seqZero:        SeqZero value
*   blockAck:       Block acknowledgement for segments(4 bytes)
*
* Return:   WICED_TRUE if handled successfully. WICED_FALSE - failed to handle.
*/
static wiced_bool_t transport_layer_on_seg_ack_(uint8_t obo, uint16_t seqZero, uint16_t src, uint32_t blockAck)
{
    int     cb_idx;
    tl_cb_t *p_cb;
    //ToDo: Handle OBO flag for friend devic

    cb_idx = transport_layer_find_cb_(seqZero, src);
    TRACE2(TRACE_INFO, "transport_layer_on_seg_ack_: obo:%d seqZero:%x\n", obo, seqZero);
    TRACE2(TRACE_INFO, " src:%x cb_idx:%d\n", src, cb_idx);
    TRACE1(TRACE_INFO, " blockAck:%x\n", blockAck);
    if (cb_idx < 0)
    {
        return WICED_FALSE;
    }
    p_cb = tl_cb[cb_idx];

#ifndef MESH_CONTROLLER
    // stop sending all acknowledged segments for that device
    if (blockAck)
        mesh_stop_acked_seg(p_cb->node_id, blockAck);
#endif

    // Remember new BlockAck
    p_cb->blockAck = blockAck;
    
    // Calc mask with all bits set to 1
    blockAck = (p_cb->len - 1) / MESH_MAX_APP_PAYLOAD_SEGMENT_LEN + 1;    // number of segments
    blockAck = ((uint32_t)1 << blockAck) - 1;

    // If all segments are acknowledged or we received 0 BlockAck then we are done. Delete control block
    if ((p_cb->blockAck & blockAck) == blockAck || p_cb->blockAck == 0)
    {
        transport_layer_del_cb_(cb_idx);
        return WICED_TRUE;
    }

    // Send all unacknowledged segments
    transport_layer_send_segments_(p_cb);

    TRACE0(TRACE_DEBUG, "transport_layer_on_seg_ack_: returns\n");
    return WICED_TRUE;
}

/**
* Calculates SeqAuth value
*
* Parameters:
*   seq:            SEQ value
*   seq_zero:       SeqZero value
*
* Return:   SeqAuth value
*/
uint32_t transport_layer_calc_seq_auth(uint32_t seq, uint16_t seq_zero)
{
    uint32_t seq_auth = (seq & ~TL_SEQZERO_MASK) | seq_zero;
    if (seq_auth > seq)
        seq_auth -= TL_SEQZERO_MASK + 1;
    return seq_auth;
}

/**
* Extracts SeqZero value from Low Transport PDU
*
* Parameters:
*   pdu:            Low Transport PDU
*
* Return:   SeqZero value
*/
uint16_t transport_layer_get_seq_zero_from_pdu(const uint8_t *pdu)
{
    return (((uint16_t)pdu[1] & 0x7f) << 6) + (((uint16_t)pdu[2]) >> 2);
}

/**
* Handles transport segment
*
* Parameters:
*   ctl:            CTL field of the network PDU
*   flags:          First byte of the message:
*                   Control Message(ctl): SEG(1bit) AKF(1bit) || AID(6bits)
*                   Access Message(!ctl): SEG(1bit) || OP(7bits)
*   szmic:          Flag to use 64bits app MIC. Ignored for control message(ctl == WICED_TRUE)
*   segO:           SegO value
*   segN:           SegN value
*   seqZero:        SeqZero value
*   payload:        Application payload segment
*   payload_len:    Length of the application payload segment
*   iv_index:       IV index
*   net_key_idx:    Local index of the NetKey
*   seq_src_dst:    pointer on the place in the packet with SEQ || SRC || DST
*   ttl:            TTL of the received packet
*   res:            bits: 0x01 - should be retransmitted; 0x02 - targeted to our device; 0x04 - targeted to our friend LPN
*   friend_idx:     -2 - network sec material;-1 - LPN sec material; >=0 - sec material of friend index
*
* Return:   WICED_TRUE/WICED_FALSE - succeeded/failed
*/
static wiced_bool_t transport_layer_on_seg_(wiced_bool_t ctl, uint8_t flags, wiced_bool_t szmic, uint8_t segO, uint8_t segN, uint16_t seqZero,
                     const uint8_t* payload, uint16_t payload_len, const uint8_t* iv_index, uint8_t net_key_idx,
                     const uint8_t* seq_src_dst, uint8_t ttl, uint8_t res, int friend_idx)
{
    int             cb_idx;
    tl_cb_t         *p_cb;
    uint32_t        segMask, segMaskAllBits;
    uint16_t        src, dst;
    uint32_t        seq, seqAuth;
    wiced_bool_t    ret = WICED_TRUE;

    seq = BE3TOUINT32(seq_src_dst);
    src = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN);

    TRACE4(TRACE_CRITICAL, "transport_layer_on_seg_: segO:%d segN:%d seqZero:%x res:%x\n", segO, segN, seqZero, res);
    TRACE1(TRACE_INFO, " seq:%x\n", seq);
    TRACE4(TRACE_INFO, " ctl:%d flags:%x szmic:%d friend_idx:%d\n", ctl, flags, szmic, friend_idx);
    TRACE2(TRACE_INFO, " src:%x payload_len:%d\n", src, payload_len);

    if (segO > segN || segN > 31 || payload_len < 1
        || payload_len > MESH_MAX_APP_PAYLOAD_SEGMENT_LEN)
    {
        return WICED_FALSE;
    }

    // calc SeqAuth
    seqAuth = transport_layer_calc_seq_auth(seq, seqZero);

    // Receiving and sending segmented packets can have same SeqZero. To resolve that issue lets use flag 0x8000 for receiving messages
    cb_idx = transport_layer_find_cb_(seqZero | 0x8000, src);

#ifndef MESH_CONTROLLER
    // handle cache for seq_auth
    if (!transport_layer_cache_seq_auth(iv_index, src, seqAuth, cb_idx < 0))
        return WICED_FALSE;
#endif

    // If it is first segment of that transaction
    if (cb_idx < 0)
    {
        cb_idx = transport_layer_alloc_cb_(seqZero | 0x8000, src, (segN + 1) * MESH_MAX_APP_PAYLOAD_SEGMENT_LEN);
        if (cb_idx < 0)
            return WICED_FALSE;
    }

    p_cb = tl_cb[cb_idx];
    dst = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN);
    // if it is first segment
    if (p_cb->blockAck == 0)
    {
        p_cb->net_key_idx = net_key_idx;
        p_cb->to = 0;
        p_cb->src = dst;
        if (friend_find_lpn(dst, NULL))
            p_cb->flags |= TL_CB_FLAGS_OBO;
    }

    // if we are LPN and this message was encrypted by friend secured material then don't send acknowledges
    if (friend_idx != -1)
    {
        // For unicast address start acknowledgement timer if it expired
        if (p_cb->to == 0 && (dst & MESH_NON_UNICAST_MASK) == 0)
            p_cb->to = TL_ACK_TO_MS + transport_layer_get_ticks_(WICED_FALSE);
    }

    segMask = ((uint32_t)1) << segO;
    // Calc mask with all bits set to 1
    segMaskAllBits = (p_cb->len - 1) / MESH_MAX_APP_PAYLOAD_SEGMENT_LEN + 1;    // number of segments
    segMaskAllBits = ((uint32_t)1 << segMaskAllBits) - 1;

    // if this segment wasn't received yet
    if ((p_cb->blockAck & segMask) == 0)
    {
        // if this message came for our friend LPN then copy this message to LPN cache
        if ((res & 4) != 0)
        {
            friend_cache_put_msg(ctl, src, dst, payload - 4, (uint8_t)payload_len + 4, ttl - 1, seq_src_dst, iv_index);
        }
        // restart incomplete timer
        p_cb->retrans_or_incompl_to = TL_INCOMPL_TO_MS + transport_layer_get_ticks_(WICED_FALSE);
        // update blockAck
        p_cb->blockAck |= segMask;
        // if it is last segment then update SDU length
        if (segO == segN)
            p_cb->len -= MESH_MAX_APP_PAYLOAD_SEGMENT_LEN - payload_len;

        memcpy(&p_cb->msg[segO * MESH_MAX_APP_PAYLOAD_SEGMENT_LEN], payload, payload_len);

        TRACE1(TRACE_INFO, "transport_layer_on_seg_: blockAck:%x\n", p_cb->blockAck);
        TRACE1(TRACE_INFO, " segMaskAllBits:%x\n", segMaskAllBits);
        TRACE1(TRACE_INFO, " seqZero:%x seq_src_dst:\n", p_cb->seqZero);
        TRACEN(TRACE_INFO, (char*)seq_src_dst, MESH_SEQ_SRC_DST_LEN);

        // If all segments are received then pass message to the app layer
        if ((p_cb->blockAck & segMaskAllBits) == segMaskAllBits)
        {
            uint8_t new_seq_src_dst[MESH_SEQ_SRC_DST_LEN];
            // if this message came for our friend LPN then remove incomplete flag from all segments of that message
            if ((res & 4) != 0)
            {
                friend_cache_complete_msg(dst, WICED_FALSE);
            }

            // Send segment acknowledgement message and remove ack timeout
            p_cb->to = 0;
            // if we are LPN and this message was encrypted by friend secured material then don't send acknowledges
            if (friend_idx != -1)
            {
#ifndef MESH_CONTROLLER
                mesh_set_adv_params(0, 0, 0, 0, p_cb->node_id, 0);
#endif
                transport_layer_send_ack_(p_cb);
            }

            // create new seq_src_dst stream using seqAuth instead of seq
            UINT32TOBE3(new_seq_src_dst, seqAuth);
            memcpy(&new_seq_src_dst[MESH_PKT_SEQ_LEN], &seq_src_dst[MESH_PKT_SEQ_LEN], MESH_SEQ_SRC_DST_LEN - MESH_PKT_SEQ_LEN);
            if ((res & 2) != 0)
            {
                // if it was Access Message then pass application payloag to application layer
                if (!ctl)
                {
                    access_layer_handle(
                        (flags & TL_CP_AKF_MASK) != 0,          // AKF
                        flags & TL_CP_AID_MASK,                 // AID
                        szmic, p_cb->msg, p_cb->len, iv_index,
                        net_key_idx, new_seq_src_dst,
                        ttl, friend_idx);
                }
                else
                {
                    // It was control message. We don't care about RSSI for segmented messages because for now we need RRSI
                    // just for Friend Poll request which is unsegmented
                    transport_layer_handle_control_message_(
                        flags & TL_CP_OPCODE_MASK,              // Opcode
                        p_cb->msg, p_cb->len, iv_index, net_key_idx, new_seq_src_dst, ttl, BTM_INQ_RES_IGNORE_RSSI, friend_idx);
                }
            }
        }
    }
    else
    {
        // if we are LPN and this message was encrypted by friend secured material then don't send acknowledges
        if (friend_idx != -1)
        {
            // this segment was received already
            // If all segments are received then send ack immediatly
            if ((p_cb->blockAck & segMaskAllBits) == segMaskAllBits)
            {
                p_cb->to = 1;
            }
        }
    }
    // fine timer will be called only if there are no more activities
    // therefore call transport_layer_timer() here
    transport_layer_timer_callback(0);
    return ret;
}

/**
* Replaces net_key_idx in all control block from new to old
*
* Parameters:
*   from_net_key_idx:       net key index to replace
*   to_net_key_idx:         net key index to replace by
*/
void transport_layer_handle_kr_finish(uint8_t from_net_key_idx, uint8_t to_net_key_idx)
{
    uint8_t i;
    for (i = 0; i < tl_cb_cnt; i++)
    {
        if (tl_cb[i]->net_key_idx == from_net_key_idx)
            tl_cb[i]->net_key_idx = to_net_key_idx;
    }

}

/**
* Sends segment acknowledge message
*
* Parameters:
*   p_cb:   Message control block
*
* Return:   WICED_TRUE if message has been sent
*/
static wiced_bool_t transport_layer_send_ack_(tl_cb_t *p_cb)
{
    wiced_bool_t    ret = WICED_TRUE;
    uint8_t   payload[7];

    // create message: SEG[0](1bit) || OP[0](7bits) || OBO[0](1bit) || SeqZero(13bits) || RFU[0](2bits) || BlockAck(4bytes)
    payload[0] = 0;
    payload[1] = (uint8_t)(p_cb->seqZero >> 6);
    if (p_cb->flags & TL_CB_FLAGS_OBO)
        payload[1] |= TL_CP_OBO_MASK;
    payload[2] = (uint8_t)(p_cb->seqZero << 2);
    UINT32TOBE4(&payload[3], p_cb->blockAck);
    TRACE0(TRACE_INFO, "transport_layer_send_ack_:\n");
    TRACEN(TRACE_INFO, (char*)payload, sizeof(payload));

    // send message
    ret = network_layer_send(payload, 1, &payload[1], 6,
        p_cb->net_key_idx, p_cb->p_frnd_sec, WICED_TRUE, p_cb->src, p_cb->node_id, MESH_USE_DEFAULT_TTL, 0xffffffff, NULL, NULL);
    return ret;
}

/**
* Should be called by application often (around once on 100ms)
*
* Parameters:   None
*
* Return:   None
*/
static void transport_layer_timer_callback(uint32_t arg)
{
    uint8_t cb_idx;
    tl_cb_t *p_cb;
    uint16_t passed_ms;

    passed_ms = transport_layer_get_ticks_(WICED_TRUE);

    for (cb_idx = 0; cb_idx < tl_cb_cnt; cb_idx++)
    {
        p_cb = tl_cb[cb_idx];

        // if it is receiving side
        if (p_cb->ctrl_or_0 == 0)
        {
            // If incomplete timer passed then delete that control block
            if (p_cb->retrans_or_incompl_to <= passed_ms)
            {
                TRACE0(TRACE_INFO, "************ transport_layer_timer: incomplete timer expired\n");
                transport_layer_del_cb_(cb_idx--);
                continue;
            }
            // update incomplete timeout
            p_cb->retrans_or_incompl_to -= passed_ms;
        }

        // ignore blocks with 0 timeout - when we don't want to send ack
        if (p_cb->to == 0)
            continue;
        // update remaining timeout value
        if (p_cb->to > passed_ms)
        {
            p_cb->to -= passed_ms;
            continue;
        }
        p_cb->to = 0;

        TRACE0(TRACE_INFO, "transport_layer_timer:\n");
        // if it is sending side
        if (p_cb->ctrl_or_0)
        {
            // If no more retransmittons then delete control block
            if (p_cb->retrans_or_incompl_to == 0)
            {
                transport_layer_del_cb_(cb_idx--);
                continue;
            }

            // Send all unacknowledged segments of the current message and exit loop
            transport_layer_send_segments_(p_cb);
            break;
        }
        else
        {
            // it is receiving side
            // Send segment acknowledgement message
#ifndef MESH_CONTROLLER
            mesh_set_adv_params(0, 0, 0, 0, p_cb->node_id, 0);
#endif
            transport_layer_send_ack_(p_cb);
        }
    }
}

static void handle_heartbeat(uint8_t* payload, uint16_t payload_len, uint16_t src, uint16_t dst, uint8_t ttl, uint8_t rssi, uint8_t net_key_idx)
{
#ifndef MESH_CONTROLLER
    uint64_t        passed_time;
    uint8_t         init_ttl, hops;
    uint16_t        features;
    wiced_bool_t    changed = WICED_FALSE;

    // ignore if dst is unassigned or message came not from subscription source or period is 0
    if (node_nv_data.heartbeat_subs.dst == MESH_NODE_ID_INVALID
        || src != node_nv_data.heartbeat_subs.src
        || node_nv_data.heartbeat_subs.period_log == 0)
    {
        TRACE2(TRACE_INFO, "handle_heartbeat: ignore. subs not configured to receive. dst:%x src:%x\n", node_nv_data.heartbeat_subs.dst, node_nv_data.heartbeat_subs.src);
        return;
    }
    passed_time = wiced_bt_mesh_core_get_tick_count() / 1000 - node_nv_data.heartbeat_subs.start_time;
    // if period is passed then set period to 0 and ignore message
    if (passed_time >= heartbeat_period_from_log(node_nv_data.heartbeat_subs.period_log))
    {
        TRACE2(TRACE_INFO, "handle_heartbeat: ignore. passed_time:%x period:%x\n", (uint32_t)passed_time, node_nv_data.heartbeat_subs.period_log);
        node_nv_data.heartbeat_subs.period_log = 0;
        mesh_save_node_nv_data();
        return;
    }
    // get parameters and check them out
    init_ttl = *payload++;
    features = BE2TOUINT16(payload);

    if (init_ttl > MESH_MAX_TTL)
    {
        TRACE2(TRACE_INFO, "handle_heartbeat: ignore invalid params. init_ttl:%x features:%x\n", init_ttl, features);
        return;
    }

    // update state
    hops = init_ttl - ttl + 1;
    if (hops < node_nv_data.heartbeat_subs.min_hops)
    {
        node_nv_data.heartbeat_subs.min_hops = hops;
        changed = WICED_TRUE;
    }
    if (hops > node_nv_data.heartbeat_subs.max_hops)
    {
        node_nv_data.heartbeat_subs.max_hops = hops;
        changed = WICED_TRUE;
    }
    if (node_nv_data.heartbeat_subs.count < 0xffff)
    {
        node_nv_data.heartbeat_subs.count++;
        changed = WICED_TRUE;
    }
    TRACE3(TRACE_INFO, "handle_heartbeat: hops min/max:%d/%d count:%d\n", node_nv_data.heartbeat_subs.min_hops, node_nv_data.heartbeat_subs.max_hops, node_nv_data.heartbeat_subs.count);
    if (changed)
        mesh_save_node_nv_data();
#endif

    {
        wiced_bt_mesh_event_t   *p_event;
        if ((p_event = (wiced_bt_mesh_event_t*)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_event_t))) != NULL)
        {
            p_event->element_idx = 0;
            p_event->company_id = 0xffff;
            p_event->model_id = 0xffff;
            p_event->opcode = WICED_BT_MESH_CORE_CMD_SPECIAL_HEARTBEAT;
            p_event->ttl = ttl;
            p_event->src = src;
            p_event->dst = dst;
            p_event->app_key_idx = net_key_idx;
            p_event->reply = WICED_FALSE;
            if (!foundation_received_msg_callback(p_event, payload, payload_len))
                wiced_bt_free_buffer(p_event);
        }
    }
}

static wiced_bool_t transport_layer_handle_control_message_(uint8_t op, uint8_t* payload, uint16_t payload_len, const uint8_t* iv_index, uint8_t net_key_idx, const uint8_t* seq_src_dst, uint8_t ttl, uint8_t rssi, int friend_idx)
{
    uint16_t src = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN);
    uint16_t dst = BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN);

    // If it is Heartbeat message
    if (op == MESH_TL_CM_OP_HEARTBEAT)
    {
        handle_heartbeat(payload, payload_len, src, dst, ttl, rssi, net_key_idx);
        return WICED_TRUE;
    }

    // Message to Low Power Node can be encrypted by LPN sec material or by network material
    if (friend_idx <= -1)
        if(low_power_handle_ctrl(op, payload, payload_len, src, dst, ttl, friend_idx == -1))
            return WICED_TRUE;

    // Message to Friend can be encrypted by friend sec material or by network material
    if (friend_idx >= 0 || friend_idx == -2)
    {
        if (friend_handle_ctrl(op, payload, payload_len, src, dst, ttl, rssi, net_key_idx, friend_idx))
            return WICED_TRUE;
    }
    return WICED_FALSE;
}

wiced_bool_t transport_layer_is_sending_segments(void)
{
    wiced_bool_t ret = WICED_FALSE;
    uint8_t cb_idx;
    for (cb_idx = 0; cb_idx < tl_cb_cnt; cb_idx++)
    {
        // if it is sending side then return WICED_TRUE
        if (tl_cb[cb_idx]->ctrl_or_0)
        {
            ret = WICED_TRUE;
            break;
        }
    }
    TRACE1(TRACE_DEBUG, "is_sending_segments: returns:%d\n", ret);
    return ret;
}

/**
* Stops segemented message transmitting
*
* Parameters:
*   p_event:      Application payload to send.
*
* Return:   WICED_TRUE if segemented message is found and canceled. WICED_FALSE - this segemented message is not found.
*/
wiced_bool_t transport_layer_cancel_send(wiced_bt_mesh_event_t* p_event)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t         cb_idx;
    for (cb_idx = 0; cb_idx < tl_cb_cnt; cb_idx++)
    {
        if (tl_cb[cb_idx]->p_event == p_event)
        {
            ret = WICED_TRUE;
            // disable callback
            tl_cb[cb_idx]->complete_callback = NULL;
            // it will delete event
            transport_layer_del_cb_(cb_idx);
            break;
        }
    }
    return ret;
}

#ifndef MESH_CONTROLLER
/**
* Clears Replay Protection List
*/
void transport_layer_clear_rpl(void)
{
    memset(ctx.auth_cache_src, 0, sizeof(ctx.auth_cache_src));
    memset(ctx.app_cache, 0, sizeof(ctx.app_cache));
    memset(ctx.app_cache_prev, 0, sizeof(ctx.app_cache_prev));
    app_cache_save(WICED_TRUE);
    app_cache_save(WICED_FALSE);
}
#endif

