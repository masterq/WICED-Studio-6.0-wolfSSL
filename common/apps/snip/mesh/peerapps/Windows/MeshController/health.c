/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Health Model implementation
 *
*/
#include <string.h>
#include <stdio.h>
#include "platform.h"

#include "mesh_core.h"
#include "mesh_util.h"
#include "foundation_int.h"
#include "foundation.h"
#include "access_layer.h"
#include "core_aes_ccm.h"
#include "key_refresh.h"
#include "wiced_bt_mesh_core.h"
#include "wiced_timer.h"
#include "health.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__HEALTH_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__HEALTH_C
#include "mesh_trace.h"

static void health_handle_attention_get_(uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id);
static void health_handle_attention_set_(wiced_bool_t unreliable, uint8_t attention, uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id);
static void health_send_status_(uint16_t op, uint8_t state, uint8_t app_key_idx, uint16_t src_id);
static void health_handle_fault_clear_(uint16_t company_id, wiced_bool_t unreliable, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id);
static void health_send_fault_status_(uint16_t company_id, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id, uint8_t ttl, wiced_bool_t credential_flag);
static void health_send_current_status_(uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id, uint8_t ttl, wiced_bool_t credential_flag);
static void health_handle_period_set_(wiced_bool_t unreliable, uint8_t period, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id);
static void health_handle_fault_test_(wiced_bool_t unreliable, uint8_t test_id, uint16_t company_id, uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id);

#pragma pack(1)
#ifndef PACKED
#define PACKED
#endif

typedef PACKED struct
{
    uint64_t                attention_end;      // System time of the attention end. 0 - Off
    uint64_t                publish_start;      // System time when publishing period started. 0 - Off
    tFND_NVM_ELEMENT        *p_elem;            // element which that item belongs to
    uint8_t                 model_idx;          // Index of the model in the array p_elem->models
} tHEALTH_STATE_ELEMENT;

typedef PACKED struct
{
    wiced_bt_mesh_core_health_fault_test_cb_t           fault_test_cb;
    wiced_bt_mesh_core_attention_cb_t                   attention_cb;
    uint8_t                                             count;              // number of elements with health model (in the array elements
    tHEALTH_STATE_ELEMENT                               elements[1];        // array of elements with health model
} tHEALTH_STATE;

#pragma pack()

tHEALTH_STATE           *health_state_;
tHEALTH_NVM_STATE       *health_nvm_state;

/**
* \brief Timer function to handle time related logic.
* \details Core should call it on each second tick or so
*/
void health_timer(void)
{
    tHEALTH_STATE_ELEMENT   *p_state;
    tFND_NVM_MODEL          *p_model;
    uint8_t                 idx;
    uint64_t                current_time;
    uint32_t                period_ms;

    current_time = wiced_bt_mesh_core_get_tick_count();
    for (idx = 0; idx < health_state_->count; idx++)
    {
        p_state = &health_state_->elements[idx];
        if (p_state->attention_end != 0 && p_state->attention_end <= current_time)
        {
            p_state->attention_end = 0;
            if (health_state_->attention_cb)
                health_state_->attention_cb(p_state->p_elem->idx, 0);
            break;
        }
        if (health_state_->fault_test_cb != NULL)
        {
            p_model = &p_state->p_elem->models[p_state->model_idx];
            //TRACE3(TRACE_DEBUG, "health_timer: faults_number:%x publish_period:%x publish_address:%x\n", health_nvm_state[idx].faults_number, p_model->publish_period, p_model->publish_address);
            if (p_model->publish_address != MESH_NODE_ID_INVALID)
            {
                period_ms = p_model->publish_period & 0x3f;
                if (period_ms != 0)
                {
                    //TRACE2(TRACE_DEBUG, "health_timer: publish_start:%x current_time:%x\n", (uint16_t)p_state->publish_start, (uint16_t)current_time);
                    if (p_state->publish_start == 0)
                    {
                        p_state->publish_start = current_time;
                    }
                    else
                    {
                        switch (p_model->publish_period >> 6)
                        {
                        case 0: period_ms *= 100; break;
                        case 1: period_ms *= 1000; break;
                        case 2: period_ms *= 10000; break;
                        case 3: period_ms *= 600000; break;
                        }
                        // if faults array isn't empty then use FastPeriodDivisor
                        if (health_nvm_state[idx].faults_number > 0)
                            period_ms /= (uint32_t)1 << health_nvm_state[idx].fast_period_log;

                        if (p_state->publish_start + period_ms <= current_time)
                        {
                            uint8_t app_key_idx;
                            if (foundation_find_appkey_(p_model->publish_app_key_idx, &app_key_idx, NULL, NULL))
                            {
                                p_state->publish_start = current_time;
                                health_send_current_status_(idx, app_key_idx, p_model->publish_address, p_model->publish_ttl, p_model->credential_flag);
                            }
                        }
                    }
                }
            }
        }
    }
}

static wiced_bool_t health_write_config_(void)
{
    uint16_t    nvm_len = sizeof(tHEALTH_NVM_STATE) * health_state_->count;
    if (nvm_len != mesh_write_node_info(MESH_NVM_IDX_HEALTH_STATE, (uint8_t*)health_nvm_state, nvm_len, NULL))
        return WICED_FALSE;
    return WICED_TRUE;
}

static int health_get_count_cb_(tFND_NVM_ELEMENT *p_elem, tFND_NVM_MODEL *p_model, uint8_t *elements_cnt)
{
    if (p_model->company_id != 0 || p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV)
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    // make sure base element(0) contains health model - it is mandatory
    if (*elements_cnt == 0 && p_elem->idx != 0)
        return FND_SCAN_MODELS_CB_NEXT_STOP;
    (*elements_cnt)++;
    return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
}

static int health_init_cb_(tFND_NVM_ELEMENT *p_elem, tFND_NVM_MODEL *p_model, tHEALTH_STATE *p_state)
{
    if (p_model->company_id != 0 || p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV)
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    p_state->elements[p_state->count].p_elem = p_elem;
    p_state->elements[p_state->count].model_idx = p_model->idx;
    p_state->count++;
    return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
}

/**
* \brief Initializes Health Model of the all elements reading state from the NVM.
* \details Application should call it on startup
*
* @param[in]   get_current_fault_cb        :Callback function to be called to get Current Fault
* @param[in]   attention_cb                :Callback function to be called to attract human attention
*
* @return      WICED_TRUE - succeeded. WICED_FALSE - failed;
*/
wiced_bool_t health_init(wiced_bt_mesh_core_health_fault_test_cb_t fault_test_cb, wiced_bt_mesh_core_attention_cb_t attention_cb)
{
    wiced_bool_t            res = WICED_TRUE;
    uint8_t                 elements_cnt = 0;
    uint16_t                len;

    // calc elements number with health model
#ifndef MESH_CONTROLLER
    foundation_scan_models_((foundation_scan_models_cb_)health_get_count_cb_, &elements_cnt);
#endif
    TRACE1(TRACE_DEBUG, "health_init: elements_cnt:%d\n", elements_cnt);
    len = OFFSETOF(tHEALTH_STATE, elements) + sizeof(tHEALTH_STATE_ELEMENT) * elements_cnt;
    health_state_ = (tHEALTH_STATE*)wiced_memory_permanent_allocate(len);
    memset(health_state_, 0, len);
#ifndef MESH_CONTROLLER
    health_state_->fault_test_cb = fault_test_cb;
    health_state_->attention_cb = attention_cb;
    if (elements_cnt == 0)
        res = WICED_FALSE;
    else
    {
        foundation_scan_models_((foundation_scan_models_cb_)health_init_cb_, health_state_);
        len = sizeof(tHEALTH_NVM_STATE) * elements_cnt;
        health_nvm_state = (tHEALTH_NVM_STATE*)wiced_memory_permanent_allocate(len);
        memset(health_nvm_state, 0, len);
        if (len != mesh_read_node_info(MESH_NVM_IDX_HEALTH_STATE, (uint8_t*)health_nvm_state, len, NULL))
        {
            if (!health_write_config_())
            {
                health_state_->count = 0;
                res = WICED_FALSE;
            }
        }
    }
#endif
    TRACE1(TRACE_DEBUG, "health_init: returns %d\n", res);
    return res;
}

typedef struct
{
    uint8_t             element_idx;
    uint8_t             nvm_idx;
    tFND_NVM_ELEMENT    *p_elem;
    tFND_NVM_MODEL      *p_model;
} tHEALTH_FIND_CB_DATA;

static int health_find_cb_(tFND_NVM_ELEMENT *p_elem, tFND_NVM_MODEL *p_model, tHEALTH_FIND_CB_DATA* p_data)
{
    if (p_model->company_id != 0
        || (p_model->model_id != WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV))
        return FND_SCAN_MODELS_CB_NEXT_MODEL;
    p_data->nvm_idx++;
    if (p_data->element_idx != p_elem->idx)
        return FND_SCAN_MODELS_CB_NEXT_ELEMENT;
    p_data->p_elem = p_elem;
    p_data->p_model = p_model;
    return FND_SCAN_MODELS_CB_NEXT_STOP;
}

/**
 * \brief Process data packet addressed to this mesh node
 *
 * @param[in]   element_idx     :Index of the destination element
 * @param[in]   src_id:         :Address of the message source.
 * @param[in]   app_key_idx     :app key index
 * @param[in]   ttl             :TTL of the received packet
 * @param[in]   opcode          :opcode of the payload
 * @param[in]   params          :Application Parameters - extracted from the Access Payload
 * @param[in]   params_len      :Length of the Application Parameters
 *
 * @return      WICED_TRUE if it is health opcode. Otherwise WICED_FALSE.
 */
wiced_bool_t health_handle(uint8_t element_idx, uint16_t src_id, uint8_t app_key_idx, uint8_t ttl, uint16_t opcode, const uint8_t *params, uint16_t params_len)
{
    wiced_bool_t            ret = WICED_TRUE;
    uint8_t                 nvm_idx;
    tHEALTH_STATE_ELEMENT   *p_state;
    tFND_NVM_MODEL          *p_model;

    // find related model config and health state
    for (nvm_idx = 0; nvm_idx < health_state_->count; nvm_idx++)
    {
        p_state = &health_state_->elements[nvm_idx];
        if (p_state->p_elem->idx == element_idx)
        {
            p_model = &p_state->p_elem->models[p_state->model_idx];
            if (p_model->company_id == 0 && p_model->model_id == WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV)
                break;
        }
    }

    TRACE3(TRACE_INFO, "health_handle: element_idx:%d ttl=%x src_id=%x\n", element_idx, ttl, src_id);
    TRACE3(TRACE_INFO, " nvm_idx:%d nvm_cnt:%d opcode=%x ***************************\n", nvm_idx, health_state_->count, opcode);
    if (params_len)
        TRACEN(TRACE_INFO, (char*)params, params_len);

    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_ATTENTION_GET:
        if (params_len == 0 && nvm_idx < health_state_->count)
            health_handle_attention_get_(element_idx, nvm_idx, app_key_idx, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET:
    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED:
        if (params_len == 1 && nvm_idx < health_state_->count)
            health_handle_attention_set_(opcode == WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED, *params, element_idx, nvm_idx, app_key_idx, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED:
        if (params_len == 2 && nvm_idx < health_state_->count)
            health_handle_fault_clear_(LE2TOUINT16(params), opcode == WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED, nvm_idx, app_key_idx, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET:
        if (params_len == 2 && nvm_idx < health_state_->count)
            health_send_fault_status_(LE2TOUINT16(params), nvm_idx, app_key_idx, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2);
        break;

    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED:
        if (params_len == 3 && nvm_idx < health_state_->count)
            health_handle_fault_test_(opcode == WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED, *params, LE2TOUINT16(params + 1), element_idx, nvm_idx, app_key_idx, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET:
        if (params_len == 0 && nvm_idx < health_state_->count)
            health_send_status_(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS, health_nvm_state[nvm_idx].fast_period_log, app_key_idx, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET:
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED:
        if (params_len == 1 && nvm_idx < health_state_->count)
            health_handle_period_set_(opcode == WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED, *params, nvm_idx, app_key_idx, src_id);
        break;
    default:
        ret = WICED_FALSE;
        break;
    }
    TRACE1(TRACE_DEBUG, " returns %d\n", ret);
    return ret;
}

static void health_handle_attention_get_(uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id)
{
    uint8_t     attention = 0;
    uint64_t    attention_end = health_state_->elements[nvm_idx].attention_end;
    uint64_t    current_time = wiced_bt_mesh_core_get_tick_count();
    // use 0 attention timer if it is Off or expired
    if (attention_end != 0 && attention_end >= current_time)
    {
        attention_end = (attention_end - current_time) / 1000;
        attention = (uint8_t)(attention_end == 0 ? 1 : (attention_end > 0xff ? 0xff : attention_end));
    }
    health_send_status_(WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS, attention, app_key_idx, src_id);
}

static void health_handle_attention_set_(wiced_bool_t unreliable, uint8_t attention, uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id)
{
    if (attention)
        health_state_->elements[nvm_idx].attention_end = wiced_bt_mesh_core_get_tick_count() + attention * 1000;
    else
        health_state_->elements[nvm_idx].attention_end = 0;
    if (!unreliable)
        health_send_status_(WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS, attention, app_key_idx, src_id);
    if(health_state_->attention_cb != NULL)
        health_state_->attention_cb(element_idx, attention);
}

static void health_send_status_(uint16_t op, uint8_t state, uint8_t app_key_idx, uint16_t src_id)
{
    uint8_t   buf[2 + 1];
    uint8_t   *p;

    // OP code
    p = op2be(&buf[0], op);
    *p++ = state;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), app_key_idx, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static void health_handle_fault_clear_(uint16_t company_id, wiced_bool_t unreliable, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id)
{
    if (health_nvm_state[nvm_idx].faults_number != 0)
    {
        health_nvm_state[nvm_idx].faults_number = 0;
        health_write_config_();
    }
    if (!unreliable)
        health_send_fault_status_(company_id, nvm_idx, app_key_idx, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2);
}

static void health_send_fault_status_(uint16_t company_id, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id, uint8_t ttl, wiced_bool_t credential_flag)
{
    uint8_t buf[1 + 2 + HEALTH_STATE_FAULTS_MAX_NUM];
    uint8_t *p;
    uint8_t idx;

    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_STATUS);
    *p++ = health_nvm_state[nvm_idx].test_id;
    UINT16TOLE2(p, company_id);
    p += 2;
    if(health_nvm_state[nvm_idx].faults_number != 0 && company_id == health_nvm_state[nvm_idx].company_id)
    {
        for (idx = 0; idx < health_nvm_state[nvm_idx].faults_number; idx++)
        {
            *p++ = health_nvm_state[nvm_idx].fault_array[idx];
        }
    }
    // 0xffffffff in the p_event means do random delay 20-50 ms
    TRACE2(TRACE_DEBUG, "***** health_send_fault_status_: test_id:%x faults_number:%x\n", health_nvm_state[nvm_idx].test_id, health_nvm_state[nvm_idx].faults_number);
    access_layer_send(buf, (uint16_t)(p - &buf[0]), app_key_idx, 0, src_id, ttl, credential_flag, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static void health_send_current_status_(uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id, uint8_t ttl, wiced_bool_t credential_flag)
{
    uint8_t buf[1 + 2 + HEALTH_STATE_FAULTS_MAX_NUM];
    uint8_t *p;
    uint8_t idx;

    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_HEALTH_CURRENT_STATUS);
    *p++ = health_nvm_state[nvm_idx].test_id;
    if (health_nvm_state[nvm_idx].faults_number == 0)
    {
        UINT16TOLE2(p, fnd_static_config_.cid);
        p += 2;
    }
    else
    {
        UINT16TOLE2(p, health_nvm_state[nvm_idx].company_id);
        p += 2;
        for (idx = 0; idx < health_nvm_state[nvm_idx].faults_number; idx++)
        {
            *p++ = health_nvm_state[nvm_idx].fault_array[idx];
        }
    }
    TRACE2(TRACE_DEBUG, "***** health_send_current_status_: test_id:%x faults_number:%x\n", health_nvm_state[nvm_idx].test_id, health_nvm_state[nvm_idx].faults_number);
    access_layer_send(buf, (uint16_t)(p - &buf[0]), app_key_idx, 0, src_id, ttl, credential_flag, NULL, NULL);
}

static void health_handle_period_set_(wiced_bool_t unreliable, uint8_t period, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id)
{
    // The values other tan 0 through 15 are prohinited
    if (period > 15)
        return;
    if (period != health_nvm_state[nvm_idx].fast_period_log)
    {
        health_nvm_state[nvm_idx].fast_period_log = period;
        health_write_config_();
    }
    if (!unreliable)
        health_send_status_(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS, period, app_key_idx, src_id);
}

static void health_faults_set_(uint8_t nvm_idx, uint8_t test_id, uint16_t company_id, uint8_t faults_number, uint8_t *fault_array)
{
    if (test_id != health_nvm_state[nvm_idx].test_id || company_id != health_nvm_state[nvm_idx].company_id)
    {
        health_nvm_state[nvm_idx].test_id = test_id;
        health_nvm_state[nvm_idx].company_id = company_id;
        health_nvm_state[nvm_idx].faults_number = faults_number;
        if (faults_number)
            memcpy(health_nvm_state[nvm_idx].fault_array, fault_array, faults_number);
        health_write_config_();
    }
    else
    {
        uint8_t     idx, idx_out, cnt;
        cnt = health_nvm_state[nvm_idx].faults_number;
        // go through each fault in the received fault_array
        for (idx = 0; idx < faults_number; idx++)
        {
            // exit loop if array is full
            if (cnt >= HEALTH_STATE_FAULTS_MAX_NUM)
                break;
            // find current fault in the fault_array
            for (idx_out = 0; idx_out < cnt; idx_out++)
            {
                if (fault_array[idx] == health_nvm_state[nvm_idx].fault_array[idx_out])
                    break;
            }
            // if current fault doesn't exist in the array then add it
            if (idx_out >= cnt)
                health_nvm_state[nvm_idx].fault_array[cnt++] = fault_array[idx];
        }
        // save status if array is changed
        if (cnt != health_nvm_state[nvm_idx].faults_number)
        {
            health_nvm_state[nvm_idx].faults_number = cnt;
            health_write_config_();
        }
    }
}

static void health_handle_fault_test_(wiced_bool_t unreliable, uint8_t test_id, uint16_t company_id, uint8_t element_idx, uint8_t nvm_idx, uint8_t app_key_idx, uint16_t src_id)
{
    uint8_t     fault_array[HEALTH_STATE_FAULTS_MAX_NUM];
    uint8_t     faults_number = 0;
    if (health_state_->fault_test_cb)
        faults_number = health_state_->fault_test_cb(element_idx, test_id, company_id, HEALTH_STATE_FAULTS_MAX_NUM, fault_array);

    TRACE3(TRACE_INFO, "health_handle_fault_test_: element_idx:%x test_id=%x company_id=%x\n", element_idx, test_id, company_id);
    TRACE4(TRACE_INFO, " nvm_idx:%x nvm_cnt:%x faults_number:%x unreliable:%x\n", nvm_idx, health_state_->count, faults_number, unreliable);

    // Ignore message with invalid test_id or company_id
    if (faults_number == 0xff)
        return;

    health_faults_set_(nvm_idx, test_id, company_id, faults_number, fault_array);

    if (!unreliable)
        health_send_fault_status_(company_id, nvm_idx, app_key_idx, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2);
}

/**
* \brief Creates application payload for message Health Fault Test or Health Fault Test Unreliable
*
* @param[in]   unreliable          :WICED_TRUE - creat Health Fault Test Unreliable. WICED_FALSE - create Health Fault Test
* @param[in]   test_id             :Identifier of a specific test to be performed
* @param[in]   company_id          :16-bit Bluetooth assigned Company Identifier
* @param[out]  buf                 :Bufer for command. Should be big enough
*
* @return      Length of the command in the buf. 0 on error.
*/
uint16_t health_crt_fault_test(wiced_bool_t unreliable, uint8_t test_id, uint16_t company_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, unreliable ? WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED : WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST) - buf);
    buf[len++] = test_id;
    UINT16TOLE2(&buf[len], company_id);
    len += 2;
    TRACE0(TRACE_INFO, "health_crt_fault_test:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
 * \brief Indicates the fault happened.
 * \details Application shall call that function to indicate the fault
 *
 * @param[in]   element_idx         :Index of the elemnt
 * @param[in]   test_id             :Identifier of a specific test to be performed
 * @param[in]   company_id          :Company ID of the test
 * @param[in]   faults_number       :Number of faults in FaultArray
 * @param[out]  fault_array         :FaultArray
 *
 * @return      WICED_TRUE if accepted. Otherwise returns WICED_FALSE.
 */
wiced_bool_t wiced_bt_mesh_core_health_set_faults(uint8_t element_idx, uint8_t test_id, uint16_t company_id, uint8_t faults_number, uint8_t *fault_array)
{
    uint8_t                 nvm_idx;
    tHEALTH_STATE_ELEMENT   *p_state;

    // find related model config and health state
    for (nvm_idx = 0; nvm_idx < health_state_->count; nvm_idx++)
    {
        p_state = &health_state_->elements[nvm_idx];
        if (p_state->p_elem->idx == element_idx)
            break;
    }

    TRACE3(TRACE_INFO, "health_set_faults: element_idx:%x test_id=%x company_id=%x\n", element_idx, test_id, company_id);
    TRACE3(TRACE_INFO, " nvm_idx:%x nvm_cnt:%x fault_array_size:%x\n", nvm_idx, health_state_->count, faults_number);
    if (nvm_idx >= health_state_->count)
        return WICED_FALSE;

    health_faults_set_(nvm_idx, test_id, company_id, faults_number, fault_array);

    return WICED_TRUE;
}

