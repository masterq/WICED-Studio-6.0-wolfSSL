
// BtInterface.h : header file
//

#pragma once

class CBtInterface
{
public:
    CBtInterface (BLUETOOTH_ADDRESS *bth, HMODULE hLib, LPVOID NotificationContext, BOOL isWin8) {m_bth = *bth; m_hLib = hLib; m_NotificationContext = NotificationContext; m_bWin8 = isWin8; };

    virtual BOOL Init() = NULL;
    virtual BOOL SetDescriptorValue(const GUID *p_guidServ, const GUID *p_guidChar, USHORT uuidDescr, BTW_GATT_VALUE *pValue) = NULL;
    virtual BOOL WriteCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar, BOOL without_resp, BTW_GATT_VALUE *pValue) = NULL;

    BOOL SendWsUpgradeCommand(BTW_GATT_VALUE *pValue);
    BOOL SendWsUpgradeCommand(BYTE Command);
    BOOL SendWsUpgradeCommand(BYTE Command, USHORT sParam);
    BOOL SendWsUpgradeCommand(BYTE Command, ULONG lParam);
    BOOL SendWsUpgradeData(BYTE *Data, DWORD len);

    BLUETOOTH_ADDRESS m_bth;
    HMODULE m_hLib;
    LPVOID m_NotificationContext;
    BOOL m_bWin8;
};

