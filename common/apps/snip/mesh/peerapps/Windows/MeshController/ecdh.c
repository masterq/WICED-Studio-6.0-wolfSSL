/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
#include "wiced_bt_ble.h"
#include "ecdh.h"

#include "p_256_ecc_pp.h"

#define DO_REVERSE
//#define DO_REVERSE2

#define BT_OCTET32_LEN    32
typedef uint8_t BT_OCTET32[BT_OCTET32_LEN];   /* octet array: size 32 */

/**
* Initializes of ECDH parameters.
*
* Parameters:   None
*
* Return:   None
*
*/
void ecdh_init_curve(void)
{
    // initialization of P-256 parameters
    p_256_init_curve(KEY_LENGTH_DWORDS_P256);
}

#ifdef DO_REVERSE
#ifdef DO_REVERSE2
static void memcpy_r(void* to, const void* from, uint8_t len)
{
    uint8_t i;
    for (i = 0; i < len; i++)
    {
        ((uint8_t*)to)[i] = ((uint8_t*)from)[len - 1 - i];
    }
}
#else
#define BE4_TO_UINT32(p) ((uint32_t)(p)[3] + (((uint32_t)(p)[2])<<8) + (((uint32_t)(p)[1])<<16) + (((uint32_t)(p)[0])<<24))
//#define BE_STREAM_TO_UINT32(u32, p) {u32 = ((uint32_t)(*((p) + 3)) + ((uint32_t)(*((p) + 2)) << 8) + ((uint32_t)(*((p) + 1)) << 16) + ((uint32_t)(*(p)) << 24)); (p) += 4;}
//#define UINT32_TO_BE_STREAM(p, u32) {*(p)++ = (uint8_t)((u32) >> 24);  *(p)++ = (uint8_t)((u32) >> 16); *(p)++ = (uint8_t)((u32) >> 8); *(p)++ = (uint8_t)(u32); }

void memcpy_r(void* pv_to, const void* pv_from, uint8_t len)
//static void memcpy_r(uint32_t* to, const uint8_t* from, uint8_t len)
{
    uint32_t ui;
    uint8_t i;
    uint32_t* to = (uint32_t*)pv_to;
    const uint8_t* from = (uint8_t*)pv_from;
    for (i = 0; i < len/4; i++)
    {
        BE_STREAM_TO_UINT32(ui, from);
        to[len/4 - 1 - i] = ui;
    }
}
#endif
#endif

/**
* Generates ECDH public key from private key.
*
* Parameters:
*   priv_key:   ECDH private key(length WICED_BT_MESH_PROVISION_PRIV_KEY_LEN)
*   pub_key:    Buffer for generated ECDH public key (length WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN)
*
* Return:   None
*
*/
void ecdh_create_pub_key(const uint8_t* priv_key, uint8_t* pub_key)
{
    Point       public_key;
    BT_OCTET32  private_key;

#ifndef DO_REVERSE
    memcpy(private_key, priv_key, BT_OCTET32_LEN);
    ECC_PM(&public_key, &(curve_p256.G), (DWORD*)private_key, KEY_LENGTH_DWORDS_P256);
    memcpy(pub_key, public_key.x, BT_OCTET32_LEN);
    memcpy(pub_key + BT_OCTET32_LEN, public_key.y, BT_OCTET32_LEN);
#else
    memcpy_r(private_key, priv_key, BT_OCTET32_LEN);
    ECC_PM(&public_key, &(curve_p256.G), (DWORD*)private_key, KEY_LENGTH_DWORDS_P256);
    memcpy_r(pub_key, public_key.x, BT_OCTET32_LEN);
    memcpy_r(pub_key + BT_OCTET32_LEN, public_key.y, BT_OCTET32_LEN);
#endif
}

/**
* Generates ECDH shared secret from peer public key and own private key.
*
* Parameters:
*   peer_pub_key:   ECDH public key of the peer device (length WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN).
*   priv_key:       own ECDH private key (length WICED_BT_MESH_PROVISION_PRIV_KEY_LEN)
*   ecdh_secret:    Buffer for generated ECDH shared secret(length WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN)
*
* Return:   None
*
*/
void ecdh_calc_secret(const uint8_t* peer_pub_key, const uint8_t* priv_key, uint8_t* secret)
{
    Point       peer_publ_key, new_publ_key;
    BT_OCTET32  private_key;

#ifndef DO_REVERSE
    memcpy(private_key, priv_key, BT_OCTET32_LEN);
    memcpy(peer_publ_key.x, peer_pub_key, BT_OCTET32_LEN);
    memcpy(peer_publ_key.y, peer_pub_key + BT_OCTET32_LEN, BT_OCTET32_LEN);
#else
    memcpy_r(private_key, priv_key, BT_OCTET32_LEN);
    memcpy_r(peer_publ_key.x, peer_pub_key, BT_OCTET32_LEN);
    memcpy_r(peer_publ_key.y, peer_pub_key + BT_OCTET32_LEN, BT_OCTET32_LEN);
#endif

    ECC_PM(&new_publ_key, &peer_publ_key, (DWORD*)private_key, KEY_LENGTH_DWORDS_P256);

#ifndef DO_REVERSE
    memcpy(secret, new_publ_key.x, BT_OCTET32_LEN);
#else
    memcpy_r(secret, new_publ_key.x, BT_OCTET32_LEN);
#endif
}

