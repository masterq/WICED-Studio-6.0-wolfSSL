#pragma once
#include "afxwin.h"

#include <bthdef.h>

// CDeviceSelect dialog

#define WM_USER_DEV_UUID     (WM_USER + 0x102)

class CDeviceSelect : public CDialogEx
{
	DECLARE_DYNAMIC(CDeviceSelect)

public:
    CDeviceSelect(BOOL select_uuid = FALSE, CWnd* pParent = NULL);   // standard constructor
	virtual ~CDeviceSelect();

// Dialog Data
	enum { IDD = IDD_SELECT_DEVICE };

    BOOL    m_select_uuid;
    BYTE    m_uuid[16];
    BLUETOOTH_ADDRESS m_bth;
    BOOL m_bWin8;

private:
    int m_numDevices;

protected:
	HICON m_hIcon;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnDblclkDeviceList();
    virtual BOOL OnInitDialog();
    LRESULT OnDevUuid(WPARAM op, LPARAM lparam);
    CListBox m_lbDevices;
    afx_msg void OnBnClickedOk();
    afx_msg void OnBnClickedCancel();
};
