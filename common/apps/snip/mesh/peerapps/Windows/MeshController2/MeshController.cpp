
// MeshController.cpp : Defines the class behaviors for the application.
//
#include "stdafx.h"
#include "btwleapis.h"
#include "MeshController.h"
#include "MeshControllerDlg.h"
#include "DeviceSelect.h"
#include "DeviceSelectAdv.h"
#include "BTFullLibPath.h"

#include <VersionHelpers.h>


#include <Sddl.h>

#include <WinBase.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

GUID guidSvcWSUpgrade;
GUID guidCharWSUpgradeControlPoint;
GUID guidCharWSUpgradeData;

extern "C" BOOL mesh_info_trace_enabled = TRUE;
//extern "C" void mesh_test_keys();
extern "C" void ecdh_init_curve(void);


// CMeshControllerApp

BEGIN_MESSAGE_MAP(CMeshControllerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CMeshControllerApp construction

CMeshControllerApp::CMeshControllerApp()
{
}


// The one and only CMeshControllerApp object

CMeshControllerApp theApp;

BOOL IsOSWin10()
{
    BOOL bIsWin10 = FALSE;

	bIsWin10 = IsWindows10OrGreater();

	DWORD dwVersion = 0;
	DWORD dwMajorVersion = 0;
	DWORD dwMinorVersion = 0;
	DWORD dwBuild = 0;

	dwVersion = GetVersion();

	// Get the Windows version.
	dwMajorVersion = (DWORD)(LOBYTE(LOWORD(dwVersion)));
	dwMinorVersion = (DWORD)(HIBYTE(LOWORD(dwVersion)));

	// Get the build number.
	if (dwVersion < 0x80000000)
		dwBuild = (DWORD)(HIWORD(dwVersion));

	char buf[100];
	memset(buf, 0, sizeof(buf));
	sprintf_s(buf, "Version is %d.%d (%d)\n", dwMajorVersion, dwMinorVersion, dwBuild);

	if (dwMajorVersion == 10 && dwBuild < 15063)
		return FALSE;

    return bIsWin10;
}

BOOL IsOSWin8()
{
	BOOL bIsWin8 = FALSE;

	OSVERSIONINFOEX osvi;
	DWORDLONG dwlConditionMask = 0;

	// Initialize the OSVERSIONINFOEX structure.
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	osvi.dwMajorVersion = 6;
	osvi.dwMinorVersion = 2;
	osvi.wServicePackMajor = 0;

	// Initialize the condition mask.
	VER_SET_CONDITION( dwlConditionMask, VER_MAJORVERSION, VER_GREATER_EQUAL );
	VER_SET_CONDITION( dwlConditionMask, VER_MINORVERSION, VER_GREATER_EQUAL );
	VER_SET_CONDITION( dwlConditionMask, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL );

	// Perform the test.
	if(VerifyVersionInfo(&osvi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, dwlConditionMask))
	{
		bIsWin8 = TRUE;
	}

	return bIsWin8;
}

BOOL IsOSWin7()
{
	BOOL bIsWinSeven = FALSE;

	OSVERSIONINFOEX osvi;
	DWORDLONG dwlConditionMask = 0;

	// Initialize the OSVERSIONINFOEX structure.
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
	osvi.dwMajorVersion = 6;
	osvi.dwMinorVersion = 1;
	osvi.wServicePackMajor = 0;

	// Initialize the condition mask.
	VER_SET_CONDITION( dwlConditionMask, VER_MAJORVERSION, VER_GREATER_EQUAL );
	VER_SET_CONDITION( dwlConditionMask, VER_MINORVERSION, VER_GREATER_EQUAL );
	VER_SET_CONDITION( dwlConditionMask, VER_SERVICEPACKMAJOR, VER_GREATER_EQUAL );

	// Perform the test.
	if(VerifyVersionInfo(&osvi, VER_MAJORVERSION | VER_MINORVERSION | VER_SERVICEPACKMAJOR, dwlConditionMask))
	{
		bIsWinSeven = TRUE;
	}

	return bIsWinSeven;
}

//#define _DBG_TEST_CRC
#ifdef _DBG_TEST_CRC
extern "C" UINT8 pb_crc8_updateBlock(UINT8* data, UINT32 len, UINT8 seed);
#endif

// CMeshControllerApp initialization

/*
extern "C" void AES_CMAC(unsigned char *key, unsigned char *input, int length, unsigned char *mac);
extern "C" void ble_tracen(const char *p_str, UINT32 len);
static void test_aes(void)
{
    UINT8 key[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
    //UINT8 key[16] = { 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
    UINT8 input[] = { 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
    //UINT8 input[] = { 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7 };
    UINT8 mac[16] = { 0 };
    AES_CMAC(key, input, sizeof(input), mac);
    ble_tracen((char*)mac, 16);
}
*/

void BtwGuidFromGuid(GUID *pOut, const GUID *pIn)
{
    pOut->Data1 = (pIn->Data4[4] << 24) + (pIn->Data4[5] << 16) + (pIn->Data4[6] << 8) + pIn->Data4[7];
    pOut->Data2 = (pIn->Data4[2] << 8) + pIn->Data4[3];
    pOut->Data3 = (pIn->Data4[0] << 8) + pIn->Data4[1];
    pOut->Data4[0] = (pIn->Data3) & 0xff;
    pOut->Data4[1] = (pIn->Data3 >> 8) & 0xff;
    pOut->Data4[2] = (pIn->Data2) & 0xff;
    pOut->Data4[3] = (pIn->Data2 >> 8) & 0xff;
    pOut->Data4[4] = (pIn->Data1) & 0xff;
    pOut->Data4[5] = (pIn->Data1 >> 8) & 0xff;
    pOut->Data4[6] = (pIn->Data1 >> 16) & 0xff;
    pOut->Data4[7] = (pIn->Data1 >> 24) & 0xff;
}


DWORD MakeSDAbsolute(PSECURITY_DESCRIPTOR psidOld, PSECURITY_DESCRIPTOR *psidNew)
{
	PSECURITY_DESCRIPTOR  pSid = NULL;
	DWORD                 cbDescriptor = 0;
	DWORD                 cbDacl = 0;
	DWORD                 cbSacl = 0;
	DWORD                 cbOwnerSID = 0;
	DWORD                 cbGroupSID = 0;
	PACL                  pDacl = NULL;
	PACL                  pSacl = NULL;
	PSID                  psidOwner = NULL;
	PSID                  psidGroup = NULL;
	BOOL                  fPresent = FALSE;
	BOOL                  fSystemDefault = FALSE;
	DWORD                 dwReturnValue = ERROR_SUCCESS;

	// Get SACL
	if (!GetSecurityDescriptorSacl(psidOld, &fPresent, &pSacl, &fSystemDefault))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

	if (pSacl && fPresent)
	{
		cbSacl = pSacl->AclSize;
	}

	// Get DACL
	if (!GetSecurityDescriptorDacl(psidOld, &fPresent, &pDacl, &fSystemDefault))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

	if (pDacl && fPresent)
	{
		cbDacl = pDacl->AclSize;
	}

	// Get Owner
	if (!GetSecurityDescriptorOwner(psidOld, &psidOwner, &fSystemDefault))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

	cbOwnerSID = GetLengthSid(psidOwner);

	// Get Group
	if (!GetSecurityDescriptorGroup(psidOld, &psidGroup, &fSystemDefault))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

	cbGroupSID = GetLengthSid(psidGroup);

	// Do the conversion
	cbDescriptor = 0;

	MakeAbsoluteSD(psidOld, pSid, &cbDescriptor, pDacl, &cbDacl, pSacl,
		&cbSacl, psidOwner, &cbOwnerSID, psidGroup,
		&cbGroupSID);

	pSid = (PSECURITY_DESCRIPTOR)malloc(cbDescriptor);
	if (!pSid)
	{
		dwReturnValue = ERROR_OUTOFMEMORY;
		goto CLEANUP;
	}

	ZeroMemory(pSid, cbDescriptor);

	if (!InitializeSecurityDescriptor(pSid, SECURITY_DESCRIPTOR_REVISION))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

	if (!MakeAbsoluteSD(psidOld, pSid, &cbDescriptor, pDacl, &cbDacl, pSacl,
		&cbSacl, psidOwner, &cbOwnerSID, psidGroup,
		&cbGroupSID))
	{
		dwReturnValue = GetLastError();
		goto CLEANUP;
	}

CLEANUP:

	if (dwReturnValue != ERROR_SUCCESS && pSid)
	{
		free(pSid);
		pSid = NULL;
	}

	*psidNew = pSid;

	return dwReturnValue;
}

BOOL CMeshControllerApp::InitInstance()
{
#ifdef _DBG_TEST_CRC
    BYTE buf1[] = { 0 };
    BYTE buf2[] = { 0x02, 0x00, 0x00, 0x00, 0x00, 0x00 };
    BYTE buf3[] = { 0x03, 0xe4, 0xfb, 0x0b, 0x5f, 0xa0, 0x06, 0x60, 0x5d, 0xdd, 0xc7, 0x5f, 0x5e, 0x7d, 0x5c, 0x09,
                    0x06, 0xd7, 0x83, 0x8c, 0xc1, 0x63, 0x93, 0x00, 0xc2, 0xbe, 0x4e, 0x72, 0x42, 0x8a, 0xf5, 0x17,
                    0xde, 0xb7, 0x08, 0x58, 0x93, 0x66, 0xac, 0x26, 0x00, 0x93, 0x52, 0x42, 0x33, 0x7d, 0x12, 0xfa,
                    0x87, 0xed, 0x93, 0x96, 0x26, 0x28, 0x3b, 0xc4, 0xa3, 0x2a, 0xd9, 0x8f, 0x84, 0xa6, 0x09, 0xdc,
                    0xba };
    BYTE crc1 = pb_crc8_updateBlock(buf1, sizeof(buf1), 0xff);  //0x30
    BYTE crc2 = pb_crc8_updateBlock(buf2, sizeof(buf2), 0xff);  //0x64
    BYTE crc3 = pb_crc8_updateBlock(buf3, sizeof(buf3), 0xff);  //0x22
#endif

    //test_aes();

    // InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	//==== CoInitializeEx and Security for WRL LE Functionality ====//
	HRESULT hr = NULL;

	if (IsOSWin10())
	{
		hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

		const WCHAR* security = L"O:BAG:BAD:(A;;0x7;;;PS)(A;;0x3;;;SY)(A;;0x7;;;BA)(A;;0x3;;;AC)(A;;0x3;;;LS)(A;;0x3;;;NS)";
		PSECURITY_DESCRIPTOR pSecurityDescriptor;
		ULONG securityDescriptorSize;

		if (!ConvertStringSecurityDescriptorToSecurityDescriptor(
			security,
			SDDL_REVISION_1,
			&pSecurityDescriptor,
			&securityDescriptorSize))
		{
			return FALSE;
		}

		// MakeSDAbsolute as defined in
		// https://github.com/pauldotknopf/WindowsSDK7-Samples/blob/master/com/fundamentals/dcom/dcomperm/SDMgmt.Cpp
		PSECURITY_DESCRIPTOR pAbsoluteSecurityDescriptor = NULL;
		MakeSDAbsolute(pSecurityDescriptor, &pAbsoluteSecurityDescriptor);

		HRESULT hResult = CoInitializeSecurity(
			pAbsoluteSecurityDescriptor, // Converted from the above string.
			-1,
			nullptr,
			nullptr,
			RPC_C_AUTHN_LEVEL_DEFAULT,
			RPC_C_IMP_LEVEL_IDENTIFY,
			NULL,
			EOAC_NONE,
			nullptr);
		if (FAILED(hResult))
		{
			return FALSE;
		}
	}
	//==== CoInitializeEx and Security for WRL LE Functionality ====//


    ecdh_init_curve();
    //mesh_test_keys();

	CMeshControllerDlg dlg;

	// Lets check if BT is plugged in
	HANDLE hRadio = INVALID_HANDLE_VALUE;
	BLUETOOTH_FIND_RADIO_PARAMS params = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};
	HBLUETOOTH_RADIO_FIND hf = BluetoothFindFirstRadio(&params, &hRadio);
	if (hf == NULL)
	{
		MessageBox(NULL, L"Bluetooth radio is not present or not functioning.  Please plug in the dongle and try again.", L"Error", MB_OK);
		return FALSE;
	}
    CloseHandle(hRadio);
    BluetoothFindRadioClose(hf);

	HMODULE hLib = NULL;

    dlg.m_bWin10 = dlg.m_bWin8 = FALSE;

	// This application supports Microsoft on Win8 or BTW on Win7.
    if (IsOSWin10())
    {
        dlg.m_bWin10 = TRUE;

        guidSvcWSUpgrade = GUID_OTA_FW_UPGRADE_SERVICE;
        guidCharWSUpgradeControlPoint = GUID_OTA_FW_UPGRADE_CHARACTERISTIC_CONTROL_POINT;
        guidCharWSUpgradeData = GUID_OTA_FW_UPGRADE_CHARACTERISTIC_DATA;
    }
    else if (IsOSWin8())
	{
		dlg.m_bWin8 = TRUE;
        if ((hLib = LoadLibrary(L"BluetoothApis.dll")) == NULL)
		{
			MessageBox(NULL, L"Failed to load BluetoothAPIs library", L"Error", MB_OK);
			return FALSE;
		}
        guidSvcWSUpgrade = GUID_OTA_FW_UPGRADE_SERVICE;
        guidCharWSUpgradeControlPoint = GUID_OTA_FW_UPGRADE_CHARACTERISTIC_CONTROL_POINT;
        guidCharWSUpgradeData = GUID_OTA_FW_UPGRADE_CHARACTERISTIC_DATA;
    }
	else if (IsOSWin7())
	{
		//dlg.m_bWin8 = FALSE;
        //TCHAR BtDevFullPath[MAX_PATH+1] = { '\0' };
        //CBTFullLibPath LibPath;
        //LibPath.GetFullInstallPathOf(L"BTWLeApi.Dll", BtDevFullPath, MAX_PATH);
        //if ((hLib = LoadLibrary(BtDevFullPath)) == NULL)

        if ((hLib = LoadLibrary(L"BTWLeApi.Dll")) == NULL)
        //if ((hLib = LoadLibrary(L"WicedLeApi.DLL")) == NULL)
		{
			MessageBox(NULL, L"Broadcom Blueototh profile pack for Windows (BTW) has to be installed", L"Error", MB_OK);
			return FALSE;
		}
        BtwGuidFromGuid(&guidSvcWSUpgrade, &GUID_OTA_FW_UPGRADE_SERVICE);
        BtwGuidFromGuid(&guidCharWSUpgradeControlPoint, &GUID_OTA_FW_UPGRADE_CHARACTERISTIC_CONTROL_POINT);
        BtwGuidFromGuid(&guidCharWSUpgradeData, &GUID_OTA_FW_UPGRADE_CHARACTERISTIC_DATA);
    }
	else
	{
		MessageBox(NULL, L"This application can run on Windows 8 or on Windows 7 with Broadcom Bluetooth profile pack for Windows (BTW) installed", L"Error", MB_OK);
		return FALSE;
	}

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	SetRegistryKey(_T("Cypress"));

    CDeviceSelect dlgDeviceSelect;
    CDeviceSelectAdv dlgDeviceSelectAdv;

	INT_PTR nResponse;

	if (dlg.m_bWin8)
	{
		dlgDeviceSelect.m_bWin8 = dlg.m_bWin8;
		dlgDeviceSelect.m_bth.ullLong = 0;
		nResponse = dlgDeviceSelect.DoModal();
	}
	else if (dlg.m_bWin10)
	{
		dlgDeviceSelectAdv.m_bWin8 = dlg.m_bWin8;
		dlgDeviceSelectAdv.m_bth.ullLong = 0;
		nResponse = dlgDeviceSelectAdv.DoModal();
	}


	if ((nResponse == IDOK))// && (dlgDeviceSelect.m_bth.ullLong != 0))
	{
        OutputDebugStringW(L"CMeshControllerDlg::dlg.hLib");

		if(dlg.m_bWin8)
		{
			dlg.SetParam(&dlgDeviceSelect.m_bth, hLib);
		}
		else if (dlg.m_bWin10)
		{
			dlg.SetParam(&dlgDeviceSelectAdv.m_bth, hLib);
		}

	    m_pMainWnd = &dlg;

        OutputDebugStringW(L"CMeshControllerDlg::dlg.DoModal();");

	    nResponse = dlg.DoModal();
	    if (nResponse == IDOK)
	    {

	    }
	    else if (nResponse == IDCANCEL)
	    {

	    }
	    else if (nResponse == -1)
	    {
		    TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		    TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	    }
    }

    OutputDebugStringW(L"Exiting Mesh");

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

    if (dlg.m_hLib != NULL)
        FreeLibrary(dlg.m_hLib);

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


extern "C" void ods(char * fmt_str, ...)
{
	char buf[1000] = {0};
	va_list marker = NULL;

	va_start (marker, fmt_str );

    SYSTEMTIME st;
    GetLocalTime(&st);

    int len = sprintf_s(buf, sizeof(buf), "%02d:%02d:%02d.%03d ", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
    vsnprintf_s(&buf[len], sizeof(buf)-len, _TRUNCATE, fmt_str, marker);
	va_end  ( marker );

    strcat_s(buf, sizeof(buf), "\n");
	OutputDebugString(CA2W(buf));	
}

void BdaToString (PWCHAR buffer, BLUETOOTH_ADDRESS *btha)
{
	WCHAR c;
	for (int i = 0; i < 6; i++)
	{
		c = btha->rgBytes[5 - i] >> 4;
		buffer[2 * i    ] = c < 10 ? c + '0' : c + 'A' - 10;
		c = btha->rgBytes[5 - i] & 0x0f;
		buffer[2 * i + 1] = c < 10 ? c + '0' : c + 'A' - 10;
	}
}


void UuidToString(LPWSTR buffer, size_t buffer_size, GUID *uuid)
{
	// Example {00001101-0000-1000-8000-00805F9B34FB}
    _swprintf_p(buffer, buffer_size, L"{%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X}", uuid->Data1, uuid->Data2,
			uuid->Data3, uuid->Data4[0], uuid->Data4[1], uuid->Data4[2], uuid->Data4[3],
			uuid->Data4[4], uuid->Data4[5], uuid->Data4[6], uuid->Data4[7]);
}


/**
* mesh trace functions.
* These are just wrapper function for WICED trace function call. We use these
* wrapper functions to make the mesh code easier to port on different platforms.
*/
extern "C" void ble_trace0(const char *p_str)
{
    ods((char *)p_str);
}

extern "C" void ble_trace1(const char *fmt_str, UINT32 p1)
{
    ods((char *)fmt_str, p1);
}

extern "C" void ble_trace2(const char *fmt_str, UINT32 p1, UINT32 p2)
{
    ods((char *)fmt_str, p1, p2);
}

extern "C" void ble_trace3(const char *fmt_str, UINT32 p1, UINT32 p2, UINT32 p3)
{
    ods((char *)fmt_str, p1, p2, p3);
}

extern "C" void ble_trace4(const char *fmt_str, UINT32 p1, UINT32 p2, UINT32 p3, UINT32 p4)
{
    ods((char *)fmt_str, p1, p2, p3, p4);
}

extern "C" void ble_tracen(const char *p_str, UINT32 len)
{
    char buf[100];
    memset(buf, 0, sizeof(buf));
    unsigned int i;
    while (len)
    {
        for (i = 0; i < len && i < 16; i++)
            sprintf_s(&buf[3 * i], 100 - (3 * i), "%02x ", (UINT8)p_str[i]);
        buf[3 * i] = 0;
        ods(buf);
        len -= i;
        p_str += i;
    }
}

extern "C" int wiced_printf(char * buffer, int len, char * fmt_str, ...)
{
    char buf[2048];
    va_list va;
    va_start(va, fmt_str);
    vsprintf_s(buf, sizeof(buf), fmt_str, va);
    ods(buf);
    va_end(va);
    return 0;
}

extern "C" void* wiced_memory_allocate(UINT32 length)
{
    return malloc(length);
}
extern "C" void* wiced_memory_permanent_allocate(UINT32 length)
{
    return malloc(length);
}
extern "C" void wiced_memory_free(void *memoryBlock)
{
    free(memoryBlock);
}

