/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
#include <stdio.h> 
#include <string.h> 
#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_aes_ccm.h"
#include "core_ovl.h"
#include "mesh_util.h"
#include "provisioning_int.h"
#include "ecdh.h"
#include "key_refresh.h"
#include "iv_updt.h"
#include "low_power.h"
#include "friend.h"
#include "transport_layer.h"
#include "foundation.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__CORE_AES_CCM_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__CORE_AES_CCM_C
#include "mesh_trace.h"

#ifdef MESH_CONTROLLER
#define AES_CMAC_BE AES_CMAC
#else
#if MESH_CHIP == 20703
typedef void (*AES_CMAC_T)(uint8_t *key, uint8_t *input, uint16_t length, uint16_t tlen, uint8_t *p_signature);
AES_CMAC_T p_AES_CMAC = (AES_CMAC_T)0x000b92c7;
#elif MESH_CHIP == 20719
extern void AES_CMAC(uint8_t *key, uint8_t *input, uint16_t length, uint16_t tlen, uint8_t *p_signature);
#else
extern void AES_CMAC(uint8_t *key, uint8_t *input, uint16_t length, uint16_t tlen, uint8_t *p_signature);
#endif
void invert(uint8_t* in, uint8_t* out, uint16_t len)
{
    uint16_t i;
    for (i = 0; i < len; i++)
    {
        out[i] = in[len - 1 - i];
    }
}

void AES_CMAC_BE(unsigned char *key, unsigned char *input, int length, unsigned char *mac)
{
    uint8_t key_le[16];
    uint8_t input_le[150]; //max length is 145 - length of ConfirmationInputs on calcualtion of ConfirmationSalt
    uint8_t mac_le[16];
    invert(key, key_le, 16);
    invert(input, input_le, length);
#if MESH_CHIP == 20703
    p_AES_CMAC(key_le, input_le, length, 16, mac_le);
#elif MESH_CHIP == 20719
    AES_CMAC(key_le, input_le, length, 16, mac_le);
#else
    AES_CMAC(key_le, input_le, length, 16, mac_le);
#endif

    invert(mac_le, mac, 16);
}
#endif

// For debugging - updates current iv-index if received packet contains bigger one
//#define _DEB_UPDATE_IV_IDX_ON_RECEIVED_PACKET

/**
* Does Network Layer Obfuscation.
* Privacy Random = (encDST || encTransportPDU || NetMIC )[0�6]
* PECB = AES (PrivacyKey, 0x0000000000 || IV Index || Privacy Random)
* ObfuscatedData = (CTL || TTL || SEQ || SRC) ^ PECB[0�5]
*
* Parameters:
*   CT:             pointer to the first field in the packet to be obfuscated (starting from field CT)
*   encDST:         pointer to the first encrypted field in the packet to use for privacy counter for obfuscation
*   iv_index:       IV index
*   privacy_key:    network privacy key
*
* Return:   None.
*
*/
static void mesh_obfuscation(uint8_t* FMT, const uint8_t* encDST, const uint8_t* iv_index, uint8_t* privacy_key)
{
    uint8_t   privacy_counter[16];
    uint8_t   pecb[16];
    uint8_t   i;
    aes_context  aes[1];

    TRACE1(TRACE_DEBUG, "obfuscation: len = %d iv_index, privacy_key, privacy_counter, pecb, before, after:\n", MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);
    TRACEN(TRACE_DEBUG, (char*)iv_index, MESH_IV_INDEX_LEN);

    mesh_core_boost_cpu(WICED_TRUE);
    // Privacy Random = (encDST || encTransportPDU || NetMIC)[0�6]
    // 0x0000000000 || IVindex || Privacy Random
    memset(&privacy_counter[0], 0, 5);
    memcpy(&privacy_counter[5], iv_index, MESH_IV_INDEX_LEN);
    memcpy(&privacy_counter[5 + MESH_IV_INDEX_LEN], encDST, 7);
    // PECB = AES (PriKey, privacy_counter)
    smp_aes_set_key(privacy_key, MESH_KEY_LEN, aes);
#ifndef MESH_CONTROLLER
    smp_aes_encrypt(privacy_counter, pecb, aes);
#else
    aes_encrypt(privacy_counter, pecb, aes);
#endif
    mesh_core_boost_cpu(WICED_FALSE);

    //TRACE1(TRACE_INFO, "obfuscation: len = %d privacy_key, privacy_counter, pecb, before, after:\n", MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);
    TRACEN(TRACE_DEBUG, (char*)privacy_key, MESH_KEY_LEN);
    TRACEN(TRACE_DEBUG, (char*)privacy_counter, sizeof(privacy_counter));
    TRACEN(TRACE_DEBUG, (char*)pecb, sizeof(pecb));
    TRACEN(TRACE_DEBUG, (char*)FMT, MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);

    // obfuscatedData = (CTL || TTL || SEQ || SRC) ^ PECB[0-5]
    for (i = 0; i < MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN; i++)
    {
        FMT[i] ^= pecb[i];
    }
    TRACEN(TRACE_DEBUG, (char*)FMT, MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);
}

/**
* Builds nonce
*
* Parameters:
*   nonce:          Buffer for nonce with length MESH_NONCE_LEN. On return contains:
*                   dev: 0x0200 || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || IV-INDEX(4bytes)
*                   app: 0x0100 || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || IV-INDEX(4bytes)
*                   net: 0x00 || CTL(1bit) || TTL(7bits) || SEQ(3bytes) || SRC(2bytes) || 0x0000 || IV-INDEX(4bytes)
*   type:           Type of the nonce. Can be any of MESH_NONCE_TYPE_XXX
*   iv_index:       IV index
*   seq_src_dst:    pointer on the place in the packet with SEQ || SRC || DST
*   ttl:            TTL - needed only for Network type of nonce
*   ctl:            CTL flag
*   aszmic:         WICED_TRUE if SZMIC is ON and a Segmented Access Message or WICED_FALSE for all other message formats
*                   Valid only for type values MESH_NONCE_TYPE_APP and MESH_NONCE_TYPE_DEV
*
* Return: None
*
*/
void mesh_build_nonce(uint8_t* nonce, uint8_t type, const uint8_t* iv_index, const uint8_t* seq_src_dst, uint8_t ttl, wiced_bool_t ctl, wiced_bool_t aszmic)
{
    int pos = 0;
    // 0x0200 or 0x0100 or (0x00 || CTL(1bit) || TTL(7bits))
    nonce[pos++] = type;
    if (type == MESH_NONCE_TYPE_NET)
        nonce[pos++] = ttl | (ctl ? MESH_CTL_MASK : 0);
    else if (type == MESH_NONCE_TYPE_APP || type == MESH_NONCE_TYPE_DEV)
        nonce[pos++] = aszmic ? 0x80 : 0x00;
    else // if (type == MESH_NONCE_TYPE_PROXY)
        nonce[pos++] = 0;
    // SEQ(3bytes) || SRC(2bytes)
    memcpy(&nonce[pos], seq_src_dst, MESH_SEQ_SRC_LEN);
    pos += MESH_SEQ_SRC_LEN;
    // DST(2bytes) or 0x0000
    if (type == MESH_NONCE_TYPE_NET || type == MESH_NONCE_TYPE_PROXY)
        memset(&nonce[pos], 0, MESH_NODE_ID_LEN);
    else
        memcpy(&nonce[pos], seq_src_dst + MESH_SEQ_SRC_LEN, MESH_NODE_ID_LEN);
    pos += MESH_NODE_ID_LEN;
    // IV-INDEX(4bytes)
    memcpy(&nonce[pos], iv_index, MESH_IV_INDEX_LEN);
    pos += MESH_IV_INDEX_LEN;

    TRACE0(TRACE_DEBUG, "build_nonce: nonce:\n");
    TRACEN(TRACE_DEBUG, (char*)nonce, pos);
}

/**
* Does net decryption of the mesh packet with specific security material.
*
* Parameters:
*   proxy_nonce:    WICED_TRUE - use proxy nonce. WICED_FALSE - use NET nonce
*   sec_material:   Security material to use for decryption
*   adv_data:       Received packet.
*                   Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits))
*                   || SEQ(3bytes) || SRC(2bytes) || DST(2bytes)
*                   || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
*   adv_len:        Length of the received packet.
*   msg:            Buffer for decrypted packet. Should be suficiently big (ADV_LEN_MAX).
*   p_iv_index:     IV index
*
* Return:   WICED_TRUE/WICED_FALSE - decrypt succeded/failed.
*
*/
wiced_bool_t mesh_sec_material_decrypt(wiced_bool_t proxy_nonce, MeshSecMaterial *sec_material, const BYTE* adv_data, BYTE adv_len, BYTE* msg, uint8_t* p_iv_index)
{
    ccm_ctx     ccm_node;
    ret_type    ret = RETURN_ERROR;
    uint8_t       *dest_id, *net_mic, net_mic_len;
    uint8_t       nonce[MESH_NONCE_LEN];

    TRACE3(TRACE_INFO, "mesh_sec_material_decrypt: sec_material->nid:%x packet_nid:%x proxy_nonce:%d [encr_key:]\n", sec_material->nid, adv_data[0] & 0x7f, proxy_nonce);

    // The first byte of the adv packet is key_id and iv_index
    // ignore key if its Network id(NID) doesn't match the NID from the received packet
    if (sec_material->nid != (adv_data[0] & 0x7f))
        return WICED_FALSE;

    TRACEN(TRACE_DEBUG, (char*)sec_material->encr_key, MESH_KEY_LEN);

    // copy packet to the temp buffer msg and decrypt/authenticate it there with current encr_key
    memcpy(msg, adv_data, adv_len);

    // Do Network Layer Obfuscation
    mesh_obfuscation(&msg[MESH_IVI_NID_LEN],
        &msg[MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN],
        p_iv_index,
        sec_material->privacy_key);

    // MIC len can be 32 or 64 bits depending on CTL flag
    net_mic_len = (msg[MESH_IVI_NID_LEN] & MESH_CTL_MASK) == 0 ? MESH_MIC_LEN : MESH_MIC_LEN2;
    net_mic = msg + adv_len - net_mic_len;
    dest_id = msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN;

    // Build net nonce
    mesh_build_nonce(nonce,
        proxy_nonce ? MESH_NONCE_TYPE_PROXY : MESH_NONCE_TYPE_NET,
        p_iv_index,
        msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN,
        msg[MESH_IVI_NID_LEN] & MESH_TTL_MASK,
        (msg[MESH_IVI_NID_LEN] & MESH_CTL_MASK) != 0, WICED_FALSE);

    mesh_core_boost_cpu(WICED_TRUE);

    ret = ccm_init_and_key(sec_material->encr_key, MESH_KEY_LEN, &ccm_node);
    if (ret == RETURN_GOOD)
    {

        // decrypt and authenticate
        ret = ccm_decrypt_message(nonce, MESH_NONCE_LEN,
            NULL, 0,                                // no additional data for authentication but not encryption
            dest_id, (uint32_t)(net_mic - dest_id),   // data for decryption and authentication
            net_mic, net_mic_len,                   // net MIC
            &ccm_node);
    }

    mesh_core_boost_cpu(WICED_FALSE);

    if (ret != RETURN_GOOD)
    {
        TRACE1(TRACE_WARNING, "mesh_sec_material_decrypt: ccm init/decrypt failed, ret = %d\n", ret);
        return WICED_FALSE;
    }
    TRACEN(TRACE_DEBUG, (char*)msg, adv_len - net_mic_len);
    return WICED_TRUE;
}

/**
* Does net decryption of the mesh packet trying different net keys
* and different iv indices if needed.
*
* Parameters:
*   proxy_nonce:    WICED_TRUE - use proxy nonce. WICED_FALSE - use NET nonce
*   adv_data:       Received packet.
*                   Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits))
*                   || SEQ(3bytes) || SRC(2bytes) || DST(2bytes)
*                   || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
*   adv_len:        Length of the received packet.
*   msg:            Buffer for decrypted packet. Should be suficiently big (ADV_LEN_MAX).
*   p_iv_index:     Buffer for selected IV index
*   p_net_key_idx:  Buffer for the net key
*
* Return:   WICED_TRUE - decrypted successfully.
*           WICED_FALSE - drop that packet.
*
*/
wiced_bool_t mesh_net_decrypt(wiced_bool_t proxy_nonce, const BYTE* adv_data, BYTE adv_len, BYTE* msg, uint8_t* p_iv_index, uint8_t* p_net_key_idx)
{
    uint8_t   net_key_idx;

    // Per spec we can use current and previous IV index. Select which one matches mwssage's bit
    if ((node_nv_data.net_iv_index[MESH_IV_INDEX_LEN - 1] & 1) == (adv_data[0] >> 7))
    {
        memcpy(p_iv_index, node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);
    }
    else
    {
        uint32_t iv_index = BE4TOUINT32(node_nv_data.net_iv_index);
        iv_index--;
        UINT32TOBE4(p_iv_index, iv_index);
    }
    TRACE0(TRACE_INFO, "net_decrypt: iv_index\n");
    TRACEN(TRACE_INFO, (char*)p_iv_index, MESH_IV_INDEX_LEN);

    // go through all network keys
    for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
    {
        // ignore unexisting key index
        if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
            continue;

        TRACE2(TRACE_INFO, "net_decrypt: net_key_idx:%d proxy_nonce:%d\n", net_key_idx, proxy_nonce);
        if (mesh_sec_material_decrypt(proxy_nonce, &node_nv_net_key[net_key_idx].sec_material, adv_data, adv_len, msg, p_iv_index))
            break;
    }

    // if we were not able to decrypt message
    if (net_key_idx >= MESH_NET_KEY_MAX_NUM)
    {
        return WICED_FALSE;
    }

    *p_net_key_idx = net_key_idx;

    return WICED_TRUE;
}

/**
* Calculates all friend values(keys and IDs) derived from the network key.
*
* Parameters:
*   lpn_addr:       The unicast address set as source address in the Friend Request message.
*   friend_addr:    The unicast address set as source address in the Friend Offer message.
*   lpn_counter:    The value from the LPNCounter field of the Friend Request message.
*   friend_counter: The value from the FriendCounter field of the Friend Offer message.
*   net_key:        Network key to use for calculations.
*   nid:            7 bit NID (from k2)
*   encr_key:       Buffer for Encryption key with length MESH_KEY_LEN (from k2).
*   privacy_key:    Buffer for Privacy key with length MESH_KEY_LEN (from k2).
*
* Return:   None.
*/
void calc_net_security_friend(uint16_t lpn_addr, uint16_t friend_addr, uint16_t lpn_counter, uint16_t friend_counter,
    const uint8_t* net_key, uint8_t* nid, uint8_t* encr_key, uint8_t* privacy_key)
{
    uint8_t p[1 + 2 + 2 + 2 + 2];
    // NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x01 || LPNAddress || FriendAddress || LPNCounter || FriendCounter)
    p[0] = 0x01;
    UINT16TOBE2(&p[1], lpn_addr);
    UINT16TOBE2(&p[1 + 2], friend_addr);
    UINT16TOBE2(&p[1 + 2 + 2], lpn_counter);
    UINT16TOBE2(&p[1 + 2 + 2 + 2], friend_counter);
    mesh_k2(net_key, p, sizeof(p), nid, encr_key, privacy_key);
    TRACE1(TRACE_INFO, "calc_net_security_friend: nid:%x param: net_key: encr_key: privacy_key\n", *nid);
    TRACEN(TRACE_DEBUG, (char*)p, sizeof(p));
    TRACEN(TRACE_DEBUG, (char*)net_key, MESH_KEY_LEN);
    TRACEN(TRACE_DEBUG, (char*)encr_key, MESH_KEY_LEN);
    TRACEN(TRACE_DEBUG, (char*)privacy_key, MESH_KEY_LEN);
}


/**
* Calculates all values(keys and IDs) derived from the networ key.
*
* Parameters:
*   net_key:        Network key to use for calculations.
*   nid:            7 bit NID (from k2)
*   encr_key:       Buffer for Encryption key with length MESH_KEY_LEN (from k2).
*   privacy_key:    Buffer for Privacy key with length MESH_KEY_LEN (from k2).
*   network_id:     Buffer for Network ID with length MESH_NETWORK_ID_LEN (from k3).
*   identity_key:   Buffer for Identity key with length MESH_KEY_LEN.
*   beacon_key:     Buffer for Beacon key with length MESH_KEY_LEN.
*   proxy_key:      Buffer for Proxy key with length MESH_KEY_LEN.
*
* Return:   None.
*/
void calc_net_security(const uint8_t* net_key, uint8_t* nid, uint8_t* encr_key, uint8_t* privacy_key, uint8_t* network_id, uint8_t* identity_key, uint8_t* beacon_key, uint8_t* proxy_key)
{
    uint8_t p[6];
    uint8_t s[MESH_KEY_LEN];
    // NID || EncryptionKey || PrivacyKey = k2(NetKey, 0x00)
    p[0] = 0x00;
    mesh_k2(net_key, p, 1, nid, encr_key, privacy_key);
    // Network ID = k3 (NetKey)
    mesh_k3(net_key, network_id);
    // salt = s1 (�nkik�)
    // P = �id128� || 0x01
    // IdentityKey = k1(NetKey, salt, P)
    mesh_s1("nkik", 4, s);
    memcpy(p, "id128", 5);
    p[5] = 0x01;
    mesh_k1(net_key, MESH_KEY_LEN, s, MESH_KEY_LEN, p, 6, identity_key);
    // salt = s1 (�nkbk�)
    // P = �id128� || 0x01
    // BeaconKey = k1(NetKey, salt, P)
    mesh_s1("nkbk", 4, s);
    mesh_k1(net_key, MESH_KEY_LEN, s, MESH_KEY_LEN, p, 6, beacon_key);
    // salt = s1 (�nkpk�)
    // P = �id128� || 0x01
    // ProxyKey  = k1(NetKey, salt, P)
    mesh_s1("nkpk", 4, s);
    mesh_k1(net_key, MESH_KEY_LEN, s, MESH_KEY_LEN, p, 6, proxy_key);
}

/**
* Encrypts or decrypts and authenticate data.
*
* Parameters:
*   enc:            WICED_TRUE - encrypt.
*                   WICED_FALSE - decrypt.
*   key:            Encryption/decryption key.
*   nonce:          Nonce for encryption/decryption.
*   payload:        Payload to encrypt/decrypt.
*   payload_len:    Length of the payload to encrypt/decrypt.
*   mic_len:        Length of the MIC field
*   label_uuid:     Optional (can be NULL) additional data
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - failed.
*
*/
wiced_bool_t ccm_crypt(wiced_bool_t enc, uint8_t* key, uint8_t* nonce, uint8_t* payload, uint32_t payload_len, uint8_t mic_len, uint8_t *label_uuid)
{
    ccm_ctx     ccm_node;
    ret_type    ret;

    TRACE2(TRACE_INFO, "ccm_crypt: enc:%d mic_len:%d key, nonce, data before, data after, [label:]\n", enc, mic_len);
    TRACEN(TRACE_DEBUG, (char*)key, MESH_KEY_LEN);
    TRACEN(TRACE_DEBUG, (char*)nonce, MESH_NONCE_LEN);
    TRACEN(TRACE_DEBUG, (char*)payload, enc ? payload_len : payload_len + mic_len);
    if(label_uuid)
        TRACEN(TRACE_DEBUG, (char*)label_uuid, 16);

    mesh_core_boost_cpu(WICED_TRUE);

    ret = ccm_init_and_key(key, MESH_KEY_LEN, &ccm_node);
    if (ret == RETURN_GOOD)
    {
        if (enc)
        {
            ret = ccm_encrypt_message(nonce, MESH_NONCE_LEN,
                label_uuid, label_uuid == NULL ? 0 : 16,            // optional additional data for authentication but not encryption
                payload, payload_len,                               // data for encryption and authentication
                &payload[payload_len], mic_len,                     //MIC
                &ccm_node);
        }
        else
        {
            ret = ccm_decrypt_message(nonce, MESH_NONCE_LEN,
                label_uuid, label_uuid == NULL ? 0 : 16,            // optional additional data for authentication but not encryption
                payload, payload_len,                               // data for decryption and authentication
                &payload[payload_len], mic_len,                     //MIC
                &ccm_node);
        }
    }

    mesh_core_boost_cpu(WICED_FALSE);

    TRACEN(TRACE_DEBUG, (char*)payload, enc ? payload_len + mic_len : payload_len);

    if (ret != RETURN_GOOD)
    {
        TRACE1(TRACE_WARNING, "ccm_crypt: ccm init/encrypt/decrypt failed, ret = %d\n", ret);
    }


    return ret == RETURN_GOOD;
}

/**
* Creates nonce, encrypts or decrypts and authenticates data.
*
* Parameters:
*   enc:            WICED_TRUE - encrypt.
*                   WICED_FALSE - decrypt.
*   type:           Type of the nonce. Can be any of MESH_NONCE_TYPE_XXX
*   iv_index:       IV index
*   seq_src_dst:    pointer on the place in the packet with SEQ || SRC || DST
*   ttl:            TTL - needed only for Network type of nonce
*   ctl:            CTL flag - needed only for Network type of nonce
*   szmic:          WICED_TRUE means long(8 bytes) MIC length; WICED_FALSE means short(4 bytes) MIC length
*   key:            Encryption/decryption key.
*   payload:        Payload to encrypt/decrypt.
*   payload_len:    Length of the payload to encrypt/decrypt.
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - failed.
*
*/
wiced_bool_t mesh_ccm_crypt(wiced_bool_t enc, uint8_t type, const uint8_t* iv_index, const uint8_t* seq_src_dst, uint8_t ttl, wiced_bool_t ctl, wiced_bool_t szmic, uint8_t* key, uint8_t* payload, uint32_t payload_len)
{
    uint8_t    nonce[MESH_NONCE_LEN];
    uint8_t    *label_uuid = NULL;
    // for application encryption/decryption use label UUID for virtual address and fail for unknown virtual address
    if (type == MESH_NONCE_TYPE_APP)
    {
        uint16_t   dst = BE2TOUINT16(seq_src_dst + MESH_SEQ_SRC_LEN);
        if ((dst & MESH_GROUP_MASK) == MESH_VIRT_ADDR)
        {
            label_uuid = foundation_find_virt_addr(dst);
            if (label_uuid == NULL)
            {
                TRACE1(TRACE_WARNING, "mesh_ccm_crypt: unknown virt addr:%x\n", dst);
                return WICED_FALSE;
            }
        }
    }

    // Build app or net nonce
    mesh_build_nonce(nonce, type, iv_index, seq_src_dst, ttl, ctl, szmic);

    return ccm_crypt(enc, key, nonce, payload, payload_len, szmic ? MESH_MIC_LEN2 : MESH_MIC_LEN, label_uuid);
}

/**
* Does net encryption or decryption of the ota fw upgrade data.
*
* Parameters:
*   enc:            WICED_TRUE - encrypt.
*                   WICED_FALSE - decrypt.
*   in_data:        OTA FW upgrade data.
*   data_len:       Length of the OTA FW upgrade data.
*   out_data:       Buffer for output result data. Should be suficiently big.
*
* Return:   Length of the output result data in the buffer out_data
*
*/
uint16_t mesh_ota_fw_upgrade_crypt(wiced_bool_t enc, const uint8_t* in_data, uint16_t data_len, uint8_t* out_data)
{
    ret_type    ret = RETURN_ERROR;
    uint8_t     nonce[MESH_NONCE_LEN];
    uint8_t     seq_src_dst[MESH_SEQ_SRC_LEN];

    TRACE2(TRACE_INFO, "mesh_ota_fw_upgrade_crypt: enc:%d data_len:%d encr_key, out_data:\n", enc, data_len);
    TRACEN(TRACE_DEBUG, (char*)node_nv_net_key[0].sec_material.encr_key, MESH_KEY_LEN);

    // Build net nonce
    memset(seq_src_dst, 0, sizeof(seq_src_dst));
    mesh_build_nonce(nonce, MESH_NONCE_TYPE_NET, node_nv_data.net_iv_index, seq_src_dst, 0, WICED_FALSE, WICED_FALSE);

    // copy packet to the output buffer out_data
    if(out_data != in_data)
        memcpy(out_data, in_data, data_len);

    if (!enc)
    {
        if (data_len <= MESH_MIC_LEN)
            data_len = 0;
        else
            data_len -= MESH_MIC_LEN;
    }

    if (data_len)
    {
        if (!ccm_crypt(enc, node_nv_net_key[0].sec_material.encr_key, nonce, out_data, data_len, MESH_MIC_LEN, NULL))
            data_len = 0;
        else if (enc)
        {
            data_len += MESH_MIC_LEN;
        }
    }

    TRACE1(TRACE_INFO, "mesh_ota_fw_upgrade_crypt: returns %d\n", data_len);
    return data_len;
}

/**
* For network packet creates nonce, encrypts or decrypts, authenticates data and then does obfuscation
*
* Parameters:
*   p_sec_material: Secure material to use for encryption
*   nonce_type:     Type of the nonce. Can be MESH_NONCE_TYPE_NET or MESH_NONCE_TYPE_PROXY
*   iv_index:       IV index
*   msg:            Payload to encrypt/decrypt.
*   msg_len:        Length of the payload to encrypt/decrypt.
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - failed.
*
*/
wiced_bool_t mesh_ccm_encrypt_net(MeshSecMaterial *p_sec_material, uint8_t nonce_type, const uint8_t* iv_index, uint8_t* msg, uint32_t msg_len)
{
    wiced_bool_t ret;
    uint8_t   hdr_len = MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN;
    ret = mesh_ccm_crypt(WICED_TRUE, nonce_type, iv_index,
        msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN,
        msg[MESH_IVI_NID_LEN] & MESH_TTL_MASK,
        (msg[MESH_IVI_NID_LEN] & MESH_CTL_MASK) != 0,
        (msg[MESH_IVI_NID_LEN] & MESH_CTL_MASK) != 0,
        p_sec_material->encr_key,
        msg + hdr_len, msg_len - hdr_len);
    if (ret)
    {
        // Do Network Layer Obfuscation
        mesh_obfuscation(&msg[MESH_IVI_NID_LEN], msg + hdr_len,
            iv_index,
            p_sec_material->privacy_key);
    }
    return ret;
}

/**
* Calculates virtual address 10vv vvvv vvvv vvvv form the 16 bytes virtual_label_uuid
* The fourteen v-bits are the least significant bits of the following calculation
* SALT = s1 (�vtad�)
* v = AES-CMAC (SALT, virtual_label_uuid) mod 2**14;
*
* Parameters:
*   virtual_label_uuid:     16 bytes of virtual label UUID
*   addr:                   var to receive calculated virtual address
*
* Return:   None
*/
void mesh_calc_virt_addr(const uint8_t *virtual_label_uuid, uint16_t *addr)
{
    uint8_t out[16];
    mesh_s1("vtad", 4, out);
    AES_CMAC_BE(out, (uint8_t*)virtual_label_uuid, 16, out);
    *addr = (BE2TOUINT16(&out[14])  & ~MESH_GROUP_MASK) | MESH_VIRT_ADDR;
}

/*
* \breaf Calculates URI hash URI Hash: s1(URI Data)[0-3]
*
* @param[in]   uri         :URI data: <scheme(1byte: 0x16-"http:" or 0x17-"https:")><uri with removed scheme(max 29bytes)>
* @param[in]   uri_len     :Length of the URI data <= 29
* @param[out]  hash        :buffer to received calculated URI hash
*/
void wiced_bt_mesh_core_calc_uri_hash(const uint8_t *uri, uint8_t len, uint8_t *hash)
{
    uint8_t out[MESH_KEY_LEN];
    mesh_s1(uri, len, out);
    memcpy(hash, &out[MESH_KEY_LEN - 4], 4);
}

/**
* Calculates peer conformation.
* ConfirmationProvisioner = AES-CMAC (ConfirmationKey , RandomProvisioner || AuthValue)
* or ConfirmationDevice = AES-CMAC (ConfirmationKey, RandomDevice || AuthValue)
*
* Parameters:
*   conn_idx:       Index of the provisioning controll block in the array pb_cb
*   random:         Pointer to random received from peer device
*   conformation:   buffer to receive calculated conformation
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - failed.
*/
static void pb_calc_conformation_(uint32_t conn_idx, const uint8_t* random, uint8_t *conformation)
{
    uint8_t buf[WICED_BT_MESH_PROVISION_RANDOM_LEN + MESH_AUTH_VALUE_MAX_LEN];
    memcpy(&buf[0], random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
    // AuthValue is 16 bytes always (oob with 0 padding)
    memset(&buf[WICED_BT_MESH_PROVISION_RANDOM_LEN], 0, 16);
    if (pb_cb[conn_idx]->oob_value_len)
        memcpy(&buf[WICED_BT_MESH_PROVISION_RANDOM_LEN], pb_cb[conn_idx]->oob_value, pb_cb[conn_idx]->oob_value_len);
    AES_CMAC_BE(pb_cb[conn_idx]->conf_key, (uint8_t*)&buf[0], WICED_BT_MESH_PROVISION_RANDOM_LEN + 16, conformation);

    TRACE1(TRACE_INFO, "calc_conformation: auth_value_len=%d random, auth_value, conf:\n", pb_cb[conn_idx]->oob_value_len);
    TRACEN(TRACE_DEBUG, (char*)random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
    if (pb_cb[conn_idx]->oob_value_len)
        TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->oob_value, pb_cb[conn_idx]->oob_value_len);
    TRACEN(TRACE_DEBUG, (char*)conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);

}

/**
* Static function to calculate different values related to provisioning authentication.
*
* Parameters:
*   conn_idx:       Index of the provisioning controll block in the array pb_cb
*   ecdh_secret:    ECDH shared secret. If NULL then it is calculated here.
*   provisioner:    WICED_TRUE - provisioner authentication.
*                   WICED_FALSE - provisioning authentication.
*
* Return:   None.
*/
void pb_calc_authentication(uint32_t conn_idx, uint8_t *ecdh_secret, wiced_bool_t provisioner)
{
    uint8_t len = 0;
    // we will use it for ConfirmationInputs
    uint8_t buf[1 + sizeof(pb_cb[conn_idx]->capabilities) + sizeof(pb_cb[conn_idx]->start) + (2 * WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN)];
    uint8_t zeros[MESH_KEY_LEN];
    memset(zeros, 0, sizeof(zeros));

    TRACE2(TRACE_INFO, "authentication: conn_idx:%d provisioner:%d inputs, conf_salt, ecdh_secret, conf_key, random, conformation\n", conn_idx, provisioner);

    // create ConfirmationInputs:= ProvisioningInvitePDUValue || ProvisioningCapabilitiesPDUValue || ProvisioningStartPDUValue || PublicKeyProvisioner || PublicKeyDevice
    // use buf as ConfirmationInputs
    len = 0;
    buf[len++] = pb_cb[conn_idx]->invite_identify;
    memcpy(&buf[len], &pb_cb[conn_idx]->capabilities, sizeof(pb_cb[conn_idx]->capabilities));
    len += sizeof(pb_cb[conn_idx]->capabilities);
    memcpy(&buf[len], &pb_cb[conn_idx]->start, sizeof(pb_cb[conn_idx]->start));
    len += sizeof(pb_cb[conn_idx]->start);
    memcpy(&buf[len],
        provisioner ? pb_public_key : pb_cb[conn_idx]->peer_public_key_or_ecdh_secret,
        WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
    len += WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN;
    memcpy(&buf[len],
        provisioner ? pb_cb[conn_idx]->peer_public_key_or_ecdh_secret : pb_public_key,
        WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN);
    len += WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN;

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)buf, len);

    // ConfirmationSalt = AES-CMAC(0x00000000000000000000000000000000, ConfirmationInputs)
    AES_CMAC_BE(&zeros[0], &buf[0], len, pb_cb[conn_idx]->prov_salt);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN);

    // if NULL then calculate ECDH secret
    if (ecdh_secret == NULL)
    {
        ecdh_calc_secret(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, pb_priv_key, buf);
        ecdh_secret = buf;
    }
    memcpy(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN);

    // calculate ConfirmationKey = k1(ECDHSecret, ConfirmationSalt, �prck�)
    mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prck", 4, pb_cb[conn_idx]->conf_key);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->conf_key, MESH_KEY_LEN);

#ifndef HARDCODED_RANDOM
    // generate random number
    wiced_bt_mesh__generate_random(pb_cb[conn_idx]->random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
#else
    if (provisioner)
    {
        //uint8_t rand[16] = { 0x8b, 0x19, 0xac, 0x31, 0xd5, 0x8b, 0x12, 0x4c, 0x94, 0x62, 0x09, 0xb5, 0xdb, 0x10, 0x21, 0xb9 };
        uint8_t rand[16] = { HARDCODED_RANDOM_PROVISIONER };
        memcpy(pb_cb[conn_idx]->random, rand, 16);
    }
    else
    {
        //uint8_t rand[16] = { 0x55, 0xa2, 0xa2, 0xbc, 0xa0, 0x4c, 0xd3, 0x2f, 0xf6, 0xf3, 0x46, 0xbd, 0x0a, 0x0c, 0x1a, 0x3a };
        uint8_t rand[16] = { HARDCODED_RANDOM_DEVICE };
        memcpy(pb_cb[conn_idx]->random, rand, 16);
    }
#endif

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->random, MESH_KEY_LEN);

    // calculate ConfirmationProvisioner = AES-CMAC (ConfirmationKey , RandomProvisioner || AuthValue)
    // or ConfirmationDevice = AES-CMAC (ConfirmationKey , RandomDevice || AuthValue)
    pb_calc_conformation_(conn_idx, pb_cb[conn_idx]->random, pb_cb[conn_idx]->conformation);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);

}

/**
* Function to calculate session key for data emcryption/decrypton/authentication.
*
* Parameters:
*   conn_idx:       Index of the provisioning controll block in the array pb_cb
*   peer_random:    Random of the peer device.
*   provisioner:    WICED_TRUE - provisioner authentication.
*                   WICED_FALSE - provisioning authentication.
*
* Return:   None.
*/
static void pb_calc_session_key_(uint32_t conn_idx, const uint8_t *peer_random, wiced_bool_t provisioner)
{
    uint8_t len = 0;
    uint8_t buf[sizeof(pb_cb[conn_idx]->prov_salt) + (2 * WICED_BT_MESH_PROVISION_RANDOM_LEN)];
    uint8_t zeros[MESH_KEY_LEN];
    memset(zeros, 0, sizeof(zeros));

    TRACE2(TRACE_INFO, "session_key: conn_idx:%d provisioner:%d salt_data, prov_salt, ecdh_secret, session_key\n", conn_idx, provisioner);
    // create salt_data:= ConformationSalt || RandomProvisioner || RandomDevice
    // use buf as ConfirmationInputs
    len = 0;
    memcpy(&buf[len], pb_cb[conn_idx]->prov_salt, sizeof(pb_cb[conn_idx]->prov_salt));
    len += sizeof(pb_cb[conn_idx]->prov_salt);
    memcpy(&buf[len], provisioner ? pb_cb[conn_idx]->random : peer_random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
    len += WICED_BT_MESH_PROVISION_RANDOM_LEN;
    memcpy(&buf[len], provisioner ? peer_random : pb_cb[conn_idx]->random, WICED_BT_MESH_PROVISION_RANDOM_LEN);
    len += WICED_BT_MESH_PROVISION_RANDOM_LEN;

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)buf, len);

    // ProvisioningSalt = AES-CMAC(0x00000000000000000000000000000000, salt_data)
    AES_CMAC_BE(&zeros[0], &buf[0], len, pb_cb[conn_idx]->prov_salt);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN);

    // calculate SessionKey = k1(ECDHSecret, ProvisioningSalt, �prsk�)
    mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prsk", 4, pb_cb[conn_idx]->session_key);

    TRACE0(TRACE_INFO, "\n");
    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->session_key, MESH_KEY_LEN);
}

/**
* Calculates peer conformation and compares it with received conformation.
* ConfirmationProvisioner = AES-CMAC (ConfirmationKey , RandomProvisioner || AuthValue)
* or ConfirmationDevice = AES-CMAC (ConfirmationKey, RandomDevice || AuthValue)
*
* Parameters:
*   conn_idx:       Index of the provisioning controll block in the array pb_cb
*   random:         Pointer to random received from peer device
*   provisioner:    WICED_TRUE - provisioner authentication.
*                   WICED_FALSE - provisioning authentication.
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - failed.
*/
wiced_bool_t pb_check_conformation(uint32_t conn_idx, const uint8_t* random, wiced_bool_t provisioner)
{
    uint8_t buf[WICED_BT_MESH_PROVISION_RANDOM_LEN + MESH_AUTH_VALUE_MAX_LEN];

    TRACE1(TRACE_INFO, "check_conformation: provisioner:%d received conf:\n", provisioner);

    pb_calc_session_key_(conn_idx, random, provisioner);
    pb_calc_conformation_(conn_idx, random, buf);

    TRACEN(TRACE_DEBUG, (char*)pb_cb[conn_idx]->conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);

    // compare it with received conformation
    return 0 == memcmp(&buf[0], pb_cb[conn_idx]->conformation, WICED_BT_MESH_PROVISION_CONFORMATION_LEN);
}

/**
* creates provisioning_data for final message after authentication.
*
* Parameters:
*   conn_idx:       Index of the provisioning controll block in the array pb_cb
*   provisioning_data:  Buffer for provisioning_data:
*                       net_key(16bytes) || IVindex(4bytes) || unicast_address(2bytes)
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - error.
*
*/
wiced_bool_t pb_create_provisioning_data(uint32_t conn_idx, uint8_t* provisioning_data)
{
    uint8_t   nonce[MESH_KEY_LEN];

    TRACE1(TRACE_INFO, "create_provisioning_data: node_id:%04x net_key, iv_index:\n", pb_cb[conn_idx]->node_id);
    TRACEN(TRACE_DEBUG, (char*)node_nv_net_key[0].key, MESH_KEY_LEN);
    TRACEN(TRACE_DEBUG, (char*)node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);

    // net_key(16bytes) || NetKeyIdx(2bytes) || <KR, IV Update(1 byte)> ||IVindex(4bytes) || unicast_address(2bytes)
    // Copy Primary NetKey and its index
    memcpy(&provisioning_data[0], node_nv_net_key[0].key, MESH_KEY_LEN);
    UINT16TOLE2(&provisioning_data[MESH_KEY_LEN], node_nv_net_key[0].global_idx);
    //??? ToDo: use 0 flags for now
    provisioning_data[MESH_KEY_LEN + 2] = 0;
    // copy IV index to the output buffer provisioning_data
    memcpy(&provisioning_data[MESH_KEY_LEN + 2 + 1], node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);
    // copy unicast address to the output buffer provisioning_data
    UINT16TOBE2(&provisioning_data[MESH_KEY_LEN + 2 + 1 + MESH_IV_INDEX_LEN], pb_cb[conn_idx]->node_id);

#if !defined(HARDCODED_DEV_KEY) || defined(HARDCODED_PROVIS)
    //calculate DevKey just to print it DevKey = k1(ECDHSecret, ProvisioningSalt, �prdk�)
    mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prdk", 4, nonce);
    //TRACE0(TRACE_CRITICAL, "create_provisioning_data: dev_key:\n");
#if (TRACE_COMPILE_LEVEL <= 3)
    wiced_printf(NULL, 0, "create_provisioning_data: dev_key:****************************\n");
#endif
    TRACEN(TRACE_CRITICAL, (char*)nonce, MESH_KEY_LEN);
#endif
    // now we can encrypt it and authenticate: encData, MICdata = AES-CCM (SessionKey, Nonce, Data)
    // create The nonce of session = 13 LSB octets of Nonce = k1(ECDHSecret, ProvisioningSalt, �prsn�)
    mesh_k1(pb_cb[conn_idx]->peer_public_key_or_ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN, pb_cb[conn_idx]->prov_salt, MESH_KEY_LEN, "prsn", 4, nonce);
    // encrypt and authenticate data: encData, MICdata = AES-CCM (SessionKey, Nonce, Data)
    return ccm_crypt(WICED_TRUE, pb_cb[conn_idx]->session_key, &nonce[MESH_KEY_LEN - MESH_NONCE_LEN],
        provisioning_data, WICED_BT_MESH_PROVISION_DATA_LEN, WICED_BT_MESH_PROVISION_MIC_LEN, NULL);
}

/**
* Calculates Authentication value (4 bytes) of the secure network beacon.
*
* Parameters:
*   net_key_idx:    Local index of the net key
*   flags:          Beacon flags field
*   iv_index:       iv_index to use for auth value
*   auth:           Buffer 8(MESH_BEACON_CMAC_LEN) bytes length to save calculated authentication value
*
* Return: None
*/
void mesh_calc_beacon_auth(uint8_t net_key_idx, uint8_t flags, const uint8_t *iv_index, uint8_t* auth)
{
    uint8_t temp[32];
    uint8_t cmac[16];

    temp[0] = flags;
    memcpy(&temp[1], node_nv_net_key[net_key_idx].network_id, MESH_NETWORK_ID_LEN);
    memcpy(&temp[1 + MESH_NETWORK_ID_LEN], iv_index, MESH_IV_INDEX_LEN);

    TRACE0(TRACE_DEBUG, "calc_beacon_auth: temp:\n");
    TRACEN(TRACE_DEBUG, temp, 1 + MESH_NETWORK_ID_LEN + MESH_IV_INDEX_LEN);

    // generate CMAC
    AES_CMAC_BE(node_nv_net_key[net_key_idx].beacon_key, temp, 1 + MESH_NETWORK_ID_LEN + MESH_IV_INDEX_LEN, cmac);
    TRACEN(TRACE_DEBUG, cmac, MESH_KEY_LEN);
    memcpy(auth, cmac, MESH_BEACON_CMAC_LEN);
}

/**
* Checks if IV Update procedure is in progress.
*
* Parameters:   None
*
* Return: WICED_TRUE if IV Update procedure is in progress. Otherwise returns WICED_FALSE
*/
wiced_bool_t mesh_iv_update_active(void)
{
    return node_nv_data.iv_update_state == MESH_IV_UPDT_STATE_ACTIVE ? WICED_TRUE : WICED_FALSE;
}

/**
* Builds the the beacon data for secure network beacon.
*
* Parameters:
*   net_key_idx:    Local index of the network key
*   kr:             KR flag
*   is_iv_update:   WICED_TRUE if IV update procedure is active
*   adv:            Buffer to create beacon data:
*                   BeaconType=1(1byte) || Flags(1byte) || NID(8 LSB) || CIVI(4bytes) || CMAC(8bytes)
*
* Return: length of created beacon data
*
*/
uint8_t mesh_create_secure_network_beacon(uint8_t net_key_idx, wiced_bool_t kr, wiced_bool_t is_iv_update, uint8_t *adv)
{
    uint8_t adv_len;
    TRACE3(TRACE_DEBUG, "create_secure_network_beacon: net_key_idx:%x kr:%d is_iv_update:%d\n", net_key_idx, kr, is_iv_update);

    // Build the advertisement packet
    adv_len = 0;

    adv[adv_len++] = MESH_BEACON_TYPE_SECURE_NETWORK;
    adv[adv_len] = kr ? FLAG_KEY_REFRESH_SIGNALING_BIT : 0;
    if (is_iv_update)
        adv[adv_len] |= FLAG_IV_UPDATE_SIGNALING_BIT;
    adv_len++;

    memcpy(&adv[adv_len], &node_nv_net_key[net_key_idx].network_id, MESH_NETWORK_ID_LEN);
    adv_len += MESH_NETWORK_ID_LEN;
    memcpy(&adv[adv_len], &node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);
    adv_len += MESH_IV_INDEX_LEN;
    mesh_calc_beacon_auth(net_key_idx, adv[1], node_nv_data.net_iv_index, &adv[adv_len]);
    adv_len += MESH_BEACON_CMAC_LEN;

    return adv_len;
}

#ifndef MESH_CONTROLLER

/**
* Builds the the data for secure network beacon.
*
* Parameters:
*   net_key_idx:        Local index of the network key to check the state
*   enc_net_key_idx:    Local index of the network key for encryption
*   kr:                 KR flag
*   iv_updt:            WICED_TRUE if IV update procedure is active
*   adv:                Buffer to create advertisement packet
*
* Return: length of created advert packet
*
*/
static uint8_t create_secure_network_advert(uint8_t net_key_idx, uint8_t enc_net_key_idx, wiced_bool_t iv_updt, uint8_t *adv)
{
    uint8_t         adv_len = 0;
    wiced_bool_t    kr = key_refresh_is_phase2(net_key_idx);
    // Header for secure network beacon: Lengt(1byte) || AdType(1byte:ADV_TYPE_MESH_BEACON) || BeaconType(1byte:MESH_BEACON_TYPE_SECURE_NETWORK)
    // skip length field
    adv_len++;
    adv[adv_len++] = ADV_TYPE_MESH_BEACON;
    // if KR flag is set then use new netkey
    adv_len += mesh_create_secure_network_beacon(kr ? enc_net_key_idx : net_key_idx, kr, iv_updt, &adv[adv_len]);
    adv[0] = adv_len - 1;
    return adv_len;
}
#endif

/**
 * \brief Checks if received Service Data for Mesh Proxy Service with Node Identity corresponds to that node address
 * \details Application should call that function on each received advertizing with proxy service data for Mesh Proxy Service with Node Identity to check if it is came from specific node address.
 *  Example:
 *  if (NULL != (p_data = wiced_bt_ble_check_advertising_data(received_adv_data, 0x16, &len)))
 *      wiced_bt_mesh_core_check_node_identity(address, p_data, len, NULL);
 *
 * @param[in]   addr:           :Node address to check Identity for
 * @param[in]   p_data          :Received service data returned by wiced_bt_ble_check_advertising_data()
 * @param[in]   len             :Length of the received service data.
 * @param[out]  net_key_idx     :Optional (can be NULL) pointer to variable to receive matched global network index - it is valid on success only.
 *
 * @return      WICED_TRUE/WICED_FALSE - success/failed
 */
wiced_bool_t wiced_bt_mesh_core_check_node_identity(uint16_t addr, const uint8_t *data, uint8_t len, uint16_t *p_net_key_idx)
{
    wiced_bool_t    ret = WICED_FALSE;
    aes_context     aes[1];
    uint8_t         nonce[16], hash[16];
    uint8_t         net_key_idx;
    uint16_t        global_net_key_idx;

    mesh_core_boost_cpu(WICED_TRUE);

    // Accept if correct length
    if (len == 19
        && LE2TOUINT16(&data[0]) == WICED_BT_MESH_CORE_UUID_SERVICE_PROXY       // and it is Proxy service data
        && data[2] == WICED_BT_MESH_PROXY_IDENTIFICATION_TYPE_NODE_IDENTITY)    // and it is Node Identity
    {
        memset(&nonce[0], 0, 6);                    // 48 bits of padding
        memcpy(&nonce[6], &data[2 + 1 + 8], 8);     // random from beacon
        UINT16TOBE2(&nonce[14], addr);
        // go through all network keys
        for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
        {
            // ignore unexisting key index
            if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
                continue;
            // Hash = e(IdentityKey, Padding | Random | Address) mod 2**64
            smp_aes_set_key(node_nv_net_key[net_key_idx].identity_key, MESH_KEY_LEN, aes);
#ifndef MESH_CONTROLLER
            smp_aes_encrypt(nonce, hash, aes);
#else
            aes_encrypt(nonce, hash, aes);
#endif
            if (0 == memcmp(&data[2 + 1], &hash[8], 8))     // Hash mod 2**64
                break;
        }
        if (net_key_idx < MESH_NET_KEY_MAX_NUM)
        {
            global_net_key_idx = node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK;
            if (p_net_key_idx)
                *p_net_key_idx = global_net_key_idx;
            ret = WICED_TRUE;
        }
    }

    mesh_core_boost_cpu(WICED_FALSE);

    TRACE3(TRACE_DEBUG, "wiced_bt_mesh_core_check_node_identity: ret:%d len:%x addr:%x data:\n", ret, len, addr);
    if(ret)
        TRACE2(TRACE_DEBUG, " net_key_idx:%x global_net_key_idx:%x\n", net_key_idx, global_net_key_idx);
    TRACEN(TRACE_DEBUG, (char*)data, len);
    return ret;
}

#ifndef MESH_CONTROLLER
/**
* Builds the the data for proxy service advertising.
*
* Parameters:
*   net_key_idx:        Local index of the network key for checking the state
*   enc_net_key_idx:    Local index of the network key for encryption
*   adv:                Buffer to create advertisement packet
*
* Return: length of created advert packet
*
*/
static uint8_t create_proxy_svc_advert(uint8_t net_key_idx, uint8_t enc_net_key_idx, uint8_t *adv)
{
    uint8_t adv_len = 0;
    adv[adv_len++] = 1 + 1;
    adv[adv_len++] = BTM_BLE_ADVERT_TYPE_FLAG;
    adv[adv_len++] = BTM_BLE_GENERAL_DISCOVERABLE_FLAG | BTM_BLE_BREDR_NOT_SUPPORTED;

    adv[adv_len++] = 1 + 2;
    adv[adv_len++] = BTM_BLE_ADVERT_TYPE_16SRV_COMPLETE;
    UINT16TOLE2(&adv[adv_len], WICED_BT_MESH_CORE_UUID_SERVICE_PROXY);
    adv_len += 2;

    // If Node Identity state is ON or we received command to advert node identity for 60 seconds then use Node Identity. 
    if (node_identity_timer_net_key_idx == 0xfe || (node_nv_net_key[net_key_idx].state & MESH_NETKEY_STATE_IDENTITY_MASK) == FND_STATE_NODE_IDENTITY_ON)
    {
        aes_context aes[1];
        uint8_t     nonce[16], hash[16];
        memset(&nonce[0], 0, 6);  //48 bits of padding
        wiced_bt_mesh__generate_random(&nonce[6], 8);
        UINT16TOBE2(&nonce[14], node_nv_data.node_id);
        // Hash = e(IdentityKey, Padding | Random | Address) mod 2**64
        smp_aes_set_key(node_nv_net_key[enc_net_key_idx].identity_key, MESH_KEY_LEN, aes);
#ifndef MESH_CONTROLLER
        smp_aes_encrypt(nonce, hash, aes);
#else
        aes_encrypt(nonce, hash, aes);
#endif
        adv[adv_len++] = 1 + 2 + 1 + 8 + 8;
        adv[adv_len++] = BTM_BLE_ADVERT_TYPE_SERVICE_DATA;
        UINT16TOLE2(&adv[adv_len], WICED_BT_MESH_CORE_UUID_SERVICE_PROXY);
        adv_len += 2;
        adv[adv_len++] = WICED_BT_MESH_PROXY_IDENTIFICATION_TYPE_NODE_IDENTITY;
        memcpy(&adv[adv_len], &hash[8], 8);     // Hash mod 2**64
        adv_len += 8;
        memcpy(&adv[adv_len], &nonce[6], 8);    // random
        adv_len += 8;
    }
    // If Identity isn't on and proxy is ON then use NETWORK_ID advert type
    else if(node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)
    {
        adv[adv_len++] = 1 + 2 + 1 + MESH_NETWORK_ID_LEN;
        adv[adv_len++] = BTM_BLE_ADVERT_TYPE_SERVICE_DATA;
        UINT16TOLE2(&adv[adv_len], WICED_BT_MESH_CORE_UUID_SERVICE_PROXY);
        adv_len += 2;
        adv[adv_len++] = WICED_BT_MESH_PROXY_IDENTIFICATION_TYPE_NETWORK_ID;
        memcpy(&adv[adv_len], &node_nv_net_key[enc_net_key_idx].network_id, MESH_NETWORK_ID_LEN);
        adv_len += MESH_NETWORK_ID_LEN;
#ifdef _DEB_ADV_APPEARANCE
        adv[adv_len++] = 2 + 1;
        adv[adv_len++] = BTM_BLE_ADVERT_TYPE_APPEARANCE;
        UINT16TOLE2(&adv[adv_len], _DEB_ADV_APPEARANCE);
        adv_len += 2;
#endif
    }
    else
    {
        adv_len = 0;
    }
    return adv_len;
}

/**
* Updates proxy server advertisement and network secure beacon
*
* Parameters: None
*
* Return: None
*/
void mesh_update_beacon(void)
{
    uint8_t     adv[ADV_LEN_MAX];
    uint8_t     adv_len;
    uint8_t     net_key_idx;
    uint8_t     use_net_key_idx;

    TRACE3(TRACE_WARNING, "update_beacon: mesh_started:%d beacon_instance:%x conn_id:%x\n", ctx.mesh_started, ctx.beacon_instance[0], ctx.conn_id);
    TRACE3(TRACE_WARNING, " state_sec_net_beacon:%d state_gatt_proxy:%d conn_id:%x\n", node_nv_data.state_sec_net_beacon, node_nv_data.state_gatt_proxy, ctx.conn_id);
    // Stops proxy server advertisement and network secure beacon if they are running
    mesh_stop_advert();

    if (!ctx.mesh_started)
        return;

    // go through all network keys
    for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
    {
        // ignore unexisting key index
        if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
            continue;
        // ignore if it is not main key
        if (0 != (node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG))
            continue;
        // Use old key in phase 0 and 1
        if (((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT) < KEY_REFRESH_PHASE_STATE_2)
            use_net_key_idx = net_key_idx;
        else if (!find_netkey_((node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK) | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &use_net_key_idx))
        {
            TRACE1(TRACE_WARNING, "mesh_update_beacon: no new key in phase 2 for net_key_idx:%x use old one\n", net_key_idx);
            use_net_key_idx = net_key_idx;
        }
        TRACE4(TRACE_INFO, "mesh_update_beacon: net_key_idx:%x use_net_key_idx:%x iv_update_state:%d kr:%d\n", net_key_idx, use_net_key_idx, node_nv_data.iv_update_state, key_refresh_is_phase2(use_net_key_idx));

        // If proxy is supported and not connected then Start proxy service advertising with Network ID or Identity
        if (node_nv_data.state_gatt_proxy != FND_STATE_PROXY_NOT_SUPPORTED && !ctx.conn_id)
        {
            adv_len = create_proxy_svc_advert(net_key_idx, use_net_key_idx, adv);
            if (adv_len)
            {
                mesh_set_adv_params(0, 0xff, ctx.proxy_adv_interval, 0, 0, 0);
                ctx.proxy_adv_instance[net_key_idx] = mesh_start_adv(adv, adv_len, BTM_BLE_ADVERT_UNDIRECTED_HIGH, NULL, NULL);
            }
        }

        // if sec_net beacon is enabled then start beaconning
        if (node_nv_data.state_sec_net_beacon == FND_STATE_SEC_NET_BEACON_ON)
        {
            adv_len = create_secure_network_advert(net_key_idx, use_net_key_idx, mesh_iv_update_active(), adv);
            mesh_set_adv_params(0, 0xff, ctx.beacon_interval, 0, 0, 0);
            ctx.beacon_instance[net_key_idx] = mesh_start_adv(adv, adv_len, BTM_BLE_ADVERT_NONCONN_HIGH, NULL, NULL);
            // If proxy is enabled and connected then send that beacon to it
            if (ctx.conn_id && node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)
                split_proxy(PROXY_PDU_TYPE_BEACON, adv + 2, adv_len - 2, wiced_bt_mesh_core_gatt_send_max_len);
            else
                TRACE1(TRACE_INFO, "mesh_update_beacon: don't send to GATT. state_gatt_proxy:%d\n", node_nv_data.state_gatt_proxy);
        }
    }
}
#endif

//--------------------------- Security Toolbox --------------------------

/**
* SALT Generation Function.
* s1(M) = AES-CMAC_ZERO (M)
*
* Parameters:
*   m:          non-zero length ASCII encoded string
*   m_len:      Length of the m (>0)
*   out:        Buffer for result with length MESH_KEY_LEN.
*
* Return:   None
*
*/
void mesh_s1(const char* m, uint32_t m_len, uint8_t* out)
{
    uint8_t zero[MESH_KEY_LEN];
    memset(zero,0,sizeof(zero));
    AES_CMAC_BE(zero, (uint8_t*)m, m_len, out);
}

/**
* same as mesh_k1 but with possible shorter salt to be padded mwith 0.
*
* Parameters:
*   n:          Data for first AES_CMAC_BE (>=0 bytes).
*   n_len:      Length of the n (>=0)
*   salt:       Key for first AES_CMAC_BE (<= 16 bytes).
*   salt_len:   Length of the salt. Can be <= 16 bytes
*   p:          Data for second AES_CMAC_BE.
*   p_len:      Length of the p (>=0)
*   out:        Buffer for result with length MESH_KEY_LEN.
*
* Return:   None
*
*/
void mesh_k1(const void* n, uint32_t n_len, const uint8_t* salt, uint32_t salt_len, const uint8_t* p, uint32_t p_len, uint8_t* out)
{
    uint8_t salt_zero_padded[MESH_KEY_LEN];
    if (salt_len < MESH_KEY_LEN)
    {
        memcpy(salt_zero_padded, salt, salt_len);
        memset(&salt_zero_padded[salt_len], 0, MESH_KEY_LEN - salt_len);
        salt = salt_zero_padded;
    }
    AES_CMAC_BE((uint8_t*)salt, (uint8_t*)n, n_len, out);
    AES_CMAC_BE(out, (uint8_t*)p, p_len, out);
}

/**
* The network key material derivation function k2 is used to generate instances of EncryptionKey, PrivacyKey and NID for use as Master and Private LPN communication.
* SALT = s1(�smk2�)
* T = AES-CMAC_SALT (N)
*
* T0 = empty string (zero length)
* T1 = AES-CMACT (T0 || P || 0x01)
* T2 = AES-CMACT (T1 || P || 0x02)
* T3 = AES-CMACT (T2 || P || 0x03)
*
* k2(N, P) = (T1 || T2 || T3) mod 2**263
*
* Parameters:
*   n:              N is 16 bytes
*   p:              P is 1 byte or more
*   p_len:          Length of the p (>0)
*   nid:            1 byte variable to receive NID(7 bits).
*   encryption_key: 16 bytes buffer to receive Encryption Key
*   privacy_key:    16 bytes buffer to receive Privacy Key
*
* Return:   None
*
*/
void mesh_k2(const void* n, const uint8_t* p, uint32_t p_len, uint8_t* nid, uint8_t* encryption_key, uint8_t* privacy_key)
{
    uint8_t tn_p_n[MESH_K2_MAX_P_LEN];
    uint8_t t[MESH_KEY_LEN];    // buffer for salt and t

    // SALT = s1(�smk2�)
    mesh_s1("smk2", 4, t);
    // T = AES-CMAC_SALT (N)
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)n, MESH_KEY_LEN, t);
    // T0 = empty string (zero length)
    //tn_p_n[0] = 0;
    // T1 = AES - CMAC_T(T0 || P || 0x01)
    memcpy(&tn_p_n[0], p, p_len);
    tn_p_n[p_len] = 1;
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)tn_p_n, p_len + 1, encryption_key);
    *nid = encryption_key[MESH_KEY_LEN - 1] & 0x7f;
    // T2 = AES - CMAC_T(T1 || P || 0x02)
    memcpy(tn_p_n, encryption_key, MESH_KEY_LEN);
    memcpy(&tn_p_n[MESH_KEY_LEN], p, p_len);
    tn_p_n[MESH_KEY_LEN + p_len] = 2;
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)tn_p_n, MESH_KEY_LEN + p_len + 1, encryption_key);
    // T3 = AES - CMAC_T(T2 || P || 0x03)
    memcpy(tn_p_n, encryption_key, MESH_KEY_LEN);
    memcpy(&tn_p_n[MESH_KEY_LEN], p, p_len);
    tn_p_n[MESH_KEY_LEN + p_len] = 3;
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)tn_p_n, MESH_KEY_LEN + p_len + 1, privacy_key);
}

/**
* The derivation function k3 is used to generate a public value of 64-bits derived from a private key.
* SALT = s1(�smk3�)
* T = AES-CMAC_SALT (N)
*
* k3(N) = AES-CMAC_T ( �id64� || 0x01 ) mod 264
*
* Parameters:
*   n:      N is 16 bytes
*   out:    8 bytes buffer to receive result
*
* Return:   None
*
*/
void mesh_k3(const void* n, uint8_t* out)
{
    uint8_t t[MESH_KEY_LEN];    // buffer for salt and t
    uint8_t id64_1[] = "id64\x01";

    // SALT = s1(�smk3�)
    mesh_s1("smk3", 4, t);

    // T = AES-CMAC_SALT (N)
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)n, MESH_KEY_LEN, t);

    // k3(N) = AES-CMAC_T(�id64� || 0x01) mod 264
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)id64_1, 5, t);
    memcpy(out, &t[MESH_KEY_LEN - MESH_NETWORK_ID_LEN], MESH_NETWORK_ID_LEN);
}

/**
* The derivation function k4 is used to generate a public value of 5-bits derived from a private key.
* SALT = s1(�smk4�)
* T = AES-CMAC_SALT (N)
*
* k3(N) = AES-CMAC_T ( �id5� || 0x01 ) mod 5
*
* Parameters:
*   n:      N is 16 bytes
*   out:    1 byte byte buffer to receive 5 bits result
*
* Return:   None
*
*/
void mesh_k4(const void* n, uint8_t* out)
{
    uint8_t t[MESH_KEY_LEN];    // buffer for salt and t
    uint8_t id6_1[] = "id6\x01";

    // SALT = s1(�smk4�)
    mesh_s1("smk4", 4, t);

    // T = AES-CMAC_SALT (N)
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)n, MESH_KEY_LEN, t);

    // k3(N) = AES-CMAC_T(�id6� || 0x01) mod 264
    AES_CMAC_BE((uint8_t*)t, (uint8_t*)id6_1, 4, t);
    *out = t[MESH_KEY_LEN - 1] & TL_CP_AID_MASK;
}

//--------------------------- end of Security Toolbox --------------------------
