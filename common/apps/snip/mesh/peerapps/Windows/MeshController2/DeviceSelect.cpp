// DeviceSelect.cpp : implementation file
//

#include "stdafx.h"
#include <setupapi.h>
#include "MeshController.h"
#include "DeviceSelect.h"
#include "afxdialogex.h"

//const WCHAR szUUID_AUTOMATION_SERVICE_WIN8[] = L"BTHLEDEVICE\\{C36010CE-0000-1000-8000-00805F9B34FB}";
//const WCHAR szUUID_AUTOMATION_SERVICE_WIN7[] = L"BTHENUM\\{FB349B5F-8000-0080-0010-0000CE1060C3}";

// CDeviceSelect dialog

IMPLEMENT_DYNAMIC(CDeviceSelect, CDialogEx)

CDeviceSelect::CDeviceSelect(BOOL select_uuid /*=FALSE*/, CWnd* pParent /*=NULL*/)
	: CDialogEx(CDeviceSelect::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    m_select_uuid = select_uuid;
}

CDeviceSelect::~CDeviceSelect()
{
}

void CDeviceSelect::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_DEVICE_LIST, m_lbDevices);
}


BEGIN_MESSAGE_MAP(CDeviceSelect, CDialogEx)
    ON_LBN_DBLCLK(IDC_DEVICE_LIST, &CDeviceSelect::OnDblclkDeviceList)
    ON_BN_CLICKED(IDOK, &CDeviceSelect::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CDeviceSelect::OnBnClickedCancel)
    ON_MESSAGE(WM_USER_DEV_UUID, OnDevUuid)
END_MESSAGE_MAP()


// CDeviceSelect message handlers
BOOL CDeviceSelect::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    HDEVINFO                            hardwareDeviceInfo;
    PSP_DEVICE_INTERFACE_DETAIL_DATA    deviceInterfaceDetailData = NULL;
    ULONG                               predictedLength = 0;
    ULONG                               requiredLength = 0, bytes=0;
	WCHAR								szBda[13] = {0};
	HANDLE								hDevice = INVALID_HANDLE_VALUE;

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    m_numDevices = 0;

    if (m_select_uuid)
    {
        GetDlgItem(IDOK)->EnableWindow(FALSE);
    }
    else
    {
        if ((hardwareDeviceInfo = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT)) != INVALID_HANDLE_VALUE)
        {
            SP_DEVINFO_DATA DeviceInfoData;

            memset(&DeviceInfoData, 0, sizeof(DeviceInfoData));
            DeviceInfoData.cbSize = sizeof(DeviceInfoData);

            WCHAR szService1[80], szService2[80], szService3[80];
            GUID guid1 = guidSvcMeshProxy, guid2 = guidSvcMeshProvisioning, guid3 = guidSvcMeshCommand;
            UuidToString(szService1, 80, &guid1);
            ods("%S\n", szService1);
            UuidToString(szService2, 80, &guid2);
            ods("%S\n", szService2);
            UuidToString(szService3, 80, &guid3);
            ods("%S\n", szService3);
            for (DWORD n = 0; SetupDiEnumDeviceInfo(hardwareDeviceInfo, n, &DeviceInfoData); n++)
            {
                DWORD dwBytes = 0;

                SetupDiGetDeviceInstanceId(hardwareDeviceInfo, &DeviceInfoData, NULL, 0, &dwBytes);

                PWSTR szInstanceId = new WCHAR[dwBytes];
                if (szInstanceId)
                {
                    if (SetupDiGetDeviceInstanceId(hardwareDeviceInfo, &DeviceInfoData, szInstanceId, dwBytes, &dwBytes))
                    {
                        _wcsupr_s(szInstanceId, dwBytes);
                        ods("found %S\n", szInstanceId);
                        if (wcsstr(szInstanceId, szService1)
                            || wcsstr(szInstanceId, szService2)
                            || wcsstr(szInstanceId, szService3))
                        {
                            OutputDebugStringW(szInstanceId);
                            WCHAR buf[13];
                            wchar_t* pStart;
                            wchar_t* pEnd;
                            if (m_bWin8)
                            {
                                pStart = wcsrchr(szInstanceId, '_');
                                pEnd = wcsrchr(szInstanceId, '\\');
                            }
                            else
                            {
                                pStart = wcsrchr(szInstanceId, '&');
                                pEnd = wcsrchr(szInstanceId, '_');
                            }
                            if (pStart && pEnd)
                            {
                                *pEnd = 0;
                                wcscpy_s(buf, pStart + 1);
                                // check if we already added this device with different service
                                WCHAR   buf2[13];
                                int     i;
                                for (i = 0; i < m_numDevices; i++)
                                {
                                    m_lbDevices.GetText(i, buf2);
                                    if (!wcscmp(buf, buf2))
                                        break;
                                }
                                // Add this device if it is new one
                                if (i >= m_numDevices)
                                {
                                    m_lbDevices.AddString(buf);
                                    m_numDevices++;
                                }
                            }
                        }
                    }
                    delete[] szInstanceId;
                }
            }
            SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);
        }
        if (m_numDevices)
            m_lbDevices.SetCurSel(0);

        GetDlgItem(IDC_DEVICE_LIST)->ShowWindow((m_numDevices == 0) ? SW_HIDE : SW_SHOW);
        GetDlgItem(IDC_NO_DEVICES)->ShowWindow((m_numDevices == 0) ? SW_SHOW : SW_HIDE);
    }
	return TRUE;
}

void CDeviceSelect::OnDblclkDeviceList()
{
    OnBnClickedOk();
}


void CDeviceSelect::OnBnClickedOk()
{
    if (!m_select_uuid)
    {
        WCHAR buf[13];
        m_lbDevices.GetText(m_lbDevices.GetCurSel(), buf);
        int bda[6];
        if (swscanf_s(buf, L"%02x%02x%02x%02x%02x%02x", &bda[0], &bda[1], &bda[2], &bda[3], &bda[4], &bda[5]) == 6)
        {
            for (int i = 0; i < 6; i++)
                m_bth.rgBytes[5 - i] = (BYTE)bda[i];
        }
    }
    else
    {
        WCHAR buf[33];
        int uuid[16];
        m_lbDevices.GetText(m_lbDevices.GetCurSel(), buf);
        if (16 == swscanf_s(buf, L"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            &uuid[0], &uuid[1], &uuid[2], &uuid[3], &uuid[4], &uuid[5], &uuid[6], &uuid[7],
            &uuid[8], &uuid[9], &uuid[10], &uuid[11], &uuid[12], &uuid[13], &uuid[14], &uuid[15]))
        {
            for (int i = 0; i < 16; i++)
                m_uuid[i] = (BYTE)uuid[i];
        }
    }
    CDialogEx::OnOK();
}


void CDeviceSelect::OnBnClickedCancel()
{
    m_bth.ullLong = 0;
    memset(m_uuid, 0, sizeof(m_uuid));
    CDialogEx::OnCancel();
}

void UuidToStr(WCHAR* str, BYTE* uuid)
{
    for (int i = 0; i < 16; i++)
    {
        swprintf_s(&str[i * 2], 3, L"%02x", uuid[i]);
    }
}

LRESULT CDeviceSelect::OnDevUuid(WPARAM Instance, LPARAM lparam)
{
    WCHAR buf[33];
    BTW_GATT_VALUE *pValue = (BTW_GATT_VALUE *)lparam;
    if (pValue->len == (1 + MESH_DEVICE_UUID_LEN))
    {
        UuidToStr(buf, &pValue->value[1]);
        if (m_lbDevices.FindString(-1, buf) == LB_ERR)
        {
            m_lbDevices.AddString(buf);
            m_numDevices++;
            if (m_numDevices == 1)
            {
                GetDlgItem(IDOK)->EnableWindow(TRUE);
                m_lbDevices.SetCurSel(0);
            }
        }
        free(pValue);
    }
    return S_OK;
}
