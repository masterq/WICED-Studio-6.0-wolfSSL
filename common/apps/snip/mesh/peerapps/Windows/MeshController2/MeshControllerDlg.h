
// MeshControllerDlg.h : header file
//

#pragma once
#include "Win7Interface.h"
#include "Win8Interface.h"
#include "Win10Interface.h"
#include "afxcmn.h"
#include "WsOtaDownloader.h"

#define WM_CONNECTED                    (WM_USER + 0x101)
#define WM_USER_SELECT_DEVICE           (WM_USER + 0x102)
#define WM_USER_PROXY_DATA              (WM_USER + (WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROXY_DATA_OUT & 0xFF))
#define WM_USER_PROVISIONING_DATA       (WM_USER + (WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROVISIONING_DATA_OUT & 0xFF))
#define WM_USER_COMMAND_DATA            (WM_USER + (WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_COMMAND_DATA & 0xFF))
#define WM_USER_WS_UPGRADE_CTRL_POINT   (WM_USER + (GUID_OTA_FW_UPGRADE_CHARACTERISTIC_CONTROL_POINT.Data1 & 0xFF))

class CDeviceSelect;

// CMeshControllerDlg dialog
class CMeshControllerDlg : public CDialogEx
{
// Construction
public:
	CMeshControllerDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CMeshControllerDlg();

    void SetParam(BLUETOOTH_ADDRESS *bth, HMODULE hLib);
    void TraceMsg(BOOL bRcvd, UINT16 node, UINT8 ttl, UINT16 company_id, UINT16 opcode, UINT8 *payload, UINT16 payload_len);

// Dialog Data
	enum { IDD = IDD_MESH_CONTROLLER_DIALOG };

    CDeviceSelect*  m_pDeviceSelectDialog;
    BOOL m_bWin10;
	BOOL m_bWin8;
//    BLUETOOTH_ADDRESS m_bth;
    HMODULE m_hLib;
    CBtInterface *m_btInterface;
    DWORD       m_seq;

    CListBox *m_trace;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

private:
    HANDLE m_hDevice;
    HANDLE m_hCfgEvent;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
    virtual void PostNcDestroy();
    LRESULT OnConnected(WPARAM bConnected, LPARAM lparam);
    LRESULT OnProxyDataIn(WPARAM op, LPARAM lparam);
    LRESULT OnProvisioningDataIn(WPARAM op, LPARAM lparam);
    LRESULT OnCommandDataIn(WPARAM op, LPARAM lparam);
    LRESULT OnWsUpgradeCtrlPoint(WPARAM op, LPARAM lparam);
    LRESULT OnSelectDevice(WPARAM Instance, LPARAM lparam);
    BOOL WriteNodeAndKey(USHORT uuidChar, USHORT inx, BYTE *Out, BYTE cbOut);
    void DeviceConnected();
    DWORD GetHexValue(DWORD id, LPBYTE buf, DWORD buf_size);
    DWORD GetHexValue(char *szBuf, LPBYTE buf, DWORD buf_size);
    DWORD GetHexValueInt(DWORD id);
    void SetHexValueInt(DWORD id, DWORD val, DWORD val_len);
    void GetDlgItemTextUTF8(int id, char* buf, DWORD buf_len);
    void InitControls();
    void SendProxyFltMsg(BYTE opcode);
    void SendProxyFltSetTypeMsg(BYTE type);
    bool Send(uint16_t dst, uint8_t app_key_idx, wiced_bool_t szmicn, uint8_t *payload, uint16_t payload_len);
public:
    void SetHexValue(DWORD id, LPBYTE val, DWORD val_len);
    void BatteryClientSetup(void);
    void BatteryServerSetup(void);
    void LocationClientSetup(void);
    void LocationServerSetup(void);
    void OnOffClientSetup(void);
    void OnOffServerSetup(void);
    void LevelClientSetup(void);
    void LevelServerSetup(void);
    void TransitionTimeClientSetup(void);
    void TransitionTimeServerSetup(void);
    void PowerOnOffServerSetup(void);
    void PowerOnOffClientSetup(void);
    void PowerLevelClientSetup(void);
    void PowerLevelServerSetup(void);
    void PropertyServerSetup(void);
    void PropertyClientSetup(void);
    void LightLightnessServerSetup(void);
    void LightLightnessClientSetup(void);
    void SensorServerSetup(void);
    void SensorClientSetup(void);
    void LightCtlServerSetup(void);
    void LightCtlClientSetup(void);
    void LightHslServerSetup(void);
    void LightHslClientSetup(void);
    void LightXylServerSetup(void);
    void LightXylClientSetup(void);
    void LightLcServerSetup(void);
    void LightLcClientSetup(void);

public:
    afx_msg void OnBnClickedProvision();
    afx_msg void OnBnClickedSend();
    afx_msg void OnBnClickedNewMesh();
    afx_msg void OnBnClickedNewMeshSave();
    afx_msg void OnBnClickedSecureBeacon();
    afx_msg void OnBnClickedPbAdv();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnCbnSelchangeOpCode();
    afx_msg void OnBnClickedAddFlt();
    afx_msg void OnBnClickedDelFlt();
    afx_msg void OnBnClickedSetWhiteFlt();
    afx_msg void OnBnClickedSetBlackFlt();
    afx_msg void OnBnClickedConfigure();
    afx_msg void OnBnClickedCheckNodeIdentity();
    afx_msg void OnBnClickedOtaUpgradeStart();

    CProgressCtrl m_Progress;
    LRESULT OnProgress(WPARAM completed, LPARAM total);
    WSDownloader *m_pDownloader;
    LPBYTE m_pPatch;
    DWORD m_dwPatchSize;
    afx_msg void OnBnClickedBatteryClient();
    afx_msg void OnBnClickedBatteryServer();
    afx_msg void OnBnClickedLocationClient();
    afx_msg void OnBnClickedOnoffClient();
    afx_msg void OnBnClickedOnoffServer();
    afx_msg void OnBnClickedLevelClient();
    afx_msg void OnBnClickedLevelServer();
    afx_msg void OnBnClickedTransitionTimeClient();
    afx_msg void OnBnClickedTransitionTimeServer();
    afx_msg void OnBnClickedLocationServer();
    afx_msg void OnBnClickedPowerOnoffSetupServer();
    afx_msg void OnBnClickedPowerOnoffClient();
    afx_msg void OnBnClickedPowerLevelClient();
    afx_msg void OnBnClickedPowerLevelSetupServer();
    afx_msg void OnBnClickedPropertyServer();
    afx_msg void OnBnClickedPropertyClient();
    afx_msg void OnBnClickedLightLightnessSetupServer();
    afx_msg void OnBnClickedLightLightnessClient();
    afx_msg void OnBnClickedSensorClient();
    afx_msg void OnBnClickedSensorServer();
    afx_msg void OnBnClickedLightCtlClient();
    afx_msg void OnBnClickedLightCtlServer();
    afx_msg void OnBnClickedLightHslClient();
    afx_msg void OnBnClickedLightHslServer();
    afx_msg void OnBnClickedLightXylClient();
    afx_msg void OnBnClickedLightXylServer();
    afx_msg void OnBnClickedLightLcClient();
    afx_msg void OnBnClickedLightLcServer();
    afx_msg void OnBnClickedResetNode();
    afx_msg void OnBnClickedSelectDevice();
};
