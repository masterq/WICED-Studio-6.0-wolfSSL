/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Foundation Model implementation
 *
*/
#include <string.h>
#include <stdio.h>
#include "platform.h"

#include "mesh_core.h"
#include "mesh_util.h"
#include "foundation_int.h"
#include "foundation.h"
#include "access_layer.h"
#include "core_aes_ccm.h"
#include "mesh_util.h"
#include "key_refresh.h"
#include "transport_layer.h"
#include "core_ovl.h"
#include "wiced_timer.h"
#include "friend.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__FOUNDATION2_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__FOUNDATION2_C
#include "mesh_trace.h"

static void handle_config_compos_data_get_(uint16_t src_id, uint8_t page);
static void handle_config_beacon_set_(uint8_t state, uint16_t src_id);
static void handle_config_default_ttl_set_(uint8_t state, uint16_t src_id);
static void handle_node_discoverable_set_(uint8_t state, uint16_t src_id);
static void handle_config_proxy_set_(uint8_t state, uint16_t src_id);
static void handle_config_friend_set_(uint8_t state, uint16_t src_id);
static void handle_config_lpn_poll_timeout_get_(uint16_t src_id, const uint8_t *params, uint16_t params_len);
static void handle_config_relay_set_(uint16_t src_id, const uint8_t *params, uint16_t params_len);
static void handle_config_net_transmit_set_(uint16_t src_id, const uint8_t *params, uint16_t params_len);
static void handle_heartbeat_publication_get_(uint16_t src_id, uint8_t params_len);
static void handle_heartbeat_publication_set_(uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_heartbeat_subs_get_(uint16_t src_id, uint8_t params_len);
static void handle_heartbeat_subs_set_(uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void heartbeat_proxy_state_changed(void);
static void heartbeat_relay_state_changed(void);
static void heartbeat_friend_state_changed(void);
static void heartbeat_publication_init_(void);
static void heartbeat_publication_start_(void);
static void handle_node_identity_get_(uint16_t global_net_key_idx, uint16_t src_id);
static void handle_node_identity_set_(uint16_t global_net_key_idx, uint8_t state, uint16_t src_id);
static void foundation_send_config_relay_status_(uint8_t state, uint8_t retrans, uint16_t src_id);

// publishing timer
wiced_timer_t   publication_timer;

// identity timer
wiced_timer_t   node_identity_timer;
wiced_bool_t    node_identity_timer_inited = WICED_FALSE;
uint8_t         node_identity_timer_net_key_idx = 0xff;     // 0xff means no any node identity adverts; 0xfe means advert on all net keys; other value means net key enabled for advert

static uint8_t foundation_find_models_(uint16_t addr, uint16_t company_id, uint16_t model_id, tFND_NVM_MODEL** p_models, uint32_t max_models )
{
    uint8_t returned_models = 0;
    tFND_NVM_ELEMENT *p_nvm_elem;
    uint8_t idx_element, idx_model, idx_subs;
  
    TRACE1(TRACE_DEBUG, "foundation_find_models_: p_models:%x\n", (uint32_t)p_models);
    TRACE2(TRACE_DEBUG, " company_id:%x model_id:%x\n", company_id, model_id);
    TRACE3(TRACE_DEBUG, " max_models:%x elements_num:%x, addr:%x\n", max_models, fnd_static_config_.elements_num, addr);

    // go through all elements
    for (idx_element = 0, p_nvm_elem = fnd_config_->elements;                    // first element
        idx_element < fnd_static_config_.elements_num && returned_models < max_models;   // till out buffer is full
        idx_element++, p_nvm_elem = (tFND_NVM_ELEMENT*)((uint8_t*)&p_nvm_elem->models[0] + p_nvm_elem->models_num * sizeof(tFND_NVM_MODEL)))  // go to the next element
    {
        TRACE3(TRACE_DEBUG, "  idx_element:%x returned_models:%x p_nvm_elem->models_num:%x\n", idx_element, returned_models, p_nvm_elem->models_num);

        // ignore that element if address is unicast and it is not address of the current element
        if ((addr & MESH_NON_UNICAST_MASK) == 0
            && addr != node_nv_data.node_id + idx_element)
                continue;
        // go through all models of the current element
        for (idx_model = 0; idx_model < p_nvm_elem->models_num; idx_model++)
        {
            TRACE2(TRACE_DEBUG, "   company_id:%x model_id:%x\n", p_nvm_elem->models[idx_model].company_id, p_nvm_elem->models[idx_model].model_id);
            // exit loop if we found the model
            if (p_nvm_elem->models[idx_model].company_id == company_id
                && p_nvm_elem->models[idx_model].model_id == model_id)
                break;
        }
        // ignore that element if it doesn't have that model
        if (idx_model >= p_nvm_elem->models_num)
            continue;
        // if the address is not unicast then make sure it is in the model's subscriptions list
        if (addr & MESH_NON_UNICAST_MASK)
        {
            for (idx_subs = 0; idx_subs <= p_nvm_elem->models[idx_model].subs_num; idx_subs++)
            {
                if (p_nvm_elem->models[idx_model].subs_list[idx_subs] == addr)
                    break;
            }
            if (idx_subs > p_nvm_elem->models[idx_model].subs_num)
                continue;
        }

        // copy found model to the output buffer
        p_models[returned_models++] = &p_nvm_elem->models[idx_model];

        // exit loop if the address is unicast
        if ((addr & MESH_NON_UNICAST_MASK) == 0)
            break;
    }

    TRACE1(TRACE_INFO, "foundation_find_models_: returns %d\n", returned_models);
    return returned_models;
}

/**
* Process data packet addressed to this mesh node
*
* Parameters:
*   src_id:         Address of the message source.
*   ttl:            TTL of the received packet
*   opcode:         opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*   params:         parameters of the app payload
*   params_len:     length of the parameters of the app payload
*
* Return:   WICED_TRUE - message handled; WICED_FALSE - try other handle.
*
*/
wiced_bool_t foundation_handle_2_(uint16_t src_id, uint8_t ttl, uint16_t opcode, const uint8_t *params, uint16_t params_len)
{
    wiced_bool_t    ret = WICED_TRUE;
    TRACE2(TRACE_INFO, "foundation_handle_2: ttl=%x src_id=%x\n", ttl, src_id);
    TRACE1(TRACE_INFO, " opcode=%x ***************************\n", opcode);
    if (params_len)
        TRACEN(TRACE_INFO, (char*)params, params_len);
    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET:
        if (params_len != 1)
            break;
        handle_config_compos_data_get_(src_id, params[0]);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET:
        if (params_len != 0)
            break;
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS, node_nv_data.state_sec_net_beacon, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET:
        if (params_len != 1)
            break;
        handle_config_beacon_set_(*params, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET:
        if (params_len != 2)
            break;
        handle_node_identity_get_(LE2TOUINT16(params), src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET:
        if (params_len != 3)
            break;
        handle_node_identity_set_(LE2TOUINT16(params), params[2], src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET:
        if (params_len != 0)
            break;
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS, node_nv_data.state_default_ttl, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET:
        if (params_len != 1)
            break;
        handle_config_default_ttl_set_(*params, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET:
        if (params_len != 0)
            break;
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS, node_nv_data.state_gatt_proxy, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET:
        if (params_len != 1)
            break;
        handle_config_proxy_set_(*params, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET:
        if (params_len != 0)
            break;
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS, node_nv_data.state_friend, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET:
        if (params_len != 1)
            break;
        handle_config_friend_set_(*params, src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET:
        handle_config_lpn_poll_timeout_get_(src_id, params, params_len);
        break;

    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET:
        if (params_len != 0)
            break;
        foundation_send_config_relay_status_(node_nv_data.state_relay, node_nv_data.state_relay_retrans, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET:
        handle_config_relay_set_(src_id, params, params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET:
        if (params_len != 0)
            break;
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS, node_nv_data.state_net_trans, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET:
        handle_config_net_transmit_set_(src_id, params, params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET:
        handle_heartbeat_publication_get_(src_id, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET:
        handle_heartbeat_publication_set_(src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET:
        handle_heartbeat_subs_get_(src_id, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET:
        handle_heartbeat_subs_set_(src_id, params, (uint8_t)params_len);
        break;
    default:
        ret = WICED_FALSE;
    }
    return ret;
}

/**
* Calculates size of the memory for configuration
*
* Parameters:
*   config:     Configuration data
*
* Return:   size of the memory for configuration
*/
uint16_t foundation_get_config_size_(wiced_bt_mesh_core_config_t *config)
{
    uint16_t i, len;
    len = OFFSETOF(tFND_NVM_CONFIG, elements);
    for (i = 0; i < config->elements_num; i++)
    {
        len += OFFSETOF(tFND_NVM_ELEMENT, models);
        len += config->elements[i].models_num * sizeof(tFND_NVM_MODEL);
    }
    return len;
}

/**
* Assign Company ID, Vendor ID and Product ID to the mesh node
* Called from foundation_init which should be called by Application on startup
*
* Parameters:
*   config:     Configuration data
*
* Return:   WICED_TRUE - succeeded. WICED_FALSE - failed;
*/
wiced_bool_t foundation_init_int_(wiced_bt_mesh_core_config_t *config)
{
    uint32_t i, m;
    tFND_NVM_ELEMENT   *p_nvm_elem;

    // Fill fnd_static_config_ memory
    memset(&fnd_static_config_, 0, sizeof(fnd_static_config_));
    fnd_static_config_.oob = config->oob;
    if (config->uri)
    {
        uint32_t len = strlen(config->uri);
        uint32_t scheme_len;
        if (len > 8)
        {
            if (0 == memcmp(config->uri, "http://", 7) || 0 == memcmp(config->uri, "HTTP://", 7))
            {
                scheme_len = 5;
                fnd_static_config_.uri[0] = MESH_URI_SCHEME_HTTP;
            }
            else if (0 == memcmp(config->uri, "https://", 8) || 0 == memcmp(config->uri, "HTTP://", 8))
            {
                scheme_len = 6;
                fnd_static_config_.uri[0] = MESH_URI_SCHEME_HTTPS;
            }
            if (fnd_static_config_.uri[0] != 0 && (len - scheme_len + 1) <= MESH_URI_MAX_LEN)
            {
                memcpy(&fnd_static_config_.uri[1], &config->uri[scheme_len], len - scheme_len);
                fnd_static_config_.uri_len = 1 + len - scheme_len;
            }
        }
        if (fnd_static_config_.uri[0] == 0)
        {
            TRACE0(TRACE_WARNING, "fnd_init_int_: invalid URI\n");
            return WICED_FALSE;
        }
        else
        {
            wiced_bt_mesh_core_calc_uri_hash(&fnd_static_config_.uri[0], fnd_static_config_.uri_len, &fnd_static_config_.uri_hash[0]);
        }
    }
    fnd_static_config_.cid = config->company_id;
    fnd_static_config_.vid = config->vendor_id;
    fnd_static_config_.pid = config->product_id;
    fnd_static_config_.crpl = config->replay_cache_size;
    fnd_static_config_.elements_num = config->elements_num;
    fnd_static_config_.features = config->features;

    // Fill fnd_config_ memory
    memset(fnd_config_, 0, fnd_config_len_);
    p_nvm_elem = fnd_config_->elements;
    for (i = 0; i < config->elements_num; i++)
    {
        p_nvm_elem->idx = (uint8_t)i;
        p_nvm_elem->location = config->elements[i].location;
        p_nvm_elem->models_num = config->elements[i].models_num;
        for (m = 0; m < p_nvm_elem->models_num; m++)
        {
            p_nvm_elem->models[m].idx = (uint8_t)m;
            p_nvm_elem->models[m].company_id = config->elements[i].models[m].company_id;
            p_nvm_elem->models[m].model_id = config->elements[i].models[m].model_id;
            p_nvm_elem->models[m].publish_address = MESH_NODE_ID_INVALID;
            p_nvm_elem->models[m].publish_app_key_idx = MESH_KEY_IDX_INVALID;
        }
        p_nvm_elem = (tFND_NVM_ELEMENT*)((uint8_t*)&p_nvm_elem->models[0] + p_nvm_elem->models_num * sizeof(tFND_NVM_MODEL));
    }

    // clear the list of the virtual addresses
    if (fnd_virt_addr_)
        wiced_memory_free(fnd_virt_addr_);
    fnd_virt_addr_ = (tFND_NVM_VIRT_ADDR_CB*)wiced_memory_allocate(sizeof(tFND_NVM_VIRT_ADDR_CB));
    if (fnd_virt_addr_ == NULL)
    {
        TRACE1(TRACE_INFO, "fnd_init_int_: malloc(%d) failed\n", sizeof(tFND_NVM_VIRT_ADDR_CB));
        return WICED_FALSE;
    }

    fnd_virt_addr_->num = 0;

#ifndef MESH_CONTROLLER
    if (sizeof(tFND_NVM_VIRT_ADDR_CB) != mesh_write_node_info(MESH_NVM_IDX_VIRT_ADDR, (uint8_t*)fnd_virt_addr_, sizeof(tFND_NVM_VIRT_ADDR_CB), NULL))
    {
        TRACE0(TRACE_INFO, "foundation_init_int_: write_node_info(VIRT_ADDR) failed\n");
        return WICED_FALSE;
    }
#endif
    TRACE1(TRACE_INFO, "foundation_init_int_: write_node_info(VIRT_ADDR, %d) succeeded\n", sizeof(tFND_NVM_VIRT_ADDR_CB));

    return WICED_TRUE;
}

#ifndef MESH_CONTROLLER
/**
* Reads Configuration data from the NVRAM
* Application should call it on startup in provisioned mode
*
* Parameters:   None
*
* Return:   WICED_TRUE - succeeded. WICED_FALSE - failed;
*/
wiced_bool_t foundation_read_config_(void)
{
    wiced_bool_t    ret = WICED_FALSE;
    uint8_t         len_to_read, *p;
    uint8_t         idx;
    do
    {
        fnd_virt_addr_ = NULL;
        if(fnd_config_len_ != mesh_read_node_info2(MESH_NVM_IDX_CFG_DATA, (uint8_t*)fnd_config_, fnd_config_len_, NULL))
        {
            TRACE0(TRACE_INFO, "foundation_read_config: mesh_read_node_info2 failed\n");
            break;
        }
        // read the list of the virtual addresses
        fnd_virt_addr_ = (tFND_NVM_VIRT_ADDR_CB*)wiced_memory_allocate(sizeof(tFND_NVM_VIRT_ADDR_CB));
        if (fnd_virt_addr_ == NULL)
        {
            TRACE1(TRACE_INFO, "foundation_read_config: malloc(%d) failed\n", sizeof(tFND_NVM_VIRT_ADDR_CB));
            break;
        }
        if (sizeof(tFND_NVM_VIRT_ADDR_CB) != mesh_read_node_info(MESH_NVM_IDX_VIRT_ADDR, (uint8_t*)fnd_virt_addr_, sizeof(tFND_NVM_VIRT_ADDR_CB), NULL))
        {
            TRACE0(TRACE_INFO, "foundation_read_config: read_node_info(VIRT_ADDR) failed\n");
            break;
        }
        ret = WICED_TRUE;
    } while (0);
    if (!ret)
    {
        if (fnd_virt_addr_)
            wiced_memory_free(fnd_virt_addr_);
        fnd_virt_addr_ = NULL;
    }
    TRACE2(TRACE_INFO, "foundation_read_config: returns:%d fnd_virt_addr_->num:%d\n", ret, fnd_virt_addr_ ? fnd_virt_addr_->num : 0);

    return ret;
}
#endif

/**
* Creates application payload for messages with no params
*
* Parameters:
*   op:     OP code. Can be WICED_BT_MESH_CORE_CMD_CONFIG_BLOCK_GET, WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET
*                           WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET, WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET, WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET
*                           WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET, WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET, WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET
*   buf:    Bufer for command. Should be big enough (2 bytes).
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_get_msg(uint16_t op, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, op) - buf);
    TRACE0(TRACE_INFO, "foundation_crt_msg_with_no_params:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates application payload for message Composition Data Get
*
* Parameters:
*   page:   Page of the Composition data to get. Should be 0 for now.
*   buf:    Bufer for command. Should be big enough (2 bytes).
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_compos_data_get_msg(uint8_t page, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET) - buf);
    buf[len++] = page;
    TRACE0(TRACE_INFO, "foundation_crt_compos_data_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}


/**
* Creates application payload for messages with one param state
*
* Parameters:
*   op:     OP code. Can be WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET_XXX
*                           WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET_XXX, WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET_XXX,
*                           WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET_XXX, WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET
*   state:  parameter of the application payload
*   buf:    Bufer for command. Should be big enough (3 bytes).
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_set_msg(uint16_t op, uint8_t state, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, op) - buf);
    buf[len++] = state;
    TRACE0(TRACE_INFO, "foundation_crt_set_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates application payload for message Config Relay Set
*
* Parameters:
*   state:      Relay state
*   retrans:    Relay Retransmit: <3bits RelayRetransmitCount><5bits RelayRetransmitIntervalSteps>
*   buf:        Bufer for command. Should be big enough (4 bytes).
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_relay_set_msg(uint8_t state, uint8_t retrans, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET) - buf);
    buf[len++] = state;
    buf[len++] = retrans;
    TRACE0(TRACE_INFO, "foundation_crt_relay_set_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message config lpn poll timeout get
*
* Parameters:
*   lpn_addr:   Address of the LPN node to get PollTimeout for
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_config_lpn_poll_timeout_get_msg(uint16_t lpn_addr, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET) - buf);
    UINT16TOLE2(&buf[len], lpn_addr);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_config_lpn_poll_timeout_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message appkey add, update or delete
*
* Parameters:
*   opcode:     opcode
*   net_idx:    netkey index to bound to
*   app_idx:    appkey index to add
*   app_key:    application key to add or update. NULL if delet
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
static uint16_t foundation_crt_appkey_msg_(uint16_t opcode, uint16_t net_idx, uint16_t app_idx, uint8_t *app_key, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, opcode) - buf);
    TWOIDXTOLE3(&buf[len], net_idx, app_idx);
    len += 3;
    if (opcode != WICED_BT_MESH_CORE_CMD_APPKEY_DELETE)
    {
        memcpy(&buf[len], app_key, MESH_KEY_LEN);
        len += MESH_KEY_LEN;
    }
    TRACE0(TRACE_INFO, "foundation_crt_appkey_msg_:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message appkey add
*
* Parameters:
*   net_idx:    netkey index to bound to
*   app_idx:    appkey index to add
*   app_key:    application key to add
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_appkey_add_msg(uint16_t net_idx, uint16_t app_idx, uint8_t *app_key, uint8_t *buf)
{
    return foundation_crt_appkey_msg_(WICED_BT_MESH_CORE_CMD_APPKEY_ADD, net_idx, app_idx, app_key, buf);
}

/**
* Creates the application payload for message appkey update
*
* Parameters:
*   net_idx:    netkey index to bound to
*   app_idx:    appkey index to add
*   app_key:    application key to add
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_appkey_update_msg(uint16_t net_idx, uint16_t app_idx, uint8_t *app_key, uint8_t *buf)
{
    return foundation_crt_appkey_msg_(WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE, net_idx, app_idx, app_key, buf);
}

/**
* Creates the application payload for message appkey delete
*
* Parameters:
*   net_idx:    netkey index to bound to
*   app_idx:    appkey index to add
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_appkey_del_msg(uint16_t net_idx, uint16_t app_idx, uint8_t *buf)
{
    return foundation_crt_appkey_msg_(WICED_BT_MESH_CORE_CMD_APPKEY_DELETE, net_idx, app_idx, NULL, buf);
}

/**
* Creates the application payload for message appkey get
*
* Parameters:
*   net_idx:    appkey index to get app keys of
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_appkey_get_msg(uint16_t net_idx, uint8_t *buf)
{
    uint8_t *p;
    p = op2be(buf, WICED_BT_MESH_CORE_CMD_APPKEY_GET);
    UINT16TOLE2(p, net_idx);
    p += 2;
    TRACE0(TRACE_INFO, "foundation_crt_appkey_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, p - buf);
    return (uint16_t)(p - buf);
}

/**
* Creates the application payload for messages appkey add, update or delete
*
* Parameters:
*   opcode: OP code
*   net_idx:    netkey index to add
*   net_key:    network key to add
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
static uint16_t foundation_crt_netkey_msg_(uint16_t opcode, uint16_t net_idx, uint8_t *net_key, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, opcode) - buf);
    UINT16TOLE2(&buf[len], net_idx);
    len += 2;
    if (opcode != WICED_BT_MESH_CORE_CMD_NETKEY_DELETE)
    {
        memcpy(&buf[len], net_key, MESH_KEY_LEN);
        len += MESH_KEY_LEN;
    }
    TRACE0(TRACE_INFO, "foundation_crt_netkey_msg_:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for messages netkey add
*
* Parameters:
*   net_idx:    netkey index to update
*   net_key:    network key to update
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_netkey_add_msg(uint16_t net_idx, uint8_t *net_key, uint8_t *buf)
{
    return foundation_crt_netkey_msg_(WICED_BT_MESH_CORE_CMD_NETKEY_ADD, net_idx, net_key, buf);
}

/**
* Creates the application payload for messages netkey update
*
* Parameters:
*   net_idx:    netkey index to update
*   net_key:    network key to update
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_netkey_update_msg(uint16_t net_idx, uint8_t *net_key, uint8_t *buf)
{
    return foundation_crt_netkey_msg_(WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE, net_idx, net_key, buf);
}

/**
* Creates the application payload for messages netkey delete
*
* Parameters:
*   net_idx:    netkey index to delete
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_netkey_del_msg(uint16_t net_idx, uint8_t *buf)
{
    return foundation_crt_netkey_msg_(WICED_BT_MESH_CORE_CMD_NETKEY_DELETE, net_idx, NULL, buf);
}

/**
* Creates the application payload for message publication get
*
* Parameters:
*   elem_addr:  address of the companent
*   company_id: Company ID of the Model ID
*   model_id:   Model ID
*   buf:        Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_pub_get_msg(uint16_t elem_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_pub_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message publication set
*
* Parameters:
*   elem_addr:      Address of the companent
*   pub_addr:       Publish Address
*   pub_addr_len:   Length of the Publish Address. For testing it can be any value from 0 to 16 (correct value should be 2 or 16)
*   global_app_key_idx:    Global Index of the application key
*   pub_ttl:        Default TTL value for the outgoing messages
*   pub_period:     Period in for periodic status publishing
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   credential_flag                     Value of the Friendship Credential Flag
*   publish_retransmit_count            Number of retransmissions for each published message
*   publish_retransmit_interval_steps   Number of 50-millisecond steps between retransmissions
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
static uint16_t foundation_crt_pub_set_msg_(uint16_t elem_addr, const uint8_t *pub_addr, uint8_t pub_addr_len,
    uint16_t global_app_key_idx, uint8_t pub_ttl, uint8_t pub_period, uint16_t company_id, uint16_t model_id,
    wiced_bool_t credential_flag, uint8_t publish_retransmit_count, uint8_t publish_retransmit_interval_steps, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, pub_addr_len == 2 ? WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET : WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    memcpy(&buf[len], pub_addr, pub_addr_len);
    len += pub_addr_len;

    UINT16TOLE2(&buf[len], global_app_key_idx | (credential_flag ? 0x1000 : 0));
    len += 2;

    buf[len++] = pub_ttl;
    buf[len++] = pub_period;
    // make sure publish_retransmit_count is between 1 and 8
    if (publish_retransmit_count == 0)
        publish_retransmit_count = 1;
    else if (publish_retransmit_count > 8)
        publish_retransmit_count = 8;
    // make sure interval_steps is between 1 and 0x20
    if (publish_retransmit_interval_steps == 0)
        publish_retransmit_interval_steps = 1;
    else if (publish_retransmit_interval_steps > 0x20)
        publish_retransmit_interval_steps = 0x20;
    buf[len++] = ((publish_retransmit_count - 1) << 5) | ((publish_retransmit_interval_steps - 1) & 0x1f);

    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_pub_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message publication set
*
* Parameters:
*   elem_addr:      Address of the element
*   pub_addr:       Publish Address
*   global_app_key_idx:    Global Index of the application key
*   pub_ttl:        Default TTL value for the outgoing messages
*   pub_period:     Period in for periodic status publishing
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   credential_flag                     Value of the Friendship Credential Flag
*   publish_retransmit_count            Number of retransmissions for each published message
*   publish_retransmit_interval_steps   Number of 50-millisecond steps between retransmissions
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_pub_set_msg(uint16_t elem_addr, uint16_t pub_addr,
    uint16_t global_app_key_idx, uint8_t pub_ttl, uint8_t pub_period, uint16_t company_id, uint16_t model_id,
    wiced_bool_t credential_flag, uint8_t publish_retransmit_count, uint8_t publish_retransmit_interval_steps, uint8_t *buf)
{
    uint8_t pub_addr_le[2];
    UINT16TOLE2(pub_addr_le, pub_addr);
    return foundation_crt_pub_set_msg_(elem_addr, pub_addr_le, 2, global_app_key_idx, pub_ttl, pub_period, company_id, model_id,
        credential_flag, publish_retransmit_count, publish_retransmit_interval_steps, buf);
}

/**
* Creates the application payload for message publication virtual address set
*
* Parameters:
*   elem_addr:      Address of the element
*   pub_addr:       Virtual Publish Address 16 bytes length
*   global_app_key_idx:    Index of the application key
*   pub_ttl:        Default TTL value for the outgoing messages
*   pub_period:     Period in for periodic status publishing
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   credential_flag                     Value of the Friendship Credential Flag
*   publish_retransmit_count            Number of retransmissions for each published message
*   publish_retransmit_interval_steps   Number of 50-millisecond steps between retransmissions
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_pub_virt_addr_set_msg(uint16_t elem_addr, const uint8_t *pub_addr,
    uint16_t global_app_key_idx, uint8_t pub_ttl, uint8_t pub_period, uint16_t company_id, uint16_t model_id,
    wiced_bool_t credential_flag, uint8_t publish_retransmit_count, uint8_t publish_retransmit_interval_steps, uint8_t *buf)
{
    return foundation_crt_pub_set_msg_(elem_addr, pub_addr, 16, global_app_key_idx, pub_ttl, pub_period, company_id, model_id,
        credential_flag, publish_retransmit_count, publish_retransmit_interval_steps, buf);
}

/**
* Handles message WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET - responds with message WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS
*
* Parameters:
*   src_id:         Address of the message source.
*
* Return:   None
*/
static void handle_config_compos_data_get_(uint16_t src_id, uint8_t page)
{
    uint8_t buf[MESH_MAX_APP_PAYLOAD_LEN];
    uint8_t *p_page;
    tFND_CONFIG_COMPOS_DATA            *p_compos_data;
    tFND_CONFIG_ELEMENT_COMPOS_DATA    *p_elem;

    tFND_NVM_ELEMENT    *p_nvm_elem;
    uint8_t             elem_idx, model_idx;
    uint16_t            features;

    TRACE2(TRACE_INFO, "handle_config_compos_data_get_: page:%d src_id:%x\n", page, src_id);
    //TRACEN(TRACE_INFO, (char*)fnd_config_, fnd_config_len_);

    // OP code
    p_page = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS);
    // currently only one page is supported - 0. Per spec we have to return max supported page (0) regardless of requested page
    *p_page = 0;
    p_compos_data = (tFND_CONFIG_COMPOS_DATA*)(p_page + 1);
    p_elem = p_compos_data->elements;

    // CID(2bytes), PID(2bytes), VID(2bytes)
    UINT16TOLE2(p_compos_data->cid, fnd_static_config_.cid);
    UINT16TOLE2(p_compos_data->pid, fnd_static_config_.pid);
    UINT16TOLE2(p_compos_data->vid, fnd_static_config_.vid);
    UINT16TOLE2(p_compos_data->crpl, MAX_APP_CACHE_SRC_NUM); // for now it is hardcoded. ToDo: should be fnd_static_config_.crpl
    features = 0;
    if (fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_RELAY)
        features |= FND_FEATURE_BIT_RELAY;
    if (fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER)
        features |= FND_FEATURE_BIT_PROXY;
    if (fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND)
        features |= FND_FEATURE_BIT_FRIEND;
    if (fnd_static_config_.features & WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER)
        features |= FND_FEATURE_BIT_LOW_POWER;
    UINT16TOLE2(p_compos_data->features, features);

    // posit on first configured element
    p_nvm_elem = &fnd_config_->elements[0];
    // go through each configured element
    for (elem_idx = 0; elem_idx < fnd_static_config_.elements_num; elem_idx++)
    {
        // <LOC>(2bytes)
        UINT16TOLE2(p_elem->loc, p_nvm_elem->location);
        // number of SIG Modeles
        p_elem->num_m = 0;
        // go through each model in the current configured element and copy SIG models to the message
        for (model_idx = 0; model_idx < p_nvm_elem->models_num; model_idx++)
        {
            if (p_nvm_elem->models[model_idx].company_id == 0)
            {
                UINT16TOLE2(&p_elem->mids[p_elem->num_m * 2], p_nvm_elem->models[model_idx].model_id);
                p_elem->num_m++;
            }
        }
        // number of Vendor Modeles
        p_elem->num_v = 0;
        // go through each model in the current configured element and copy vendor models to the message
        for (model_idx = 0; model_idx < p_nvm_elem->models_num; model_idx++)
        {
            if (p_nvm_elem->models[model_idx].company_id != 0)
            {
                UINT16TOLE2(&p_elem->mids[p_elem->num_m * 2 + p_elem->num_v * 4], p_nvm_elem->models[model_idx].company_id);
                UINT16TOLE2(&p_elem->mids[p_elem->num_m * 2 + p_elem->num_v * 4 + 2], p_nvm_elem->models[model_idx].model_id);
                p_elem->num_v++;
            }
        }
        // end of the message is the start of the next element in the message
        p_elem = (tFND_CONFIG_ELEMENT_COMPOS_DATA*)&p_elem->mids[p_elem->num_m * 2 + p_elem->num_v * 4];
        // posit on the next configured element
        p_nvm_elem = (tFND_NVM_ELEMENT*)&p_nvm_elem->models[p_nvm_elem->models_num];
    }

    access_layer_send(buf, (uint16_t)((uint8_t*)p_elem - &buf[0]), 0xff, 0, src_id, MESH_USE_DEFAULT_TTL, foundation_rcv_friend_idx != -2, NULL, NULL);
}

void foundation_send_config_status_(uint16_t op, uint8_t state, uint16_t src_id)
{
    uint8_t   buf[2 + 1];
    uint8_t   *p;

    // OP code
    p = op2be(&buf[0], op);
    *p++ = state;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

/**
* Following functions handle messages WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET_XXX
*                  WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET_XXX, WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET_XXX,
*
* Parameters:
*   state:          state parameter.
*   src_id:         Address of the message source.
*
* Return:   None
*/
static void handle_config_beacon_set_(uint8_t state, uint16_t src_id)
{
    if (node_nv_data.state_sec_net_beacon != state
        && (state == FND_STATE_SEC_NET_BEACON_ON || state == FND_STATE_SEC_NET_BEACON_OFF))
    {
        node_nv_data.state_sec_net_beacon = state;
        mesh_save_node_nv_data();
#ifndef MESH_CONTROLLER
        mesh_update_beacon();
#endif
    }
    // don't send status if we got RFU value
    if (state == FND_STATE_SEC_NET_BEACON_ON || state == FND_STATE_SEC_NET_BEACON_OFF)
        foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS, node_nv_data.state_sec_net_beacon, src_id);
}

static void handle_config_default_ttl_set_(uint8_t state, uint16_t src_id)
{
    // Ignore prohibited valuse
    if (state == 1 || state > MESH_MAX_TTL)
        return;
    if (node_nv_data.state_default_ttl != state)
    {
        node_nv_data.state_default_ttl = state;
        mesh_save_node_nv_data();
    }
    foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS, node_nv_data.state_default_ttl, src_id);
}
static void handle_config_proxy_set_(uint8_t state, uint16_t src_id)
{
    // don't send status if we got Not Allowed value
    if (state != FND_STATE_PROXY_ON && state != FND_STATE_PROXY_OFF && state != FND_STATE_PROXY_NOT_SUPPORTED)
        return;
    // If the Mesh Proxy Service is not supported, the GATT Proxy state value shall be 0x02 and shall not be changed.
    if (node_nv_data.state_gatt_proxy != FND_STATE_PROXY_NOT_SUPPORTED)
    {
        // If the Mesh Proxy Service is supported, the GATT Proxy state value 0x02 shall not be used
        if (state == FND_STATE_PROXY_ON || state == FND_STATE_PROXY_OFF)
        {
            if (node_nv_data.state_gatt_proxy != state)
            {
                node_nv_data.state_gatt_proxy = state;
                mesh_save_node_nv_data();

                heartbeat_proxy_state_changed();

                // When the GATT Proxy state is set to 0x00, the Node Identity state for all Subnets shall be set to 0x00 and shall not be changed
                if (state == FND_STATE_PROXY_OFF)
                {
                    uint8_t         net_key_idx, key_state;
                    for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
                    {
                        // ignore unexisting key index
                        if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
                            continue;
                        key_state = node_nv_net_key[net_key_idx].state & MESH_NETKEY_STATE_IDENTITY_MASK;
                        if (key_state != FND_STATE_NODE_IDENTITY_OFF)
                        {
                            node_nv_net_key[net_key_idx].state = (node_nv_net_key[net_key_idx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | FND_STATE_NODE_IDENTITY_OFF;
                            saveNodeNvNetKey(net_key_idx);
                        }
                    }
                }
#ifndef MESH_CONTROLLER
                mesh_update_beacon();
#endif
            }
        }
    }
    foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS, node_nv_data.state_gatt_proxy, src_id);
}

static void handle_config_friend_set_(uint8_t state, uint16_t src_id)
{
    // don't send status if we got "Not Allowed" value
    if (state != FND_STATE_FRIEND_ON && state != FND_STATE_FRIEND_OFF && state != FND_STATE_FRIEND_NOT_SUPPORTED)
        return;
    // If the Friend feature is not supported, the Friend state value shall be 0x02 and not be changed.
    if (node_nv_data.state_friend != FND_STATE_FRIEND_NOT_SUPPORTED)
    {
        // If the Friend feature is supported, the Friend state value 0x02 shall not be used.
        if (state == FND_STATE_PROXY_ON || state == FND_STATE_PROXY_OFF)
        {
            if (node_nv_data.state_friend != state)
            {
                node_nv_data.state_friend = state;
                mesh_save_node_nv_data();
                // If the Friend feature is supported and the Friend state changes to value 0x00 and if a node is a friend
                // for one or more Low Power nodes, the node shall terminate all friend relationships and clear the associated Friend Queue
                if (state == FND_STATE_FRIEND_OFF)
                {
                    friend_terminate_all();
                }
                heartbeat_friend_state_changed();
            }
        }
    }
    foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS, node_nv_data.state_friend, src_id);
}

static void foundation_send_config_relay_status_(uint8_t state, uint8_t retrans, uint16_t src_id)
{
    uint8_t   buf[2 + 2];
    uint8_t   *p;
    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_STATUS);
    *p++ = state;
    *p++ = retrans;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static void handle_config_relay_set_(uint16_t src_id, const uint8_t *params, uint16_t params_len)
{
    uint8_t state, retrans;
    if (params_len != 2)
        return;
    state = params[0];
    retrans = params[1];
    // don't send status if we got NA value
    if(state != FND_STATE_RELAY_ON && state != FND_STATE_RELAY_OFF && state != FND_STATE_RELAY_NOT_SUPPORTED)
        return;
    // if relay feature is supported and new value is ON or OFF then accept new value
    if (node_nv_data.state_relay != FND_STATE_RELAY_NOT_SUPPORTED
        && (node_nv_data.state_relay != state || node_nv_data.state_relay_retrans != retrans)
        && (state == FND_STATE_RELAY_ON || state == FND_STATE_RELAY_OFF))
    {
        node_nv_data.state_relay = state;
        node_nv_data.state_relay_retrans = retrans;
        mesh_save_node_nv_data();

        heartbeat_relay_state_changed();
    }
    foundation_send_config_relay_status_(node_nv_data.state_relay, node_nv_data.state_relay_retrans, src_id);
}

static void handle_config_net_transmit_set_(uint16_t src_id, const uint8_t *params, uint16_t params_len)
{
    uint8_t transmit;
    if (params_len != 1)
        return;
    transmit = params[0];
    if (node_nv_data.state_net_trans != transmit)
    {
        node_nv_data.state_net_trans = transmit;
        mesh_save_node_nv_data();
    }
    foundation_send_config_status_(WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS, node_nv_data.state_net_trans, src_id);
}

static uint8_t heartbeat_subscription_count_to_log(uint16_t count)
{
    uint8_t  log;
    uint16_t exp;

    if (count == 0)
        return 0;

    for (log = 1, exp = 2; log < 0x10; log++, exp = exp << 1)
    {
        if (count <= exp - 1)
            return log;
    }
    return log;
}

static uint8_t heartbeat_publication_count_to_log(uint16_t count)
{
    uint8_t     log = 0;
    uint16_t    exp = 1;

    // Count Log value of 0xFF is equivalent to the count value
    if (count == 0xffff)
        log = 0xff;
    else if (count > 0)
    {
        log = 1;
        while (exp < count)
        {
            log++;
            exp = exp << 1;
        }
    }
    return log;
}

static uint16_t heartbeat_count_from_log(uint8_t log)
{
    uint16_t    count = 0;

    if (log == 0xff)
        count = 0xffff;
    else if (log > 0)
        count = ((uint16_t)1) << (log - 1);
    return count;
}

static uint8_t heartbeat_period_to_log(uint32_t period)
{
    uint8_t     log = 0;
    uint32_t    exp = 1;

    log = 1;
    while (exp < period)
    {
        log++;
        exp = exp << 1;
    }
    return log;
}

uint32_t heartbeat_period_from_log(uint8_t log)
{
    return ((uint32_t)1) << (log - 1);
}

static uint16_t heartbeat_publication_get_active_features(void)
{
    uint16_t features = 0;
    if (node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)
        features |= node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_PROXY;
    if (node_nv_data.state_relay == FND_STATE_RELAY_ON)
        features |= node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_RELAY;
    if (node_nv_data.state_friend == FND_STATE_FRIEND_ON)
        features |= node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_FRIEND;
    if (0)
        features |= node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_LOW_POWER;

    TRACE2(TRACE_INFO, "heartbeat_publication_get_active_features: features:%x active:%x\n", node_nv_data.heartbeat_publication.features, features);
    return features;
}

static void heartbeat_publication_start_(void)
{
    uint8_t     buf[3];
    uint32_t  period;
    uint8_t     idx;
    uint16_t  features;

    wiced_stop_timer(&publication_timer);

    features = heartbeat_publication_get_active_features();
    TRACE3(TRACE_INFO, "heartbeat_publication_start_: count:%x global_net_key_idx:%x features:%x\n", node_nv_data.heartbeat_publication.count, node_nv_data.heartbeat_publication.net_key_idx, features);
    if (node_nv_data.heartbeat_publication.count == 0)
        return;
    if (node_nv_data.heartbeat_publication.count < 0xffff)
    {
        node_nv_data.heartbeat_publication.count--;
        mesh_save_node_nv_data();
    }
    if (!find_netkey_(node_nv_data.heartbeat_publication.net_key_idx, &idx))
    {
        TRACE1(TRACE_INFO, "heartbeat_publication_start_: net key is not found node_nv_data.heartbeat_publication.net_key_idx:%x\n", node_nv_data.heartbeat_publication.net_key_idx);
        return;
    }
    // send message
    buf[0] = node_nv_data.heartbeat_publication.ttl;
    UINT16TOBE2(&buf[1], features);
    // Heartbeat uses master security credentials
    transport_layer_send_ctrl(MESH_TL_CM_OP_HEARTBEAT, buf, sizeof(buf), idx,
        NULL, node_nv_data.node_id, node_nv_data.heartbeat_publication.dst, node_nv_data.heartbeat_publication.ttl);

    // start timer
    if (!node_nv_data.heartbeat_publication.count)
        return;

    period = heartbeat_period_from_log(node_nv_data.heartbeat_publication.period_log);
    TRACE2(TRACE_INFO, " net_key_idx:%x period:%d\n", idx, period);
    wiced_start_timer(&publication_timer, period * 1000);
}

static void heartbeat_publication_timer_callback_(uint32_t arg)
{
    TRACE0(TRACE_INFO, "heartbeat_publication_timer_callback_\n");
    heartbeat_publication_start_();
}

void heartbeat_proxy_state_changed(void)
{
    if (node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_PROXY)
    {
        node_nv_data.heartbeat_publication.count = 1;
        heartbeat_publication_start_();
    }
}

void heartbeat_relay_state_changed(void)
{
    if (node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_RELAY)
    {
        node_nv_data.heartbeat_publication.count = 1;
        heartbeat_publication_start_();
    }
}

void heartbeat_friend_state_changed(void)
{
    if (node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_FRIEND)
    {
        node_nv_data.heartbeat_publication.count = 1;
        heartbeat_publication_start_();
    }
}

void heartbeat_low_power_state_changed(void)
{
    if (node_nv_data.heartbeat_publication.features & HEARTBEAT_PUBLISH_LOW_POWER)
    {
        node_nv_data.heartbeat_publication.count = 1;
        heartbeat_publication_start_();
    }
}

void foundation_heartbeat_publication_init_(void)
{
    TRACE0(TRACE_INFO, "foundation_heartbeat_publication_init_\n");
    // initialize timer
    wiced_init_timer(&publication_timer, heartbeat_publication_timer_callback_, 0, WICED_MILLI_SECONDS_TIMER);
    heartbeat_publication_start_();
}

static void send_heartbeat_publication_status_(uint8_t status, uint16_t src_id, uint16_t features)
{
    uint8_t     buf[2 + 10];
    uint16_t    len;
    len = foundation_crt_heartbeat_publication_status(status, node_nv_data.heartbeat_publication.dst, heartbeat_publication_count_to_log(node_nv_data.heartbeat_publication.count), 
        node_nv_data.heartbeat_publication.period_log, node_nv_data.heartbeat_publication.ttl, features, node_nv_data.heartbeat_publication.net_key_idx, buf);
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, len, 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

void handle_heartbeat_publication_get_(uint16_t src_id, uint8_t params_len)
{
    uint8_t net_idx;
    uint8_t status = FND_STATUS_SUCCESS;

    TRACE2(TRACE_INFO, "handle_heartbeat_publication_get_: src_id:%x params_len:%d\n", src_id, params_len);
    if (params_len != 0)
        return;
    if (!find_netkey_(node_nv_data.heartbeat_publication.net_key_idx, &net_idx))
        status = FND_STATUS_INVALID_NETKEY;

    send_heartbeat_publication_status_(status, src_id, node_nv_data.heartbeat_publication.features);
}

void handle_heartbeat_publication_set_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    MeshHeartbeatPublication    pub = { 0 };
    uint8_t                     count_log;
    uint8_t                     net_key_idx;

    TRACE2(TRACE_INFO, "handle_heartbeat_publication_set_: src_id:%x params_len:%d\n", src_id, params_len);
    TRACEN(TRACE_INFO, (char*)params, params_len);

    if (params_len != 9)
        return;

    pub.dst = LE2TOUINT16(params);
    params += 2;
    count_log = *params++;
    pub.period_log = *params++;
    pub.ttl= *params++;
    pub.features = LE2TOUINT16(params);
    params += 2;
    pub.net_key_idx = LE2TOUINT16(params);

    // check all params
    if (((pub.dst & MESH_GROUP_MASK) == MESH_VIRT_ADDR)
        || (count_log >= 0x12 && count_log <= 0xfe)
        || (pub.period_log >= 0x12)
        || (pub.ttl > MESH_MAX_TTL)
        || (pub.net_key_idx > 0x0fff))
    {
        return;
    }

    if (!find_netkey_(pub.net_key_idx, &net_key_idx))
    {
        uint8_t     buf[2 + 10];
        uint16_t    len;
        len = foundation_crt_heartbeat_publication_status(FND_STATUS_INVALID_NETKEY, pub.dst, count_log, pub.period_log, pub.ttl, pub.features, pub.net_key_idx, buf);
        // 0xffffffff in the p_event means do random delay 20-50 ms
        access_layer_send(buf, len, 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
        return;
    }
        pub.count = heartbeat_count_from_log(count_log);
        node_nv_data.heartbeat_publication = pub;
        mesh_save_node_nv_data();

    // send status message. 0 features indicates that the message was not caused by any of monitored bits (change of relay, proxy, friend state)
    send_heartbeat_publication_status_(FND_STATUS_SUCCESS, src_id, 0);

    if (pub.dst == MESH_NODE_ID_INVALID)
    {
        TRACE0(TRACE_INFO, "handle_heartbeat_publication_set_ no pubs for dst=0\n");
        return;
    }
        // start timer and send message
        heartbeat_publication_start_();
    }

/*
 * This function should be called when net key with net_key_idx is about to be removed
 */
void hearbeat_netkey_deleted(uint8_t net_key_idx)
{
    if (node_nv_data.heartbeat_publication.net_key_idx != net_key_idx)
        return;
    wiced_stop_timer(&publication_timer);
    node_nv_data.heartbeat_publication.count    = 0;
    node_nv_data.heartbeat_publication.features = 0;
}
static void send_heartbeat_subs_status_(uint8_t status, uint16_t src_id)
{
    uint8_t     buf[2 + 9];
    uint16_t    len;
    len = foundation_crt_heartbeat_subs_status(status, node_nv_data.heartbeat_subs.src, node_nv_data.heartbeat_subs.dst, node_nv_data.heartbeat_subs.period_log,
        node_nv_data.heartbeat_subs.count, node_nv_data.heartbeat_subs.min_hops, node_nv_data.heartbeat_subs.max_hops, buf);
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, len, 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

void handle_heartbeat_subs_get_(uint16_t src_id, uint8_t params_len)
{
    uint32_t passed_time;
    uint32_t period;

    TRACE2(TRACE_INFO, "handle_heartbeat_subs_get_: src_id:%x params_len:%d\n", src_id, params_len);
    if (params_len != 0)
        return;

    // adjust period log
    if (node_nv_data.heartbeat_subs.start_time != 0)
    {
        uint64_t current_time = wiced_bt_mesh_core_get_tick_count() / 1000;
        passed_time = (uint32_t)(current_time - node_nv_data.heartbeat_subs.start_time);

        period = heartbeat_period_from_log(node_nv_data.heartbeat_subs.period_log);

        if (passed_time >= period)
        {
            node_nv_data.heartbeat_subs.period_log = 0;
            mesh_save_node_nv_data();
        }
        else
        {
            node_nv_data.heartbeat_subs.period_log = heartbeat_period_to_log(period - passed_time);
        }
        TRACE2(TRACE_INFO, "heartbeat_subs_get_: passed_time:%x period:%x\n", passed_time, node_nv_data.heartbeat_subs.period_log);
    }
    send_heartbeat_subs_status_(FND_STATUS_SUCCESS, src_id);
}

void handle_heartbeat_subs_set_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    uint8_t             status;
    MeshHeartbeatSubs   subs;
    uint8_t             period_log;
    uint64_t            current_time;

    TRACE1(TRACE_INFO, "handle_heartbeat_subs_set_: src_id:%x\n", src_id);
    TRACEN(TRACE_INFO, (char*)params, params_len);

    if (params_len != 5)
        return;

    // take current state
    subs = node_nv_data.heartbeat_subs;

    // take params
    subs.src = LE2TOUINT16(params);
    params += 2;
    subs.dst = LE2TOUINT16(params);
    params += 2;
    period_log = *params;
    subs.period_log = period_log;

    current_time = wiced_bt_mesh_core_get_tick_count() / 1000;

    // check all params
    // Source shall be the unassigned address or a unicast address
    // Destination shall be the unassigned address, the primary unicast address of this node, or a group address
    if ((subs.src != MESH_NODE_ID_INVALID && (subs.src & MESH_NON_UNICAST_MASK) != 0)
        || (subs.dst != MESH_NODE_ID_INVALID && subs.dst != node_nv_data.node_id && (subs.dst & MESH_GROUP_MASK) != MESH_GROUP_ADDR)
        || (period_log >= 0x12))    // period_log is valid
    {
        return;
    }
    else
    {
        status = FND_STATUS_SUCCESS;
        // stop receiving heartbeat messages if src or dst is unassigned or period_log is 0
        if (subs.src == MESH_NODE_ID_INVALID || subs.dst == MESH_NODE_ID_INVALID || period_log == 0
            || (node_nv_data.heartbeat_subs.start_time != 0 && node_nv_data.heartbeat_subs.start_time + heartbeat_period_from_log(subs.period_log) <= current_time))        // or new period is expired already
        {
            TRACE1(TRACE_DEBUG, "handle_heartbeat_subs_set_: stop receiving heartbeat messages count:%d\n", subs.count);

            if (node_nv_data.heartbeat_subs.src != subs.src || node_nv_data.heartbeat_subs.dst != subs.dst)
            {
                subs.count  = 0;
            }
            subs.src = MESH_NODE_ID_INVALID;
            subs.dst = MESH_NODE_ID_INVALID;
            subs.min_hops   = 0; 
            subs.max_hops   = 0;
            subs.period_log = 0;
            subs.start_time = 0;
        }
        // if src or dst differs from the current value or current period is expired then start receiving heartbeat messages
        else if (node_nv_data.heartbeat_subs.src != subs.src || node_nv_data.heartbeat_subs.dst != subs.dst
            || node_nv_data.heartbeat_subs.start_time == 0
            || node_nv_data.heartbeat_subs.start_time + heartbeat_period_from_log(node_nv_data.heartbeat_subs.period_log) <= current_time)
        {
            TRACE2(TRACE_DEBUG, "handle_heartbeat_subs_set_: start receiving heartbeat messages time:%x%x\n", (uint32_t)((current_time >> 32) & 0xFFFFFFFF), (uint32_t)(current_time & 0xFFFFFFFF));
            subs.count = 0;
            subs.min_hops = 0x7f;
            subs.max_hops = 0;
            subs.start_time = current_time; 
        }
        node_nv_data.heartbeat_subs = subs;
        mesh_save_node_nv_data();
    }
    send_heartbeat_subs_status_(status, src_id);
}

/**
* Creates application payload for message Heartbeat Publication Set
*
* Parameters:
*   dst:            Destination Address for Heartbeat messages
*   count_log:      Number of Heartbeat messages to be sent
*   period_log:     Period for sending Heartbeat messages
*   ttl:            TTL to be used when sending Heartbeat messages
*   features:       Bit field indicating features that trigger Heartbeat messages when changed
*   net_key_idx:    NetKey Index
*   buf:    Bufer for command. Should be big enough.
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_heartbeat_publication_set(uint16_t dst, uint8_t count_log, uint8_t period_log, uint8_t ttl, uint16_t features, uint16_t net_key_idx,  uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET) - buf);
    UINT16TOLE2(&buf[len], dst);
    len += 2;
    buf[len++] = count_log;
    buf[len++] = period_log;
    buf[len++] = ttl;
    UINT16TOLE2(&buf[len], features);
    len += 2;
    UINT16TOLE2(&buf[len], net_key_idx);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_heartbeat_publication_set:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates application payload for message Heartbeat Publication Status
*
* Parameters:
*   status:         Status Code for the requesting message
*   dst:            Destination Address for Heartbeat messages
*   count:          Number of Heartbeat messages to be sent
*   period_log:     Period for sending Heartbeat messages
*   ttl:            TTL to be used when sending Heartbeat messages
*   features:       Bit field indicating features that trigger Heartbeat messages when changed
*   net_key_idx:    NetKey Index
*   buf:    Bufer for command. Should be big enough.
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_heartbeat_publication_status(uint8_t status, uint16_t dst, uint16_t count_log, uint8_t period_log, uint8_t ttl, uint16_t features, uint16_t net_key_idx, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_STATUS) - buf);
    buf[len++] = status;
    UINT16TOLE2(&buf[len], dst);
    len += 2;
    buf[len++] = (uint8_t)count_log;
    buf[len++] = period_log;
    buf[len++] = ttl;
    UINT16TOLE2(&buf[len], features);
    len += 2;
    UINT16TOLE2(&buf[len], net_key_idx);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_heartbeat_publication_status:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates application payload for message Heartbeat Subscription Set
*
* Parameters:
*   src:            Source Address for Heartbeat messages
*   dst:            Destination Address for Heartbeat messages
*   period_log:     Period for receiving Heartbeat messages
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_heartbeat_subs_set(uint16_t src, uint16_t dst, uint8_t period_log, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET) - buf);
    UINT16TOLE2(&buf[len], src);
    len += 2;
    UINT16TOLE2(&buf[len], dst);
    len += 2;
    buf[len++] = period_log;
    TRACE0(TRACE_INFO, "foundation_crt_heartbeat_subs_set:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates application payload for message Heartbeat Subscription Status
*
* Parameters:
*   status:         Status Code for the requesting message
*   src:            Source Address for Heartbeat messages
*   dst:            Destination Address for Heartbeat messages
*   period:         Remaining Period for processing Heartbeat messages
*   count:          Number of Heartbeat messages received since receiving the most recent Heartbeat Subscription Set message
*   min_hops:       Minimum hops registered when receiving Heartbeat messages since receiving the most recent Heartbeat Subscription Set messag
*   max_hops:       Maximum hops registered when receiving Heartbeat messages since receiving the most recent Heartbeat Subscription Set message
*
* Return:   Length of the command in the buf.
*           0 on error.
*/
uint16_t foundation_crt_heartbeat_subs_status(uint8_t status, uint16_t src, uint16_t dst, uint8_t period_log, uint16_t count, uint8_t min_hops, uint8_t max_hops, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_STATUS) - buf);
    buf[len++] = status;
    UINT16TOLE2(&buf[len], src);
    len += 2;
    UINT16TOLE2(&buf[len], dst);
    len += 2;
    buf[len++] = period_log;
    buf[len++] = heartbeat_subscription_count_to_log(count);
    buf[len++] = min_hops;
    buf[len++] = max_hops;
    TRACE0(TRACE_INFO, "foundation_crt_heartbeat_subs_status:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message model subscrition add, del and overwrite
*
* Parameters:
*   opcode:         Opcode
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Address
*   subs_addr_len:  Length of the Subscription Address. It can be 2 or 16
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
static uint16_t foundation_crt_subs_add_del_ovr_msg_(uint16_t opcode, uint16_t elem_addr, const uint8_t *subs_addr, uint8_t subs_addr_len, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, opcode) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    memcpy(&buf[len], subs_addr, subs_addr_len);
    len += subs_addr_len;

    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_subs_add_del_ovr_msg_:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message model subscrition add
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Address
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_add_msg(uint16_t elem_addr, uint16_t subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint8_t subs_addr_le[2];
    UINT16TOLE2(subs_addr_le, subs_addr);
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD, elem_addr, subs_addr_le, 2, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition virtual address add
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Virtual Address 16 bytes length
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_virt_addr_add_msg(uint16_t elem_addr, const uint8_t *subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD, elem_addr, subs_addr, 16, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition del
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Address
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_del_msg(uint16_t elem_addr, uint16_t subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint8_t subs_addr_le[2];
    UINT16TOLE2(subs_addr_le, subs_addr);
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE, elem_addr, subs_addr_le, 2, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition virtual address del
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Virtual Address 16 bytes length
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_virt_addr_del_msg(uint16_t elem_addr, const uint8_t *subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE, elem_addr, subs_addr, 16, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition overwrite
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Address
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_ovr_msg(uint16_t elem_addr, uint16_t subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint8_t subs_addr_le[2];
    UINT16TOLE2(subs_addr_le, subs_addr);
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE, elem_addr, subs_addr_le, 2, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition virtual address overwrite
*
* Parameters:
*   elem_addr:      Address of the element
*   subs_addr:      Subscription Virtual Address 16 bytes length
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_virt_addr_ovr_msg(uint16_t elem_addr, const uint8_t *subs_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    return foundation_crt_subs_add_del_ovr_msg_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE, elem_addr, subs_addr, 16, company_id, model_id, buf);
}

/**
* Creates the application payload for message model subscrition del all
*
* Parameters:
*   elem_addr:      Address of the element
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_del_all_msg(uint16_t elem_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;

    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_subs_del_all_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message model subscrition add
*
* Parameters:
*   elem_addr:      Address of the element
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_subs_get_msg(uint16_t elem_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, company_id ? WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET : WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_subs_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}


/**
* Creates the application payload for message node reset
*
* Parameters:
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_prov_node_reset_msg(uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_NODE_RESET) - buf);
    TRACE0(TRACE_INFO, "foundation_crt_prov_node_reset_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;

}

/**
* Creates the application payload for message provisioning model app bind and unbind
*
* Parameters:
*   opcode:         Opcode
*   elem_addr:      Address of the element
*   global_app_key_idx:    Global Index of the AppKey
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
static uint16_t foundation_crt_prov_model_app_bind_unbind_msg_(uint16_t opcode, uint16_t elem_addr, uint16_t global_app_key_idx, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, opcode) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    UINT16TOLE2(&buf[len], global_app_key_idx);
    len += 2;
    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_prov_model_app_bind_unbind_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the application payload for message provisioning model app bind
*
* Parameters:
*   elem_addr:      Address of the element
*   global_app_key_idx:    Global Index of the AppKey
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_prov_model_app_bind_msg(uint16_t elem_addr, uint16_t global_app_key_idx, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    return foundation_crt_prov_model_app_bind_unbind_msg_(WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND, elem_addr, global_app_key_idx, company_id, model_id, buf);
}

/**
* Creates the application payload for message provisioning model app unbind
*
* Parameters:
*   elem_addr:      Address of the element
*   global_app_key_idx:    Global Index of the AppKey
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_prov_model_app_unbind_msg(uint16_t elem_addr, uint16_t global_app_key_idx, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    return foundation_crt_prov_model_app_bind_unbind_msg_(WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND, elem_addr, global_app_key_idx, company_id, model_id, buf);
}

/**
* Creates the application payload for message provisioning model app get
*
* Parameters:
*   elem_addr:      Address of the element
*   company_id:     Company ID of the Model ID
*   model_id:       Model ID
*   buf:            Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_prov_model_app_get_msg(uint16_t elem_addr, uint16_t company_id, uint16_t model_id, uint8_t *buf)
{
    uint16_t len = 0;
    uint16_t opcode = company_id == 0 ? WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET : WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET;
    len = (uint16_t)(op2be(buf, opcode) - buf);
    UINT16TOLE2(&buf[len], elem_addr);
    len += 2;
    if (company_id)
    {
        UINT16TOLE2(&buf[len], company_id);
        len += 2;
    }
    UINT16TOLE2(&buf[len], model_id);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_prov_model_app_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Converts opcode to model id
*
* Parameters:
*   opcode:         opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*
* Return:   model id. On error return oxffff
*/
uint16_t foundation_opcode_to_model_(uint16_t opcode)
{
    uint16_t  model_id;

    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_APPKEY_ADD:
    case WICED_BT_MESH_CORE_CMD_APPKEY_DELETE:
    case WICED_BT_MESH_CORE_CMD_APPKEY_GET:
    case WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE:

    case WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND:
    case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET:
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND:

    case WICED_BT_MESH_CORE_CMD_NETKEY_ADD:
    case WICED_BT_MESH_CORE_CMD_NETKEY_DELETE:
    case WICED_BT_MESH_CORE_CMD_NETKEY_GET:
    case WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE:

    case WICED_BT_MESH_CORE_CMD_NODE_RESET:

    case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET:

    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET:
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL:
    case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE:

    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET:
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET:

    case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET:

    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET:
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET:
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET:
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET:

    case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET:
        model_id = WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV;
        break;

    case WICED_BT_MESH_CORE_CMD_APPKEY_STATUS:
    case WICED_BT_MESH_CORE_CMD_APPKEY_LIST:
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_STATUS:
    case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_LIST:
    case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_LIST:
    case WICED_BT_MESH_CORE_CMD_NETKEY_STATUS:
    case WICED_BT_MESH_CORE_CMD_NODE_RESET_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS:
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_LIST:
    case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_LIST:
    case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS:
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_STATUS:
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_STATUS:
    case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_STATUS:
        model_id = WICED_BT_MESH_CORE_MODEL_ID_CONFIG_CLNT;
        break;

    case WICED_BT_MESH_CORE_CMD_ATTENTION_GET:
    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET:
    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED:
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET:
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET:
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED:
        model_id = WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV;
        break;

    case WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS:
    case WICED_BT_MESH_CORE_CMD_HEALTH_CURRENT_STATUS:
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_STATUS:
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS:
        model_id = WICED_BT_MESH_CORE_MODEL_ID_HEALTH_CLNT;
        break;
    default:
        model_id = 0xffff;
        break;
    }

    TRACE2(TRACE_DEBUG, "foundation_opcode_to_model_: opcode:%x model_id:%x\n", opcode, model_id);

    return model_id;
}

uint8_t foundation_get_init_models_(tFND_INIT_MODEL *p_models, uint8_t models_max_num)
{
    wiced_bool_t                failed = WICED_FALSE;
    uint8_t               models_count = 0;
    uint16_t              company_id, model_id;
    tFND_NVM_ELEMENT    *p_nvm_elem;
    uint8_t               idx, idx_element, idx_model;
    tFND_INIT_MODEL     *p_m;
    
    // go through all elements
    for (idx_element = 0, p_nvm_elem = fnd_config_->elements;                                                // first element
        idx_element < fnd_static_config_.elements_num;
        idx_element++, p_nvm_elem = (tFND_NVM_ELEMENT*)((uint8_t*)&p_nvm_elem->models[0] + p_nvm_elem->models_num * sizeof(tFND_NVM_MODEL)))  // go to the next element
    {
        // go through all models of the current element
        for (idx_model = 0; idx_model < p_nvm_elem->models_num; idx_model++)
        {
            model_id = p_nvm_elem->models[idx_model].model_id;
            company_id = p_nvm_elem->models[idx_model].company_id;
            for (idx = 0; idx < models_count; idx++)
            {
                if (p_models[idx].model_id == model_id && p_models[idx].company_id == company_id)
                    break;
            }
            p_m = &p_models[idx];
            if (idx >= models_count)
            {
                if (models_count >= models_max_num)
                {
                    failed = WICED_TRUE;
                    break;
                }
                p_m->elements_num = 0;
                p_m->company_id = company_id;
                p_m->model_id = model_id;
                models_count++;
            }
            if (p_m->elements_num >= sizeof(p_m->elements_idx) / sizeof(p_m->elements_idx[0]))
            {
                failed = WICED_TRUE;
                break;
            }
            // make sure it is not second instance of the same model in the same element
            if(p_m->elements_num > 0 && p_m->elements_idx[p_m->elements_num - 1] == idx_element)
            {
                failed = WICED_TRUE;
                break;
            }
            p_m->elements_idx[p_m->elements_num++] = idx_element;
        }
        if (failed)
            break;
    }

    TRACE2(TRACE_INFO, "foundation_get_init_models_: failed:%d models_count:%d\n", (uint8_t)failed, models_count);
    TRACEN(TRACE_DEBUG, (char*)p_models, models_count * sizeof(*p_models));
    return failed ? 0 : models_count;
}

/**
* Creates the acceess payload for the message Node Identity get
*
* Parameters:
*   global_net_key_idx: Index of the NetKey
*   buf:                Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_node_identity_get_msg(uint16_t global_net_key_idx, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET) - buf);
    UINT16TOLE2(&buf[len], global_net_key_idx);
    len += 2;
    TRACE0(TRACE_INFO, "foundation_crt_node_identity_get_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the acceess payload for the message Node Identity set
*
* Parameters:
*   global_net_key_idx: Index of the NetKey
*   state:              New Node Identity state
*   buf:                Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_node_identity_set_msg(uint16_t global_net_key_idx, uint8_t state, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET) - buf);
    UINT16TOLE2(&buf[len], global_net_key_idx);
    len += 2;
    buf[len++] = state;
    TRACE0(TRACE_INFO, "foundation_crt_node_identity_set_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates the acceess payload for the message Node Identity status
*
* Parameters:
*   status:             Status Code for the requesting message
*   global_net_key_idx: Index of the NetKey
*   state:              New Node Identity state
*   buf:                Bufer for message. Should be big enough.
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t foundation_crt_node_identity_status_msg(uint8_t status, uint16_t global_net_key_idx, uint8_t state, uint8_t *buf)
{
    uint16_t len = 0;
    len = (uint16_t)(op2be(buf, WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_STATUS) - buf);
    buf[len++] = status;
    UINT16TOLE2(&buf[len], global_net_key_idx);
    len += 2;
    buf[len++] = state;
    TRACE0(TRACE_INFO, "foundation_crt_node_identity_status_msg:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

static void send_node_identity_status_(uint8_t status, uint16_t global_net_key_idx, uint8_t state, uint16_t src)
{
    uint8_t     buf[4];
    uint16_t    len;
    len = foundation_crt_node_identity_status_msg(status, global_net_key_idx, state, buf);
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, len, 0xff, 0, src, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static void handle_node_identity_get_(uint16_t global_net_key_idx, uint16_t src)
{
    uint8_t     net_key_idx;
    uint8_t     status = FND_STATUS_SUCCESS;
    uint8_t     state;
    if (!find_netkey_(global_net_key_idx & MESH_GL_KEY_IDX_MASK, &net_key_idx))
    {
        status = FND_STATUS_INVALID_NETKEY;
        state = 0;
    }
    else
        state = node_nv_net_key[net_key_idx].state & MESH_NETKEY_STATE_IDENTITY_MASK;
    send_node_identity_status_(status, global_net_key_idx, state, src);
}

static void node_identity_timer_callback_(uint32_t arg)
{
    TRACE1(TRACE_INFO, "node_identity_timer_callback_: node_identity_timer_net_key_idx:%x\n", node_identity_timer_net_key_idx);
    // it should not happen
    if (node_identity_timer_net_key_idx == 0xff)
        return;
    if (node_identity_timer_net_key_idx != 0xfe)
    {
        if (0 != (node_nv_data.net_key_bitmask & (1 << node_identity_timer_net_key_idx)))
            node_nv_net_key[node_identity_timer_net_key_idx].state = (node_nv_net_key[node_identity_timer_net_key_idx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | FND_STATE_NODE_IDENTITY_OFF;
    }
    node_identity_timer_net_key_idx = 0xff;
#ifndef MESH_CONTROLLER
    mesh_update_beacon();
#endif
}

static void set_node_identity(uint8_t state)
{
    //when the Server starts advertising as a result of the user interaction, the server shall interleave the advertising of each subnet it is a member of.This advertising is limited to 60 seconds.
    TRACE3(TRACE_INFO, "set_node_identity: state:%x node_identity_timer_inited:%d node_identity_timer_net_key_idx:%x\n", state, node_identity_timer_inited, node_identity_timer_net_key_idx);
#ifndef MESH_CONTROLLER
    if (!node_identity_timer_inited)
    {
        node_identity_timer_inited = WICED_TRUE;
        wiced_init_timer(&node_identity_timer, node_identity_timer_callback_, 0, WICED_SECONDS_TIMER);
    }
    if (node_identity_timer_net_key_idx != 0xff)
    {
        wiced_stop_timer(&node_identity_timer);
        if (node_identity_timer_net_key_idx != 0xfe)
            if (0 != (node_nv_data.net_key_bitmask & (1 << node_identity_timer_net_key_idx)))
                node_nv_net_key[node_identity_timer_net_key_idx].state = (node_nv_net_key[node_identity_timer_net_key_idx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | FND_STATE_NODE_IDENTITY_OFF;
    }
    node_identity_timer_net_key_idx = state;
    wiced_start_timer(&node_identity_timer, 60);
#endif
}

/**
* \brief Sets node identity state as a result of user interaction.
*/
void wiced_bt_mesh_core_set_node_identity(void)
{
    set_node_identity(0xfe);
#ifndef MESH_CONTROLLER
    mesh_update_beacon();
#endif
}

static void handle_node_identity_set_(uint16_t global_net_key_idx, uint8_t state, uint16_t src_id)
{
    uint8_t     net_key_idx;
    uint8_t     status, current_state, new_state;
    // ignore and don't send status if we got Prohibited value
    if (state != FND_STATE_NODE_IDENTITY_ON && state != FND_STATE_NODE_IDENTITY_OFF && state != FND_STATE_NODE_IDENTITY_NOT_SUPPORTED)
        return;
    new_state = state;
    if (!find_netkey_(global_net_key_idx & MESH_GL_KEY_IDX_MASK, &net_key_idx))
        status = FND_STATUS_INVALID_NETKEY;
    else
    {
        status = FND_STATUS_SUCCESS;

        // If the Mesh Proxy Service is not supported, then Node Identity state value shall be 0x02 and not be changed.
        if (node_nv_data.state_gatt_proxy == FND_STATE_PROXY_NOT_SUPPORTED)
            new_state = FND_STATE_NODE_IDENTITY_NOT_SUPPORTED;
        // If the Mesh Proxy Service is supported, the Node Identity state value 0x02 shall not be used. Ignore such message.
        else if (new_state == FND_STATE_NODE_IDENTITY_NOT_SUPPORTED)
            return;

        current_state = node_nv_net_key[net_key_idx].state & MESH_NETKEY_STATE_IDENTITY_MASK;
        if (current_state != new_state)
        {
            node_nv_net_key[net_key_idx].state = (node_nv_net_key[net_key_idx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | new_state;
            saveNodeNvNetKey(net_key_idx);
#ifndef MESH_CONTROLLER
            //when the Server starts advertising as a result of the Node Identity state being enabled, the Server shall only advertise using the Subnet that it was enabled on.This advertising is limited to 60 seconds.
            if (new_state == FND_STATE_NODE_IDENTITY_ON)
                set_node_identity(new_state);
            // on turning off or disable stop any timer
            else if (node_identity_timer_net_key_idx != 0xff) {
                node_identity_timer_net_key_idx = 0xff;
                wiced_stop_timer(&node_identity_timer);
            }
            mesh_update_beacon();
#endif
        }
    }
    // On error the NetKeyIndex and Identity fields are defined by the incoming message
    if (status != FND_STATUS_SUCCESS)
        new_state = state;
    send_node_identity_status_(status, global_net_key_idx, new_state, src_id);
}

static void foundation_send_config_lpn_poll_timeout_status_(uint16_t lpn_address, uint32_t poll_timeout, uint16_t src_id)
{
    uint8_t   buf[2 + 2 + 3];
    uint8_t   *p;
    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_STATUS);
    UINT16TOLE2(p, lpn_address);
    p += 2;
    UINT32TOLE3(p, poll_timeout);
    p += 3;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static void handle_config_lpn_poll_timeout_get_(uint16_t src_id, const uint8_t *params, uint16_t params_len)
{
    uint16_t    lpn_address;
    uint32_t    poll_timeout;
    TRACE1(TRACE_INFO, "handle_config_lpn_poll_timeout_get_: src_id:%x\n", src_id);
    TRACEN(TRACE_INFO, (char*)params, params_len);

    if (params_len != 2)
        return;

    lpn_address = LE2TOUINT16(params);
    // ignore that message in case of invalid address
    if ((lpn_address & MESH_NON_UNICAST_MASK) == 0
        && lpn_address != MESH_NODE_ID_INVALID)
    {
        poll_timeout = friend_get_poll_timeout(lpn_address);
        foundation_send_config_lpn_poll_timeout_status_(lpn_address, poll_timeout, src_id);
    }
}
