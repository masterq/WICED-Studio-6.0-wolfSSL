// MeshControllerDlg.cpp : implementation file
//

#include "stdafx.h"
#include <setupapi.h>
#include "MeshController.h"
#include "MeshControllerDlg.h"
#include "afxdialogex.h"
#include "DeviceSelect.h"
extern "C" {
#include "platform.h"
#include "mesh.h"
#include "mesh_core.h"
#include "aes_cmac.h"
#include "core_ovl.h"
#include "core_aes_ccm.h"
#include "wiced_bt_mesh_provision.h"
#include "ecdh.h"
#include "key_refresh.h"
#include "access_layer.h"
#include "transport_layer.h"
#include "foundation_int.h"
#include "foundation.h"
//#include "generic_on_off.h"
//#include "generic_level_int.h"
//#include "generic_def_trans_time.h"
//#include "generic_level.h"
//#include "generic_power_onoff.h"
#include "wiced_bt_mesh_models.h"
#include "network_layer.h"
#include "health.h"
#include "wiced_bt_firmware_upgrade.h"
#include "DeviceSelectAdv.h"

#define WM_PROGRESS     (WM_USER + 103)

uint8_t wiced_hal_read_nvram(uint16_t vs_id,
    uint8_t          data_length,
    uint8_t        * p_data,
    wiced_result_t * p_status)
{
    return 0;
}

}

static void wiced_timer_handle();

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//#define HARDCODED_UUID 0xab, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xab


static const char* szRegKey = "Software\\Cypress\\Mesh\\";

WCHAR *szDefaultPassPhrase = L"Mesh Specification";
WCHAR *szAppSalt1 = L"smat";
WCHAR *szAppSalt2 = L"smak";
WCHAR *szAppSalt3 = L"smit";
WCHAR *szAppSalt4 = L"smiz";

CMeshControllerDlg *m_hDlg;

extern "C" void wiced_bt_mesh_provision_started(UINT32 conn_id);
extern "C" void wiced_bt_mesh_provision_end(UINT32 conn_id, UINT8 result);
extern "C" wiced_bool_t wiced_bt_mesh_provision_get_oob(UINT32 conn_id, UINT8 type, UINT8 type_or_size, UINT8 location_or_action);
extern "C" wiced_bool_t wiced_bt_mesh_provision_on_capabilities(uint32_t conn_id, const wiced_bt_mesh_provision_capabilities_t *capabilities);
extern "C" wiced_bool_t wiced_bt_mesh_provision_get_capabilities(UINT32 conn_id);

extern "C" void mesh_controller_create_node(UINT8 *mesh_id);
extern "C" MeshResultCode mesh_get_dev_addr(UINT8* node_id);
extern "C" MeshResultCode mesh_get_iv_index(UINT8* iv_index);
extern "C" MeshResultCode mesh_get_dev_key(UINT8* key);
extern "C" MeshResultCode mesh_get_app_info(UINT8* p_data);
extern "C" MeshResultCode mesh_get_net_info(UINT8* p_data);

const WCHAR* op2str(UINT16 op);

static const BYTE key_0[MESH_KEY_LEN] = { 0 };

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

const WCHAR* data2str(const UINT8* p_data, UINT32 len)
{
    UINT32 i;
    static WCHAR buf[16 * 3 + 4]; // 16 hex bytes with space delimiter and with terminating 0 and with possible "..."
    buf[0] = 0;

    for (i = 0; i < len && i < sizeof(buf) / sizeof(buf[0]) - 4; i++)
    {
        wsprintf(&buf[i*3], L"%02x ", p_data[i]);
    }
    if (i < len)
        wcscpy_s(&buf[i * 3], sizeof(buf) / sizeof(buf[0]) - (i * 3), L"...");
    return buf;
}

const WCHAR* keyidx2str(const UINT8* p_data, UINT32 len)
{
    static WCHAR buf[8 * 4 + 4]; // 8 key indexes(3 characters each) with space delimiter and with terminating 0 and with possible "..."
    WCHAR *p_out = buf;
    buf[0] = 0;

    while (len > 1)
    {
        if ((p_out - buf) >= (sizeof(buf) / sizeof(buf[0]) - 4))
            break;
        wsprintf(p_out, L"%03x ", LE2TOUINT12(p_data));
        p_out += 4;
        p_data++;
        len--;
        if (len < 2)
            break;
        if ((p_out - buf) >= (sizeof(buf) / sizeof(buf[0]) - 4))
            break;
        wsprintf(p_out, L"%03x ", LE2TOUINT12_2(p_data));
        p_out += 4;
        p_data += 2;
        len -= 2;
    }
    if (len > 1)
        wcscpy_s(p_out, sizeof(buf) / sizeof(buf[0]) - (p_out - buf), L"...");
    return buf;
}

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMeshControllerDlg dialog



CMeshControllerDlg::CMeshControllerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMeshControllerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_btInterface = NULL;
    m_pDeviceSelectDialog = NULL;
    m_pDownloader = NULL;
    m_pPatch = NULL;
    m_dwPatchSize = 0;
    m_hCfgEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
}

CMeshControllerDlg::~CMeshControllerDlg()
{
	delete m_btInterface;
    delete m_pDownloader;
    if (m_pPatch)
        delete m_pPatch;
}

void CMeshControllerDlg::SetParam(BLUETOOTH_ADDRESS *bth, HMODULE hLib)
{
    if (m_bWin10)
        m_btInterface = new CBtWin10Interface(bth, this);
    else if (m_bWin8) 
        m_btInterface = new CBtWin8Interface(bth, hLib, this);
    else
        m_btInterface = new CBtWin7Interface(bth, hLib, this);
}

void CMeshControllerDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_PROGRESS, m_Progress);
}

void CMeshControllerDlg::InitControls()
{
    MeshNvAppKey app_key;
    MeshNvNetKey net_key;
    UINT8 buf[100];

    mesh_get_dev_key(buf);
    SetHexValue(IDC_DEV_KEY, buf, MESH_KEY_LEN);

    mesh_get_app_info((UINT8*)&app_key);
    SetHexValue(IDC_APP_KEY, app_key.key, MESH_KEY_LEN);
    SetHexValue(IDC_APP_ID, &app_key.aid, sizeof(app_key.aid));

    mesh_get_dev_addr(buf);
    SetHexValue(IDC_MY_NODE_ID, buf, MESH_NODE_ID_LEN);

    mesh_get_iv_index(buf);
    SetHexValue(IDC_NET_IV_INDEX, buf, MESH_IV_INDEX_LEN);

    mesh_get_net_info((UINT8*)&net_key);
    SetHexValue(IDC_NET_KEY, net_key.key, MESH_KEY_LEN);
    SetHexValue(IDC_NID, &net_key.sec_material.nid, sizeof(net_key.sec_material.nid));
    SetHexValue(IDC_ENCR_KEY, net_key.sec_material.encr_key, MESH_KEY_LEN);
    SetHexValue(IDC_PRIVACY_KEY, net_key.sec_material.privacy_key, MESH_KEY_LEN);
    SetHexValue(IDC_NET_ID, net_key.network_id, MESH_NETWORK_ID_LEN);
    SetHexValue(IDC_IDENTITY_KEY, net_key.identity_key, MESH_KEY_LEN);
    SetHexValue(IDC_BEACON_KEY, net_key.beacon_key, MESH_KEY_LEN);
    SetHexValue(IDC_PROXY_KEY, net_key.proxy_key, MESH_KEY_LEN);
}

BEGIN_MESSAGE_MAP(CMeshControllerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_WM_TIMER()
    ON_MESSAGE(WM_CONNECTED, OnConnected)
    ON_MESSAGE(WM_USER_PROXY_DATA, OnProxyDataIn)
    ON_MESSAGE(WM_USER_PROVISIONING_DATA, OnProvisioningDataIn)
    ON_MESSAGE(WM_USER_COMMAND_DATA, OnCommandDataIn)
    ON_MESSAGE(WM_USER_WS_UPGRADE_CTRL_POINT, OnWsUpgradeCtrlPoint)
    ON_MESSAGE(WM_USER_SELECT_DEVICE, OnSelectDevice)
    ON_BN_CLICKED(IDC_PROVISION, &CMeshControllerDlg::OnBnClickedProvision)
    ON_BN_CLICKED(IDC_SEND, &CMeshControllerDlg::OnBnClickedSend)
    ON_BN_CLICKED(IDC_NEW_MESH, &CMeshControllerDlg::OnBnClickedNewMesh)
    ON_BN_CLICKED(IDC_NEW_MESH_SAVE, &CMeshControllerDlg::OnBnClickedNewMeshSave)
    ON_BN_CLICKED(IDC_SECURE_BEACON, &CMeshControllerDlg::OnBnClickedSecureBeacon)
    ON_BN_CLICKED(IDC_PB_ADV, &CMeshControllerDlg::OnBnClickedPbAdv)
    ON_CBN_SELCHANGE(IDC_OP_CODE, &CMeshControllerDlg::OnCbnSelchangeOpCode)
    ON_BN_CLICKED(IDC_ADD_FLT, &CMeshControllerDlg::OnBnClickedAddFlt)
    ON_BN_CLICKED(IDC_DEL_FLT, &CMeshControllerDlg::OnBnClickedDelFlt)
    ON_BN_CLICKED(IDC_SET_WHITE_FLT, &CMeshControllerDlg::OnBnClickedSetWhiteFlt)
    ON_BN_CLICKED(IDC_SET_BLACK_FLT, &CMeshControllerDlg::OnBnClickedSetBlackFlt)
    ON_BN_CLICKED(IDC_CONFIGURE, &CMeshControllerDlg::OnBnClickedConfigure)
    ON_BN_CLICKED(IDC_CHECK_NODE_IDENTITY, &CMeshControllerDlg::OnBnClickedCheckNodeIdentity)
    ON_BN_CLICKED(IDC_OTA_UPGRADE_START, &CMeshControllerDlg::OnBnClickedOtaUpgradeStart)
    ON_MESSAGE(WM_PROGRESS, OnProgress)
    ON_BN_CLICKED(IDC_BATTERY_CLIENT, &CMeshControllerDlg::OnBnClickedBatteryClient)
    ON_BN_CLICKED(IDC_BATTERY_SERVER, &CMeshControllerDlg::OnBnClickedBatteryServer)
    ON_BN_CLICKED(IDC_LOCATION_CLIENT, &CMeshControllerDlg::OnBnClickedLocationClient)
    ON_BN_CLICKED(IDC_ONOFF_CLIENT, &CMeshControllerDlg::OnBnClickedOnoffClient)
    ON_BN_CLICKED(IDC_ONOFF_SERVER, &CMeshControllerDlg::OnBnClickedOnoffServer)
    ON_BN_CLICKED(IDC_LEVEL_CLIENT, &CMeshControllerDlg::OnBnClickedLevelClient)
    ON_BN_CLICKED(IDC_LEVEL_SERVER, &CMeshControllerDlg::OnBnClickedLevelServer)
    ON_BN_CLICKED(IDC_TRANSITION_TIME_CLIENT, &CMeshControllerDlg::OnBnClickedTransitionTimeClient)
    ON_BN_CLICKED(IDC_TRANSITION_TIME_SERVER, &CMeshControllerDlg::OnBnClickedTransitionTimeServer)
    ON_BN_CLICKED(IDC_LOCATION_SETUP_SERVER, &CMeshControllerDlg::OnBnClickedLocationServer)
    ON_BN_CLICKED(IDC_POWER_ONOFF_SETUP_SERVER, &CMeshControllerDlg::OnBnClickedPowerOnoffSetupServer)
    ON_BN_CLICKED(IDC_POWER_ONOFF_CLIENT, &CMeshControllerDlg::OnBnClickedPowerOnoffClient)
    ON_BN_CLICKED(IDC_POWER_LEVEL_CLIENT, &CMeshControllerDlg::OnBnClickedPowerLevelClient)
    ON_BN_CLICKED(IDC_POWER_LEVEL_SETUP_SERVER, &CMeshControllerDlg::OnBnClickedPowerLevelSetupServer)
    ON_BN_CLICKED(IDC_PROPERTY_SERVER, &CMeshControllerDlg::OnBnClickedPropertyServer)
    ON_BN_CLICKED(IDC_PROPERTY_CLIENT, &CMeshControllerDlg::OnBnClickedPropertyClient)
    ON_BN_CLICKED(IDC_LIGHT_LIGHTNESS_SETUP_SERVER, &CMeshControllerDlg::OnBnClickedLightLightnessSetupServer)
    ON_BN_CLICKED(IDC_LIGHT_LIGHTNESS_CLIENT, &CMeshControllerDlg::OnBnClickedLightLightnessClient)
    ON_BN_CLICKED(IDC_SENSOR_CLIENT, &CMeshControllerDlg::OnBnClickedSensorClient)
    ON_BN_CLICKED(IDC_SENSOR_SERVER, &CMeshControllerDlg::OnBnClickedSensorServer)
    ON_BN_CLICKED(IDC_LIGHT_CTL_CLIENT, &CMeshControllerDlg::OnBnClickedLightCtlClient)
    ON_BN_CLICKED(IDC_LIGHT_CTL_SERVER, &CMeshControllerDlg::OnBnClickedLightCtlServer)
    ON_BN_CLICKED(IDC_LIGHT_HSL_CLIENT, &CMeshControllerDlg::OnBnClickedLightHslClient)
    ON_BN_CLICKED(IDC_LIGHT_HSL_SERVER, &CMeshControllerDlg::OnBnClickedLightHslServer)
    ON_BN_CLICKED(IDC_LIGHT_XYL_CLIENT, &CMeshControllerDlg::OnBnClickedLightXylClient)
    ON_BN_CLICKED(IDC_LIGHT_XYL_SERVER, &CMeshControllerDlg::OnBnClickedLightXylServer)
    ON_BN_CLICKED(IDC_LIGHT_LC_CLIENT, &CMeshControllerDlg::OnBnClickedLightLcClient)
    ON_BN_CLICKED(IDC_LIGHT_LC_SERVER, &CMeshControllerDlg::OnBnClickedLightLcServer)
    ON_BN_CLICKED(IDC_RESET_NODE, &CMeshControllerDlg::OnBnClickedResetNode)
    ON_BN_CLICKED(IDC_SELECT_DEVICE, &CMeshControllerDlg::OnBnClickedSelectDevice)
END_MESSAGE_MAP()

static UINT32 read_reg(UINT8 *mesh_id, const char* name, UINT8* value, UINT16 len)
{
    HKEY hKey;
    char szKey[MAX_PATH];

    strcpy_s(szKey, szRegKey);
    for (int i = 0; i < 16; i++)
        sprintf_s(&szKey[strlen(szKey)], sizeof(szKey)-strlen(szKey), "%02x", mesh_id[i]);

    if (RegCreateKeyExA(HKEY_CURRENT_USER, szKey, 0, NULL, 0, KEY_QUERY_VALUE, NULL, &hKey, NULL) == ERROR_SUCCESS)
    {
        DWORD dwLen = len;
        RegQueryValueExA(hKey, name, 0, NULL, value, &dwLen);
        RegCloseKey(hKey);
        return dwLen;
    }
    return 0;
}

static UINT32 write_reg(UINT8 *mesh_id, const char* name, UINT8* value, UINT16 len)
{
    HKEY hKey;
    char szKey[MAX_PATH];
    if (0 == memcmp(mesh_id, key_0, MESH_KEY_LEN))
        return 0;

    strcpy_s(szKey, szRegKey);
    for (int i = 0; i < 16; i++)
        sprintf_s(&szKey[strlen(szKey)], sizeof(szKey)-strlen(szKey), "%02x", mesh_id[i]);

    if (RegCreateKeyExA(HKEY_CURRENT_USER, szKey, 0, NULL, 0, KEY_SET_VALUE, NULL, &hKey, NULL) == ERROR_SUCCESS)
    {
        RegSetValueExA(hKey, name, 0, REG_BINARY, value, len);
        RegCloseKey(hKey);
        return len;
    }
    return 0;
}


static UINT16 OPsWithNoParams[] = { WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET, WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET,
                                    WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET, WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET,
                                    WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET, WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET };

static UINT16 OPsWithState[] = { WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET, WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET,
                                 WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET, WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET,
                                 WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET };


#define CY_MODEL_ID_TEST_0001       0x0001
#define MESH_PID                    0x0001
#define MESH_VID                    0x0001
#define MESH_CRPL                   0x0008

wiced_bt_mesh_core_config_model_t mesh_elem1_models[] = {
    { 0, WICED_BT_MESH_CORE_MODEL_ID_CONFIG_CLNT },
    { 0, WICED_BT_MESH_CORE_MODEL_ID_HEALTH_CLNT },
    { 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT }
};

wiced_bt_mesh_core_config_element_t mesh_elements[] = {
    {
        MESH_ELEM_LOC_FRONT,                        // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        MESH_DEFAULT_TRANSITION_TIME_IN_MS,         // Default transition time for models of the element in milliseconds
        WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,    // Default element behavior on power up
        0,                                          // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        1,                                          // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        0xffff,                                     // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        0,                                          // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        0,                                          // Number of properties in the array properties
        NULL,                                       // Array of properties in the element.
        0,                                          // Number of sensors in the array sensors
        NULL,                                       // Array of sensors in the element.
        (UINT8)(sizeof(mesh_elem1_models) / sizeof(mesh_elem1_models[0])),  // Number of models in the array models
        mesh_elem1_models                                                   // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    }
};

wiced_bt_mesh_core_config_t mesh_config = {
    MESH_COMPANY_ID_CYPRESS,
    MESH_PID,
    MESH_VID,
    MESH_CRPL,
    0,
    0,
    NULL,
    { 0, 0 },                                           // Empty Configuration of the Friend Feature
    { 0, 0, 0, 0, 0 },                                  // Empty Configuration of the Low Power Feature
    (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),
    mesh_elements
};

/**
* \brief Received message callback.
 * \details Application implements that function to handle received messages.
 * If opcode doesn't correspond to model id then it returns WICED_FALSE. Otherwise it calls related Model's function and returns WICED_TRUE.
 * 0xffff value of the comany_id means special case when message came from Network or Transport layer(see @ref WICED_BT_MESH_CORE_CMD_SPECIAL "Special Commands of the Proxy Filter, Heartbeat and other")
*
 * @param[in]   p_event       :Parameters related to received messag. It should be relesed by call to wiced_bt_mesh_release_event() or it can be used to send reply by call to wiced_bt_mesh_core_send()
* @param[in]   params          :Application Parameters - extracted from the Access Payload
* @param[in]   params_len      :Length of the Application Parameters
*
* @return      WICED_TRUE if opcode corresponds to the model_id. Otherwise returns WICED_FALSE.
*/
static wiced_bool_t wiced_bt_mesh_core_received_msg_callback(wiced_bt_mesh_event_t *p_event, const uint8_t *params, uint16_t params_len)
{
    m_hDlg->TraceMsg(TRUE, p_event->src, p_event->ttl, p_event->company_id, p_event->opcode, (uint8_t*)params, params_len);
    return WICED_TRUE;
}

// CMeshControllerDlg message handlers

extern "C" void proxy_gatt_send_cb(const uint8_t* data, uint32_t data_len)
{
    BTW_GATT_VALUE gatt_value;
    gatt_value.len = data_len;
    memcpy(gatt_value.value, data, data_len);
    if (!m_hDlg->m_btInterface->WriteCharacteristic(&guidSvcMeshProxy, &guidCharProxyDataIn, TRUE, &gatt_value))
    {
        ods("proxy_gatt_send_cb: WriteCharacteristic failed.");
    }
}

BOOL CMeshControllerDlg::OnInitDialog()
{
    wiced_bt_mesh_core_set_gatt_mtu(MESH_MAX_GATT_NOTIFICATION_LEN);

    // initialize core
    if (WICED_BT_SUCCESS != wiced_bt_mesh_core_init(&mesh_config, wiced_bt_mesh_core_received_msg_callback, NULL, proxy_gatt_send_cb, NULL, NULL, NULL, NULL))
    {
        MessageBox(L"Failed to initialize Mesh Core");
        return FALSE;
    }

    BOOL bConnected = TRUE;  // assume that device is connected which should generally be the case for automation sensor

	CDialogEx::OnInitDialog();

    m_hDlg = this;

    srand((unsigned)time(NULL));

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    m_trace = (CListBox *)GetDlgItem(IDC_TRACE);

    HKEY hKey, hKey1;
    char szKey[MAX_PATH];

    strcpy_s(szKey, szRegKey);

    DWORD dwIndex = 0;
    if (RegCreateKeyExA(HKEY_CURRENT_USER, szKey, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS)
    {
        char szName[MAX_PATH];
        while (RegEnumKeyA(hKey, dwIndex++, szName, MAX_PATH) == ERROR_SUCCESS)
        {
            if (RegOpenKeyExA(hKey, szName, 0, KEY_QUERY_VALUE, &hKey1) == ERROR_SUCCESS)
            {
                UINT8 buf[16];
                UINT8 val[16];

                // initiliz node info from the registry
                GetHexValue(szName, buf, 16);
                mesh_controller_create_node(buf);

                // Show remembered new net key
                read_reg(buf, "NewNetKey", val, sizeof(val));
                SetHexValue(IDC_NEW_KEY, val, sizeof(val));

                InitControls();

                RegCloseKey(hKey1);
                break;
            }
        }
        RegCloseKey(hKey);
    }


    //SetHexValueInt(IDC_NODE_ID, 2, 2);
    SetDlgItemTextA(m_hWnd, IDC_NODE_ID, "1201");
    //SetHexValueInt(IDC_DST_NODE_ID, 2, 2);
    SetDlgItemTextA(m_hWnd, IDC_DST_NODE_ID, "1201");

    SetHexValueInt(IDC_MODEL_ID, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, 2);
    SetHexValueInt(IDC_SUBSCR_ADDR, 0xc001, 2);

    m_seq = 0x3129ab;
    mesh_set_sequence_number(m_seq);
    SetHexValueInt(IDC_MY_SEQ, m_seq, 3);

    SetHexValueInt(IDC_TTL, 0x04, 1);

    ((CButton *)GetDlgItem(IDC_AKF))->SetCheck(1);

    CComboBox* combo = (CComboBox*)GetDlgItem(IDC_OP_CODE);
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET));
    
    for (DWORD i = 0; i < sizeof(OPsWithNoParams) / sizeof(OPsWithNoParams[0]); i++)
    {
        combo->AddString(op2str(OPsWithNoParams[i]));
    }
    for (DWORD i = 0; i < sizeof(OPsWithState) / sizeof(OPsWithState[0]); i++)
    {
        combo->AddString(op2str(OPsWithState[i]));
    }
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NETKEY_ADD));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NETKEY_DELETE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_APPKEY_ADD));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_APPKEY_DELETE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_APPKEY_GET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_NODE_RESET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET));
    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED));

    combo->AddString(op2str(WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET));

    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_GET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_SET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED));

    //opcodes: def tran time model
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_GET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET_UNACKED));
/*//???
    //opcodes: level model
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_GET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_SET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_SET_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET_UNACKED));
    combo->AddString(op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_STATUS));
*///???

    combo->SetCurSel(0);
    OnCbnSelchangeOpCode();

	if (!m_btInterface->Init())
		return TRUE;

    // on Win7 we will receive notification when device is connected and will intialize dialog there
    if (!m_bWin8 && !m_bWin10)
        return TRUE;

    if (m_bWin10)
    {
        CBtWin10Interface *pWin10BtInterface = dynamic_cast<CBtWin10Interface *>(m_btInterface);

        // Assume that we are connected.  Failed attempt to read battery will change that to FALSE.
        pWin10BtInterface->m_bConnected = TRUE;

        if (pWin10BtInterface->m_bConnected)
        {
            DeviceConnected();
        }

        pWin10BtInterface->RegisterNotification(&guidSvcMeshProvisioning, &guidCharProvisioningDataOut);
        pWin10BtInterface->RegisterNotification(&guidSvcMeshProxy, &guidCharProxyDataOut);
        pWin10BtInterface->RegisterNotification(&guidSvcMeshCommand, &guidCharCommandData);
        pWin10BtInterface->RegisterNotification(&guidSvcWSUpgrade, &guidCharWSUpgradeControlPoint);
    }
    else if (m_bWin8)
    {
        CBtWin8Interface *pWin8BtInterface = dynamic_cast<CBtWin8Interface *>(m_btInterface);

        // Assume that we are connected.  Failed attempt to read battery will change that to FALSE.
        pWin8BtInterface->m_bConnected = TRUE;

        if (pWin8BtInterface->m_bConnected)
        {
            DeviceConnected();
        }

        pWin8BtInterface->RegisterNotification(&guidSvcMeshProvisioning, &guidCharProvisioningDataOut);
        pWin8BtInterface->RegisterNotification(&guidSvcMeshProxy, &guidCharProxyDataOut);
        pWin8BtInterface->RegisterNotification(&guidSvcMeshCommand, &guidCharCommandData);
        pWin8BtInterface->RegisterNotification(&guidSvcWSUpgrade, &guidCharWSUpgradeControlPoint);

    }


    SetTimer(1, 100, 0);

    SetDlgItemText(IDC_OTA_UPGRADE_START, L"OTA Start");

    return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMeshControllerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMeshControllerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMeshControllerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMeshControllerDlg::PostNcDestroy()
{
    CDialogEx::PostNcDestroy();
}

void CMeshControllerDlg::DeviceConnected()
{
    BTW_GATT_VALUE gatt_value;

    // Register for Proxy notifications
    gatt_value.len = 2;
    gatt_value.value[0] = 1;
    gatt_value.value[1] = 0;

	// Provisioning service
	m_btInterface->SetDescriptorValue(&guidSvcMeshProvisioning, &guidCharProvisioningDataOut, BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG, &gatt_value);

	// Proxy service
	m_btInterface->SetDescriptorValue(&guidSvcMeshProxy, &guidCharProxyDataOut, BTW_GATT_UUID_DESCRIPTOR_CLIENT_CONFIG, &gatt_value);
}

LRESULT CMeshControllerDlg::OnConnected(WPARAM bConnected, LPARAM lparam)
{
    if (!bConnected)
        return S_OK;

    BYTE mBatteryLevel = 0;

    if (!m_bWin8 && !m_bWin10)
    {
        DeviceConnected();
    }
    return S_OK;
}

LRESULT CMeshControllerDlg::OnProxyDataIn(WPARAM Instance, LPARAM lparam)
{
    BTW_GATT_VALUE *pValue = (BTW_GATT_VALUE *)lparam;
    wiced_bt_mesh_core_proxy_packet(pValue->value, (BYTE)pValue->len);
    free(pValue);
    return S_OK;
}

BYTE ProcNibble(char n)
{
    if ((n >= '0') && (n <= '9'))
    {
        n -= '0';
    }
    else if ((n >= 'A') && (n <= 'F'))
    {
        n = ((n - 'A') + 10);
    }
    else if ((n >= 'a') && (n <= 'f'))
    {
        n = ((n - 'a') + 10);
    }
    else
    {
        n = (char)0xff;
    }
    return (n);
}

void CMeshControllerDlg::SetHexValueInt(DWORD id, DWORD val, DWORD val_len)
{
    BYTE val_hex[4];
    if (val_len > 0)
    {
        if (val_len > 4)
            val_len = 5;
        for (int i = val_len - 1; i >= 0; i--)
        {
            val_hex[i] = (BYTE)(val);
            val = val >> 8;
        }
    }
    SetHexValue(id, val_hex, val_len);
}

void CMeshControllerDlg::SetHexValue(DWORD id, LPBYTE val, DWORD val_len)
{
    WCHAR str[1024];
    for (DWORD i = 0; i < val_len; i++)
        wsprintf(&str[3 * i], L"%02X ", val[i]);
    SetDlgItemText(id, str);
}

DWORD CMeshControllerDlg::GetHexValue(char *szbuf, LPBYTE buf, DWORD buf_size)
{
    BYTE *pbuf = buf;
    DWORD res = 0;

    // remove leading white space
    while (*szbuf != 0 && (BYTE)*szbuf <= 0x20) szbuf++;
    DWORD len = (DWORD)strlen(szbuf);
    // remove terminating white space
    while (len > 0 && (BYTE)szbuf[len - 1] <= 0x20) len--;

    memset(buf, 0, buf_size);

    if (len == 1)
    {
        szbuf[2] = 0;
        szbuf[1] = szbuf[0];
        szbuf[0] = '0';
    }
    else if (len == 3)
    {
        szbuf[4] = 0;
        szbuf[3] = szbuf[2];
        szbuf[2] = szbuf[1];
        szbuf[1] = szbuf[0];
        szbuf[0] = '0';
    }
    for (DWORD i = 0; (i < len) && (res < buf_size); i++)
    {
        if (isxdigit(szbuf[i]) && isxdigit(szbuf[i + 1]))
        {
            *pbuf++ = (ProcNibble((char)szbuf[i]) << 4) + ProcNibble((char)szbuf[i + 1]);
            res++;
            i++;
        }
    }
    return res;
}

DWORD CMeshControllerDlg::GetHexValueInt(DWORD id)
{
    DWORD ret = 0;
    BYTE buf[32];
    DWORD len = GetHexValue(id, buf, sizeof(buf));
    for (DWORD i = 0; i<len; i++)
    {
        ret = (ret << 8) + buf[i];
    }
    return ret;
}

DWORD CMeshControllerDlg::GetHexValue(DWORD id, LPBYTE buf, DWORD buf_size)
{
    WCHAR wszbuf[100];
    char szbuf[100];

    GetDlgItemText(id, wszbuf, sizeof(wszbuf) / sizeof(wszbuf[0]));
    WideCharToMultiByte(CP_ACP, 0, wszbuf, -1, szbuf, sizeof(szbuf), NULL, NULL);
    return GetHexValue(szbuf, buf, buf_size);
}

void CMeshControllerDlg::GetDlgItemTextUTF8(int id, char* buf, DWORD buf_len)
{
    CString str;
    GetDlgItemText(id, str);
    WideCharToMultiByte(CP_UTF8, 0, str, -1, buf, buf_len, NULL, NULL);
}

void CMeshControllerDlg::OnBnClickedNewMesh()
{
    int i;
    BYTE data[32];

    srand((unsigned)time(NULL));

    for (i = 0; i < 4; i++)
        rand_s((UINT *)&data[i * 4]);
    SetHexValue(IDC_APP_KEY, data, MESH_KEY_LEN);
    SetHexValue(IDC_NEW_KEY, data, MESH_KEY_LEN);

    for (i = 0; i < 4; i++)
        rand_s((UINT *)&data[i * 4]);
    SetHexValue(IDC_NET_KEY, data, MESH_KEY_LEN);

    memset(data, 0, sizeof(data));
    SetHexValue(IDC_NET_IV_INDEX, data, MESH_IV_INDEX_LEN);

    SetHexValueInt(IDC_MY_NODE_ID, 1, 2);

    OnBnClickedNewMeshSave();
}

void CMeshControllerDlg::OnBnClickedNewMeshSave()
{
    DWORD out = GetHexValueInt(IDC_MY_NODE_ID);
    UINT8 buf[32];
    UINT8 val[16];

    if (out == 0)
    {
        MessageBox(L"Enter a valid Node ID");
        return;
    }

    DWORD dw = RegDeleteTreeA(HKEY_CURRENT_USER, szRegKey);
    if (dw != ERROR_SUCCESS)
    {
        ods("Can't delete HKCU\\Software\\Cypress\\Mesh");
    }

    //reset node
    mesh_controller_create_node(NULL);

    GetHexValue(IDC_DEV_KEY, buf, 16);
    mesh_set_dev_key(buf);

    GetHexValue(IDC_APP_KEY, buf, 16);
    mesh_set_app_key(0, 0, buf, 0);

    GetHexValue(IDC_NEW_KEY, val, 16);
    write_reg(buf, "NewNetKey", val, 16);

    mesh_set_dev_addr((WORD)out);

    GetHexValue(IDC_NET_KEY, buf, 16);
    mesh_set_net_key(0, buf, 0);

    GetHexValue(IDC_NET_IV_INDEX, buf, MESH_IV_INDEX_LEN);
    mesh_set_iv_index(buf);

    m_seq = GetHexValueInt(IDC_MY_SEQ);
    mesh_set_sequence_number(m_seq);

    InitControls();
}

const WCHAR* op2str(UINT16 op)
{
    switch (op)
    {
    case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET: return L"COMPOS_DATA_GET";
    case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS: return L"COMPOS_DATA_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET: return L"BEACON_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET: return L"BEACON_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS: return L"BEACON_STATUS";
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET: return L"NODE_IDENTITY_GET";
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET: return L"NODE_IDENTITY_SET";
    case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_STATUS: return L"NODE_IDENTITY_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET: return L"DEFAULT_TTL_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET: return L"DEFAULT_TTL_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS: return L"DEFAULT_TTL_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET: return L"GATT_PROXY_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET: return L"GATT_PROXY_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS: return L"GATT_PROXY_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET: return L"GATT_FRIEND_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET: return L"GATT_FRIEND_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS: return L"GATT_FRIEND_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET: return L"NETWORK_TRANSMIT_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET: return L"NETWORK_TRANSMIT_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS: return L"NETWORK_TRANSMIT_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET: return L"RELAY_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET: return L"RELAY_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_STATUS: return L"RELAY_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET: return L"MODEL_PUB_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET: return L"MODEL_PUB_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET: return L"MODEL_PUB_VIRT_ADDR_SET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_STATUS: return L"MODEL_PUB_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD: return L"MODEL_SUBS_ADD";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD: return L"MODEL_SUBS_VIRT_ADDR_ADD";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE: return L"MODEL_SUBS_DELETE";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE: return L"MODEL_SUBS_VIRT_ADDR_DELETE";
    case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET: return L"SIG_MODEL_SUBS_GET";

    case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET: return L"VENDOR_MODEL_SUBS_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE: return L"MODEL_SUBS_OVERWRITE";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE: return L"MODEL_SUBS_VIRT_ADDR_OVERWRITE";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS: return L"MODEL_SUBS_STATUS";
    case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_LIST: return L"SIG_MODEL_SUBS_LIST";
    case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_LIST: return L"VENDOR_MODEL_SUBS_LIST";
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL: return L"MODEL_SUBS_DELETE_ALL";

    case WICED_BT_MESH_CORE_CMD_NODE_RESET: return L"NODE_RESET";
    case WICED_BT_MESH_CORE_CMD_NODE_RESET_STATUS: return L"NODE_RESET_STATUS";
    case WICED_BT_MESH_CORE_CMD_APPKEY_ADD: return L"APPKEY_ADD";
    case WICED_BT_MESH_CORE_CMD_APPKEY_DELETE: return L"APPKEY_DELETE";
    case WICED_BT_MESH_CORE_CMD_APPKEY_STATUS: return L"APPKEY_STATUS";
    case WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE: return L"APPKEY_UPDATE";
    case WICED_BT_MESH_CORE_CMD_APPKEY_GET: return L"APPKEY_GET";
    case WICED_BT_MESH_CORE_CMD_APPKEY_LIST: return L"APPKEY_LIST";
    case WICED_BT_MESH_CORE_CMD_NETKEY_ADD: return L"NETKEY_ADD";
    case WICED_BT_MESH_CORE_CMD_NETKEY_DELETE: return L"NETKEY_DELETE";
    case WICED_BT_MESH_CORE_CMD_NETKEY_STATUS: return L"NETKEY_STATUS";
    case WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE: return L"NETKEY_UPDATE";
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND: return L"MODEL_APP_BIND";
    case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET: return L"SIG_MODEL_APP_GET";
    case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET: return L"VENDOR_MODEL_APP_GET";
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_STATUS: return L"MODEL_APP_STATUS";
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND: return L"MODEL_APP_UNBIND";
    case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_LIST: return L"SIG_MODEL_APP_LIST";
    case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_LIST: return L"VENDOR_MODEL_APP_LIST";
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET: return L"KEY_REFRESH_PHASE_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET: return L"KEY_REFRESH_PHASE_SET";
    case WICED_BT_MESH_OPCODE_GEN_ONOFF_GET: return L"OPCODE_GEN_ONOFF_GET";
    case WICED_BT_MESH_OPCODE_GEN_ONOFF_SET: return L"OPCODE_GEN_ONOFF_SET";
    case WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED: return L"OPCODE_GEN_ONOFF_SET_UNACKED";
    case WICED_BT_MESH_OPCODE_GEN_ONOFF_STATUS: return L"OPCODE_GEN_ONOFF_STATUS";

    case WICED_BT_MESH_CORE_CMD_ATTENTION_GET: return L"ATTENTION_GET";
    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET: return L"ATTENTION_SET";
    case WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED: return L"ATTENTION_SET_UNACKED";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR: return L"HEALTH_FAULT_CLEAR";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED: return L"HEALTH_FAULT_CLEAR_UNACKED";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET: return L"HEALTH_FAULT_GET";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST: return L"HEALTH_FAULT_TEST";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED: return L"HEALTH_FAULT_TEST_UNACKED";
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET: return L"HEALTH_PERIOD_GET";
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET: return L"HEALTH_PERIOD_SET";
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED: return L"HEALTH_PERIOD_SET_UNACKED";
    case WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS: return L"ATTENTION_STATUS";
    case WICED_BT_MESH_CORE_CMD_HEALTH_CURRENT_STATUS: return L"HEALTH_CURRENT_STATUS";
    case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_STATUS: return L"HEALTH_FAULT_STATUS";
    case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS: return L"HEALTH_PERIOD_STATUS";

    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET: return L"HEARTBEAT_PUBLICATION_GET";
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET: return L"HEARTBEAT_PUBLICATION_SET";
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_STATUS: return L"HEARTBEAT_PUBLICATION_STATUS";
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET: return L"HEARTBEAT_SUBSCRIPTION_GET";
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET: return L"HEARTBEAT_SUBSCRIPTION_SET";
    case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_STATUS: return L"HEARTBEAT_SUBSCRIPTION_STATUS";

    case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET: return L"LPN_POLL_TIMEOUT_GET";
    case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_STATUS: return L"LPN_POLL_TIMEOUT_STATUS";


    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_GET: return L"OPCODE_GEN_DEFTRANSTIME_GET";
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET: return L"OPCODE_GEN_DEFTRANSTIME_SET";
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET_UNACKED: return L"OPCODE_GEN_DEFTRANSTIME_SET_UNACKED";
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_STATUS: return L"OPCODE_GEN_DEFTRANSTIME_STATUS";
/*//???
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_GET: return L"OPCODE_GEN_LEVEL_GET";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_SET: return L"OPCODE_GEN_LEVEL_SET";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_SET_UNACKED: return L"OPCODE_GEN_LEVEL_SET_UNACKED";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET: return L"OPCODE_GEN_LEVEL_DELTA_SET";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET_UNACKED: return L"OPCODE_GEN_LEVEL_DELTA_SET_UNACKED";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET: return L"OPCODE_GEN_LEVEL_MOVE_SET";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET_UNACKED: return L"OPCODE_GEN_LEVEL_MOVE_SET_UNACKED";
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_STATUS: return L"OPCODE_GEN_LEVEL_STATUS";
*///???

    default:
    {
        static WCHAR buf[16];
        wsprintf(buf, L"0x%x");
        return buf;
    }
        break;
    }
}

const WCHAR* proxy_op2str(UINT16 op)
{
    switch (op)
    {
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_SET_TYPE: return L"PROXY_FLT_SET_TYPE";
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_ADD_ADDR: return L"PROXY_FLT_ADD_ADDR";
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_DEL_ADDR: return L"PROXY_FLT_DEL_ADDR";
    case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_STATUS: return L"PROXY_FLT_STATUS";
    case WICED_BT_MESH_CORE_CMD_SPECIAL_HEARTBEAT: return L"HEARTBEAT";
    default:
    {
        static WCHAR buf[16];
        wsprintf(buf, L"PROXY_CFG_0x%x");
        return buf;
    }
    break;
    }
}

void CMeshControllerDlg::TraceMsg(BOOL bRcvd, UINT16 node, UINT8 ttl, UINT16 company_id, UINT16 opcode, UINT8 *payload, UINT16 payload_len)
{
    WCHAR buf[1000];
    const WCHAR *op_str;
    UINT32 num;
    BOOL    unknown_opcode = FALSE;
    BOOL    invalid_data = TRUE;

    wsprintf(buf, L"%s:%x TTL:%x ", bRcvd ? L"received from" : L"sent to", node, ttl);

    // Handle special case of the Proxy Configuration Messages
    if (company_id == 0xffff)
    {
        op_str = proxy_op2str(opcode);
        wsprintf(&buf[wcslen(buf)], L"%s ", op_str);
        switch (opcode)
        {
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_SET_TYPE:
            if (payload_len != 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"type:%x", payload[0]);
            break;
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_ADD_ADDR:
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_DEL_ADDR:
            if (payload_len == 0 || (payload_len % 2) != 0)
                break;
            invalid_data = FALSE;
            for (UINT8 i = 0; i < payload_len; i+=2)
            {
                wsprintf(&buf[wcslen(buf)], L"%x, ", BE2TOUINT16(&payload[i]));
            }
            break;
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_STATUS:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"type:%x count:%d", payload[0], BE2TOUINT16(&payload[1]));
            break;
        case WICED_BT_MESH_CORE_CMD_SPECIAL_HEARTBEAT:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"init_ttl:%x features:%x", payload[0], BE2TOUINT16(&payload[1]));
            break;
        default:
            unknown_opcode = TRUE;
            break;
        }
    }
    else if (company_id == 0)
    {
        op_str = op2str(opcode);
        wsprintf(&buf[wcslen(buf)], L"%s ", op_str);
        switch (opcode)
        {
        case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_STATUS:
        {
            UINT8 page = *payload++;
            payload_len--;
            tFND_CONFIG_COMPOS_DATA            *p_data = (tFND_CONFIG_COMPOS_DATA*)payload;
            tFND_CONFIG_ELEMENT_COMPOS_DATA    *p_comp = &p_data->elements[0];
            UINT8* p_end;
            if (payload_len < sizeof(tFND_CONFIG_COMPOS_DATA))
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"page:%x cid:%x pid:%x vid:%x crpl:%x features:%x",
                page, LE2TOUINT16(p_data->cid), LE2TOUINT16(p_data->pid), LE2TOUINT16(p_data->vid), LE2TOUINT16(p_data->crpl), LE2TOUINT16(p_data->features));
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            while (true)
            {
                p_end = &p_comp->mids[p_comp->num_m * 2 + p_comp->num_v * 4];
                wcscpy_s(buf, sizeof(buf) / sizeof(buf[0]), L"    ");
                if (p_end > payload + payload_len)
                {
                    wsprintf(&buf[wcslen(buf)], L"%s invalid data", op_str);
                    break;
                }
                wsprintf(&buf[wcslen(buf)], L"loc:%x m:%d v:%d MIDs:",
                    LE2TOUINT16(p_comp->loc), p_comp->num_m, p_comp->num_v);
                for (UINT8 i = 0; i < p_comp->num_m; i++)
                {
                    wsprintf(&buf[wcslen(buf)], L"%x, ", LE2TOUINT16(&p_comp->mids[i * 2]));
                }
                for (UINT8 i = 0; i < p_comp->num_v; i++)
                {
                    wsprintf(&buf[wcslen(buf)], L"%x:%x, ", LE2TOUINT16(&p_comp->mids[p_comp->num_m * 2 + i * 4]),
                        LE2TOUINT16(&p_comp->mids[p_comp->num_m * 2 + i * 4 + 2]));
                }
                if (p_end == payload + payload_len)
                    break;
                m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
                p_comp = (tFND_CONFIG_ELEMENT_COMPOS_DATA*)p_end;
            }
        }
        break;
        case WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET:
            if (payload_len != 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"page:%x", *payload);
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_GET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_GET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_GET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_GET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_GET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_GET:
            if (payload_len != 0)
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_STATUS:
        case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_STATUS:
        case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_STATUS:
        case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_STATUS:
        case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_STATUS:
            if (payload_len != 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x", *payload);
            break;

        case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_STATUS:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x retrans:%x", *payload, payload[1]);
            break;

        case WICED_BT_MESH_CORE_CMD_CONFIG_BEACON_SET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_GATT_PROXY_SET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_NETWORK_TRANSMIT_SET:
        case WICED_BT_MESH_CORE_CMD_CONFIG_FRIEND_SET:
            if (payload_len != 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x", *payload);
            break;

        case WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x retrans:%x", *payload, payload[1]);
            break;

        case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_GET:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"lpn_addr:%x", LE2TOUINT16(payload));
            break;

        case WICED_BT_MESH_CORE_CMD_CONFIG_LPN_POLL_TIMEOUT_STATUS:
            if (payload_len != 5)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"LPN addr:%x PollTimeout", LE2TOUINT16(payload), LE3TOUINT32(payload + 2));
            break;

        case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"net_idx:%x", LE2TOUINT12(payload));
            break;

        case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"net_idx:%x state", LE2TOUINT12(payload), payload[2]);
            break;
        case WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_STATUS:
            if (payload_len != 4)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"status:%x net_idx:%x state:%x", payload[0], LE2TOUINT12(&payload[1]), payload[3]);
            break;

        case WICED_BT_MESH_CORE_CMD_APPKEY_ADD:
        case WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE:
            if (payload_len != 3 + MESH_KEY_LEN)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"net_idx:%x app_idx:%x", LE2TOUINT12(payload), LE2TOUINT12_2(payload + 1));
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            wsprintf(buf, L"    key:%s", data2str(payload + 3, MESH_KEY_LEN));
            break;
        case WICED_BT_MESH_CORE_CMD_APPKEY_DELETE:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"net_idx:%x app_idx:%x", LE2TOUINT12(payload), LE2TOUINT12_2(payload + 1));
            break;
        case WICED_BT_MESH_CORE_CMD_APPKEY_STATUS:
            if (payload_len != 4)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x netkey_idx:%x appkey_idx:%x", *payload, LE2TOUINT12(payload + 1), LE2TOUINT12_2(payload + 2));
            if (*payload == 0)
                SetEvent(m_hCfgEvent);
            break;
        case WICED_BT_MESH_CORE_CMD_APPKEY_GET:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"net_idx:%x", LE2TOUINT16(payload));
            break;
        case WICED_BT_MESH_CORE_CMD_APPKEY_LIST:
            if (payload_len < 1 + 2
                || (((payload_len - 3) % 2) != 0 && ((payload_len - 3) % 3) != 0 && ((payload_len - 3) % 5) != 0))
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x netkey_idx:%x", *payload, LE2TOUINT12(payload + 1));
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            wsprintf(buf, L"    app_indexes:%s", keyidx2str(payload + 3, payload_len - 3));
            break;
        case WICED_BT_MESH_CORE_CMD_NETKEY_ADD:
        case WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE:
            if (payload_len != 2 + MESH_KEY_LEN)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"idx:%x", LE2TOUINT16(payload));
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            wsprintf(buf, L"    key:%s", data2str(payload + 2, MESH_KEY_LEN));
            break;
        case WICED_BT_MESH_CORE_CMD_NETKEY_DELETE:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"idx:%x", LE2TOUINT16(payload));
            break;
        case WICED_BT_MESH_CORE_CMD_NETKEY_STATUS:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x netkey_idx:%x", *payload, LE2TOUINT16(payload + 1));
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET:
            if (payload_len == 4)
                wsprintf(&buf[wcslen(buf)], L"elem:%x model_id:%x", LE2TOUINT16(payload), LE2TOUINT16(payload + 2));
            else if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x company_id:%x model_id:%x", LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET:
            if (payload_len == 11)
                wsprintf(&buf[wcslen(buf)], L"elem:%x pub_addr:%x app_idx:%x ttl:%x period:%x pub_retr:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4), payload[6], payload[7], payload[8], LE2TOUINT16(payload + 9));
            else if (payload_len == 13)
                wsprintf(&buf[wcslen(buf)], L"elem:%x pub_addr:%x app_idx:%x ttl:%x period:%x pub_retr:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4), payload[6], payload[7], payload[8], LE2TOUINT16(payload + 9), LE2TOUINT16(payload + 11));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET:
            if (payload_len == 25)
                wsprintf(&buf[wcslen(buf)], L"elem:%x app_idx:%x ttl:%x period:%x pub_retr:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 18), payload[20], payload[21], payload[22], LE2TOUINT16(payload + 23));
            else if (payload_len == 27)
                wsprintf(&buf[wcslen(buf)], L"elem:%x app_idx:%x ttl:%x period:%x pub_retr:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 18), payload[20], payload[21], payload[22], LE2TOUINT16(payload + 23), LE2TOUINT16(payload + 25));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_STATUS:
            if (payload_len == 12)
            {
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x pub_addr:%x app_idx,cred,rfu:%x ttl:%x period:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5), payload[7], payload[8]);
                m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
                wcscpy_s(buf, sizeof(buf) / sizeof(buf[0]), L"    ");
                wsprintf(&buf[wcslen(buf)], L"retrans:%x model:%x", payload[9], LE2TOUINT16(payload + 10));
            }
            else if (payload_len == 14)
            {
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x pub_addr:%x app_idx,cred,rfu:%x ttl:%x period:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5), payload[7], payload[8]);
                m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
                wcscpy_s(buf, sizeof(buf) / sizeof(buf[0]), L"    ");
                wsprintf(&buf[wcslen(buf)], L"retrans:%x comp:%x model:%x", payload[9], LE2TOUINT16(payload + 10), LE2TOUINT16(payload + 12));
            }
            else
                break;
            invalid_data = FALSE;
            if (payload[0] == 0)
                SetEvent(m_hCfgEvent);
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL:
            if (payload_len == 4)
                wsprintf(&buf[wcslen(buf)], L"elem:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2));
            else if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD:
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE:
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE:
            if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x sub_addr:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else if (payload_len == 8)
                wsprintf(&buf[wcslen(buf)], L"elem:%x sub_addr:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4), LE2TOUINT16(payload + 6));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD:
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE:
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE:
            if (payload_len == 20)
                wsprintf(&buf[wcslen(buf)], L"elem:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 18));
            else if (payload_len == 22)
                wsprintf(&buf[wcslen(buf)], L"elem:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 18), LE2TOUINT16(payload + 20));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS:
            if (payload_len == 7)
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x sub_addr:%x model_id:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5));
            else if (payload_len == 9)
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x sub_addr:%x comp_id:%x model_id:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5), LE2TOUINT16(payload + 7));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET:
            if (payload_len == 4)
                wsprintf(&buf[wcslen(buf)], L"elem:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET:
            if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_LIST:
            if (payload_len < 5 || ((payload_len - 5) % 2) == 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x model_id:%x subs:",
                payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3));
            num = (payload_len - 5) / 2;
            for (UINT8 i = 0; i < num; i++)
            {
                wsprintf(&buf[wcslen(buf)], L"%x, ", LE2TOUINT16(payload + 5 + i * 2));
            }
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_LIST:
            if (payload_len < 7 || ((payload_len - 7) % 2) == 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x comp_id:%x model_id:%x subs:",
                payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5));
            num = (payload_len - 7) / 2;
            for (UINT8 i = 0; i < num; i++)
            {
                wsprintf(&buf[wcslen(buf)], L"%x:%x, ", LE2TOUINT16(payload + 7 + i * 4), LE2TOUINT16(payload + 7 + i * 4 + 2));
            }
            break;
        case WICED_BT_MESH_CORE_CMD_NODE_RESET:
        case WICED_BT_MESH_CORE_CMD_NODE_RESET_STATUS:
            if (payload_len != 0)
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND:
        case WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND:
            if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x app_idx:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else if (payload_len == 8)
                wsprintf(&buf[wcslen(buf)], L"elem:%x app_idx:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4), LE2TOUINT16(payload + 6));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_MODEL_APP_STATUS:
            if (payload_len == 7)
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x app_idx:%x model_id:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5));
            else if (payload_len == 9)
                wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x app_idx:%x comp_id:%x model_id:%x",
                    payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5), LE2TOUINT16(payload + 7));
            else
                break;
            invalid_data = FALSE;
            if (payload[0] == 0)
                SetEvent(m_hCfgEvent);
            break;
        case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET:
            if (payload_len == 4)
                wsprintf(&buf[wcslen(buf)], L"elem:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET:
            if (payload_len == 6)
                wsprintf(&buf[wcslen(buf)], L"elem:%x comp_id:%x model_id:%x",
                    LE2TOUINT16(payload), LE2TOUINT16(payload + 2), LE2TOUINT16(payload + 4));
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_LIST:
            if (payload_len < 5 || ((payload_len - 5) % 2) == 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x model_id:%x apps:",
                payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3));
            num = (payload_len - 5) / 2;
            for (UINT8 i = 0; i < num; i++)
            {
                wsprintf(&buf[wcslen(buf)], L"%x, ", LE2TOUINT16(payload + 5 + i * 2));
            }
            break;
        case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_LIST:
            if (payload_len < 7 || ((payload_len - 7) % 2) == 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"st:%x elem:%x comp_id:%x model_id:%x apps:",
                payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), LE2TOUINT16(payload + 5));
            num = (payload_len - 7) / 2;
            for (UINT8 i = 0; i < num; i++)
            {
                wsprintf(&buf[wcslen(buf)], L"%x:%x, ", LE2TOUINT16(payload + 7 + i * 4), LE2TOUINT16(payload + 7 + i * 4 + 2));
            }
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_STATUS:
            if (payload_len != 4)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"status:%x idx:%x phase:%x", *payload, LE2TOUINT16(payload + 1), payload[3]);
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET:
            if (payload_len != 2)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"idx:%x", LE2TOUINT16(payload));
            break;
        case WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"idx:%x phase:%x", LE2TOUINT16(payload), payload[2]);
            break;

        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET:
        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET:
            if (payload_len != 0)
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET:
            if (payload_len != 9)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"dst:%x count:%x period:%x ttl:%x features:%x net_key_idx", LE2TOUINT16(payload), payload[2], payload[3], payload[4], LE2TOUINT16(payload + 5), LE2TOUINT16(payload + 7));
            break;
        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_STATUS:
            if (payload_len != 10)
                break;
            invalid_data = FALSE;
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            wsprintf(buf, L"    status:%x dst:%x count:%x period:%x ttl:%x features:%x net_key_idx", payload[0], LE2TOUINT16(payload + 1), payload[3], payload[4], payload[5], LE2TOUINT16(payload + 6), LE2TOUINT16(payload + 8));
            break;
        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET:
            if (payload_len != 5)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"src:%x dst:%x period:%x", LE2TOUINT16(payload), LE2TOUINT16(payload +2), payload[4]);
            break;
        case WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_STATUS:
            if (payload_len != 9)
                break;
            invalid_data = FALSE;
            m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
            wsprintf(buf, L"    status:%x src:%x dst:%x period:%x count:%x min_hops:%x max_hops:%x", payload[0], LE2TOUINT16(payload + 1), LE2TOUINT16(payload + 3), payload[5], payload[6], payload[7], payload[8]);
            break;

        case WICED_BT_MESH_CORE_CMD_ATTENTION_GET:
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR:
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED:
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET:
        case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET:
            if (payload_len != 0)
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_CORE_CMD_ATTENTION_SET:
        case WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED:
        case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET:
        case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED:
        case WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_STATUS:
        case WICED_BT_MESH_CORE_CMD_ATTENTION_STATUS:
            if (payload_len != 1)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"state:%x", *payload);
            break;
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST:
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED:
            if (payload_len != 3)
                break;
            invalid_data = FALSE;
            wsprintf(&buf[wcslen(buf)], L"test:%x company:%x", *payload, LE2TOUINT16(payload + 1));
            break;
        case WICED_BT_MESH_CORE_CMD_HEALTH_CURRENT_STATUS:
        case WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_STATUS:
            if (payload_len < 3)
                break;
            invalid_data = FALSE;
            num = payload_len - 3;
            wsprintf(&buf[wcslen(buf)], L"test:%x company:%x faults:", *payload, LE2TOUINT16(payload + 1));
            for (UINT8 i = 0; i < num; i++)
            {
                wsprintf(&buf[wcslen(buf)], L"%x ", payload[3 + i]);
            }
            break;


        case WICED_BT_MESH_OPCODE_GEN_ONOFF_GET:
            if (payload_len != 0)
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_OPCODE_GEN_ONOFF_SET:
        case WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED:
            if (payload_len == 4)
                wsprintf(&buf[wcslen(buf)], L"OnOff:%x TID:%x trans_time:%x delay:%x", payload[0], payload[1], payload[2], payload[3]);
            else if (payload_len == 2)
                wsprintf(&buf[wcslen(buf)], L"OnOff:%x TID:%x", payload[0], payload[1]);
            else
                break;
            invalid_data = FALSE;
            break;
        case WICED_BT_MESH_OPCODE_GEN_ONOFF_STATUS:
            if (payload_len == 3)
                wsprintf(&buf[wcslen(buf)], L"OnOff:%x TargetOnOff:%x remain_time:%x", payload[0], payload[1], payload[2]);
            else if (payload_len == 1)
                wsprintf(&buf[wcslen(buf)], L"OnOff:%x", payload[0]);
            else
                break;
            invalid_data = FALSE;
            break;
    //opcodes def tran time model
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_GET:
        if (payload_len != 0)
            break;
        invalid_data = FALSE;
        break;

    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET:
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET_UNACKED:
        if (payload_len == 1)
            wsprintf(&buf[wcslen(buf)], L"trans_time:%x ", payload[0]);
        else
            break;
        invalid_data = FALSE;
        break;
    case WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_STATUS:
        if (payload_len == 1)
            wsprintf(&buf[wcslen(buf)], L"trans_time:%x ", payload[0]);
        else
            break;
        invalid_data = FALSE;
        break;
/*//???
    //opcodes level model
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_GET:
        if (payload_len != 0)
            break;

        invalid_data = FALSE;
        break;

    case WICED_BT_MESH_OPCODE_GEN_LEVEL_SET:
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_SET_UNACKED:
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET:
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET_UNACKED:
        if (payload_len == 5)
            wsprintf(&buf[wcslen(buf)], L"Level:%x TID:%x trans_time:%x delay:%x", (payload[0] | payload[1]<<8), payload[2], payload[3], payload[4]);
        else if (payload_len == 3)
            wsprintf(&buf[wcslen(buf)], L"Level:%x TID:%x", (payload[0] | payload[1] << 8), payload[2]);
        else        
            break;
        
        invalid_data = FALSE;
        break;

    case WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET:
    case WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET_UNACKED:
        if (payload_len == 7)
            wsprintf(&buf[wcslen(buf)], L"Delta Level:%x TID:%x trans_time:%x delay:%x", (payload[0] | payload[1] << 8 | payload[2] << 16 | payload[3] <<24), payload[4], payload[5], payload[6]);
        else if (payload_len == 5)
            wsprintf(&buf[wcslen(buf)], L"Delta Level:%x TID:%x", (payload[0] | payload[1] << 8 | payload[2] << 16 | payload[3] << 24), payload[4]);
        else
            break;

        invalid_data = FALSE;
        break;

    case WICED_BT_MESH_OPCODE_GEN_LEVEL_STATUS:
        if (payload_len == 5)
            wsprintf(&buf[wcslen(buf)], L"Level:%x TargetLevel:%x remain_time:%x  %d ms", (payload[0] | payload[1] << 8), (payload[2] | payload[3] << 8), payload[4], get_transtime_in_ms(payload[4]));
        else if (payload_len == 2)
            wsprintf(&buf[wcslen(buf)], L"Level:%x", (payload[0] | payload[1] << 8));
        else
            break;

        invalid_data = FALSE;
        break;
*///???

        default:
            unknown_opcode = TRUE;
            break;
        }
    }
    else
    {
        wsprintf(&buf[wcslen(buf)], L"Company:%x ", company_id);
        unknown_opcode = TRUE;
    }
    if (unknown_opcode)
    {
        wsprintf(&buf[wcslen(buf)], L"Opcode:%04x ", opcode);
    }
    else if (invalid_data)
    {
        wsprintf(&buf[wcslen(buf)], L"invalid data ");
    }
    if (unknown_opcode || invalid_data)
    {
        wsprintf(&buf[wcslen(buf)], L"params_len:%d params:", payload_len);
        for (num = 0; num < payload_len && num < 10; num++)
            wsprintf(&buf[wcslen(buf)], L" %02x", payload[num]);
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
}

static void writeCharProxy(BYTE type, BYTE* val, BYTE len)
{
    split_proxy(type, val, len, MESH_MAX_GATT_NOTIFICATION_LEN-3);
}

void CMeshControllerDlg::OnBnClickedSecureBeacon()
{
    BYTE buf[31];
    GetHexValue(IDC_NET_IV_INDEX, buf, MESH_IV_INDEX_LEN);
    mesh_set_iv_index(buf);
    BOOLEAN kr = ((CButton *)GetDlgItem(IDC_KR))->GetCheck() != 0;
    BOOLEAN iv_updt = ((CButton *)GetDlgItem(IDC_IV_UPDATE))->GetCheck() != 0;

    BYTE len = mesh_create_secure_network_beacon(0, kr, iv_updt, buf);
    writeCharProxy(PROXY_PDU_TYPE_BEACON, buf, len);
}

void CMeshControllerDlg::OnBnClickedSend()
{
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    BOOLEAN         akf         = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic       = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;
    UINT16          dst         = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT8           payload_len = (UINT8)GetHexValue(IDC_PAYLOAD, payload, MESH_MAX_APP_PAYLOAD_LEN);
    
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
}

/**
* Sends new message to the mesh. On the device it sends advert and notification to the connected proxy client.
* On Windows control app it should send GATT write
*
* Parameters:
*   dst:            Destination address.
*   msg:            Message to send. Must contain two bytes before that message for possible header.
*   msg_len:        Length of the message to send
*   from_gatt:          WICED_TRUE - message came from GATT
*                       WICED_FALSE - message came from ADV
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:          Context for complete_callback
*
*   Return:     WICED_TRUE if message sent or placed to the buffer
*/
wiced_bool_t mesh_send_message(uint16_t dst, uint8_t* msg, uint8_t msg_len, wiced_bool_t from_gatt,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    // dst == MESH_NODE_ID_INVALID means proxy configuration
    split_proxy(dst == MESH_NODE_ID_INVALID ? PROXY_PDU_TYPE_CONFIG : PROXY_PDU_TYPE_NETWORK, msg, msg_len, MESH_MAX_GATT_NOTIFICATION_LEN-3);
    return TRUE;
}

extern "C" UINT32 mesh_controller_write_node_info(UINT8 *mesh_id, int inx, const UINT8* node_info, UINT16 len)
{
    char lpszEntry[80];
    sprintf_s(lpszEntry, sizeof(lpszEntry), "NVRAM %d", inx);
    return write_reg(mesh_id, lpszEntry, (UINT8*)node_info, len);
}

extern "C" UINT32 mesh_controller_read_node_info(UINT8 *mesh_id, int inx, const UINT8* node_info, UINT16 len)
{
    char lpszEntry[80];
    sprintf_s(lpszEntry, sizeof(lpszEntry), "NVRAM %d", inx);
    return read_reg(mesh_id, lpszEntry, (UINT8*)node_info, len);
}

extern "C" UINT32 rbg_rand(void)
{
    UINT32 res;
    rand_s(&res);
    return res;
}

//---------------------- PV_ADV -------------------------------------

void CMeshControllerDlg::OnBnClickedPbAdv()
{
    BTW_GATT_VALUE gatt_value;

    // get node address(ID) to use for provisioning
    DWORD out = GetHexValueInt(IDC_NODE_ID);
    if (out == 0)
    {
        MessageBox(L"Enter a valid Node ID");
        return;
    }

#ifndef HARDCODED_UUID
    // start scanning
    gatt_value.len = 1;
    gatt_value.value[0] = MESH_COMMAND_START_SCAN_UNPROVISIONED;
    if (!m_btInterface->WriteCharacteristic(&guidSvcMeshCommand, &guidCharCommandData, TRUE, &gatt_value))
    {
        ods("WriteCharacteristic failed.");
        return;
    }
    // show detected devices and select one of them
    m_pDeviceSelectDialog = new CDeviceSelect(TRUE);
    INT_PTR nResponse = m_pDeviceSelectDialog->DoModal();

    // stop scanning
    gatt_value.len = 1;
    gatt_value.value[0] = MESH_COMMAND_STOP_SCAN_UNPROVISIONED;
    if (!m_btInterface->WriteCharacteristic(&guidSvcMeshCommand, &guidCharCommandData, TRUE, &gatt_value))
    {
        ods("WriteCharacteristic 2 failed.");
    }

#else
    INT_PTR nResponse = IDOK;
#endif

    // on successfully selected device start provisioning
    if (nResponse == IDOK)
    {
        gatt_value.len = 1 + 16 + 2;
        gatt_value.value[0] = MESH_COMMAND_PROVISION;
#ifndef HARDCODED_UUID
        memcpy(&gatt_value.value[1], m_pDeviceSelectDialog->m_uuid, 16);
#else
        //UINT8 uuid[16] = { 0xdd, 0xdd, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        UINT8 uuid[16] = { HARDCODED_UUID };
        memcpy(&gatt_value.value[1], uuid, 16);
#endif
        gatt_value.value[1 + 16] = (BYTE)(out >> 8);
        gatt_value.value[1 + 16 + 1] = (BYTE)out;
        if (!m_btInterface->WriteCharacteristic(&guidSvcMeshCommand, &guidCharCommandData, TRUE, &gatt_value))
        {
            ods("WriteCharacteristic failed.");
        }
    }

    delete m_pDeviceSelectDialog;
    m_pDeviceSelectDialog = NULL;
}

LRESULT CMeshControllerDlg::OnCommandDataIn(WPARAM Instance, LPARAM lparam)
{
    BTW_GATT_VALUE *pValue = (BTW_GATT_VALUE *)lparam;
    if (pValue->len == (1 + MESH_DEVICE_UUID_LEN)
        && pValue->value[0] == MESH_COMMAND_EVENT_UNPROVISIONED_DEV)
    {
        if (m_pDeviceSelectDialog)
            m_pDeviceSelectDialog->PostMessage(WM_USER_DEV_UUID, Instance, lparam);
    }
    else if (pValue->len == 2 && pValue->value[0] == MESH_COMMAND_EVENT_PB_END)
    {
        if (pValue->value[1] == WICED_BT_MESH_PROVISION_RESULT_SUCCESS)
            MessageBox(L"Device Succeeded Provisioning");
        else
            MessageBox(L"Device Failed Provisioning");
    }
    return S_OK;
}

LRESULT CMeshControllerDlg::OnWsUpgradeCtrlPoint(WPARAM Instance, LPARAM lparam)
{
    BTW_GATT_VALUE *pValue = (BTW_GATT_VALUE *)lparam;
    // Decrypt received message
    pValue->len = mesh_ota_fw_upgrade_crypt(WICED_FALSE, pValue->value, pValue->len, pValue->value);
    ods("OnWsUpgradeCtrlPoint: len:%d\n", pValue->len);
    if (pValue->len == 1)
    {
        switch (pValue->value[0])
        {
        case WICED_OTA_UPGRADE_STATUS_OK:
            m_pDownloader->ProcessEvent(WSDownloader::WS_UPGRADE_RESPONSE_OK);
            break;
        case WICED_OTA_UPGRADE_STATUS_CONTINUE:
            m_pDownloader->ProcessEvent(WSDownloader::WS_UPGRADE_CONTINUE);
            break;
        default:
            m_pDownloader->ProcessEvent(WSDownloader::WS_UPGRADE_RESPONSE_FAILED);
            break;
        }
    }
    free(pValue);

    return S_OK;
}


LRESULT CMeshControllerDlg::OnSelectDevice(WPARAM Instance, LPARAM lparam)
{
	if (!m_bWin10)
		return S_OK;

	CDeviceSelectAdv dlgDeviceSelect;
	dlgDeviceSelect.m_bWin8 = FALSE;
	dlgDeviceSelect.m_bth.ullLong = 0;
	INT_PTR nResponse = dlgDeviceSelect.DoModal();
	if ((nResponse == IDOK))// && (dlgDeviceSelect.m_bth.ullLong != 0))
	{
		OutputDebugStringW(L"CMeshControllerDlg::dlg.hLib");

		{
			CBtWin10Interface *pWin10BtInterface = dynamic_cast<CBtWin10Interface *>(m_btInterface);
			pWin10BtInterface->ResetInterface();
			delete m_btInterface;
			m_btInterface = NULL;
		}

		//dlg.SetParam(&dlgDeviceSelect.m_bth, hLib);
		m_btInterface = new CBtWin10Interface(&dlgDeviceSelect.m_bth, this);


		if (!m_btInterface->Init())
			return TRUE;

		CBtWin10Interface *pWin10BtInterface = dynamic_cast<CBtWin10Interface *>(m_btInterface);

		// Assume that we are connected.  Failed attempt to read battery will change that to FALSE.
		pWin10BtInterface->m_bConnected = TRUE;

		if (pWin10BtInterface->m_bConnected)
		{
			DeviceConnected();
		}

		pWin10BtInterface->RegisterNotification(&guidSvcMeshProvisioning, &guidCharProvisioningDataOut);
		pWin10BtInterface->RegisterNotification(&guidSvcMeshProxy, &guidCharProxyDataOut);
		pWin10BtInterface->RegisterNotification(&guidSvcMeshCommand, &guidCharCommandData);
		pWin10BtInterface->RegisterNotification(&guidSvcWSUpgrade, &guidCharWSUpgradeControlPoint);
	}

	return S_OK;
}



//---------------------- PB_GATT  -------------------------------------
/**
* Generates random number.
*
* Parameters:
*   random:     Buffer for generated random.
*   len:        Length of the random number to generate.
*
* Return: None
*
*/
void mesh_generate_random(uint8_t* random, uint8_t len)
{
    uint32_t r;
    uint8_t l;
    while (len)
    {
        r = rbg_rand();
        l = len > 4 ? 4 : len;
        memcpy(random, &r, l);
        len -= l;
        random += l;
    }
}

extern "C" void provision_gatt_send_cb(const UINT8 *packet, UINT32 packet_len)
{
    BTW_GATT_VALUE gatt_value;
    gatt_value.len = packet_len;
    memcpy(gatt_value.value, packet, packet_len);
    if (!m_hDlg->m_btInterface->WriteCharacteristic(&guidSvcMeshProvisioning, &guidCharProvisioningDataIn, TRUE, &gatt_value))
    {
        ods("provision_gatt_send_cb: WriteCharacteristic failed.");
    }
}

void CMeshControllerDlg::OnBnClickedProvision()
{
    DWORD out = GetHexValueInt(IDC_NODE_ID);

    if (out == 0)
    {
        MessageBox(L"Enter a valid Node ID");
        return;
    }

    // initialize provisioning layer
    {
        UINT8                                   pb_priv_key[WICED_BT_MESH_PROVISION_PRIV_KEY_LEN];
        mesh_generate_random(pb_priv_key, WICED_BT_MESH_PROVISION_PRIV_KEY_LEN);
        wiced_bt_mesh_provision_init(pb_priv_key, wiced_bt_mesh_provision_started, wiced_bt_mesh_provision_end,
            wiced_bt_mesh_provision_on_capabilities, wiced_bt_mesh_provision_get_capabilities,
            wiced_bt_mesh_provision_get_oob, provision_gatt_send_cb);
    }
    // start provisioning using conn_id=1
    wiced_bt_mesh_provision_start(1, (UINT16)out, NULL, 0);
}

// handles packet received from provisioning characteristic
LRESULT CMeshControllerDlg::OnProvisioningDataIn(WPARAM Instance, LPARAM lparam)
{
    BTW_GATT_VALUE *pValue = (BTW_GATT_VALUE *)lparam;
    // we started provisioning with hardcoded conn_id=1 in call to wiced_bt_mesh_provision_start()
    wiced_bt_mesh_provision_gatt_packet(1, pValue->value, (UINT8)pValue->len);
    free(pValue);
    return S_OK;
}

// application must implement functions: wiced_bt_mesh_provision_end, wiced_bt_mesh_provision_on_capabilities,
// wiced_bt_mesh_provision_get_oob and wiced_bt_mesh_provision_get_capabilities
// Called by provisioning layer on successfull or failed end of provisioning.
void wiced_bt_mesh_provision_end(UINT32 conn_id, UINT8 result)
{
    if (result == WICED_BT_MESH_PROVISION_RESULT_SUCCESS)
    {
        // on success update dev key in the UI
        UINT8 buf[MESH_KEY_LEN];
        mesh_get_dev_key(buf);
        m_hDlg->SetHexValue(IDC_DEV_KEY, buf, MESH_KEY_LEN);

        m_hDlg->MessageBox(L"Provisioning Succeeded");
    }
    else
        m_hDlg->MessageBox(L"Provisioning Failed");
}

// for now we don't support OOB. So just call wiced_bt_mesh_provision_set_mode() with 0(FALSE) in each field of the start starting from second assuming provisioning level it shouldn't be called
wiced_bool_t wiced_bt_mesh_provision_on_capabilities(uint32_t conn_id, const wiced_bt_mesh_provision_capabilities_t *capabilities)
{
    wiced_bt_mesh_provision_start_t start = { WICED_BT_MESH_PROVISION_START_ALG_FIPS_P256 };
    return wiced_bt_mesh_provision_set_mode(conn_id, &start);
}

// for now we don't support OOB. So it shouldn't be called.
wiced_bool_t wiced_bt_mesh_provision_get_oob(UINT32 conn_id, UINT8 type, UINT8 type_or_size, UINT8 location_or_action)
{
    return FALSE;
}

// for provisioner app it shouldn't be called.
wiced_bool_t wiced_bt_mesh_provision_get_capabilities(UINT32 conn_id)
{
    return FALSE;
}


void CMeshControllerDlg::OnTimer(UINT_PTR nIDEvent)
{
    wiced_timer_handle();

    UINT32 seq = mesh_get_sequence_number();
    if (seq != m_seq)
    {
        m_seq = seq;
        SetHexValueInt(IDC_MY_SEQ, m_seq, 3);
    }

    CDialogEx::OnTimer(nIDEvent);
}

/**
* Should be implemented by app to handle received pong message
*
* Parameters:
*   src:            address of the element which sent this PONG
*   ttl_received:   ttl of the received ping message on peer side.
*   init_ttl:       initial ttl of this message.
*   ttl:            message TTL.
*
* Return:   None
*/
void tl_handle_pong(UINT16 src, UINT8 ttl_received, UINT8 init_ttl, UINT8 ttl)
{
    WCHAR buf[128];
    wsprintf(buf, L"received from:%x PONG peer_received_ttl:%x init_ttl:%x received_ttl:%x", src, ttl_received, init_ttl, ttl);
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(buf));
}

/**
* Converts the given opcode to the corresponding byte stream
* Note: It does not check the validity of the opcode.
*/
uint8_t* mesh_opcode_to_stream(uint8_t *p, uint32_t op)
{
    if (op <= 0xff)
    {
        *p++ = (uint8_t)op;
    }
    else if (op <= 0xffff)
    {
        *p++ = (uint8_t)((op & 0x00FF00) >> 8);
        *p++ = (uint8_t)(op & 0x0000FF);
    }
    else
    {
        *p++ = (uint8_t)((op & 0xFF0000) >> 16);
        *p++ = (uint8_t)((op & 0x00FF00) >> 8);
        *p++ = (uint8_t)(op & 0x0000FF);
    }

    return p;
}

uint16_t genonoff_create_get_msg(uint8_t *buf)
{
    uint16_t len = (uint16_t)(mesh_opcode_to_stream(buf, WICED_BT_MESH_OPCODE_GEN_ONOFF_GET) - buf);
    return len;
}

uint16_t genonoff_create_set_msg(wiced_bool_t unreliable, wiced_bool_t onoff, uint8_t tid, uint8_t trans_time_ms, uint8_t delay_5ms, uint8_t *buf)
{
    uint16_t len = (uint16_t)(mesh_opcode_to_stream(buf, unreliable ? WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED : WICED_BT_MESH_OPCODE_GEN_ONOFF_SET) - buf);
    buf[len++] = onoff ? 0x01 : 0x00;
    buf[len++] = tid;
    if (trans_time_ms != (uint8_t)-1)
    {
        buf[len++] = trans_time_ms;
        buf[len++] = delay_5ms;
    }
    return len;
}


void CMeshControllerDlg::OnCbnSelchangeOpCode()
{
    WCHAR text[128];
    UINT8 buf[100];
    UINT16 len;
    UINT8 newKey[16];
    UINT8 ttl;
    CComboBox* combo;
    DWORD i;
    UINT16  dst;
    UINT16  addr;
    UINT16  my_addr;

    combo = (CComboBox*)GetDlgItem(IDC_OP_CODE);
    ttl = (UINT8)GetHexValueInt(IDC_TTL);
    GetHexValue(IDC_NEW_KEY, newKey, sizeof(newKey));
    combo->GetLBText(combo->GetCurSel(), text);
    dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    addr = (UINT16)GetHexValueInt(IDC_NODE_ID);
    my_addr = (UINT16)GetHexValueInt(IDC_MY_NODE_ID);

    do
    {
        // For configuration model make AKF unchecked
        ((CButton *)GetDlgItem(IDC_AKF))->SetCheck(BST_UNCHECKED);
        len = 0;
        for (i = 0; i < sizeof(OPsWithNoParams) / sizeof(OPsWithNoParams[0]); i++)
        {
            if (0 == wcscmp(text, op2str(OPsWithNoParams[i])))
                break;
        }
        if (i < sizeof(OPsWithNoParams) / sizeof(OPsWithNoParams[0]))
        {
            len = foundation_crt_get_msg(OPsWithNoParams[i], buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_DEVICE_COMPOS_DATA_GET)))
        {
            len = foundation_crt_compos_data_get_msg(0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET)))
        {
            len = foundation_crt_set_msg(WICED_BT_MESH_CORE_CMD_CONFIG_DEFAULT_TTL_SET, ttl, buf);
            break;
        }
        for (i = 0; i < sizeof(OPsWithState) / sizeof(OPsWithState[0]); i++)
        {
            if (0 == wcscmp(text, op2str(OPsWithState[i])))
                break;
        }
        if (i < sizeof(OPsWithState) / sizeof(OPsWithState[0]))
        {
            len = foundation_crt_set_msg(OPsWithState[i], 1, buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET)))
        {
            // enabled, no retransmittions
            len = foundation_crt_relay_set_msg(1, 0, buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_RELAY_SET)))
        {
            // enabled, no retransmittions
            len = foundation_crt_config_lpn_poll_timeout_get_msg(addr, buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_GET)))
        {
            len = foundation_crt_node_identity_get_msg(0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NODE_IDENTITY_SET)))
        {
            len = foundation_crt_node_identity_set_msg(0, 0, buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_APPKEY_ADD)))
        {
            len = foundation_crt_appkey_add_msg(0x000, 0x000, newKey, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE)))
        {
            len = foundation_crt_appkey_update_msg(0x000, 0x000, newKey, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_APPKEY_DELETE)))
        {
            len = foundation_crt_appkey_del_msg(0x000, 0x000, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_APPKEY_GET)))
        {
            len = foundation_crt_appkey_get_msg(0x000, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NETKEY_ADD)))
        {
            len = foundation_crt_netkey_add_msg(0x456, newKey, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE)))
        {
            len = foundation_crt_netkey_update_msg(0, newKey, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NETKEY_DELETE)))
        {
            len = foundation_crt_netkey_del_msg(1, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET)))
        {
            len = foundation_crt_pub_get_msg(dst, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET)))
        {
            len = foundation_crt_pub_set_msg(dst, 1, 1, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET)))
        {
            UINT8 addr[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            len = foundation_crt_pub_virt_addr_set_msg(dst, addr, 1, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 0, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD)))
        {
            len = foundation_crt_subs_add_msg(dst, 0xc001, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD)))
        {
            UINT8 addr[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
            len = foundation_crt_subs_virt_addr_add_msg(dst, addr, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE)))
        {
            len = foundation_crt_subs_del_msg(dst, 1, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL)))
        {
            len = foundation_crt_subs_del_all_msg(dst, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE)))
        {
            UINT8 addr[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            len = foundation_crt_subs_virt_addr_del_msg(dst, addr, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE)))
        {
            len = foundation_crt_subs_ovr_msg(dst, 1, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE)))
        {
            UINT8 addr[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
            len = foundation_crt_subs_virt_addr_ovr_msg(dst, addr, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET)))
        {
            len = foundation_crt_subs_get_msg(dst, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET)))
        {
            len = foundation_crt_subs_get_msg(dst, MESH_COMPANY_ID_CYPRESS, CY_MODEL_ID_TEST_0001, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_NODE_RESET)))
        {
            len = foundation_crt_prov_node_reset_msg(buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND)))
        {
            len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET)))
        {
            len = foundation_crt_prov_model_app_get_msg(dst, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND)))
        {
            len = foundation_crt_prov_model_app_unbind_msg(dst, 1, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET)))
        {
            len = foundation_crt_prov_model_app_get_msg(dst, MESH_COMPANY_ID_CYPRESS, CY_MODEL_ID_TEST_0001, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_GET)))
        {
            len = key_refresh_crt_key_refresh_phase_get_msg(0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_CONFIG_KEY_REFRESH_PHASE_SET)))
        {
            len = key_refresh_crt_key_refresh_phase_set_msg(0, 2, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_GET, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_PUBLICATION_SET)))
        {
            len = foundation_crt_heartbeat_publication_set(my_addr, 0x03, 0x03, 0x1f, 0x0007, 0x0000, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_GET, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEARTBEAT_SUBSCRIPTION_SET)))
        {
            len = foundation_crt_heartbeat_subs_set(dst, addr, 0x03, buf);
            break;
        }
        // For non-configuration model make AKF checked
        ((CButton *)GetDlgItem(IDC_AKF))->SetCheck(BST_CHECKED);

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_GET)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_ATTENTION_GET, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_SET)))
        {
            len = foundation_crt_set_msg(WICED_BT_MESH_CORE_CMD_ATTENTION_SET, 10, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED)))
        {
            len = foundation_crt_set_msg(WICED_BT_MESH_CORE_CMD_ATTENTION_SET_UNACKED, 10, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_CLEAR_UNACKED, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_GET, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST)))
        {
            len = health_crt_fault_test(WICED_FALSE, 0x01, MESH_COMPANY_ID_CYPRESS, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_FAULT_TEST_UNACKED)))
        {
            len = health_crt_fault_test(WICED_TRUE, 0x01, MESH_COMPANY_ID_CYPRESS, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET)))
        {
            len = foundation_crt_get_msg(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_GET, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET)))
        {
            len = foundation_crt_set_msg(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET, 4, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED)))
        {
            len = foundation_crt_set_msg(WICED_BT_MESH_CORE_CMD_HEALTH_PERIOD_SET_UNACKED, 4, buf);
            break;
        }
        
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_GET)))
        {
            len = genonoff_create_get_msg(buf);
            break;
        }

        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_SET)))
        {
            len = genonoff_create_set_msg(FALSE, TRUE, 10, 0x54, 0x05, buf);
            break;
        }
        /*
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_ONOFF_SET_UNACKED)))
        {
            len = genonoff_create_set_msg(TRUE, TRUE, 1, 0, 0, buf);
            break;
        }
        //opcodes: def tran time model
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_GET)))
        {
            len = gendeftrantime_create_get_msg(buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET)))
        {
            len = gendeftrantime_create_set_msg(FALSE, 5000, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_DEFTRANSTIME_SET_UNACKED)))
        {
            len = gendeftrantime_create_set_msg(TRUE, 5000, buf);
            break;
        }
        //opcodes: level model
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_GET)))
        {
            len = genlevel_create_get_msg(buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_SET)))
        {
            //len = genlevel_create_set_msg(FALSE, 0x9ABC, 1, 0, 0, buf);
            len = genlevel_create_set_msg(FALSE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_SET_UNACKED)))
        {
            //len = genlevel_create_set_msg(TRUE, 0x9ABC, 1, 0, 0, buf);
            len = genlevel_create_set_msg(TRUE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET)))
        {
            len = genlevel_create_delta_set_msg(FALSE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_DELTA_SET_UNACKED)))
        {
            len = genlevel_create_delta_set_msg(TRUE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET)))
        {
            len = genlevel_create_move_set_msg(FALSE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
        if (0 == wcscmp(text, op2str(WICED_BT_MESH_OPCODE_GEN_LEVEL_MOVE_SET_UNACKED)))
        {
            len = genlevel_create_move_set_msg(TRUE, 0x001F, 1, 0x1F * 1000, 0, buf);
            break;
        }
*///???
    } while (false);
    if (len)
        SetHexValue(IDC_PAYLOAD, buf, len);
}

/**
* Process data packet addressed to this mesh node
*
* Parameters:
*   dst_id:         Destination address of the message.
*   src_id:         Address of the message source.
*   app_key_idx:    app key index. 0xff means dev key.
*   ttl:            TTL of the received packet
*   opcode:         opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*   company_id:     Manufacturer ID. 0 means it is not manufacturer specific
*   params:         parameters of the app payload
*   params_len:     length of the parameters of the app payload
*
* Return:   None
*
*/
void mesh_handle_manuf_spec_cmd(UINT16 dst_id, UINT16 src_id, UINT8 app_key_idx, UINT8 ttl, UINT16 opcode, UINT16 op_company_id, const UINT8 *params, UINT16 params_len)
{
    ods("handle_manuf_spec_cmd: ttl=%x app_key_idx:%x src_id=%x", ttl, app_key_idx, src_id);
    ods(" op_company_id:%x opcode:%x", op_company_id, opcode);
    ods(" dst_id:%x", dst_id);
    if (params_len)
        ble_tracen((char*)params, params_len);
}

/**
* Implemented by provisioner and provisioning app.
* Called by provisioning layer on successfull start of provisioning.
*
* Parameters:
*   conn_id:        Connection ID of the provisioning connection
*
* Return:   None
*/
static void wiced_bt_mesh_provision_started(UINT32 conn_id)
{
}

static wiced_timer_t *wiced_timer_first = NULL;

static void wiced_timer_handle()
{
    wiced_timer_t   *p_timer;
    uint64_t        curr_time = GetTickCount64();

    // got through each initialized timer
    for (p_timer = wiced_timer_first; p_timer != NULL; p_timer = (wiced_timer_t*)p_timer->next)
    {
        // if it is not started or not expired then ignore it
        if (p_timer->end_time == 0 || p_timer->end_time > curr_time)
            continue;
        // update end_time
        switch (p_timer->type)
        {
        case WICED_SECONDS_PERIODIC_TIMER:
            p_timer->end_time += (uint64_t)p_timer->timeout * 1000;
            break;
        case WICED_MILLI_SECONDS_PERIODIC_TIMER:
            p_timer->end_time += p_timer->timeout;
            break;
        default:
            p_timer->end_time = 0;
            break;
        }
        //call callback
        if (p_timer->cback)
            p_timer->cback(p_timer->cback_param);
        //exit. Oter expired timers will be handled next time
        break;
    }
}

wiced_result_t wiced_init_timer(wiced_timer_t* p_timer, wiced_timer_callback_fp TimerCb, uint32_t cBackparam, wiced_timer_type_t type)
{
    p_timer->cback = TimerCb;
    p_timer->cback_param = cBackparam;
    p_timer->type = type;
    p_timer->end_time = 0;
    p_timer->timeout = 0;
    p_timer->next = wiced_timer_first == NULL ? NULL : wiced_timer_first->next;
    wiced_timer_first = p_timer;
    return WICED_BT_SUCCESS;
}

wiced_result_t wiced_start_timer(wiced_timer_t* p_timer, uint32_t timeout)
{
    p_timer->timeout = timeout;
    p_timer->end_time = timeout;
    if (p_timer->type == WICED_SECONDS_TIMER || p_timer->type == WICED_SECONDS_PERIODIC_TIMER)
        p_timer->end_time *= 1000;
    p_timer->end_time += GetTickCount64();
    return WICED_BT_SUCCESS;
}

wiced_result_t wiced_stop_timer(wiced_timer_t* p_timer)
{
    p_timer->end_time = 0;
    return WICED_BT_SUCCESS;
}

UINT32 mesh_read_node_info(int inx, UINT8* node_info, UINT16 len, wiced_result_t *p_result)
{
    return 0;
}

UINT32 mesh_write_node_info(int inx, const UINT8* node_info, UINT16 len, wiced_result_t *p_result)
{
    return 0;
}

void CMeshControllerDlg::SendProxyFltMsg(BYTE opcode)
{
    // get node address(ID) to use for add filter
    DWORD addr_add = GetHexValueInt(IDC_NODE_ID);
    if (addr_add == 0)
    {
        MessageBox(L"Enter a valid Node ID");
        return;
    }
    wiced_bt_mesh_event_t *p_event = wiced_bt_mesh_create_event(0, 0xffff, 0, 0, 0);
    if (p_event == NULL)
    {
        MessageBox(L"Failed to create context");
        return;
    }
    p_event->opcode = opcode;
    uint8_t params[2];
    UINT16TOBE2(params, (uint16_t)addr_add);
    if(wiced_bt_mesh_core_send(p_event, params, sizeof(params), NULL))
    {
        MessageBox(L"Failed to send");
        return;
    }
}

void CMeshControllerDlg::OnBnClickedAddFlt()
{
    SendProxyFltMsg(WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_ADD_ADDR);
}

void CMeshControllerDlg::OnBnClickedDelFlt()
{
    SendProxyFltMsg(WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_DEL_ADDR);
}

void CMeshControllerDlg::SendProxyFltSetTypeMsg(BYTE type)
{
    uint8_t opcode = WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_SET_TYPE;

    TraceMsg(FALSE, MESH_NODE_ID_INVALID, 0, 0xffff, opcode, &type, 1);
    wiced_bt_mesh_event_t *p_event = wiced_bt_mesh_create_event(0, 0xffff, 0, 0, 0);
    if (p_event == NULL)
    {
        MessageBox(L"Failed to create context");
        return;
    }
    p_event->opcode = opcode;
    if(wiced_bt_mesh_core_send(p_event, &type, 1, NULL))
    {
        MessageBox(L"Failed to send");
        return;
    }
}

void CMeshControllerDlg::OnBnClickedSetWhiteFlt()
{
    SendProxyFltSetTypeMsg(MESH_FLT_TYPE_WHITE_LIST);
}

void CMeshControllerDlg::OnBnClickedSetBlackFlt()
{
    SendProxyFltSetTypeMsg(MESH_FLT_TYPE_BLACK_LIST);
}

/**
* Application should implement this function and return milliseconds passed since start or any other moment.
*
* Parameters:   None
*
*   Return:     Milliseconds passed since start or any other moment
*/
extern "C" uint64_t wiced_bt_mesh_core_get_tick_count()
{
    return GetTickCount64();
}

#if 1
extern "C" uint8_t mesh_ble_evt_type = 0;//?????
#endif


bool CMeshControllerDlg::Send(uint16_t dst, uint8_t app_key_idx, wiced_bool_t szmicn, uint8_t *payload, uint16_t payload_len)
{
    UINT8   ttl = (UINT8)GetHexValueInt(IDC_TTL);
    UINT32  seq = GetHexValueInt(IDC_MY_SEQ);

    UINT16  company_id, opcode;
    UINT8 offset = access_layer_parse_payload(payload, payload_len, &company_id, &opcode);
    if (!offset)
    {
        MessageBox(L"Invalid message");
        return false;
    }
    TraceMsg(FALSE, dst, ttl, 0, opcode, &payload[offset], payload_len - offset);

    if (seq != m_seq)
    {
        m_seq = seq;
        mesh_set_sequence_number(m_seq);
    }

    wiced_bt_mesh_event_t *p_event = wiced_bt_mesh_create_event(0, 0xffff, 0, 0, 0);
    if(p_event == NULL)
    {
        MessageBox(L"Failed to create context");
        return false;
    }
    p_event->company_id = company_id;
    p_event->opcode = opcode;
    p_event->app_key_idx = app_key_idx;
    p_event->dst = dst;
    if (wiced_bt_mesh_core_send(p_event, &payload[offset], payload_len - offset, NULL))
    {
        MessageBox(L"Failed to send");
        return false;
    }
    return true;
}

void CMeshControllerDlg::OnBnClickedConfigure()
{
    UINT8 key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  model_id = (UINT16)GetHexValueInt(IDC_MODEL_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);
    if (!Send(dst, 0xff, WICED_FALSE, payload, payload_len))
        return;
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, model_id, payload);
    if(!Send(dst, 0xff, WICED_FALSE, payload, payload_len))
        return;
    payload_len = foundation_crt_subs_add_msg(dst, subscr_addr, 0, model_id, payload);
    if(!Send(dst, 0xff, WICED_FALSE, payload, payload_len))
        return;
}


void CMeshControllerDlg::OnBnClickedCheckNodeIdentity()
{
    uint8_t         payload[MESH_MAX_APP_PAYLOAD_LEN];
    uint16_t        net_key_idx;
    uint16_t        dst = (uint16_t)GetHexValueInt(IDC_DST_NODE_ID);
    uint8_t         payload_len = (uint8_t)GetHexValue(IDC_PAYLOAD, payload, MESH_MAX_APP_PAYLOAD_LEN);
    if(!wiced_bt_mesh_core_check_node_identity(dst, payload, payload_len, &net_key_idx))
        MessageBox(L"Failed to check");
    else
    {
        wchar_t buf[64];
        swprintf_s(buf, sizeof(buf)/sizeof(buf[0]), L"net_key_idx: %d", net_key_idx);
        MessageBox(buf);
    }
}


void CMeshControllerDlg::OnBnClickedOtaUpgradeStart()
{
    // If Downloader object is created already
    if (m_pDownloader != NULL)
    {
        delete m_pDownloader;
        m_pDownloader = NULL;
        SetDlgItemText(IDC_OTA_UPGRADE_START, L"OTA Start");
        return;
    }
    if (m_dwPatchSize == 0)
    {
        // Select OTA FW upgrade file
        CString fileName;
        CFileDialog dlgFile(TRUE, NULL, NULL, OFN_OVERWRITEPROMPT, NULL);
        if (dlgFile.DoModal() != IDOK)
            return;
        fileName = dlgFile.GetPathName();

        FILE *fPatch;
        if (_wfopen_s(&fPatch, fileName, L"rb"))
        {
            MessageBox(L"Failed to open the patch file", L"Error", MB_OK);
            return;
        }

        // Load OTA FW file into memory
        fseek(fPatch, 0, SEEK_END);
        m_dwPatchSize = ftell(fPatch);
        rewind(fPatch);
        if (m_pPatch)
            delete m_pPatch;
        m_pPatch = (LPBYTE)new BYTE[m_dwPatchSize];

        m_dwPatchSize = (DWORD)fread(m_pPatch, 1, m_dwPatchSize, fPatch);
        fclose(fPatch);
    }
    CBtWin8Interface *pWin8BtInterface = dynamic_cast<CBtWin8Interface *>(m_btInterface);

    // Assume that we are connected.
    pWin8BtInterface->m_bConnected = TRUE;

    SetDlgItemText(IDC_OTA_UPGRADE_START, L"OTA Abort");

    // Create new downloader object
    m_pDownloader = new WSDownloader(m_btInterface, m_pPatch, m_dwPatchSize, m_hWnd);
    m_pDownloader->ProcessEvent(WSDownloader::WS_UPGRADE_CONNECTED);
}

LRESULT CMeshControllerDlg::OnProgress(WPARAM state, LPARAM param)
{
    static UINT total;
    if (state == WSDownloader::WS_UPGRADE_STATE_WAIT_FOR_READY_FOR_DOWNLOAD)
    {
        total = (UINT)param;
        m_Progress.SetRange32(0, (int)param);
        SetDlgItemText(IDC_OTA_UPGRADE_START, L"Abort");
    }
    else if (state == WSDownloader::WS_UPGRADE_STATE_DATA_TRANSFER)
    {
        m_Progress.SetPos((int)param);
        if (param == total)
        {
            m_pDownloader->ProcessEvent(WSDownloader::WS_UPGRADE_START_VERIFICATION);
        }
    }
    else if (state == WSDownloader::WS_UPGRADE_STATE_VERIFIED)
    {
        SetDlgItemText(IDC_OTA_UPGRADE_START, L"Done");
    }
    else if (state == WSDownloader::WS_UPGRADE_STATE_ABORTED)
    {
        m_Progress.SetPos(total);
        SetDlgItemText(IDC_OTA_UPGRADE_START, L"Failed");
    }
    return S_OK;
}

DWORD WINAPI BatteryClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->BatteryClientSetup();
    return 0;
}

void CMeshControllerDlg::BatteryClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedBatteryClient()
{
    CreateThread(NULL, 0, BatteryClientSetupThread, this, 0, NULL);
}

DWORD WINAPI BatteryServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->BatteryServerSetup();
    return 0;
}

void CMeshControllerDlg::BatteryServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_BATTERY_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedBatteryServer()
{
    CreateThread(NULL, 0, BatteryServerSetupThread, this, 0, NULL);
}

extern "C" void *wiced_bt_get_buffer(uint32_t len)
{
    return malloc(len);
}
extern "C" void wiced_bt_free_buffer(void* buffer)
{
    free(buffer);
}

extern "C" uint64_t clock_SystemTimeMicroseconds64(void)
{
    return GetTickCount64() * 1000;
}

DWORD WINAPI LocationClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LocationClientSetup();
    return 0;
}

void CMeshControllerDlg::LocationClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedLocationClient()
{
    CreateThread(NULL, 0, LocationClientSetupThread, this, 0, NULL);
}

DWORD WINAPI LocationServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LocationServerSetup();
    return 0;
}

void CMeshControllerDlg::LocationServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LOCATION_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedLocationServer()
{
    CreateThread(NULL, 0, LocationServerSetupThread, this, 0, NULL);
}

DWORD WINAPI OnOffClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->OnOffClientSetup();
    return 0;
}

void CMeshControllerDlg::OnOffClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedOnoffClient()
{
    CreateThread(NULL, 0, OnOffClientSetupThread, this, 0, NULL);
}


DWORD WINAPI OnOffServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->OnOffServerSetup();
    return 0;
}

void CMeshControllerDlg::OnOffServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedOnoffServer()
{
    CreateThread(NULL, 0, OnOffServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LevelClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LevelClientSetup();
    return 0;
}

void CMeshControllerDlg::LevelClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedLevelClient()
{
    CreateThread(NULL, 0, LevelClientSetupThread, this, 0, NULL);
}


DWORD WINAPI LevelServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LevelServerSetup();
    return 0;
}

void CMeshControllerDlg::LevelServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedLevelServer()
{
    CreateThread(NULL, 0, LevelServerSetupThread, this, 0, NULL);
}


DWORD WINAPI TransitionTimeClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->TransitionTimeClientSetup();
    return 0;
}

void CMeshControllerDlg::TransitionTimeClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedTransitionTimeClient()
{
    CreateThread(NULL, 0, TransitionTimeClientSetupThread, this, 0, NULL);
}

DWORD WINAPI TransitionTimeServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->TransitionTimeServerSetup();
    return 0;
}

void CMeshControllerDlg::TransitionTimeServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedTransitionTimeServer()
{
    CreateThread(NULL, 0, TransitionTimeServerSetupThread, this, 0, NULL);
}

DWORD WINAPI PowerOnOffServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PowerOnOffServerSetup();
    return 0;
}

void CMeshControllerDlg::PowerOnOffServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;
    UINT16          pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x00, 0x00, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
}

void CMeshControllerDlg::OnBnClickedPowerOnoffSetupServer()
{
    CreateThread(NULL, 0, PowerOnOffServerSetupThread, this, 0, NULL);
}

DWORD WINAPI PowerOnOffClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PowerOnOffClientSetup();
    return 0;
}

void CMeshControllerDlg::PowerOnOffClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;
    UINT16          pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
}

void CMeshControllerDlg::OnBnClickedPowerOnoffClient()
{
    CreateThread(NULL, 0, PowerOnOffClientSetupThread, this, 0, NULL);
}

DWORD WINAPI PowerLevelClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PowerLevelClientSetup();
    return 0;
}

void CMeshControllerDlg::PowerLevelClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedPowerLevelClient()
{
    CreateThread(NULL, 0, PowerLevelClientSetupThread, this, 0, NULL);
}

DWORD WINAPI PowerLevelServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PowerLevelServerSetup();
    return 0;
}

void CMeshControllerDlg::PowerLevelServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedPowerLevelSetupServer()
{
    CreateThread(NULL, 0, PowerLevelServerSetupThread, this, 0, NULL);
}

DWORD WINAPI PropertyServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PropertyServerSetup();
    return 0;
}

void CMeshControllerDlg::PropertyServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_USER_PROPERTY_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_USER_PROPERTY_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedPropertyServer()
{
    CreateThread(NULL, 0, PropertyServerSetupThread, this, 0, NULL);
}


DWORD WINAPI PropertyClientSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->PropertyClientSetup();
    return 0;
}

void CMeshControllerDlg::PropertyClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
}

void CMeshControllerDlg::OnBnClickedPropertyClient()
{
    CreateThread(NULL, 0, PropertyClientSetupThread, this, 0, NULL);
}

DWORD WINAPI LightLightnessServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightLightnessServerSetup();
    return 0;
}

void CMeshControllerDlg::LightLightnessServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
    
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightLightnessSetupServer()
{
    CreateThread(NULL, 0, LightLightnessServerSetupThread, this, 0, NULL);
}


DWORD WINAPI LightLightnessClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightLightnessClientSetup();
    return 0;
}

void CMeshControllerDlg::LightLightnessClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightLightnessClient()
{
    CreateThread(NULL, 0, LightLightnessClientThread, this, 0, NULL);
}

DWORD WINAPI SensorClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->SensorClientSetup();
    return 0;
}

void CMeshControllerDlg::SensorClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedSensorClient()
{
    CreateThread(NULL, 0, SensorClientThread, this, 0, NULL);
}

DWORD WINAPI SensorServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->SensorServerSetup();
    return 0;
}

void CMeshControllerDlg::SensorServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedSensorServer()
{
    CreateThread(NULL, 0, SensorServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LightCtlServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightCtlServerSetup();
    return 0;
}

void CMeshControllerDlg::LightCtlServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    // First configure the second element with Light CTL Temperature Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_TEMPERATURE_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_TEMPERATURE_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    // Second configure the first element with Light CTL Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
    return;
failed:
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"failed"));
    return;
}

void CMeshControllerDlg::OnBnClickedLightCtlServer()
{
    CreateThread(NULL, 0, LightCtlServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LightCtlClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightCtlClientSetup();
    return 0;
}

void CMeshControllerDlg::LightCtlClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_CTL_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightCtlClient()
{
    CreateThread(NULL, 0, LightCtlClientThread, this, 0, NULL);
}

DWORD WINAPI LightHslServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightHslServerSetup();
    return 0;
}

void CMeshControllerDlg::LightHslServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    // First configure the third element with Light HSL Saturation Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 2, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SATURATION_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 2, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SATURATION_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 2, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 2, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    // Configure the second element with Light HSL Hue Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_HUE_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_HUE_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    // Second configure the first element with Light HSL Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
    return;
failed:
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"failed"));
    return;
}

void CMeshControllerDlg::OnBnClickedLightHslServer()
{
    CreateThread(NULL, 0, LightHslServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LightHslClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightHslClientSetup();
    return 0;
}

void CMeshControllerDlg::LightHslClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_HSL_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightHslClient()
{
    CreateThread(NULL, 0, LightHslClientThread, this, 0, NULL);
}


DWORD WINAPI LightXylServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightXylServerSetup();
    return 0;
}

void CMeshControllerDlg::LightXylServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        goto failed;

    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
    return;
failed:
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"failed"));
    return;
}

void CMeshControllerDlg::OnBnClickedLightXylServer()
{
    CreateThread(NULL, 0, LightXylServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LightXylClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightXylClientSetup();
    return 0;
}

void CMeshControllerDlg::LightXylClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightXylClient()
{
    CreateThread(NULL, 0, LightXylClientThread, this, 0, NULL);
}

DWORD WINAPI LightLcServerSetupThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightLcServerSetup();
    return 0;
}

void CMeshControllerDlg::LightLcServerSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_SRVR);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    // second element contains Generic OnOff, Light LC Server and Light LC Setup Server
    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst + 1, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst + 1, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    // the first contains light lightness model and everything related to it
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_LEVEL_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_SETUP_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_SRV, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        goto failed;

    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
    return;
failed:
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"failed"));
    return;
}

void CMeshControllerDlg::OnBnClickedLightLcServer()
{
    CreateThread(NULL, 0, LightLcServerSetupThread, this, 0, NULL);
}

DWORD WINAPI LightLcClientThread(void *Context)
{
    CMeshControllerDlg *pDlg = (CMeshControllerDlg *)Context;
    pDlg->LightLcClientSetup();
    return 0;
}

void CMeshControllerDlg::LightLcClientSetup(void)
{
    UINT8   key[16];
    UINT8   payload[MESH_MAX_APP_PAYLOAD_LEN];
    UINT16  payload_len;
    UINT16  dst = (UINT16)GetHexValueInt(IDC_DST_NODE_ID);
    UINT16  subscr_addr = (UINT16)GetHexValueInt(IDC_SUBSCR_ADDR);
    UINT16  pub_addr = (UINT16)GetHexValueInt(IDC_PUB_ADDR_CLNT);

    BOOLEAN         akf = ((CButton *)GetDlgItem(IDC_AKF))->GetCheck() != 0;
    wiced_bool_t    szmic = ((CButton *)GetDlgItem(IDC_SZMICN))->GetCheck() != 0 ? WICED_TRUE : WICED_FALSE;

    GetHexValue(IDC_APP_KEY, key, sizeof(key));
    payload_len = foundation_crt_appkey_add_msg(0x000, 0x000, key, payload);

    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 10000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LC_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
        return;

    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONPOWERUP_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_DEFTT_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_prov_model_app_bind_msg(dst, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    payload_len = foundation_crt_pub_set_msg(dst, pub_addr, 0, 7, 0, 0, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ONOFF_CLNT, WICED_FALSE, 3, 2, payload);
    ResetEvent(m_hCfgEvent);
    Send(dst, akf ? 0 : 0xff, szmic, payload, payload_len);
    if (WaitForSingleObject(m_hCfgEvent, 5000) != WAIT_OBJECT_0)
    {
        ods("failed.");
        return;
    }
    m_hDlg->m_trace->SetCurSel(m_hDlg->m_trace->AddString(L"success"));
}

void CMeshControllerDlg::OnBnClickedLightLcClient()
{
    CreateThread(NULL, 0, LightLcClientThread, this, 0, NULL);
}

void CMeshControllerDlg::OnBnClickedResetNode()
{
    BTW_GATT_VALUE gatt_value;
    gatt_value.len = 1;
    gatt_value.value[0] = MESH_COMMAND_RESET_NODE;
    if (!m_btInterface->WriteCharacteristic(&guidSvcMeshCommand, &guidCharCommandData, TRUE, &gatt_value))
    {
        ods("WriteCharacteristic failed.");
        return;
    }
}

void CMeshControllerDlg::OnBnClickedSelectDevice()
{
	m_hDlg->PostMessage(WM_USER_SELECT_DEVICE, 0, 0);
}
