/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh core library implementation.
 */
#include <stdio.h> 
#include <string.h> 

#include "platform.h"

//#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_ovl.h"
#include "core_aes_ccm.h"
#include "mesh_util.h"
#include "provisioning_int.h"
#include "ecdh.h"
#include "transport_layer.h"
#include "key_refresh.h"
#include "network_layer.h"
#include "access_layer.h"
#include "foundation_int.h"
#include "pb_transport.h"
#include "low_power.h"
#include "friend.h"
#include "wiced_timer.h"

#if MESH_CHIP == 20703
#include "rtc.h"
#elif MESH_CHIP == 20719
#else   //20735
#include "clock_timer.h"
#endif
#include "health.h"
#ifndef MESH_CONTROLLER
#include "wiced_bt_gatt.h"
#include "mesh_ota_fw_upgrade.h"
#endif

#include "hci_control_api.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__MESH_CORE_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__MESH_CORE_C
#include "mesh_trace.h"

// Forward declarations
extern uint32_t bleapputils_get32bitCrc(uint32_t* start, uint32_t lengthInWords);

// Default minimu beacon adv interval.
// The actual interval is this value plus a random number between 0 and 255.

MeshNodeNvData node_nv_data;
MeshNvAppKey node_nv_app_key[MESH_APP_KEY_MAX_NUM];
MeshNvNetKey node_nv_net_key[MESH_NET_KEY_MAX_NUM];
// node uuid
uint8_t node_uuid[MESH_DEVICE_UUID_LEN];

const static uint8_t sixteen_0[16] = {0};

MeshCtx ctx;

uint16_t    wiced_bt_mesh_core_gatt_send_max_len = 20;

wiced_timer_t   mesh_core_reboot_delay_timer;       // timer for delayed reboot
uint32_t        mesh_core_reboot_delay_timer_delay_ms;

// If true then don't alow more than one net key (for PTS tests)
wiced_bool_t mesh_core_use_one_net_key = WICED_FALSE;

static wiced_bt_mesh_core_proxy_send_cb_t    wiced_bt_mesh_core_gatt_send_callback       = NULL;
static wiced_bt_core_nvram_access_t         wiced_bt_mesh_core_nvram_access_callback    = NULL;

static wiced_timer_t    mesh_core_app_timer;  // mesh core periodic 1 sec timer
static void mesh_core_app_timer_callback(uint32_t arg);

#ifndef MESH_CONTROLLER
static uint8_t delayed_beacon_cnt = 0;
#ifdef HARDCODED_PROVIS
wiced_bool_t mesh_core_hardcoded_prov(uint16_t addr, const uint8_t *net_key, const uint8_t *dev_key, uint32_t iv_index, const uint8_t* app_key, uint8_t element, uint16_t model_id, uint16_t subs_addr);
#endif
#endif

// IV UPDATE test mode. It is FALSE on startup. HCI_CONTROL_MESH_COMMAND_CORE_SET_IV_UPDATE_TEST_MODE sets it to TRUE
wiced_bool_t mesh_core_iv_updt_test_mode = WICED_FALSE;

#ifndef MESH_CONTROLLER

#if MESH_CHIP == 20703
uint64_t clock_SystemTimeMicroseconds64(void)
{
    tRTC_REAL_TIME_CLOCK rtcClock;
    rtc_getRTCRawClock(&rtcClock);
    // To convert 128 kHz rtc timer to milliseconds divide it by 131: //128 kHz = 128 * 1024 = 131072; to microseconds: 1000000 / 131072 = 7.62939453125 (7.63)
    return rtcClock.rtc64 * 763 / 100;
}
#endif

/**
* Local function to initialize messages cache
*
* Parameters:   None
*
* Return:   None
*
*/
static void auth_cache_init(void)
{
    memset(ctx.auth_cache_src, 0, sizeof(ctx.auth_cache_src));
    ctx.auth_cache_pos = 0;
}

/**
* Sends an advertisement packet.
*
* Parameters:
*   adv:                Packet to send.
*   adv_len:            Length of the packet to send.
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:            Mesh event to be used in complete_callback
*
*   Return:     WICED_TRUE if message sent or placed to the buffer
*/
wiced_bool_t send_adv(const uint8_t* adv, uint8_t adv_len,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    uint8_t instance;
    instance = mesh_start_adv(adv, adv_len, BTM_BLE_ADVERT_NONCONN_HIGH, complete_callback, p_event);
    TRACE1(TRACE_INFO, "send_adv: started instance:%d\n", instance);
    return instance != 0xff;
}
#endif

/**
* save node NV data.
*
* Parameters:   None
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*
*/
wiced_bool_t mesh_save_node_nv_data()
{
    uint32_t len;

#ifdef MESH_CONTROLLER
    len = mesh_controller_write_node_info(node_nv_app_key[0].key, 0, (uint8_t*)&node_nv_data, sizeof(MeshNodeNvData));
#else
    len = mesh_write_node_info(MESH_NVM_IDX_NODE_DATA, (uint8_t*)&node_nv_data, sizeof(MeshNodeNvData), NULL);
#endif
    if (len == sizeof(MeshNodeNvData))
    {
        return WICED_TRUE;
    }
    TRACE1(TRACE_INFO, "save_node_nv_data failed: len = %d\n", len);
    return WICED_FALSE;
}

/**
* Mesh core library function to set or delete Application Key
*
* Parameters:
*   appkey_local_idx:   Index of the Application key in the local array.
*   netkey_local_idx:   Index of the related network key. In the local array. This param ignore for delete request (app_key == NULL)
*   app_key:            Application key (16 bytes). NULL means delete that net_key
*   appkey_global_idx:  Global index of this Application key
*
* Return:
*     Returns the result code.
*/
MeshResultCode mesh_set_app_key(uint8_t appkey_local_idx, uint8_t netkey_local_idx, const uint8_t* app_key, uint16_t apkey_global_idx)
{
    uint8_t aid;
//    TRACE3(TRACE_WARNING, "mesh_set_app_key: appkey_local_idx:%x netkey_local_idx:%x apkey_global_idx:%x\n", appkey_local_idx, netkey_local_idx, apkey_global_idx);
    // if it is not delete request then calculate all key security values
    if (app_key)
    {
        // Calculate the Application Key Identifier
        // It may be used to identify the application key used to authenticate and encrypt a message
        // AID= k4(AppKey)
        mesh_k4(app_key, &aid);
    }
    return mesh_set_app_key_and_id(appkey_local_idx, netkey_local_idx, app_key, aid, apkey_global_idx);
}

/**
* Mesh core library function to add or delete Network Key
*
* Parameters:
*   netkey_local_idx:   Index of the network key in the local array
*   net_key:            Network key (16 bytes). NULL means delete that net_key
*   netkey_global_idx:  Global index of this network key. . This param ignore for delete request (net_key == NULL)
*
* Return:
*     Returns the result code.
*
*/
MeshResultCode mesh_set_net_key(uint8_t netkey_local_idx, const uint8_t* net_key, uint16_t netkey_global_idx)
{
    uint8_t nid, encr_key[MESH_KEY_LEN], privacy_key[MESH_KEY_LEN], network_id[MESH_NETWORK_ID_LEN], identity_key[MESH_KEY_LEN], beacon_key[MESH_KEY_LEN], proxy_key[MESH_KEY_LEN];
    // if it is not delete request then calculate all key security values
    if (net_key)
        calc_net_security(net_key, &nid, encr_key, privacy_key, network_id, identity_key, beacon_key, proxy_key);
    return mesh_set_net_security(netkey_local_idx, net_key, nid, encr_key, privacy_key, network_id, identity_key, beacon_key, proxy_key, netkey_global_idx);
}

/**
* Handles secured network broadcast beacon.
*
* Parameters:
*   adv_data:   Beacon data
*   adv_len:    Length of the beacon data.
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*
*/
static wiced_bool_t handle_sec_net_broadcast_beacon(const uint8_t* adv_data, uint8_t adv_len)
{
    uint8_t   auth[MESH_BEACON_CMAC_LEN];
#if !defined(MESH_CONTROLLER)
    wiced_bool_t    update_beacon = WICED_FALSE;
    uint32_t        ivi;
#endif
    uint8_t    net_key_idx;

    // Secure Network Broadcast Beacon Data Format: Flags(1byte) || NetworkID(8bytes) || IVI-Index(4bytes) || CMAC(8bytes)
    if (adv_len != 1 + MESH_NETWORK_ID_LEN + MESH_IV_INDEX_LEN + MESH_BEACON_CMAC_LEN)
    {
        TRACE1(TRACE_INFO, "handle_sec_net_beacon: invalid adv_len:%d\n", adv_len);
        // drop that packet
        return WICED_FALSE;
    }

#ifdef _DEB_BEACON_EXTENDED_TRACE
    TRACE0(TRACE_DEBUG, "handle_sec_net_beacon:\n");
    TRACEN(TRACE_DEBUG, (char*)adv_data, adv_len);
#endif

    for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
    {
        // ignore unexisting key index
        if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
            continue;
        // ignore key if its id doesn't match received NetworkID
        if (0 != memcmp(&node_nv_net_key[net_key_idx].network_id, adv_data + 1, MESH_NETWORK_ID_LEN))
            continue;

        TRACE1(TRACE_DEBUG, "handle_sec_net_beacon: net_key_idx:%d\n", net_key_idx);
        TRACEN(TRACE_DEBUG, (char*)adv_data, adv_len);
        // generate authentication value:
        mesh_calc_beacon_auth(net_key_idx, adv_data[0], &adv_data[1 + MESH_NETWORK_ID_LEN], auth);

        // Ignore key if authentication value doesn't match
        if (0 != memcmp(auth, adv_data + 1 + MESH_NETWORK_ID_LEN + MESH_IV_INDEX_LEN, MESH_BEACON_CMAC_LEN))
            continue;
        break;
    }

    if (net_key_idx >= MESH_NET_KEY_MAX_NUM)
    {
#ifdef _DEB_BEACON_EXTENDED_TRACE
        TRACE0(TRACE_DEBUG, "handle_sec_net_beacon: key isn't found\n");
#endif
        // drop that packet
        return WICED_FALSE;
    }

    // handle KR bit (Key Refresh Signaling Bit)
#ifndef MESH_CONTROLLER
    update_beacon =
#endif
        key_refresh_handle_beacon((adv_data[0] & FLAG_KEY_REFRESH_SIGNALING_BIT) != 0, net_key_idx);

#ifndef MESH_CONTROLLER
    ivi = BE4TOUINT32(&adv_data[1 + MESH_NETWORK_ID_LEN]);
 
    // Looks like IV update logic should work with beacons authenticated for prinary Subnet
    if (net_key_idx == 0)
    {
        // if IV update bit is set in the beacon then handle it
        if (iv_updt_handle((adv_data[0] & FLAG_IV_UPDATE_SIGNALING_BIT) != 0, ivi, mesh_core_iv_updt_test_mode))
            update_beacon = WICED_TRUE;
    }

    // In case of a beacon authenticated for of a primary Subnet check IVI recovery logic
    if (net_key_idx == 0)
    {
        if (ivi_recovery_handle(ivi))
            update_beacon = WICED_TRUE;
    }

    // Update proxy server advertisement and network secure beacon if needed (if IV index is changed or KR flag is changed)
    if (update_beacon)
    {
        mesh_update_beacon();
    }

#endif
    return WICED_TRUE;
}

/**
* Checks if message can be sent via GATT notifications
*
* Parameters:
*   from_gatt:  WICED_TRUE - message came from GATT
*               WICED_FALSE - message came from ADV
*   dst_id:     Address of the destination node.
*
* Return:   WICED_TRUE - message can be sent via GATT notifications
*           WICED_FALSE - message can't be sent via GATT notifications
*
*/
static wiced_bool_t can_send_notification(wiced_bool_t from_gatt, uint16_t dst_id)
{
    wiced_bool_t ret = WICED_FALSE;
    TRACE3(TRACE_DEBUG, "can_send_notification: filter_type:%d from_gatt:%d dst_id:%x\n", ctx.proxy_out_flt.type, from_gatt, dst_id);
    // can't if proxy isn't connected or this message came from gatt
    if (!ctx.conn_id || from_gatt)
        ret = WICED_FALSE;
    // if dst is in the filter list
    else if (mesh_find_filter_addr_(dst_id, NULL))
        // enable message if it is White List
        ret = ctx.proxy_out_flt.type == MESH_FLT_TYPE_WHITE_LIST ? WICED_TRUE : WICED_FALSE;
    // if dst is not in the filter list
    else
        // drop message if it is White List
        ret = ctx.proxy_out_flt.type == MESH_FLT_TYPE_WHITE_LIST ? WICED_FALSE : WICED_TRUE;
    if (!ret)
    {
        TRACE1(TRACE_DEBUG, " conn_id:%x drop msg. flt list:\n", ctx.conn_id);
        TRACEN(TRACE_DEBUG, (char*)ctx.proxy_out_flt.addr, ctx.proxy_out_flt.cnt * 2);
    }

    return ret;
}

/**
* Callback function for split_proxy() to send fragment packet via GATT notification
*
* Parameters:
*   hdr:        Header - it is different for diferent bearers (GATT and ADV)
*   hdr_len:    Length of the header
*   data:       Fragment of the splitted message to send
*   data_len:   Length of the fragment of the splitted message to send
*
* Return: None
*
*/
void mesh_core_send_message_over_proxy_connection(const uint8_t* hdr, uint32_t hdr_len, const uint8_t* data, uint32_t data_len)
{
    uint8_t buf[82];
    memcpy(buf, hdr, hdr_len);
    memcpy(&buf[hdr_len], data, data_len);
    TRACE0(TRACE_INFO, "mesh_core_send_message_over_proxy_connection:\n");
    TRACEN(TRACE_INFO, (char*)buf, hdr_len + data_len);
    if (wiced_bt_mesh_core_gatt_send_callback)
        wiced_bt_mesh_core_gatt_send_callback((uint8_t*)buf, hdr_len + data_len);
}

/**
* Processes beacon data
*
* Parameters:
*   p_data:     Beacon data - data of the beacon field of the adv packet
*               <beacon type>(1byte), <beacon data>(>0 bytes)
*   data_len:   Length of the beacon data
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*
*/
wiced_bool_t handle_beacon(const uint8_t* p_data, uint32_t data_len)
{
#ifdef _DEB_BEACON_EXTENDED_TRACE
    TRACE1(TRACE_DEBUG, "handle_beacon: len=%d\n", data_len);
    TRACEN(TRACE_DEBUG, (char*)p_data, data_len);
#endif

    // generic beacon format: BeaconType(1byte) || Data(??)
    // if it is secured network broadcast beacon
    if (data_len > 1 && p_data[0] == MESH_BEACON_TYPE_SECURE_NETWORK)
    {
        return handle_sec_net_broadcast_beacon(&p_data[1], (uint8_t)(data_len - 1));
    }
#ifndef MESH_CONTROLLER
    else if (data_len > 1 && p_data[0] == MESH_BEACON_TYPE_UNPROVISIONED)
    {
        return mesh_handle_unprovisioned_beacon(&p_data[1], (uint8_t)(data_len - 1));
    }
#endif
#ifdef _DEB_BEACON_EXTENDED_TRACE
    TRACE0(TRACE_DEBUG, "handle_beacon: invalid beacon\n");
#endif
    // drop that packet
    return WICED_FALSE;
}

/**
* Handles proxy fragment packets received via GATT and assebles them into message.
*
* Parameters:
*   p_data:     Fragment packet to handle.
*   data_len:   Length of the fragment packet to handle.
*
* Return: one of the MESH_HANDLE_PROV_RES_XXX
*
*/
int handle_proxy_frag(const uint8_t* p_data, uint8_t data_len)
{
    int res = MESH_HANDLE_PROV_RES_FAILED;
    uint8_t sar;

    TRACE3(TRACE_INFO, "handle_proxy_frag: len=%d received_len=%d type=%d\n", data_len, ctx.proxy_data.len, ctx.proxy_data.type);
    TRACEN(TRACE_INFO, (char*)p_data, data_len);

    do
    {
        // first byte with SAR(2bits) || Provisioning_PDU_Type(6bits), and then the actual data. 
        if (data_len < 2)
            break;
        sar = p_data[0] & MESH_SAR_MASK;
        // if it is start of message
        if (sar == MESH_SAR_START || sar == MESH_SAR_COMPLETE)
        {
            ctx.proxy_data.len = 0;
            ctx.proxy_data.type = p_data[0] & MESH_TYPE_MASK;
        }
        // it is not start of message then check current buffer
        else if (ctx.proxy_data.len == 0
            || ctx.proxy_data.type != (p_data[0] & MESH_TYPE_MASK))
            break;
        // make sure data fits into buffer
        if (data_len - 1 + ctx.proxy_data.len > sizeof(ctx.proxy_data.data))
            break;
        // copy data
        memcpy(&ctx.proxy_data.data[ctx.proxy_data.len], p_data + 1, data_len - 1);
        ctx.proxy_data.len += data_len - 1;

        // if it is end of message
        if (sar == MESH_SAR_END || sar == MESH_SAR_COMPLETE)
            res = MESH_HANDLE_PROV_RES_SUCCEEDED;
        else
            res = MESH_HANDLE_PROV_RES_PENDING;
    } while (WICED_FALSE);
    // on error reset buffer
    if (res == MESH_HANDLE_PROV_RES_FAILED)
        ctx.proxy_data.len = 0;

    TRACE3(TRACE_INFO, "handle_proxy_frag: returns %d type=%d received_len=%d\n", res, ctx.proxy_data.type, ctx.proxy_data.len);

    return res;
}

/**
 * \brief Handle packet received by proxy via GATT.
 * \details Application should call that function on each received packet from the GATT data in characteristic of the proxy service.
 *
 * @param[in]   p_data          :Received packet
 * @param[in]   data_len        :Length of the received packet.
 *
 * @return      None
 */
void wiced_bt_mesh_core_proxy_packet(const uint8_t* p_data, uint8_t data_len)
{
    // exit if it is not end of message
    if (MESH_HANDLE_PROV_RES_SUCCEEDED != handle_proxy_frag(p_data, data_len))
        return;

    // Handle proxy configuration messages regardless of the proxy state
    if (ctx.proxy_data.type == PROXY_PDU_TYPE_CONFIG)
    {
        network_layer_handle(ctx.proxy_data.data, ctx.proxy_data.len, BTM_INQ_RES_IGNORE_RSSI, NETWORK_LAYER_MSG_TYPE_PROXY);
        return;
    }

    // If proxy is not enabled then drop other packets(beacons and network PDUs)
    if (node_nv_data.state_gatt_proxy != FND_STATE_PROXY_ON)
    {
        TRACE0(TRACE_INFO, "wiced_bt_mesh_core_proxy_packet: drop GATT proxy packet\n");
        return;
    }

    // handle beacons and network PDUs
    if (ctx.proxy_data.type == PROXY_PDU_TYPE_BEACON)
    {
        //uint64_t time = clock_SystemTimeMicroseconds64();
        handle_beacon(ctx.proxy_data.data, ctx.proxy_data.len);
        //TRACE1(TRACE_DEBUG, "GATT beacon boost %d\n", (uint32_t)(clock_SystemTimeMicroseconds64() - time));
    }
    else if (ctx.proxy_data.type == PROXY_PDU_TYPE_NETWORK)
    {
        //uint64_t time = clock_SystemTimeMicroseconds64();
        network_layer_handle(ctx.proxy_data.data, ctx.proxy_data.len, BTM_INQ_RES_IGNORE_RSSI, NETWORK_LAYER_MSG_TYPE_GATT);
        //TRACE1(TRACE_DEBUG, "GATT net boost %d\n", (uint32_t)(clock_SystemTimeMicroseconds64() - time));
    }
}

/**
* Finds address in the proxy filter
*
* Parameters:
*   addr:       Address to find in the proxy filter
*   p_idx:      output found index of the address in the proxy filter array
*
* Return:   WICED_TRUE - found
*           WICED_FALSE - not found
*
*/
wiced_bool_t mesh_find_filter_addr_(uint16_t addr, uint8_t *p_idx)
{
    uint8_t idx;
    for (idx = 0; idx < ctx.proxy_out_flt.cnt; idx++)
    {
        if (addr == ctx.proxy_out_flt.addr[idx])
        {
            if(p_idx)
                *p_idx = idx;
            break;
        }
    }
    return idx < ctx.proxy_out_flt.cnt ? WICED_TRUE : WICED_FALSE;
}

#ifndef MESH_CONTROLLER
/**
* Sends new message to the mesh. On the device it sends advert and notification to the connected proxy client.
* On Windows control app it should send GATT write
*
* Parameters:
*   dst:                Destination address.
*   msg:                Message to send. Must contain two bytes before that message for possible header.
*   msg_len:            Length of the message to send
*   from_gatt:          WICED_TRUE - message came from GATT
*                       WICED_FALSE - message came from ADV
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:            Mesh event to be used in complete_callback
*
*   Return:     WICED_TRUE if message sent or placed to the buffer
*/
wiced_bool_t mesh_send_message(uint16_t dst, uint8_t* msg, uint8_t msg_len, wiced_bool_t from_gatt,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    wiced_bool_t ret = WICED_FALSE;
    TRACE2(TRACE_INFO, "mesh_send_message: dst:%x connected_dev_id:%x\n", dst, ctx.connected_dev_id);
    
    // dst == MESH_NODE_ID_INVALID means proxy configuration
    if (dst == MESH_NODE_ID_INVALID)
    {
        split_proxy(PROXY_PDU_TYPE_CONFIG, msg, msg_len, wiced_bt_mesh_core_gatt_send_max_len);
        return WICED_TRUE;
    }
    
    // send advert if destination is not device to which we are the proxy, or if we connected to a proxy
    if ((dst != ctx.connected_dev_id) && !ctx.connected_to_proxy)
    {
        msg[-2] = (uint8_t)(msg_len + 1);
        msg[-1] = ADV_TYPE_MESH;
        ret = send_adv(msg - 2, msg_len + 2, complete_callback, p_event);
    }
    else
    {
        ret = WICED_TRUE;
    }

    // send this message via gatt notifications if appropriate
    if (can_send_notification(from_gatt, dst))
    {
        // If proxy is enabled then send that packet to proxy if can
        if (node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)
        {
            split_proxy(PROXY_PDU_TYPE_NETWORK, msg, msg_len, wiced_bt_mesh_core_gatt_send_max_len);
        }
        else
        {
            TRACE1(TRACE_INFO, "mesh_send_message: don't send to GATT. state_gatt_proxy:%d\n", node_nv_data.state_gatt_proxy);
        }
    }
    return ret;
}
#endif

#ifdef MESH_CONTROLLER
/**
* Creates node reading all info from persistent memory(registry for windows controller)
*
* Parameters:
*   mesh_id:    Node address. NULL mesh_id means reset node.
*
* Return:   None
*
*/
void mesh_controller_create_node(uint8_t *mesh_id)
{
    uint32_t len;
    int    key_idx;

    // Reset the node nv data
    mesh_clear_node_nv_data(WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER);

    ecdh_init_curve();

    if (mesh_id == NULL)
    {
        return;
    }

    len = mesh_controller_read_node_info(mesh_id, 0, (uint8_t*) &node_nv_data, sizeof(MeshNodeNvData));
    if (len != sizeof(MeshNodeNvData))
    {
        mesh_clear_node_nv_data(WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER);
        TRACE1(TRACE_INFO, "controller_create_node: main read failed, len = %d\n", len);
        return;
    }
    for (key_idx = 0; key_idx < MESH_APP_KEY_MAX_NUM; key_idx++)
    {
        if (node_nv_data.app_key_bitmask & (1 << key_idx))
        {
            len = mesh_controller_read_node_info(mesh_id, 1 + key_idx, (uint8_t*)&node_nv_app_key[key_idx], sizeof(MeshNvAppKey));
            if (len != sizeof(MeshNvAppKey))
            {
                node_nv_data.node_id = MESH_NODE_ID_INVALID;
                TRACE1(TRACE_INFO, "controller_create_node: app key read failed, len = %d\n", len);
                return;
            }
        }
    }

    for (key_idx = 0; key_idx < MESH_NET_KEY_MAX_NUM; key_idx++)
    {
        if (node_nv_data.net_key_bitmask & (1 << key_idx))
        {
            len = mesh_controller_read_node_info(mesh_id, 1 + MESH_APP_KEY_MAX_NUM + key_idx, (uint8_t*)&node_nv_net_key[key_idx], sizeof(MeshNvNetKey));
            if (len != sizeof(MeshNvNetKey))
            {
                node_nv_data.node_id = MESH_NODE_ID_INVALID;
                TRACE1(TRACE_INFO, "controller_create_node: net key read failed, len = %d\n", len);
                return;
            }
        }
    }

    TRACE3(TRACE_INFO, "create_node: Initialized from NVRAM app_key_bitmask:%x net_key_bitmask:%x node_id:%x\n", node_nv_data.app_key_bitmask, node_nv_data.net_key_bitmask, node_nv_data.node_id);
    TRACE1(TRACE_INFO, " net_iv_index:%x\n", BE4TOUINT32(node_nv_data.net_iv_index));
    TRACE1(TRACE_INFO, " SEQ:%x iv_index, dev_key:\n", node_nv_data.sequence_number);
    TRACEN(TRACE_INFO, (char*)node_nv_data.dev_key, MESH_KEY_LEN);

    for (key_idx = 0; key_idx < MESH_APP_KEY_MAX_NUM; key_idx++)
    {
        if (node_nv_data.app_key_bitmask & (1 << key_idx))
        {
            TRACE3(TRACE_INFO, "create_node: idx:%d aid:%x global_idx:%d\n", key_idx, node_nv_app_key[key_idx].aid, node_nv_app_key[key_idx].global_idx);
            TRACE1(TRACE_INFO, " global_net_key_idx:%d app_key:\n", node_nv_app_key[key_idx].global_net_key_idx);
            TRACEN(TRACE_INFO, (char*)node_nv_app_key[key_idx].key, MESH_KEY_LEN);
        }
    }
    for (key_idx = 0; key_idx < MESH_NET_KEY_MAX_NUM; key_idx++)
    {
        if (node_nv_data.net_key_bitmask & (1 << key_idx))
        {
            TRACE3(TRACE_INFO, "create_node: idx:%d nid:%x global_idx:%x net_key, encr_key, privacy_key, network_id, identity_key, beacon_key, proxy_key:\n", key_idx, node_nv_net_key[key_idx].sec_material.nid, node_nv_net_key[key_idx].global_idx);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].sec_material.encr_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].sec_material.privacy_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].network_id, MESH_NETWORK_ID_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].identity_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].beacon_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[key_idx].proxy_key, MESH_KEY_LEN);
        }
    }
}
#endif

#ifndef MESH_CONTROLLER
/**
 * Mesh core library function to start mesh
 *
 * This function is to start the mesh function using the specified parameters.
 *
 * Parameters:  None
 *
 * Return:
 *     Returns the result code.
 *
 */
MeshResultCode mesh_start_mesh(void)
{
    int i;

    TRACE1(TRACE_INFO, "start_mesh: node_id = %x\n", node_nv_data.node_id);

    auth_cache_init();

    ctx.mesh_started = WICED_TRUE;

    // Update proxy server advertisement and network secure beacon
    mesh_update_beacon();

    return MESH_RESULT_SUCCESS;
}

#endif

/**
* Splits proxy message to send via GATT and calls callback cb for each fragment.
*
* Parameters:
*   type:       Type of the message. It can be any of PROXY_PDU_TYPE_XXX.
*   msg:        Message to split
*   msg_len:    Length of the message to split
*   max_len:    Maximum length of the resulting fragment packets.
*   cb:         Callback function to call for each fragment packet.
*
* Return: None
*
*/
void split_proxy(uint8_t type, const uint8_t* msg, uint32_t msg_len, uint32_t max_len)
{
    uint32_t sent_len = 0;
    uint32_t to_send;
    uint8_t hdr;
    TRACE3(TRACE_INFO, "split_proxy: type:%d msg_len:%d max_len:%d\n", type, msg_len, max_len);
    while (msg_len > sent_len)
    {
        if (msg_len - sent_len <= max_len - 1)
        {
            to_send = (uint8_t)(msg_len - sent_len);
            if (sent_len == 0)
                hdr = type | MESH_SAR_COMPLETE;
            else
                hdr = type | MESH_SAR_END;
        }
        else
        {
            to_send = max_len - 1;
            if (sent_len == 0)
                hdr = type | MESH_SAR_START;
            else
                hdr = type | MESH_SAR_CONT;
        }
        if(type != PROXY_PDU_TYPE_PROVIS)
            mesh_core_send_message_over_proxy_connection(&hdr, 1, &msg[sent_len], to_send);
        else
            pb_transport_gatt_split_cb(&hdr, 1, &msg[sent_len], to_send);

        TRACE1(TRACE_INFO, "hdr:%02x\n", hdr);
        TRACEN(TRACE_INFO, (char*)&msg[sent_len], to_send);
        sent_len += to_send;
    }
}

#ifdef MESH_CONTROLLER
/**
* Sets value of the seq number for testing
*
* Parameters:
*   seq:    seq number to set.
*
* Return:   returns result code
*
*/
MeshResultCode mesh_set_sequence_number(uint32_t seq)
{
    node_nv_data.sequence_number = seq;
#ifndef MESH_CONTROLLER
    mesh_save_node_nv_data();
#endif
    return MESH_RESULT_SUCCESS;
}

/**
* Gets value of the seq number for testing
*
* Parameters:   None
*
* Return:   seq number
*
*/
uint32_t mesh_get_sequence_number()
{
    return node_nv_data.sequence_number;
}

/**
* Retrives node address
*
* Parameters:
*   p_data:     Buffer for retrived address.
*
* Return:   returns result code
*
*/
MeshResultCode mesh_get_dev_addr(uint8_t* p_data)
{
    p_data[0] = (node_nv_data.node_id >> 8) & 0xff;
    p_data[1] = node_nv_data.node_id & 0xff;
    return MESH_RESULT_SUCCESS;
}

/**
* Retrives IV INDEX
*
* Parameters:
*   iv_index:     Buffer for retrived IV INDEX.
*
* Return:   returns result code
*
*/
extern MeshResultCode mesh_get_iv_index(uint8_t* iv_index)
{
    memcpy(iv_index, node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);
    return MESH_RESULT_SUCCESS;
}

/**
* Retrives first application key
*
* Parameters:
*   p_data:     Buffer for retrived key.
*
* Return:   returns result code
*
*/
MeshResultCode mesh_get_app_info(uint8_t* p_data)
{
    memcpy(p_data, &node_nv_app_key[0], sizeof(node_nv_app_key[0]));
    return MESH_RESULT_SUCCESS;
}

MeshResultCode mesh_get_dev_key(uint8_t* key)
{
    memcpy(key, node_nv_data.dev_key, MESH_KEY_LEN);
    return MESH_RESULT_SUCCESS;
}


/**
* Retrives first network key
*
* Parameters:
*   p_data:     Buffer for retrived key.
*
* Return:   returns result code
*
*/
MeshResultCode mesh_get_net_info(uint8_t* p_data)
{
    memcpy(p_data, &node_nv_net_key[0], sizeof(node_nv_net_key[0]));
    return MESH_RESULT_SUCCESS;
}

static uint8_t _test_provisioner_priv_key[] = {
    0x06, 0xa5, 0x16, 0x69, 0x3c, 0x9a, 0xa3, 0x1a,
    0x60, 0x84, 0x54, 0x5d, 0x0c, 0x5d, 0xb6, 0x41,
    0xb4, 0x85, 0x72, 0xb9, 0x72, 0x03, 0xdd, 0xff,
    0xb7, 0xac, 0x73, 0xf7, 0xd0, 0x45, 0x76, 0x63 };
static uint8_t _test_provisioner_pub_key[] = {
    0x2c, 0x31, 0xa4, 0x7b, 0x57, 0x79, 0x80, 0x9e, 0xf4, 0x4c, 0xb5, 0xea, 0xaf, 0x5c, 0x3e, 0x43,
    0xd5, 0xf8, 0xfa, 0xad, 0x4a, 0x87, 0x94, 0xcb, 0x98, 0x7e, 0x9b, 0x03, 0x74, 0x5c, 0x78, 0xdd,
    0x91, 0x95, 0x12, 0x18, 0x38, 0x98, 0xdf, 0xbe, 0xcd, 0x52, 0xe2, 0x40, 0x8e, 0x43, 0x87, 0x1f,
    0xd0, 0x21, 0x10, 0x91, 0x17, 0xbd, 0x3e, 0xd4, 0xea, 0xf8, 0x43, 0x77, 0x43, 0x71, 0x5d, 0x4f
};
static uint8_t _test_dev_priv_key[] = {
    0x52, 0x9a, 0xa0, 0x67, 0x0d, 0x72, 0xcd, 0x64,
    0x97, 0x50, 0x2e, 0xd4, 0x73, 0x50, 0x2b, 0x03,
    0x7e, 0x88, 0x03, 0xb5, 0xc6, 0x08, 0x29, 0xa5,
    0xa3, 0xca, 0xa2, 0x19, 0x50, 0x55, 0x30, 0xba
};
static uint8_t _test_dev_pub_key[] = {
    0xf4, 0x65, 0xe4, 0x3f, 0xf2, 0x3d, 0x3f, 0x1b, 0x9d, 0xc7, 0xdf, 0xc0, 0x4d, 0xa8, 0x75, 0x81,
    0x84, 0xdb, 0xc9, 0x66, 0x20, 0x47, 0x96, 0xec, 0xcf, 0x0d, 0x6c, 0xf5, 0xe1, 0x65, 0x00, 0xcc,
    0x02, 0x01, 0xd0, 0x48, 0xbc, 0xbb, 0xd8, 0x99, 0xee, 0xef, 0xc4, 0x24, 0x16, 0x4e, 0x33, 0xc2,
    0x01, 0xc2, 0xb0, 0x10, 0xca, 0x6b, 0x4d, 0x43, 0xa8, 0xa1, 0x55, 0xca, 0xd8, 0xec, 0xb2, 0x79
};

#if 0
void mesh_test_keys()
{
    uint8_t ecdh_secret[WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN];
    ecdh_calc_secret(_test_provisioner_pub_key, _test_dev_priv_key, ecdh_secret);
    TRACE0("_test_provisioner_pub_key:\n");
    TRACEN((char*)_test_provisioner_pub_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2);
    TRACEN((char*)&_test_provisioner_pub_key[WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2], WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2);
    TRACE0("_test_dev_priv_key:\n");
    TRACEN((char*)_test_dev_priv_key, WICED_BT_MESH_PROVISION_PRIV_KEY_LEN);
    TRACE0("ecdh_secret:\n");
    TRACEN((char*)ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN);

    ecdh_calc_secret(_test_dev_pub_key, _test_provisioner_priv_key, ecdh_secret);
    TRACE0("_test_dev_pub_key:\n");
    TRACEN((char*)_test_dev_pub_key, WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2);
    TRACEN((char*)&_test_dev_pub_key[WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2], WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN / 2);
    TRACE0("_test_provisioner_priv_key:\n");
    TRACEN((char*)_test_provisioner_priv_key, WICED_BT_MESH_PROVISION_PRIV_KEY_LEN);
    TRACE0("ecdh_secret:\n");
    TRACEN((char*)ecdh_secret, WICED_BT_MESH_PROVISION_ECDH_SECRET_LEN);
}
#endif
#endif

extern uint32_t rbg_rand(void);

/**
* Generates random number.
*
* Parameters:
*   random:     Buffer for generated random.
*   len:        Length of the random number to generate.
*
* Return: None
*
*/
void wiced_bt_mesh__generate_random(uint8_t* random, uint8_t len)
{
    uint32_t r;
    uint8_t l;
    while (len)
    {
        r = rbg_rand();
        l = len > 4 ? 4 : len;
        memcpy(random, &r, l);
        len -= l;
        random += l;
    }
}

#ifndef MESH_CONTROLLER
/**
* Saves application cache to the NVRAM
*
* Parameters:
*   prev_iv_idx:    WICED_TRUE - cache for previous IV INDEX; WICED_FALSE - cache for current IV INDEX;
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - error.
*
*/
wiced_bool_t app_cache_save(wiced_bool_t prev_iv_idx)
{
    uint32_t len;
    len = mesh_write_node_info(prev_iv_idx ? MESH_NVM_IDX_APP_CACHE_PREV : MESH_NVM_IDX_APP_CACHE,
        prev_iv_idx ? (uint8_t*)&ctx.app_cache_prev[0] : (uint8_t*)&ctx.app_cache[0], sizeof(ctx.app_cache), NULL);
    if (len == sizeof(ctx.app_cache))
    {
        return WICED_TRUE;
    }
    TRACE2(TRACE_INFO, "app_cache_save: write failed: prev_iv_idx:%d len:%d\n", prev_iv_idx, len);
    return WICED_FALSE;
}

/**
* Clears application cache in the NVRAM
*
* Parameters:
*   prev_iv_idx:    WICED_TRUE - cache for previous IV INDEX; WICED_FALSE - cache for current IV INDEX;
*
* Return: WICED_TRUE - success. WICED_FALSE - error.
*
*/
wiced_bool_t app_cache_clear(wiced_bool_t prev_iv_idx)
{
    memset(prev_iv_idx ? (uint8_t*)&ctx.app_cache_prev[0] : (uint8_t*)&ctx.app_cache[0], 0, sizeof(ctx.app_cache));
    return app_cache_save(prev_iv_idx);
}

#endif

/**
* Finds net key in the local NVRAM array by its global index and returns local index of that key
*
* Parameters:
*   global_idx:     Global index of the network key including flag MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG. 
*                   0xffff value of global_idx means request to find available index for new key
*   p_idx:          pointer to variable to receive local index of that key
*
* Return: WICED_TRUE - success. WICED_FALSE - error.
*
*/
wiced_bool_t find_netkey_(uint16_t global_idx, uint8_t *p_idx)
{
    uint8_t idx;
    for (idx = 0; idx < MESH_NET_KEY_MAX_NUM; idx++)
    {
        if (node_nv_data.net_key_bitmask & (1 << idx))
        {
            if ((node_nv_net_key[idx].global_idx & (MESH_GL_KEY_IDX_MASK | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG)) == global_idx)
            {
                *p_idx = idx;
                break;
            }
        }
        else if (global_idx == 0xffff)
        {
            *p_idx = idx;
            break;
        }
    }
    return idx < MESH_NET_KEY_MAX_NUM;
}

/**
* Local function to save node network keys in the NVRAM
*
* Parameters:
*   key_inx:    network key index to save
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - error.
*
*/
wiced_bool_t saveNodeNvNetKey(int key_inx)
{
    uint32_t len;

#ifdef MESH_CONTROLLER
    len = mesh_controller_write_node_info(node_nv_app_key[0].key, 1 + MESH_APP_KEY_MAX_NUM + key_inx, (uint8_t*)&node_nv_net_key[key_inx], sizeof(MeshNvNetKey));
#else
    len = mesh_write_node_info(MESH_NVM_IDX_NET_KEY_BEGIN + key_inx, (uint8_t*)&node_nv_net_key[key_inx], sizeof(MeshNvNetKey), NULL);
#endif
    if (len == sizeof(MeshNvNetKey))
    {
        return WICED_TRUE;
    }

    TRACE1(TRACE_INFO, "saveNodeNvNetKey failed: len = %d\n", len);

    return WICED_FALSE;
}

/**
* Local function to save node application keys in the NVRAM
*
* Parameters:
*   key_inx:    application key index to save
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - error.
*
*/
wiced_bool_t saveNodeNvAppKey(int key_inx)
{
    uint32_t len;

#ifdef MESH_CONTROLLER
    len = mesh_controller_write_node_info(node_nv_app_key[0].key, 1 + key_inx, (uint8_t*)&node_nv_app_key[key_inx], sizeof(MeshNvAppKey));
#else
    len = mesh_write_node_info(MESH_NVM_IDX_APP_KEY_BEGIN + key_inx, (uint8_t*)&node_nv_app_key[key_inx], sizeof(MeshNvAppKey), NULL);
#endif
    if (len == sizeof(MeshNvAppKey))
    {
        return WICED_TRUE;
    }
    TRACE1(TRACE_INFO, "saveNodeNvAppKey failed: len = %d\n", len);

    return WICED_FALSE;
}

#ifndef MESH_CORE_DONT_USE_TRACE_ENCODER
void mesh_trace(uint32_t fid, uint32_t line, uint32_t var)
{
#ifdef MESH_TRACE_TIME
    uint32_t time = (uint32_t)wiced_bt_mesh_core_get_tick_count();
    wiced_printf(NULL, 0, "~@%x %x %x %x\n", fid, line, var, time);
#else
    wiced_printf(NULL, 0, "~@%x %x %x\n", fid, line, var);
#endif
}
#else
#ifdef MESH_TRACE_TIME
void mesh_trace_time(const char * str)
{
    uint32_t time = (uint32_t)wiced_bt_mesh_core_get_tick_count();
    wiced_printf(NULL, 0, " %03d.%03d ", (time / 1000) % 1000, time % 1000);
}
#endif
#endif

/**
* \brief Handles received advertising packet.
* \details Application should call that function on each received advertisment packet.
*
* @param[in]   rssi             :RSSI of the received packet
* @param[in]   p_adv_data       :Advertisment packet
*
* @return      wiced_result_t
*/
wiced_result_t wiced_bt_mesh_core_adv_packet(uint8_t rssi, const uint8_t *p_adv_data)
{
    wiced_result_t  res = WICED_BT_ERROR;
#ifndef MESH_CONTROLLER
    uint8_t         *p_data;
    uint8_t         length;

    // handle provisioning packet
    if (NULL != (p_data = wiced_bt_ble_check_advertising_data((uint8_t*)p_adv_data, ADV_TYPE_PB_ADV_TYPE, &length)))
    {
        pb_adv_handle_packet(p_data, length);
        res = WICED_BT_SUCCESS;
    }
    // handle beacons
    else if (NULL != (p_data = wiced_bt_ble_check_advertising_data((uint8_t*)p_adv_data, ADV_TYPE_MESH_BEACON, &length)))
    {
        //uint64_t time = clock_SystemTimeMicroseconds64();
        handle_beacon(p_data, length);
        //TRACE1(TRACE_DEBUG, "ADV beacon boost %d\n", (uint32_t)(clock_SystemTimeMicroseconds64() - time));
        res = WICED_BT_SUCCESS;
    }
    // handle mesh packets and beacons only in provisioned state
    else if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
    {
        if (NULL != (p_data = wiced_bt_ble_check_advertising_data((uint8_t*)p_adv_data, ADV_TYPE_MESH, &length)))
        {
            //uint64_t time = clock_SystemTimeMicroseconds64();
            network_layer_handle(p_data, length, rssi, NETWORK_LAYER_MSG_TYPE_ADV);
            //TRACE1(TRACE_DEBUG, "ADV net boost %d\n", (uint32_t)(clock_SystemTimeMicroseconds64() - time));
            res = WICED_BT_SUCCESS;
        }
    }
#endif
    return res;
}

/**
 * \brief Sets GATT MTU.
 * \details Called by app to set max sending packet length in the core and provisioning layer.
 *
 * @param[in]   gatt_send_max_len       :Maximum length of the packet sending over GATT by send_cb(). 0 means default value(20)
 */
void wiced_bt_mesh_core_set_gatt_mtu(uint16_t gatt_mtu)
{
    wiced_bt_mesh_core_gatt_send_max_len = gatt_mtu - 3;
    if (wiced_bt_mesh_core_gatt_send_max_len > MESH_MAX_GATT_NOTIFICATION_LEN)
        wiced_bt_mesh_core_gatt_send_max_len = MESH_MAX_GATT_NOTIFICATION_LEN;
}

void mesh_ota_firmware_upgrade_status_callback(uint8_t status)
{
    TRACE1(TRACE_INFO, "mesh_ota_firmware_upgrade_status_callback: status:%d\n", status);
}

/**
 * \brief Mesh Core initialization or Reset.
 * \details This function should be called at device startup before calling wiced_bt_mesh_core_register() for each supported model
 * It also can be called at any time with NULL p_config_data to reset device
 *
 * @param[in]   p_config_data   :Configuration data. NULL means reset device
 * @param[in]   callback        :Callback function to be called by the Core at received message subscribed by any Model
 * @param[in]   device_uuid                         :128-bit Device UUID. Device manufacturers shall follow the standard UUID format
 *                                                  and generation procedure to ensure the uniqueness of each Device UUID
 * @param[in]   gatt_send_callback                  :Callback function to send proxy packet ofer GATT
 * @param[in]   fault_test_cb                       :Callback function to be called to invoke a self test procedure of an Element
 * @param[in]   attention_cb                        :Callback function to be called to attract human attention
 * @param[out]  mesh_proxy_enabled                  :WICED_TRUE if Proxy is enabled. Otherwise WICED_FALSE.
 *
 * @return      wiced_result_t: WICED_BT_SUCCESS means device provisioned. Otherwise it is not provisioned.
 */
wiced_result_t wiced_bt_mesh_core_init(wiced_bt_mesh_core_config_t *p_config_data, wiced_bt_mesh_core_received_msg_callback_t callback,
    uint8_t *device_uuid, wiced_bt_mesh_core_proxy_send_cb_t gatt_send_callback, wiced_bt_core_nvram_access_t nvram_access_callback,
    wiced_bt_mesh_core_health_fault_test_cb_t fault_test_cb, wiced_bt_mesh_core_attention_cb_t attention_cb, wiced_bool_t *mesh_proxy_enabled)
{
    wiced_bool_t    node_provisioned = WICED_FALSE;;
    wiced_result_t  res = WICED_BT_SUCCESS;

    TRACE2(TRACE_INFO, "wiced_bt_mesh_core_init: p_config_data!=NULL:%d features:%x device_uuid:\n", p_config_data != NULL ? 1 : 0, p_config_data != NULL ? p_config_data->features : fnd_static_config_.features);
    if(mesh_proxy_enabled)
        *mesh_proxy_enabled = WICED_FALSE;
    if (device_uuid)
        memcpy(node_uuid, device_uuid, MESH_DEVICE_UUID_LEN);
#ifdef HARDCODED_UUID
    uint8_t hardcoded_uuid[MESH_DEVICE_UUID_LEN] = { HARDCODED_UUID };
    memcpy(node_uuid, hardcoded_uuid, MESH_DEVICE_UUID_LEN);
#endif

    TRACEN(TRACE_INFO, (char*)node_uuid, sizeof(node_uuid));

    // If it is request to reset device
    if (p_config_data == NULL)
    {
#ifndef MESH_CONTROLLER
        mesh_stop_mesh();
        mesh_create_node(WICED_TRUE, NULL, fnd_static_config_.features);
        mesh_discovery_start();
#ifndef _DEB_DONT_REBOOT_AFTER_RESET
        // reboot with delay 1 sec
        mesh_core_reboot(1000);
#endif
#endif
        // return WICED_BT_ERROR as a sign of unprovisioned device
        return WICED_BT_ERROR;
    }

#ifndef MESH_CONTROLLER
    TRACE2(TRACE_INFO, " stack_size:%x stack_pointer:%x\n", mesh_core_get_stack_size(), mesh_core_get_current_stack_pointer());
#endif

    wiced_init_timer(&mesh_core_app_timer, mesh_core_app_timer_callback, 0, WICED_SECONDS_PERIODIC_TIMER);
    wiced_start_timer(&mesh_core_app_timer, 1);

#if MESH_CHIP == 20703
    // Initialize the RTC calling wiced_bt_mesh_core_rtc_init() defined in app. It just should call rtc_init().
    // For that chip rtc_init should be called from application. Otherwise there is build error at link phase
    wiced_bt_mesh_core_rtc_init();
#endif

    wiced_bt_mesh_core_gatt_send_callback = gatt_send_callback;
    wiced_bt_mesh_core_nvram_access_callback = nvram_access_callback;

#ifndef MESH_CONTROLLER
    // create node will read information from the NVRAM
    mesh_create_node(WICED_FALSE, &node_provisioned, p_config_data->features);
    ctx.tick_count_init_value = clock_SystemTimeMicroseconds64() / 1000;
#endif

    // If Low Power Feature is supported
    if ((p_config_data->features & WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER) != 0)
    {
        if (!low_power_init(&p_config_data->low_power))
            return WICED_BT_ERROR;
    }

    // If Friend Feature is supported
    if ((p_config_data->features & WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND) != 0)
    {
        if(!friend_init(&p_config_data->friend_cfg))
            return WICED_BT_ERROR;
    }

    if (!foundation_init(p_config_data, callback, fault_test_cb, attention_cb))
        return WICED_BT_ERROR;

#ifndef MESH_CONTROLLER
    mesh_util_init();

    if (!node_provisioned)
        res = WICED_BT_ERROR;
#endif

    // on success (if are provisioned) then start mesh. Otherwise start unprovisioned advertising
    if (res == WICED_BT_SUCCESS)
    {
#ifndef MESH_CONTROLLER
        // Start the mesh network
        mesh_start_mesh();
#endif

        if (ctx.conn_id)
            wiced_bt_mesh_core_connection_status(ctx.conn_id);
#ifndef MESH_CONTROLLER
        // Initialize OTA FW upgrade
        if(!wiced_bt_mesh_core_ota_fw_upgrade_init(mesh_ota_firmware_upgrade_status_callback))
        {
            TRACE0(TRACE_WARNING, "wiced_bt_mesh_core_init: wiced_bt_mesh_core_ota_fw_upgrade_init failed\n");
        }
#endif
    }
#ifndef MESH_CONTROLLER
    else
        mesh_discovery_start();
#endif

#ifndef MESH_CONTROLLER
    // start scan. In nonprovisioned state we need it to be provisioned via PB_ADV
    mesh_scan(WICED_TRUE);
#endif
    if (res == WICED_BT_SUCCESS)
    {
        if (mesh_proxy_enabled)
            *mesh_proxy_enabled = node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON ? WICED_TRUE : WICED_FALSE;
    }
    TRACE2(TRACE_INFO, "wiced_bt_mesh_core_init: returns:%d state_gatt_proxy:%d\n", res,  node_nv_data.state_gatt_proxy);
    return res;
}

/**
 * Stops proxy server advertisement and network secure beacon if they are running
 *
 * Parameters:   None
 *
 * Return:   None
 */
void mesh_stop_advert(void)
{
    uint8_t     net_key_idx;
    // go through all network keys
    for (net_key_idx = 0; net_key_idx < MESH_NET_KEY_MAX_NUM; net_key_idx++)
    {
        // ignore unexisted key index
        if (0 == (node_nv_data.net_key_bitmask & (1 << net_key_idx)))
            continue;
        // ignore not started advertisings
        if (ctx.proxy_adv_instance[net_key_idx] != 0xff)
        {
#ifndef MESH_CONTROLLER
            mesh_stop_adv(ctx.proxy_adv_instance[net_key_idx]);
#endif
            ctx.proxy_adv_instance[net_key_idx] = 0xff;
        }
        if (ctx.beacon_instance[net_key_idx] != 0xff)
        {
#ifndef MESH_CONTROLLER
            mesh_stop_adv(ctx.beacon_instance[net_key_idx]);
#endif
            ctx.beacon_instance[net_key_idx] = 0xff;
        }
    }
}

/**
 * \brief Handles connection up and down events.
 * \details Application should call that function on each connection up and down state. Only one connection is alowed.
 *
 * @param[in]   conn_id         :Connection ID if connected. 0 if disconnected
 *
 * @return      None
 */
void wiced_bt_mesh_core_connection_status(uint16_t conn_id)
{
    TRACE4(TRACE_INFO, "wiced_bt_mesh_core_connection_status: conn_id:%d prev_conn_id:%x mesh_started:%d beacon_instance:%x\n", conn_id, ctx.conn_id, ctx.mesh_started, ctx.beacon_instance[0]);
    ctx.connected_dev_id = MESH_NODE_ID_INVALID;
    //if ((conn_id && ctx.conn_id) || (!conn_id && !ctx.conn_id))
    //    return;
    ctx.conn_id = conn_id;

    // Stops proxy server advertisement and network secure beacon if they are running
    mesh_stop_advert();

#ifndef MESH_CONTROLLER
    if (!ctx.mesh_started)
    {
        mesh_discovery_connection_state(conn_id);
        return;
    }

    // initialize the proxy filter as a white list filter and empty it.
    ctx.proxy_out_flt.type = MESH_FLT_TYPE_WHITE_LIST;
    ctx.proxy_out_flt.cnt = 0;

    if (ctx.mesh_started)
    {
        if (conn_id)

            // Notify the proxy client with delay 1-2 seconds - otherwise client loses it
            delayed_beacon_cnt = 2;
        else
        {
            // Device is idle
            mesh_update_beacon();
        }
    }
    // Pass connection up/down event to the OTA FW upgrade library
    {
        wiced_bt_gatt_connection_status_t connection_status = { 0 };
        connection_status.connected = conn_id ? WICED_TRUE : WICED_FALSE;
        connection_status.conn_id = conn_id;
        wiced_bt_mesh_core_ota_fw_upgrade_connection_status_event(&connection_status);
    }
#endif
}

/**
* \brief Handles application timer.
* \details Application should call that function on each app timer tick and each fine timer tick.
*
* @return      None
*/
static void mesh_core_app_timer_callback(uint32_t arg)
{
    wiced_bool_t    need_update_adv = WICED_FALSE;

    if ((ctx.app_timer_cntr & 0xF) == 0)
    {
        TRACE2(TRACE_INFO, "mesh_core_app_timer_callback: mesh_started:%d count:%d\n", ctx.mesh_started, ctx.app_timer_cntr);
    }
    ctx.app_timer_cntr++;

    //Check provisioning transport timeouts and make needed actions.
    pb_transport_timer();

    if (!ctx.mesh_started)
    {
#ifndef MESH_CONTROLLER
#ifdef HARDCODED_PROVIS
        if (ctx.app_timer_cntr == 2)
        {
#if MESH_FRIENDSHIP == 2 //MESH_FRIENDSHIP_LPN
            uint16_t    addr = 0x00bb;
#else
            uint16_t    addr = 0x1201;
#endif
            //uint8_t     net_key[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2 };
            uint8_t     net_key[] = { 0x7d, 0xd7, 0x36, 0x4c, 0xd8, 0x42, 0xad, 0x18, 0xc1, 0x7c, 0x2b, 0x82, 0x0c, 0x84, 0xc3, 0xd0 };
#ifdef HARDCODED_DEV_KEY
            uint8_t     dev_key[] = { HARDCODED_DEV_KEY };
#else
            uint8_t     dev_key[] = { 0x9d, 0x6d, 0xd0, 0xe9, 0x6e, 0xb2, 0x5d, 0xc1, 0x9a, 0x40, 0xed, 0x99, 0x14, 0xf8, 0xf0, 0x3f };
            //uint8_t     dev_key[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3 };
#endif
            uint8_t     app_key[] = { 0x63, 0x96, 0x47, 0x71, 0x73, 0x4f, 0xbd, 0x76, 0xe3, 0xb4, 0x05, 0x19, 0xd1, 0xd9, 0x4a, 0x48 };
            //uint8_t     app_key[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4 };
            uint32_t    iv_index = 0x12345678;
            uint8_t     element = 0;
            uint16_t    model_id = WICED_BT_MESH_CORE_MODEL_ID_HEALTH_SRV;
            uint16_t    subs_addr = 0xc001;
            mesh_core_hardcoded_prov(addr, net_key, dev_key, iv_index, app_key, element, model_id, subs_addr);
            mesh_discovery_stop();
            mesh_start_mesh();
#ifndef _DEB_DONT_REBOOT_AFTER_PROVIS
            mesh_core_reboot(0);
#endif
            return;
        }
#endif
        mesh_discovery_app_timer();
        if (need_update_adv)
            mesh_discovery_restart();
#endif
        return;
    }

#ifndef MESH_CONTROLLER
    if (delayed_beacon_cnt != 0 && --delayed_beacon_cnt == 0 && ctx.conn_id)
    {
        need_update_adv = WICED_TRUE;
    }
#endif
#ifndef MESH_CONTROLLER
    key_refresh_timer();
    // Update proxy server advertisement and network secure beacon if if IV index is changed
    if (iv_updt_timer())
        need_update_adv = WICED_TRUE;
    ivi_recovery_timer();
    if (need_update_adv)
    {
        mesh_update_beacon();
    }
    health_timer();
#endif
}

#ifndef MESH_CONTROLLER
/**
* Reads data from NVRAM
*
* Parameters:
*     id:        Index of the memory chunk to save
*     data:      The node info buffer
*     len:       The buffer length
*     p_result:  Pointer to variable to reseive the result of operation
*
* Return:
*     Return the number of bytes filled into the data buff
*/
uint32_t mesh_read_node_info(int id, uint8_t* data, uint16_t len, wiced_result_t *p_result)
{
    uint32_t ret = 0;
    wiced_result_t result;
    if (p_result == NULL)
        p_result = &result;
    if(wiced_bt_mesh_core_nvram_access_callback)
        ret = wiced_bt_mesh_core_nvram_access_callback(WICED_FALSE, id, data, len, p_result);
    TRACE4(TRACE_INFO, "read_node_info: id:%d result:%x len:%d / %d\n", id, *p_result, ret, len);
    return ret;
}

/**
* Reads data from NVRAM splitted into blocks mwith max 255 bytes len
*
* Parameters:
*     id:        Index of the memory chunk to save
*     data:      The node info buffer
*     len:       The buffer length
*     p_result:  Pointer to variable to reseive the result of operation
*
* Return:
*     Return the number of bytes filled into the data buff
*/
uint32_t mesh_read_node_info2(int id, uint8_t* data, uint16_t len, wiced_result_t *p_result)
{
    uint32_t        ret = 0;
    uint8_t         *p;
    uint16_t        len_to_read, len_rest;
    uint8_t         idx;
    wiced_result_t  result = 0;
    for (idx = 0, len_rest = len, p = (uint8_t*)data; len_rest > 0; idx++, len_rest -= len_to_read, p += len_to_read)
    {
        len_to_read = len_rest > 0xff ? 0xff : len_rest;
        if (len_to_read != mesh_read_node_info(id + idx, p, len_to_read, &result))
            break;
        ret += len_to_read;
    }
    if (p_result)
        *p_result = result;
    return ret;
}

/**
* Writes the information into NVRAM
*
* Parameters:
*     id:        Index of the memory chunk to save
*     data:      Data to write
*     len:       Data length. 0 means delete
*     p_result:  Pointer to variable to reseive the result of operation
*
* Return:
*     Return the number of bytes written into NVRAM
*/
uint32_t mesh_write_node_info(int id, const uint8_t* data, uint16_t len, wiced_result_t *p_result)
{
    uint32_t ret = 0;
    wiced_result_t result;
    if (p_result == NULL)
        p_result = &result;
    if (wiced_bt_mesh_core_nvram_access_callback)
        ret = wiced_bt_mesh_core_nvram_access_callback(WICED_TRUE, id, (uint8_t*)data, len, p_result);
    TRACE4(TRACE_INFO, "write_node_info: id:%d result:%x len:%d / %d\n", id, *p_result, ret, len);
    //TRACEN(TRACE_DEBUG, (char*)data, len);
    return ret;
}

/**
* Writes the information into NVRAM splitting data into blocks mwith max 255 bytes len
*
* Parameters:
*     id:        Index of the memory chunk to save
*     data:      Data to write
*     len:       Data length
*     p_result:  Pointer to variable to reseive the result of operation
*
* Return:
*     Return the number of bytes written into NVRAM
*/
uint32_t mesh_write_node_info2(int id, const uint8_t* data, uint16_t len, wiced_result_t *p_result)
{
    uint32_t        ret = 0;
    uint8_t         *p;
    uint16_t        len_to_write, len_rest;
    uint8_t         idx;
    wiced_result_t  result = 0;
    for (idx = 0, len_rest = len, p = (uint8_t*)data; len_rest > 0; idx++, len_rest -= len_to_write, p += len_to_write)
    {
        len_to_write = len_rest > 0xff ? 0xff : len_rest;
        if (len_to_write != mesh_write_node_info(id + idx, p, len_to_write, &result))
            break;
        ret += len_to_write;
    }
    if (p_result)
        *p_result = result;
    return ret;
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_NETWORK_LAYER_TRNSMIT
static void test_mode_signal_network_layer_trnsmit(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bt_mesh_core_hci_cmd_network_layer_transmit_t *p = (wiced_bt_mesh_core_hci_cmd_network_layer_transmit_t*)p_data;
    if (data_len < 1 + OFFSETOF(wiced_bt_mesh_core_hci_cmd_network_layer_transmit_t, pdu)
        || data_len > 16 + OFFSETOF(wiced_bt_mesh_core_hci_cmd_network_layer_transmit_t, pdu)
        || p->ctl > 1
        || p->ttl > 0x7f)
        return;
    mesh_set_adv_params(0, 0, 0, 0, 0, 0);
    // use master secure material
    network_layer_send(&p->pdu[0], 1, &p->pdu[1], data_len - OFFSETOF(wiced_bt_mesh_core_hci_cmd_network_layer_transmit_t, pdu) - 1,
        0, NULL, p->ctl != 0 ? WICED_TRUE : WICED_FALSE, node_nv_data.node_id, LE2TOUINT16(p->dst), p->ttl, 0xffffffff, NULL, NULL);
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_TRANSPORT_LAYER_TRNSMIT
static void test_mode_signal_transport_layer_trnsmit(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bt_mesh_core_hci_cmd_transport_layer_transmit_t *p = (wiced_bt_mesh_core_hci_cmd_transport_layer_transmit_t*)p_data;
    uint16_t dst;
    uint8_t     app_key_idx;

    if (data_len < 1 + OFFSETOF(wiced_bt_mesh_core_hci_cmd_transport_layer_transmit_t, pdu)
        || data_len > 380 + OFFSETOF(wiced_bt_mesh_core_hci_cmd_transport_layer_transmit_t, pdu)
        || p->ctl > 1
        || p->ttl > 0x7f
        || p->szmic > 1
        || p->dst_len != 2 && p->dst_len != 16)
        return;
    if (p->dst_len == 2)
        dst = BE2TOUINT16(p->dst);
    else
    {
        // it is label UUID of the virtual address. Add it to the foundation level
        dst = foundation_add_virt_addr(p->dst);
        if (dst == MESH_NODE_ID_INVALID)
            return;
    }

    // if 0 app_key_idx exists then use it. Otherwise use dev_key
    app_key_idx = 0;;
    if (app_key_idx >= MESH_APP_KEY_MAX_NUM || (node_nv_data.app_key_bitmask & (1 << app_key_idx)) == 0)
        app_key_idx = 0xff;

    mesh_set_adv_params(0, 0, 0, 0, 0, 0);
    access_layer_send(&p->pdu[0], data_len - OFFSETOF(wiced_bt_mesh_core_hci_cmd_transport_layer_transmit_t, pdu), app_key_idx, 0, dst, p->ttl, WICED_FALSE, NULL, NULL);
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_IVUPDATE_SIGNAL_TRNSIT
static void test_mode_signal_ivupdate_signal_transit(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bool_t    update;
    wiced_bt_mesh_core_hci_cmd_ivupdate_signal_transit_t *p = (wiced_bt_mesh_core_hci_cmd_ivupdate_signal_transit_t*)p_data;
    if (data_len != sizeof(wiced_bt_mesh_core_hci_cmd_ivupdate_signal_transit_t)
        || p->in_progress > 1)
        return;
    if (p->in_progress)
        update = iv_updt_handle(WICED_TRUE, BE4TOUINT32(node_nv_data.net_iv_index) + 1, WICED_TRUE);
    else
        update = iv_updt_handle(WICED_FALSE, BE4TOUINT32(node_nv_data.net_iv_index), WICED_TRUE);
    if(update)
    {
        // Update proxy server advertisement and network secure beacon
        mesh_update_beacon();
    }
}

static void test_mode_signal_provision_end_cb(uint32_t conn_id, uint8_t result)
{
    wiced_bt_mesh_core_hci_evt_provision_end_t data;
    UINT32TOLE4(data.conn_id, conn_id);
    data.result = result;
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_CORE_PROVISION_END, (uint8_t*)&data, sizeof(data));
}


// Handles HCI_CONTROL_MESH_COMMAND_CORE_PROVISION
static void test_mode_signal_provision(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bt_mesh_core_hci_cmd_provision_t *p = (wiced_bt_mesh_core_hci_cmd_provision_t*)p_data;
    if (data_len != sizeof(wiced_bt_mesh_core_hci_cmd_provision_t))
        return;
    wiced_bt_mesh_provision_set_end_hci_callback(test_mode_signal_provision_end_cb);
    wiced_bt_mesh_provision_start(LE4TOUINT32(p->conn_id), LE2TOUINT16(p->addr), p->uuid, p->identify_duration);
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_PROVISION
static void test_mode_signal_health_set_faults(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bt_mesh_core_hci_cmd_health_set_faults_t *p = (wiced_bt_mesh_core_hci_cmd_health_set_faults_t*)p_data;
    if (data_len < 3 || data_len > (3 + 16))
        return;
    wiced_bt_mesh_core_health_set_faults(0, p->test_id, LE2TOUINT16(p->company_id), data_len - 3, p->faults);
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_ACCESS_PDU
static void test_mode_signal_access_pdu(const uint8_t *p_data, uint16_t data_len)
{
    uint16_t company_id, opcode, pdu_len;
    uint8_t offset, app_key_idx;
    wiced_bt_mesh_core_hci_cmd_access_pdu_t *p = (wiced_bt_mesh_core_hci_cmd_access_pdu_t*)p_data;
    if (data_len <= OFFSETOF(wiced_bt_mesh_core_hci_cmd_access_pdu_t, pdu)
        || p->ttl > 0x7f)
        return;
    pdu_len = (uint16_t)(uint32_t)(p_data + data_len - p->pdu);
    offset = access_layer_parse_payload(p->pdu, pdu_len, &company_id, &opcode);
    if (!offset)
    {
        TRACE0(TRACE_INFO, "signal_access_pdu: invalid payload\n");
        return;
    }
    if (p->app_key_idx[0] == 0xff && p->app_key_idx[1] == 0xff)
        app_key_idx = 0xff;
    else if (!foundation_find_appkey_(LE2TOUINT16(p->app_key_idx), &app_key_idx, NULL, NULL))
    {
        TRACE0(TRACE_INFO, "signal_access_pdu: invalid appkey\n");
        return;
    }
    foundation_handle(LE2TOUINT16(p->dst), LE2TOUINT16(p->src), app_key_idx,
        p->ttl, opcode, company_id, &p->pdu[offset], pdu_len - offset, -2);
}

// Handles HCI_CONTROL_MESH_COMMAND_CORE_SEND_SUBS_UPDT
static void test_mode_signal_send_subs_updt(const uint8_t *p_data, uint16_t data_len)
{
    wiced_bt_mesh_core_hci_cmd_send_subs_updt_t *p = (wiced_bt_mesh_core_hci_cmd_send_subs_updt_t*)p_data;
    if (data_len != sizeof(wiced_bt_mesh_core_hci_cmd_send_subs_updt_t))
        return;
    low_power_send_subslist_update(p->add, LE2TOUINT16(p->addr));
}

/**
* \brief The signals for different test modes.
* \details Application shall support some test modes (for example IV update) used for certification and compliance testing.
* The activation of the test mode shall be carried out locally (via a HW or SW interface).
*
* @param[in]   opcode      : opcode (see HCI_CONTROL_MESH_COMMAND_CORE_XXX in hci_control_api.h)
* @param[in]   p_data      : data
* @param[in]   data_len    : length of the data
*
* @return      WICED_BT_SUCCESS if opcode is recognized and handles. Otherwise caller has to call some other handler
*/
wiced_result_t wiced_bt_mesh_core_test_mode_signal(uint16_t opcode, const uint8_t *p_data, uint16_t data_len)
{
    wiced_result_t res = WICED_BT_SUCCESS;
    TRACE1(TRACE_INFO, "test_mode_signal: opcode:%x\n", opcode);
    TRACEN(TRACE_INFO, (char*)p_data, data_len);

    switch (opcode)
    {
    case WICED_BT_MESH_CORE_TEST_MODE_SIGNAL_IV_UPDATE_BEGIN:
        if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
        {
            if (iv_updt_handle(WICED_TRUE, BE4TOUINT32(node_nv_data.net_iv_index) + 1, WICED_TRUE))
            {
                // Update proxy server advertisement and network secure beacon
                mesh_update_beacon();
            }
        }
        break;
    case WICED_BT_MESH_CORE_TEST_MODE_SIGNAL_IV_UPDATE_END:
        if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
        {
            if (iv_updt_handle(WICED_FALSE, BE4TOUINT32(node_nv_data.net_iv_index), WICED_TRUE))
            {
                // Update proxy server advertisement and network secure beacon
                mesh_update_beacon();
            }
        }
        break;
    case WICED_BT_MESH_CORE_TEST_MODE_SIGNAL_TEST:
#ifdef _DEB_LOW_POWER_DONT_START_ON_INIT
        low_power_start();
#endif
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_NETWORK_LAYER_TRNSMIT:
        test_mode_signal_network_layer_trnsmit(p_data, data_len);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_TRANSPORT_LAYER_TRNSMIT:
        test_mode_signal_transport_layer_trnsmit(p_data, data_len);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_IVUPDATE_SIGNAL_TRNSIT:
        test_mode_signal_ivupdate_signal_transit(p_data, data_len);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_LOW_POWER_SEND_FRIEND_CLEAR:
        if (data_len == 0)
            low_power_send_friend_clear();
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_PROVISION:
        test_mode_signal_provision(p_data, data_len);
        break;
#ifndef MESH_CONTROLLER
    case HCI_CONTROL_MESH_COMMAND_CORE_CLEAR_REPLAY_PROT_LIST:
        if (data_len == 0)
            transport_layer_clear_rpl();
        break;
#endif
    case HCI_CONTROL_MESH_COMMAND_CORE_SET_IV_UPDATE_TEST_MODE:
        if (data_len == 0)
            mesh_core_iv_updt_test_mode = WICED_TRUE;
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_SET_IV_RECOVERY_STATE:
        if (data_len == 0)
            ivi_recovery_start(NULL);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_HEALTH_SET_FAULTS:
        test_mode_signal_health_set_faults(p_data, data_len);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_CFG_ONE_NETKEY:
        if (data_len == 0)
            mesh_core_use_one_net_key = WICED_TRUE;
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_CFG_ADV_IDENTITY:
        if (data_len == 0)
            wiced_bt_mesh_core_set_node_identity();
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_ACCESS_PDU:
        test_mode_signal_access_pdu(p_data, data_len);
        break;
    case HCI_CONTROL_MESH_COMMAND_CORE_SEND_SUBS_UPDT:
        test_mode_signal_send_subs_updt(p_data, data_len);
        break;
    default:
        res = WICED_BT_ERROR;
        break;
    }
    return res;
}

/**
* Reboot Delay timer callback function for function wiced_init_timer() called in the mesh_core_reboot()
*
* Parameters:
*   arg:    not used
*
* Return:   None
*
*/
static void mesh_core_reboot_delay_timer_callback_(uint32_t arg)
{
    TRACE1(TRACE_CRITICAL, "mesh_core_reboot_delay_timer_callback_: resetting system. delay_ms:%d\n", mesh_core_reboot_delay_timer_delay_ms);
    if (mesh_core_reboot_delay_timer_delay_ms < 20)
    wiced_hal_wdog_reset_system();
    else
        wiced_start_timer(&mesh_core_reboot_delay_timer, mesh_core_reboot_delay_timer_delay_ms);
    mesh_core_reboot_delay_timer_delay_ms = 0;
}

/**
* Reboots device after delay
*
* Parameters:
*   delay_ms:   Delay in ms before reboot. 0 means reboot now.
*
* Return:   None
*/
void mesh_core_reboot(uint32_t delay_ms)
{
    TRACE2(TRACE_CRITICAL, "mesh_core_reboot: delay_ms:%x conn_id:%x\n", delay_ms, ctx.conn_id);
    
    if (ctx.conn_id == 0 && delay_ms == 0)
    {
        wiced_hal_wdog_reset_system();
        return;
    }
    
    wiced_init_timer(&mesh_core_reboot_delay_timer, mesh_core_reboot_delay_timer_callback_, 0, WICED_MILLI_SECONDS_TIMER);

    if (ctx.conn_id)
    {
        // disconnect with 200ms delay
        if (delay_ms <= 200)
            mesh_core_reboot_delay_timer_delay_ms = 1;   //make it != 0
        else
            mesh_core_reboot_delay_timer_delay_ms = delay_ms - 200;
        wiced_start_timer(&mesh_core_reboot_delay_timer, 200);
        return;
    }

    wiced_start_timer(&mesh_core_reboot_delay_timer, delay_ms);
}

/**
 * \brief Return milliseconds passed since start.
 *
 *   @return      Milliseconds passed since start
 */
uint64_t wiced_bt_mesh_core_get_tick_count(void)
{
    return clock_SystemTimeMicroseconds64() / 1000 - ctx.tick_count_init_value;
}

#ifdef HARDCODED_PROVIS
wiced_bool_t mesh_core_hardcoded_prov(uint16_t addr, const uint8_t *net_key, const uint8_t *dev_key, uint32_t iv_index, const uint8_t* app_key, uint8_t element, uint16_t model_id, uint16_t subs_addr)
{
    wiced_bool_t ret = WICED_FALSE;
    uint8_t     net_iv_index[4];
    
    UINT32TOBE4(net_iv_index, iv_index);

    do
    {
        if (MESH_RESULT_SUCCESS != mesh_set_dev_key(dev_key))
            break;
        if (MESH_RESULT_SUCCESS != mesh_set_net_key(0, net_key, 0))
            break;
        if (MESH_RESULT_SUCCESS != mesh_set_iv_index(net_iv_index))
            break;
        if (MESH_RESULT_SUCCESS != mesh_set_dev_addr(addr))
            break;
        if (app_key)
        {
            if (MESH_RESULT_SUCCESS != mesh_set_app_key(0, 0, app_key, 0))
                break;
            if (model_id)
            {
                if (0 != mesh_core_app_bind_(element, 0, model_id, 0))
                    break;
                if(subs_addr)
                    if(0 != mesh_core_subs_add_(element, 0, model_id, subs_addr))
                        break;
            }
        }
        app_cache_clear(WICED_TRUE);
        app_cache_clear(WICED_FALSE);
        ret = WICED_TRUE;
    } while (0);

    TRACE1(TRACE_INFO, "mesh_core_hardcoded_prov: returns:%d\n", ret);
    return ret;
}
#endif

#endif

#if MESH_CHIP == 20703
//#ifndef REG32
//#define REG32(x)     *((volatile unsigned*)(x))
//#endif

// code from fw_20703A1\fw\bsp\inc\pmu_clk.h
typedef enum
{
    PLC1,
    PLC2,
    PLC3,
    BEC_CLK,
    WBS_SBC_HIGH,
    WBS_SBC_LOW,
    EMB_LT_HOST,
    EMB_LT_HOST_MAX,    // it is for 48 MHz
    NUM_FEATURES
} FEATURE_CODE;
void pmu_clk_feature_notify(FEATURE_CODE feature, BOOL on);
#elif MESH_CHIP == 20719
typedef enum {          // this enumerated type is also used to index a table of required wait states
    CLOCK_DISABLE,      // for Thick Oxide Ram in slimboot.h.  If you modify this type, update the table
    CLOCK_1MHZ,         // in slimboot.h as well.  20150714 (sm)
    CLOCK_4MHZ,
    CLOCK_6MHZ,
    CLOCK_12MHZ,
    CLOCK_16MHZ,
    CLOCK_24MHZ,
    CLOCK_32MHZ,
    CLOCK_48MHZ,
    CLOCK_96MHZ,
    CLOCK_XTAL,
} CLOCK_FREQ;

typedef enum {
    CLOCK_REQ_TRANS,            // Set Transport clock to specific clock freqency.
    CLOCK_REQ_FM,               // Set FM clock  freqency.
    CLOCK_REQ_CPU_SET,          // Set CPU to specific clock frequency
    CLOCK_REQ_CPU_SET_DEFAULT,  // Set CPU to back to default value
    CLOCK_REQ_CPU_NEED_UPTO,    // Update CPU to match new (usually more) clock frequency requirement
    CLOCK_REQ_CPU_RELEASE_FROM, // release previous clock freqency updating requirement.
} CLOCK_REQ;

//! Request clock.
//! This will make clock request based on req type.
extern void clock_Request(CLOCK_REQ req, CLOCK_FREQ freq);
extern void mia_enableLhlInterrupt(BOOL32 enable);
#endif

void mesh_core_boost_cpu(wiced_bool_t enable)
{
#if MESH_CHIP == 20703
    // enable/disable LHL interrupts
    //mia_enableLhlInterrupt(!enable);
    // set/remove 96 MHz clock override
    if (enable)
    {
        //REG32(cr_clk_div_sel_adr) |= (0x1 << 12);
    }
    else
    {
        //REG32(cr_clk_div_sel_adr) &= ~(0x1 << 12);
    }
    // set/remove 48 MHz clock override
    pmu_clk_feature_notify(EMB_LT_HOST_MAX, enable);

#elif MESH_CHIP == 20719
    //mia_enableLhlInterrupt(!enable);
    clock_Request(enable ? CLOCK_REQ_CPU_NEED_UPTO : CLOCK_REQ_CPU_RELEASE_FROM, CLOCK_96MHZ);
#elif MESH_CHIP == 20735
    //mia_enableLhlInterrupt(!enable);
    clock_Request(enable ? CLOCK_REQ_CPU_NEED_UPTO : CLOCK_REQ_CPU_RELEASE_FROM, CLOCK_96MHZ);
#endif
}

#if MESH_CHIP == 20719
void *mpaf_cfa_permanentAlloc(uint32_t size);
// Temporary implementation of that function in 20719 till it will be exposed by FW
void* wiced_memory_permanent_allocate(uint32_t size)
{
//    return wiced_bt_get_buffer((uint16_t)size);
    return mpaf_cfa_permanentAlloc(size);
}
#endif

#ifndef MESH_CONTROLLER
extern uint16_t g_mpaf_config_StackSize;

uint16_t mesh_core_get_stack_size(void)
{
    return g_mpaf_config_StackSize;
}

uint32_t mesh_core_get_current_stack_pointer(void)
{
    asm volatile
        (
        "mov  r0, sp\n\t"
        );
}
#endif
