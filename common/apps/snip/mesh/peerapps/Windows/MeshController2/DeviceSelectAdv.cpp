// DeviceSelect.cpp : implementation file
//

#include "stdafx.h"
#include "MeshController.h"
#include "DeviceSelectAdv.h"
#include "afxdialogex.h"

#include <Sddl.h>

#include <WinBase.h>

// =======================
#pragma comment(lib, "RuntimeObject.lib")

#ifndef BD_ADDR_LEN
#define BD_ADDR_LEN     6
typedef UINT8 BD_ADDR[BD_ADDR_LEN];
#endif

#define STREAM_TO_BDADDR(a, p)   {register int _i; register UINT8 *pbda = (UINT8 *)a + BD_ADDR_LEN - 1; for (_i = 0; _i < BD_ADDR_LEN; _i++) *pbda-- = *p++;}


EventRegistrationToken *watcherToken;
ComPtr<IBluetoothLEAdvertisementWatcherFactory> bleAdvWatcherFactory;
ComPtr<IBluetoothLEAdvertisementWatcher> bleWatcher;
ComPtr<IBluetoothLEAdvertisementFilter> bleFilter;
ComPtr<ITypedEventHandler<BluetoothLEAdvertisementWatcher*, BluetoothLEAdvertisementReceivedEventArgs*>> handler;

void BthAddrToBDA(BD_ADDR bda, ULONGLONG *btha)
{
    BYTE *p = (BYTE *)btha;
    STREAM_TO_BDADDR(bda, p);
}

// Prints an error string for the provided source code line and HRESULT
// value and returns the HRESULT value as an int.
int PrintError(unsigned int line, HRESULT hr)
{
    wprintf_s(L"ERROR: Line:%d HRESULT: 0x%X\n", line, hr);
    return hr;
}

template <typename T, typename U>
extern inline HRESULT await(const Microsoft::WRL::ComPtr<T> &asyncOp, U *results, UINT timeout = 0);

//HRESULT CDeviceSelectAdv::OnAdvertisementReceived(IBluetoothLEAdvertisementWatcher* watcher, IBluetoothLEAdvertisementReceivedEventArgs* args)
//{
//    UINT64 address;
//    HRESULT hr;
//    cout << "OnAdvertisementReceived received" << endl;
//
//    BluetoothLEAdvertisementType  value;
//
//    hr = args->get_AdvertisementType(&value);
//
//    cout << "Advertisement type: " << value << endl;
//
//    hr = args->get_BluetoothAddress(&address);
//    if (FAILED(hr))
//    {
//        cout << "OnAdvertisementReceived address not retrieved" << endl;
//    }
//    else
//    {
//        BD_ADDR bda;
//        WCHAR buf[64] = { 0 };
//
//        BthAddrToBDA(bda, &address);
//        cout << "OnAdvertisementReceived address: " << address << endl;
//        swprintf_s(buf, sizeof(buf) / sizeof(buf[0]), L"%02x:%02x:%02x:%02x:%02x:%02x", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);
//
//        // check if we already added this device with different service
//        WCHAR   buf2[64] = { 0 };
//        int     i;
//        for (i = 0; i < m_numDevices; i++)
//        {
//            m_lbDevices.GetText(i, buf2);
//            if (!wcscmp(buf, buf2))
//                break;
//        }
//        // Add this device if it is new one
//        if (i >= m_numDevices)
//        {
//            m_lbDevices.AddString(buf);
//            m_numDevices++;
//        }
//
//    } // got address
//
//    return S_OK;
//}


HRESULT CDeviceSelectAdv::OnAdvertisementReceived(IBluetoothLEAdvertisementWatcher* watcher, IBluetoothLEAdvertisementReceivedEventArgs* args)
{
	UINT64 address;
	HRESULT hr;
	cout << "OnAdvertisementReceived received" << endl;

	BluetoothLEAdvertisementType  value;

	hr = args->get_AdvertisementType(&value);

	cout << "Advertisement type: " << value << endl;

	hr = args->get_BluetoothAddress(&address);
	if (FAILED(hr))
	{
		cout << "OnAdvertisementReceived address not retrieved" << endl;
	}
	else
	{
		BD_ADDR bda;
		char buff[256] = { 0 };

		BthAddrToBDA(bda, &address);
		cout << "OnAdvertisementReceived address: " << address << endl;
		sprintf_s(buff, sizeof(buff), " LE Device: %02x:%02x:%02x:%02x:%02x:%02x", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);
		cout << buff << endl;

		//==

		//BD_ADDR bda;
		WCHAR buf[64] = { 0 };

		BthAddrToBDA(bda, &address);
		swprintf_s(buf, sizeof(buf) / sizeof(buf[0]), L"%02x:%02x:%02x:%02x:%02x:%02x", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);


		WCHAR   buf2[64] = { 0 };
		int     i;
		for (i = 0; i < m_numDevices; i++)
		{
			m_lbDevices.GetText(i, buf2);
			if (!wcslen(buf2) || !wcscmp(buf, buf2))
				break;
		}
		// Add this device if it is new one
		if (i >= m_numDevices)
		{
			m_lbDevices.AddString(buf);
			m_numDevices++;
		}

		//==


		ComPtr<IBluetoothLEAdvertisement> bleAdvert;

		hr = args->get_Advertisement(&bleAdvert);
		if (FAILED(hr))
		{
			cout << "get_Advertisement failed" << endl;
		}
		else
		{
			cout << "get_Advertisement data retreived" << endl;

			// Get Name of the device
			HString name;
			hr = bleAdvert->get_LocalName(name.GetAddressOf());
			sprintf_s(buff, sizeof(buff), "%S", name.GetRawBuffer(nullptr));
			cout << "Local Name: " << buff << endl;

			// Get Services 
			ComPtr<ABI::Windows::Foundation::Collections::IVector<GUID>> vecGuid;

			hr = bleAdvert->get_ServiceUuids(&vecGuid);

			if (FAILED(hr))
			{
				cout << "get_ServiceUuids failed" << endl;
			}
			else
			{
				UINT guidCount = 0;
				hr = vecGuid->get_Size(&guidCount);
				cout << "Guid Count " << guidCount << endl;

				if (SUCCEEDED(hr))
				{
					for (int i = 0; i < (int)guidCount; ++i)
					{
						GUID guid = { 0 };
						//ComPtr<GUID> guid;
						hr = vecGuid->GetAt(i, &guid);
						if (SUCCEEDED(hr))
						{
							WCHAR szService[80] = { 0 };
							UuidToString(szService, 80, &guid);
							sprintf_s(buff, sizeof(buff), "%S", szService);
							cout << "index: " << i << " GUID: " << buff << endl;
						}
					}
				}
			}
		}

		// Get Advertisement Data

		ComPtr <ABI::Windows::Foundation::Collections::IVector<ABI::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementDataSection*>> vecData;
		hr = bleAdvert->get_DataSections(&vecData);

		if (FAILED(hr))
		{
			cout << "get_DataSections failed" << endl;
		}
		else
		{
			UINT count = 0;
			hr = vecData->get_Size(&count);

			cout << "Datasections Count " << count << endl;

			if (SUCCEEDED(hr))
			{
				for (UINT i = 0; i < count; ++i)
				{
					ComPtr<ABI::Windows::Devices::Bluetooth::Advertisement::IBluetoothLEAdvertisementDataSection> ds;

					hr = vecData->GetAt(i, &ds);
					if (SUCCEEDED(hr))
					{
						ComPtr<ABI::Windows::Storage::Streams::IBuffer> ibuf;
						BYTE datatype = 0;

						hr = ds->get_DataType(&datatype);
						memset(buff, 0, sizeof(buff));
						sprintf_s(buff, sizeof(buff), "%d", datatype);
						cout << "Data Type: " << buff << endl;

						hr = ds->get_Data(&ibuf);

						Microsoft::WRL::ComPtr<Windows::Storage::Streams::IBufferByteAccess> pBufferByteAccess;
						ibuf.As(&pBufferByteAccess);

						// Get pointer to pixel bytes 
						byte* pdatabuf = nullptr;
						pBufferByteAccess->Buffer(&pdatabuf);

						memset(buff, 0, sizeof(buff));

						UINT32 length = 0;

						hr = ibuf->get_Length(&length);

						cout << "Length " << length << endl;

						for (UINT32 i = 0; i < length; ++i)
						{
							sprintf_s(buff + 3 * i, sizeof(buff) - 3 * i, "%02x ", *(pdatabuf + i));
						}

						//sprintf(buff, "DataType: 0x%x buffer %s", pdatabuf);
						cout << "Data Buffer: " << buff << endl;
					}
				}
			}
		}


	} // got address

	return S_OK;
}

HRESULT CDeviceSelectAdv::onBluetoothLEDeviceFoundAsync(IAsyncOperation<BluetoothLEDevice *> *op, AsyncStatus status)
{
	if (status != AsyncStatus::Completed)
		return S_OK;

	ComPtr<IBluetoothLEDevice> device;
	HRESULT hr;
	hr = op->GetResults(&device);
	//Q_ASSERT_SUCCEEDED(hr);

	return hr;
	//return onBluetoothLEDeviceFound(device, PairingCheck::CheckForPairing);
}


int CDeviceSelectAdv::StartLEAdvertisementWatcher()
{
    OutputDebugStringW(L"StartLEAdvertisementWatcher");

	HRESULT hr = NULL;

	// Initialize the Windows Runtime.
	RoInitializeWrapper initialize(RO_INIT_MULTITHREADED);
	if (FAILED(initialize))
	{
		return PrintError(__LINE__, initialize);
	}

    watcherToken = new EventRegistrationToken();

    // Get the activation factory for the IBluetoothLEAdvertisementWatcherFactory interface.
    OutputDebugStringW(L"StartLEAdvertisementWatcher");
     hr = GetActivationFactory(HStringReference(RuntimeClass_Windows_Devices_Bluetooth_Advertisement_BluetoothLEAdvertisementWatcher).Get(), &bleAdvWatcherFactory);
    if (FAILED(hr))
    {
        return PrintError(__LINE__, hr);
    }

    Wrappers::HStringReference class_id_filter2(RuntimeClass_Windows_Devices_Bluetooth_Advertisement_BluetoothLEAdvertisementFilter);
    hr = RoActivateInstance(class_id_filter2.Get(), reinterpret_cast<IInspectable**>(bleFilter.GetAddressOf()));

    hr = bleAdvWatcherFactory->Create(bleFilter.Get(), &bleWatcher);

    OutputDebugStringW(L"StartLEAdvertisementWatcher");
    if (bleWatcher == NULL)
    {
        cout << "bleWatcher is NULL, err is " << hex << hr;
    }
    else
    {
		handler = Callback<ITypedEventHandler<BluetoothLEAdvertisementWatcher*, BluetoothLEAdvertisementReceivedEventArgs*>>
			(this, &CDeviceSelectAdv::OnAdvertisementReceived);

        OutputDebugStringW(L"StartLEAdvertisementWatcher");
        hr = bleWatcher->add_Received(handler.Get(), watcherToken);
		if (FAILED(hr))
		{
			return PrintError(__LINE__, hr);
		}

        HRESULT hr = bleWatcher->Start();
        if (FAILED(hr))
        {
            return PrintError(__LINE__, hr);
        }

		while (!m_bStop) {
			Sleep(100);
		}
    }

    OutputDebugStringW(L"StartLEAdvertisementWatcher - return");
    return 0;
}

int CDeviceSelectAdv::StartwatchLEAdvertisementWatcher()
{
    OutputDebugStringW(L"StartLEAdvertisementWatcher");

    HRESULT hr = bleWatcher->Start();
    if (FAILED(hr))
    {
        return PrintError(__LINE__, hr);
    }
    return 0;
}

int CDeviceSelectAdv::StopLEAdvertisementWatcher()
{
    HRESULT hr = bleWatcher->Stop();

	m_bStop = TRUE;

    if (FAILED(hr))
    {
        return PrintError(__LINE__, hr);
    }

    return 0;
}



// =======================

// CDeviceSelectAdv dialog

IMPLEMENT_DYNAMIC(CDeviceSelectAdv, CDialogEx)

CDeviceSelectAdv::CDeviceSelectAdv(CWnd* pParent /*=NULL*/)
    : CDialogEx(CDeviceSelectAdv::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CDeviceSelectAdv::~CDeviceSelectAdv()
{
}

void CDeviceSelectAdv::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_DEVICE_LIST_ADV, m_lbDevices);
}


BEGIN_MESSAGE_MAP(CDeviceSelectAdv, CDialogEx)
    ON_LBN_DBLCLK(IDC_DEVICE_LIST_ADV, &CDeviceSelectAdv::OnDblclkDeviceList)
    ON_BN_CLICKED(IDOK, &CDeviceSelectAdv::OnBnClickedOk)
    ON_BN_CLICKED(IDCANCEL, &CDeviceSelectAdv::OnBnClickedCancel)

END_MESSAGE_MAP()



DWORD WINAPI DeviceDiscoveryThread(void *Context)
{
	CDeviceSelectAdv *pDlg = (CDeviceSelectAdv *)Context;
	pDlg->StartLEAdvertisementWatcher();
	return 0;
}


// CDeviceSelectAdv message handlers
BOOL CDeviceSelectAdv::OnInitDialog()
{
    OutputDebugStringW(L"CDeviceSelectAdv::OnInitDialog");

    CDialogEx::OnInitDialog();

	m_bStop = FALSE;

    // Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);            // Set big icon
    SetIcon(m_hIcon, FALSE);        // Set small icon

    m_numDevices = 0;

    GetDlgItem(IDC_DEVICE_LIST_ADV)->ShowWindow(SW_SHOW);
    GetDlgItem(IDC_NO_DEVICES_ADV)->ShowWindow(SW_HIDE);
	
	CreateThread(NULL, 0, DeviceDiscoveryThread, this, 0, NULL);

    return TRUE;
}

void CDeviceSelectAdv::OnDblclkDeviceList()
{
    OnBnClickedOk();
}

void CDeviceSelectAdv::OnBnClickedOk()
{
	WCHAR buf[24] = { 0 };

    StopLEAdvertisementWatcher();

    m_lbDevices.GetText(m_lbDevices.GetCurSel(), buf);

//	wcscpy(buf, L"41:3e:09:9b:71:20");
//	wcscpy(buf, L"20:71:9b:09:3e:41");

//	wcscpy(buf, L"20:70:6a:20:89:87");
	//20706a208987

//	wcscpy(buf, L"87:89:20:6a:70:20");

	//20:73:51:11:11:11
	//wcscpy(buf, L"11:11:11:51:73:20");

	//WCHAR buf[64] = { 0 };

	int bda[6];
    if (swscanf_s(buf, L"%02x:%02x:%02x:%02x:%02x:%02x", &bda[0], &bda[1], &bda[2], &bda[3], &bda[4], &bda[5]) == 6)
    {
        for (int i = 0; i < 6; i++)
//            m_bth.rgBytes[5 - i] = (BYTE)bda[i];
			 m_bth.rgBytes[i] = (BYTE)bda[i];
    }

    CDialogEx::OnOK();
}

void CDeviceSelectAdv::OnBnClickedCancel()
{
    m_bth.ullLong = 0;
    
    StopLEAdvertisementWatcher();

    CDialogEx::OnCancel();
}





//==========================================================================================================================
//	//==
//	ComPtr<IBluetoothLEDeviceStatics> deviceStatics;
//	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::IBluetoothLEDevice> mDevice;
//	HRESULT hr = GetActivationFactory(HString::MakeReference(RuntimeClass_Windows_Devices_Bluetooth_BluetoothLEDevice).Get(), &deviceStatics);
//	//ASSERT_SUCCEEDED(hr);
//	ComPtr<IAsyncOperation<BluetoothLEDevice *>> deviceFromIdOperation;
//	hr = deviceStatics->FromBluetoothAddressAsync(address, &deviceFromIdOperation);
////	ASSERT_SUCCEEDED(hr);
////	hr = await(deviceFromIdOperation, mDevice.GetAddressOf());
////	ASSERT_SUCCEEDED(hr);

//	//if (!mDevice) {
//	//	//cout << "Could not find LE device";
//	//	OutputDebugStringW(L"Could not find LE device");

//	////	setError(InvalidBluetoothAdapterError);
//	////	setState(UnconnectedState);
//	//}
//	//else
//	//{
//	//	OutputDebugStringW(L"Found LE device");
//	//	//cout << "Got device" << endl;
//	//}

//	hr = deviceFromIdOperation->put_Completed(Callback<IAsyncOperationCompletedHandler<BluetoothLEDevice *>>
//		(this, &CDeviceSelectAdv::onBluetoothLEDeviceFoundAsync).Get());
//	if (FAILED(hr)) {
//		cout << "Could not register device found callback";
//		return S_OK;
//	}
//	//==

