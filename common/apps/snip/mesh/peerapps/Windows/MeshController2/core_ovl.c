/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh core library implementation.
*/
#include <stdio.h> 
#include <string.h> 

#include "platform.h"
#include "mesh_core.h"
#include "mesh_util.h"
#include "core_ovl.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__CORE_OVL_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__CORE_OVL_C
#include "mesh_trace.h"

/**
* Sets all members of the node_nv_data to the initial value
*
* Parameters:
*     features:            any combination of features defined by WICED_BT_MESH_CORE_FEATURE_BIT_XXX
*
* Return:   None
*/
void mesh_clear_node_nv_data(uint16_t features)
{
    memset(&node_nv_data, 0, sizeof(MeshNodeNvData));
    node_nv_data.node_id = MESH_NODE_ID_INVALID;
    node_nv_data.state_sec_net_beacon = FND_STATE_SEC_NET_BEACON_ON;
    node_nv_data.state_default_ttl = MESH_DEFAULT_TTL;
    node_nv_data.state_relay_retrans = MESH_ADV_NET_TRANS;      // 3 transmits with 100ms interval (0x49)
    node_nv_data.state_net_trans = MESH_ADV_NET_TRANS;          // 3 transmits with 100ms interval  ( 0x49)

    if ((features & WICED_BT_MESH_CORE_FEATURE_BIT_RELAY) == 0)
        node_nv_data.state_relay = FND_STATE_RELAY_NOT_SUPPORTED;
    else
        node_nv_data.state_relay = FND_STATE_RELAY_ON;
    if ((features & WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND) == 0)
        node_nv_data.state_friend = FND_STATE_FRIEND_NOT_SUPPORTED;
    else
        node_nv_data.state_friend = FND_STATE_FRIEND_ON;
    if ((features & WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER) == 0)
        node_nv_data.state_gatt_proxy = FND_STATE_PROXY_NOT_SUPPORTED;
    else
        node_nv_data.state_gatt_proxy = FND_STATE_PROXY_ON;
    TRACE3(TRACE_INFO, "mesh_clear_node_nv_data: state_relay:%x state_gatt_proxy:%x features:%x \n", node_nv_data.state_relay, node_nv_data.state_gatt_proxy, features);
}

#ifndef MESH_CONTROLLER
/**
* Loads app cache from the NVRAM
*
* Parameters:
*   prev_iv_idx:    WICED_TRUE - cache for previous IV INDEX; WICED_FALSE - cache for current IV INDEX;
*
* Return:   WICED_TRUE - success.
*           WICED_FALSE - error.
*/
static wiced_bool_t app_cache_load(wiced_bool_t prev_iv_idx)
{
    uint32_t len;
    len = mesh_read_node_info(prev_iv_idx ? MESH_NVM_IDX_APP_CACHE_PREV : MESH_NVM_IDX_APP_CACHE,
        prev_iv_idx ? (uint8_t*)&ctx.app_cache_prev[0] : (uint8_t*)&ctx.app_cache[0],
        sizeof(ctx.app_cache), NULL);
    if (len == sizeof(ctx.app_cache))
    {
        return WICED_TRUE;
    }

    TRACE2(TRACE_INFO, "app_cache_load: read failed: prev_iv_idx:%d len:%d\n", prev_iv_idx, len);

    return WICED_FALSE;
}

/**
 * Mesh core library function to create the node
 *
 * Parameters:
 *     forceReset:          Flag whether to force reset this node to delete any saved node info.
 *     nodeAuthenticated:   flag returned to indicate whether a node has been authenticated
 *                          into the mesh network. It can be NULL.
 *     features:            any combination of features defined by WICED_BT_MESH_CORE_FEATURE_BIT_XXX
 *
 * Return: None
 *
 */
void mesh_create_node(
    wiced_bool_t    force_reset,
    wiced_bool_t    *node_authenticated,
    uint16_t        features)
{
    uint32_t len;
    uint32_t num;
    int    key_inx;

    // Reset the device context
    memset(&ctx, 0, sizeof(MeshCtx));
    memset(ctx.beacon_instance, 0xff, sizeof(ctx.beacon_instance));
    memset(ctx.proxy_adv_instance, 0xff, sizeof(ctx.proxy_adv_instance));

    // Set the default operation parameters
    ctx.beacon_interval = MESH_BEACON_INTERVAL;
    ctx.proxy_adv_interval = MESH_PROXY_ADV_INTERVAL;
    ctx.default_ttl = MESH_DEFAULT_TTL;

    // Reset the node nv data
    mesh_clear_node_nv_data(features);

    if(node_authenticated)
        *node_authenticated = WICED_FALSE;
    if (force_reset)
    {
        mesh_save_node_nv_data();
        app_cache_clear(WICED_TRUE);
        app_cache_clear(WICED_FALSE);
        
        TRACE1(TRACE_INFO, "create_node: force_reset: features:%x\n", features);

        return;
    }

    len = mesh_read_node_info(MESH_NVM_IDX_NODE_DATA, (uint8_t*)&node_nv_data, sizeof(MeshNodeNvData), NULL);
    if (len != sizeof(MeshNodeNvData))
    {
        mesh_clear_node_nv_data(features);
        mesh_save_node_nv_data();


        TRACE2(TRACE_INFO, "create_node: main read failed, len:%d features:%x\n", len, features);

        return;
    }

    if (!app_cache_load(WICED_FALSE)
        || !app_cache_load(WICED_TRUE))
    {
        TRACE0(TRACE_INFO, "create_node: app_cache_load failed\n");
        return;
    }

    for (key_inx = 0; key_inx < MESH_APP_KEY_MAX_NUM; key_inx++)
    {
        if (node_nv_data.app_key_bitmask & (1 << key_inx))
        {
            len = mesh_read_node_info(MESH_NVM_IDX_APP_KEY_BEGIN + key_inx, (uint8_t*)&node_nv_app_key[key_inx], sizeof(MeshNvAppKey), NULL);
            if (len != sizeof(MeshNvAppKey))
            {
                node_nv_data.node_id = MESH_NODE_ID_INVALID;
                TRACE1(TRACE_INFO, "create_node: key read failed, len = %d\n", len);
                return;
            }
        }
    }

    for (key_inx = 0; key_inx < MESH_NET_KEY_MAX_NUM; key_inx++)
    {
        if (node_nv_data.net_key_bitmask & (1 << key_inx))
        {
            len = mesh_read_node_info(MESH_NVM_IDX_NET_KEY_BEGIN + key_inx, (uint8_t*)&node_nv_net_key[key_inx], sizeof(MeshNvNetKey), NULL);
            if (len != sizeof(MeshNvNetKey))
            {
                node_nv_data.node_id = MESH_NODE_ID_INVALID;
                TRACE1(TRACE_INFO, "create_node: key read failed, len = %d\n", len);
                return;
            }
            // If the Mesh Proxy Service is not supported, then Node Identity state value shall be 0x02 and not be changed.
            if (node_nv_data.state_gatt_proxy == FND_STATE_PROXY_NOT_SUPPORTED)
                node_nv_net_key[key_inx].state = (node_nv_net_key[key_inx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | FND_STATE_NODE_IDENTITY_NOT_SUPPORTED;
            // reset node identity state (se OFF) unless it is NOT_SUPPORTED
            else if ((node_nv_net_key[key_inx].state & MESH_NETKEY_STATE_IDENTITY_MASK) != FND_STATE_NODE_IDENTITY_NOT_SUPPORTED)
                node_nv_net_key[key_inx].state = (node_nv_net_key[key_inx].state & ~MESH_NETKEY_STATE_IDENTITY_MASK) | FND_STATE_NODE_IDENTITY_OFF;
        }
    }
    TRACE3(TRACE_INFO, "create_node: Initialized from NVRAM app_key_bitmask:%x net_key_bitmask:%x node_id:%x\n", node_nv_data.app_key_bitmask, node_nv_data.net_key_bitmask, node_nv_data.node_id);
    TRACE1(TRACE_INFO, " net_iv_index:%x\n", BE4TOUINT32(node_nv_data.net_iv_index));
    TRACE1(TRACE_INFO, " features:%x\n", features);
    //TRACE1(TRACE_CRITICAL, " SEQ:%x dev_key:\n", node_nv_data.sequence_number);
#if (TRACE_COMPILE_LEVEL <= 3)
    wiced_printf(NULL, 0, " SEQ:%x dev_key:****************************\n\n", node_nv_data.sequence_number);
#endif
    TRACEN(TRACE_CRITICAL, (char*)node_nv_data.dev_key, MESH_KEY_LEN);


    for (num = 0; num < MESH_APP_KEY_MAX_NUM; num++)
    {
        if (node_nv_data.app_key_bitmask & (1 << num))
        {
            TRACE3(TRACE_INFO, "create_node: idx:%d aid:%x global_idx:%d\n", num, node_nv_app_key[num].aid, node_nv_app_key[num].global_idx);
            TRACE1(TRACE_INFO, " global_net_key_idx:%d app_key:\n", node_nv_app_key[num].global_net_key_idx);
            TRACEN(TRACE_INFO, (char*)node_nv_app_key[num].key, MESH_KEY_LEN);
        }
    }
    for (num = 0; num < MESH_NET_KEY_MAX_NUM; num++)
    {
        if (node_nv_data.net_key_bitmask & (1 << num))
        {
            TRACE4(TRACE_INFO, "create_node: idx:%d nid:%x state:%x global_idx:%x net_key, encr_key, privacy_key, network_id, identity_key, beacon_key, proxy_key:\n", num, node_nv_net_key[num].sec_material.nid, node_nv_net_key[num].state, node_nv_net_key[num].global_idx);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].sec_material.encr_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].sec_material.privacy_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].network_id, MESH_NETWORK_ID_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].identity_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].beacon_key, MESH_KEY_LEN);
            TRACEN(TRACE_INFO, (char*)node_nv_net_key[num].proxy_key, MESH_KEY_LEN);
        }
    }

    if (node_authenticated)
        *node_authenticated = (node_nv_data.node_id == MESH_NODE_ID_INVALID) ? WICED_FALSE : WICED_TRUE;
}
#endif

/**
* Mesh core library function to set IV index
*
* Parameters:
*     iv_index: IV_INDEX (MESH_IV_INDEX_LEN bytes)
*
* Return:
*     Returns the result code.
*
*/
MeshResultCode mesh_set_iv_index(const uint8_t* iv_index)
{
    // save in the node info
    memcpy(node_nv_data.net_iv_index, iv_index, MESH_IV_INDEX_LEN);

    if (!mesh_save_node_nv_data())
    {
        TRACE0(TRACE_INFO, "set_iv_index: failed\n");
        return MESH_RESULT_NVRAM_FAILURE;
    }

    TRACE0(TRACE_INFO, "set_iv_index\n");
    TRACEN(TRACE_INFO, (char*)node_nv_data.net_iv_index, MESH_IV_INDEX_LEN);

    return MESH_RESULT_SUCCESS;
}

/**
* Mesh core library function to set or delete Application Key and all related values
*
* Parameters:
*   idx:            Index of the Application key
*   net_key_idx:    Index of the related network key. This param ignored if delete request.
*   app_key:        Application key (16 bytes). NULL means delete that key
*   aid:            Application key ID (5 bits) generated from Application key. This param ignored if delete request.
*   global_idx:     Global index of this Application key. This param ignored if delete request.
*
* Return:
*     Returns the result code.
*
*/
MeshResultCode mesh_set_app_key_and_id(uint8_t idx, uint8_t net_key_idx, const uint8_t* app_key, uint8_t aid, uint16_t global_idx)
{
    TRACE4(TRACE_INFO, "mesh_set_app_key_and_id: idx:%d net_key_idx:%d app_key:%x aid:%x\n", idx, net_key_idx, (uint32_t)app_key, aid);
    TRACE2(TRACE_INFO, " global_idx:%x global_net_key_idx:%x\n", global_idx, node_nv_net_key[net_key_idx].global_idx);

    // if it is not delete request then update key values
    if (app_key)
    {
        // remember key in the node info
        node_nv_data.app_key_bitmask |= (1 << idx);

        memcpy(node_nv_app_key[idx].key, app_key, MESH_KEY_LEN);
        node_nv_app_key[idx].aid = aid;
        node_nv_app_key[idx].global_net_key_idx = (node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK);
        node_nv_app_key[idx].global_idx = global_idx;

        if (!saveNodeNvAppKey(idx))
        {
            node_nv_data.app_key_bitmask &= ~(1 << idx);

            TRACE1(TRACE_INFO, "set_app_key save key: failed idx:%d\n", idx);

            return MESH_RESULT_NVRAM_FAILURE;
        }
    }
    else
    {
        node_nv_data.app_key_bitmask &= ~(1 << idx);
    }
    if (!mesh_save_node_nv_data())
    {
        TRACE0(TRACE_INFO, "set_app_key: failed\n");
        return MESH_RESULT_NVRAM_FAILURE;
    }

    TRACE3(TRACE_INFO, "set_app_key: idx:%x net_key_idx:%x global_idx:%x\n", idx, net_key_idx, global_idx);
    TRACE1(TRACE_INFO, " aid:%x app key:\n", aid);
    TRACEN(TRACE_INFO, (char*)app_key, MESH_KEY_LEN);

    return MESH_RESULT_SUCCESS;
}

/**
* Mesh core library function to set Device Key
*
* Parameters:
*     dev_key: Device key (16 bytes)
*
* Return:
*     Returns the result code.
*
*/
MeshResultCode mesh_set_dev_key(const uint8_t* dev_key)
{
#if defined(HARDCODED_DEV_KEY) && !defined(HARDCODED_PROVIS)
    uint8_t hardcoded_dev_key[MESH_KEY_LEN] = { HARDCODED_DEV_KEY };
    dev_key = hardcoded_dev_key;
#endif
    memcpy(node_nv_data.dev_key, dev_key, MESH_KEY_LEN);
    if (!mesh_save_node_nv_data())
    {
        TRACE0(TRACE_INFO, "set_dev_key: failed\n");
        return MESH_RESULT_NVRAM_FAILURE;
    }

    TRACE0(TRACE_INFO, "set_dev_key: dev key:\n");
    TRACEN(TRACE_INFO, (char*)node_nv_data.dev_key, MESH_KEY_LEN);

    return MESH_RESULT_SUCCESS;
}

/**
* Mesh core library function to set or delete Network Key and all related values
*
* Parameters:
*   key_idx:        Index of the network key
*   net_key:        Network key (16 bytes). NULL means delete that net_key - all following params are ignored in such case
*   nid:            7 bit NID (from k2)
*   encr_key:       Buffer for Encryption key with length MESH_KEY_LEN.
*   privacy_key:    Buffer for Privacy key with length MESH_KEY_LEN.
*   network_id:     Buffer for Network ID with length MESH_NETWORK_ID_LEN.
*   identity_key:   Buffer for Identity key with length MESH_KEY_LEN.
*   beacon_key:     Buffer for Beacon key with length MESH_KEY_LEN.
*   proxy_key:      Buffer for Proxy key with length MESH_KEY_LEN.
*   global_idx:     Global index of this network key.
*
* Return:
*     Returns the result code.
*/
MeshResultCode mesh_set_net_security(uint8_t key_idx, const uint8_t* net_key, uint8_t nid, const uint8_t* encr_key, const uint8_t* privacy_key,
    const uint8_t* network_id, const uint8_t* identity_key, const uint8_t* beacon_key, const uint8_t* proxy_key, uint16_t global_idx)
{
    if (key_idx >= MESH_NET_KEY_MAX_NUM)
    {
        TRACE1(TRACE_INFO, "set_net_key: too big idx %d\n", key_idx);
        return MESH_RESULT_GENERIC_ERROR;
    }
    // if it is not delete request then 
    if (net_key)
    {
        // If the Mesh Proxy Service is not supported, the Node Identity state value shall be 0x02 and not be changed.
        if (node_nv_data.state_gatt_proxy == FND_STATE_PROXY_NOT_SUPPORTED)
            node_nv_net_key[key_idx].state = FND_STATE_NODE_IDENTITY_NOT_SUPPORTED;
        else
            node_nv_net_key[key_idx].state = FND_STATE_NODE_IDENTITY_OFF;
        TRACE2(TRACE_INFO, "mesh_set_net_security: state_gatt_proxy:%d state:%x\n", node_nv_data.state_gatt_proxy, node_nv_net_key[key_idx].state);

        // remember key in the node info
        node_nv_data.net_key_bitmask |= (1 << key_idx);
        // save all values
        memcpy(node_nv_net_key[key_idx].key, net_key, MESH_KEY_LEN);
        node_nv_net_key[key_idx].sec_material.nid = nid;
        memcpy(node_nv_net_key[key_idx].sec_material.encr_key, encr_key, MESH_KEY_LEN);
        memcpy(node_nv_net_key[key_idx].sec_material.privacy_key, privacy_key, MESH_KEY_LEN);
        memcpy(node_nv_net_key[key_idx].network_id, network_id, MESH_NETWORK_ID_LEN);
        memcpy(node_nv_net_key[key_idx].identity_key, identity_key, MESH_KEY_LEN);
        memcpy(node_nv_net_key[key_idx].beacon_key, beacon_key, MESH_KEY_LEN);
        memcpy(node_nv_net_key[key_idx].proxy_key, proxy_key, MESH_KEY_LEN);
        node_nv_net_key[key_idx].global_idx = global_idx;

        if (!saveNodeNvNetKey(key_idx))
        {
            node_nv_data.net_key_bitmask &= ~(1 << key_idx);
            TRACE1(TRACE_INFO, "mesh_set_net_security: save key: failed key_idx:%x\n", key_idx);

            return MESH_RESULT_NVRAM_FAILURE;
        }
    }
    else
    {
        // it is delete request then remove that key from node info
        node_nv_data.net_key_bitmask &= ~(1 << key_idx);
    }
    if (!mesh_save_node_nv_data())
    {
        TRACE1(TRACE_INFO, "mesh_set_net_security: failed key_idx:%x\n", key_idx);
        return MESH_RESULT_NVRAM_FAILURE;
    }

    if (net_key)
    {
        TRACE3(TRACE_INFO, "mesh_set_net_security: key_idx:%x nid:%x global_idx:%x  net_key encr_key, privacy_key, network_id, identity_key, beacon_key, proxy_key:\n", key_idx, nid, global_idx);
        TRACEN(TRACE_INFO, (char*)net_key, MESH_KEY_LEN);
        TRACEN(TRACE_INFO, (char*)encr_key, MESH_KEY_LEN);
        TRACEN(TRACE_INFO, (char*)privacy_key, MESH_KEY_LEN);
        TRACEN(TRACE_INFO, (char*)network_id, MESH_NETWORK_ID_LEN);
        TRACEN(TRACE_INFO, (char*)identity_key, MESH_KEY_LEN);
        TRACEN(TRACE_INFO, (char*)beacon_key, MESH_KEY_LEN);
        TRACEN(TRACE_INFO, (char*)proxy_key, MESH_KEY_LEN);
    }
    else
    {
        TRACE2(TRACE_INFO, "mesh_set_net_security: delete key_idx:%x global_idx:%x\n", key_idx, global_idx);
#ifndef MESH_CONTROLLER
        mesh_update_beacon();
#endif
    }
    return MESH_RESULT_SUCCESS;
}

/**
* Mesh core library function to set Device Address into the node info
*
* Parameters:
*     node_id:  device addr.
*
* Return:
*     Returns the result code.
*
*/
MeshResultCode mesh_set_dev_addr(uint16_t node_id)
{
    if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
    {
        TRACE1(TRACE_INFO, "set_dev_addr: already provisioned dev id:%04x\n", node_nv_data.node_id);
        return MESH_RESULT_DEVICE_BUSY;
    }
    // remember own id
    node_nv_data.node_id = node_id;

    if (!mesh_save_node_nv_data())
    {
        TRACE0(TRACE_INFO, "set_dev_addr: failed to save\n");
        return MESH_RESULT_NVRAM_FAILURE;
    }
    TRACE1(TRACE_INFO, "set_dev_addr: dev id:%04x\n", node_nv_data.node_id);

    return MESH_RESULT_SUCCESS;
}

//----------------- provision fragmentation -----------------------

#ifndef MESH_CONTROLLER
/**
* Handles secured unprovisioned beacon.
*
* Parameters:
*   adv_data:   Unprovisioned beacon data: <uuid>(16 bytes)
*   adv_len:    Length of the beacon data (16).
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - failed
*
*/
wiced_bool_t mesh_handle_unprovisioned_beacon(const uint8_t* adv_data, uint8_t adv_len)
{
    const BYTE* uri_hash;
    // UUID(16bytes), OOB(2bytes)[, URI_HASH(4bytes)]
    if (adv_len == (MESH_DEVICE_UUID_LEN + WICED_BT_MESH_PROVISION_ADV_OOB_LEN))
        uri_hash = 0;
    else if (adv_len == (MESH_DEVICE_UUID_LEN + WICED_BT_MESH_PROVISION_ADV_OOB_LEN + WICED_BT_MESH_PROVISION_ADV_URI_HASH_LEN))
        uri_hash = adv_data + MESH_DEVICE_UUID_LEN + WICED_BT_MESH_PROVISION_ADV_OOB_LEN;
    else
    {
        TRACE1(TRACE_DEBUG, "unprovisioned_beacon: wrong len %d\n", adv_len);
        return WICED_FALSE;
    }
    // make sure connected client is expecting UUIDs of unprovisioned nodes
    if (ctx.unprovisioned_cb)
    {
        ctx.unprovisioned_cb(adv_data, BE2TOUINT16(adv_data + MESH_DEVICE_UUID_LEN), uri_hash);
    }
    return WICED_TRUE;
}

/**
* Starts detection of the unprovisioned devices calling callback function unprovisioned_cb on each of them
*
* Parameters:
*   unprovisioned_cb:   Callback function to be called on each detected unprovisioned device.
*                       It can be called repeatedly for the same device.
*                       NULL meand stop detecting
*
* Return:   None
*/
void wiced_bt_mesh_provision_scan_unprovisioned_devices(wiced_bt_mesh_provision_scan_unprovisioned_devices_cb_t unprovisioned_cb)
{
    TRACE1(TRACE_INFO, "wiced_bt_mesh_provision_scan_unprovisioned_devices: cb:%x\n", (uint32_t)unprovisioned_cb);
    ctx.unprovisioned_cb = unprovisioned_cb;
}

/**
* Mesh core library function to stop mesh
*
* This function is to stop the mesh function.
*
* Parameters: None
*
* Return: None
*
*/
void mesh_stop_mesh(void)
{
    TRACE1(TRACE_INFO, "stop_mesh: beacon_instance:%x\n", ctx.beacon_instance[0]);

    ctx.mesh_started = WICED_FALSE;
    // Stops proxy server advertisement and network secure beacon if they are running
    mesh_update_beacon();

}

#endif

