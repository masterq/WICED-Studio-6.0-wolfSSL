
// BtInterface.h : header file
//

#pragma once

typedef enum {
    OSVERSION_WINDOWS_7 = 0,
    OSVERSION_WINDOWS_8,
    OSVERSION_WINDOWS_10
}tOSVersion;


class CBtInterface
{
public:
    CBtInterface(BLUETOOTH_ADDRESS *bth, HMODULE hLib, LPVOID NotificationContext, tOSVersion osversion)
    {
        m_bth = *bth;
        m_hLib = hLib;
        m_NotificationContext = NotificationContext;
        m_osversion = osversion;
        m_bWin8 = (osversion == OSVERSION_WINDOWS_8) ? TRUE : FALSE;
    };

    virtual BOOL Init() = NULL;
    virtual BOOL SetDescriptorValue(const GUID *p_guidServ, const GUID *p_guidChar, USHORT uuidDescr, BTW_GATT_VALUE *pValue) = NULL;
    virtual BOOL WriteCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar, BOOL without_resp, BTW_GATT_VALUE *pValue) = NULL;

    BOOL SendWsUpgradeCommand(BTW_GATT_VALUE *pValue);
    BOOL SendWsUpgradeCommand(BYTE Command);
    BOOL SendWsUpgradeCommand(BYTE Command, USHORT sParam);
    BOOL SendWsUpgradeCommand(BYTE Command, ULONG lParam);
    BOOL SendWsUpgradeData(BYTE *Data, DWORD len);

    BLUETOOTH_ADDRESS m_bth;
    HMODULE m_hLib;
    LPVOID m_NotificationContext;
    BOOL m_bWin8;
    tOSVersion m_osversion;
};

