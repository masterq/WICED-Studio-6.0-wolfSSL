#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#ifndef MESH_CONTROLLER
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_cfg.h"
#include "wiced_hal_gpio.h"
#include "wiced_bt_uuid.h"
#include "wiced_result.h"
#include "wiced_bt_trace.h"
//#include "wiced_bt_stack.h"
#include "wiced_gki.h"
#include "wiced_bt_app_common.h"
#include "wiced_bt_app_hal_common.h"
#include "wiced_hal_nvram.h"

#include "wiced_hal_platform.h"
#include "wiced_transport.h"
#include "wiced_gki.h"
#endif

//#include "wiced.h"
//#include "brcm_fw_types.h"

void* memcpy(void* _Dst, void const* _Src, unsigned int _Size);

#ifndef UINT32
typedef unsigned int UINT32;
#endif
typedef unsigned long DWORD;
typedef unsigned char UINT8;
typedef unsigned char BYTE;
typedef unsigned short UINT16;
#if 1
#ifndef BOOL
typedef int BOOL;
//typedef unsigned char BOOL;
#endif
#endif

#define TRUE 1
#define FALSE 0
typedef int INT32;
#define BT_MEMSET memset
#define BT_MEMCPY memcpy
typedef unsigned long long UINT64;
void* wiced_memory_allocate(UINT32 length);
void* wiced_memory_permanent_allocate(UINT32 length);
void wiced_memory_free(void *memoryBlock);
#define smp_aes_set_key aes_set_key
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef unsigned char uint8_t;
typedef signed char int8_t;
typedef unsigned int uint32_t;
typedef signed int int32_t;
typedef unsigned long long uint64_t;
typedef signed long long int64_t;
typedef void(*wiced_timer_callback_fp)(uint32_t cBackparam);
enum wiced_timer_type_e
{
    WICED_SECONDS_TIMER = 1,
    WICED_MILLI_SECONDS_TIMER, /* The minimum resolution supported is 1 ms */
    WICED_SECONDS_PERIODIC_TIMER,
    WICED_MILLI_SECONDS_PERIODIC_TIMER /*The minimum resolution supported is 1 ms */
};
typedef UINT8 wiced_timer_type_t;/* (see #wiced_timer_type_e) */
typedef struct
{
    wiced_timer_callback_fp cback;
    uint32_t                cback_param;
    wiced_timer_type_t      type;
    uint32_t                timeout;
    uint64_t                end_time;
    void                    *next;  //wiced_timer_t*
} wiced_timer_t;
//typedef int wiced_bool_t;
typedef unsigned int   wiced_bool_t;
#define WICED_TRUE 1
#define WICED_FALSE 0
typedef uint16_t wiced_result_t;
#define WICED_ERROR 4
wiced_result_t wiced_init_timer(wiced_timer_t* p_timer, wiced_timer_callback_fp TimerCb, uint32_t cBackparam, wiced_timer_type_t type);
wiced_result_t wiced_start_timer(wiced_timer_t* p_timer, uint32_t timeout);
wiced_result_t wiced_stop_timer(wiced_timer_t* p_timer);
UINT32 mesh_read_node_info(int inx, UINT8* node_info, UINT16 len, wiced_result_t *p_result);
UINT32 mesh_write_node_info(int inx, const UINT8* node_info, UINT16 len, wiced_result_t *p_result);
#define BTM_INQ_RES_IGNORE_RSSI     0x7f    /**< RSSI value not supplied (ignore it) */
#define WICED_BT_SUCCESS    0
#define WICED_BT_ERROR      1
#ifndef NULL
    #ifdef __cplusplus
        #define NULL 0
    #else
        #define NULL ((void *)0)
    #endif
#endif
uint8_t wiced_hal_read_nvram(uint16_t vs_id,
    uint8_t          data_length,
    uint8_t        * p_data,
    wiced_result_t * p_status);

void *wiced_bt_get_buffer(uint32_t len);
void wiced_bt_free_buffer(void* buffer);
uint64_t clock_SystemTimeMicroseconds64(void);

#endif /* __PLATFORM_H__ */
