/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
*
* This is based upon BCM2045 Debug Framework Software Design Document ver 0.6
*/

#ifndef __DBFW_APP_H__
#define __DBFW_APP_H__

// Levels of Trace
enum
{
    TRACE_DEBUG     = 0,
    TRACE_INFO      = 1,
    TRACE_WARNING   = 2,
    TRACE_CRITICAL  = 3,
};

#ifndef TRACE_COMPILE_LEVEL
#define TRACE_COMPILE_LEVEL 3       // Default Trace Compile Level
#endif

#if (TRACE_COMPILE_LEVEL == 0)
#define TRACE_TRACE_DEBUG(module, msg, var) trace(TVF_WW(TRACE_DEBUG, module), FID, __LINE__,var)
#endif

#if (TRACE_COMPILE_LEVEL <= 1)
#define TRACE_TRACE_INFO(module, msg, var) trace(TVF_WW(TRACE_INFO, module), FID, __LINE__,var)
#endif

#if (TRACE_COMPILE_LEVEL <= 2)
#define TRACE_TRACE_WARNING(module, msg, var) trace(TVF_WW(TRACE_WARNING, module), FID, __LINE__,var)
#endif

#if (TRACE_COMPILE_LEVEL <= 3)
#define TRACE_TRACE_CRITICAL(module, msg, var) trace(TVF_WW(TRACE_CRITICAL, module), FID, __LINE__,var)
#endif

#if !defined(TRACE_TRACE_WARNING)
#define TRACE_TRACE_WARNING(module, msg, var)
#define TRACE_TS_TRACE_WARNING(module, msg, var)
#endif

#if !defined(TRACE_TRACE_INFO)
#define TRACE_TRACE_INFO(module, msg, var)
#define TRACE_TS_TRACE_INFO(module, msg, var)
#endif

#if !defined(TRACE_TRACE_DEBUG)
#define TRACE_TRACE_DEBUG(module, msg, var)
#define TRACE_TS_TRACE_DEBUG(module, msg, var)
#endif

#define TRACE(level, module, msg, var) TRACE_##level(module, msg, var)


#define TRACE_FORCE_DUMP()     dbfw_ForceDump()

#endif  // end of __DBFW_H__

