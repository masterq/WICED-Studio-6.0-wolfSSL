/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/
/** @file
*
* Generic model OnOff implementation.
*/

#include <string.h> 
#include "wiced_bt_ble.h"
#include "wiced_bt_mesh_event.h"
#include "mesh_core.h"
#include "foundation_int.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__MESH_EVENT_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__MESH_EVENT_C
#include "mesh_trace.h"

/*
 * Create message reply mesh event from the received mesh event
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_create_reply_event(wiced_bt_mesh_event_t *p_event)
{
    TRACE1(TRACE_DEBUG, "create_reply_event: p_event:%x\n", (uint32_t)p_event);
    p_event->dst                = p_event->src;
    p_event->src                = node_nv_data.node_id + p_event->element_idx;
    p_event->reply              = WICED_FALSE;
    p_event->ttl                = node_nv_data.state_default_ttl;
    p_event->retransmit         = 0x41;
    return p_event;
}

/*
 * Create the copy of the existing mesh event 
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_copy_event(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_event_t *p_copy_event = (wiced_bt_mesh_event_t*)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_event_t));

    TRACE1(TRACE_DEBUG, "wiced_bt_mesh_copy_event: p_event:%x\n", (uint32_t)p_event);
    TRACE1(TRACE_DEBUG, " p_copy_event:%x\n", (uint32_t)p_copy_event);

    if (p_copy_event)
        memcpy(p_copy_event, p_event, sizeof(wiced_bt_mesh_event_t));

    return p_copy_event;
}

/*
 * Create mesh event for unsolicited message
 */
wiced_bt_mesh_event_t *wiced_bt_mesh_create_event(uint8_t element_idx, uint16_t company_id, uint16_t model_id, uint16_t dst, uint16_t app_key_idx)
{
    wiced_bt_mesh_event_t *p_event = (wiced_bt_mesh_event_t*)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_event_t));

    TRACE2(TRACE_DEBUG, "create_unsolicited_event: dst:%x app_key_idx:%x\n", dst, app_key_idx);

    if (p_event == NULL)
    {
        TRACE0(TRACE_WARNING, "create_unsolicited_event: wiced_bt_get_buffer failed\n");
        return NULL;
    }
    TRACE1(TRACE_DEBUG, "create_unsolicited_event: p_event:%x\n", (uint32_t)p_event);

    memset(p_event, 0, sizeof(wiced_bt_mesh_event_t));

    p_event->opcode = WICED_BT_MESH_OPCODE_UNKNOWN;
    p_event->element_idx = element_idx;
    p_event->company_id = company_id;
    p_event->model_id = model_id;
    // if it is special case with 0xffff company_id then just create mesh event with default ttl and 0x41 retransmit
    if (company_id == 0xffff)
    {
        p_event->ttl = node_nv_data.state_default_ttl;
        p_event->retransmit = 0x41;
    }
    else if (dst != MESH_NODE_ID_INVALID)
    {
        if (app_key_idx == 0xffff)
        {
            p_event->app_key_idx = 0xff;
        }
        else if (!foundation_find_appkey_(app_key_idx, &p_event->app_key_idx, NULL, NULL))
        {
            TRACE1(TRACE_WARNING, "create_unsolicited_event: no app_key_idx:%x\n", app_key_idx);
            wiced_bt_free_buffer(p_event);
            p_event = NULL;
            return NULL;
        }
        p_event->dst = dst;
        p_event->ttl = node_nv_data.state_default_ttl;
        p_event->credential_flag = WICED_FALSE;
        p_event->period = 0;
        p_event->retransmit = 0;
    }
    else
    {
        if (wiced_bt_mesh_core_get_publication(p_event))
        {
            wiced_bt_free_buffer(p_event);
            p_event = NULL;
            return NULL;
        }
    }

    p_event->src = node_nv_data.node_id + p_event->element_idx;
    p_event->reply = WICED_FALSE;
    return p_event;
}

/*
 * Release mesh event
 */
void wiced_bt_mesh_release_event(wiced_bt_mesh_event_t *p_event)
{
    TRACE1(TRACE_DEBUG, "release_event: p_event:%x\n", (uint32_t)p_event);
    wiced_bt_free_buffer(p_event);
}

