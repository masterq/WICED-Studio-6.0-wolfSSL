
// MeshControllerDlg.cpp : implementation file
//

#include "stdafx.h"
#include <setupapi.h>
#include "MeshController.h"
#include "MeshControllerDlg.h"
#include "afxdialogex.h"

#include <vector>

#define ASSERT_SUCCEEDED(hr) {if(FAILED(hr)) return (hr == S_OK);}
#define RETURN_IF_FAILED(p, q) { FAILED(hr) ? { cout << p;} : q;}

typedef ITypedEventHandler<GattCharacteristic *, GattValueChangedEventArgs *> ValueChangedHandler;
typedef ITypedEventHandler<BluetoothLEDevice *, IInspectable *> StatusHandler;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//converts BD_ADDR to the __int64
#define bda2int(a) (a ? ((((__int64)a[0])<<40)+(((__int64)a[1])<<32)+(((__int64)a[2])<<24)+(((__int64)a[3])<<16)+(((__int64)a[4])<<8)+((__int64)a[5])) : 0)

Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::IBluetoothLEDevice> mDevice;

EventRegistrationToken mStatusChangedToken;

struct ValueChangedEntry {
	ValueChangedEntry() {}
	ValueChangedEntry(Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattCharacteristic> c,
		EventRegistrationToken t)
		: characteristic(c)
		, token(t)
	{
	}

	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattCharacteristic> characteristic;
	EventRegistrationToken token;
};

std::vector <ValueChangedEntry> mValueChangedTokens;

struct ServiceCharEntry {
	ServiceCharEntry() {}
	ServiceCharEntry(GUID g, ComPtr<IVectorView<GattCharacteristic *>>  c)
		: svcGuid(g)
		, charVec(c)
	{
	}

	ComPtr<IVectorView<GattCharacteristic *>> charVec;
	GUID svcGuid;
};

std::vector <ServiceCharEntry> mServiceCharsVector;

tServiceState m_state;
tError m_error;

void CBtWin10Interface::setState(tServiceState state)
{
	m_state = state;
}

void CBtWin10Interface::setError(tError error)
{
	m_error = error;
}

char* guid2str(GUID guid)
{
	WCHAR szService[80];
	static char buff[256] = { 0 };
	UuidToString(szService, 80, &guid);
	sprintf_s(buff, "%S", szService);
	return buff;
}

template <typename T>
inline HRESULT _await_impl(const Microsoft::WRL::ComPtr<T> &asyncOp, UINT timeout)
{
	Microsoft::WRL::ComPtr<IAsyncInfo> asyncInfo;
	HRESULT hr = asyncOp.As(&asyncInfo);
	if (FAILED(hr))
		return hr;

	AsyncStatus status;

	while (SUCCEEDED(hr = asyncInfo->get_Status(&status)) && status == AsyncStatus::Started)
	{
		Sleep(1000);
	}

	if (FAILED(hr) || status != AsyncStatus::Completed) {
		HRESULT ec;
		hr = asyncInfo->get_ErrorCode(&ec);
		if (FAILED(hr))
			return hr;
		hr = asyncInfo->Close();
		if (FAILED(hr))
			return hr;
		return ec;
	}

	return hr;
}

template <typename T, typename U>
inline HRESULT await(const Microsoft::WRL::ComPtr<T> &asyncOp, U *results, UINT timeout = 0)
{
	HRESULT hr = _await_impl(asyncOp, timeout);
	if (FAILED(hr))
		return hr;

	return asyncOp->GetResults(results);
}

static char* byteArrayFromBuffer(const ComPtr<IBuffer> &buffer, bool isWCharString = false)
{
	ComPtr<Windows::Storage::Streams::IBufferByteAccess> byteAccess;
	HRESULT hr = buffer.As(&byteAccess);
	char *data;
	hr = byteAccess->Buffer(reinterpret_cast<byte **>(&data));
	UINT32 size;
	hr = buffer->get_Length(&size);

	return data;
}

ComPtr<IGattDeviceService> CBtWin10Interface::getNativeService(const GUID *p_guidServ)
{
	GUID serviceUuid = *p_guidServ;
	ComPtr<IGattDeviceService> deviceService;

	HRESULT hr;
	hr = mDevice->GetGattService(serviceUuid, &deviceService);
	if (FAILED(hr))
		cout << "Could not obtain native service for Uuid" << guid2str(serviceUuid);
	return deviceService;
}

ComPtr<IGattCharacteristic> CBtWin10Interface::getNativeCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar)
{
	GUID serviceUuid = *p_guidServ;
	GUID  charUuid = *p_guidChar;

	ComPtr<IGattCharacteristic> characteristic;

	cout << __FUNCTION__;

	for (const ServiceCharEntry &entry : mServiceCharsVector) {
		HRESULT hr;

		if (memcmp(&serviceUuid, &entry.svcGuid, sizeof(GUID)) == 0)
		{
			UINT characteristicsCount = 0;
			hr = entry.charVec->get_Size(&characteristicsCount);

			for (UINT j = 0; j < characteristicsCount; j++) {
				hr = entry.charVec->GetAt(j, &characteristic);
				GUID charuuid = { 0 };
				characteristic->get_Uuid(&charuuid);
				if (memcmp(&charuuid, &charUuid, sizeof(GUID)) == 0)
					return characteristic;
			}
		}
	}

	return characteristic;
}

void CBtWin10Interface::PostNotification(int charHandle, ComPtr<IBuffer> buffer)
{
	cout << "Characteristic change notification";
	ComPtr<Windows::Storage::Streams::IBufferByteAccess> byteAccess;
	HRESULT hr = buffer.As(&byteAccess);

	char *data;
	hr = byteAccess->Buffer(reinterpret_cast<byte **>(&data));

	UINT32 size;
	hr = buffer->get_Length(&size);

	ComPtr<IGattCharacteristic> characteristic;
	BOOL bFound = FALSE;

	cout << __FUNCTION__;

	GUID charuuid = { 0 };
	UINT16 charattrHandle = 0;

	for (const ServiceCharEntry &entry : mServiceCharsVector)
	{
		HRESULT hr;

		UINT characteristicsCount = 0;
		hr = entry.charVec->get_Size(&characteristicsCount);

		for (UINT j = 0; j < characteristicsCount; j++) {
			hr = entry.charVec->GetAt(j, &characteristic);

			characteristic->get_Uuid(&charuuid);
			charattrHandle = 0;
			characteristic->get_AttributeHandle(&charattrHandle);

			if (charattrHandle == charHandle)
			{
				bFound = TRUE;
				break;
			}
		}

		if (bFound)
			break;
	}

	if (!bFound)
		return;

	CMeshControllerDlg *pDlg = (CMeshControllerDlg *)m_NotificationContext;

	DWORD dwCharInstance = 0;

	BTW_GATT_VALUE *p = (BTW_GATT_VALUE *)malloc(sizeof(BTW_GATT_VALUE));
	if (!p)
		return;

	p->len = (USHORT)size;
	memcpy(p->value, data, size);

	pDlg->PostMessage(WM_USER + (charuuid.Data1 & 0xff), (WPARAM)dwCharInstance, (LPARAM)p);
}

BOOL CBtWin10Interface::RegisterNotification(const GUID *p_guidServ, const GUID *p_guidChar)
{
	GUID serviceUuid = *p_guidServ;
	GUID charUuid = *p_guidChar;

	cout << "RegisterNotification characteristic " << guid2str(charUuid) << " in service " << guid2str(serviceUuid) << " for value changes";

	for (const ValueChangedEntry &entry : mValueChangedTokens)
	{
		GUID guuid;
		HRESULT hr;
		hr = entry.characteristic->get_Uuid(&guuid);
		ASSERT_SUCCEEDED(hr);

		if (memcmp(&guuid, &charUuid, sizeof(GUID)) == 0)
		{
			cout << "Already registered" << endl;
			return FALSE;
		}
	}

	ComPtr<IGattCharacteristic> characteristic = getNativeCharacteristic(p_guidServ, p_guidChar);

	if (!characteristic)
	{
		cout << "Characteristic NULL" << endl;
		return FALSE;
	}

	EventRegistrationToken token;
	HRESULT hr;
	hr = characteristic->add_ValueChanged(Callback<ValueChangedHandler>([this](IGattCharacteristic *characteristic, IGattValueChangedEventArgs *args) {
		HRESULT hr;
		UINT16 handle;
		hr = characteristic->get_AttributeHandle(&handle);
		ASSERT_SUCCEEDED(hr);
		ComPtr<IBuffer> buffer;
		hr = args->get_CharacteristicValue(&buffer);
		ASSERT_SUCCEEDED(hr);
		PostNotification(handle, buffer);

		return (bool)TRUE;

	}).Get(), &token);
	ASSERT_SUCCEEDED(hr);

	mValueChangedTokens.push_back(ValueChangedEntry(characteristic, token));

	return TRUE;
}

BOOL CBtWin10Interface::Init()
{
	cout << __FUNCTION__;

	__int64 remoteDevice = bda2int(m_bth.rgBytes);

	if (remoteDevice == 0) {
		cout << "Invalid/null remote device address";
		setError(UnknownRemoteDeviceError);
		return FALSE;
	}

	setState(ConnectingState);

	HRESULT hr;
	ComPtr<IBluetoothLEDeviceStatics> deviceStatics;
	hr = GetActivationFactory(HString::MakeReference(RuntimeClass_Windows_Devices_Bluetooth_BluetoothLEDevice).Get(), &deviceStatics);
	ASSERT_SUCCEEDED(hr);
	ComPtr<IAsyncOperation<BluetoothLEDevice *>> deviceFromIdOperation;
	hr = deviceStatics->FromBluetoothAddressAsync(remoteDevice, &deviceFromIdOperation);
	ASSERT_SUCCEEDED(hr);
	hr = await(deviceFromIdOperation, mDevice.GetAddressOf());
	ASSERT_SUCCEEDED(hr);

	if (!mDevice) {
		cout << "Could not find LE device";
		setError(InvalidBluetoothAdapterError);
		setState(UnconnectedState);
		return FALSE;
	}
	BluetoothConnectionStatus status;
	hr = mDevice->get_ConnectionStatus(&status);
	ASSERT_SUCCEEDED(hr);

	hr = mDevice->add_ConnectionStatusChanged(Callback<StatusHandler>([this](IBluetoothLEDevice *dev, IInspectable *) {
		BluetoothConnectionStatus status;
		HRESULT hr;
		hr = dev->get_ConnectionStatus(&status);
		ASSERT_SUCCEEDED(hr);
		if (m_state == ConnectingState
			&& status == BluetoothConnectionStatus::BluetoothConnectionStatus_Connected) {
			setState(ConnectedState);

			// PostMessage Connected message
		}
		else if (m_state == ConnectedState
			&& status == BluetoothConnectionStatus::BluetoothConnectionStatus_Disconnected) {
			setState(UnconnectedState);
			// PostMessage disconnected message
		}
		return (bool)TRUE;
	}).Get(), &mStatusChangedToken);
	ASSERT_SUCCEEDED(hr);

	ASSERT_SUCCEEDED(hr);

	if (status == BluetoothConnectionStatus::BluetoothConnectionStatus_Connected) {
		setState(ConnectedState);
		// PostMessage Connected message
		return TRUE;
	}


	ComPtr<IAsyncOperation<GattDeviceServicesResult*>> op;
	ComPtr<IBluetoothLEDevice3> mDevice3;
	hr = mDevice.As(&mDevice3);

	hr = mDevice3->GetGattServicesWithCacheModeAsync(BluetoothCacheMode_Cached, &op);
	ASSERT_SUCCEEDED(hr);

	ComPtr<IGattDeviceServicesResult> servicesresult;
	hr = await(op, servicesresult.GetAddressOf());
	ASSERT_SUCCEEDED(hr);

	ComPtr<IVectorView <GattDeviceService *>> deviceServices;
	hr = servicesresult->get_Services(&deviceServices);
	ASSERT_SUCCEEDED(hr);

	UINT serviceCount;
	hr = deviceServices->get_Size(&serviceCount);
	ASSERT_SUCCEEDED(hr);

	for (UINT i = 0; i < serviceCount; i++) {

		ComPtr<IGattDeviceService> service;
		hr = deviceServices->GetAt(i, &service);
		ASSERT_SUCCEEDED(hr);

		GUID uuid = { 0 };
		service->get_Uuid(&uuid);

		UINT16 attribHandle = 0;
		service->get_AttributeHandle(&attribHandle);

		ComPtr<IGattDeviceService3> service3;
		hr = service.As(&service3);
		ASSERT_SUCCEEDED(hr);

		ComPtr<IAsyncOperation<GattCharacteristicsResult*>> charop;
		ComPtr<IGattCharacteristicsResult> charresult;
		ComPtr<IVectorView<GattCharacteristic *>> characteristics;

		hr = service3->GetCharacteristicsWithCacheModeAsync(BluetoothCacheMode_Cached, &charop);
		ASSERT_SUCCEEDED(hr);

		hr = await(charop, charresult.GetAddressOf());
		ASSERT_SUCCEEDED(hr);

		hr = charresult->get_Characteristics(&characteristics);
		ASSERT_SUCCEEDED(hr);

		if (hr == E_ACCESSDENIED) {
			// Everything will work as expected up until this point if the manifest capabilties
			// for bluetooth LE are not set.
			cout << "Could not obtain characteristic list. Please check your manifest capabilities";
			setState(UnconnectedState);
			setError(ConnectionError);
			return FALSE;
		}
		else {
			ASSERT_SUCCEEDED(hr);
		}

		mServiceCharsVector.push_back(ServiceCharEntry(uuid, characteristics));

		UINT characteristicsCount;
		hr = characteristics->get_Size(&characteristicsCount);
		ASSERT_SUCCEEDED(hr);
		for (UINT j = 0; j < characteristicsCount; j++) {
			ComPtr<IGattCharacteristic> characteristic;
			hr = characteristics->GetAt(j, &characteristic);
			ASSERT_SUCCEEDED(hr);

			GUID charuuid = { 0 };
			characteristic->get_Uuid(&charuuid);
			UINT16 charattrHandle = 0;
			characteristic->get_AttributeHandle(&charattrHandle);

			ComPtr<IAsyncOperation<GattReadResult *>> op;
			GattCharacteristicProperties props;
			hr = characteristic->get_CharacteristicProperties(&props);
			ASSERT_SUCCEEDED(hr);
			if (!(props & GattCharacteristicProperties_Read))
				continue;
			//hr = characteristic->ReadValueWithCacheModeAsync(BluetoothCacheMode::BluetoothCacheMode_Uncached, &op);
			//ASSERT_SUCCEEDED(hr);
			//ComPtr<IGattReadResult> result;
			//hr = await(op, result.GetAddressOf());
			//ASSERT_SUCCEEDED(hr);
			//ComPtr<ABI::Windows::Storage::Streams::IBuffer> buffer;
			//hr = result->get_Value(&buffer);
			//ASSERT_SUCCEEDED(hr);
			//if (!buffer) {
			//	cout << "Problem reading value";
			//	setError(ConnectionError);
			//	setState(UnconnectedState);
			//}
				//return TRUE;

			////== Get descriptors

			//ComPtr<IAsyncOperation<GattDescriptorsResult*>> descop;
			//ComPtr<IGattDescriptorsResult> descresult;
			//ComPtr<IVectorView<GattDescriptor *>> descs;
			//ComPtr<IGattCharacteristic3> characteristic3;
			//hr = characteristic.As(characteristic3);

			//hr = characteristic3->GetDescriptorsAsync(&descop);
			//ASSERT_SUCCEEDED(hr);
			//hr = await(descop, descresult.GetAddressOf());
			//ASSERT_SUCCEEDED(hr);

			//hr = descresult->get_Descriptors(&descs);
			//ASSERT_SUCCEEDED(hr);
			////== Get descriptors
		}
	}

	return TRUE;
}

CBtWin10Interface::CBtWin10Interface(BLUETOOTH_ADDRESS *bth, LPVOID NotificationContext)
	: CBtInterface(bth, NULL, NotificationContext, OSVERSION_WINDOWS_10)
{
}

CBtWin10Interface::~CBtWin10Interface()
{
	if (mDevice && mStatusChangedToken.value)
		mDevice->remove_ConnectionStatusChanged(mStatusChangedToken);

	cout << "Unregistering " << mValueChangedTokens.size() << " value change tokens";

	for (const ValueChangedEntry &entry : mValueChangedTokens)
		entry.characteristic->remove_ValueChanged(entry.token);

	mValueChangedTokens.clear();
	mServiceCharsVector.clear();
}

void CBtWin10Interface::ResetInterface()
{
	if (mDevice && mStatusChangedToken.value)
		mDevice->remove_ConnectionStatusChanged(mStatusChangedToken);

	cout << "Unregistering " << mValueChangedTokens.size() << " value change tokens";

	for (const ValueChangedEntry &entry : mValueChangedTokens)
		entry.characteristic->remove_ValueChanged(entry.token);

	mValueChangedTokens.clear();
	mServiceCharsVector.clear();
}

BOOL CBtWin10Interface::SetDescriptorValue(const GUID *p_guidServ, const GUID *p_guidChar, USHORT uuidDescr, BTW_GATT_VALUE *pValue)
{
	GUID serviceUuid = *p_guidServ;
	GUID charUuid = *p_guidChar;

	cout << "SetDescriptorValue characteristic " << guid2str(charUuid) << " in service " << guid2str(serviceUuid) << " for value changes";
	cout << "Descriptor " << uuidDescr << endl;

	HRESULT hr;
	ComPtr<IGattCharacteristic> characteristic = getNativeCharacteristic(p_guidServ, p_guidChar);
	if (!characteristic) {
		cout << "Could not obtain native characteristic";

		return S_OK;
	}

	GattClientCharacteristicConfigurationDescriptorValue value;

	if (pValue->value[0] == 1)
		value = GattClientCharacteristicConfigurationDescriptorValue_Notify;
	else if (pValue->value[0] == 2)
		value = GattClientCharacteristicConfigurationDescriptorValue_Indicate;
	else
		value = GattClientCharacteristicConfigurationDescriptorValue_None;

	ComPtr<IAsyncOperation<enum GattCommunicationStatus>> writeOp;
	hr = characteristic->WriteClientCharacteristicConfigurationDescriptorAsync(value, &writeOp);
	ASSERT_SUCCEEDED(hr);
	hr = writeOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCommunicationStatus >>([this](IAsyncOperation<GattCommunicationStatus> *op, AsyncStatus status)
	{
		if (status == AsyncStatus::Canceled || status == AsyncStatus::Error) {
			cout << "Descriptor write operation failed";
			//service->setError(QLowEnergyService::DescriptorWriteError);
			return S_OK;
		}
		GattCommunicationStatus result;
		HRESULT hr;
		hr = op->GetResults(&result);
		if (FAILED(hr)) {
			cout << "Could not obtain result for descriptor";

			return S_OK;
		}
		if (result != GattCommunicationStatus_Success) {
			cout << "Descriptor write operation failed";

			return S_OK;
		}
		return S_OK;
	}).Get());
	ASSERT_SUCCEEDED(hr);
	return S_OK;
}

BOOL CBtWin10Interface::WriteCharacteristic(const GUID *p_guidServ, const GUID *p_guidChar, BOOL without_resp, BTW_GATT_VALUE *pValue)
{
	GUID serviceUuid = *p_guidServ;
	GUID charUuid = *p_guidChar;

	cout << "WriteCharacteristic characteristic " << guid2str(charUuid) << " in service " << guid2str(serviceUuid) << " for value changes";
	const bool writeWithResponse = (without_resp == GattWriteOption_WriteWithResponse);

	HRESULT hr;
	ComPtr<IGattCharacteristic> characteristic = getNativeCharacteristic(p_guidServ, p_guidChar);
	if (!characteristic) {
		cout << "Could not obtain native characteristic";

		return FALSE;
	}
	ComPtr<ABI::Windows::Storage::Streams::IBufferFactory> bufferFactory;
	hr = GetActivationFactory(HStringReference(RuntimeClass_Windows_Storage_Streams_Buffer).Get(), &bufferFactory);
	ASSERT_SUCCEEDED(hr);
	ComPtr<ABI::Windows::Storage::Streams::IBuffer> buffer;
	const int length = pValue->len;
	hr = bufferFactory->Create(length, &buffer);
	ASSERT_SUCCEEDED(hr);
	hr = buffer->put_Length(length);
	ASSERT_SUCCEEDED(hr);
	ComPtr<Windows::Storage::Streams::IBufferByteAccess> byteAccess;
	hr = buffer.As(&byteAccess);
	ASSERT_SUCCEEDED(hr);
	byte *bytes;
	hr = byteAccess->Buffer(&bytes);
	ASSERT_SUCCEEDED(hr);
	memcpy(bytes, pValue->value, length);
	ComPtr<IAsyncOperation<GattCommunicationStatus>> writeOp;
	GattWriteOption option = writeWithResponse ? GattWriteOption_WriteWithResponse : GattWriteOption_WriteWithoutResponse;
	hr = characteristic->WriteValueWithOptionAsync(buffer.Get(), option, &writeOp);
	ASSERT_SUCCEEDED(hr);

	hr = writeOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCommunicationStatus>>([this](IAsyncOperation<GattCommunicationStatus> *op, AsyncStatus status)
	{
		if (status == AsyncStatus::Canceled || status == AsyncStatus::Error) {
			cout << "Characteristic write operation failed";
			return FALSE;
		}
		GattCommunicationStatus result;
		HRESULT hr;
		hr = op->GetResults(&result);
		if (hr == E_BLUETOOTH_ATT_INVALID_ATTRIBUTE_VALUE_LENGTH) {
			cout << "Characteristic write operation was tried with invalid value length";
			return FALSE;
		}

		if (result != GattCommunicationStatus_Success) {
			return FALSE;
		}
		return TRUE;
	}).Get());


	ASSERT_SUCCEEDED(hr);
	return hr == S_OK;
}

