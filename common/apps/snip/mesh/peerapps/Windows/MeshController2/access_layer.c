/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Access layer implementation.
*/
#include <stdio.h> 
#include <string.h> 
#include "platform.h"

#include "mesh_core.h"
#include "core_aes_ccm.h"
#include "mesh_util.h"
#include "transport_layer.h"
#include "access_layer.h"
#include "key_refresh.h"
#include "foundation.h"
#include "network_layer.h"
#include "foundation_int.h"
#include "wiced_bt_mesh_model_defs.h"
#include "low_power.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__ACESS_LAYER_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__ACESS_LAYER_C
#include "mesh_trace.h"

/**
* Encrypts and authenticates access payload and passes it to transport layer for sending
*
* Parameters:
*   payload:            Access payload.
*   payload_len:        Length of the access payload
*   app_key_idx:        Index if the application key. Value 0xff means device key
*   element_idx:        Index of the source element
*   dst:                Destination address.
*   ttl:                Message TTL.
*   credential_flag:    Value of the Friendship Credential Flag. It can be TRUE for LPN only.
*   complete_callback:  Callback function to be called at the end of all retransmissions. Can be NULL.
*   p_event:            Mesh event to be used in the complete_callback
*
* Return:   WICED_TRUE if send started successfully. WICED_FALSE - failed to start send.
*/
wiced_bool_t access_layer_send(const uint8_t* payload, uint16_t payload_len, uint8_t app_key_idx, uint8_t element_idx, uint16_t dst, uint8_t ttl, wiced_bool_t credential_flag,
    wiced_bt_mesh_core_send_complete_callback_t complete_callback, wiced_bt_mesh_event_t *p_event)
{
    wiced_bool_t    ret;
    uint8_t         data[MESH_MAX_APP_PAYLOAD_LEN + MESH_MIC_LEN2];
    uint8_t         seq_src_dst[MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN + MESH_NODE_ID_LEN];
    wiced_bool_t    szmic;
    uint8_t         net_iv_index[MESH_IV_INDEX_LEN];
    uint8_t         *p_net_iv_index;

    TRACE4(TRACE_WARNING, "access_layer_send: app_key_idx:%x ttl:%x element_idx:%x credential_flag:%d\n", app_key_idx, ttl, element_idx, credential_flag);
    TRACE2(TRACE_WARNING, " src:%04x dst:%x\n", (node_nv_data.node_id + element_idx), dst);
    TRACEN(TRACE_WARNING, (char*)payload, payload_len);

    // if application requested to use default TTL use the configured one
    if (ttl == MESH_USE_DEFAULT_TTL)
    {
#ifndef MESH_CONTROLLER
        ttl = ctx.default_ttl;
#else
        ttl = MESH_DEFAULT_TTL;
#endif
    }

    UINT32TOBE3(seq_src_dst, node_nv_data.sequence_number);
    UINT16TOBE2(seq_src_dst + MESH_PKT_SEQ_LEN, (node_nv_data.node_id + element_idx));
    UINT16TOBE2(seq_src_dst + MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN, dst);

    // Use short MIC if payload fits into unsegmented access messgae.
    // Use long MIC if it fit into max app payload
    if (payload_len <= MESH_MAX_UNSEGMENTED_APP_PAYLOAD_LEN
        || payload_len > (MESH_MAX_APP_PAYLOAD_LEN - MESH_MIC_LEN))
        szmic = WICED_FALSE;
    else
        szmic = WICED_TRUE;

    memcpy(data, payload, payload_len);

    if (node_nv_data.iv_update_state != MESH_IV_UPDT_STATE_ACTIVE)
        p_net_iv_index = node_nv_data.net_iv_index;
    else
    {
        UINT32 iv_index = BE4TOUINT32(node_nv_data.net_iv_index) - 1;
        UINT32TOBE4(net_iv_index, iv_index);
        p_net_iv_index = net_iv_index;
    }

    ret = mesh_ccm_crypt(WICED_TRUE, app_key_idx != 0xff ? MESH_NONCE_TYPE_APP : MESH_NONCE_TYPE_DEV,
        p_net_iv_index, seq_src_dst, ttl, WICED_FALSE, szmic,
        app_key_idx != 0xff ? node_nv_app_key[app_key_idx].key : node_nv_data.dev_key,
        data, payload_len);
    if(ret)
    {
        MeshSecMaterial *p_frnd_sec = NULL;
        // If credential_flag is TRUE then use friendship secure credentials
        if (credential_flag) {
            p_frnd_sec = low_power_get_sec_material();
            // if no friendship established then don't send that message
            if (p_frnd_sec == NULL)
            {
                TRACE0(TRACE_DEBUG, " no friendship secure material\n");
                ret = WICED_FALSE;
            }
        }
        if(ret)
            ret = transport_layer_send(data, payload_len + (szmic ? MESH_MIC_LEN2 : MESH_MIC_LEN), szmic, app_key_idx, p_frnd_sec, (node_nv_data.node_id + element_idx), dst, ttl, complete_callback, p_event);
    }
    TRACE1(TRACE_DEBUG, "access_layer_send: returns %d\n", ret);
    return ret;
}

/**
* Parses application payload
*
* Parameters:
*   payload:        The app payload to parse
*   payload_len:    Length of the app payload to parse
*   p_company_id:   Buffer for Manufacturer ID. 0 means no Manufacturer ID
*   p_opcode:       Buffer for opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*
*   Return:     Offset of the parameters in the app payload. On error returns 0;
*/
uint8_t access_layer_parse_payload(const uint8_t* payload, uint16_t payload_len, uint16_t *p_company_id, uint16_t *p_opcode)
{
    uint8_t ret = 0;
    uint16_t company_id = 0, opcode = 0;
    const uint8_t* p_payload = payload;
    //TRACE1(TRACE_DEBUG, "access_layer_parse_payload: len:%d\n", payload_len);
    //TRACEN(TRACE_DEBUG, (char*)payload, payload_len);

    do
    {
        //make sure payload has at least one byte opcode
        if (payload_len < 1)
            break;
        opcode = *p_payload++;
        payload_len--;
        if ((opcode & MESH_APP_PAYLOAD_OP_LONG) != 0)
        {
            if ((opcode & MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC) == 0)
            {
                //make sure payload has at least one more byte opcode
                if (payload_len < 1)
                    break;
                opcode = (opcode << 8) + *p_payload++;
                payload_len--;
                company_id = 0;
            }
            //make sure payload has at least two bytes of the company ID
            else if (payload_len < 2)
                break;
            else
            {
                company_id = (((uint16_t)*p_payload++) << 8) + *p_payload++;
                payload_len -= 2;
            }
        }
        ret = (uint8_t)(p_payload - payload);
    } while (WICED_FALSE);

    if (p_company_id)
        *p_company_id = company_id;
    if (p_opcode)
        *p_opcode = opcode;

    TRACE2(TRACE_DEBUG, "access_layer_parse_payload: ret:%d len:%d\n", ret, payload_len);
    TRACE2(TRACE_DEBUG, " opcode:%x company_id:%x\n", opcode, company_id);

    return ret;
}

/**
* Handles application payload
* Applycation layer should implement that function.
*
* Parameters:
*   akf:            Application Key flag. WICED_TRUE for application key; WICED_FALSE for device key
*   aid:            Three LSB of the Aplication ID
*   szmict:         Flag to use 64bits app MIC
*   payload:        Application payload. This function may change the data in the payload.
*   payload_len:    Length of the application payload includin MIC
*   iv_index:       IV index
*   net_key_idx:    Local index of the NetKey
*   seq_src_dst:    pointer on the place in the packet with SEQ || SRC
*   ttl:            TTL of the received packet
*   friend_idx:     -2 - network sec material;-1 - LPN sec material; >=0 - sec material of friend index
*
* Return:   WICED_TRUE if handled successfully. WICED_FALSE - failed to handle.
*/
wiced_bool_t access_layer_handle(wiced_bool_t akf, uint8_t aid, wiced_bool_t szmict, uint8_t* payload, uint16_t payload_len, const uint8_t* iv_index, uint8_t net_key_idx, const uint8_t* seq_src_dst, uint8_t ttl, int friend_idx)
{
    uint8_t app_key_idx;
    uint16_t company_id, opcode;
    uint8_t offset;
    uint8_t *decrypt_key = node_nv_data.dev_key;
    uint8_t type = MESH_NONCE_TYPE_DEV;

    TRACE4(TRACE_DEBUG, "access_layer_handle: akf:%d aid:%x szmict:%d friend_idx:%d\n", akf, aid, szmict, friend_idx);
    TRACE3(TRACE_DEBUG, " app_key_bitmask:%x net_key_idx:%x net_key-global_idx:%x\n", node_nv_data.app_key_bitmask, net_key_idx, node_nv_net_key[net_key_idx].global_idx);

    payload_len -= szmict ? MESH_MIC_LEN2 : MESH_MIC_LEN;

    // if it is encrypted by app key
    if (akf)
    {
        // go through all application keys
        for (app_key_idx = 0; app_key_idx < MESH_APP_KEY_MAX_NUM; app_key_idx++)
        {
            TRACE3(TRACE_DEBUG, "  app_key_idx:%x aid:%x global_net_key_idx:%x\n", app_key_idx, node_nv_app_key[app_key_idx].aid, node_nv_app_key[app_key_idx].global_net_key_idx);
            // ignore unexisting key
            if ((node_nv_data.app_key_bitmask & (1 << app_key_idx)) == 0)
                continue;
#ifndef MESH_CONTROLLER
            // ignore key if it is not bound to know NetKey
            if (node_nv_app_key[app_key_idx].global_net_key_idx != (node_nv_net_key[net_key_idx].global_idx & MESH_GL_KEY_IDX_MASK))
                continue;
#endif
            // ignore key with different app key id
            if (aid != node_nv_app_key[app_key_idx].aid)
                continue;
            break;
        }
        if (app_key_idx >= MESH_APP_KEY_MAX_NUM)
            return WICED_FALSE;
        type = MESH_NONCE_TYPE_APP;
        decrypt_key = node_nv_app_key[app_key_idx].key;
    }
    // now try to decrypt/authenticate
    if (!mesh_ccm_crypt(WICED_FALSE, type, iv_index, seq_src_dst, 0, WICED_FALSE, szmict, decrypt_key, payload, payload_len))
        return WICED_FALSE;
    // decrypted successfully
    TRACE1(TRACE_INFO, "access_layer_handle: ttl:%x **************************************\n", ttl);
    TRACEN(TRACE_INFO, (char*)payload, payload_len);
    offset = access_layer_parse_payload(payload, payload_len, &company_id, &opcode);
    if (!offset)
    {
        TRACE0(TRACE_WARNING, "access_layer_handle: wrong payload\n");
        return WICED_FALSE;
    }

    foundation_handle(
        BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN + MESH_NODE_ID_LEN),     //dst_id
        BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN),                              //src_id
        akf ? app_key_idx : 0xff,                                                       // application key index or 0xff for device key
        ttl, opcode, company_id, &payload[offset], payload_len - offset,    // ttl, opcode, payload
        friend_idx);

    return WICED_TRUE;
}

/**  
 * \brief Send message to the dst address.
 * \details Encrypts and authenticates application payload and passes it to transport layer and then to network layer for sending.
 * If it is used for sending the response on received message then it should use app_key_idx and SRC of the received message and default TTL.
 * If it is used for sending unsolicited message then application or model should get app_key_idx, DST and TTL from publication using wiced_bt_mesh_core_get_publication()
 *
 * @param[in]   p_event       :All details of the sending message
 * @param[in]   params          :Parameters of the Access Payload
 * @param[in]   params_len      :Length of the Parameters of the Access Payload
 * @param[in]   complete_callback   :Callback function to be called at the end of all retransmissions. Can be NULL.
 *
 * @return      wiced_result_t
 */
wiced_result_t wiced_bt_mesh_core_send(wiced_bt_mesh_event_t *p_event, const uint8_t* params, uint16_t params_len, wiced_bt_mesh_core_send_complete_callback_t complete_callback)
{
    wiced_result_t  res = WICED_BT_ERROR;
    uint8_t     payload[MESH_MAX_APP_PAYLOAD_LEN];
    uint8_t     *p;

    // if it is special command
    if (p_event->company_id == 0xffff)
    {
        switch (p_event->opcode)
        {
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_SET_TYPE:
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_ADD_ADDR:
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_DEL_ADDR:
        case WICED_BT_MESH_CORE_CMD_SPECIAL_PROXY_FLT_STATUS:
#ifndef MESH_CONTROLLER
            mesh_set_adv_params(0, 0, 0, 0, 0, 0);
#endif
            // use maser secure material
            if (network_layer_send((uint8_t*)&p_event->opcode, 1, params, (uint8_t)params_len, 0, NULL, WICED_TRUE, node_nv_data.node_id, MESH_NODE_ID_INVALID, 0, 0xffffffff, complete_callback, p_event))
                res = WICED_BT_SUCCESS;
            break;
        }
    }
    else
    {
        p = op2be(&payload[0], p_event->opcode);
        if (params_len)
        {
            memcpy(p, params, params_len);
            p = p + params_len;
        }

        // Device key may be used only in Configuration Model and Time Setup Server and some messages of the Time Client (note Configuration Server doesn't come here)
#ifndef _DEB_ENABLE_DEV_KEY_FOR_ALL_MODELS
        if (p_event->app_key_idx == 0xff)
        {
            if (p_event->model_id != WICED_BT_MESH_CORE_MODEL_ID_CONFIG_CLNT
                && p_event->model_id != WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV
                && p_event->model_id != WICED_BT_MESH_CORE_MODEL_ID_TIME_SETUP_SRV
                && (p_event->model_id != WICED_BT_MESH_CORE_MODEL_ID_TIME_CLNT
                    || (p_event->opcode != WICED_BT_MESH_OPCODE_TIME_SET
                        && p_event->opcode != WICED_BT_MESH_OPCODE_TIME_ROLE_GET
                        && p_event->opcode != WICED_BT_MESH_OPCODE_TIME_ROLE_SET
                        && p_event->opcode != WICED_BT_MESH_OPCODE_TIME_ZONE_SET
                        && p_event->opcode != WICED_BT_MESH_OPCODE_TAI_UTC_DELTA_SET)))
            {
                TRACE2(TRACE_DEBUG, "wiced_bt_mesh_core_send: ignore dev key with Model:%x opcode:%x\n", p_event->model_id, p_event->opcode);
                return WICED_BT_ERROR;
            }
        }
#endif

        // pass access payload to the access layer for sending
        if (access_layer_send(payload, (uint16_t)(p - &payload[0]), p_event->app_key_idx, p_event->element_idx, p_event->dst, p_event->ttl, p_event->credential_flag, complete_callback, p_event))
        {
            res = WICED_BT_SUCCESS;
        }
    }
    // On error or if caller doesn't expect the reply release mesh event
    if(res != WICED_BT_SUCCESS || complete_callback == NULL)
        wiced_bt_mesh_release_event(p_event);

    return res;
}

/**  
 * \brief Stop sending retransmittions.
 * \details Stopss sending retransmittions started on wiced_bt_mesh_core_send(). It can be used to stop retransmitions
 * on received answer on reliable message.
 *
 * @param[in]   p_event       Mesh event used to send message by wiced_bt_mesh_core_send() to stop retransmitions.
 *
 * @return      None
 */
void wiced_bt_mesh_core_cancel_send(wiced_bt_mesh_event_t *p_event)
{
    TRACE1(TRACE_INFO, "wiced_bt_mesh_core_cancel_send: p_event:%x\n", (uint32_t)p_event);
    if (p_event == NULL)
        return;
    // If it was related to segmented message then exit
    if (transport_layer_cancel_send(p_event))
        return;
#ifndef MESH_CONTROLLER
    // Otherwise cancel all related advertisements
    mesh_stop_adv_for_mesh_event(p_event);
#endif
}
