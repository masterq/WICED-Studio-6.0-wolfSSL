/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Friend feature implementation
 *
*/
#include <stdio.h> 
#include <string.h> 

#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_aes_ccm.h"

#include "friendship.h"
#include "friend.h"
#include "transport_layer.h"
#include "foundation.h"
#include "network_layer.h"
#include "wiced_timer.h"
#include "key_refresh.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__FRIEND_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__FRIEND_C
#include "mesh_trace.h"

// All states of the friend LPN
#define FRND_STATE_RECEIVED_REQUEST             0
#define FRND_STATE_SENT_OFFER                   1
#define FRND_STATE_RECEIVED_SUBS_LIST_ADD       2
#define FRND_STATE_RECEIVED_SUBS_LIST_REMOVE    3
#define FRND_STATE_RECEIVED_POLL                4
#define FRND_STATE_SENT_RESPONSE                5
#define FRND_STATE_SENT_CLEAR_CONF              6

#pragma pack(1)
#ifndef PACKED
#define PACKED
#endif

// structure to save the state of the Friend node in the NVM
typedef PACKED struct
{
    uint8_t         state;              // Friend state, Can be any of the FRND_STATE_XXX
    uint16_t        friend_counter;     // After each Friend Offer message is sent, the FriendCounter value shall be incremented by 1.
    uint8_t         peer_lpn_count;     // Number of the LPN nodes served by this friend
} tFRND_NVM_STATE;

// structure to save the state of the Low Power nodes which are served by this friend node
typedef PACKED struct
{
    uint64_t                delay_timer;                        // timer end for that Low Power Mode
    uint64_t                clear_repeat_timer;                 // Friend Clear Repeat timer end
    uint64_t                clear_procedure_timer;              // Friend Clear Procedure timer end
    uint8_t                 sent_clear_cnt;                     // counter of the sent Friend Clear messages. 0 means no active Friend Clear logic and related timers
    uint8_t                 state;                              // state of the LPN
    uint16_t                addr;                               // Unicast address of LPN.
    uint8_t                 receive_delay;                      // Receive delay requested by the Low Power node
    uint8_t                 friend_offer_delay;                 // calculated on friend offer
    uint32_t                poll_timeout;                       // Poll timeout requested by the Low Power node
    uint16_t                lpn_counter;                        // LPNCounter field received from the LPN in the Friend Request
    uint8_t                 num_elements;                       // Number of Elements in the Low Power node
    uint8_t                 rssi;                               // RSSI measured by the Friend node

    uint16_t                previous_address;                   // Previous Friend�s unicast address

    uint8_t                 net_key_idx;                        // local network key index
    MeshSecMaterial         sec_material;                       // security material which is comprised of the NID, the Encryption Key, and the Privacy Key.

    uint8_t                 fsn;                                // FSN (Friend Sequence Number) expecting in the next Poll
    uint8_t                 received_fsn;                       // FSN received in the latest Poll
    uint8_t                 transaction_number;                 // Transaction number received in Subscription List Add/Remove message
    uint8_t                 subs_list_num;                      // number of addresses in the subscriptions list
    uint16_t                subs_list[FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST];    // subscriptions list
} tFRND_PEER_LPN_STATE;

#define FRND_CACHE_MSG_FLAGS_TYPE_MASK          0x03    // bits used for cache message type
#define FRND_CACHE_MSG_FLAGS_TYPE_UPDATE        0x00    // it is Friend Update
#define FRND_CACHE_MSG_FLAGS_TYPE_MSG           0x01    // It is cached non-segmented message
#define FRND_CACHE_MSG_FLAGS_TYPE_SEG           0x02    // It is segment
#define FRND_CACHE_MSG_FLAGS_TYPE_SEG_ACK       0x03    // It is segment ack
#define FRND_CACHE_MSG_FLAGS_INCOMPLETE         0x04    // It is segment of incomplete messag - not all segments are received for that message
#define FRND_CACHE_MSG_FLAGS_SENT               0x08    // That message has been sent on previous Poll

typedef PACKED struct
{
    uint16_t                addr;                                       // Unicast address of LPN node.
    uint8_t                 flags;                                      // Can be any of the FRND_CACHE_MSG_FLAGS_XXX
    uint8_t                 ttl;                                        // TTL of the received message
    uint16_t                src;                                        // message source
    uint16_t                dst;                                        // message destination
    uint32_t                seq;                                        // Sequense number from the message
    uint32_t                iv_index;                                   // message's IV index
    uint8_t                 len;                                        // Length of the Lower Transport PDU (pdu)
    uint8_t                 pdu[1];                                     // Lower Transport PDU with max len MESH_LOWER_TRANSPORT_PDU_MAX_LEN
} tFRND_CACHE_MSG;

// structure to save the state of the Friend node in the NVM
typedef PACKED struct
{
    // Messages cache
    uint8_t                 *cache_buf;
    uint16_t                msg_cache_len;              // total length of the all messages in the cache cache_buf
    //uint8_t                 msg_cache_cnt;              // total number of the non-update messages in the cache

    wiced_timer_t           timer;                      // Common timer. Its type is milliseconds
    uint64_t                timer_end;                  // Time of the current timer's timeout end. 0 means timer is stopped
    wiced_bool_t            timer_initialized;          // WICED_TRUE - timer is initialized
    // array of all LPN nodes served by this friend
    tFRND_PEER_LPN_STATE    *peer_lpn_state[MESH_LPN_MAX_NUM];

    tFRND_NVM_STATE         nvm;                        // persistent state members
} tFRND_STATE;


#pragma pack()

static tFRND_STATE                          frnd_state = { 0 };
static wiced_bt_mesh_core_config_friend_t   frnd_config = { 0 };    // Configuration received at startup from wiced_bt_mesh_core_init()



#define FRND_NUM_ELEMENTS_MIN       0x01

static void friend_handle_request_(int friend_idx, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl, uint8_t rssi, uint8_t net_key_idx);
static void friend_handle_poll_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl);
static void friend_handle_clear_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t net_key_idx);
static void friend_handle_clear_conf_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len);
static void friend_handle_subslist_add_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl);
static void friend_handle_subslist_remove_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl);
static void friend_delay_timer_callback_(uint32_t lpn_idx);
static wiced_bool_t friend_find_subs(uint32_t lpn_idx, uint16_t addr, uint8_t *p_idx);
static void friend_del_lpn_(uint32_t lpn_idx);
static void friend_send_offer_(uint32_t lpn_idx);
static void friend_send_subs_conf_(uint32_t lpn_idx);
static tFRND_CACHE_MSG *friend_add_update_(uint8_t lpn_idx);
static void friend_send_clear_(uint32_t lpn_idx);
static void friend_send_cache_(uint32_t lpn_idx);
static void friend_clear_repeat_timer_callback_(uint32_t lpn_idx);
static void friend_clear_procedure_timer_callback_(uint32_t lpn_idx);

typedef void(*wiced_bt_core_timer_callback_t)(uint32_t cBackparam);

// calls callback related to the first expired timer and returns timeout value for next closest timer
static uint64_t friend_timer_scan_(wiced_bt_core_timer_callback_t *p_cb, uint32_t *p_lpn_idx)
{
    uint32_t                        lpn_idx;
    uint32_t                        min_lpn_idx;
    tFRND_PEER_LPN_STATE            *p_lpn;
    wiced_bt_core_timer_callback_t  cb;
    wiced_bt_core_timer_callback_t  min_cb = NULL;
    uint64_t                        end_cb = 0;
    uint64_t                        end_cb2 = 0;
    uint64_t                        end_cb_tmp;
    uint64_t                        current_time;
    uint64_t                        ret = 0;
    
    // find nearest timer
    for (lpn_idx = 0; lpn_idx < frnd_state.nvm.peer_lpn_count; lpn_idx++)
    {
        p_lpn = frnd_state.peer_lpn_state[lpn_idx];
        if (p_lpn->delay_timer != 0 && (end_cb2 == 0 || end_cb2 > p_lpn->delay_timer))
        {
            end_cb2 = p_lpn->delay_timer;
            cb = friend_delay_timer_callback_;
        }
        if (p_lpn->clear_repeat_timer != 0)
        {
            if (end_cb2 == 0 || end_cb2 > p_lpn->clear_repeat_timer)
            {
                end_cb2 = p_lpn->clear_repeat_timer;
                cb = friend_clear_repeat_timer_callback_;
            }
            if (end_cb2 == 0 || end_cb2 > p_lpn->clear_procedure_timer)
            {
                end_cb2 = p_lpn->clear_procedure_timer;
                cb = friend_clear_procedure_timer_callback_;
            }
        }
        // do bubble to have two smallest sorted timers
        if (end_cb2)
        {
            if (end_cb == 0 || end_cb > end_cb2)
            {
                end_cb_tmp = end_cb;
                end_cb = end_cb2;
                end_cb2 = end_cb_tmp;
                min_cb = cb;
                min_lpn_idx = lpn_idx;
            }
        }
    }

    // if at least one timeout is found
    if (end_cb)
    {
        current_time = wiced_bt_mesh_core_get_tick_count();
        // if first timeout is expired then call callback
        if (current_time >= end_cb)
        {
            *p_cb = min_cb;
            *p_lpn_idx = min_lpn_idx;
            // if second timeout is found then return non-0 value to restart timer. Otherwise return 0 - to not start timer
            if (end_cb2)
                ret = current_time >= end_cb2 ? 1 : (end_cb2 - current_time);
        }
        else
        {
            // first timeout isn't expired yet. Don't call callback and return time till that timer
            *p_cb = NULL;
            ret = end_cb - current_time;
        }
    }
    
    return ret ;
}

static void friend_start_wiced_timer_(uint64_t timeout)
{
    // restrict timeout to be more then 10 ms and max 32 bit positive val
    if (timeout < 10)
        timeout = 10;
    else if (timeout > 0x7fffffff)
        timeout = 0x7fffffff;
    TRACE1(TRACE_INFO, "friend_start_wiced_timer_: timeout:%x\n", (uint32_t)timeout);
    frnd_state.timer_end = timeout + wiced_bt_mesh_core_get_tick_count();
    wiced_start_timer(&frnd_state.timer, (uint32_t)timeout);
}

static void friend_timer_callback_(uint32_t arg)
{
    uint64_t                        timeout;
    wiced_bt_core_timer_callback_t  cb;
    uint32_t                        lpn_idx;

    // on callback timer becomes inactive
    frnd_state.timer_end = 0;
    // scan timers
    timeout = friend_timer_scan_(&cb, &lpn_idx);

    // if there is at least one started timer
    if (timeout)
    {
        friend_start_wiced_timer_(timeout);
    }
    TRACE1(TRACE_INFO, "friend_timer_callback_: timeout:%x\n", (uint32_t)timeout);
    // Now it is time to call callback if expired timer is detected
    if (cb)
    {
        TRACE1(TRACE_INFO, "friend_timer_callback_: calling cb %x\n", (uint32_t)cb);
        cb(lpn_idx);
    }
}

static void friend_start_timer_(uint64_t *p_end, uint64_t timeout)
{
    TRACE3(TRACE_INFO, "friend_start_timer_: timer_initialized:%d timeout:%x%04x\n", frnd_state.timer_initialized, (uint8_t)(timeout>>16), (uint16_t)timeout);
    if (!frnd_state.timer_initialized)
    {
        frnd_state.timer_initialized = WICED_TRUE;
        wiced_init_timer(&frnd_state.timer, friend_timer_callback_, 0, WICED_MILLI_SECONDS_TIMER);
    }
    *p_end = timeout + wiced_bt_mesh_core_get_tick_count();
    // If timer is inactive then just start it
    if (frnd_state.timer_end == 0)
    {
        TRACE0(TRACE_INFO, "friend_start_timer_: start timer\n");
        friend_start_wiced_timer_(timeout);
    }
    // if new timeout comes before current timeout then restart timer
    else if (*p_end < frnd_state.timer_end)
    {
        TRACE0(TRACE_INFO, "friend_start_timer_: restart timer\n");
        wiced_stop_timer(&frnd_state.timer);
        friend_start_wiced_timer_(timeout);
    }
}

static void friend_stop_timer_(uint64_t *p_end)
{
    *p_end = 0;
}

/**
* Local function to write current configuration in the NVM.
*
* Parameters:   None
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
static wiced_bool_t friend_write_config_state_int_(void)
{
    TRACE3(TRACE_INFO, "friend_write_config_state_int_: state:%d peer_lpn_count:%d node_id:%x\n", frnd_state.nvm.state, frnd_state.nvm.peer_lpn_count, node_nv_data.node_id);

    // write state
    if (sizeof(frnd_state.nvm) != mesh_write_node_info(MESH_NVM_IDX_FRND_STATE, (uint8_t*)&frnd_state.nvm, sizeof(frnd_state.nvm), NULL))
    {
        TRACE0(TRACE_WARNING, "friend_write_config_state_int_: mesh_write_node_info failed for frnd_state\n");
        return WICED_FALSE;
    }
    return WICED_TRUE;
}

/**
* Local function to write current configuration in the NVM.
*
* Parameters:
*   lpn_idx:    Index of the LPN record to save
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
static wiced_bool_t friend_write_config_lpn_int_(uint32_t lpn_idx)
{
    wiced_bool_t            ret = WICED_TRUE;
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE3(TRACE_INFO, "friend_write_config_lpn_int_: lpn_idx:%d state:%d addr:%x\n", lpn_idx, p_lpn->state, p_lpn->addr);

    if (sizeof(tFRND_PEER_LPN_STATE) != mesh_write_node_info(MESH_NVM_IDX_LPN_BEGIN + lpn_idx, (uint8_t*)frnd_state.peer_lpn_state[lpn_idx], sizeof(tFRND_PEER_LPN_STATE), NULL))
    {
        TRACE1(TRACE_WARNING, "friend_write_config_int_: mesh_write_node_info failed for lpn %d\n", lpn_idx);
        return WICED_FALSE;
    }
    return WICED_TRUE;
}

/**
* Local function to write current configuration in the NVM.
*
* Parameters:   None
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
wiced_bool_t friend_write_config_all_lpn_int_(void)
{
    wiced_bool_t            ret = WICED_TRUE;
    uint32_t                i;

    TRACE3(TRACE_INFO, "friend_write_config_all_lpn_int_: state:%d peer_lpn_count:%d node_id:%x\n", frnd_state.nvm.state, frnd_state.nvm.peer_lpn_count, node_nv_data.node_id);

    if (frnd_state.nvm.state == FRIEND_STATE_ENABLED)
    {
        // Write data for all our Low Power friends
        for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
        {
            if(!friend_write_config_lpn_int_(i))
            {
                TRACE1(TRACE_WARNING, "friend_write_config_all_lpn_int_: frnd_write_config_lpn_int_ failed for lpn %d\n", i);
                ret = WICED_FALSE;
                break;
            }
        }
    }

    return ret;
}

/**
* Writes current configuration in the NVM. It must be called on on any data change.
*
* Parameters:   None
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
wiced_bool_t friend_write_config(void)
{
    wiced_bool_t    ret = WICED_TRUE;
    // exit with error if friend feature is disabled or we are not provisioned
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return WICED_FALSE;
    if(!friend_write_config_state_int_())
        ret = WICED_FALSE;
    if(!friend_write_config_all_lpn_int_())
        ret = WICED_FALSE;
    return ret;
}

// clears friend related data
static void friend_reset_(wiced_bool_t release)
{
    // clear state
    memset(&frnd_state, 0, sizeof(frnd_state));
    frnd_state.nvm.state = FRIEND_STATE_UNSUPPORTED;

    // release and clear LPN friends related data
    if (release)
    {
        while (frnd_state.nvm.peer_lpn_count)
        {
            wiced_memory_free(frnd_state.peer_lpn_state[--frnd_state.nvm.peer_lpn_count]);
        }
    }
    frnd_state.nvm.peer_lpn_count = 0;

    // clear cache
    frnd_state.msg_cache_len = 0;
    //frnd_state.msg_cache_cnt = 0;
}

static void friend_print_cache_(void)
{
    TRACE2(TRACE_DEBUG, "friend_print_cache_: cache_buf_len:%x msg_cache_len:%d\n", frnd_config.cache_buf_len, frnd_state.msg_cache_len);
    //TRACE3(TRACE_DEBUG, "friend_print_cache_: cache_buf_len:%d msg_cache_len:%d msg_cache_cnt:%d\n", frnd_config.cache_buf_len, frnd_state.msg_cache_len, frnd_state.msg_cache_cnt);
    if(frnd_state.msg_cache_len)
        TRACEN(TRACE_DEBUG, (char*)frnd_state.cache_buf, frnd_state.msg_cache_len < 64 ? frnd_state.msg_cache_len : 64);
}
/**
* Initializes Friend feature and related data. It must be called on device startup.
* Stack calls that function with NULL parameter on device provision and on device reset
* to adjusts Friend feature in accordance with changed provisioned state of the device.
*
* Parameters:
*   p_config:               Friend Configuration data got from the wiced_bt_mesh_core_init() or NULL if called on provision starte change.
*
* Return:   WICED_TRUE/WICED_FALSE - succeded/failed
*
*/
wiced_bool_t friend_init(wiced_bt_mesh_core_config_friend_t *p_config)
{
    wiced_bool_t            ret = WICED_TRUE;
    uint32_t                i;
    uint8_t                 buf[sizeof(tFRND_PEER_LPN_STATE)];

    // clear friend related data
    friend_reset_(WICED_FALSE);

    if (p_config)
    {
        TRACE2(TRACE_INFO, "friend_init: cache_buf_len:%x node_id:%x\n", p_config->cache_buf_len, node_nv_data.node_id);
        TRACE2(TRACE_INFO, " receive_window:%d peer_lpn_count:%d\n", p_config->receive_window, frnd_state.nvm.peer_lpn_count);

        // Exit on invalid params
        if (p_config->cache_buf_len < FRIEND_CACHE_BUF_SIZE_MIN
            || p_config->receive_window < FRIEND_RECEIVE_WINDOW_MIN)
        {
            TRACE0(TRACE_INFO, "friend_init: invalid arg\n");
            return WICED_FALSE;
        }

        // remember parameters
        frnd_config = *p_config;

        // allocate c ache buffer
        frnd_state.cache_buf = (uint8_t*)wiced_memory_permanent_allocate(frnd_config.cache_buf_len);

    }
    else
    {
        TRACE0(TRACE_INFO, "friend_init: p_config == NULL\n");
    }

    // read state
    if (sizeof(frnd_state.nvm) != mesh_read_node_info(MESH_NVM_IDX_FRND_STATE, (uint8_t*)&frnd_state.nvm, sizeof(frnd_state.nvm), NULL))
    {
        TRACE0(TRACE_WARNING, "friend_init: mesh_read_node_info failed for frnd_state\n");
        // It is first start. If that function is called then Friend feature is enabled.
        frnd_state.nvm.state = FRIEND_STATE_ENABLED;
        if(!friend_write_config_state_int_())
            return WICED_FALSE;
        if (!friend_write_config_all_lpn_int_())
            return WICED_FALSE;
    }

    // if node isn't provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
    {
        // Set Friend feature non-established (frnd_state.nvm.peer_lpn_count == 0)
        frnd_state.nvm.peer_lpn_count = 0;
        // and save NVM data.
        friend_write_config_state_int_();
        friend_write_config_all_lpn_int_();
        // Return success
        ret = WICED_TRUE;
    }
    else
    {
        // Node is provisioned
        // Read data for all our Low Power friends
        for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
        {
            if (sizeof(tFRND_PEER_LPN_STATE) != mesh_read_node_info(MESH_NVM_IDX_LPN_BEGIN + i, buf, sizeof(tFRND_PEER_LPN_STATE), NULL))
            {
                TRACE1(TRACE_WARNING, "friend_init: mesh_read_node_info failed idx:%d\n", i);
                ret = WICED_FALSE;
                break;
            }
            frnd_state.peer_lpn_state[i] = (tFRND_PEER_LPN_STATE*)wiced_memory_allocate(sizeof(tFRND_PEER_LPN_STATE));
            if (frnd_state.peer_lpn_state[i] == NULL)
            {
                TRACE1(TRACE_WARNING, "friend_init: wiced_memory_allocate failed idx:%d\n", i);
                ret = WICED_FALSE;
                break;
            }
            memcpy(frnd_state.peer_lpn_state[i], buf, sizeof(tFRND_PEER_LPN_STATE));
        }
    }
    // on error reset everything
    if (!ret)
    {
        friend_reset_(WICED_TRUE);
    }
    friend_print_cache_();//????
    return ret;
}

/**
* Does net decryption of the mesh packet trying Friend security material depending on device type and state
*
* Parameters:
*   adv_data:       Received packet.
*                   Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits))
*                   || SEQ(3bytes) || SRC(2bytes) || DST(2bytes)
*                   || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
*   adv_len:        Length of the received packet.
*   msg:            Buffer for decrypted packet. Should be suficiently big (ADV_LEN_MAX).
*   p_iv_index:     IV index
*   p_net_key_idx:  Buffer for the index of the net key
*
* Return:           -1 - error; >=0 - friend index
*
*/
int friend_net_decrypt(const BYTE* adv_data, BYTE adv_len, BYTE* msg, uint8_t* p_iv_index, uint8_t* p_net_key_idx)
{
    TRACE2(TRACE_DEBUG, "friend_net_decrypt: state:%d peer_lpn_count:%d\n", frnd_state.nvm.state, frnd_state.nvm.peer_lpn_count);
    // if we are Friend and enabled then try to decrypt 
    if (frnd_state.nvm.state == FRIEND_STATE_ENABLED)
    {
        uint16_t addr , src, dst;
        uint32_t i;
        for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
        {
            addr = frnd_state.peer_lpn_state[i]->addr;
            TRACE2(TRACE_INFO, "friend_net_decrypt: LPN:%x idx:%d\n", addr, i);
            if (mesh_sec_material_decrypt(WICED_FALSE, &frnd_state.peer_lpn_state[i]->sec_material, adv_data, adv_len, msg, p_iv_index))
            {
                src = BE2TOUINT16(msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_PKT_SEQ_LEN);
                dst = BE2TOUINT16(msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);
//                TRACE2(TRACE_INFO, "friend_net_decrypt: src:%x dst:%x\n", src, dst);
                // LPN friend's address should be src or dst or dst should be in the LPN friend's subscriptions list
                if (addr == src || addr == dst  || friend_find_subs(i, dst, NULL))
                {
                    *p_net_key_idx = frnd_state.peer_lpn_state[i]->net_key_idx;
//                    TRACE2(TRACE_INFO, "friend_net_decrypt: succeeded idx:%d net_key_idx:%x\n", i, *p_net_key_idx);
                    return i;
                }
            }
        }
    }
//    TRACE1(TRACE_INFO, "friend_net_decrypt: failed. peer_lpn_count:%d\n", frnd_state.nvm.peer_lpn_count);
    return -1;
}

#if 0
/**
* Returns friendship secure material if state and friend node are OK
*
* Parameters:
*   friend_addr:    Friend address.
*
* Return:           Pointer to secure material
*
*/
MeshSecMaterial *friend_get_sec_material(uint16_t friend_addr)
{
    MeshSecMaterial         *ret = NULL;
    uint32_t                i;
    for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
    {
        if (frnd_state.peer_lpn_state[i]->addr == friend_addr)
        {
            ret = &frnd_state.peer_lpn_state[i]->sec_material;
            break;
        }
    }
    return ret;
}
#endif

static wiced_bool_t friend_find_subs(uint32_t lpn_idx, uint16_t addr, uint8_t *p_idx)
{
    wiced_bool_t ret = WICED_FALSE;
    uint8_t idx, subs_list_num;
    uint16_t *subs_list;
    subs_list = frnd_state.peer_lpn_state[lpn_idx]->subs_list;
    subs_list_num = frnd_state.peer_lpn_state[lpn_idx]->subs_list_num;
    for (idx = 0; idx < subs_list_num; idx++)
    {
        if (subs_list[idx] == addr)
            break;
    }
    if (idx < subs_list_num)
    {
        if (p_idx)
            *p_idx = idx;
        ret = WICED_TRUE;
    }
    return ret;
}

/**
* Discard messages depending on message and requested parameters
*
* Parameters:
*   addr:           Address of the LPN
*   and:            Only handle the cache message if (type & and) == equals
*   equals:         Only handle the cache message if (type & and) == equals
*   src:            Source address of the message
*   dst:            Destination address of the message
*   seq:            SEQ value of the  of the message
*   iv_index:       IV index used for message security
*   seq_zero:       SeqZero value from Low Transport PDU
*
* Return:   pointer to the found message. NULL on error.
*/
static void friend_cache_del_msg_(uint16_t addr, uint8_t and, uint8_t equals, uint16_t src, uint16_t dst, uint32_t seq, uint32_t iv_index, uint16_t seq_zero)
{
    tFRND_CACHE_MSG *p_msg;
    tFRND_CACHE_MSG *p_msg_next = (tFRND_CACHE_MSG*)frnd_state.cache_buf;
    tFRND_CACHE_MSG *p_msg_end = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);

    TRACE3(TRACE_WARNING, "friend_cache_del_msg_: and:%x equals:%x addr:%x\n", and, equals, addr);
//wiced_printf(NULL, 0, " src:%x dst:%x seq:%x seq_zero:%x iv_index:%x\n", src, dst, seq, seq_zero, iv_index);
    friend_print_cache_();//????

//wiced_printf(NULL, 0, " p_msg_next:%x p_msg_end:%x\n", p_msg_next, p_msg_end);
    while (p_msg_next < p_msg_end)
    {
        p_msg = p_msg_next;
        p_msg_next = (tFRND_CACHE_MSG*)&p_msg->pdu[p_msg->len];
        // skip message if it is not for our LPN or it doesn't have requested flag (pass and/equals test)
//wiced_printf(NULL, 0, " p_msg_next:%x p_msg->addr:%x p_msg->flags:%x\n", p_msg_next, p_msg->addr, p_msg->flags);
        if (addr != p_msg->addr || (p_msg->flags & and) != equals)
            continue;
        // if it is request to delete segment ack
        if (equals == FRND_CACHE_MSG_FLAGS_TYPE_SEG_ACK)
        {
            // Spec says: Discard Segment Acknowledgement message that has the same source and destination addresses,
            // and the same SeqAuth value, but a lower iv_index or sequence number
//wiced_printf(NULL, 0, " p_msg=> src:%x dst:%x seq:%x iv_index:%x\n", p_msg->src, p_msg->dst, p_msg->seq, p_msg->iv_index);
            if (src != p_msg->src || dst != p_msg->dst
                || transport_layer_calc_seq_auth(p_msg->seq, transport_layer_get_seq_zero_from_pdu(p_msg->pdu)) != transport_layer_calc_seq_auth(seq, seq_zero)
                || (p_msg->seq >= seq && p_msg->iv_index >= iv_index))
                    continue;
        }
        // delete it from the cache
//        if ((p_msg->flags & FRND_CACHE_MSG_FLAGS_TYPE_MASK) != FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
//            frnd_state.msg_cache_cnt--;
        if (p_msg_next < p_msg_end)
            memcpy(p_msg, p_msg_next, ((uint8_t*)p_msg_end - (uint8_t*)p_msg_next));
        frnd_state.msg_cache_len -= (uint8_t*)p_msg_next - (uint8_t*)p_msg;
        p_msg_end = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);
        p_msg_next = p_msg;
//wiced_printf(NULL, 0, " p_msg_next:%x p_msg_end:%x\n", p_msg_next, p_msg_end);
    }
    friend_print_cache_();//????
}

/**
* Finds mesage in the cache for specific LPN and delete first one if requested
*
* Parameters:
*   addr:           LPN address
*   sent:           get message already sent previously
*   p_more_data:    variable to receive MD flag
*
* Return:   pointer to the found message. NULL on error.
*/
static tFRND_CACHE_MSG* friend_cache_get_msg_(uint16_t addr, wiced_bool_t sent, wiced_bool_t *p_more_data)
{
    tFRND_CACHE_MSG *ret = NULL;
    tFRND_CACHE_MSG *p_msg, *p_msg_end;
    tFRND_CACHE_MSG *p_msg_next = (tFRND_CACHE_MSG*)frnd_state.cache_buf;

    TRACE2(TRACE_INFO, "friend_cache_get_msg_: addr:%x sent:%d\n", addr, sent);
    //TRACE3(TRACE_INFO, "friend_cache_get_msg_: msg_cache_cnt:%d sent:%d addr:%x\n", frnd_state.msg_cache_cnt, sent, addr);
    friend_print_cache_();//????

    // if it is request for new message then delete all sent messages for addr
    if(!sent)
        friend_cache_del_msg_(addr, FRND_CACHE_MSG_FLAGS_SENT, FRND_CACHE_MSG_FLAGS_SENT, 0, 0, 0, 0, 0);

    p_msg_end = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);

    *p_more_data = WICED_FALSE;
    while (p_msg_next < p_msg_end)
    {
        p_msg = p_msg_next;
        p_msg_next = (tFRND_CACHE_MSG*)((uint8_t*)p_msg + OFFSETOF(tFRND_CACHE_MSG, pdu) + p_msg->len);
        // skip message if it is not for our LPN or it is incomplete
        if (addr != p_msg->addr || (p_msg->flags & FRND_CACHE_MSG_FLAGS_INCOMPLETE) != 0)
            continue;
        // If it is not first message set more_data flag an exit loop.
        if (ret)
        {
            *p_more_data = WICED_TRUE;
            break;
        }
        // It is first message - remember it and proceed in the loop to check if the queue has more data for that LPN.
        ret = p_msg;
        // Mark it as sent message
        p_msg->flags |= FRND_CACHE_MSG_FLAGS_SENT;
    }

    TRACE2(TRACE_INFO, "friend_cache_get_msg_: returns!=NULL:%d more_data:%d\n", ret != NULL, *p_more_data);
    //TRACE3(TRACE_INFO, "friend_cache_get_msg_: returns!=NULL:%d more_data:%d msg_cache_cnt:%d\n", ret != NULL, *p_more_data, frnd_state.msg_cache_cnt);
    friend_print_cache_();//????
    return ret;
}

/**
* Discard oldest message to release memory for new message
*
* Parameters:       None
*
* Return:   WICED_TRUE - message discarded successfully. WICED_FALSE - No message to discard
*/
static wiced_bool_t friend_cache_del_old_msg_(void)
{
    wiced_bool_t    ret;
    tFRND_CACHE_MSG *p_msg;
    tFRND_CACHE_MSG *p_msg_next = (tFRND_CACHE_MSG*)frnd_state.cache_buf;
    tFRND_CACHE_MSG *p_msg_end = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);

    while (p_msg_next < p_msg_end)
    {
        p_msg = p_msg_next;
        p_msg_next = (tFRND_CACHE_MSG*)&p_msg->pdu[p_msg->len];
        // don't discard segments of incomplete messages
        if (p_msg->flags & FRND_CACHE_MSG_FLAGS_INCOMPLETE)
            continue;
        // delete it from the cache
//        if ((p_msg->flags & FRND_CACHE_MSG_FLAGS_TYPE_MASK) != FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
//            frnd_state.msg_cache_cnt--;
        if (p_msg_next < p_msg_end)
            memcpy(p_msg, p_msg_next, ((uint8_t*)p_msg_end - (uint8_t*)p_msg_next));
        frnd_state.msg_cache_len -= (uint8_t*)p_msg_next - (uint8_t*)p_msg;
        break;
    }
    ret = p_msg_next < p_msg_end ? WICED_TRUE : WICED_FALSE;
//    TRACE2(TRACE_INFO, "friend_cache_del_old_msg_: returns:%d msg_cache_cnt:%d\n", ret, frnd_state.msg_cache_cnt);
    return ret;
}

/**
* Puts mesage, seg, ack or update into the cache
*
* Parameters:
*   friend_idx:     index of the LPN
*   msg:            message to put to the cache
*   msg_len:        length of the message to put to the cache
*   type:           Message type. Can be any of the FRND_CACHE_MSG_FLAGS_TYPE_XXX
*   ttl:            TTL of that message
*   src:            Source address of the message
*   dst:            Destination address of the message
*   seq:            SEQ value of the  of the message
*   iv_index:       IV index used for message security
*
* Return:   pointer to the added message. NULL on error.
*
*/
static tFRND_CACHE_MSG *friend_cache_put_(uint16_t addr, const uint8_t *msg, uint8_t msg_len, uint8_t ttl, uint8_t type, uint16_t src, uint16_t dst, uint32_t seq, uint32_t iv_index)
{
    tFRND_CACHE_MSG *p_msg;
    TRACE2(TRACE_INFO, "friend_cache_put_: addr:%x available:%d\n", addr, frnd_config.cache_buf_len - frnd_state.msg_cache_len);
    TRACE2(TRACE_INFO, " type:%d ttl:%x msg:\n", type, ttl);
    TRACEN(TRACE_INFO, (char*)msg, msg_len);

    // If the message that is being stored is a Friend Update message and the Friend Cache contains
    // another Friend Update message, then the older Friend Update message shall be discarded.
    if (type == FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
        friend_cache_del_msg_(addr, FRND_CACHE_MSG_FLAGS_TYPE_MASK, type, 0, 0, 0, 0, 0);
    // If the message that is being stored is a Segment Acknowledgement message and the Friend Cache
    // contains another Segment Acknowledgement message that has the same source and destination addresses,
    // and the same SeqAuth value, but a lower iv_index or sequence number, then the older
    //Segment Acknowledgement message shall be discarded.
    else if (type == FRND_CACHE_MSG_FLAGS_TYPE_SEG_ACK)
        friend_cache_del_msg_(addr, FRND_CACHE_MSG_FLAGS_TYPE_MASK, type, src, dst, seq, iv_index, transport_layer_get_seq_zero_from_pdu(msg));
    else if (type != FRND_CACHE_MSG_FLAGS_TYPE_MSG && type != (FRND_CACHE_MSG_FLAGS_TYPE_SEG | FRND_CACHE_MSG_FLAGS_INCOMPLETE))
    {
        return NULL;
    }

    // If the Friend Cache is full and a new message needs to be cached, the oldest entry that is different
    // from the Friend Update message shall be discarded to make room for the new one.
    while (OFFSETOF(tFRND_CACHE_MSG, pdu) + msg_len > frnd_config.cache_buf_len - frnd_state.msg_cache_len)
//        || frnd_state.msg_cache_cnt >= (frnd_config.cache_buf_len / (OFFSETOF(tFRND_CACHE_MSG, pdu) + MESH_LOWER_TRANSPORT_PDU_MAX_LEN)))
    {
        if(!friend_cache_del_old_msg_())
            return NULL;
    }

    p_msg = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);
    frnd_state.msg_cache_len = (uint8_t*)&p_msg->pdu[msg_len] - frnd_state.cache_buf;
    p_msg->len = msg_len;
    memcpy(p_msg->pdu, msg, msg_len);
    p_msg->addr = addr;
    p_msg->flags = type;
    p_msg->ttl = ttl;
    p_msg->src = src;
    p_msg->dst = dst;
    p_msg->seq = seq;
    p_msg->iv_index = iv_index;
//    if ((p_msg->flags & FRND_CACHE_MSG_FLAGS_TYPE_MASK) != FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
//        frnd_state.msg_cache_cnt++;

    TRACE1(TRACE_INFO, "friend_cache_put_: returns:%x\n", (uint32_t)p_msg);
    //TRACE2(TRACE_INFO, "friend_cache_put_: returns:%x msg_cache_cnt:%d\n", (uint32_t)p_msg, frnd_state.msg_cache_cnt);
    return p_msg;
}

/**
* Puts mesage into the cache for each LPN it targeted for
*
* Parameters:
*   ctl:            CTL flag from network PDU
*   src:            Source address
*   dst:            Destination address
*   tl_pdu:         Low Transport PDU to put to the cache
*   tl_pdu_len:     length of the Low Transport PDU to put to the cache
*   ttl:            TTL of that message
*   seq_src_dst:    pointer to the part of the network message with SEQ, SRC, DST
*   iv_index:       IV index used for security of that message
*
* Return:   pointer to the found message. NULL on error.
*
*/
wiced_bool_t friend_cache_put_msg(wiced_bool_t ctl, uint16_t src, uint16_t dst, const uint8_t *tl_pdu, uint8_t tl_pdu_len, uint8_t ttl, const uint8_t *seq_src_dst, const uint8_t *iv_index)
{
    uint32_t    i;
    uint8_t     type;
    if (ctl && tl_pdu[0] == 0)
        type = FRND_CACHE_MSG_FLAGS_TYPE_SEG_ACK;
    else if ((tl_pdu[0] & TL_CP_SEG_MASK) == 0)
        type = FRND_CACHE_MSG_FLAGS_TYPE_MSG;
    else
        type = FRND_CACHE_MSG_FLAGS_TYPE_SEG | FRND_CACHE_MSG_FLAGS_INCOMPLETE;
    for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
    {
        if (frnd_state.peer_lpn_state[i]->addr == src)
            continue;
        if (frnd_state.peer_lpn_state[i]->addr == dst || friend_find_subs(i, dst, NULL))
        {
            if (NULL == friend_cache_put_(frnd_state.peer_lpn_state[i]->addr, tl_pdu, tl_pdu_len, ttl, type,
                BE2TOUINT16(seq_src_dst + MESH_PKT_SEQ_LEN), dst, BE3TOUINT32(seq_src_dst), BE4TOUINT32(iv_index)))
                break;
        }
    }
    return i < frnd_state.nvm.peer_lpn_count;
}

/**
* Marks all segments in the cache for all targeted LPNs as completed
*
* Parameters:
*   dst:            Destination address
*   remove:     WICED_TRUE - remove all segments; WICED_FALSE - remove incomplete flag from all segments
*
* Return:   None
*
*/
void friend_cache_complete_msg(uint16_t dst, wiced_bool_t remove)
{
    tFRND_CACHE_MSG *p_msg;
    tFRND_CACHE_MSG *p_msg_next = (tFRND_CACHE_MSG*)frnd_state.cache_buf;
    tFRND_CACHE_MSG *p_msg_end = (tFRND_CACHE_MSG*)(frnd_state.cache_buf + frnd_state.msg_cache_len);
    uint32_t        lpn_idx;

    // go through each package in the cache
    while (p_msg_next < p_msg_end)
    {
        p_msg = p_msg_next;
        p_msg_next = (tFRND_CACHE_MSG*)&p_msg->pdu[p_msg->len];

        // ignore current packet if is not incomplete segment
        if ((p_msg->flags & FRND_CACHE_MSG_FLAGS_INCOMPLETE) == 0)
            continue;
        if (dst != p_msg->addr)
        {
            if (!friend_find_lpn(p_msg->addr, &lpn_idx))
                continue;   // It should never happen
            if (!friend_find_subs(lpn_idx, dst, NULL))
                continue;
        }
        // remove it if requested
        if (remove)
        {
//            if ((p_msg->flags & FRND_CACHE_MSG_FLAGS_TYPE_MASK) != FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
//                frnd_state.msg_cache_cnt--;
            if (p_msg_next < p_msg_end)
                memcpy(p_msg, p_msg_next, ((uint8_t*)p_msg_end - (uint8_t*)p_msg_next));
            frnd_state.msg_cache_len -= (uint8_t*)p_msg_next - (uint8_t*)p_msg;
            continue;
        }
        // remove incomplete flag
        p_msg->flags &= ~FRND_CACHE_MSG_FLAGS_INCOMPLETE;
    }
//    TRACE1(TRACE_INFO, "friend_cache_complete_msg: msg_cache_cnt:%d\n", frnd_state.msg_cache_cnt);
}

/**
* Process data packet addressed to this Low Power node.
*
* Parameters:
*   op:             OP code.
*   params:         parameters of the payload
*   params_len:     length of the parameters of the payload
*   src:            source address
*   dst:            destination address
*   ttl:            TTL of the received packet
*   rssi:           RSSI of the received packet. #BTM_INQ_RES_IGNORE_RSSI if not valid.
*   net_key_idx:    Network key index used for decryption
*   friend_idx:     Index of the friend LPN which security material is used. Negative value means it was encrypted with master security credentials
*
* Return:   WICED_TRUE - message handled; WICED_FALSE - try other handler.
*
*/
wiced_bool_t friend_handle_ctrl(uint8_t op, uint8_t* params, uint16_t params_len, uint16_t src, uint16_t dst, uint8_t ttl, uint8_t rssi, uint8_t net_key_idx, int friend_idx)
{
    wiced_bool_t ret = WICED_TRUE;

    TRACE4(TRACE_INFO, "friend_handle_ctrl: op:%d ttl:%d friend_idx:%d state:%d\n", op, ttl, friend_idx, frnd_state.nvm.state);
    TRACE2(TRACE_INFO, " src:%x dst:%x\n", src, dst);
    friend_print_cache_();//????

    switch (op)
    {
    case MESH_TL_CM_OP_FRIEND_REQUEST:
        friend_handle_request_(friend_idx, src, params, params_len, ttl, rssi, net_key_idx);
        break;

    case MESH_TL_CM_OP_FRIEND_POLL:
        friend_handle_poll_(friend_idx, dst, src, params, params_len, ttl);
        break;

    case MESH_TL_CM_OP_FRIEND_SUBSLIST_ADD:
        friend_handle_subslist_add_(friend_idx, dst, src, params, params_len, ttl);
        break;

    case MESH_TL_CM_OP_FRIEND_SUBSLIST_REMOVE:
        friend_handle_subslist_remove_(friend_idx, dst, src, params, params_len, ttl);
        break;

    case MESH_TL_CM_OP_FRIEND_CLEAR:
        friend_handle_clear_(friend_idx, dst, src, params, params_len, net_key_idx);
        break;

    case MESH_TL_CM_OP_FRIEND_CLEAR_CONF:
        friend_handle_clear_conf_(friend_idx, dst, src, params, params_len);
        break;

    default:
        ret = WICED_FALSE;
        break;
    }
    friend_print_cache_();//????
    return ret;
}

/**
* Creates Friend Update message. It is sent by a Friend node to a Low Power node to inform the Low Power node that the security parameters
* for the network have changed or are changing, or that the Friend Cache is empty.
*
* Parameters:
*   iv_update:  IV Update Flag
*   kr:         Key Refresh Flag
*   ivi:        Current IV Index
*   md:         MD flag (More Data)
*   buf:        Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t friend_crt_update(wiced_bool_t iv_update, wiced_bool_t kr, uint32_t ivi, wiced_bool_t md, uint8_t *buf)
{
    uint16_t len = 0;
    buf[len] = 0x00;
    if (iv_update)
        buf[len] |= FRIENDSHIP_FRIEND_UPDATE_IV_UPDATE_FLAG;
    if (kr)
        buf[len] |= FRIENDSHIP_FRIEND_UPDATE_KEY_REFRESH_FLAG;
    len++;
    UINT32TOBE4(buf + len, ivi);
    len += 4;
    buf[len++] = md ? 0x01 : 0x00;
    TRACE0(TRACE_INFO, "friend_crt_update:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Offer message. It is sent by a Friend node to confirm a friendship.
*
* Parameters:
*   receive_window:     Receive Window value in units of 1 millisecond supported by the Friend node. Can be 0x01�0xFF
*   cache_size:         Cache Size available on the Friend node
*   subs_list_size:     Size of the Subscription List that can be supported by a Friend node for a Low Power node
*   rssi:               RSSI measured by the Friend node. It shall be 0x7F if the signal strength indication is not available.
*   friend_counter:     Number of Friend Offer messages that the Friend node has sent
*   buf:                Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t friend_crt_offer(uint8_t receive_window, uint8_t cache_size, uint8_t subs_list_size, uint8_t rssi, uint16_t friend_counter, uint8_t *buf)
{
    uint16_t len = 0;
    if (receive_window < FRIEND_RECEIVE_WINDOW_MIN) //RFU
        return 0;
    buf[len++] = receive_window;
    buf[len++] = cache_size;
    buf[len++] = subs_list_size;
    buf[len++] = rssi;
    UINT16TOBE2(buf + len, friend_counter);
    len += 2;

    TRACE0(TRACE_INFO, "friend_crt_offer:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Subscription List Confirm message. It is used to respond to a Friend Subscription List Add message
* or Friend Subscription List Remove message.
*
* Parameters:
*   trans_num:      The number for identifying a transaction
*   buf:            Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t friend_crt_subs_list_confirm(uint8_t trans_num, uint8_t *buf)
{
    uint16_t len = 0;
    buf[len++] = trans_num;

    TRACE0(TRACE_INFO, "friend_crt_subs_list_confirm:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

static void friend_handle_request_(int friend_idx, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl, uint8_t rssi, uint8_t net_key_idx)
{
    int                     friend_offer_delay;
    uint32_t                val;
    tFRND_PEER_LPN_STATE    *p_lpn;
    uint32_t                lpn_idx;
    uint16_t                previous_address;

    uint8_t     receive_delay, num_elements;
    uint32_t    poll_timeout;

    // <Criteria(1 byte)><ReceiveDelay(1 byte)><PollTimeout(3 bytes)><PreviousAddress(2 bytes)><NumElements(1 byte)><LPNCounter(2 bytes)>
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx >= 0                                            // or it was not encrypted by the master security credentials
        || ttl != 0                                                 // or TTL is not 0
        || frnd_state.nvm.peer_lpn_count >= MESH_LPN_MAX_NUM  // or we already have reached the max friends number
        || params_len != 10)                                        // or if length is wrong
    {
        TRACE0(TRACE_INFO, "friend_handle_request_: invalid param\n");
        return;
    }

    // If the source address of the Friend Request message is an address of a Low Power node that is currently in a friendship
    //with the Friend node, then the Friend node shall also consider the existing friendship with that Low Power node terminated
    if (friend_find_lpn(src, &lpn_idx))
    {
        friend_del_lpn_(lpn_idx);
    }

    // Get and check MinCacheSizeLog
    val = params[0] & 0x7;
    if (val < FRIENDSHIP_MIN_CACHE_SIZE_LOG_MIN || val > FRIENDSHIP_MIN_CACHE_SIZE_LOG_MAX)
    {
        TRACE1(TRACE_INFO, "friend_handle_request_: invalid MinCacheSizeLog:%x\n", val);
        return;
    }
    // MinCacheSize
    val = 1 << val;
    // Calculate Minimum cache buffer and check it out
    val = val * (OFFSETOF(tFRND_CACHE_MSG, pdu) + MESH_LOWER_TRANSPORT_PDU_MAX_LEN);
    if (val > frnd_config.cache_buf_len)
    {
        TRACE2(TRACE_INFO, "friend_handle_request_: proceed with not suficient cache size:%x requested:%x\n", frnd_config.cache_buf_len, val);
        //return;
    }

    receive_delay = params[1];
    if (receive_delay < FRIENDSHIP_RECEIVE_DELAY_MIN)
    {
        TRACE1(TRACE_INFO, "friend_handle_request_: invalid ReceiveDelay:%x\n", receive_delay);
        return;
    }
    poll_timeout = BE3TOUINT32(params + 2);
    if (poll_timeout < FRIENDSHIP_POLL_TIMEOUT_MIN || poll_timeout > FRIENDSHIP_POLL_TIMEOUT_MAX)   // RFU
    {
        TRACE1(TRACE_INFO, "friend_handle_request_: invalid PollTimeout:%x\n", poll_timeout);
        return;
    }

    num_elements = params[7];
    if (num_elements < FRND_NUM_ELEMENTS_MIN)
    {
        TRACE1(TRACE_INFO, "friend_handle_request_: invalid NumElements:%x\n", num_elements);
        return;
    }

    // Previous address should be Unicast address
    previous_address = BE2TOUINT16(params + 5);
    if (previous_address != MESH_NODE_ID_INVALID && (previous_address & MESH_NON_UNICAST_MASK) != 0)
    {
        TRACE1(TRACE_INFO, "friend_handle_request_: invalid PreviousAddress:%x\n", previous_address);
        return;
    }

    p_lpn = (tFRND_PEER_LPN_STATE*)wiced_memory_allocate(sizeof(tFRND_PEER_LPN_STATE));
    if (p_lpn == NULL)
    {
        TRACE0(TRACE_INFO, "friend_handle_request_: wiced_memory_allocate failed\n");
        return;
    }

    lpn_idx = frnd_state.nvm.peer_lpn_count++;
    TRACE1(TRACE_INFO, "friend_handle_request_: lpn_idx:%d\n", lpn_idx);
    frnd_state.peer_lpn_state[lpn_idx] = p_lpn;
    memset(p_lpn, 0, sizeof(tFRND_PEER_LPN_STATE));
    p_lpn->addr = src;
    p_lpn->receive_delay = receive_delay;
    p_lpn->poll_timeout = poll_timeout;
    p_lpn->num_elements = num_elements;
    p_lpn->net_key_idx = net_key_idx;
    p_lpn->previous_address = previous_address;
    p_lpn->rssi = rssi;
    p_lpn->lpn_counter = BE2TOUINT16(params + 8);   // Number of Friend Request messages that the Low Power node has sent

    // Calculate friendship secure material to be ready to receive friend poll message secured with friendship security material
    calc_net_security_friend(p_lpn->addr, node_nv_data.node_id, p_lpn->lpn_counter, frnd_state.nvm.friend_counter,
        node_nv_net_key[p_lpn->net_key_idx].key, &p_lpn->sec_material.nid, p_lpn->sec_material.encr_key,
        p_lpn->sec_material.privacy_key);

    // Calculate and set Friend Offer Delay - the time between receiving the Friend Request message and sending the Friend Offer
    // ReceiveWindowFactor * ReceiveWindow - RSSIFactor * RSSI (make it <= 100 ms)
    friend_offer_delay = ((FRIENDSHIP_CRITERIA_TO_RECEIVE_WINDOW_FACTOR(params[0]) * 5 + 10) * (uint32_t)frnd_config.receive_window
        - (FRIENDSHIP_CRITERIA_TO_RSSI_FACTOR(params[0]) * 5 + 10) * (uint32_t)rssi) / 10;
    TRACE1(TRACE_INFO, "friend_handle_request_: friend_offer_delay:%d\n", friend_offer_delay);
    if (friend_offer_delay < FRIENDSHIP_OFFER_LISTEN_DELAY)
        friend_offer_delay = FRIENDSHIP_OFFER_LISTEN_DELAY;
    else if (friend_offer_delay > 0xfe)
        friend_offer_delay = 0xfe;
    p_lpn->friend_offer_delay = (uint8_t)friend_offer_delay;
    p_lpn->state = FRND_STATE_RECEIVED_REQUEST;

    friend_write_config_state_int_();
    friend_write_config_all_lpn_int_();

#ifndef _DEB_FRIEND_NO_RECEIVEDELAY
    friend_start_timer_(&p_lpn->delay_timer, friend_offer_delay);
#else
    friend_send_offer_(lpn_idx);
#endif
}

static void friend_handle_poll_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[friend_idx];

    // <RFU(7 bits><FSN(1 bit)>
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx < 0                               // or message has been encrypted by master security credentials
        || (p_lpn->state != FRND_STATE_SENT_RESPONSE
            && p_lpn->state != FRND_STATE_SENT_OFFER)   // or we don't expect Poll
        || ttl != 0                                     // or TTL is not 0
        || dst != node_nv_data.node_id                  // or DST is not our address
        || src != p_lpn->addr                           // or SRC doesn't correspond to secured material
        || params_len != 1                              // or length is wrong
        || params[0] > 1)                               // or 7 MS bits are not 0 (Prohibited)
    {
        TRACE1(TRACE_INFO, "friend_handle_poll_: invalid param. state:%d\n", friend_idx>=0 ? p_lpn->state : -1);
        return;
    }

    TRACE2(TRACE_INFO, "friend_handle_poll_: state:%d fsn:%d\n", p_lpn->state, p_lpn->fsn);

    p_lpn->received_fsn = params[0] & FRIENDSHIP_FRIEND_POLL_FSN_FLAG;

    // if we are establishing the friendship
    if (p_lpn->state == FRND_STATE_SENT_OFFER)
    {
        // The node received a Friend Poll message within 1 second after sending the Friend Offer message.
        // The Friendship is established.
        p_lpn->transaction_number =  0;

        // If we received unicast PreviousAddress in the Friend Request message that is not the Friend node�s own unicast address then send Friend Clear
        if (p_lpn->previous_address != MESH_NODE_ID_INVALID
            && p_lpn->previous_address != node_nv_data.node_id)
        {
            friend_send_clear_(friend_idx);
        }
    }

    // set new state
    p_lpn->state = FRND_STATE_RECEIVED_POLL;
    friend_write_config_lpn_int_(friend_idx);

#ifndef _DEB_FRIEND_NO_RECEIVEDELAY
    // Start ReceiveDelay timer to send the cached message
    friend_start_timer_(&p_lpn->delay_timer, p_lpn->receive_delay);
#else
    friend_send_cache_(friend_idx);
#endif
}

static void friend_handle_subslist_add_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[friend_idx];
    uint16_t                subs;
    uint32_t                i;
    wiced_bool_t            needs_write = WICED_FALSE;

    // <TransactionNumber(1 byte)>[<LPN Address(2 bytes)>]
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx < 0                               // or message has been encrypted by master security credentials
        || (p_lpn->state != FRND_STATE_SENT_RESPONSE)   // or we don't expect subslist_add
        || ttl != 0                                     // or TTL is not 0
        || dst != node_nv_data.node_id                  // or DST is not our address
        || src != p_lpn->addr                           // or SRC doesn't correspond to secured material
        || params_len < 3 || (params_len % 2) != 1)     // or if length is wrong
    {
        TRACE1(TRACE_INFO, "friend_handle_subslist_add_: invalid param. state:%d\n", friend_idx >= 0 ? p_lpn->state : -1);
        return;
    }

    for (i = 1; i < params_len; i+=2)
    {
        subs = BE2TOUINT16(params + i);
        // ignore current address if it is already in the list
        if (friend_find_subs(friend_idx, subs, NULL))
            continue;
        // exit loop if we have max number of subscriptions
        if (p_lpn->subs_list_num >= FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST)
            break;
        // add current address to the list
        p_lpn->subs_list[p_lpn->subs_list_num++] = subs;
        needs_write = WICED_TRUE;
    }
    TRACE1(TRACE_INFO, "friend_handle_subslist_add_: subs_list_num:%d\n", p_lpn->subs_list_num);
    // just exit on error
    if (i < params_len)
        return;
    // write to the NVM if at least one addr is added
    if (needs_write)
        friend_write_config_lpn_int_(friend_idx);
    // Remember transaction number for upcoming Subscription List Confirm message
    p_lpn->transaction_number = params[0];
    // Start ReceiveDelay timer to send the Subscription List Confirm
    p_lpn->state = FRND_STATE_RECEIVED_SUBS_LIST_ADD;
    friend_write_config_lpn_int_(friend_idx);

#ifndef _DEB_FRIEND_NO_RECEIVEDELAY
    friend_start_timer_(&p_lpn->delay_timer, p_lpn->receive_delay);
#else
    friend_send_subs_conf_(friend_idx);
#endif
}

static void friend_handle_subslist_remove_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t ttl)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[friend_idx];
    uint16_t                subs;
    uint32_t                i;
    uint8_t                 subs_idx;
    wiced_bool_t            needs_write = WICED_FALSE;

    // <TransactionNumber(1 byte)>[<LPN Address(2 bytes)>]
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx < 0                           // or message has been encrypted by master security credentials
        || (p_lpn->state != FRND_STATE_SENT_RESPONSE)   // or we don't expect subslist_remove
        || ttl != 0                                 // or TTL is not 0
        || dst != node_nv_data.node_id                  // or DST is not our address
        || src != p_lpn->addr                           // or SRC doesn't correspond to secured material
        || params_len < 3 || (params_len % 2) != 1) // or if length is wrong
        return;

    for (i = 1; i < params_len; i += 2)
    {
        subs = BE2TOUINT16(params + i);
        // ignore current address if it is not in the list
        if (!friend_find_subs(friend_idx, subs, &subs_idx))
            continue;
        // remove current address from the list
        if (subs_idx < --p_lpn->subs_list_num)
            memcpy(&p_lpn->subs_list[subs_idx], &p_lpn->subs_list[subs_idx + 1], (p_lpn->subs_list_num - subs_idx) * 2);
        needs_write = WICED_TRUE;
    }
    // just exit on error
    if (i < params_len)
        return;
    // write to the NVM if at least one addr is added
    if (needs_write)
        friend_write_config_lpn_int_(friend_idx);

    // Remember transaction number for upcoming Subscription List Confirm message
    p_lpn->transaction_number = params[0];
    // Start ReceiveDelay timer to send the Subscription List Confirm
    p_lpn->state = FRND_STATE_RECEIVED_SUBS_LIST_REMOVE;
    friend_write_config_lpn_int_(friend_idx);

#ifndef _DEB_FRIEND_NO_RECEIVEDELAY
    friend_start_timer_(&p_lpn->delay_timer, p_lpn->receive_delay);
#else
    friend_send_subs_conf_(friend_idx);
#endif
}

/**
* Finds the existing friendship with that Low Power node address.
*
* Parameters:   addr:    LPN unicast address
*
* Return    WICED_TRUE - success. WICED_FALSE - not found
*/
wiced_bool_t friend_find_lpn(uint16_t addr, uint32_t *p_lpn_idx)
{
    uint32_t    i;

    // Find friend LPN
    for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
    {
        if (frnd_state.peer_lpn_state[i]->addr == addr)
            break;
    }
    
    if (i >= frnd_state.nvm.peer_lpn_count)
    {
        TRACE1(TRACE_WARNING, "frnd_find_lpn_: not found addr:%x\n", addr);
        return WICED_FALSE;
    }

    if(p_lpn_idx)
        *p_lpn_idx = i;
    return WICED_TRUE;
}

//Consider the existing friendship with that Low Power node terminated
static void friend_del_lpn_(uint32_t lpn_idx)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE2(TRACE_WARNING, "********** friend_del_lpn_: lpn_idx:%d peer_lpn_count:%d\n", lpn_idx, frnd_state.nvm.peer_lpn_count);

    // delete all related messages from the cache
    friend_cache_del_msg_(p_lpn->addr, 0, 0, 0, 0, 0, 0, 0);

    // Delete that LPN from our memory
    wiced_memory_free(frnd_state.peer_lpn_state[lpn_idx]);
    if (lpn_idx < --frnd_state.nvm.peer_lpn_count)
    {
        memcpy(&frnd_state.peer_lpn_state[lpn_idx], &frnd_state.peer_lpn_state[lpn_idx + 1], (frnd_state.nvm.peer_lpn_count - lpn_idx) * sizeof(frnd_state.peer_lpn_state[0]));
    }

    // save made changes
    friend_write_config_state_int_();
    friend_write_config_all_lpn_int_();
}

static void friend_send_offer_(uint32_t lpn_idx)
{
    uint8_t                 buf[16];
    uint16_t                len;
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE1(TRACE_WARNING, "friend_send_offer_: lpn_idx:%d\n", lpn_idx);

    // create Upper transport PDU 
    len = friend_crt_offer(frnd_config.receive_window,
        frnd_config.cache_buf_len / (OFFSETOF(tFRND_CACHE_MSG, pdu) + MESH_LOWER_TRANSPORT_PDU_MAX_LEN),
        FRIENDSHIP_MAX_ADDRESSES_IN_SUBSLIST, p_lpn->rssi, frnd_state.nvm.friend_counter, buf);

    // After each Friend Offer message is sent, FriendCounter shall be incremented by 1
    frnd_state.nvm.friend_counter++;

    friend_write_config_state_int_();
    friend_write_config_lpn_int_(lpn_idx);

#ifndef MESH_CONTROLLER
    //mesh_set_adv_params(p_lpn->friend_offer_delay, 2, 1, 0, 0, 0);
    mesh_set_adv_params(0, 2, 1, 0, 0, 0);
#endif
    // send controll message with 0 TTL and main net_key with master security credentials
    transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_OFFER, buf, len, p_lpn->net_key_idx, NULL, node_nv_data.node_id, p_lpn->addr, 0);

    p_lpn->state = FRND_STATE_SENT_OFFER;
    friend_write_config_lpn_int_(lpn_idx);

    friend_start_timer_(&p_lpn->delay_timer, FRIENDSHIP_OFFER_LISTEN_INTERVAL);
}

static void friend_send_subs_conf_(uint32_t lpn_idx)
{
    uint8_t                 buf[16];
    uint16_t                len;
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE1(TRACE_WARNING, "friend_send_subs_conf_: lpn_idx:%d\n", lpn_idx);

    // create Upper transport PDU 
    len = friend_crt_subs_list_confirm(p_lpn->transaction_number, buf);

    // send controll message with 0 TTL and friend security material
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 2, 1, 0, 0, 0);
#endif
    // Use friendship security credentials
    transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_SUBSLIST_CONF, buf, len, p_lpn->net_key_idx, &p_lpn->sec_material, node_nv_data.node_id, p_lpn->addr, 0);

    p_lpn->state = FRND_STATE_SENT_RESPONSE;
    friend_write_config_lpn_int_(lpn_idx);

    friend_start_timer_(&p_lpn->delay_timer, (uint64_t)p_lpn->poll_timeout * 100);  // poll_timeout is in 100ms units but we want milli seconds
}

static void friend_delay_timer_callback_(uint32_t lpn_idx)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE4(TRACE_INFO, "friend_delay_timer_callback_: lpn_idx:%d state:%d fsn:%d node_id(low byte):%x\n", lpn_idx, p_lpn->state, p_lpn->fsn, node_nv_data.node_id);
    friend_print_cache_();//????
    // just exit if we are not provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return;

    switch(p_lpn->state)
    {
    case FRND_STATE_RECEIVED_REQUEST:
        // Friend Offer Delay is passed. Send Friend Offer message
        friend_send_offer_(lpn_idx);
        break;
    case FRND_STATE_SENT_OFFER:
        // We didn't receive Poll message within 1 sec after sending the Friend Offer.
        // Friendship establishment has failed
        friend_del_lpn_(lpn_idx);
        break;
    case FRND_STATE_RECEIVED_POLL:
        // ReceiveDelay is passed. We have to send Friend Update message. 
        // friend_send_cache_() with empty cache will add update to the cache and send it
        friend_send_cache_(lpn_idx);
        break;
    case FRND_STATE_SENT_RESPONSE:
        // No Poll, Subscription List Add, or Subscription List Remove messages are received during PollTimeout
        // Terminate the friendship
#ifndef _DEB_FRIEND_DONT_DEL_FRIENDSHIP_ON_POLL_TIMEOUT
        friend_del_lpn_(lpn_idx);
#endif
        break;
    case FRND_STATE_RECEIVED_SUBS_LIST_ADD:
    case FRND_STATE_RECEIVED_SUBS_LIST_REMOVE:
        // ReceiveDelay is passed. Send SubsList Confirm
        friend_send_subs_conf_(lpn_idx);
        break;
    case FRND_STATE_SENT_CLEAR_CONF:
        //Poll Timeout passed - delete that LPN record
        friend_del_lpn_(lpn_idx);
        break;
    }
    friend_print_cache_();//????
}

static void friend_send_cache_(uint32_t lpn_idx)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];
    tFRND_CACHE_MSG         *p_msg;
    wiced_bool_t            more_data;

    TRACE2(TRACE_INFO, "friend_send_cache_: lpn_idx:%d msg_cache_len:%d\n", lpn_idx, frnd_state.msg_cache_len);

    // get next message from the cache, assuming it is request for already sent message if expecting FSN differs from the received FSN
    p_msg = friend_cache_get_msg_(p_lpn->addr, p_lpn->received_fsn != p_lpn->fsn, &more_data);
    // update expecting FSN - it should be equals to inverted received FSN
    p_lpn->fsn = p_lpn->received_fsn ^ FRIENDSHIP_FRIEND_POLL_FSN_FLAG;
    // If cache is empty for that node then generate a new Friend Update message and add that message to the Friend Cache with SENT flag
    if (p_msg == NULL)
    {
        p_msg = friend_add_update_(lpn_idx);
        if (p_msg)
            p_msg->flags |= FRND_CACHE_MSG_FLAGS_SENT;
        more_data = WICED_FALSE;
    }

    if (p_msg)
    {
        uint8_t type = p_msg->flags & FRND_CACHE_MSG_FLAGS_TYPE_MASK;
        friend_print_cache_();//????
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0, 2, 1, 0, 0, 0);
#endif
        TRACE1(TRACE_INFO, "friend_send_cache_: type:%x\n", type);
        if (type == FRND_CACHE_MSG_FLAGS_TYPE_UPDATE)
        {
            p_msg->pdu[1 + 5] = more_data ? 1 : 0;
            transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_UPDATE, &p_msg->pdu[1], p_msg->len - 1, p_lpn->net_key_idx, &p_lpn->sec_material, node_nv_data.node_id, p_lpn->addr, 0);
        }
        else
        {
            network_layer_send(&p_msg->pdu[0], 1, &p_msg->pdu[1], p_msg->len - 1, p_lpn->net_key_idx, &p_lpn->sec_material,
                type == FRND_CACHE_MSG_FLAGS_TYPE_SEG_ACK,
                p_msg->src, p_msg->dst, p_msg->ttl, p_msg->seq, NULL, NULL);
        }
    }

    p_lpn->state = FRND_STATE_SENT_RESPONSE;
    friend_write_config_lpn_int_(lpn_idx);

    friend_start_timer_(&p_lpn->delay_timer, (uint64_t)p_lpn->poll_timeout * 100);    // poll_timeout is in 100ms units but we want milli seconds
}

static tFRND_CACHE_MSG *friend_add_update_(uint8_t lpn_idx)
{
    uint8_t                 buf[16];
    uint16_t                len;
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE1(TRACE_INFO, "friend_add_update_: lpn_idx\n", lpn_idx);

    // Low Transport PDU for that (unsegmented control) message: <SEG=0(1 bit)><opcode(7 bits)><parameters>
    buf[0] = MESH_TL_CM_OP_FRIEND_UPDATE;
    len = 1 + friend_crt_update(mesh_iv_update_active(), key_refresh_is_phase2(p_lpn->net_key_idx), BE4TOUINT32(node_nv_data.net_iv_index), WICED_FALSE, buf + 1);
    
    return friend_cache_put_(frnd_state.peer_lpn_state[lpn_idx]->addr, buf, (uint8_t)len, 0, FRND_CACHE_MSG_FLAGS_TYPE_UPDATE,
        node_nv_data.node_id, p_lpn->addr, node_nv_data.sequence_number, BE4TOUINT32(node_nv_data.net_iv_index));
}

static void friend_clear_repeat_timer_callback_(uint32_t lpn_idx)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE4(TRACE_INFO, "friend_clear_repeat_timer_callback_: lpn_idx:%d state:%d sent_clear_cnt:%d node_id(low byte):%x\n", lpn_idx, p_lpn->state, p_lpn->sent_clear_cnt, node_nv_data.node_id);
    // just exit if we are not provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return;

    friend_send_clear_(lpn_idx);
}

/**
* Creates Friend Clear message. It is sent to a Friend node to inform it about the removal of a friendship.
* The message is sent by the new Friend node after receiving the initial Friend Poll from the Low Power node
* confirming the new relationship.
*
* Parameters:
*   lpn_address:    The unicast address of the Low Power node being removed.
*   lpn_counter:    the LPNCounter value from the latest Friend Request used to establish the relationship.
*   buf:            Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t friend_crt_clear(uint16_t lpn_address, uint16_t lpn_counter, uint8_t *buf)
{
    uint16_t len = 0;
    UINT16TOBE2(buf + len, lpn_address);
    len += 2;
    UINT16TOBE2(buf + len, lpn_counter);
    len += 2;

    TRACE0(TRACE_INFO, "friend_crt_clear:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

/**
* Creates Friend Clear Confirm message. It is sent in response to the Friend Clear message from the new Friend node
* of a Low Power node to confirm that the friendship has been terminated.
*
* Parameters:
*   lpn_address:    The unicast address of the Low Power node being removed.
*   lpn_counter:    LPNCounter value from the corresponding Friend Clear message.
*   buf:            Buffer for created message. It should have suficient size to fit all message
*
* Return:   Length of the message in the buf.
*           0 on error.
*/
uint16_t friend_crt_clear_confirm(uint16_t lpn_address, uint16_t lpn_counter, uint8_t *buf)
{
    uint16_t len = 0;
    UINT16TOBE2(buf + len, lpn_address);
    len += 2;
    UINT16TOBE2(buf + len, lpn_counter);
    len += 2;

    TRACE0(TRACE_INFO, "friend_crt_clear_confirm:\n");
    TRACEN(TRACE_INFO, (char*)buf, len);
    return len;
}

static void friend_clear_procedure_timer_callback_(uint32_t lpn_idx)
{
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE4(TRACE_INFO, "friend_clear_procedure_timer_callback_: lpn_idx:%d state:%d sent_clear_cnt:%d node_id(low byte):%x\n", lpn_idx, p_lpn->state, p_lpn->sent_clear_cnt, node_nv_data.node_id);
    // just exit if we are not provisioned
    if (node_nv_data.node_id == MESH_NODE_ID_INVALID)
        return;

    TRACE2(TRACE_INFO, " state:%d sent_clear_cnt:%d\n", p_lpn->state, p_lpn->sent_clear_cnt);

    // stop clear repeat timer
    friend_stop_timer_(&p_lpn->clear_repeat_timer);

    // It is sign that timers are not initialized
    p_lpn->sent_clear_cnt = 0;
    //friend_write_config_state_int_();
    friend_write_config_lpn_int_(lpn_idx);
}

static void friend_send_clear_(uint32_t lpn_idx)
{
    uint8_t                 buf[16];
    uint16_t                len;
    tFRND_PEER_LPN_STATE    *p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    TRACE3(TRACE_INFO, "friend_send_clear_: lpn_idx:%d state:%d sent_clear_cnt:%d\n", lpn_idx, p_lpn->state, p_lpn->sent_clear_cnt);
    // if it is first call
    if (p_lpn->sent_clear_cnt == 0)
    {
        // Friend Clear Procedure timer shall be started with the period equal to two times the Friend Poll Timeout value
        friend_start_timer_(&p_lpn->clear_procedure_timer, p_lpn->poll_timeout * 100 * 2); // poll_timeout is in 100ms units but we want milli seconds
    }
    // Friend Clear Repeat timer shall be started with the period set to 1 second.
    // new Friend Clear message shall be sent and the timer shall be restarted with a period that is double the previous Friend Clear Repeat timer period
    friend_start_timer_(&p_lpn->clear_repeat_timer, (uint64_t)(FRIENDSHIP_CLEAR_REPEAT_START_TIMEOUT << p_lpn->sent_clear_cnt) * 1000);
    p_lpn->sent_clear_cnt++;
    friend_write_config_state_int_();

    // send controll message with max TTL and main net_key
    len = friend_crt_clear(p_lpn->addr, p_lpn->lpn_counter, buf);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 2, 1, 0, 0, 0);
#endif
    // Use master secure material
    transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_CLEAR, buf, len, p_lpn->net_key_idx, NULL, node_nv_data.node_id, p_lpn->previous_address, MESH_MAX_TTL);
}

static void friend_handle_clear_conf_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len)
{
    uint32_t                lpn_idx;
    tFRND_PEER_LPN_STATE    *p_lpn;
    uint16_t                lpn_address;

    // <LPN Address(2 bytes)>
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx >= 0                      // or it was not encrypted by the master security credentials
        || dst != node_nv_data.node_id          // or DST is not our address
        || params_len != 2)                     // or if length is wrong
        return;

    lpn_address = BE2TOUINT16(params);

    if (!friend_find_lpn(lpn_address, &lpn_idx))
    {
        TRACE1(TRACE_INFO, "friend_handle_clear_conf_: not found addr:%x\n", lpn_address);
        return;
    }

    p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    // Exit if Clear is not active
    if (p_lpn->sent_clear_cnt == 0
        || src != p_lpn->previous_address)          // or SRC is not node we sent clear to
    {
        TRACE0(TRACE_INFO, "friend_handle_clear_conf_: not active\n");
        return;
    }

    // Exit if source is not device we sent Friend Clear to
    if (src != p_lpn->previous_address)
    {
        TRACE2(TRACE_INFO, "friend_handle_clear_conf_: wrong src:%x expected previous_address:%x\n", src, p_lpn->previous_address);
        return;
    }

    p_lpn->sent_clear_cnt = 0;
    friend_write_config_state_int_();

    // stop both timers
    friend_stop_timer_(&p_lpn->clear_repeat_timer);
    friend_stop_timer_(&p_lpn->clear_procedure_timer);
}

static void friend_handle_clear_(int friend_idx, uint16_t dst, uint16_t src, uint8_t *params, uint16_t params_len, uint8_t net_key_idx)
{
    uint8_t                 buf[16];
    uint16_t                len;
    uint32_t                lpn_idx;
    tFRND_PEER_LPN_STATE    *p_lpn;
    uint16_t                lpn_address, lpn_counter;

    // <LPN Address(2 bytes)>
    // ignore that message if friend feature is not enabled (not supported or disabled)
    if (frnd_state.nvm.state != FRIEND_STATE_ENABLED
        || friend_idx >= 0                                    // or it was not encrypted by the master security credentials
        || dst != node_nv_data.node_id                      // or DST is not our address
        || params_len != 4)                                 //  or if length is wrong
        return;
    lpn_address = BE2TOUINT16(params);
    lpn_counter = BE2TOUINT16(params + 2);

    if (!friend_find_lpn(lpn_address, &lpn_idx))
    {
        TRACE1(TRACE_INFO, "friend_handle_clear_: not found addr:%x\n", lpn_address);
        return;
    }

    p_lpn = frnd_state.peer_lpn_state[lpn_idx];

    // The message should only be sent in response to a Friend Clear message received within the Poll Timeout
    if (p_lpn->state != FRND_STATE_SENT_RESPONSE && p_lpn->state != FRND_STATE_SENT_CLEAR_CONF)
    {
        TRACE1(TRACE_INFO, "friend_handle_clear_: wrong state:%d\n", p_lpn->state);
        return;
    }

    // A Friend Clear message is considered valid if the result of the subtraction of the value of the LPNCounter
    // field of the Friend Request message (the one that initiated the friendship) from the value
    // of the LPNCounter field // of the Friend Clear message, modulo 65536, is in the range 0 to 255 inclusive
    if (p_lpn->lpn_counter - lpn_counter > 255)
    {
        TRACE2(TRACE_INFO, "friend_handle_clear_: wrong p_lpn->lpn_counter:%x lpn_counter\n", p_lpn->lpn_counter, lpn_counter);
        return;
    }

    p_lpn->state = FRND_STATE_SENT_CLEAR_CONF;
    friend_write_config_lpn_int_(lpn_idx);

    // send controll message with max TTL and main net_key of the Friend Clear received message
    len = friend_crt_clear_confirm(lpn_address, lpn_counter, buf);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0, 2, 1, 0, 0, 0);
#endif
    // use master secure material
    transport_layer_send_ctrl(MESH_TL_CM_OP_FRIEND_CLEAR_CONF, buf, len, net_key_idx, NULL, node_nv_data.node_id, src, MESH_MAX_TTL);
}

/**
* Returns current state of the friend feature.
*
* Parameters:   None
*
* Return:   Current state of the friend feature. Van be any of FRIEND_STATE_XXX
*           0 on error.
*/
uint8_t friend_get_state(void)
{
    return frnd_state.nvm.state;
}

/**
* Terminates all friend relationships and clear the associated Friend Queue.
*
* Parameters:   None
*
* Return    None
*/
void friend_terminate_all(void)
{
    friend_reset_(WICED_TRUE);
}

/**
* Returns PollTimeout of the specific LPN lpn_address.
*
* Parameters:   lpn_address:    LPN unicast address
*
* Return    PollTimeout of the requested LPN
*/
uint32_t friend_get_poll_timeout(uint16_t lpn_address)
{
    uint8_t     lpn_idx;
    uint32_t    poll_timeout = 0;
    for (lpn_idx = 0; lpn_idx < frnd_state.nvm.peer_lpn_count; lpn_idx++)
    {
        if (frnd_state.peer_lpn_state[lpn_idx]->addr == lpn_address)
        {
            poll_timeout = frnd_state.peer_lpn_state[lpn_idx]->poll_timeout;
            break;
        }
    }
    return poll_timeout;
}

// Returns TRUE if that address is our friend LPN or in its subscription
wiced_bool_t friend_does_go_to_lpn(uint16_t src, uint16_t dst)
{
    uint32_t    i;
    for (i = 0; i < frnd_state.nvm.peer_lpn_count; i++)
    {
        // ignore LPN if it is source of that message
        if (frnd_state.peer_lpn_state[i]->addr == src)
            continue;
        if (frnd_state.peer_lpn_state[i]->addr == dst)
            break;
        if (friend_find_subs(i, dst, NULL))
            break;
    }
    return i < frnd_state.nvm.peer_lpn_count ? WICED_TRUE : WICED_FALSE;
}

