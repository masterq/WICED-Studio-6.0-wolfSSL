/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Network layer implementation.
 */
#include <stdio.h> 
#include <string.h> 
#include "platform.h"

#include "mesh_core.h"
#include "core_aes_ccm.h"
#include "mesh_util.h"
#include "transport_layer.h"
#include "key_refresh.h"
#include "foundation_int.h"
#include "low_power.h"
#include "friend.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__NETWORK_LAYER1_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__NETWORK_LAYER1_C
#include "mesh_trace.h"

#ifndef MESH_CONTROLLER
/**
* Local function to validate message by SRC and SEQ fields.
*
* Parameters:
*   src_id:         (in) Value of the SRC field of the message
*   seq:            (in) Value of the SEQ field of the message
*   iv_idx:         (in) LS byte of the IV Index of the message
*
* Return:   WICED_TRUE - we didn't receive yet the packet from that SRC with the same SEQ
*
*/
wiced_bool_t auth_cache_valid(uint16_t src_id, uint32_t seq, uint8_t iv_idx)
{
    int cache_idx;
    // find cache item for SRC and SEQ and return WICED_FALSE if found
    for (cache_idx = 0; cache_idx < MAX_SEQ_SRC_CACHE_ITEMS; cache_idx++)
    {
        if (src_id == ctx.auth_cache_src[cache_idx]
            && seq == ctx.auth_cache_seq[cache_idx]
            && iv_idx == ctx.auth_cache_iv_idx[cache_idx])
        {
            TRACE3(TRACE_INFO, "auth_cache_valid: found idx:%d iv_idx:%x src:%x\n", cache_idx, iv_idx, src_id);
            TRACE1(TRACE_INFO, " seq:%x\n", seq);
            return WICED_FALSE;
        }
    }
    ctx.auth_cache_src[ctx.auth_cache_pos] = src_id;
    ctx.auth_cache_seq[ctx.auth_cache_pos] = seq;
    ctx.auth_cache_iv_idx[ctx.auth_cache_pos] = iv_idx;
    TRACE3(TRACE_INFO, "auth_cache_valid: added item idx:%d iv_idx:%x src:%x\n", ctx.auth_cache_pos, iv_idx, src_id);
    TRACE1(TRACE_INFO, " seq:%x\n", seq);
    if (++ctx.auth_cache_pos >= MAX_SEQ_SRC_CACHE_ITEMS)
        ctx.auth_cache_pos = 0;
    return WICED_TRUE;
}
#endif

wiced_bool_t network_layer_find_subs_(uint16_t dst_id)
{
    wiced_bool_t                found = WICED_FALSE;
    tFND_NVM_ELEMENT    *p_elem;
    tFND_NVM_MODEL      *p_model;
    uint8_t               elem_idx, model_idx, subs_idx;

    //TRACE2(TRACE_INFO, "network_layer_find_subs_: fnd_config_:%x dst_id:%x\n", (uint32_t)fnd_config_, dst_id);
    //TRACEN(TRACE_INFO, (char*)fnd_config_, fnd_config_len_);

    // posit on first configured element
    p_elem = &fnd_config_->elements[0];
    // go through each configured element
    for (elem_idx = 0; !found && elem_idx < fnd_static_config_.elements_num; elem_idx++)
    {
        // go through each model in the current configured element and copy SIG models to the message
        for (model_idx = 0; !found && model_idx < p_elem->models_num; model_idx++)
        {
            p_model = &p_elem->models[model_idx];
            for (subs_idx = 0; !found && subs_idx < p_model->subs_num; subs_idx++)
            {
                if (dst_id == p_model->subs_list[subs_idx])
                    found = WICED_TRUE;
            }
        }
        // posit on the next configured element
        p_elem = (tFND_NVM_ELEMENT*)&p_elem->models[p_elem->models_num];
    }
    return found;
}

wiced_bool_t network_layer_handle_check_len_(const uint8_t *msg_data, uint32_t msg_len)
{
    //TRACE1(TRACE_CRITICAL, "network_layer_handle_check_len_: len = %d\n", msg_len);
    //TRACEN(TRACE_DEBUG, (char*)msg_data, msg_len);

    // Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits)) || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
    //???// TRANSP_PAYLOAD: TRANSP_CTRL(1-4bytes) || APP_PAYLOAD(0-11bytes) || APP_MIC(4bytes)
    if ((msg_len < MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN + MESH_MIN_TRANSP_PAYLOAD_LEN + MESH_MIC_LEN))
    {
        TRACE1(TRACE_INFO, "network_layer_handle_check_len_: len = %d. Ignore it.\n", msg_len);
        // drop that packet
        return WICED_FALSE;
    }
    return WICED_TRUE;
}

// out_msg should have length ADV_LEN_MAX + 2
// returns: bits: 0x01 - should be retransmitted; 0x02 - targeted to our device; 0x04 - targeted to our friend LPN
uint8_t network_layer_handle_parse_(const uint8_t *msg, uint32_t msg_len, wiced_bool_t from_gatt, int friend_idx, uint8_t* p_mic_len, uint16_t src_id, uint16_t *p_dst_id, wiced_bool_t *p_ctl, uint8_t *p_ttl)
{
    uint8_t    res = 0;
    uint16_t   dst_id;
    uint8_t    ttl, ctl;

    // Network PDU: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL+TTL(7bits)) || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8 bytes)
    dst_id = BE2TOUINT16(msg + MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_LEN);

    // secod byte contains CTL and TTL
    ttl = msg[MESH_IVI_NID_LEN];
    ctl = ttl & MESH_CTL_MASK;
    ttl &= MESH_TTL_MASK;
    *p_mic_len = ctl ? MESH_MIC_LEN2 : MESH_MIC_LEN;

    // virtual address is not valid in Control Messages
    if (ctl != 0 && (dst_id & MESH_GROUP_MASK) == MESH_VIRT_ADDR)
    {
        TRACE0(TRACE_INFO, "network_layer_handle_parse_: Ignore controll message with virtual dst\n");
        return 0;
    }

    // Drop packet if dst is prohibited
    if (dst_id >= MESH_FIXED_GROUP_RFU_MIN && dst_id <= MESH_FIXED_GROUP_RFU_MAX)
    {
        TRACE0(TRACE_INFO, " Ignore prohibited dst\n");
        return 0;
    }

    // if peer is connected to us and that message came from GATT then we know ID of the connected proxy
    if (from_gatt && ctx.conn_id)
        ctx.connected_dev_id = src_id;

    *p_dst_id = dst_id;
    *p_ctl = ctl != 0;
    *p_ttl = ttl;

#ifndef MESH_CONTROLLER
    //Check if this message is targeted to our device only (no retransmittions and no friend LPNs)
    if (dst_id >= node_nv_data.node_id && dst_id < (node_nv_data.node_id + fnd_static_config_.elements_num))
    {
        res = 2;
    }
    // If dst is unicast
    else if ((dst_id & MESH_NON_UNICAST_MASK) == 0)
    {
        // TTL should be > 1 to send it to our friend LPN queue or to retransmit
        if (ttl > 1)
        {
            // if dst is our friend LPN then message targeted to LPN
            if (friend_find_lpn(dst_id, NULL))
                res |= 4;
            // otherwise retransmit if relay feature is on
            else if (node_nv_data.state_relay == FND_STATE_RELAY_ON)
                res |= 1;
        }
    }
    else
    {
        // dst isn't unicast
        // TTL should be > 1 to send it to our friend LPN queue or to retransmit
        if (ttl > 1)
        {
            // retransmit if relay feature is on
            if (node_nv_data.state_relay == FND_STATE_RELAY_ON)
                res |= 1;
            // check if it is targeted to our friend LPN
            if (friend_does_go_to_lpn(src_id, dst_id))
                res |= 4;
        }
        // it is targeted to our device if it is broadcast message
        if (dst_id == MESH_FIXED_GROUP_ALL_NODES
            || (dst_id == MESH_FIXED_GROUP_ALL_PROXIES && node_nv_data.state_gatt_proxy == FND_STATE_PROXY_ON)  // or all_proxies and proxy is on
            || (dst_id == MESH_FIXED_GROUP_ALL_FRIENDS && friend_get_state() == FRIEND_STATE_ENABLED)           // or all friends and friend feature is enabled
            || (dst_id == MESH_FIXED_GROUP_ALL_RELAYS && node_nv_data.state_relay == FND_STATE_RELAY_ON)        // or all relays and relay feature is enabled
            || ((dst_id & MESH_NON_UNICAST_MASK) != 0 && network_layer_find_subs_(dst_id))                      // or non-unicast address (group or virtual) and we have subscription for it
            || (ctl != 0))                                                                                      // pass all control message to transport
        {
            res |= 2;
        }
    }

    TRACE4(TRACE_INFO, "network_layer_handle_parse_: from_gatt:%d state_relay:%d conn_id:%x friend_idx:%d\n", from_gatt, node_nv_data.state_relay, ctx.conn_id, friend_idx);
    TRACE4(TRACE_DEBUG, " elements_num:%d features:%x mic_len:%d returns:%x\n", fnd_static_config_.elements_num, fnd_static_config_.features, *p_mic_len, res);

    TRACE4(TRACE_DEBUG, "******************* ctl:%d ttl:%x src_id:%x dst_id:%x SEQ: PDU:\n", ctl, ttl, src_id, dst_id);
    TRACEN(TRACE_DEBUG, (char*)msg + (MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN), MESH_PKT_SEQ_LEN);
    TRACEN(TRACE_DEBUG, (char*)msg + (MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN), msg_len - *p_mic_len - (MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN));

    // if it is access message encrypted by device key
    if (!ctl && (msg[MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN] & TL_CP_AKF_MASK) == 0)
    {
        // don't pass message to the transport layer but We need to relay it or to put to LPN queue to pass PTS test
        // if destination address is not unicast DST or AID != 0
        if (((dst_id & MESH_NON_UNICAST_MASK) != 0 || (msg[MESH_IVI_NID_LEN + MESH_CTL_TTL_LEN + MESH_SEQ_SRC_DST_LEN] & TL_CP_AID_MASK) != 0)
            && friend_idx != -1) // and it is not encrypted by friend sec material
        {
            TRACE0(TRACE_INFO, " packet encrypted by device key with non-unicast dst or with non-0 AID\n");
            res &= 5;
        }
    }

#else
    res = 2;
#endif

    return res;
}

void network_layer_send_prepare_(const uint8_t* ctrl_field, uint8_t ctrl_field_len, const uint8_t* app_payload, uint8_t app_payload_len,
    MeshSecMaterial *p_sec_material, wiced_bool_t ctl, uint16_t src, uint16_t dst, uint8_t ttl, uint32_t seq, uint8_t *msg, uint32_t *p_msg_len)
{
    uint32_t  msg_len;

    TRACE2(TRACE_INFO, "network_layer_send_prepare_: ctl:%d ttl:%d\n", ctl, ttl);
    TRACE1(TRACE_INFO, " dst:%x ctr[, payload]:\n", dst);
    TRACEN(TRACE_INFO, (char*)ctrl_field, ctrl_field_len);
    if (app_payload_len)
        TRACEN(TRACE_INFO, (char*)app_payload, app_payload_len);

    // Packet format: IN(1BYTE:IVI[1LSB]+NID[7LSB]) || CT(1byte:CTL(1bit)+TTL(7bits)) || SEQ(3bytes) || SRC(2bytes) || DST(2bytes) || TRANSP_PAYLOAD(1-16bytes) || NET_MIC(4 or 8bytes)
    if (ttl == MESH_USE_DEFAULT_TTL)
#ifndef MESH_CONTROLLER
        ttl = ctx.default_ttl;
#else
        ttl = MESH_DEFAULT_TTL;
#endif
    // make sure TLL fits into field
    else if (ttl > MESH_TTL_MASK)
        ttl = MESH_TTL_MASK;

    // create packet in the buffer
    msg_len = 0;
    // IN(1BYTE:IVI[1LSB]+NID[7LSB])
    msg[msg_len++] = (node_nv_data.net_iv_index[MESH_IV_INDEX_LEN - 1] << 7) | p_sec_material->nid;
    // IV Update in Progress state transmit using the current IV Index - 1
    if (node_nv_data.iv_update_state == MESH_IV_UPDT_STATE_ACTIVE)
        msg[msg_len - 1] ^= 0x80;
    // CT(1byte:CTL(1bit)+TTL(7bits))
    msg[msg_len++] = (ctl ? MESH_CTL_MASK : 0) | (ttl > MESH_MAX_TTL ? MESH_MAX_TTL : ttl);
    // SEQ(3bytes)
    if (seq == 0xffffffff)
        seq = node_nv_data.sequence_number;
    UINT32TOBE3(&msg[msg_len], seq);
    msg_len += 3;
    // SRC
    msg[msg_len++] = (uint8_t)(src >> 8);
    msg[msg_len++] = (uint8_t)src;
    // DST
    msg[msg_len++] = (uint8_t)(dst >> 8);
    msg[msg_len++] = (uint8_t)dst;
    // transport Control Field
    memcpy(&msg[msg_len], ctrl_field, ctrl_field_len);
    msg_len += ctrl_field_len;
    // application payload
    if (app_payload_len)
    {
        memcpy(&msg[msg_len], app_payload, app_payload_len);
        msg_len += app_payload_len;
    }

    // update SEQ
    node_nv_data.sequence_number = (node_nv_data.sequence_number + 1) & 0xFFFFFF;

    TRACE0(TRACE_WARNING, "network_layer_send_prepare_: msg\n");
    TRACEN(TRACE_WARNING, (char*)msg, msg_len);

    *p_msg_len = msg_len;

    mesh_save_node_nv_data();
}

