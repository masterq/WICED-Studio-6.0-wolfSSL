/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh provisioning transport layer implementation.
 */
#include <stdio.h> 
#include <string.h> 

#include "platform.h"

#include "ccm.h"
#ifdef MESH_CONTROLLER
#include "aes_cmac.h"
#endif

#include "mesh_core.h"
#include "core_ovl.h"
#include "core_aes_ccm.h"

#include "mesh_util.h"

#include "ecdh.h"

#include "pb_transport.h"
#include "wiced_bt_mesh_provision.h"

//#define NO_FCS_CHECK

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__PB_TRANSPORT_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__PB_TRANSPORT_C
#include "mesh_trace.h"

//--------------------------------- PB_ADV transport layer API --------------------------------------

// PB_ADV: The timeout for receiving an ACK on Link Open message 
#define MESH_PB_ADV_OPEN_ACK_TO           3
// PB_ADV: The timeout for link establishment (transmitting and retransmitting Link Open message)
#define MESH_PB_ADV_OPEN_TO               30
// PB_ADV: The timeout for receiving an ACK on transaction transfer
#define MESH_PB_ADV_TRANSACTION_ACK_TO    3
// PB_ADV: The timeout for transaction (transmitting and retransmitting provisioning message)
#define MESH_PB_ADV_TRANSACTION_TO        30


/* possible state of the connection - values of the BpTransportCb::state */
#define BP_TRANSPORT_STATE_SENT_OPEN            1
#define BP_TRANSPORT_STATE_OPENED               2
#define BP_TRANSPORT_STATE_RECEIVING_PACKET     3

/* PB_ADV Link establishment: Values for Info field with contrl field = MESH_PROV_CTRL_TRANS_SPEC */
#define PB_ADV_MSG_TYPE_LINK_OPEN        0x00    /* Link Open */
#define PB_ADV_MSG_TYPE_LINK_OPEN_CFM    0x01    /* Link ACK */
#define PB_ADV_MSG_TYPE_LINK_CLOSE       0x02    /* Link Close */

/* PB_ADV Link establishment: Values for Info field with contrl field = MESH_PROV_CTRL_TRANS_SPEC */
#define PB_ADV_INFO_LINK_OPEN        0x00    /* Link Open */
#define PB_ADV_INFO_LINK_OPEN_ACK    0x01    /* Link ACK */
#define PB_ADV_INFO_LINK_CLOSE       0x02    /* Link Close */

typedef struct
{
    uint32_t  link_id;                // link id (connection id) for PB_ADV
    uint8_t   state;                  // one of the BP_TRANSPORT_STATE_XXX
    uint8_t   data[2 + WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN]; //buffer for assembling of the received segmented data. Also we use it to save uuid to retransmit OPEN packet
    uint16_t  data_len;               //lenght of the data in the buffer data
    uint8_t   out_data[2 + WICED_BT_MESH_PROVISION_PUBLIC_KEY_LEN]; //buffer for last sent data for possible retransmittion
    uint16_t  out_data_len;           //lenght of the last last sent data for possible retransmittion
                                    // 0 means we received ack and don't need to retransmit anymore
    // fields for states SENT_OPEN and SENT_PACKET
    uint8_t   sent_transactions;      // number of sent transactions
    uint8_t   ack_to;                 // remaining seconds of ack timeout (valid for states SENT_OPEN and SENT_PACKET)
    uint8_t   trans_to;               // Time for transmition and retransmittions transaction or OPEN packet (in states SENT_OPEN and SENT_PACKET)
                                    // Time to receive transaction message(strats from enetering OPENED. Ends by entering SENT_PACKET state or exiting RECEIVING_PACKET)
    // fields for state RECEIVING_PACKET
    uint8_t   lst_rcv_trans_num;      // Transaction number of the last received messsage. Value 0xff means we didn't receive message yet
    uint8_t   rcv_trans_num;          // Currently receiving transaction number 
    uint16_t  data_full_len;          //full lenght of the data received in the first packet
    uint8_t   fcs;                    // checksum of the data
    uint8_t   fragments_number;       //number of fragments
    uint8_t   received_fragments;     // bit mask of the received fragments
} BpTransportCb;

#define PB_TRANSPORT_MAX_CONNECTIONS    1

// -------------------- forward declarations --------------
static void pb_adv_send_(uint32_t conn_idx, const uint8_t* data, uint8_t data_len);

// --------------------- static variables ----------------------
static BpTransportCb* pb_transport_cb[PB_TRANSPORT_MAX_CONNECTIONS];
uint8_t pb_transport_cb_cnt = 0;

#ifdef _DEB_USE_STATIC_ALLOC_PROV_TRANSP
static BpTransportCb    s_pb_transport_cb;
#endif

//--------------- static functions -----------------------
static int pb_transport_find_conn_(uint32_t conn_id)
{
    uint8_t i;
    for (i = 0; i < pb_transport_cb_cnt; i++)
    {
        if (conn_id == pb_transport_cb[i]->link_id)
                break;
    }
    return i < pb_transport_cb_cnt ? i : -1;
}

static int pb_transport_alloc_conn_(
    uint32_t                            conn_id)
{
    int conn_idx = -1;
    if (pb_transport_cb_cnt < PB_TRANSPORT_MAX_CONNECTIONS)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV_TRANSP
        if (pb_transport_cb_cnt == 0)
            pb_transport_cb[pb_transport_cb_cnt] = &s_pb_transport_cb;
        else
#endif
            pb_transport_cb[pb_transport_cb_cnt] = (BpTransportCb*)wiced_memory_allocate(sizeof(BpTransportCb));
        if (pb_transport_cb[pb_transport_cb_cnt] != NULL)
        {
            conn_idx = pb_transport_cb_cnt++;
            memset(pb_transport_cb[conn_idx], 0, sizeof(BpTransportCb));
            pb_transport_cb[conn_idx]->lst_rcv_trans_num = 0xff;
            pb_transport_cb[conn_idx]->link_id = conn_id;
            TRACE2(TRACE_WARNING, "pb_transport_alloc_conn_: idx:%d len:%d\n", conn_idx, sizeof(BpTransportCb));
            TRACE1(TRACE_WARNING, " id:%x\n", conn_id);
        }
        else
        {
            TRACE1(TRACE_WARNING, "pb_transport_alloc_conn_: wiced_memory_allocate failed len:%d\n", sizeof(BpTransportCb));
        }
    }
    else
    {
        TRACE0(TRACE_INFO, "pb_transport_alloc_conn_: no available conn\n");
    }
    return conn_idx;
}

static void pb_transport_del_conn_(uint32_t conn_idx)
{
    TRACE2(TRACE_WARNING, "pb_transport_del_conn_: idx:%d cnt:%d\n", conn_idx, pb_transport_cb_cnt);
    if (conn_idx < pb_transport_cb_cnt)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV_TRANSP
        if (pb_transport_cb_cnt != 1)
#endif
        wiced_memory_free(pb_transport_cb[conn_idx]);
        pb_transport_cb_cnt--;
        while (conn_idx++ < pb_transport_cb_cnt)
        {
            pb_transport_cb[conn_idx-1] = pb_transport_cb[conn_idx];
        }
    }
}

#ifndef MESH_CONTROLLER
#if 0
// taken from fw_20732A1/fw/misc/utilslib.c,h
extern uint8_t crc8_updateBlock(uint8_t* data, uint32_t len, uint8_t seed);
#define pb_crc8_updateBlock crc8_updateBlock
#else
static const uint8_t crctable[256] = { //reversed, 8-bit, poly=0x07
    0x00, 0x91, 0xE3, 0x72, 0x07, 0x96, 0xE4, 0x75, 0x0E, 0x9F, 0xED, 0x7C, 0x09, 0x98, 0xEA, 0x7B,
    0x1C, 0x8D, 0xFF, 0x6E, 0x1B, 0x8A, 0xF8, 0x69, 0x12, 0x83, 0xF1, 0x60, 0x15, 0x84, 0xF6, 0x67,
    0x38, 0xA9, 0xDB, 0x4A, 0x3F, 0xAE, 0xDC, 0x4D, 0x36, 0xA7, 0xD5, 0x44, 0x31, 0xA0, 0xD2, 0x43,
    0x24, 0xB5, 0xC7, 0x56, 0x23, 0xB2, 0xC0, 0x51, 0x2A, 0xBB, 0xC9, 0x58, 0x2D, 0xBC, 0xCE, 0x5F,
    0x70, 0xE1, 0x93, 0x02, 0x77, 0xE6, 0x94, 0x05, 0x7E, 0xEF, 0x9D, 0x0C, 0x79, 0xE8, 0x9A, 0x0B,
    0x6C, 0xFD, 0x8F, 0x1E, 0x6B, 0xFA, 0x88, 0x19, 0x62, 0xF3, 0x81, 0x10, 0x65, 0xF4, 0x86, 0x17,
    0x48, 0xD9, 0xAB, 0x3A, 0x4F, 0xDE, 0xAC, 0x3D, 0x46, 0xD7, 0xA5, 0x34, 0x41, 0xD0, 0xA2, 0x33,
    0x54, 0xC5, 0xB7, 0x26, 0x53, 0xC2, 0xB0, 0x21, 0x5A, 0xCB, 0xB9, 0x28, 0x5D, 0xCC, 0xBE, 0x2F,
    0xE0, 0x71, 0x03, 0x92, 0xE7, 0x76, 0x04, 0x95, 0xEE, 0x7F, 0x0D, 0x9C, 0xE9, 0x78, 0x0A, 0x9B,
    0xFC, 0x6D, 0x1F, 0x8E, 0xFB, 0x6A, 0x18, 0x89, 0xF2, 0x63, 0x11, 0x80, 0xF5, 0x64, 0x16, 0x87,
    0xD8, 0x49, 0x3B, 0xAA, 0xDF, 0x4E, 0x3C, 0xAD, 0xD6, 0x47, 0x35, 0xA4, 0xD1, 0x40, 0x32, 0xA3,
    0xC4, 0x55, 0x27, 0xB6, 0xC3, 0x52, 0x20, 0xB1, 0xCA, 0x5B, 0x29, 0xB8, 0xCD, 0x5C, 0x2E, 0xBF,
    0x90, 0x01, 0x73, 0xE2, 0x97, 0x06, 0x74, 0xE5, 0x9E, 0x0F, 0x7D, 0xEC, 0x99, 0x08, 0x7A, 0xEB,
    0x8C, 0x1D, 0x6F, 0xFE, 0x8B, 0x1A, 0x68, 0xF9, 0x82, 0x13, 0x61, 0xF0, 0x85, 0x14, 0x66, 0xF7,
    0xA8, 0x39, 0x4B, 0xDA, 0xAF, 0x3E, 0x4C, 0xDD, 0xA6, 0x37, 0x45, 0xD4, 0xA1, 0x30, 0x42, 0xD3,
    0xB4, 0x25, 0x57, 0xC6, 0xB3, 0x22, 0x50, 0xC1, 0xBA, 0x2B, 0x59, 0xC8, 0xBD, 0x2C, 0x5E, 0xCF
};

uint8_t pb_crc8_updateBlock(uint8_t *data, uint32_t len, uint8_t seed)
{
    uint8_t fcs = seed;
    while (len--) {
        fcs = crctable[fcs ^ *data++];
    }
    /*Ones complement*/
    fcs = 0xFF - fcs;
    return fcs;
}

#endif
#else
// See math in Annex B of http://www.3gpp.org/ftp/tsg_t/tsg_t/tsgt_04/docs/pdfs/TP-99119.pdf
#if 1
// taken from https://chromium.googlesource.com/chromiumos/platform/vboot_reference/+/master/firmware/lib/crc8.c
// Return CRC-8 of the data, using x^8 + x^2 + x + 1 polynomial.  A table-based algorithm would be faster
uint8_t pb_crc8_updateBlock(uint8_t* data, uint32_t len, uint8_t seed)
{
    unsigned crc = seed;
    int i, j;
    for (j = len; j; j--, data++) {
        crc ^= (*data << 8);
        for (i = 8; i; i--) {
            if (crc & 0x8000)
                crc ^= (0x1070 << 3);
            crc <<= 1;
        }
    }
    return (uint8_t)(crc >> 8);
}
#else
// http://electronics.stackexchange.com/questions/33821/calculating-a-simple-crc
// table-based algorithm : Return CRC-8 of the data, using x^8 + x^2 + x + 1 polynomial 
#define GP  0x107   /* x^8 + x^2 + x + 1 */
#define DI  0x07
static uint8_t crc8_table[256];     /* 8-bit table */
static int made_table = 0;

static void init_crc8()
{
    int i, j;
    uint8_t crc;
    if (!made_table) {
        for (i = 0; i<256; i++) {
            crc = i;
            for (j = 0; j<8; j++)
                crc = (crc << 1) ^ ((crc & 0x80) ? DI : 0);
            crc8_table[i] = crc & 0xFF;
        }
        made_table = 1;
    }
}

uint8_t pb_crc8_updateBlock(uint8_t* data, uint32_t len, uint8_t seed)
{
    uint8_t crc = seed;
    uint32_t i;
    if (!made_table)
        init_crc8();
    for (i = 0; i < len; i + )
    {
        crc = crc8_table[(crc) ^ *data++];
    }
    return crc;
}
#endif
#endif

/* Values returned by pb_transport_handle_frag */
#define PB_TRANSPORT_HANDLE_FRAG_RES_FAILED                 0   /* Failed */
#define PB_TRANSPORT_HANDLE_FRAG_RES_PENDING                1   /* Intermediate fragment of the message is handled successfully */
#define PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED              2   /* Last fragment of the new message is assembled successfully */
#define PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED_OLD_PACKET   3   /* Last fragment of the retransmitted message is assembled successfully */
#define PB_TRANSPORT_HANDLE_FRAG_RES_ACK                    4   /* acknowledge is receved successfully */

/**
* Static function for to handle provisioning fragment packets received via ADV and assebles them into message.
*
* Parameters:
*   conn_id:    Connection ID
*   trans_num:  Transaction number
*   p_data:     Fragment packet to handle: <ctrl-2bits,info-6bits>[1byte],<data>[>=1byte]
*   data_len:   Length of the fragment packet to handle.
*
* Return: one of the PB_TRANSPORT_HANDLE_FRAG_RES_XXX
*
*/
static int pb_transport_handle_frag_(uint32_t conn_idx, uint8_t trans_num, const uint8_t* p_data, uint8_t data_len)
{
    int     res = PB_TRANSPORT_HANDLE_FRAG_RES_FAILED;
    uint8_t ctrl, info;
    uint8_t off, all_fragments;

    TRACE3(TRACE_WARNING, "handle_frag: conn_idx:%d ack_to:%d received_fragments=%x\n", conn_idx, pb_transport_cb[conn_idx]->ack_to, pb_transport_cb[conn_idx]->received_fragments);
    TRACE2(TRACE_WARNING, " full_len=%d out_data_len:%d\n", pb_transport_cb[conn_idx]->data_full_len, pb_transport_cb[conn_idx]->out_data_len);

    do
    {
        ctrl = *p_data++;
        data_len--;
        info = ctrl >> 2;
        ctrl &= MESH_PROV_CTRL_MASK;
        // if it is control message
        if (ctrl == MESH_PROV_CTRL_CTRL_MSG)
        {
            // only ack control message with no data is possible
            if (info != MESH_PROV_CTRL_CTRL_MSG_ACK || data_len != 0)
                break;
            // we expect ACK only if we sent the provisioning message and keep it in the out_data for possible retransmittion
            if (pb_transport_cb[conn_idx]->out_data_len == 0)
                break;
#ifndef MESH_CONTROLLER
            // make sure it is ACK for correct transaction
            if (trans_num != (pb_transport_cb[conn_idx]->sent_transactions + 1))
            {
                break;
            }
            // stop retransmitting(advertising) that packet
#endif
            // clear acknowledge timeout
            pb_transport_cb[conn_idx]->ack_to = 0;
            //pb_transport_cb[conn_idx]->trans_to = 0;
            //increment sent transactions counter and clear out_data_len as a sign of successfylly sent message
            pb_transport_cb[conn_idx]->sent_transactions++;
            pb_transport_cb[conn_idx]->out_data_len = 0;
            res = PB_TRANSPORT_HANDLE_FRAG_RES_ACK;
            break;
        }
        // if it is start of message
        if (ctrl == MESH_PROV_CTRL_FRAG_NUM)
        {
            // for start fragment we have to be in the OPENED state
            if (pb_transport_cb[conn_idx]->state != BP_TRANSPORT_STATE_OPENED
                && pb_transport_cb[conn_idx]->state != BP_TRANSPORT_STATE_RECEIVING_PACKET)
            {
                TRACE0(TRACE_INFO, "handle_frag: wrong state\n");
                break;
            }
            // <len-2bytes><fcs-1byte><data-minimum 1 byte>
            // info cintains SegN (last segment number)
            if (data_len < 4)
            {
                TRACE0(TRACE_INFO, "handle_frag: wrong len\n");
                break;
            }
#ifndef MESH_CONTROLLER
            // if it is not first transaction 
            if (pb_transport_cb[conn_idx]->lst_rcv_trans_num != 0xff)
            {
                //fail if packet has older transaction number then last received
                // we have to receive and acknoladge message with same as last transaction number
                // but should not handle it
                if (trans_num < pb_transport_cb[conn_idx]->lst_rcv_trans_num)
                {
                    TRACE0(TRACE_INFO, "handle_frag: wrong trans_num\n");
                    break;
                }
            }
            // it is start of transaction - remember transaction number
            pb_transport_cb[conn_idx]->rcv_trans_num = trans_num;
#endif
            pb_transport_cb[conn_idx]->fragments_number = info + 1;
            pb_transport_cb[conn_idx]->data_len = 0;
            pb_transport_cb[conn_idx]->data_full_len = BE2TOUINT16(p_data);
            // make sure data fits into buffer
            if (pb_transport_cb[conn_idx]->data_full_len < 1 || pb_transport_cb[conn_idx]->data_full_len > sizeof(pb_transport_cb[conn_idx]->data))
            {
                TRACE0(TRACE_INFO, "handle_frag: doesn't fit\n");
                break;
            }
            pb_transport_cb[conn_idx]->fcs = p_data[2];
            pb_transport_cb[conn_idx]->received_fragments = 1;
            p_data += 3;
            data_len -= 3;
            // copy first fragment
            memcpy(&pb_transport_cb[conn_idx]->data[0], p_data, data_len);
        }
        // if it is the message continuation
        else if (ctrl == MESH_PROV_CTRL_FRAG_IDX)
        {
            // for continuation fragment we have to be in the RECEIVING_PACKET state
            if (pb_transport_cb[conn_idx]->state != BP_TRANSPORT_STATE_RECEIVING_PACKET)
                break;
            // <data-minimum 1 byte>
            if (data_len < 1
                || info >= pb_transport_cb[conn_idx]->fragments_number)
                break;
#ifndef MESH_CONTROLLER
            // fail if packet has wrong transaction
            if (trans_num != pb_transport_cb[conn_idx]->rcv_trans_num)
                break;
#endif
            // offset of that segment in the data buffer
            off = 20 + 23 * (info - 1);

            // Each Generic Provisioning PDU shall be the length of the full MTU for that bearer, except for the last segment of a transaction.
            if (info != (pb_transport_cb[conn_idx]->fragments_number - 1))
            {
                if (data_len != 23)
                    break;
            }
            else
            {
                // it is last 
                if (data_len != pb_transport_cb[conn_idx]->data_full_len - off)
                    break;
            }

            pb_transport_cb[conn_idx]->received_fragments |= ((uint8_t)1 << info);
            // copy data
            memcpy(&pb_transport_cb[conn_idx]->data[off], p_data, data_len);
        }
        else
        {
            break;
        }
        // if we haven't received yet all fragments
        all_fragments = ((uint8_t)1 << pb_transport_cb[conn_idx]->fragments_number) - 1;
        if(pb_transport_cb[conn_idx]->received_fragments != all_fragments)
        {
            pb_transport_cb[conn_idx]->state = BP_TRANSPORT_STATE_RECEIVING_PACKET;
            res = PB_TRANSPORT_HANDLE_FRAG_RES_PENDING;
            break;
        }
        // we received all data
        pb_transport_cb[conn_idx]->data_len = pb_transport_cb[conn_idx]->data_full_len;
#ifndef MESH_CONTROLLER
        // check fcs
        if (pb_transport_cb[conn_idx]->fcs == 0)
        {
            TRACE0(TRACE_INFO, "handle_frag: ignore 0 fcs\n");
        }
        else
        {
            uint8_t fcs = pb_crc8_updateBlock(pb_transport_cb[conn_idx]->data, pb_transport_cb[conn_idx]->data_len, 0xff);
#ifndef NO_FCS_CHECK
            if (fcs != pb_transport_cb[conn_idx]->fcs)
            {
                TRACE2(TRACE_INFO, "handle_frag: invalid fcs:%02x expected:%02x\n", fcs, pb_transport_cb[conn_idx]->fcs);
                break;
            }
#endif
        }
#endif
        // all fragments are successfully received and reassembled
        pb_transport_cb[conn_idx]->state = BP_TRANSPORT_STATE_OPENED;
        // the acknowledgment shall be sent
        // if it is old repeated packet then dont notify upper layer
        if (pb_transport_cb[conn_idx]->lst_rcv_trans_num != 0xff
                       && pb_transport_cb[conn_idx]->lst_rcv_trans_num == trans_num)
            res = PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED_OLD_PACKET;
        else
        {
            // it is new packet.
            // we can't receive message if we are waiting for ack on sent message
            // It means we lost ack from the peer device and we will need to retransmit our message on timeout
            if (pb_transport_cb[conn_idx]->out_data_len)
            {
                TRACE0(TRACE_INFO, "handle_frag: sending\n");
                break;
            }
            // Update last received transaction number
            pb_transport_cb[conn_idx]->lst_rcv_trans_num = trans_num;
            res = PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED;
        }
    } while (WICED_FALSE);

    TRACE4(TRACE_INFO, "handle_frag: returns %d state:%d received_fragments=%x data_len=%d\n", res, pb_transport_cb[conn_idx]->state, pb_transport_cb[conn_idx]->received_fragments, pb_transport_cb[conn_idx]->data_len);
    return res;
}

/**
* Static function for PB_ADV to processes received provisioning packets
*
* Parameters:
*   conn_idx:   Index of the transaction controll block in the array pb_transport_cb
*   trans_num:  Transaction number
*   packet:     Packet to process.
*               packet format: <ctrl-2bits,info-6bits>[1byte],<data>[1-67bytes]
*   packet_len: Length of the packet to process
*
* Return:   None
*
*/
static void pb_adv_handle_data_(uint32_t conn_idx, uint8_t trans_num, const uint8_t* packet, uint8_t packet_len)
{
    wiced_bool_t bRes = WICED_FALSE;
    uint8_t msg[2];
    uint32_t conn_id = pb_transport_cb[conn_idx]->link_id;
    TRACE4(TRACE_INFO, "pb_adv_handle_data_: conn_idx:%d trans_num:%d sent_transactions:%d lst_rcv_trans_num:%d\n", conn_idx, trans_num, pb_transport_cb[conn_idx]->sent_transactions, pb_transport_cb[conn_idx]->lst_rcv_trans_num);
    TRACE2(TRACE_INFO, " rcv_trans_num:%d state:%d\n", pb_transport_cb[conn_idx]->rcv_trans_num, pb_transport_cb[conn_idx]->state);
    TRACEN(TRACE_INFO, (char*)packet, packet_len);

    int res = pb_transport_handle_frag_(conn_idx, trans_num, packet, packet_len);
    switch (res)
    {
    case PB_TRANSPORT_HANDLE_FRAG_RES_FAILED:
        // Ignore all unexpected and bad packets. It will cause retransmit on timeout
        bRes = WICED_TRUE;
        break;
    case PB_TRANSPORT_HANDLE_FRAG_RES_PENDING:
        // PENDING tesult(received fragment) can be only in the states OPENED and RECEIVING_PACKET but pb_transport_handle_frag() already checked that out
        bRes = WICED_TRUE;
        break;
    case PB_TRANSPORT_HANDLE_FRAG_RES_ACK:
        // ACK can come only in the state SENT_PACKET but pb_transport_handle_frag() already checked that out
        // notify application
        pb_adv_packet_sent(conn_id);
        bRes = WICED_TRUE;
        break;
    case PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED:
    case PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED_OLD_PACKET:
        // SUCCEEDED tesult(received full packet) can be only in the states OPENED and RECEIVING_PACKET but pb_transport_handle_frag() already checked that out
        // all fragments are successfully received and reassembled, the acknowledgment shall be sent
        msg[0] = MESH_PROV_CTRL_CTRL_MSG | (uint8_t)(MESH_PROV_CTRL_CTRL_MSG_ACK << 2);
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0xff, 0, 0, 1, 0, 0);
#endif
        pb_adv_send_(conn_idx, msg, 1);
        // notify application if it is not already received packet
        if (res != PB_TRANSPORT_HANDLE_FRAG_RES_SUCCEEDED_OLD_PACKET)
            pb_transport_received_packet(conn_id, pb_transport_cb[conn_idx]->data, pb_transport_cb[conn_idx]->data_len);
        bRes = WICED_TRUE;
        break;
    }
    // on error with known connection close connection and notify app
    if (!bRes)
    {
        TRACE0(TRACE_INFO, "pb_adv_handle_data_: failed\n");
        // send close packet
        msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_CLOSE << 2);
        msg[1] = WICED_BT_MESH_PROVISION_RESULT_FAILED;
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
        pb_adv_send_(conn_idx, msg, 2);

        // delete that transport control block
        pb_transport_del_conn_(conn_idx);
        // notify application
        pb_adv_link_closed(conn_id, WICED_BT_MESH_PROVISION_RESULT_FAILED);
    }
}

/**
* Static function to split provisioning message onto fragments and send them via ADV.
*
* Parameters:
*   conn_idx:       Index of the transaction controll block in the array pb_transport_cb
*   packet:         Message to split and send
*   packet_len:     Length of the message to split and send
*
* Return: None
*
*/
static void pb_transport_split_message_(uint32_t conn_idx, const uint8_t* packet, uint32_t packet_len)
{
    uint32_t sent_len = 0;
    uint32_t to_send;
    uint8_t buf[32]; //length should be bigger then possible packet lenght: max (ADV_LEN_MAX-8)
    uint8_t hdr_len;
    uint8_t fragment_index = 0;

    // 7 bytes PB_ADV header will be added: <Length>[1byte] || <PB AD Type>[1byte] || <Link ID>[4bytes] || <Transaction Number>[1byte]
    uint8_t max_len = ADV_LEN_MAX - 7;

    TRACE3(TRACE_INFO, "split_message: conn_idx:%d packet_len:%d max_len:%d\n", conn_idx, packet_len, max_len);
    while (packet_len - sent_len > 0)
    {
        if (sent_len == 0)
        {
            // create first fragment: <00, (fragments_number-1)>[1byte],<total_length>[2bytes],<fcs>[1byte],<data>[(1 - (max_len-4))bytes]
            buf[0] = MESH_PROV_CTRL_FRAG_NUM | (uint8_t)((((packet_len + 3) + (max_len - 2)) / (max_len - 1) - 1) << 2);
            UINT16TOBE2(buf + 1, packet_len);
#ifndef MESH_CONTROLLER
#ifndef NO_FCS_CHECK            
            buf[3] = pb_crc8_updateBlock((uint8_t*)packet, packet_len, 0xff);
#else
            buf[3] = 0;
#endif
#else
            buf[3] = 0;
#endif
            hdr_len = 4;
            to_send = (packet_len < (uint32_t)(max_len - 4)) ? packet_len : (max_len - 4);
        }
        else
        {
            // create continuation fragment: <01, fragment_index>[1byte],<data>[(1-max_len)bytes]
            buf[0] = MESH_PROV_CTRL_FRAG_IDX | (fragment_index << 2);
            hdr_len = 1;
            to_send = ((packet_len - sent_len) < (uint32_t)(max_len - 1)) ? (packet_len - sent_len) : (max_len - 1);
        }
        memcpy(&buf[hdr_len], packet + sent_len, to_send);
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0xff, 0, 0, 0, 0, 0);
#endif
        pb_adv_send_(conn_idx, buf, hdr_len + to_send);
        sent_len += to_send;
        fragment_index++;
    }
}

/**
* Static function to create advertising packet and send it through ADV baerer:
*   <Length>[1byte] || <PB AD Type>[1byte] || <Link ID>[4bytes] || <Transaction Number>[1byte] || <data>[>=1bytes]
*
* Parameters:
*   conn_idx:       Index of the transaction controll block in the array pb_transport_cb
*   data:           Data to send: <ctrl-2bits,info-6bits>[1byte] <Data>[>=0bytes]
*   data_len:       Length of the data to send (>=1).
*
* Return:   None
*
*/
static void pb_adv_send_(uint32_t conn_idx, const uint8_t* data, uint8_t data_len)
{
    uint8_t len;
    uint8_t msg[ADV_LEN_MAX];
    len = 1;    //skip length for now
    msg[len++] = ADV_TYPE_PB_ADV_TYPE;
    memcpy(&msg[len], &pb_transport_cb[conn_idx]->link_id, MESH_PB_ADV_LINK_ID_LEN);
    len += MESH_PB_ADV_LINK_ID_LEN;
    // for transport specific packets the transaction number should be 0
    if ((*data & MESH_PROV_CTRL_MASK) == MESH_PROV_CTRL_TRANS_SPEC)
        msg[len] = 0;
    // for ack it should be the same as in last received packet
    else if (*data == (MESH_PROV_CTRL_CTRL_MSG | (uint8_t)(MESH_PROV_CTRL_CTRL_MSG_ACK << 2)))
        msg[len] = pb_transport_cb[conn_idx]->lst_rcv_trans_num;
    // for all other it should be sent transactions counter + 1
    else
        msg[len] = pb_transport_cb[conn_idx]->sent_transactions + 1;
    len++;
    memcpy(&msg[len], data, data_len);
    len += data_len;
    msg[0] = len - 1;
#ifndef MESH_CONTROLLER
    send_adv(msg, len, NULL, NULL);
#endif
}

/**
* Static function to handle received link establishment message, update state, send close message if needed and notify application.
*
* Parameters:
*   p_message:      Received message:
*                   <link_id>[4 bytes],<transaction_num>[1 byte],<ctrl-2bits(0x3),info-6bits>[1byte],<data>[1-67bytes]
*   message_len:    Length of the received message.
*
* Return: None
*
*/
static void pb_adv_handle_link_establ_message_(const uint8_t* p_message, uint32_t message_len)
{
    wiced_bool_t res = WICED_FALSE;
    uint8_t msg[2];
    uint32_t conn_id = *(uint32_t*)p_message;
    // find transport controll block
    int conn_idx = pb_transport_find_conn_(*(uint32_t*)p_message);
    TRACE1(TRACE_DEBUG, "handle_link_establ: conn_id:%x\n", conn_id);
    TRACE1(TRACE_DEBUG, " node_id:%x\n", node_nv_data.node_id);
    TRACE4(TRACE_DEBUG, " idx:%d state:%d sent_trans:%d lst_rcv_trans:%d uuid:\n", conn_idx, conn_idx < 0 ? -1 : pb_transport_cb[conn_idx]->state, conn_idx < 0 ? -1 : pb_transport_cb[conn_idx]->sent_transactions, conn_idx < 0 ? -1 : pb_transport_cb[conn_idx]->lst_rcv_trans_num);
    TRACEN(TRACE_DEBUG, (char*)node_uuid, MESH_DEVICE_UUID_LEN);
    TRACEN(TRACE_DEBUG, (char*)p_message, message_len);
    // The transaction number shell be ignored upon receipt for messages that have the control field set to 11 
    switch (p_message[MESH_PB_ADV_LINK_ID_LEN + 1] >> 2)
    {
    case PB_ADV_INFO_LINK_OPEN:
        // ignore if lenght is wrong
        if (message_len != MESH_PB_ADV_LINK_ID_LEN + 2 + MESH_DEVICE_UUID_LEN)
        {
            break;
        }
        // ignore open requests with wrong device UUID
        if (0 != memcmp(&p_message[MESH_PB_ADV_LINK_ID_LEN + 1 + 1], node_uuid, MESH_DEVICE_UUID_LEN))
        {
            break;
        }
        // ignore it if we are provisioned
        if (node_nv_data.node_id != MESH_NODE_ID_INVALID)
        {
            break;
        }
        // if connection exists already
        if (conn_idx >= 0)
        {
            if (pb_transport_cb_cnt != 1)
            {
                break;
            }
            // if we are in the OPENED state and didn't sent or receive any message
            // then it means sender retransmits OPEN message because he lost our ack
            if (pb_transport_cb[conn_idx]->state == BP_TRANSPORT_STATE_OPENED
                && pb_transport_cb[conn_idx]->sent_transactions == 0x80
                && pb_transport_cb[conn_idx]->lst_rcv_trans_num == 0xff)
            {
                //retransmit link_open_ack message
                msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_OPEN_ACK << 2);
#ifndef MESH_CONTROLLER
                mesh_set_adv_params(0xff, 0, 0, 1, 0, 0);
#endif
                pb_adv_send_(conn_idx, msg, 1);
                res = WICED_TRUE;
            }
            // ignore message in any other state
            break;
        }
        // only one connection is allowed
        if (pb_transport_cb_cnt > 0)
        {
            break;
        }

        // allocate transport controll block
        conn_idx = pb_transport_alloc_conn_(*(uint32_t*)p_message);
        if (conn_idx < 0)
        {
            break;
        }

        pb_transport_cb[conn_idx]->state = BP_TRANSPORT_STATE_OPENED;
        pb_transport_cb[conn_idx]->trans_to = MESH_PB_ADV_TRANSACTION_TO;
        // The transaction number shell be 0 for messages that have the control field set to 11 
        // For other messages of the provisioning device transaction number should start from 0x80.
        // Then we will increment sent_transactions on ACK
        pb_transport_cb[conn_idx]->sent_transactions = 0x80;
        // 0xff means accept any transaction number in received pqackets (it is only for first transaction)
        pb_transport_cb[conn_idx]->lst_rcv_trans_num = 0xff;
        // clear out_data_len as a sign of successfylly sent message to be ready to send message
        pb_transport_cb[conn_idx]->out_data_len = 0;
        //send link_open_ack message
        msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_OPEN_ACK << 2);
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0xff, 0, 0, 1, 0, 0);
#endif
        pb_adv_send_(conn_idx, msg, 1);
        // notify application about opened link and if it failed then delete that transport control block
        if (!pb_adv_link_opened(*(uint32_t*)p_message))
        {
            pb_transport_del_conn_(conn_idx);
            break;
        }
        res = WICED_TRUE;
        break;
    case PB_ADV_INFO_LINK_OPEN_ACK:
        // ignore if lenght is wrong
        if (message_len != MESH_PB_ADV_LINK_ID_LEN + 2)
        {
            break;
        }
        if (conn_idx < 0)
        {
            break;
        }
        // if we are in the wrong state
        if (pb_transport_cb[conn_idx]->state != BP_TRANSPORT_STATE_SENT_OPEN)
        {
            TRACE1(TRACE_INFO, "link_establ: invalid state %d\n", pb_transport_cb[conn_idx]->state);
/*  just ignore unexpected open conformation
            // delete that transport control block
            pb_transport_del_conn_(conn_idx);
            // send close packet
            msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_CLOSE << 2);
            msg[1] = WICED_BT_MESH_PROVISION_RESULT_FAILED;
#ifndef MESH_CONTROLLER
            mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
            pb_adv_send_(conn_idx, msg, 2);
            // notify application
            pb_adv_link_closed(*(uint32_t*)p_message, WICED_BT_MESH_PROVISION_RESULT_FAILED);
*/
            break;
        }
        // clear timeouts
        pb_transport_cb[conn_idx]->ack_to = 0;
        pb_transport_cb[conn_idx]->trans_to = 0;
        // start sending messages from transaction number 1 for provisioner (we will increment sent_transactions on ACK)
        pb_transport_cb[conn_idx]->sent_transactions = 0;
        // 0xff means accept any transaction number in received pqackets (it is only for first transaction)
        pb_transport_cb[conn_idx]->lst_rcv_trans_num = 0xff;
        // Set opened state and notify application
        pb_transport_cb[conn_idx]->state = BP_TRANSPORT_STATE_OPENED;
        // clear out_data_len as a sign of successfylly sent message to be ready to send message
        pb_transport_cb[conn_idx]->out_data_len = 0;
        // notify application about opened link and if it failed then delete that transport control block
        if (!pb_adv_link_opened(pb_transport_cb[conn_idx]->link_id))
        {
            pb_transport_del_conn_(conn_idx);
            break;
        }
        res = WICED_TRUE;
        break;
    case PB_ADV_INFO_LINK_CLOSE:
        // ignore if lenght is wrong
        if (message_len != MESH_PB_ADV_LINK_ID_LEN + 2 + 1)
        {
            break;
        }
        if (conn_idx < 0)
        {
            break;
        }
        // delete that transport control block
        pb_transport_del_conn_(conn_idx);
        // notify application with result code received in packet
        pb_adv_link_closed(*(uint32_t*)p_message, p_message[MESH_PB_ADV_LINK_ID_LEN + 2]);
        res = WICED_TRUE;
        break;
    }
    if (!res)
    {
        TRACE0(TRACE_DEBUG, "link_establ: failed\n");
    }
}

/**
* Called by app to start link establishment for provisioning of the device.
* App should wait for pb_adv_link_opened() or pb_adv_link_closed()
*
* Parameters:
*   conn_id:            Connection ID for provisioning. It should be randomly generated before that call.
*   uuid:               (16 bytes) Node UUID to start provisioning to. 
*
* Return:   WICED_TRUE - succeeded
*           WICED_FALSE - failed
*
*/
wiced_bool_t pb_adv_link_open(
    uint32_t                            conn_id,
    const uint8_t                       *uuid)
{
    int conn_idx;
    uint8_t buf[1 + MESH_DEVICE_UUID_LEN];

    conn_idx = pb_transport_find_conn_(conn_id);
    if (conn_idx >= 0)
    {
        TRACE0(TRACE_INFO, "pb_adv_link_open: already exists\n");
        return WICED_FALSE;
    }

    conn_idx = pb_transport_alloc_conn_(conn_id);
    if (conn_idx < 0)
    {
        return WICED_FALSE;
    }
    // use out_data to save uuid - we will need it just to retransmit LINK_OPEN packet
    memcpy(pb_transport_cb[conn_idx]->out_data, uuid, MESH_DEVICE_UUID_LEN);
    pb_transport_cb[conn_idx]->out_data_len = MESH_DEVICE_UUID_LEN;
    // set state and timeout fields
    pb_transport_cb[conn_idx]->state = BP_TRANSPORT_STATE_SENT_OPEN;
    pb_transport_cb[conn_idx]->ack_to = MESH_PB_ADV_OPEN_ACK_TO;
    pb_transport_cb[conn_idx]->trans_to = MESH_PB_ADV_OPEN_TO;
    // start sending nessages from transaction number 1 (we will increment sent_transactions on ACK)
    pb_transport_cb[conn_idx]->sent_transactions = 0;
    // 0xff means accept any transaction number in received pqackets (it is only for first transaction)
    pb_transport_cb[conn_idx]->lst_rcv_trans_num = 0xff;
    //send link_open message
    buf[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_MSG_TYPE_LINK_OPEN << 2);
    memcpy(&buf[1], uuid, MESH_DEVICE_UUID_LEN);
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
    pb_adv_send_(conn_idx, buf, 1 + MESH_DEVICE_UUID_LEN);
    return WICED_TRUE;
}

/**
* Called by app to close link. It can be called at any time.
*
* Parameters:
*   conn_id:    (4 bytes) Connection ID for provisioning
*   reason:     Reason of link closing. Can be any of WICED_BT_MESH_PROVISION_RESULT_XXX
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - error no such connection
*/
wiced_bool_t pb_adv_link_close(uint32_t conn_id, uint8_t reason)
{
    uint8_t msg[2];
    int conn_idx;
    conn_idx = pb_transport_find_conn_(conn_id);
    if (conn_idx < 0)
        return WICED_FALSE;
    // send close packet
    msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_CLOSE << 2);
    msg[1] = reason;
#ifndef MESH_CONTROLLER
    mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
    pb_adv_send_(conn_idx, msg, 2);
    // delete that transport control block
    pb_transport_del_conn_(conn_idx);
    return WICED_TRUE;
}

/**
* Called by app to send provisioning packet on specific link. It can be called at any time after link establishment
* but not previous pb_adv_send_packet() and pb_adv_packet_sent().
*
* Parameters:
*   conn_id:    (4 bytes) Connection ID for provisioning
*   packet:     Packet to send
*   packet_len: Length of the packet to send
*
* Return:   WICED_TRUE - success
*           WICED_FALSE - error no such connection
*
*/
wiced_bool_t pb_adv_send_packet(uint32_t conn_id, const uint8_t* packet, uint16_t packet_len)
{
    int conn_idx;
    conn_idx = pb_transport_find_conn_(conn_id);
    TRACE1(TRACE_INFO, "pb_adv_send_packet: id:%x\n", conn_id);
    TRACE3(TRACE_INFO, " idx:%d state:%d out_data_len:%d\n", conn_idx, conn_idx < 0 ? -1 : pb_transport_cb[conn_idx]->state, conn_idx < 0 ? -1 : pb_transport_cb[conn_idx]->out_data_len);
    if (conn_idx < 0)
        return WICED_FALSE;
    // we can't send packet if we didn't receive ack on previous one
    if (pb_transport_cb[conn_idx]->out_data_len > 0
        || pb_transport_cb[conn_idx]->state == BP_TRANSPORT_STATE_SENT_OPEN)
        return WICED_FALSE;
    // set timeout fields
    pb_transport_cb[conn_idx]->ack_to = MESH_PB_ADV_TRANSACTION_ACK_TO;
    pb_transport_cb[conn_idx]->trans_to = MESH_PB_ADV_TRANSACTION_TO;
    // remember packet for the case if we will need to retransmit it
    memcpy(pb_transport_cb[conn_idx]->out_data, packet, packet_len);
    pb_transport_cb[conn_idx]->out_data_len = packet_len;
    pb_transport_split_message_(conn_idx, packet, packet_len);
    return WICED_TRUE;
}

/**
* Processes received PB_ADV packets
*
* Parameters:
*   packet:     Packet to process.
*               packet format: <link_id>[4 bytes],<transaction_num>[1 byte],<ctrl-2bits,info-6bits>[1byte],<data>[1-67bytes]
*   packet_len: Length of the packet to process
*
* Return:   None
*
*/
void pb_adv_handle_packet(uint8_t *packet, uint8_t packet_len)
{
    int conn_idx = -1;
    if (packet_len > MESH_PB_ADV_LINK_ID_LEN + 1)
    {
        // if it is PB_ADV Link Establishment 
        if ((packet[MESH_PB_ADV_LINK_ID_LEN + 1] & MESH_PROV_CTRL_MASK) == MESH_PROV_CTRL_TRANS_SPEC)
        {
            pb_adv_handle_link_establ_message_(packet, packet_len);
        }
        // ignore if received link id doesn't match any opened link id
        else if ((conn_idx = pb_transport_find_conn_(*(uint32_t*)packet)) >= 0)
        {
            pb_adv_handle_data_(conn_idx, packet[MESH_PB_ADV_LINK_ID_LEN], &packet[MESH_PB_ADV_LINK_ID_LEN + 1], packet_len - MESH_PB_ADV_LINK_ID_LEN - 1);
        }
    }
}

/**
* Called by app to initialize provisioning transport layer.
*
* Parameters:   None
*
* Return:   None
*
*/
void pb_adv_init(void)
{
    TRACE1(TRACE_INFO, "pb_transport_init: pb_transport_cb_cnt:%d\n", pb_transport_cb_cnt);

    // delete all allocated control blocks
    while (pb_transport_cb_cnt > 0)
    {
#ifdef _DEB_USE_STATIC_ALLOC_PROV_TRANSP
        if (pb_transport_cb_cnt == 1)
            pb_transport_cb_cnt--;
        else
#endif
        wiced_memory_free(pb_transport_cb[--pb_transport_cb_cnt]);
    }
}

/**
* Checks transport timeouts and makes needed actions.
* It must be called by application once in each second
*
* Parameters:   None
*
* Return: None
*
*/
void pb_transport_timer(void)
{
    uint8_t msg[1 + MESH_DEVICE_UUID_LEN];
    uint32_t conn_idx, conn_idx_to_close;
    //TRACE1(TRACE_INFO, "pb_transport_timer: cnt:%d\n", pb_transport_cb_cnt);
    // just return if no activity
    if (pb_transport_cb_cnt == 0)
        return;
    //at first decrement all non-0 timeouts and remember failed connection to close
    conn_idx_to_close = (uint32_t)-1;
    for (conn_idx = 0; conn_idx < pb_transport_cb_cnt; conn_idx++)
    {
        // decrement remaining transaction time and remember timed out connection to close
        if (pb_transport_cb[conn_idx]->trans_to != 0 && --pb_transport_cb[conn_idx]->trans_to == 0)
            conn_idx_to_close = conn_idx;
        // for timed out acknowledges
        else if (pb_transport_cb[conn_idx]->ack_to != 0 && --pb_transport_cb[conn_idx]->ack_to == 0)
        {
            TRACE3(TRACE_INFO, "pb_transport_timer: ack_to: conn_idx:%d state:%d out_data_len:%d\n", conn_idx, pb_transport_cb[conn_idx]->state, pb_transport_cb[conn_idx]->out_data_len);
            if (pb_transport_cb[conn_idx]->state == BP_TRANSPORT_STATE_SENT_OPEN)
            {
                // set ack timeout
                pb_transport_cb[conn_idx]->ack_to = MESH_PB_ADV_OPEN_ACK_TO;
                //retransmit link_open message
                msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_MSG_TYPE_LINK_OPEN << 2);
                memcpy(&msg[1], pb_transport_cb[conn_idx]->out_data, MESH_DEVICE_UUID_LEN);
#ifndef MESH_CONTROLLER
                mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
                pb_adv_send_(conn_idx, msg, 1 + MESH_DEVICE_UUID_LEN);
            }
            else if (pb_transport_cb[conn_idx]->out_data_len != 0)
            {
                //retransmit message
                // set ack timeout
                pb_transport_cb[conn_idx]->ack_to = MESH_PB_ADV_TRANSACTION_ACK_TO;
                pb_transport_split_message_(conn_idx, pb_transport_cb[conn_idx]->out_data, pb_transport_cb[conn_idx]->out_data_len);
            }
        }
    }

    // transaction timeout works in any state
    if (conn_idx_to_close < pb_transport_cb_cnt)
    {
        uint32_t link_id;
        TRACE2(TRACE_INFO, "pb_transport_timer: trans to: conn_idx:%d state:%d\n", conn_idx_to_close, pb_transport_cb[conn_idx_to_close]->state);
        // send close packet
        msg[0] = MESH_PROV_CTRL_TRANS_SPEC | (PB_ADV_INFO_LINK_CLOSE << 2);
        msg[1] = WICED_BT_MESH_PROVISION_RESULT_TIMEOUT;
#ifndef MESH_CONTROLLER
        mesh_set_adv_params(0xff, 0, 0, 2, 0, 0);
#endif
        pb_adv_send_(conn_idx_to_close, msg, 2);
        // remember link_id to delete transaction control block before notifying app with link_id
        link_id = pb_transport_cb[conn_idx_to_close]->link_id;
        // delete that transport control block
        pb_transport_del_conn_(conn_idx_to_close);
        // notify application
        pb_adv_link_closed(link_id, WICED_BT_MESH_PROVISION_RESULT_TIMEOUT);
    }
}

