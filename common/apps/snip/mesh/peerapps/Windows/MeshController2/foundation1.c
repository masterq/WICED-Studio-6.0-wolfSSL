/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * Mesh Foundation Model implementation
 *
*/
#include <string.h>
#include <stdio.h>
#include "platform.h"

#include "mesh_core.h"
#include "mesh_util.h"
#include "foundation_int.h"
#include "foundation.h"
#include "access_layer.h"
#include "core_aes_ccm.h"
#include "core_ovl.h"
#include "mesh_util.h"
#include "key_refresh.h"
#include "wiced_timer.h"
#include "low_power.h"

#undef FID  // to get rid of redefinition in include in overlay
#undef TRACE_COMPILE_LEVEL

#define FID                 FID_MESH_APP__FOUNDATION1_C
#define TRACE_COMPILE_LEVEL TRACE_COMPILE_LEVEL__FOUNDATION1_C
#include "mesh_trace.h"

#ifndef MESH_CONTROLLER
static wiced_timer_t    reset_timer;
#endif

// Static functions forward definitions
static void handle_config_publication_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id);
static void handle_config_publication_set_(uint16_t addr, uint16_t src_id, const uint8_t *params, uint8_t params_len);
static uint8_t find_virt_addr_(const uint8_t *virt_addr_lable_uuid);
static void handle_config_subs_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id);
static void handle_config_subs_add_(uint16_t subs_addr, uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_config_subs_del_(uint16_t opcode, uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_config_subs_overwr_(uint16_t opcode, uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_config_subs_del_all_(uint16_t src_id, const uint8_t *params, uint8_t params_len);

// Adds net key.
static void handle_provis_netkey_add_(uint16_t global_idx, const uint8_t *net_key, uint16_t src_id);
// Updates net key. If net_key == NULL then deletes it.
static void handle_provis_netkey_update_(uint16_t global_idx, const uint8_t *net_key, uint16_t src_id);
// Adds app key.
static void handle_provis_appkey_add_(uint16_t netkey_global_idx, uint16_t appkey_global_idx, const uint8_t *app_key, uint16_t src_id);
// Updates app key. If app_key == NULL then deletes it.
static void handle_provis_appkey_update_(uint16_t netkey_global_idx, uint16_t appkey_global_idx, const uint8_t *app_key, uint16_t src_id);
// Returns the list of the app keys of the specific net key
static void handle_provis_appkey_get_(uint16_t netkey_global_idx, uint16_t src_id);
// Returns the list of the net keys
static void handle_provis_netkey_get_(uint16_t src_id);
//// Requests discoverabele state
//static void handle_provis_discoverabele_get_(uint16_t src_id);
static void handle_provis_model_app_bind_(uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_provis_model_app_unbind_(uint16_t src_id, const uint8_t *params, uint8_t params_len);
static void handle_provis_node_reset_(uint16_t src_id);
static void handle_provis_model_app_get_(uint16_t src_id, const uint8_t *params, uint8_t params_len);

// Represents Configuration persistent in the NVM
tFND_NVM_CONFIG         *fnd_config_ = NULL;
uint16_t                fnd_config_len_ = 0;

tFND_NVM_VIRT_ADDR_CB   *fnd_virt_addr_ = NULL;

// Represents non-persistent Configuration
tFND_CONFIG             fnd_static_config_ = { 0 };

// Returns status (one of the FND_STATUS_XXX) to send in the response
static uint8_t foundation_find_model_(uint16_t element_addr, uint16_t company_id, uint16_t model_id, tFND_NVM_MODEL **p_model, tFND_NVM_ELEMENT **p_elem)
{
    uint32_t i;
    tFND_NVM_ELEMENT *p_nvm_elem;
    // element index
    i = element_addr - node_nv_data.node_id;
    // return error if no such element
    if (i >= fnd_static_config_.elements_num)
    {
        TRACE2(TRACE_INFO, "foundation_find_model_: invalid address i:%d elements_num:%d\n", i, fnd_static_config_.elements_num);
        return FND_STATUS_INVALID_ADDRESS;
    }
    // first element
    p_nvm_elem = fnd_config_->elements;
    // go through all elements
    while (i-- > 0)
    {
        // go to the next element
        p_nvm_elem = (tFND_NVM_ELEMENT*)((uint8_t*)&p_nvm_elem->models[0] + p_nvm_elem->models_num * sizeof(tFND_NVM_MODEL));
    }
    // go through all models of the found element
    for (i = 0; i < p_nvm_elem->models_num; i++)
    {
        // exit loop if we found the model
        if (p_nvm_elem->models[i].company_id == company_id
            && p_nvm_elem->models[i].model_id == model_id)
            break;
    }
    // return error if we haven't found the model
    if (i >= p_nvm_elem->models_num)
    {
        TRACE2(TRACE_INFO, "foundation_find_model_: invalid model i:%d models_num:%d\n", i, p_nvm_elem->models_num);
        return FND_STATUS_INVALID_MODEL;
    }
    if (p_elem)
        *p_elem = p_nvm_elem;
    if (p_model)
        *p_model = &p_nvm_elem->models[i];

    return FND_STATUS_SUCCESS;
}

#ifndef MESH_CONTROLLER
// saves configuration in the NVRAM
wiced_bool_t foundation_write_config_(void)
{
    wiced_bool_t    res = WICED_FALSE;
    if(fnd_config_len_ == mesh_write_node_info2(MESH_NVM_IDX_CFG_DATA, (uint8_t*)fnd_config_, fnd_config_len_, NULL))
        res = WICED_TRUE;
    return res;
}
#endif

/**
* Process data packet addressed to this mesh node
*
* Parameters:
*   src_id:         Address of the message source.
*   ttl:            TTL of the received packet
*   opcode:         opcode of the payload with bits MESH_APP_PAYLOAD_OP_LONG and MESH_APP_PAYLOAD_OP_MANUF_SPECIFIC
*   params:         parameters of the app payload
*   params_len:     length of the parameters of the app payload
*
* Return:   WICED_TRUE - message handled; WICED_FALSE - try other handle.
*
*/
wiced_bool_t foundation_handle_1_(uint16_t src_id, uint8_t ttl, uint16_t opcode, const uint8_t *params, uint16_t params_len)
{
    wiced_bool_t    ret = WICED_TRUE;
    uint8_t   idx;
    TRACE2(TRACE_INFO, "foundation_handle_1: ttl=%x src_id=%x\n", ttl, src_id);
    TRACE1(TRACE_INFO, " opcode=%x ***************************\n", opcode);
    if (params_len)
        TRACEN(TRACE_INFO, (char*)params, params_len);
    switch (opcode)
    {
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_GET:
        if (params_len != 4 && params_len != 6)
            break;
        handle_config_publication_get_(params, params_len, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_SET:
        if (params_len != 11 && params_len != 13)
            break;
        handle_config_publication_set_(LE2TOUINT16(&params[2]), src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_VIRT_ADDR_SET:
        if (params_len != 25 && params_len != 27)
            break;
        // if we already know that virtual publish address
        if (0xff != (idx = find_virt_addr_(params + 2)))
            handle_config_publication_set_(fnd_virt_addr_->addr[idx].short_val, src_id, params, (uint8_t)params_len);
        // we need to calculate virt address (mesh_calc_virt_addr) and then handle command using calculated virt address
        else
        {
            // if both mesh_calc_virt_addr() and handle_config_publication_set_() are in the overlays
            uint16_t  addr;
            mesh_calc_virt_addr(&params[2], &addr);
            handle_config_publication_set_(addr, src_id, params, (uint8_t)params_len);
        }
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_GET:
        if (params_len != 4)
            break;
        handle_config_subs_get_(params, params_len, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_GET:
        if (params_len != 6)
            break;
        handle_config_subs_get_(params, params_len, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_ADD:
        if (params_len != 6 && params_len != 8)
            break;
        handle_config_subs_add_(LE2TOUINT16(&params[2]), src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_ADD:
        if (params_len != 20 && params_len != 22)
            break;
        // if we already know that subs virtual address
        if (0xff != (idx = find_virt_addr_(params + 2)))
            handle_config_subs_add_(fnd_virt_addr_->addr[idx].short_val, src_id, params, (uint8_t)params_len);
        // we need to calculate virt address (mesh_calc_virt_addr) and then handle command using calculated virt address
        else
        {
            // if both mesh_calc_virt_addr() and handle_config_subs_add_() are in the overlays
            uint16_t  addr;
            mesh_calc_virt_addr(&params[2], &addr);
            handle_config_subs_add_(addr, src_id, params, (uint8_t)params_len);
        }
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE:
        if (params_len != 6 && params_len != 8)
            break;
        handle_config_subs_del_(opcode, src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE:
        if (params_len != 6 && params_len != 8)
            break;
        handle_config_subs_overwr_(opcode, src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_DELETE:
        if (params_len != 20 && params_len != 22)
            break;
        handle_config_subs_del_(opcode, src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_VIRT_ADDR_OVERWRITE:
        if (params_len != 20 && params_len != 22)
            break;
        handle_config_subs_overwr_(opcode, src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL:
        if (params_len != 4 && params_len != 6)
            break;
        handle_config_subs_del_all_(src_id, params, (uint8_t)params_len);
        break;

    case WICED_BT_MESH_CORE_CMD_NODE_RESET:
        if (params_len != 0)
            break;
        handle_provis_node_reset_(src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_APPKEY_ADD:
        if (params_len != 3 + MESH_KEY_LEN)
            break;
        handle_provis_appkey_add_(LE2TOUINT12(params), LE2TOUINT12_2(params + 1), params + 3, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_APPKEY_DELETE:
        if (params_len != 3)
            break;
        handle_provis_appkey_update_(LE2TOUINT12(params), LE2TOUINT12_2(params + 1), NULL, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_APPKEY_UPDATE:
        if (params_len != 3 + MESH_KEY_LEN)
            break;
        handle_provis_appkey_update_(LE2TOUINT12(params), LE2TOUINT12_2(params + 1), params + 3, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_APPKEY_GET:
        if (params_len != 2)
            break;
        handle_provis_appkey_get_(LE2TOUINT16(params), src_id);
        break;

    case WICED_BT_MESH_CORE_CMD_NETKEY_GET:
        if (params_len != 0)
            break;
        handle_provis_netkey_get_(src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_NETKEY_ADD:
        if (params_len != 2 + MESH_KEY_LEN)
            break;
        handle_provis_netkey_add_(LE2TOUINT16(params), params + 2, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_NETKEY_DELETE:
        if (params_len != 2)
            break;
        handle_provis_netkey_update_(LE2TOUINT16(params), NULL, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_NETKEY_UPDATE:
        if (params_len != 2 + MESH_KEY_LEN)
            break;
        handle_provis_netkey_update_(LE2TOUINT16(params), params + 2, src_id);
        break;
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_BIND:
        if (params_len != 6 && params_len != 8)
            break;
        handle_provis_model_app_bind_(src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_MODEL_APP_UNBIND:
        if (params_len != 6 && params_len != 8)
            break;
        handle_provis_model_app_unbind_(src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_GET:
        if (params_len != 4)
            break;
        handle_provis_model_app_get_(src_id, params, (uint8_t)params_len);
        break;
    case WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_GET:
        if (params_len != 6)
            break;
        handle_provis_model_app_get_(src_id, params, (uint8_t)params_len);
        break;
    default:
        ret = WICED_FALSE;
        break;
    }
    return ret;
}

uint8_t* op2be(uint8_t *p, uint16_t op)
{
    if (op <= 0xff)
        *p++ = (uint8_t)op;
    else
    {
        UINT16TOBE2(p, op);
        p += 2;
    }
    return p;
}

// Creates and sends the response message: Config Model Publication Status:
// status(1byte),PublishAddr(2bytes),AppKeyIndex(2bytes),DefaultTTL(1byte)
static void send_config_publication_status_(uint8_t status, uint16_t elem_addr, uint16_t addr, uint16_t publish_app_key_idx, uint8_t publish_ttl, uint8_t period,
    wiced_bool_t credential_flag, uint8_t retransmit, uint16_t company_id, uint16_t model_id, uint16_t src_id)
{
    uint8_t               msg[32];
    uint8_t               *p_msg;

    p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_PUBLICATION_STATUS);
    *p_msg++ = status;
    UINT16TOLE2(p_msg, elem_addr);
    p_msg += 2;
    UINT16TOLE2(p_msg, addr);
    p_msg += 2;
    UINT16TOLE2(p_msg, publish_app_key_idx | (credential_flag ? 0x1000 : 0));
    p_msg += 2;
    *p_msg++ = publish_ttl;
    *p_msg++ = period;
    *p_msg++ = retransmit;
    if (company_id)
    {
        UINT16TOLE2(p_msg, company_id);
        p_msg += 2;
    }
    UINT16TOLE2(p_msg, model_id);
    p_msg += 2;

    // use dev key (0xff)
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(&msg[0], (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

static const uint8_t* parse_elem_addr_(const uint8_t *p_msg_elem_addr, uint16_t *p_elem_addr, uint8_t *p_status)
{
    // element address
    *p_elem_addr = LE2TOUINT16(p_msg_elem_addr);
    p_msg_elem_addr += 2;
    // make sure it is unicast
    if ((*p_elem_addr & MESH_NON_UNICAST_MASK) != 0 || *p_elem_addr == MESH_NODE_ID_INVALID)
    {
        // change status if it is success yet
        if (*p_status == FND_STATUS_SUCCESS)
            *p_status = FND_STATUS_INVALID_ADDRESS;
    }
    return p_msg_elem_addr;
}

static tFND_NVM_MODEL *parse_model_(uint16_t elem_addr, wiced_bool_t is_manuf_model, const uint8_t *p_msg_model, uint16_t *p_company_id, uint16_t *p_model_id, uint8_t *p_status)
{
    tFND_NVM_MODEL *p_model;
    if (is_manuf_model)
    {
        *p_company_id = LE2TOUINT16(p_msg_model);
        p_msg_model += 2;
    }
    else
        *p_company_id = 0;
    *p_model_id = LE2TOUINT16(p_msg_model);

    // if we are not failed already then find element and model
    if (*p_status == FND_STATUS_SUCCESS)
        *p_status = foundation_find_model_(elem_addr, *p_company_id, *p_model_id, &p_model, NULL);
    return p_model;
}

static void handle_config_publication_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id)
{
    tFND_NVM_MODEL     *p_model;
    uint8_t               status = FND_STATUS_SUCCESS;
    uint16_t              elem_addr, company_id = 0, model_id = 0;

    // parse and check element address
    params = parse_elem_addr_(params, &elem_addr, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 6, params, &company_id, &model_id, &status);

    if (status != FND_STATUS_SUCCESS)
        send_config_publication_status_(status, elem_addr, MESH_NODE_ID_INVALID, 0, 0, 0, WICED_FALSE, 0, company_id, model_id, src_id);
    else
    {
        uint16_t publish_address = p_model->publish_address;
        if (!foundation_find_appkey_(p_model->publish_app_key_idx, NULL, NULL, NULL))
            publish_address = MESH_NODE_ID_INVALID;
        else if (!foundation_find_model_app_(p_model, p_model->publish_app_key_idx, NULL))
            publish_address = MESH_NODE_ID_INVALID;

        send_config_publication_status_(status, elem_addr, publish_address, p_model->publish_app_key_idx, p_model->publish_ttl,
            p_model->publish_period, p_model->credential_flag, p_model->retransmit, company_id, model_id, src_id);
    }
}

// finds virtual address in the persistent data and returns its label UUID.
// On error returns NULL
uint8_t* foundation_find_virt_addr(uint16_t virt_addr)
{
    uint8_t idx;
    for (idx = 0; idx < fnd_virt_addr_->num; idx++)
    {

        if(fnd_virt_addr_->addr[idx].short_val == virt_addr)
            break;
    }
    return idx < fnd_virt_addr_->num ? fnd_virt_addr_->addr[idx].long_val : NULL;
}

// finds virtual address in the persistent data and returns its index.
// On error returns 0xff
static uint8_t find_virt_addr_(const uint8_t *virt_addr_lable_uuid)
{
    uint8_t idx;
    for (idx = 0; idx < fnd_virt_addr_->num; idx++)
    {
        if (0 == memcmp(fnd_virt_addr_->addr[idx].long_val, virt_addr_lable_uuid, 16))
            break;
    }
    if (idx >= fnd_virt_addr_->num)
        idx = 0xff;
    return idx;
}

// adds virtual address to the persistent data and returns its index.
// On error returns 0xff
static uint8_t add_virt_addr_(uint16_t short_val, const uint8_t *long_val)
{
    if (fnd_virt_addr_->num >= FND_MAX_VIRT_ADDR_NUM)
    {
        TRACE1(TRACE_DEBUG, "add_virt_addr_: too many virt addresses %d. \n", fnd_virt_addr_->num);
        return 0xff;
    }
    fnd_virt_addr_->addr[fnd_virt_addr_->num].short_val = short_val;
    memcpy(fnd_virt_addr_->addr[fnd_virt_addr_->num].long_val, long_val, 16);
    fnd_virt_addr_->num++;

#ifndef MESH_CONTROLLER
    // save the list of the virtual addresses
    if (sizeof(tFND_NVM_VIRT_ADDR_CB) != mesh_write_node_info(MESH_NVM_IDX_VIRT_ADDR, (uint8_t*)fnd_virt_addr_, sizeof(tFND_NVM_VIRT_ADDR_CB), NULL))
    {
        TRACE1(TRACE_INFO, "add_virt_addr_: write_node_info(%d) failed\n", sizeof(tFND_NVM_VIRT_ADDR_CB));
        fnd_virt_addr_->num--;
        return 0xff;
    }
#endif

    // return index of just added virt addr
    return fnd_virt_addr_->num - 1;
}

// adds virtual address to the persistent data and returns its value.
// On error returns 0
uint16_t foundation_add_virt_addr(const uint8_t *label_uuid)
{
    uint16_t    addr;
    uint8_t     virt_addr_idx;
    
    virt_addr_idx = find_virt_addr_(label_uuid);
    if (virt_addr_idx != 0xff)
        addr = fnd_virt_addr_->addr[virt_addr_idx].short_val;
    else
    {
        mesh_calc_virt_addr(label_uuid, &addr);
        if (0xff == add_virt_addr_(addr, label_uuid))
            addr = MESH_NODE_ID_INVALID;
    }
    return addr;
}

/* Parses and checks element address and model/subs address fields of the message of the configuration Model
* Parameters:
*   is_virt_uuid:       WICED_TRUE means it is virtual label UUID 16 bytes length. WICED_FALSE - 2 bytes address
*   addr:               address from the message or virtual address calculated from 16 bytes virtual UUID pointed by p_addr
*   p_msg_addr:         pointer on 2 or 16 bytes address in the message
*   p_status:           points on status variable - can be modified by this function
*
* Return:   pointer on the message after addresses
*/
static const uint8_t* parse_addr_(wiced_bool_t is_virt_uuid, uint16_t addr, const uint8_t *p_msg_addr, uint8_t *p_status)
{
    // if it is not virtual address
    if (!is_virt_uuid)
    {
        // make sure it is valid address (not invalid and not virtual)
        if (addr == MESH_NODE_ID_INVALID ||
            (addr & MESH_GROUP_MASK) == MESH_VIRT_ADDR)
        {
            // change status if it is success yet
            if (*p_status == FND_STATUS_SUCCESS)
                *p_status = FND_STATUS_INVALID_ADDRESS;
        }
        p_msg_addr += 2;
    }
    // If it is virtual address.
    else
    {
        // if we are not failed yet then find or add virt address
        if (*p_status == FND_STATUS_SUCCESS)
        {
            // If we don't know that virtual address then it just has been calculated and has to be added into the persistent array
            if (0xff == find_virt_addr_(p_msg_addr))
            {
                // invalid address means we don't want to add it
                if (addr == MESH_NODE_ID_INVALID)
                    *p_status = FND_STATUS_INVALID_ADDRESS;
                else if (0xff == add_virt_addr_(addr, p_msg_addr))
                {
                    *p_status = FND_STATUS_STORAGE_FAILURE;
                }
            }
        }
        p_msg_addr += 16;
    }
    return p_msg_addr;
}

// params:=ElementAddr(2bytes),PublishAddr(2 or 16 bytes),AppKeyIdx(2bytes),PublishTTL(1byte),PublishPeriod(1byte),ModelID(2 or 4 bytes)
static void handle_config_publication_set_(uint16_t addr, uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL      *p_model;
    uint16_t            publish_app_key_idx;
    uint8_t             publish_ttl, period, retransmit;
    wiced_bool_t        credential_flag;
    uint16_t              elem_addr, model_id, company_id;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    // for non-virtual address we allow invalid addr to remove publication
    if (params_len < 24 && addr == MESH_NODE_ID_INVALID)
        p += 2;
    // handle publication address(check if it is valid and save it in the NVM if it is virtual)
    else
        p = parse_addr_(params_len >= 24, addr, p, &status);

    publish_app_key_idx = LE2TOUINT16(p);
    p += 2;
    credential_flag = (publish_app_key_idx & 0x1000) != 0 ? WICED_TRUE : WICED_FALSE;
    publish_app_key_idx &= 0x0fff;

    publish_ttl = *p++;
    period = *p++;
    retransmit = *p++;

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 27 || params_len == 13, p, &company_id, &model_id, &status);

    if (status == FND_STATUS_SUCCESS)
    {
        if (!foundation_find_appkey_(publish_app_key_idx, NULL, NULL, NULL))
            status = FND_STATUS_INVALID_APPKEY;
    }

    // if we are not failed already then then update found model
    if (status == FND_STATUS_SUCCESS)
    {
        if (addr == MESH_NODE_ID_INVALID)
        {
            publish_app_key_idx = 0;
            credential_flag = WICED_FALSE;
            publish_ttl = 0;
            period = 0;
            retransmit = 0;
        }
        p_model->publish_address = addr;
        p_model->publish_app_key_idx = publish_app_key_idx;
        p_model->credential_flag = credential_flag;
        p_model->publish_ttl = publish_ttl;
        p_model->publish_period = period;
        p_model->retransmit = retransmit;
#ifndef MESH_CONTROLLER
        // save it in the NVRAM
        foundation_write_config_();
#endif
    }
    else
    {
        addr = MESH_NODE_ID_INVALID;
        publish_app_key_idx = 0;
        credential_flag = WICED_FALSE;
        publish_ttl = 0;
        period = 0;
        retransmit = 0;
    }

    // send response
    send_config_publication_status_(status, elem_addr, addr, publish_app_key_idx, publish_ttl, period, credential_flag, retransmit, company_id, model_id, src_id);
}

// sends response message: Config Model Subscription Status
// status(1byte),ElementAddr(2bytes),SubscriptionAddr(2bytes),ModelId(2 or 4 bytes)
static void send_config_subs_status_(uint16_t op, uint8_t status, uint16_t elem_addr, uint16_t addr, uint16_t company_id, uint16_t model_id, uint16_t src_id)
{
    uint8_t   msg[32];
    uint8_t   *p_msg;

    p_msg = op2be(&msg[0], op);
    *p_msg++ = status;
    UINT16TOLE2(p_msg, elem_addr);
    p_msg += 2;
    UINT16TOLE2(p_msg, addr);
    p_msg += 2;
    if (company_id)
    {
        UINT16TOLE2(p_msg, company_id);
        p_msg += 2;
    }
    UINT16TOLE2(p_msg, model_id);
    p_msg += 2;

    // use dev key (0xff)
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(msg, (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

// sends response message: Config Model Subscription List
// status(1byte),ElementAddr(2bytes),SubscriptionAddr(2bytes),ModelId(2 or 4 bytes),Addresses(2*N)
static void send_config_subs_list_(uint8_t status, uint16_t elem_addr, uint16_t company_id, uint16_t model_id, tFND_NVM_MODEL *p_model, uint16_t src_id)
{
    uint8_t     msg[11 + FND_MODEL_MAX_SUBS_NUM * 2];
    uint8_t   *p_msg;

    if (company_id == 0)
        p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_CONFIG_SIG_MODEL_SUBSCRIPTION_LIST);
    else
        p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_CONFIG_VENDOR_MODEL_SUBSCRIPTION_LIST);
    *p_msg++ = status;
    UINT16TOLE2(p_msg, elem_addr);
    p_msg += 2;
    if (company_id)
    {
        UINT16TOLE2(p_msg, company_id);
        p_msg += 2;
    }
    UINT16TOLE2(p_msg, model_id);
    p_msg += 2;

    if (status == FND_STATUS_SUCCESS)
    {
        uint8_t idx;
        for (idx = 0; idx < p_model->subs_num; idx++)
        {
            UINT16TOLE2(p_msg, p_model->subs_list[idx]);
            p_msg += 2;
        }
    }
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(msg, (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

// params:=ElementAddr(2bytes),ModelID(2 or 4 bytes)
static void handle_config_subs_get_(const uint8_t *params, uint16_t params_len, uint16_t src_id)
{
    tFND_NVM_MODEL     *p_model;
    uint8_t               status = FND_STATUS_SUCCESS;
    uint16_t              elem_addr, company_id = 0, model_id = 0;

    // parse and check element address
    params = parse_elem_addr_(params, &elem_addr, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 6, params, &company_id, &model_id, &status);

    send_config_subs_list_(status, elem_addr, company_id, model_id, p_model, src_id);
}

#ifdef HARDCODED_PROVIS
uint8_t mesh_core_subs_add_(uint8_t element, uint16_t company_id, uint16_t model_id, uint16_t addr)
{
    uint8_t     status = FND_STATUS_SUCCESS;
    tFND_NVM_MODEL *p_model;
    status = foundation_find_model_(node_nv_data.node_id + element, company_id, model_id, &p_model, NULL);
    // if we are not failed already then then proceed
    if (status == FND_STATUS_SUCCESS)
    {
        // if that address is there already then just return success. Otherwise try to add that address
        foundation_find_subs_(p_model, addr, &status);
        if (status != FND_STATUS_SUCCESS)
        {
            if (p_model->subs_num >= FND_MODEL_MAX_SUBS_NUM)
                status = FND_STATUS_INSUFFICIENT_RESOURCES;
            else
            {
                status = FND_STATUS_SUCCESS;
                // update found model
                p_model->subs_list[p_model->subs_num++] = addr;
#ifndef MESH_CONTROLLER
                // save configuration in the NVRAM
                foundation_write_config_();
#endif
            }
        }
    }
    return status;
}
#endif

// params:=ElementAddr(2bytes),PublishAddr(2 or 16 bytes),ModelID(2 or 4 bytes)
// sends response message: Config Model Subscription Status
static void handle_config_subs_add_(uint16_t addr, uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    // handle subs address(check if it is valid and save it in the NVM if it is virtual)
    p = parse_addr_(params_len >= 20, addr, p, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 22 || params_len == 8, p, &company_id, &model_id, &status);

    // if we are not failed already then then update found model
    if (status == FND_STATUS_SUCCESS)
    {
        // if that address is there already then just return success. Otherwise try to add that address
        foundation_find_subs_(p_model, addr, &status);
        if (status != FND_STATUS_SUCCESS)
        {
            if (p_model->subs_num >= FND_MODEL_MAX_SUBS_NUM)
                status = FND_STATUS_INSUFFICIENT_RESOURCES;
            else
            {
                status = FND_STATUS_SUCCESS;
                // update found model
                p_model->subs_list[p_model->subs_num++] = addr;
#ifndef MESH_CONTROLLER
                // save configuration in the NVRAM
                foundation_write_config_();
#endif
            }
        }
    }
    // on error set 0 to fields noit from received message
    if (status != FND_STATUS_SUCCESS && params_len >= 20)
        addr = MESH_NODE_ID_INVALID;

    // send response
    send_config_subs_status_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS, status, elem_addr, addr, company_id, model_id, src_id);
}

uint8_t foundation_find_subs_(tFND_NVM_MODEL *p_model, uint16_t addr, uint8_t* p_status)
{
    uint8_t idx;
    for (idx = 0; idx < p_model->subs_num; idx++)
    {
        if (addr == p_model->subs_list[idx])
            break;
    }
    if (idx >= p_model->subs_num)
        *p_status = FND_STATUS_INVALID_ADDRESS;
    return idx;
}

// params:=ElementAddr(2bytes),SubsAddr(2 or 16 bytes),ModelID(2 or 4 bytes)
// sends response message: Config Model Subscription Status
static void handle_config_subs_del_(uint16_t opcode, uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;
    uint16_t              addr;
    uint8_t               idx;
    uint8_t               status2;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    // if subs address is not virtual
    if (params_len <= 8)
        addr = LE2TOUINT16(p);
    // if subs address is virtual and we already know that virtual address
    else if (0xff != (idx = find_virt_addr_(params + 2)))
        addr = fnd_virt_addr_->addr[idx].short_val;
    // we can't delete or overwrite subscroption with unknown address - call parse_addr_ with invalid addr just to calc all other params
    else
        addr = MESH_NODE_ID_INVALID;

    // handle publication address(check if it is valid)
    p = parse_addr_(params_len >= 20, addr, p, &status);

    // parse model and find it in the configuration. Error status of model parsing has high priority
    status2 = FND_STATUS_SUCCESS;
    p_model = parse_model_(elem_addr, params_len == 22 || params_len == 8, p, &company_id, &model_id, &status2);
    if (status2 != FND_STATUS_SUCCESS)
        status = status2;

    // if we are not failed already then then update found model
    if (status == FND_STATUS_SUCCESS)
    {
        uint8_t idx = foundation_find_subs_(p_model, addr, &status);
        if (status == FND_STATUS_SUCCESS)
        {
            // delete found subscription
            p_model->subs_num--;
            memcpy(&p_model->subs_list[idx], &p_model->subs_list[idx + 1], (p_model->subs_num - idx) * sizeof(p_model->subs_list[0]));
#ifndef MESH_CONTROLLER
            // save configuration in the NVRAM
            foundation_write_config_();
#endif
        }
        else
        {
            TRACE0(TRACE_INFO, "handle_config_subs_del_: no subs\n");
            status = FND_STATUS_SUCCESS;
        }
    }

    // send response
    send_config_subs_status_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS, status, elem_addr, addr, company_id, model_id, src_id);
}

// params:=ElementAddr(2bytes),SubsAddr(2 or 16 bytes),ModelID(2 or 4 bytes)
// sends response message: Config Model Subscription Status
static void handle_config_subs_overwr_(uint16_t opcode, uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;
    uint16_t              addr;
    uint8_t               idx;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    // if subs address is not virtual
    if (params_len <= 8)
        addr = LE2TOUINT16(p);
    // if subs address is virtual and we already know that virtual address
    else if (0xff != (idx = find_virt_addr_(p)))
        addr = fnd_virt_addr_->addr[idx].short_val;
    // we need to calculate virt address
    else
        mesh_calc_virt_addr(p, &addr);

    // handle subs address(check if it is valid)
    p = parse_addr_(params_len >= 20, addr, p, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 22 || params_len == 8, p, &company_id, &model_id, &status);

    // if we are not failed already then then update found model
    if (status == FND_STATUS_SUCCESS)
    {
        // discard the Subscription List and add an Address to the cleared Subscription List of a Model
        p_model->subs_list[0] = addr;
        p_model->subs_num = 1;
#ifndef MESH_CONTROLLER
        // save configuration in the NVRAM
        foundation_write_config_();
#endif
    }

    // on error set 0 to fields noit from received message
    if (status != FND_STATUS_SUCCESS && params_len >= 20)
        addr = MESH_NODE_ID_INVALID;

    // send response
    send_config_subs_status_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS, status, elem_addr, addr, company_id, model_id, src_id);
}

// params:=ElementAddr(2bytes),ModelID(2 or 4 bytes)
// sends response message: Config Model Subscription Status
static void handle_config_subs_del_all_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 6, p, &company_id, &model_id, &status);

    // if we are not failed already then then update found model
    if (status == FND_STATUS_SUCCESS)
    {
        p_model->subs_num = 0;
#ifndef MESH_CONTROLLER
        // save configuration in the NVRAM
        foundation_write_config_();
#endif
    }

    // send response
    send_config_subs_status_(WICED_BT_MESH_CORE_CMD_CONFIG_MODEL_SUBSCRIPTION_STATUS, status, elem_addr, MESH_NODE_ID_INVALID, company_id, model_id, src_id);
}

/**
* Sends response with netkey status message
*
* Parameters:
*   status:         status to send
*   idx:            Index of the netkey
*   src_id:         SRC of the request
*
* Return:   None
*/
void foundation_send_netkey_status(uint8_t status, uint16_t idx, uint16_t src_id)
{
    uint8_t   buf[2 + 1 + 2];
    uint8_t   *p;
    TRACE2(TRACE_INFO, "foundation_send_netkey_status: status:%d idx:%d\n", status, idx);
    TRACE1(TRACE_INFO, " src:%x\n", src_id);
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_NETKEY_STATUS);
    *p++ = status;
    UINT16TOLE2(p, idx);
    p += 2;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

/**
* Sends response with appkey status message
*
* Parameters:
*   status:         status to send
*   netkey_idx:     Index of the netkey
*   appkey_idx:     Index of the appkey
*   src_id:         SRC of the request
*
* Return:   None
*/
void foundation_send_appkey_status(uint8_t status, uint16_t netkey_idx, uint16_t appkey_idx, uint16_t src_id)
{
    uint8_t   buf[2 + 1 + 3];
    uint8_t   *p;
    TRACE2(TRACE_INFO, "foundation_send_appkey_status: status:%d netkey_idx:%x\n", status, netkey_idx);
    TRACE2(TRACE_INFO, " appkey_idx:%x src:%x\n", appkey_idx, src_id);
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_APPKEY_STATUS);
    *p++ = status;
    TWOIDXTOLE3(p, netkey_idx, appkey_idx);
    p += 3;
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

/**
* Adds, updates or deletes Network Key and sends response with netkey status message to src
* It is created to be placed in the non-overlay code because it calls two different overlays
* and it is called from fnd overlay throug serialization
*
* Parameters:
*   local_idx:      Index in the local array for that network key
*   net_key:        Network key (16 bytes). NULL means delete that net_key
*   global_idx:     Global index of this network key. This param is ignored for delete request (net_key == NULL)
*   src_id:         SRC of the request
*
* Return:   None
*/
static void mesh_set_netkey_and_respond(uint8_t local_idx, const uint8_t* net_key, uint16_t global_idx, uint16_t src_id)
{
    uint8_t status = MESH_RESULT_SUCCESS == mesh_set_net_key(local_idx, net_key, global_idx) ?
        FND_STATUS_SUCCESS : FND_STATUS_STORAGE_FAILURE;
    foundation_send_netkey_status(status, global_idx & MESH_GL_KEY_IDX_MASK, src_id);
}

static void handle_provis_netkey_add_(uint16_t global_idx, const uint8_t *net_key, uint16_t src_id)
{
    uint8_t status = FND_STATUS_SUCCESS;
    uint8_t idx;
    // Ignore request with RFU value and don't send responce
    if (global_idx > 0x0fff)
        return;
    if (find_netkey_(global_idx, &idx))
    {
        // if same key has been added already then response success, otherwise sent error status
        if (0 != memcmp(net_key, node_nv_net_key[idx].key, MESH_KEY_LEN))
            status = FND_STATUS_KEY_INDEX_ALREADY_STORED;
    }
    // fail if only one net key is allowed (for PTS tests)
    else if (mesh_core_use_one_net_key)
        status = FND_STATUS_INSUFFICIENT_RESOURCES;
    // find place for new key and send error in no more room for it
    else if (!find_netkey_(0xffff, &idx))
        status = FND_STATUS_INSUFFICIENT_RESOURCES;
    // remember new key
    else if (MESH_RESULT_SUCCESS != mesh_set_net_key(idx, net_key, global_idx))
        status = FND_STATUS_STORAGE_FAILURE;
#ifndef MESH_CONTROLLER
    // on success update advertisings
    else
        mesh_update_beacon();
#endif
    // send netkey status
    foundation_send_netkey_status(status, global_idx, src_id);
}

static void handle_provis_netkey_update_(uint16_t global_idx, const uint8_t *net_key, uint16_t src_id)
{
    uint8_t idx, new_idx;
    TRACE2(TRACE_INFO, "handle_provis_netkey_update_: global_idx:%x src_id:%x\n", global_idx, src_id);
    TRACE1(TRACE_INFO, " net_key_bitmask:%x\n", node_nv_data.net_key_bitmask);
    // Ignore request with RFU value and don't send responce
    if (global_idx > 0x0fff)
        return;
    if (!find_netkey_(global_idx, &idx))
    {
        if(net_key)
            foundation_send_netkey_status(FND_STATUS_INVALID_NETKEY, global_idx, src_id);
        else
            foundation_send_netkey_status(FND_STATUS_SUCCESS, global_idx, src_id);
        return;
    }
    // if that key is in the any KR phase
    if (((node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT) != KEY_REFRESH_PHASE_STATE_NONE)
    {
        // if same new key exists then it is just retransmittion of the netkey_update - response with success
        if (find_netkey_(global_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &new_idx)
            && 0 == memcmp(net_key, node_nv_net_key[new_idx].key, MESH_KEY_LEN))
        {
            foundation_send_netkey_status(FND_STATUS_SUCCESS, global_idx, src_id);
            return;
        }
        // otherwise response with error
        foundation_send_netkey_status(net_key == NULL ? FND_STATUS_CANNOT_REMOVE : FND_STATUS_CANNOT_UPDATE, global_idx, src_id);
        return;
    }
    // if it is request to delete key then just delete it and respond
    if (net_key == NULL)
    {
        // if it is last net_key then fail
        for (new_idx = 0; new_idx < MESH_NET_KEY_MAX_NUM; new_idx++)
        {
            if (new_idx != idx
                && 0 != (node_nv_data.net_key_bitmask & (1 << new_idx)))
                break;
        }
        if (new_idx >= MESH_NET_KEY_MAX_NUM)
        {
            foundation_send_netkey_status(FND_STATUS_CANNOT_REMOVE, global_idx, src_id);
            return;
        }
        mesh_set_netkey_and_respond(idx, NULL, global_idx, src_id);
        return;
    }
    // that key not in the any KR phase
    // find available index for new key
    if (!find_netkey_(0xffff, &new_idx))
    {
        foundation_send_netkey_status(FND_STATUS_INSUFFICIENT_RESOURCES, global_idx, src_id);
        return;
    }
    // mark updating key as old key in phase 1
    node_nv_net_key[idx].global_idx = (node_nv_net_key[idx].global_idx & ~MESH_GL_KEY_IDX_KR_PHASE_MASK) | (KEY_REFRESH_PHASE_STATE_1 << MESH_GL_KEY_IDX_KR_PHASE_SHIFT);
    saveNodeNvNetKey(idx);

    // add key marked as new key
    mesh_set_netkey_and_respond(new_idx, net_key, global_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, src_id);

    low_power_kr_set_phase_1(node_nv_net_key[new_idx].key);
}

wiced_bool_t foundation_find_appkey_(uint16_t appkey_global_idx, uint8_t *p_app_idx, uint8_t *p_net_idx, uint16_t *p_netkey_global_idx)
{
    uint8_t idx, net_idx;
    uint16_t global_net_idx;
    for (idx = 0; idx < MESH_APP_KEY_MAX_NUM; idx++)
    {
        if (node_nv_data.app_key_bitmask & (1 << idx))
        {
            if ((node_nv_app_key[idx].global_idx & (MESH_GL_KEY_IDX_MASK | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG)) == appkey_global_idx)
            {
                global_net_idx = node_nv_app_key[idx].global_net_key_idx;
                // make sure network key is assigned to that appkey
                if (find_netkey_(global_net_idx, &net_idx))
                {
                    if (p_app_idx)
                        *p_app_idx = idx;
                    if (*p_net_idx)
                        *p_net_idx = net_idx;
                    if (p_netkey_global_idx)
                        *p_netkey_global_idx = global_net_idx;
                    break;
                }
            }
        }
        else if (appkey_global_idx == 0xffff)
        {
            if (p_app_idx)
                *p_app_idx = idx;
            break;
        }
    }
    return idx < MESH_APP_KEY_MAX_NUM;
}

/**
* Adds, updates or deletes Application Key and sends response with appkey status message to src
* It is created to be placed in the non-overlay code because it calls two different overlays
* and it is called from fnd overlay throug serialization
*
* Parameters:
*   appkey_local_idx:   Index of the application key in the local array
*   netkey_local_idx:   Index of the network key in the local array
*   app_key:            Application key (16 bytes). NULL means delete that net_key
*   netkey_global_idx:  Global index of this network key. . This param ignore for delete request (net_key == NULL)
*   appkey_global_idx:  Global index of this application key. . This param ignore for delete request (net_key == NULL)
*   src_id:             SRC of the request
*
* Return:   None
*/
static void mesh_set_appkey_and_respond(uint8_t appkey_local_idx, uint8_t netkey_local_idx, const uint8_t* app_key, uint16_t netkey_global_idx, uint16_t appkey_global_idx, uint16_t src_id)
{
    uint8_t status = MESH_RESULT_SUCCESS == mesh_set_app_key(appkey_local_idx, netkey_local_idx, app_key, appkey_global_idx) ?
        FND_STATUS_SUCCESS : FND_STATUS_STORAGE_FAILURE;
    foundation_send_appkey_status(status, netkey_global_idx, appkey_global_idx, src_id);
}

static void handle_provis_appkey_add_(uint16_t netkey_global_idx, uint16_t appkey_global_idx, const uint8_t *app_key, uint16_t src_id)
{
    uint8_t net_idx, app_idx, app_idx2;
    // Ignore request with RFU value and don't send responce
    if (netkey_global_idx > 0x0fff)
        return;
    if (appkey_global_idx > 0x0fff)
        return;

    if (!find_netkey_(netkey_global_idx, &net_idx))
        foundation_send_appkey_status(FND_STATUS_INVALID_NETKEY, netkey_global_idx, appkey_global_idx, src_id);
    else if (foundation_find_appkey_(appkey_global_idx, &app_idx, NULL, NULL))
    {
        // make sure this app key isn't bound to other net key
        if (node_nv_app_key[app_idx].global_net_key_idx != netkey_global_idx)
        {
            foundation_send_appkey_status(FND_STATUS_INVALID_NETKEY, netkey_global_idx, appkey_global_idx, src_id);
            return;
        }

        // An application key can only be used with a single network key. 
        // So make sure any other appkey is not bound to that netkey
        for (app_idx2 = 0; app_idx2 < MESH_APP_KEY_MAX_NUM; app_idx2++)
        {
            if (app_idx == app_idx2)
                continue;
            if ((node_nv_data.app_key_bitmask & (1 << app_idx2)) == 0)
                continue;
            if (node_nv_app_key[app_idx2].global_net_key_idx == netkey_global_idx)
                break;
        }
        if (app_idx2 < MESH_APP_KEY_MAX_NUM)
        {
            foundation_send_appkey_status(FND_STATUS_INVALID_NETKEY, netkey_global_idx, appkey_global_idx, src_id);
            return;
        }

        // if same key has been added already then response success
        if (0 == memcmp(app_key, node_nv_app_key[app_idx].key, MESH_KEY_LEN))
        {
            foundation_send_appkey_status(FND_STATUS_SUCCESS, netkey_global_idx, appkey_global_idx, src_id);
        }
        else
        {
            foundation_send_appkey_status(FND_STATUS_KEY_INDEX_ALREADY_STORED, netkey_global_idx, appkey_global_idx, src_id);
        }
    }
    else
    {
        for (app_idx = 0; app_idx < MESH_APP_KEY_MAX_NUM; app_idx++)
        {
            if ((node_nv_data.app_key_bitmask & (1 << app_idx)) == 0)
                break;
        }
        if (app_idx >= MESH_APP_KEY_MAX_NUM)
            foundation_send_appkey_status(FND_STATUS_INSUFFICIENT_RESOURCES, netkey_global_idx, appkey_global_idx, src_id);
        else
            mesh_set_appkey_and_respond(app_idx, net_idx, app_key, netkey_global_idx, appkey_global_idx, src_id);
    }
}

static void handle_provis_appkey_update_(uint16_t netkey_global_idx, uint16_t appkey_global_idx, const uint8_t *app_key, uint16_t src_id)
{
    uint8_t app_idx, net_idx, new_idx;
    uint16_t current_netkey_global_idx;

    // Ignore request with RFU value and don't send responce
    if (netkey_global_idx > 0x0fff)
        return;
    if (appkey_global_idx > 0x0fff)
        return;

    if (!find_netkey_(netkey_global_idx, &net_idx))
    {
        foundation_send_appkey_status(FND_STATUS_INVALID_NETKEY, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }

    if (!foundation_find_appkey_(appkey_global_idx, &app_idx, &net_idx, &current_netkey_global_idx))
    {
        if (app_key)
            foundation_send_appkey_status(FND_STATUS_INVALID_APPKEY, netkey_global_idx, appkey_global_idx, src_id);
        else
            foundation_send_appkey_status(FND_STATUS_SUCCESS, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }
    if (netkey_global_idx != current_netkey_global_idx)
    {
        foundation_send_appkey_status(FND_STATUS_INVALID_BINDING, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }
    // if that key is in the any KR phase
    if (((node_nv_app_key[app_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) >> MESH_GL_KEY_IDX_KR_PHASE_SHIFT) != KEY_REFRESH_PHASE_STATE_NONE)
    {
        // if same new key exists then it is just retransmittion of the netkey_update - response with success
        if (foundation_find_appkey_(appkey_global_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, &new_idx, NULL, NULL)
            && 0 == memcmp(app_key, node_nv_app_key[new_idx].key, MESH_KEY_LEN))
        {
            foundation_send_appkey_status(FND_STATUS_SUCCESS, netkey_global_idx, appkey_global_idx, src_id);
            return;
        }
        // otherwise response with error
        foundation_send_appkey_status(app_key == NULL ? FND_STATUS_CANNOT_REMOVE : FND_STATUS_CANNOT_UPDATE, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }

    // if it is request to delete key then just delete it and respond
    if (app_key == NULL)
    {
        mesh_set_appkey_and_respond(app_idx, net_idx, NULL, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }

    // if related netkey is not updated yet then return error status
    if ((node_nv_net_key[net_idx].global_idx & MESH_GL_KEY_IDX_KR_PHASE_MASK) == 0)
    {
        foundation_send_appkey_status(FND_STATUS_CANNOT_UPDATE, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }

    // that key not in the any KR phase. find available index for new key
    if (!foundation_find_appkey_(0xffff, &new_idx, NULL, NULL))
    {
        foundation_send_appkey_status(FND_STATUS_INSUFFICIENT_RESOURCES, netkey_global_idx, appkey_global_idx, src_id);
        return;
    }
    // mark updating key as old key in phase 1
    node_nv_app_key[app_idx].global_idx = (node_nv_app_key[app_idx].global_idx & ~MESH_GL_KEY_IDX_KR_PHASE_MASK) | (KEY_REFRESH_PHASE_STATE_1 << MESH_GL_KEY_IDX_KR_PHASE_SHIFT);
    saveNodeNvAppKey(app_idx);

    // add key marked as new key
    mesh_set_appkey_and_respond(new_idx, net_idx, app_key, netkey_global_idx, appkey_global_idx | MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG, src_id);
}

// Sends message appkey list message: status(1byte), NetKeyIdx(2bytes), AppKeyIndexes
static void handle_provis_appkey_get_(uint16_t netkey_global_idx, uint16_t src_id)
{
    uint8_t   buf[2 + 1 + 2 + (MESH_APP_KEY_MAX_NUM * 3 + 1) / 2];
    uint8_t   idx, net_idx;
    uint16_t  key_idx, key_idx_prev = 0xffff;
    uint8_t   *p;

    // Ignore request with RFU value and don't send responce
    if (netkey_global_idx > 0x0fff)
        return;

    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_APPKEY_LIST);

    // if net key is unknown
    if (!find_netkey_(netkey_global_idx, &net_idx))
    {
        // status
        *p++ = FND_STATUS_INVALID_NETKEY;
        // NetKeyIdx
        UINT16TOLE2(p, netkey_global_idx);
        p += 2;
    }
    else
    {
        // status
        *p++ = FND_STATUS_SUCCESS;
        // NetKeyIdx
        UINT16TOLE2(p, netkey_global_idx);
        p += 2;
        //AppKeyIndexes
        // go trough each app key
        for (idx = 0; idx < MESH_APP_KEY_MAX_NUM; idx++)
        {
            if ((node_nv_data.app_key_bitmask & (1 << idx)) == 0)
                continue;
            // ignore app key bound to the other net key
            if (netkey_global_idx != (node_nv_app_key[idx].global_net_key_idx & MESH_GL_KEY_IDX_MASK))
                continue;
            // if appkey is in the non-0 KR phase then ignore new appkey
            if ((node_nv_app_key[idx].global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG) != 0)
                continue;
            // copy that app key index to the message
            key_idx = node_nv_app_key[idx].global_idx & MESH_GL_KEY_IDX_MASK;
            if (key_idx_prev == 0xffff)
                key_idx_prev = key_idx;
            else
            {
                TWOIDXTOLE3(p, key_idx_prev, key_idx);
                p += 3;
                key_idx_prev = 0xffff;
            }
        }
        if (key_idx_prev != 0xffff)
        {
            UINT16TOLE2(p, key_idx_prev);
            p += 2;
        }
    }
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

// Sends message netkey list message: status(1byte), NetKeyIndexes
static void handle_provis_netkey_get_(uint16_t src_id)
{
    uint8_t   buf[2 + 1 + (MESH_NET_KEY_MAX_NUM * 3 + 1)/ 2];
    uint8_t   idx;
    uint16_t  key_idx, key_idx_prev = 0xffff;
    uint8_t   *p;

    // OP code
    p = op2be(&buf[0], WICED_BT_MESH_CORE_CMD_NETKEY_LIST);

    //AppKeyIndexes
    // go trough each net key
    for (idx = 0; idx < MESH_NET_KEY_MAX_NUM; idx++)
    {
        if ((node_nv_data.net_key_bitmask & (1 << idx)) == 0)
            continue;
        // ignore new net key in the KR phase
        if ((node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_KR_NEW_KEY_FLAG) != 0)
            continue;
        // copy that net key index to the message
        key_idx = node_nv_net_key[idx].global_idx & MESH_GL_KEY_IDX_MASK;
        if (key_idx_prev == 0xffff)
            key_idx_prev = key_idx;
        else
        {
            TWOIDXTOLE3(p, key_idx_prev, key_idx);
            p += 3;
            key_idx_prev = 0xffff;
        }
    }
    if (key_idx_prev != 0xffff)
    {
        UINT16TOLE2(p, key_idx_prev);
        p += 2;
    }

    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(buf, (uint16_t)(p - &buf[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

// sends response message: Model App Status
// status(1byte),ElementAddr(2bytes),AppKeyIndex(2bytes),ModelId(2 or 4 bytes)
static void send_provis_model_app_status_(uint8_t status, uint16_t elem_addr, uint16_t model_app_key_idx, uint16_t company_id, uint16_t model_id, uint16_t src_id)
{
    uint8_t   msg[32];
    uint8_t   *p_msg;

    p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_MODEL_APP_STATUS);
    *p_msg++ = status;
    UINT16TOLE2(p_msg, elem_addr);
    p_msg += 2;
    UINT16TOLE2(p_msg, model_app_key_idx);
    p_msg += 2;
    if (company_id)
    {
        UINT16TOLE2(p_msg, company_id);
        p_msg += 2;
    }
    UINT16TOLE2(p_msg, model_id);
    p_msg += 2;

    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(msg, (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

wiced_bool_t foundation_find_model_app_(tFND_NVM_MODEL *p_model, uint16_t model_app_key_idx, uint8_t *p_idx)
{
    // find model_app_key_idx in the bound keys
    uint8_t idx;
    for (idx = 0; idx < p_model->app_bind_num; idx++)
    {
        if (p_model->app_bind[idx] == model_app_key_idx)
            break;
    }
    if (idx >= p_model->app_bind_num)
        return WICED_FALSE;
    if (p_idx)
        *p_idx = idx;
    return WICED_TRUE;
}

#ifdef HARDCODED_PROVIS
uint8_t mesh_core_app_bind_(uint8_t element, uint16_t company_id, uint16_t model_id, uint16_t global_app_key_idx)
{
    uint8_t     status = FND_STATUS_SUCCESS;
    tFND_NVM_MODEL *p_model;
    status = foundation_find_model_(node_nv_data.node_id + element, company_id, model_id, &p_model, NULL);
    // if we are not failed already then then proceed
    if (status == FND_STATUS_SUCCESS)
    {
        if (foundation_find_model_app_(p_model, global_app_key_idx, NULL))
            status = FND_STATUS_INVALID_APPKEY;
        else if (p_model->app_bind_num >= FND_MODEL_MAX_APP_BIND_NUM)
            status = FND_STATUS_INSUFFICIENT_RESOURCES;
        else
        {
            // update found model
            p_model->app_bind[p_model->app_bind_num++] = global_app_key_idx;
#ifndef MESH_CONTROLLER
            // save configuration in the NVRAM
            foundation_write_config_();
#endif
        }
    }
    return status;
}
#endif

// params:=ElementAddr(2bytes),AppKeyIndex(2bytes),ModelID(2 or 4 bytes)
// sends response message: Model App Status: status(1byte), ElementAddr(2bytes), AppKeyIndex(2bytes), ModelID(2 or 4 bytes)
static void handle_provis_model_app_bind_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_app_key_idx, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    model_app_key_idx = LE2TOUINT16(p);
    p += 2;

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 8, p, &company_id, &model_id, &status);

    // if we are not failed already then then proceed
    if (status == FND_STATUS_SUCCESS)
    {
        if (model_app_key_idx >= 0x1000
            || !foundation_find_appkey_(model_app_key_idx, NULL, NULL, NULL))
            status = FND_STATUS_INVALID_APPKEY;
        else if (model_id == WICED_BT_MESH_CORE_MODEL_ID_CONFIG_SRV)
            status = FND_STATUS_CANNOT_BIND;
        // if same appkey is already bound then response success
        else if (foundation_find_model_app_(p_model, model_app_key_idx, NULL))
            status = FND_STATUS_SUCCESS;
        else if (p_model->app_bind_num >= FND_MODEL_MAX_APP_BIND_NUM)
            status = FND_STATUS_INSUFFICIENT_RESOURCES;
        else
        {
            // update found model
            p_model->app_bind[p_model->app_bind_num++] = model_app_key_idx;
#ifndef MESH_CONTROLLER
            // save configuration in the NVRAM
            foundation_write_config_();
#endif
        }
    }

    // send response
    send_provis_model_app_status_(status, elem_addr, model_app_key_idx, company_id, model_id, src_id);
}

// params:=ElementAddr(2bytes),AppKeyIndex(2bytes),ModelID(2 or 4 bytes)
// sends response message: Model App Status: status(1byte), ElementAddr(2bytes), AppKeyIndex(2bytes), ModelID(2 or 4 bytes)
static void handle_provis_model_app_unbind_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_app_key_idx, model_id, company_id = 0;
    const uint8_t         *p = params;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    p = parse_elem_addr_(p, &elem_addr, &status);

    model_app_key_idx = LE2TOUINT16(p);
    p += 2;

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len == 8, p, &company_id, &model_id, &status);

    // if we are not failed already then then proceed
    if (status == FND_STATUS_SUCCESS)
    {
        uint8_t idx;
        // find model_app_key_idx in the bound keys
        if (!foundation_find_model_app_(p_model, model_app_key_idx, &idx))
            status = FND_STATUS_INVALID_APPKEY;
        else
        {
            // delete found key index and update found model
            p_model->app_bind_num--;
            if (p_model->app_bind_num - idx > 0)
                memcpy(&p_model->app_bind[idx], &p_model->app_bind[idx + 1], (p_model->app_bind_num - idx) * sizeof(p_model->app_bind[0]));
#ifndef MESH_CONTROLLER
            // save configuration in the NVRAM
            foundation_write_config_();
#endif
        }
    }

    // send response
    send_provis_model_app_status_(status, elem_addr, model_app_key_idx, company_id, model_id, src_id);
}

// sends response message: Model App List
// status(1byte),ElementAddr(2bytes),ModelId(2 or 4bytes),AppKeyIdx(2*N)
static void send_provis_model_app_list_(uint8_t status, uint16_t elem_addr, uint16_t company_id, uint16_t model_id, tFND_NVM_MODEL *p_model, uint16_t src_id)
{
    uint8_t   msg[9 + FND_MODEL_MAX_APP_BIND_NUM * 2];
    uint8_t   *p_msg;
    uint8_t   idx;

    // if it is vendor model
    if (company_id)
        p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_VENDOR_MODEL_APP_LIST);
    else
        p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_SIG_MODEL_APP_LIST);

    *p_msg++ = status;
    UINT16TOLE2(p_msg, elem_addr);
    p_msg += 2;
    if (company_id)
    {
        UINT16TOLE2(p_msg, company_id);
        p_msg += 2;
    }
    UINT16TOLE2(p_msg, model_id);
    p_msg += 2;

    if (status == FND_STATUS_SUCCESS)
    {
        for (idx = 0; idx < p_model->app_bind_num; idx++)
        {
            UINT16TOLE2(p_msg, p_model->app_bind[idx]);
            p_msg += 2;
        }
    }
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(msg, (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);
}

// params:=ElementAddr(2bytes),ModelID(2 or 4 bytes)
// sends response message: Model App List: status(1byte), ElementAddr(2bytes), ModelID(2bytes)
static void handle_provis_model_app_get_(uint16_t src_id, const uint8_t *params, uint8_t params_len)
{
    tFND_NVM_MODEL     *p_model;
    uint16_t              elem_addr, model_id, company_id = 0;
    uint8_t               status = FND_STATUS_SUCCESS;

    // parse and check element address
    params = parse_elem_addr_(params, &elem_addr, &status);

    // parse model and find it in the configuration
    p_model = parse_model_(elem_addr, params_len > 4, params, &company_id, &model_id, &status);

    // send response
    send_provis_model_app_list_(status, elem_addr, company_id, model_id, p_model, src_id);
}

static void delayed_node_reset(uint32_t param)
{
    TRACE0(TRACE_INFO, "delayed_node_reset:\n");
    wiced_bt_mesh_core_init(NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
#ifndef _DEB_DONT_REBOOT_AFTER_RESET
    mesh_core_reboot(300);
#endif
}

// resets node after it sends response message: Security Model Node Reset Status - no params
static void handle_provis_node_reset_(uint16_t src_id)
{
    uint8_t   msg[32];
    uint8_t   *p_msg;

    p_msg = op2be(&msg[0], WICED_BT_MESH_CORE_CMD_NODE_RESET_STATUS);
    // 0xffffffff in the p_event means do random delay 20-50 ms
    access_layer_send(msg, (uint16_t)(p_msg - &msg[0]), 0xff, 0, src_id, node_nv_data.state_default_ttl, foundation_rcv_friend_idx != -2, NULL, (wiced_bt_mesh_event_t*)0xffffffff);

#ifndef MESH_CONTROLLER
    // reset node with 1 second delay to give chance to the response message to be sent
    wiced_init_timer(&reset_timer, delayed_node_reset, 0, WICED_MILLI_SECONDS_TIMER);
    wiced_start_timer(&reset_timer, 2000);
    TRACE0(TRACE_INFO, "handle_provis_node_reset_: 2 sec timer started\n");
#endif
}

/**
* \brief Retrives Publication parameters of the Model.
* \details If should be used for sending unsolicited message to get Publication Information (app_key_idx, destination address and TTL) before calling wiced_bt_mesh_core_send()
*
* @return      wiced_result_t
*/
wiced_result_t wiced_bt_mesh_core_get_publication(wiced_bt_mesh_event_t *p_event)
{
    tFND_NVM_MODEL  *p_model;

    TRACE3(TRACE_INFO, "wiced_bt_mesh_core_get_publication: element_idx:%d company_id:%x model_id:%x\n", p_event->element_idx, p_event->company_id, p_event->model_id);
    if (!foundation_find_model_(node_nv_data.node_id + p_event->element_idx, p_event->company_id, p_event->model_id, &p_model, NULL) == FND_STATUS_SUCCESS)
    {
        TRACE0(TRACE_WARNING, "wiced_bt_mesh_core_get_publication: model not found\n");
        return WICED_BT_ERROR;
    }
    if(p_model->publish_address == MESH_NODE_ID_INVALID)
    {
        TRACE0(TRACE_WARNING, "wiced_bt_mesh_core_get_publication: no publish address\n");
        return WICED_BT_ERROR;
    }
    if (!foundation_find_appkey_(p_model->publish_app_key_idx, &p_event->app_key_idx, NULL, NULL))
    {
        TRACE0(TRACE_WARNING, "wiced_bt_mesh_core_get_publication: no app key\n");
        return WICED_BT_ERROR;
    }
    p_event->dst = p_model->publish_address;
    p_event->ttl = p_model->publish_ttl;
    p_event->credential_flag = p_model->credential_flag;
    p_event->period = p_model->publish_period;
    p_event->retransmit = p_model->retransmit;
    TRACE3(TRACE_INFO, "wiced_bt_mesh_core_get_publication: app_key_idx:%x ttl:%x dst:%x\n", p_event->app_key_idx, p_event->ttl, p_event->dst);
    TRACE4(TRACE_INFO, " credential_flag:%d period:%d retransmit:%x publish_period:%x\n", p_event->credential_flag, p_event->period, p_event->retransmit, p_model->publish_period);
    return WICED_BT_SUCCESS;
}
