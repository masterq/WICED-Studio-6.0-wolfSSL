/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 *
 * This file shows how to create a device which implements mesh user property client.
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3006
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_property_dev_name[]       = "Property Client";
uint8_t mesh_property_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_property_mfr_name[]       = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_property_model_num[]      = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_property_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };


wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
        WICED_BT_MESH_DEVICE,
        WICED_BT_MESH_MODEL_PROPERTY_CLIENT,
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_PROPERTY_CLIENT_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 0,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = 0,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};


/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_property_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_properties_client_get(uint8_t *p_data, uint32_t length);
static void mesh_property_client_get(uint8_t *p_data, uint32_t length);
static void mesh_property_client_set(uint8_t *p_data, uint32_t length);
static void mesh_properties_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_properties_status_data_t *p_data);
static void mesh_property_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_property_status_data_t *p_data);

/******************************************************
 *          Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    // register with the library to receive parsed data
    wiced_bt_mesh_model_property_client_init(MESH_PROPERTY_CLIENT_ELEMENT_INDEX, mesh_property_client_message_handler, is_provisioned);
}

/*
 * Process event received from the property Server.
 */
void mesh_property_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    wiced_bt_mesh_property_status_data_t   *p_property_status;
    wiced_bt_mesh_properties_status_data_t *p_properties_status;
    WICED_BT_TRACE("property clt msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_CLIENT_PROPERTIES_STATUS:
    case WICED_BT_MESH_ADMIN_PROPERTIES_STATUS:
    case WICED_BT_MESH_MANUF_PROPERTIES_STATUS:
    case WICED_BT_MESH_USER_PROPERTIES_STATUS:
        p_properties_status = (wiced_bt_mesh_properties_status_data_t *)p_data;
        WICED_BT_TRACE("user properties type:%d num:%d\n", p_properties_status->type, p_properties_status->properties_num);

#if defined HCI_CONTROL
        mesh_properties_hci_event_send(p_event, p_properties_status);
#endif
        break;

    case WICED_BT_MESH_ADMIN_PROPERTY_STATUS:
    case WICED_BT_MESH_MANUF_PROPERTY_STATUS:
    case WICED_BT_MESH_USER_PROPERTY_STATUS:
        p_property_status = (wiced_bt_mesh_property_status_data_t *)p_data;
        WICED_BT_TRACE("user property type:%d id:%04x len:%d\n", p_property_status->type, p_property_status->id, p_property_status->len);

#if defined HCI_CONTROL
        mesh_property_hci_event_send(p_event, p_property_status);
#endif
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
    wiced_bt_mesh_release_event(p_event);
}

/*
 * In 2 chip solutions MCU can send commands to change property state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_PROPERTIES_GET:
        mesh_properties_client_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROPERTY_GET:
        mesh_property_client_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROPERTY_SET:
        mesh_property_client_set(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
    return 0;
}

/*
 * Create mesh event for unsolicited message and send properties get command
 */
void mesh_properties_client_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint16_t property_id;
    wiced_bt_mesh_properties_get_data_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT8(get_data.type, p_data);

    // starting ID is valid for Client Properties
    if ((get_data.type == WICED_BT_MESH_PROPERTY_TYPE_CLIENT) && (length >= 7))
    {
        STREAM_TO_UINT16(get_data.starting_id, p_data);
    }
    else
    {
        get_data.starting_id = 0;
    }
    p_event = wiced_bt_mesh_create_event(MESH_PROPERTY_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("properties clnt get: no mem\n");
        return;
    }
    wiced_bt_mesh_model_property_client_send_properties_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send property get command
 */
void mesh_property_client_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint16_t property_id;
    wiced_bt_mesh_property_get_data_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT8(get_data.type, p_data);
    STREAM_TO_UINT16(get_data.id, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_PROPERTY_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("property state changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_property_client_send_property_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send property set command
 */
void mesh_property_client_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    wiced_bt_mesh_property_set_data_t set_data;
    uint16_t dst;
    uint16_t app_key_idx;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_PROPERTY_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_PROPERTY_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("property state changed: no mem\n");
        return;
    }
    STREAM_TO_UINT8(p_event->reply, p_data);
    STREAM_TO_UINT8(set_data.type, p_data);
    STREAM_TO_UINT16(set_data.id, p_data);
    STREAM_TO_UINT8(set_data.access, p_data);

    set_data.len = length - 9;
    STREAM_TO_ARRAY(set_data.value, p_data, set_data.len);

    wiced_bt_mesh_model_property_client_send_property_set(p_event, &set_data);
}

#ifdef HCI_CONTROL
/*
 * Send Properties Status event over transport
 */
void mesh_properties_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_properties_status_data_t *p_data)
{
    int i;
    uint8_t *p_buffer;
    uint8_t *p;

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(5 + p_data->properties_num * sizeof(uint16_t))) != NULL)
    {
        p = p_buffer;

        UINT16_TO_STREAM(p, p_event->src);
        UINT8_TO_STREAM(p, p_event->app_key_idx);
        UINT8_TO_STREAM(p, p_event->element_idx);
        UINT8_TO_STREAM(p, p_data->type);
        for (i = 0; i < p_data->properties_num; i++)
            UINT16_TO_STREAM(p, p_data->id[i]);

        wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROPERTIES_STATUS, p_buffer, (uint16_t)(p - p_buffer));
        wiced_bt_free_buffer(p_buffer);
    }
}

/*
 * Send property Status event over transport
 */
void mesh_property_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_property_status_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(7 + p_data->len)) != NULL)
    {
        p = p_buffer;

        UINT16_TO_STREAM(p, p_event->src);
        UINT8_TO_STREAM(p, p_event->app_key_idx);
        UINT8_TO_STREAM(p, p_event->element_idx);
        UINT8_TO_STREAM(p, p_data->type);
        UINT8_TO_STREAM(p, p_data->access);
        UINT16_TO_STREAM(p, p_data->id);
        ARRAY_TO_STREAM(p, p_data->value, p_data->len);

        wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROPERTY_STATUS, p_buffer, (uint16_t)(p - p_buffer));
        wiced_bt_free_buffer(p_buffer);
    }
}

#endif
