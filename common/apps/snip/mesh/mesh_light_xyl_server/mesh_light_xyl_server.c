/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * This file shows how to create a mesh dimmable color light.
 *
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#include "wiced_timer.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

#define MESH_LIGHT_XYL_X_MIN                    0
#define MESH_LIGHT_XYL_X_MAX                    0xffff
#define MESH_LIGHT_XYL_Y_MIN                    0
#define MESH_LIGHT_XYL_Y_MAX                    0xffff

#define MESH_LIGHT_LIGHTNESS_TICK_IN_MS         100 // 25

#define WICED_BT_MESH_LIGHT_LIGHTNESS_DEFAULT   0
#define WICED_BT_MESH_LIGHT_LIGHTNESS_MIN       0
#define WICED_BT_MESH_LIGHT_LIGHTNESS_MAX       100

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3106
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_light_xyl_server_dev_name[]       = "Light xyL Server";
uint8_t mesh_light_xyl_server_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_light_xyl_server_mfr_name[]       = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_light_xyl_server_model_num[]      = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_light_xyl_server_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };


wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_LIGHT_XYL_SETUP_SERVER,
};

#define MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX             0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 1,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = 0,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t)),  // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};

// Default x and y ranges.
wiced_bt_mesh_light_xyl_xy_settings_t xy_settings =
{
    .x_default = 0,
    .x_min     = 0,
    .x_max     = 0xffff,
    .y_default = 0,
    .y_min     = 0,
    .y_max     = 0xffff,
};

/******************************************************
 *          Structures
 ******************************************************/
typedef struct
{
#define MESH_TRANSITION_STATE_IDLE               0
#define MESH_TRANSITION_STATE_DELAY        1
#define MESH_TRANSITION_STATE_TRANSITION   2
    uint8_t              transition_state;
    wiced_bt_mesh_light_xyl_data_t present;
    wiced_bt_mesh_light_xyl_data_t target;
    wiced_bt_mesh_light_xyl_data_t default_value;
    wiced_bt_mesh_light_xyl_data_t range_min;
    wiced_bt_mesh_light_xyl_data_t range_max;
    wiced_bt_mesh_light_xyl_data_t start;
    uint32_t             transition_start_time;
    uint32_t             transition_remaining_time;
    uint32_t             transition_time;
    uint16_t             delay;
    wiced_timer_t        timer;
} mesh_light_xyl_server_t;

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void     mesh_light_xyl_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void     mesh_light_xyl_server_status_changed(uint8_t *p_data, uint32_t length);
static void     mesh_light_xyl_server_default_status_changed(uint8_t *p_data, uint32_t length);
static void     mesh_light_xyl_server_range_status_changed(uint8_t *p_data, uint32_t length);
static void     mesh_light_xyl_server_send_status(wiced_bt_mesh_event_t *p_event);
static void     mesh_light_xyl_server_send_default_status(wiced_bt_mesh_event_t *p_event);
static void     mesh_light_xyl_server_send_range_status(wiced_bt_mesh_event_t *p_event, uint8_t result);
static void     mesh_light_xyl_process_set(uint8_t element_idx, wiced_bt_mesh_light_xyl_set_t *p_data);
static void     mesh_light_xyl_process_set_lightness_range(uint8_t element_idx, wiced_bt_mesh_light_lightness_range_set_data_t *p_data);
static uint8_t  mesh_light_xyl_process_set_range(uint8_t element_idx, wiced_bt_mesh_light_xyl_range_set_data_t *p_data);
static void     mesh_light_xyl_app_timer_callback(uint32_t arg);
static void     mesh_light_xyl_server_send_lightness_range_status(wiced_bt_mesh_event_t *p_event, uint8_t status);

#ifdef HCI_CONTROL
static void     mesh_light_xyl_hci_event_send_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_xyl_set_t *p_data);
static void     mesh_light_xyl_hci_event_send_set_lightness_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_range_set_data_t *p_data);
static void     mesh_light_xyl_hci_event_send_set_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_xyl_range_set_data_t *p_data);
#endif

/******************************************************
 *          Variables Definitions
 ******************************************************/
// Application state
mesh_light_xyl_server_t app_state;

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    memset (&app_state, 0, sizeof(app_state));
    app_state.transition_state    = MESH_TRANSITION_STATE_IDLE;
    app_state.range_min.lightness = mesh_config.elements[MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX].range_min;
    app_state.range_max.lightness = mesh_config.elements[MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX].range_max;
    app_state.default_value.x     = xy_settings.x_default;
    app_state.range_min.x         = xy_settings.x_min;
    app_state.range_max.x         = xy_settings.x_max;
    app_state.default_value.y     = xy_settings.y_default;
    app_state.range_min.y         = xy_settings.y_min;
    app_state.range_max.y         = xy_settings.y_max;
    wiced_init_timer(&app_state.timer, &mesh_light_xyl_app_timer_callback, MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, WICED_MILLI_SECONDS_TIMER);

    wiced_bt_mesh_model_light_xyl_setup_server_init(MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, mesh_light_xyl_server_message_handler, &xy_settings, is_provisioned);
}

/*
 * Process event received from the models library for mesh xyl lighting.
 */
void mesh_light_xyl_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    uint8_t result;

    WICED_BT_TRACE("light xyl srv msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_LIGHT_XYL_GET:
        mesh_light_xyl_server_send_status(wiced_bt_mesh_create_reply_event(p_event));
        break;

    case WICED_BT_MESH_LIGHT_XYL_SET:
#if defined HCI_CONTROL
        mesh_light_xyl_hci_event_send_set(p_event, (wiced_bt_mesh_light_xyl_set_t *)p_data);
#endif
        mesh_light_xyl_process_set(p_event->element_idx, (wiced_bt_mesh_light_xyl_set_t *)p_data);
        if (p_event->reply)
        {
            mesh_light_xyl_server_send_status(wiced_bt_mesh_create_reply_event(p_event));
        }
        else
        {
            wiced_bt_mesh_release_event(p_event);
        }
        break;

    case WICED_BT_MESH_LIGHT_LIGHTNESS_SET_RANGE:
#if defined HCI_CONTROL
        mesh_light_xyl_hci_event_send_set_lightness_range(p_event, (wiced_bt_mesh_light_lightness_range_set_data_t *)p_data);
#endif
        mesh_light_xyl_process_set_lightness_range(p_event->element_idx, (wiced_bt_mesh_light_lightness_range_set_data_t *)p_data);
        if (p_event->reply)
        {
            mesh_light_xyl_server_send_lightness_range_status(wiced_bt_mesh_create_reply_event(p_event), WICED_BT_MESH_STATUS_SUCCESS);
        }
        else
        {
            wiced_bt_mesh_release_event(p_event);
        }
        break;

    case WICED_BT_MESH_LIGHT_XYL_RANGE_SET:
#if defined HCI_CONTROL
        mesh_light_xyl_hci_event_send_set_range(p_event, (wiced_bt_mesh_light_xyl_range_set_data_t *)p_data);
#endif
        result = mesh_light_xyl_process_set_range(p_event->element_idx, (wiced_bt_mesh_light_xyl_range_set_data_t *)p_data);
        if (p_event->reply)
        {
            mesh_light_xyl_server_send_range_status(wiced_bt_mesh_create_reply_event(p_event), result);
        }
        else
        {
            wiced_bt_mesh_release_event(p_event);
        }
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        wiced_bt_mesh_release_event(p_event);
        break;
    }
}

/*
 * In 2 chip solutions MCU can send commands to indicate that Level has changed.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
#ifdef HCI_CONTROL
    uint16_t dst         = p_data[0] + (p_data[1] << 8);
    uint16_t app_key_idx = p_data[2] + (p_data[3] << 8);

    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_LIGHT_XYL_SET:
        mesh_light_xyl_server_status_changed(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_XYL_DEFAULT_SET:
        mesh_light_xyl_server_default_status_changed(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_XYL_RANGE_SET:
        mesh_light_xyl_server_range_status_changed(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
#endif
    return 0;
}

/*
 * Create mesh event for unsolicited message and send Light xyL Status event
 */
void mesh_light_xyl_server_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    // local change aborts the transaction
    app_state.transition_state = MESH_TRANSITION_STATE_IDLE;
    app_state.transition_remaining_time = 0;
    wiced_stop_timer(&app_state.timer);

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    p_data++;      // skip reliable

    STREAM_TO_UINT16(app_state.present.lightness, p_data);
    STREAM_TO_UINT16(app_state.present.x, p_data);
    STREAM_TO_UINT32(app_state.present.y, p_data);

    memcpy(&app_state.target, &app_state.present, sizeof(&app_state.target));

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("light xyl status: no mem\n");
        return;
    }
    mesh_light_xyl_server_send_status(p_event);
}

/*
 * Create mesh event for unsolicited message and send Light xyL Default Status event
 */
void mesh_light_xyl_server_default_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    p_data++;    // skipt reliable 

    STREAM_TO_UINT16(app_state.default_value.lightness, p_data);
    STREAM_TO_UINT16(app_state.default_value.x, p_data);
    STREAM_TO_UINT16(app_state.default_value.y, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("light xyl def status: no mem\n");
        return;
    }
    mesh_light_xyl_server_send_default_status(p_event);
}

/*
 * Create mesh event for unsolicited message and send Light xyL Default Status event
 */
void mesh_light_xyl_server_range_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint8_t  result;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    p_data++;    // skipt reliable 

    STREAM_TO_UINT8(result, p_data);
    STREAM_TO_UINT16(app_state.range_min.x, p_data);
    STREAM_TO_UINT16(app_state.range_max.x, p_data);
    STREAM_TO_UINT16(app_state.range_min.y, p_data);
    STREAM_TO_UINT16(app_state.range_max.y, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("light xyl def status: no mem\n");
        return;
    }
    mesh_light_xyl_server_send_range_status(p_event, result);
}

/*
 * Command from the xyL, Lightness, Generic Level or On/Off client received to set the new level
 */
void mesh_light_xyl_process_set(uint8_t element_idx, wiced_bt_mesh_light_xyl_set_t *p_set)
{
    wiced_result_t res;

    WICED_BT_TRACE("light xyl srv set state:%d target light:%d x:%d y:%d time:%d delay:%d\n",
            app_state.transition_state, p_set->target.lightness, p_set->target.x, p_set->target.y, p_set->transition_time, p_set->delay);

    // whenever we receive set, the previous transition is cancelled.
    app_state.transition_state  = MESH_TRANSITION_STATE_IDLE;
    wiced_stop_timer(&app_state.timer);

    memcpy(&app_state.target, &p_set->target, sizeof(wiced_bt_mesh_light_xyl_data_t));

    // Special time 0xFFFFFFFF indicates that transition should stop
    if (p_set->transition_time == WICED_BT_MESH_TRANSITION_TIME_DEFAULT)
    {
        memcpy(&app_state.target, &app_state.present, sizeof(&app_state.target));
        app_state.transition_remaining_time = 0;
    }
    // If transition time is 0, change the state immediately to the target state.
    else if (p_set->transition_time == 0)
    {
        memcpy(&app_state.present, &p_set->target, sizeof(wiced_bt_mesh_light_xyl_data_t));
        app_state.transition_remaining_time = 0;
    }
    else
    {
        // Transition time is not 0.  Save parameters and start delay or transition itself.
        app_state.transition_time       = p_set->transition_time;
        app_state.delay                 = p_set->delay;
        app_state.transition_start_time = (uint32_t)wiced_bt_mesh_core_get_tick_count();
        memcpy(&app_state.start, &app_state.present, sizeof(wiced_bt_mesh_light_xyl_data_t));

        if (p_set->delay != 0)
        {
            app_state.transition_state  = MESH_TRANSITION_STATE_DELAY;
            res = wiced_start_timer(&app_state.timer, app_state.delay);
        }
        else
        {
            app_state.transition_state          = MESH_TRANSITION_STATE_TRANSITION;
            app_state.transition_remaining_time = app_state.transition_time;
            res = wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);
        }
        WICED_BT_TRACE("next state:%d start_timer_result:%d\n", app_state.transition_state, res);
    }
}

void mesh_light_xyl_process_set_lightness_range(uint8_t element_idx, wiced_bt_mesh_light_lightness_range_set_data_t *p_data)
{
    app_state.range_min.lightness = p_data->min_level;
    app_state.range_max.lightness = p_data->max_level;
}

uint8_t mesh_light_xyl_process_set_range(uint8_t element_idx, wiced_bt_mesh_light_xyl_range_set_data_t *p_data)
{
    if ((p_data->x_min < MESH_LIGHT_XYL_X_MIN) ||
        (p_data->y_min < MESH_LIGHT_XYL_Y_MIN))
    {
        return WICED_BT_MESH_STATUS_CANNOT_SET_RANGE_MIN;
    }
    if ((p_data->x_max > MESH_LIGHT_XYL_X_MAX) ||
        (p_data->y_min > MESH_LIGHT_XYL_Y_MAX))
    {
        return WICED_BT_MESH_STATUS_CANNOT_SET_RANGE_MAX;
    }
    app_state.range_min.x = p_data->x_min;
    app_state.range_max.x = p_data->x_max;
    app_state.range_min.y = p_data->y_min;
    app_state.range_max.y = p_data->y_max;
    return WICED_BT_MESH_STATUS_SUCCESS;
}

/*
 * Transition timeout
 */
void mesh_light_xyl_app_timer_callback(uint32_t arg)
{
    uint8_t element_idx = (uint8_t)arg;

    WICED_BT_TRACE("level timer state:%d remain:%d start:%d target:%d present:%d\n",
            app_state.transition_state, app_state.transition_remaining_time, app_state.start.lightness, app_state.target.lightness, app_state.present.lightness);

    switch (app_state.transition_state)
    {
    case MESH_TRANSITION_STATE_DELAY:
        // if it was a delay before present transition, start transition now
        app_state.transition_state = MESH_TRANSITION_STATE_TRANSITION;

        app_state.transition_remaining_time = app_state.transition_time;
        if (app_state.transition_remaining_time != 0)
        {
            wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);
        }
        else
        {
            app_state.transition_state   = MESH_TRANSITION_STATE_IDLE;
            memcpy(&app_state.present, &app_state.target, sizeof(wiced_bt_mesh_light_xyl_data_t));

            mesh_light_xyl_server_send_status(NULL);
        }
        break;

    case MESH_TRANSITION_STATE_TRANSITION:
        // check if we are done
        if (app_state.transition_remaining_time < MESH_LIGHT_LIGHTNESS_TICK_IN_MS)
        {
            app_state.transition_state          = MESH_TRANSITION_STATE_IDLE;
            app_state.transition_remaining_time = 0;
            memcpy(&app_state.present, &app_state.target, sizeof(wiced_bt_mesh_light_xyl_data_t));

            mesh_light_xyl_server_send_status(NULL);
        }
        else
        {
            // still in the transition, adjust remaining time and start timer for another tick
            app_state.transition_remaining_time -= MESH_LIGHT_LIGHTNESS_TICK_IN_MS;
            wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);

            app_state.present.lightness = app_state.start.lightness +
                    (int16_t)(((int32_t)app_state.target.lightness - app_state.start.lightness) *
                               (int32_t)(app_state.transition_time - app_state.transition_remaining_time) /
                               (int32_t)app_state.transition_time);
        }
        break;
    }
}

/*
 * Send Light xyL status message to the Client
 */
void mesh_light_xyl_server_send_status(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_light_xyl_status_data_t event;

    memcpy(&event.present, &app_state.present, sizeof(wiced_bt_mesh_light_xyl_data_t));

    if (app_state.transition_state == MESH_TRANSITION_STATE_DELAY)
        event.remaining_time = app_state.transition_time + app_state.delay - ((uint32_t)wiced_bt_mesh_core_get_tick_count() - app_state.transition_start_time);
    else
        event.remaining_time = app_state.transition_remaining_time;

    if (p_event == NULL)
    {
        p_event = wiced_bt_mesh_create_event(MESH_LIGHT_XYL_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_XYL_SETUP_SRV, 0, 0);
        if (p_event == NULL)
        {
            WICED_BT_TRACE("light xyl status: no pub\n");
            return;
        }
    }
    wiced_bt_mesh_model_light_xyl_server_send_status(p_event, &event);
}

/*
 * Send Light xyL Default status message to the Client
 */
void mesh_light_xyl_server_send_default_status(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_light_xyl_default_status_data_t event;

    event.default_status.lightness  = app_state.default_value.lightness;
    event.default_status.x          = app_state.default_value.x;
    event.default_status.y          = app_state.default_value.y;
    wiced_bt_mesh_model_light_xyl_server_send_default_status(p_event, &event);
}

/*
* Send Lightness Range status message to the Client
*/
void mesh_light_xyl_server_send_lightness_range_status(wiced_bt_mesh_event_t *p_event, uint8_t status)
{
    wiced_bt_mesh_light_lightness_range_status_data_t event;

    event.status    = status;
    event.min_level = app_state.range_min.lightness;
    event.max_level = app_state.range_max.lightness;

    wiced_bt_mesh_model_light_lightness_server_send_range_status(p_event, &event);
}

/*
 * Send xyL Range status message to the Client
 */
void mesh_light_xyl_server_send_range_status(wiced_bt_mesh_event_t *p_event, uint8_t result)
{
    wiced_bt_mesh_light_xyl_range_status_data_t event;

    event.status = result;
    event.x_min  = app_state.range_min.x;
    event.x_max  = app_state.range_max.x;
    event.y_min  = app_state.range_min.y;
    event.y_max  = app_state.range_max.y;

    wiced_bt_mesh_model_light_xyl_server_send_range_status(p_event, &event);
}

#ifdef HCI_CONTROL
/*
 * Send Level Set event over transport
 */
void mesh_light_xyl_hci_event_send_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_xyl_set_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->target.lightness);
    UINT16_TO_STREAM(p, p_data->target.x);
    UINT16_TO_STREAM(p, p_data->target.y);
    UINT32_TO_STREAM(p, p_data->transition_time);
    UINT16_TO_STREAM(p, p_data->delay);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LIGHT_XYL_SET, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Light Lightness Range Set event over transport
 */
void mesh_light_xyl_hci_event_send_set_lightness_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_range_set_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->min_level);
    UINT16_TO_STREAM(p, p_data->max_level);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LIGHT_LIGHTNESS_RANGE_SET, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Light XYL Temperature Range Set event over transport
 */
void mesh_light_xyl_hci_event_send_set_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_xyl_range_set_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->x_min);
    UINT16_TO_STREAM(p, p_data->x_max);
    UINT16_TO_STREAM(p, p_data->y_min);
    UINT16_TO_STREAM(p, p_data->y_max);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LIGHT_XYL_RANGE_SET, buffer, (uint16_t)(p - buffer));
}
#endif
