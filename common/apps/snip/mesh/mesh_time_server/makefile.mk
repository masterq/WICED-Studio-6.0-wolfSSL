#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
#  Corporation. All rights reserved. This software, including source code, documentation and  related 
# materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
#  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
# (United States and foreign), United States copyright laws and international treaty provisions. 
# Therefore, you may use this Software only as provided in the license agreement accompanying the 
# software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
# hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
# compile the Software source code solely for use in connection with Cypress's  integrated circuit 
# products. Any reproduction, modification, translation, compilation,  or representation of this 
# Software except as specified above is prohibited without the express written permission of 
# Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
# the Software without notice. Cypress does not assume any liability arising out of the application 
# or use of the Software or any product or circuit  described in the Software. Cypress does 
# not authorize its products for use in any products where a malfunction or failure of the 
# Cypress product may reasonably be expected to result  in significant property damage, injury 
# or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
#  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
# to indemnify Cypress against all liability.
#

NAME := mesh_time_server

########################################################################
# Add Application sources here.
########################################################################
APP_SRC = mesh_time_server.c

########################################################################
# Component(s) needed
########################################################################
$(NAME)_COMPONENTS := mesh_app_lib.a
$(NAME)_COMPONENTS += mesh_core_lib.a
$(NAME)_COMPONENTS += mesh_models_lib.a
$(NAME)_COMPONENTS += fw_upgrade_lib.a

ifeq ($(BLD),A_20719B0)
APP_PATCHES_AND_LIBS := multi_adv_patch_lib.a
else ifeq ($(CHIP),20703)
APP_PATCHES_AND_LIBS := rtc_lib.a
endif

########################################################################
# C flags
########################################################################
# define this for 2 chip solution or for testing over WICED HCI
C_FLAGS += -DHCI_CONTROL

ifdef NO_PB_ADV
C_FLAGS += -DNO_PB_ADV=$(NO_PB_ADV)
endif

#value of the FRIEND defines mode. It can be normal node(0), friend(1) or LPN(2)
ifdef FRIEND
C_FLAGS += -DMESH_FRIENDSHIP=$(FRIEND)
else
C_FLAGS += -DMESH_FRIENDSHIP=0
endif

ifdef NO_PB_ADV
C_FLAGS += -DNO_PB_ADV=$(NO_PB_ADV)
endif

C_FLAGS += -DWICED_BT_TRACE_ENABLE

C_FLAGS += -DMESH_CHIP=$(CHIP)
