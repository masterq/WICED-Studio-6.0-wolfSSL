/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 *
 * This file shows how to create a device which implements mesh user time client.
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#include "rtc.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3006
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
        WICED_BT_MESH_DEVICE,
        WICED_BT_MESH_MODEL_TIME_SETUP_SERVER,
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_TIME_SERVER_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .properties_num = 4,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

uint8_t mesh_num_elements = 1;

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};


/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_time_setup_server_message_handler(uint16_t event, void *p_data);
static void mesh_time_get(uint8_t *p_data, uint32_t length);
static void mesh_time_set(uint8_t *p_data, uint32_t length);
static void mesh_time_send_status(uint8_t *p_data, uint32_t length);
static void mesh_time_set_hci_event_send(RtcTime *p_time);

/******************************************************
 *          Variables Definitions
 ******************************************************/
wiced_bool_t auth = WICED_FALSE;
/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init()
{
    // register with the library to receive parsed data
    wiced_bt_mesh_model_time_server_init(mesh_time_setup_server_message_handler);
}

/*
 * Process event received from the time client.
 */
void mesh_time_setup_server_message_handler(uint16_t event, void *p_data)
{
    WICED_BT_TRACE("time srv msg:%d\n", event);

    RtcTime *p_time;
    switch (event)
    {
    case WICED_BT_MESH_TIME_SET:
    {
        p_time = (RtcTime*)p_data;
#if defined HCI_CONTROL
        mesh_time_set_hci_event_send(p_time);
#endif
        break;
    }
    default:
        WICED_BT_TRACE("unknown\n");
    break;
    }
}


/*
 * In 2 chip solutions MCU can send commands to change time state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {

//    case HCI_CONTROL_MESH_COMMAND_TIME_SERVER_SET:
//           mesh_time_set(p_data, length);
//           break;
    case HCI_CONTROL_MESH_COMMAND_TIME_SET:
           mesh_time_send_status(p_data, length);
           break;
    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }

    return 0;
}

/*
 * Create mesh event for unsolicited message and send time set command
 */
/*
void mesh_time_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_time_state_msg_t set_data;
    WICED_BT_TRACE("mesh_time_set\n");

    STREAM_TO_UINT40(set_data.tai_seconds, p_data);
    STREAM_TO_UINT8(set_data.subsecond, p_data);
    STREAM_TO_UINT8(set_data.uncertainty, p_data);

    set_data.time_authority = WICED_TRUE;
    auth = WICED_TRUE;
    STREAM_TO_UINT16(set_data.tai_utc_delta_current, p_data);
    STREAM_TO_UINT8(set_data.time_zone_offset_current, p_data);

    wiced_bt_mesh_model_time_sever_time_set(&set_data);
}
*/

/*
 * Create mesh event for unsolicited message and send time zone set command
 */
void mesh_time_zone_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_time_zone_set_t set_data;
    WICED_BT_TRACE("mesh_time_zone_set\n");

    STREAM_TO_UINT8(set_data.time_zone_offset_new, p_data);
    STREAM_TO_UINT40(set_data.tai_of_zone_change, p_data);
    wiced_bt_mesh_model_time_server_time_zone_set(&set_data);
}

/*
 * Create mesh event for unsolicited message and send time tai_utc delta set command
 */
void mesh_time_tai_utc_delta_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_time_tai_utc_delta_set_t set_data;
    WICED_BT_TRACE("mesh_time_tai_utc_delta_set\n");

    STREAM_TO_UINT16(set_data.tai_utc_delta_new, p_data);
    STREAM_TO_UINT40(set_data.tai_of_delta_change, p_data);

    wiced_bt_mesh_model_time_server_tai_utc_delta_set(&set_data);
}

void mesh_time_send_status(uint8_t *p_data, uint32_t data_len)
{
    wiced_bt_mesh_event_t *p_event;
    wiced_bt_mesh_time_state_msg_t status;
    uint16_t dst, app_key_idx;
    WICED_BT_TRACE("mesh_time_status_send len:%d\n", data_len);

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_TIME_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_TIME_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
       WICED_BT_TRACE("time state changed: no mem\n");
       return;
    }

    if (data_len == 9)
    {
        memset(&status, 0, sizeof(wiced_bt_mesh_time_state_msg_t));
        wiced_bt_mesh_model_time_server_status_send(p_event, &status);
    }
    else
    {
        STREAM_TO_UINT40(status.tai_seconds, p_data);
        STREAM_TO_UINT8(status.subsecond, p_data);
        STREAM_TO_UINT8(status.uncertainty, p_data);
        STREAM_TO_UINT8(status.time_authority, p_data);
        STREAM_TO_UINT16(status.tai_utc_delta_current, p_data);
        STREAM_TO_UINT8(status.time_zone_offset_current, p_data);
        wiced_bt_mesh_model_time_server_status_send(p_event, &status);
    }

}

#ifdef HCI_CONTROL
/*
 * Send Time set event over transport
 */
void mesh_time_set_hci_event_send(RtcTime *p_time)
{
    uint8_t buffer[12];
    uint8_t *p = buffer;

    WICED_BT_TRACE("mesh_time_set_hci_event_send");
    WICED_BT_TRACE("year 0x%02x\n", p_time->year);
    WICED_BT_TRACE("month 0x%02x\n", p_time->month);
    WICED_BT_TRACE("day 0x%02x\n", p_time->day);
    WICED_BT_TRACE("hour 0x%02x\n", p_time->hour);
    WICED_BT_TRACE("min 0x%02x\n", p_time->minute);
    WICED_BT_TRACE("sec 0x%02x\n", p_time->second);

    UINT16_TO_STREAM(p, p_time->year);
    UINT16_TO_STREAM(p, p_time->month);
    UINT16_TO_STREAM(p, p_time->day);
    UINT16_TO_STREAM(p, p_time->hour);
    UINT16_TO_STREAM(p, p_time->minute);
    UINT16_TO_STREAM(p, p_time->second);
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_TIME_SET , buffer, (uint16_t)(p - buffer));

}

#endif

#if MESH_CHIP == 20703
// Core initialization initializes the RTC calling wiced_bt_mesh_core_rtc_init() defined in app. It just should call rtc_init().
// For that chip rtc_init should be called from application. Otherwise there is build error at link phase
void wiced_bt_mesh_core_rtc_init(void)
{
    rtc_init();
}
#endif
