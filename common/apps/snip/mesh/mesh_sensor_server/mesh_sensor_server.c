/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 *
 * This file shows how to create a device which publishes user_property level.
 */
#include "wiced_bt_uuid.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#include "wiced_timer.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

// defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3106
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

#define MESH_TEMP_SENSOR_POSITIVE_TOLERANCE 100
#define MESH_TEMP_SENSOR_NEGATIVE_TOLERANCE 20
#define MESH_TEMP_SENSOR_SAMPLING_FUNCTION  10
#define MESH_TEMP_SENSOR_MEASUREMENT_PERIOD 40
#define MESH_TEMP_SENSOR_UPDATE_INTERVAL    10

#define MESH_TEMP_SENSOR_CADENCE_DEFAULT_MIN_INTERVAL 0x0A
#define TEMPERATURE_SETTING_PROP_ID 0x2AC9


uint8_t mesh_temperature_sensor_val[]          = {0x01, 0x02};
uint8_t mesh_temperature_sensor_setting0_val[] = {0x01, 0x02};

// first set of series and column values
uint8_t raw_valuex_0[]                         = {0x00,0x01};
uint8_t column_width0[]                        = {0x01,0x10};
uint8_t raw_valuey0[]                          = {0x00,0x10};

// second set of series and column values
uint8_t raw_valuex_1[]                         = {0x00,0x10};
uint8_t column_width1[]                        = {0x01,0x10};
uint8_t raw_valuey1[]                          = {0x00,0x20};

// temperature sensor cadence
uint8_t temperature_sensor_trigger_delta_down[]       = {0x01, 0x02};
uint8_t temperature_sensor_trigger_delta_up[]         = {0x01, 0x02};
uint8_t tempertature_sensor_fast_cadence_low[]        = {0x01, 0x02};
uint8_t temperature_sensor_fast_cadence_high[]        = {0x01, 0x02};


wiced_bt_mesh_core_config_model_t mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_SENSOR_SETUP_SERVER,
};

#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_APP_NUM_SENSORS 1

wiced_bt_mesh_sensor_config_setting_t sensor_settings[] =
{
    {
        .setting_property_id = TEMPERATURE_SETTING_PROP_ID,
        .access = WICED_BT_MESH_SENSOR_SETTING_READABLE_AND_WRITABLE,
        .value_len = 2,
        mesh_temperature_sensor_setting0_val
    },
};


wiced_bt_mesh_sensor_config_column_data_t   mesh_temperature_sensor_columns[] =
{
    { raw_valuex_0, column_width0, raw_valuey0 },
    { raw_valuex_1, column_width1, raw_valuey1 },
};

wiced_bt_mesh_core_config_sensor_t mesh_element1_sensors[] =
{
    {
        UUID_CHARACTERISTIC_TEMPERATURE_MEASUREMENT,
          sizeof(uint16_t),
         // descriptors
        {
            MESH_TEMP_SENSOR_POSITIVE_TOLERANCE,
            MESH_TEMP_SENSOR_NEGATIVE_TOLERANCE,
            MESH_TEMP_SENSOR_SAMPLING_FUNCTION,
            MESH_TEMP_SENSOR_MEASUREMENT_PERIOD,
            MESH_TEMP_SENSOR_UPDATE_INTERVAL
        },
        //data
        mesh_temperature_sensor_val,
        //cadence
        {
            .fast_cadence_period_divisor = 0x01,
            .trigger_type  = WICED_TRUE,
            temperature_sensor_trigger_delta_down,
            temperature_sensor_trigger_delta_up,
            .min_interval = 0x07,
            tempertature_sensor_fast_cadence_low,
            temperature_sensor_fast_cadence_high
        },
        // num of series
        2,
        mesh_temperature_sensor_columns,
        // num of settings
        1,
        sensor_settings,
    },
};


#define MESH_APP_NUM_PROPERTIES (sizeof(mesh_element1_properties) / sizeof(wiced_bt_mesh_core_config_property_t))

#define MESH_SENSOR_SERVER_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,    // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,    // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,    // Default element behavior on power up
        .properties_num = 0,                             // Number of properties in the array models
        .properties = NULL,                                   // Array of properties in the element.
        .sensors_num = 1,                                // Number of properties in the array models
        .sensors = mesh_element1_sensors,                     // Array of properties in the element.
        .models_num = MESH_APP_NUM_MODELS,                   // Number of models in the array models
        .models = mesh_element1_models,    // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};

/******************************************************
 *          Structures
 ******************************************************/



/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_sensor_server_message_handler(uint16_t event,
        wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_sensor_server_setting_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_sensor_server_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_sensor_server_column_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_sensor_server_series_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_sensor_server_cadenence_status_changed(uint8_t *p_data, uint32_t length);

static void mesh_sensor_server_send_cadence_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id);
static void mesh_sensor_server_send_setting_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id, uint16_t setting_property_id);
static void mesh_sensor_server_process_cadence_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_set_data_t *p_data);
static void mesh_sensor_server_process_setting_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_set_data_t *p_data);
static void mesh_sensor_server_send_column_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_column_get_data_t *p_get_column);
static void mesh_sensor_server_send_series_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_series_get_data_t* data);
static void mesh_sensor_server_send_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id);

#ifdef HCI_CONTROL
static void mesh_sensor_hci_event_send_cadence_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_set_data_t *p_set);
static void mesh_sensor_hci_event_send_setting_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_set_data_t *p_set);
#endif

extern wiced_bt_mesh_core_config_t mesh_config;

/******************************************************
 *          Variables Definitions
 ******************************************************/

 /******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    wiced_bt_mesh_model_sensor_server_init(MESH_SENSOR_SERVER_ELEMENT_INDEX, mesh_sensor_server_message_handler, is_provisioned);
}

/*
 * Process event received from the Property Client.
 */
void mesh_sensor_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    WICED_BT_TRACE("mesh_sensor_server_message_handler msg: %d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_SENSOR_GET:
        mesh_sensor_server_send_status(wiced_bt_mesh_create_reply_event(p_event), ((wiced_bt_mesh_sensor_get_t *)p_data)->property_id);
        break;

    case WICED_BT_MESH_SENSOR_COLUMN_GET:
        mesh_sensor_server_send_column_status(wiced_bt_mesh_create_reply_event(p_event), (wiced_bt_mesh_sensor_column_get_data_t *)p_data);
        break;

    case WICED_BT_MESH_SENSOR_SERIES_GET:
        mesh_sensor_server_send_series_status(wiced_bt_mesh_create_reply_event(p_event), (wiced_bt_mesh_sensor_series_get_data_t *)p_data);
        break;

    case WICED_BT_MESH_SENSOR_CADENCE_GET:
        mesh_sensor_server_send_cadence_status(wiced_bt_mesh_create_reply_event(p_event), ((wiced_bt_mesh_sensor_get_t *)p_data)->property_id);
        break;

    case WICED_BT_MESH_SENSOR_SETTING_GET:
        mesh_sensor_server_send_setting_status(wiced_bt_mesh_create_reply_event(p_event), 
            ((wiced_bt_mesh_sensor_setting_get_data_t *)p_data)->property_id, ((wiced_bt_mesh_sensor_setting_get_data_t *)p_data)->setting_property_id);
        break;

    case WICED_BT_MESH_SENSOR_CADENCE_SET:
#if defined HCI_CONTROL
        mesh_sensor_hci_event_send_cadence_set(p_event, (wiced_bt_mesh_sensor_cadence_set_data_t *)p_data);
#endif
        mesh_sensor_server_process_cadence_set(p_event, (wiced_bt_mesh_sensor_cadence_set_data_t *)p_data);
        break;

    case WICED_BT_MESH_SENSOR_SETTING_SET:
#if defined HCI_CONTROL
        mesh_sensor_hci_event_send_setting_set(p_event, (wiced_bt_mesh_sensor_setting_set_data_t *)p_data);
#endif
        mesh_sensor_server_process_setting_set(p_event, (wiced_bt_mesh_sensor_setting_set_data_t *)p_data);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        wiced_bt_mesh_release_event(p_event);
        break;
    }
}

/*
 * In 2 chip solutions MCU can send commands to change user_property state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    uint16_t dst         = p_data[0] + (p_data[1] << 8);
    uint16_t app_key_idx = p_data[2] + (p_data[3] << 8);

    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {

        case HCI_CONTROL_MESH_COMMAND_SENSOR_CADENCE_SET:
            mesh_sensor_server_cadenence_status_changed(p_data, length);
            break;

        case HCI_CONTROL_MESH_COMMAND_SENSOR_SETTING_SET:
            mesh_sensor_server_setting_status_changed(p_data, length);
            break;

        case HCI_CONTROL_MESH_COMMAND_SENSOR_SET:
            mesh_sensor_server_status_changed(p_data, length);
            break;

        case HCI_CONTROL_MESH_COMMAND_SENSOR_COLUMN_SET:
            mesh_sensor_server_column_status_changed(p_data, length);
            break;

        case HCI_CONTROL_MESH_COMMAND_SENSOR_SERIES_SET:
            mesh_sensor_server_series_status_changed(p_data, length);
            break;

        default:
            WICED_BT_TRACE("unknown\n");
            break;
    }
    return 0;
}

/*
 * Send Sensor Cadence Status message to the Sensor Client
 */
void mesh_sensor_server_send_cadence_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id)
{
    int i;
    wiced_bt_mesh_sensor_cadence_status_data_t event;

    event.property_id = property_id;

    WICED_BT_TRACE("%s\n", __FUNCTION__);

    //search for property_id
    for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == property_id)
        {
            event.is_data_present = WICED_TRUE;
            event.prop_value_len =mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len;

            event.cadence_data.fast_cadence_period_divisor   = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_period_divisor;
            event.cadence_data.trigger_type   = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_type;
            if (event.cadence_data.trigger_type)
            {
                memcpy (event.cadence_data.trigger_delta_up, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_up, sizeof(uint16_t));
                memcpy (event.cadence_data.trigger_delta_down, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_down, sizeof(uint16_t));
            }
            else
            {
                memcpy (event.cadence_data.trigger_delta_up, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_up, event.prop_value_len);
                memcpy (event.cadence_data.trigger_delta_down, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_down, event.prop_value_len);
            }
            memcpy (event.cadence_data.fast_cadence_high, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_high, event.prop_value_len);
            memcpy (event.cadence_data.fast_cadence_low, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_low, event.prop_value_len);

            event.cadence_data.min_interval =  mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.min_interval;

            wiced_bt_mesh_model_sensor_server_cadence_status_send(p_event, &event);
            break;
        }
    }
}


/*
 * Send Sensor Series Status message to the Sensor Client
 */
void mesh_sensor_server_send_series_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_series_get_data_t* data)
{
    uint8_t i,j,k=0;
    wiced_bt_mesh_sensor_series_status_data_t series_data;
    uint8_t end_idx;
    wiced_bool_t copy_flag = WICED_FALSE;

    series_data.property_id = data->property_id;

    for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == data->property_id)
        {
            series_data.prop_value_len = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len;

            end_idx = (data->end_index > mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].num_series) ?
                            mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].num_series : data->end_index;
            for (j = data->start_index; j < end_idx; j++, k++)
            {
                memcpy(series_data.column_list[k].raw_valuex, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].raw_valuex, data->prop_value_len);
                memcpy(series_data.column_list[k].raw_valuey, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].raw_valuey, data->prop_value_len);
                memcpy(series_data.column_list[k].column_width, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].column_width, data->prop_value_len);
            }
            series_data.no_of_columns = k;
            wiced_bt_mesh_model_sensor_server_series_status_send(p_event, &series_data);
            return;
        }
    }
}

/*
 * Send Sensor Setting Status message to the Sensor Client
 */
void mesh_sensor_server_send_setting_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id, uint16_t setting_property_id)
{
    uint8_t i, j;
    wiced_bt_mesh_sensor_setting_status_data_t event;

    event.property_id = property_id;
    WICED_BT_TRACE("%s\n", __FUNCTION__);

    //search for property_id
    for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == property_id)
        {
            for (j = 0; j < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].num_settings; j++)
            {
                if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].setting_property_id == setting_property_id)
                {
                    event.setting.access = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].access;
                    event.setting.value_len = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].value_len;
                    event.setting.setting_property_id = setting_property_id;
                    memcpy (event.setting.val, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].val, event.setting.value_len);

                    wiced_bt_mesh_model_sensor_server_setting_status_send(p_event, &event);
                    return;
                }
            }
        }
    }
}

/*
 * Process cadence set and send Status message to the Sensor Client
 */
void mesh_sensor_server_process_cadence_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_set_data_t *p_data)
{

    uint8_t i, j;
    WICED_BT_TRACE("%s\n", __FUNCTION__);
    for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == p_data->property_id)
        {
            mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_period_divisor =
                            p_data->cadence_data.fast_cadence_period_divisor;
            mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_type = p_data->cadence_data.trigger_type;

            if (p_data->cadence_data.trigger_type)
            {
                memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_up, p_data->cadence_data.trigger_delta_up, sizeof(uint16_t));
                memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_down, p_data->cadence_data.trigger_delta_down, sizeof(uint16_t));
            }
            else
            {
                memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_up, p_data->cadence_data.trigger_delta_up, p_data->prop_value_len);
                memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_down, p_data->cadence_data.trigger_delta_down, p_data->prop_value_len);
            }

            memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_high, p_data->cadence_data.fast_cadence_high, p_data->prop_value_len);
            memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_low, p_data->cadence_data.fast_cadence_low,  p_data->prop_value_len);
            mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.min_interval = p_data->cadence_data.min_interval;

            WICED_BT_TRACE("fast cadence period divisor %x\n",mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_period_divisor);
            WICED_BT_TRACE("trigger_type %x\n",mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_type);
            WICED_BT_TRACE("trigger_delta_up %4x\n",mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_up);
            WICED_BT_TRACE("trigger_delta_down %4x\n",mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.trigger_delta_down);
            WICED_BT_TRACE("min_interval %x\n",mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.min_interval);
            WICED_BT_TRACE("\nfast_cadence_high\n");
            for (j = 0; j < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len; j++)
            {
                WICED_BT_TRACE(" %x ", mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_high[j]);
            }
            WICED_BT_TRACE("\nfast_cadence_low\n");
            for (j = 0; j < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len; j++)
            {
                WICED_BT_TRACE(" %x ", mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].cadence.fast_cadence_low[j]);
            }
            if (p_event->reply)
            {
                mesh_sensor_server_send_cadence_status(wiced_bt_mesh_create_reply_event(p_event), p_data->property_id);
                return;
            }
            else
            {
                break;
            }
        }
    }
    wiced_bt_mesh_release_event(p_event);
}

/*
 * Process setting set and send Status message to the Sensor Client
 */
void mesh_sensor_server_process_setting_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_set_data_t *p_data)
{
    uint8_t i, j;
    WICED_BT_TRACE("%s\n", __FUNCTION__);

    //search for property_id
    for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == p_data->property_id)
        {
            WICED_BT_TRACE("\nproperty id found\n");
            for (j = 0; j < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].num_settings; i++)
            {
                if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].setting_property_id ==  p_data->setting_property_id)
                {
                    WICED_BT_TRACE("\n setting property id found\n");
                    mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].value_len =  p_data->prop_value_len;

                    memcpy (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].settings[j].val,  p_data->setting_raw_val,  p_data->prop_value_len);

                    if (p_event->reply)
                    {
                        mesh_sensor_server_send_setting_status(wiced_bt_mesh_create_reply_event(p_event),  p_data->property_id,  p_data->setting_property_id);
                        return;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }
    wiced_bt_mesh_release_event(p_event);
}

/*
 * Send Sensor Column Status message to the Sensor Client
 */
void mesh_sensor_server_send_column_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_column_get_data_t *p_get_column)
{
    wiced_bt_mesh_sensor_column_status_data_t event;
    uint8_t num_sensor = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num;
    uint16_t property_id;
    uint16_t prop_val_len;
    uint8_t i;
    uint8_t j;

    WICED_BT_TRACE("%s\n", __FUNCTION__);

    for (i = 0; i < num_sensor; i++)
    {
        if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == p_get_column->property_id)
        {
            property_id  = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id;
            prop_val_len = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len;
            for (j = 0; j < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].num_series; j++)
            {
                if (memcmp(p_get_column->raw_valuex, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].raw_valuex, prop_val_len) == 0)
                {
                    event.property_id = property_id;
                    event.prop_value_len = prop_val_len;
                    event.is_column_present = WICED_TRUE;
                    memcpy(event.column_data.raw_valuex, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].raw_valuex, prop_val_len);
                    memcpy(event.column_data.raw_valuey, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].raw_valuey, prop_val_len);
                    memcpy(event.column_data.column_width, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].series_columns[j].column_width, prop_val_len);
                }
            }
        }
    }
    wiced_bt_mesh_model_sensor_server_column_status_send(p_event, &event);
}

/*
 * Send Sensor Status message to the Sensor Client
 */
void mesh_sensor_server_send_status(wiced_bt_mesh_event_t *p_event, uint16_t property_id)
{
    int i;
    wiced_bt_mesh_sensor_status_data_t event;
    event.num_status = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num;

    WICED_BT_TRACE("%s\n", __FUNCTION__);

    // if property id is zero send status for all
    if (property_id == 0x00)
    {
        // send all the status
        for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
        {
            event.status_list[i].property_id = property_id;
            event.status_list[i].prop_value_len = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len;
            memcpy(event.status_list[i].raw_value, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].data ,event.status_list[0].prop_value_len);
            break;
        }
    }
    else
    {
        // search for requested property_id
        for (i = 0; i < mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors_num; i++)
        {
            if (mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].property_id == property_id)
            {
                event.num_status = 1;
                event.status_list[0].property_id = property_id;
                event.status_list[0].prop_value_len = mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].prop_value_len;
                memcpy(event.status_list[0].raw_value, mesh_config.elements[MESH_SENSOR_SERVER_ELEMENT_INDEX].sensors[i].data ,event.status_list[0].prop_value_len);
                break;
            }
        }
    }
    wiced_bt_mesh_model_sensor_server_status_send(p_event, &event);
}
/*
 * Create mesh event for unsolicited message and send Cadence Status event
 */
void mesh_sensor_server_cadenence_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_sensor_cadence_status_data_t status;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint8_t flag_data_present;
    uint8_t flag_trigger_type;

    WICED_BT_TRACE("mesh_sensor_server_cadenence_status:\n");

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);


    STREAM_TO_UINT16(status.property_id, p_data);
    STREAM_TO_UINT8(flag_data_present, p_data);
    status.is_data_present = flag_data_present > 0 ? WICED_TRUE : WICED_FALSE;
    STREAM_TO_UINT8(status.prop_value_len, p_data);

    if (status.is_data_present)
    {
        STREAM_TO_UINT8(status.cadence_data.fast_cadence_period_divisor, p_data);
        STREAM_TO_UINT8(flag_trigger_type, p_data);
        status.cadence_data.trigger_type = flag_trigger_type ? WICED_TRUE : WICED_FALSE;
        if (status.cadence_data.trigger_type)
        {
            STREAM_TO_ARRAY(status.cadence_data.trigger_delta_down, p_data, status.prop_value_len);
            STREAM_TO_ARRAY(status.cadence_data.trigger_delta_up, p_data, status.prop_value_len);
        }
        else
        {
            STREAM_TO_ARRAY(status.cadence_data.trigger_delta_down, p_data, sizeof(uint16_t));
            STREAM_TO_ARRAY(status.cadence_data.trigger_delta_down, p_data, sizeof(uint16_t));
        }

        STREAM_TO_UINT8(status.cadence_data.min_interval, p_data);
        STREAM_TO_ARRAY(status.cadence_data.fast_cadence_low, p_data, status.prop_value_len);
        STREAM_TO_ARRAY(status.cadence_data.fast_cadence_high, p_data, status.prop_value_len);
    }

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor cadence state changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_server_cadence_status_send(p_event, &status);

}


/*
 * Create mesh event for unsolicited message and send Setting Status event
 */
void mesh_sensor_server_setting_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_sensor_setting_status_data_t setting_status;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    WICED_BT_TRACE("mesh_sensor_server_status_changed:\n");

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);


    STREAM_TO_UINT16(setting_status.property_id, p_data);
    STREAM_TO_UINT16(setting_status.setting.setting_property_id, p_data);
    STREAM_TO_UINT8(setting_status.setting.access, p_data);
    STREAM_TO_UINT8(setting_status.setting.value_len, p_data);
    STREAM_TO_ARRAY(setting_status.setting.val, p_data, setting_status.setting.value_len);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor setting state changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_server_setting_status_send(p_event, &setting_status);

}

/*
 * Create mesh event for unsolicited message and send Sensor Status event
 */
void mesh_sensor_server_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_sensor_status_data_t status;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint8_t i;

    WICED_BT_TRACE("mesh_sensor_server_status_changed: \n");

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT8(status.num_status, p_data);

    for(i=0; i<status.num_status; i++)
    {
        STREAM_TO_UINT16(status.status_list[i].property_id, p_data);
        STREAM_TO_UINT16(status.status_list[i].prop_value_len, p_data);
        STREAM_TO_ARRAY(status.status_list[i].raw_value, p_data, status.status_list[i].prop_value_len);
    }

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor status changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_server_status_send(p_event, &status);

}

/*
 * Create mesh event for unsolicited message and send Sensor column status event
 */
void mesh_sensor_server_column_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_sensor_column_status_data_t column_status;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    WICED_BT_TRACE("mesh_sensor_server_column_status_changed: \n");

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(column_status.property_id, p_data);
    STREAM_TO_UINT8(column_status.prop_value_len, p_data);

    STREAM_TO_ARRAY(column_status.column_data.raw_valuex,p_data, column_status.prop_value_len);
    STREAM_TO_ARRAY(column_status.column_data.column_width,p_data, column_status.prop_value_len);
    STREAM_TO_ARRAY(column_status.column_data.raw_valuey,p_data, column_status.prop_value_len);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor column status changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_server_column_status_send(p_event, &column_status);

}

/*
 * Create mesh event for unsolicited message and send Sensor series status event
 */
void mesh_sensor_server_series_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_sensor_series_status_data_t status;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint8_t i;

    WICED_BT_TRACE("mesh_sensor_server_status_changed: \n");

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT8(status.property_id, p_data);
    STREAM_TO_UINT8(status.prop_value_len, p_data);
    STREAM_TO_UINT8(status.no_of_columns, p_data);

    for(i=0; i<status.no_of_columns; i++)
    {
        STREAM_TO_ARRAY(status.column_list[i].raw_valuex, p_data, status.prop_value_len);
        STREAM_TO_ARRAY(status.column_list[i].column_width, p_data, status.prop_value_len);
        STREAM_TO_ARRAY(status.column_list[i].raw_valuey, p_data, status.prop_value_len);
    }

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor status changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_server_series_status_send(p_event, &status);
}

#ifdef HCI_CONTROL
/*
 * Send Sensor Cadence Set event over transport
 */
void mesh_sensor_hci_event_send_cadence_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_set_data_t *p_set)
{
    WICED_BT_TRACE("mesh_sensor_hci_event_send_cadence_set: \n");
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    uint8_t flag_trigger_type;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_set->property_id);
    UINT8_TO_STREAM(p, p_set->prop_value_len);
    UINT8_TO_STREAM(p, p_set->cadence_data.fast_cadence_period_divisor);

    flag_trigger_type = p_set->cadence_data.trigger_type ? 0x01 : 0x00;

    UINT8_TO_STREAM(p, flag_trigger_type);

    if(p_set->cadence_data.trigger_type)
    {
       ARRAY_TO_STREAM(p, p_set->cadence_data.trigger_delta_down, p_set->prop_value_len);
       ARRAY_TO_STREAM(p, p_set->cadence_data.trigger_delta_up, p_set->prop_value_len);
    }
    else
    {
        ARRAY_TO_STREAM(p, p_set->cadence_data.trigger_delta_down, sizeof(uint16_t));
        ARRAY_TO_STREAM(p, p_set->cadence_data.trigger_delta_up, sizeof(uint16_t));
    }

    UINT8_TO_STREAM(p, p_set->cadence_data.min_interval);

    ARRAY_TO_STREAM(p, p_set->cadence_data.fast_cadence_low, p_set->prop_value_len);
    ARRAY_TO_STREAM(p, p_set->cadence_data.fast_cadence_high, p_set->prop_value_len);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_CADENCE_SET, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Sensor Setting Set event over transport
 */
void mesh_sensor_hci_event_send_setting_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_set_data_t *p_set)
{
    WICED_BT_TRACE("mesh_sensor_hci_event_send_setting_get: \n");
    uint8_t buffer[40];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_set->property_id);
    UINT16_TO_STREAM(p, p_set->setting_property_id);
    UINT8_TO_STREAM(p, p_set->prop_value_len);
    ARRAY_TO_STREAM(p, p_set->setting_raw_val, p_set->prop_value_len);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_SETTING_SET, buffer, (uint16_t)(p - buffer));
}

#endif
