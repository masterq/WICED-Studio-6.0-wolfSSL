/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * This file shows how to create a device which implements mesh provisioner client.
 * The main purpose of the app is to process messages coming from the MCU and call Mesh Core
 * Library to perform functionality.
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_core.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_mesh_provision.h"
#include "wiced_bt_trace.h"
#include "wiced_transport.h"
#include "hci_control_api.h"

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3006
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_provisioner_dev_name[]       = "Provisioner Client";
uint8_t mesh_provisioner_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_provisioner_mfr_name[]       = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_provisioner_model_num[]      = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_provisioner_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };


wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_CONFIG_CLIENT,
    WICED_BT_MESH_MODEL_HEALTH_CLIENT,
    WICED_BT_MESH_MODEL_PROXY_CLIENT
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_PROVISIONER_CLIENT_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 0,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = 0,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 50                                 // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    .features                  = 0,                                        // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window        = 0,                                 // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len         = 0                                  // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};

/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_provisioner_client_message_handler(uint16_t event, void *p_data);
static uint8_t mesh_provisioner_process_set_local_device(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_set_dev_key(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_scan_unprovisioned_devices(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_connect(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_disconnect(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_start(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_oob_value(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_search_proxy(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_proxy_connect(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_proxy_disconnect(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_node_reset(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_beacon_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_beacon_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_composition_data_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_default_ttl_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_default_ttl_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_gatt_proxy_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_gatt_proxy_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_relay_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_relay_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_friend_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_friend_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_key_refresh_phase_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_key_refresh_phase_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_node_identity_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_node_identity_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_publication_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_publication_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_subscription_change(uint8_t opcode, uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_subscription_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_netkey_change(uint8_t operation, uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_netkey_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_appkey_change(uint8_t operation, uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_appkey_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_app_change(uint8_t operation, uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_model_app_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_heartbeat_subscription_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_heartbeat_subscription_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_heartbeat_publication_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_heartbeat_publication_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_network_transmit_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_fault_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_fault_clear(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_fault_test(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_period_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_period_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_attention_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_health_attention_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_lpn_poll_timeout_get(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_network_transmit_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_proxy_filter_type_set(uint8_t *p_data, uint32_t length);
static uint8_t mesh_provisioner_process_proxy_filter_change(wiced_bool_t is_add, uint8_t *p_data, uint32_t length);
static void mesh_provisioner_hci_event_provision_end_send(wiced_bt_mesh_provision_status_data_t *p_data);
void mesh_provisioner_hci_event_unprovisioned_device_send(wiced_bt_mesh_provision_unprovisioned_device_data_t *p_data);
static void mesh_provisioner_hci_event_device_capabilities_send(wiced_bt_mesh_provision_device_capabilities_data_t *p_data);
static void mesh_provisioner_hci_event_device_get_oob_data_send(wiced_bt_mesh_provision_device_oob_request_data_t *p_data);
static void mesh_provisioner_hci_event_node_reset_status_send(wiced_bt_mesh_config_node_reset_status_data_t *p_data);
static void mesh_provisioner_hci_event_node_identity_status_send(wiced_bt_mesh_config_node_identity_status_data_t *p_data);
static void mesh_provisioner_hci_event_composition_data_status_send(wiced_bt_mesh_config_composition_data_status_data_t *p_data);
static void mesh_provisioner_hci_event_friend_status_send(wiced_bt_mesh_config_friend_status_data_t *p_data);
static void mesh_provisioner_hci_event_default_ttl_status_send(wiced_bt_mesh_config_default_ttl_status_data_t *p_data);
static void mesh_provisioner_hci_event_relay_status_send(wiced_bt_mesh_config_relay_status_data_t *p_data);
static void mesh_provisioner_hci_event_gatt_proxy_status_send(wiced_bt_mesh_config_gatt_proxy_status_data_t *p_data);
static void mesh_provisioner_hci_event_beacon_status_send(wiced_bt_mesh_config_beacon_status_data_t *p_data);
static void mesh_provisioner_hci_event_model_publication_status_send(wiced_bt_mesh_config_model_publication_status_data_t *p_data);
static void mesh_provisioner_hci_event_model_subscription_status_send(wiced_bt_mesh_config_model_subscription_status_data_t *p_data);
static void mesh_provisioner_hci_event_model_subscription_list_send(wiced_bt_mesh_config_model_subscription_list_data_t *p_data);
static void mesh_provisioner_hci_event_netkey_status_send(wiced_bt_mesh_config_netkey_status_data_t *p_data);
static void mesh_provisioner_hci_event_netkey_list_send(wiced_bt_mesh_config_netkey_list_data_t *p_data);
static void mesh_provisioner_hci_event_appkey_status_send(wiced_bt_mesh_config_appkey_status_data_t *p_data);
static void mesh_provisioner_hci_event_appkey_list_send(wiced_bt_mesh_config_appkey_list_data_t *p_data);
static void mesh_provisioner_hci_event_model_app_status_send(wiced_bt_mesh_config_model_app_status_data_t *p_data);
static void mesh_provisioner_hci_event_model_app_list_send(wiced_bt_mesh_config_model_app_list_data_t *p_data);
static void mesh_provisioner_hci_event_hearbeat_subscription_status_send(wiced_bt_mesh_config_heartbeat_subscription_status_data_t *p_data);
static void mesh_provisioner_hci_event_hearbeat_publication_status_send(wiced_bt_mesh_config_heartbeat_publication_status_data_t *p_data);
static void mesh_provisioner_hci_event_network_transmit_status_send(wiced_bt_mesh_config_network_transmit_status_data_t *p_data);
static void mesh_provisioner_hci_event_health_current_status_send(wiced_bt_mesh_health_fault_status_data_t *p_data);
static void mesh_provisioner_hci_event_health_fault_status_send(wiced_bt_mesh_health_fault_status_data_t *p_data);
static void mesh_provisioner_hci_event_health_period_status_send(wiced_bt_mesh_health_period_status_data_t *p_data);
static void mesh_provisioner_hci_event_health_attention_status_send(wiced_bt_mesh_health_attention_status_data_t *p_data);
static void mesh_provisioner_hci_event_lpn_poll_timeout_status_send(wiced_bt_mesh_lpn_poll_timeout_status_data_t *p_data);
static void mesh_provisioner_hci_event_proxy_filter_status_send(wiced_bt_mesh_proxy_filter_status_data_t *p_data);

static void mesh_provisioner_hci_send_status(uint8_t status);

/******************************************************
 *          Variables Definitions
 ******************************************************/

// Use hardcoded PTS default priv key. In real app it will be generated once and written into OTP memory
uint8_t pb_priv_key[WICED_BT_MESH_PROVISION_PRIV_KEY_LEN] = { 0x52, 0x9A, 0xA0, 0x67, 0x0D, 0x72, 0xCD, 0x64, 0x97, 0x50, 0x2E, 0xD4, 0x73, 0x50, 0x2B, 0x03, 0x7E, 0x88, 0x03, 0xB5, 0xC6, 0x08, 0x29, 0xA5, 0xA3, 0xCA, 0xA2, 0x19, 0x50, 0x55, 0x30, 0xBA };

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    wiced_bt_mesh_provision_client_init(pb_priv_key, mesh_provisioner_client_message_handler);

    wiced_bt_mesh_config_client_init(mesh_provisioner_client_message_handler);

    wiced_bt_mesh_health_client_init(mesh_provisioner_client_message_handler);

    wiced_bt_mesh_proxy_client_init(mesh_provisioner_client_message_handler);

    mesh_gatt_client_init();
    // initialize provisioning layer
    {
        // Now we can initialize provisioning layer
//            wiced_bt_mesh_provision_started, wiced_bt_mesh_provision_end,
//            wiced_bt_mesh_provision_on_capabilities, wiced_bt_mesh_provision_get_capabilities,
//            wiced_bt_mesh_provision_get_oob, pb_gatt_send_cb);
    }
}

/*
 * Process event received from the provisioner Server.
 */
void mesh_provisioner_client_message_handler(uint16_t event, void *p_data)
{
    WICED_BT_TRACE("provisioner clt msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_UNPROVISIONED_DEVICE:
        mesh_provisioner_hci_event_unprovisioned_device_send((wiced_bt_mesh_provision_unprovisioned_device_data_t *)p_data);
        break;

    case WICED_BT_MESH_PROVISION_END:
        mesh_provisioner_hci_event_provision_end_send((wiced_bt_mesh_provision_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_PROVISION_DEVICE_CAPABILITIES:
        mesh_provisioner_hci_event_device_capabilities_send((wiced_bt_mesh_provision_device_capabilities_data_t *)p_data);
        break;

    case WICED_BT_MESH_PROVISION_GET_OOB_DATA:
        mesh_provisioner_hci_event_device_get_oob_data_send((wiced_bt_mesh_provision_device_oob_request_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_NODE_RESET_STATUS:
        mesh_provisioner_hci_event_node_reset_status_send((wiced_bt_mesh_config_node_reset_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_COMPOSITION_DATA_STATUS:
        mesh_provisioner_hci_event_composition_data_status_send((wiced_bt_mesh_config_composition_data_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_FRIEND_STATUS:
        mesh_provisioner_hci_event_friend_status_send((wiced_bt_mesh_config_friend_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_GATT_PROXY_STATUS:
        mesh_provisioner_hci_event_gatt_proxy_status_send((wiced_bt_mesh_config_gatt_proxy_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_RELAY_STATUS:
        mesh_provisioner_hci_event_relay_status_send((wiced_bt_mesh_config_relay_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_BEACON_STATUS:
        mesh_provisioner_hci_event_beacon_status_send((wiced_bt_mesh_config_beacon_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_DEFAULT_TTL_STATUS:
        mesh_provisioner_hci_event_default_ttl_status_send((wiced_bt_mesh_config_default_ttl_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_NODE_IDENTITY_STATUS:
        mesh_provisioner_hci_event_node_identity_status_send((wiced_bt_mesh_config_node_identity_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_MODEL_PUBLICATION_STATUS:
        mesh_provisioner_hci_event_model_publication_status_send((wiced_bt_mesh_config_model_publication_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_MODEL_SUBSCRIPTION_STATUS:
        mesh_provisioner_hci_event_model_subscription_status_send((wiced_bt_mesh_config_model_subscription_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_MODEL_SUBSCRIPTION_LIST:
        mesh_provisioner_hci_event_model_subscription_list_send((wiced_bt_mesh_config_model_subscription_list_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_NETKEY_STATUS:
        mesh_provisioner_hci_event_netkey_status_send((wiced_bt_mesh_config_netkey_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_NETKEY_LIST:
        mesh_provisioner_hci_event_netkey_list_send((wiced_bt_mesh_config_netkey_list_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_APPKEY_STATUS:
        mesh_provisioner_hci_event_appkey_status_send((wiced_bt_mesh_config_appkey_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_APPKEY_LIST:
        mesh_provisioner_hci_event_appkey_list_send((wiced_bt_mesh_config_appkey_list_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_MODEL_APP_STATUS:
        mesh_provisioner_hci_event_model_app_status_send((wiced_bt_mesh_config_model_app_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_MODEL_APP_LIST:
        mesh_provisioner_hci_event_model_app_list_send((wiced_bt_mesh_config_model_app_list_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_HEARBEAT_SUBSCRIPTION_STATUS:
        mesh_provisioner_hci_event_hearbeat_subscription_status_send((wiced_bt_mesh_config_heartbeat_subscription_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_HEARBEAT_PUBLICATION_STATUS:
        mesh_provisioner_hci_event_hearbeat_publication_status_send((wiced_bt_mesh_config_heartbeat_publication_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_NETWORK_TRANSMIT_STATUS:
        mesh_provisioner_hci_event_network_transmit_status_send((wiced_bt_mesh_config_network_transmit_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_CONFIG_LPN_POLL_TIMEOUT_STATUS:
        mesh_provisioner_hci_event_lpn_poll_timeout_status_send((wiced_bt_mesh_lpn_poll_timeout_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_HEALTH_CURRENT_STATUS:
        mesh_provisioner_hci_event_health_current_status_send((wiced_bt_mesh_health_fault_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_HEALTH_FAULT_STATUS:
        mesh_provisioner_hci_event_health_fault_status_send((wiced_bt_mesh_health_fault_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_HEALTH_PERIOD_STATUS:
        mesh_provisioner_hci_event_health_period_status_send((wiced_bt_mesh_health_period_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_HEALTH_ATTENTION_STATUS:
        mesh_provisioner_hci_event_health_attention_status_send((wiced_bt_mesh_health_attention_status_data_t *)p_data);
        break;

    case WICED_BT_MESH_PROXY_FILTER_STATUS:
        mesh_provisioner_hci_event_proxy_filter_status_send((wiced_bt_mesh_proxy_filter_status_data_t *)p_data);
        break;

    default:
        break;
    }
}


/*
 * In 2 chip solutions MCU can send commands to change provisioner state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    uint8_t status;

    WICED_BT_TRACE("%s opcode:%x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_SET_LOCAL_DEVICE:
        status = mesh_provisioner_process_set_local_device(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SET_DEVICE_KEY:
        status = mesh_provisioner_process_set_dev_key(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SCAN_UNPROVISIONED:
        status = mesh_provisioner_process_scan_unprovisioned_devices(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROVISION_CONNECT:
        status = mesh_provisioner_process_connect(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROVISION_DISCONNECT:
        status = mesh_provisioner_process_disconnect(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROVISION_START:
        status = mesh_provisioner_process_start(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROVISION_OOB_VALUE:
        status = mesh_provisioner_process_oob_value(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SEARCH_PROXY:
        status = mesh_provisioner_process_search_proxy(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROXY_CONNECT:
        status = mesh_provisioner_process_proxy_connect(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROXY_DISCONNECT:
        status = mesh_provisioner_process_proxy_disconnect(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_RESET:
        status = mesh_provisioner_process_node_reset(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_BEACON_GET:
        status = mesh_provisioner_process_beacon_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_BEACON_SET:
        status = mesh_provisioner_process_beacon_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_COMPOSITION_DATA_GET:
        status = mesh_provisioner_process_composition_data_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_DEFAULT_TTL_GET:
        status = mesh_provisioner_process_default_ttl_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_DEFAULT_TTL_SET:
        status = mesh_provisioner_process_default_ttl_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_GATT_PROXY_GET:
        status = mesh_provisioner_process_gatt_proxy_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_GATT_PROXY_SET:
        status = mesh_provisioner_process_gatt_proxy_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_RELAY_GET:
        status = mesh_provisioner_process_relay_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_RELAY_SET:
        status = mesh_provisioner_process_relay_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_FRIEND_GET:
        status = mesh_provisioner_process_friend_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_FRIEND_SET:
        status = mesh_provisioner_process_friend_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_KEY_REFRESH_PHASE_GET:
        status = mesh_provisioner_process_key_refresh_phase_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_KEY_REFRESH_PHASE_SET:
        status = mesh_provisioner_process_key_refresh_phase_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_IDENTITY_GET:
        status = mesh_provisioner_process_node_identity_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_IDENTITY_SET:
        status = mesh_provisioner_process_node_identity_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_PUBLICATION_GET:
        status = mesh_provisioner_process_model_publication_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_PUBLICATION_SET:
        status = mesh_provisioner_process_model_publication_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_ADD:
        status = mesh_provisioner_process_model_subscription_change(OPERATION_ADD, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_DELETE:
        status = mesh_provisioner_process_model_subscription_change(OPERATION_DELETE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE:
        status = mesh_provisioner_process_model_subscription_change(OPERATION_OVERWRITE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL:
        status = mesh_provisioner_process_model_subscription_change(OPERATION_DELETE_ALL, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_GET:
        status = mesh_provisioner_process_model_subscription_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_ADD:
        status = mesh_provisioner_process_netkey_change(OPERATION_ADD, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_DELETE:
        status = mesh_provisioner_process_netkey_change(OPERATION_DELETE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_UPDATE:
        status = mesh_provisioner_process_netkey_change(OPERATION_OVERWRITE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_GET:
        status = mesh_provisioner_process_netkey_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_ADD:
        status = mesh_provisioner_process_appkey_change(OPERATION_ADD, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_DELETE:
        status = mesh_provisioner_process_appkey_change(OPERATION_DELETE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_UPDATE:
        status = mesh_provisioner_process_appkey_change(OPERATION_OVERWRITE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_GET:
        status = mesh_provisioner_process_appkey_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_BIND:
        status = mesh_provisioner_process_model_app_change(OPERATION_ADD, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_UNBIND:
        status = mesh_provisioner_process_model_app_change(OPERATION_DELETE, p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_GET:
        status = mesh_provisioner_process_model_app_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_SUBSCRIPTION_GET:
        status = mesh_provisioner_process_heartbeat_subscription_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_SUBSCRIPTION_SET:
        status = mesh_provisioner_process_heartbeat_subscription_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_PUBLICATION_GET:
        status = mesh_provisioner_process_heartbeat_publication_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_PUBLICATION_SET:
        status = mesh_provisioner_process_heartbeat_publication_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NETWORK_TRANSMIT_GET:
        status = mesh_provisioner_process_network_transmit_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_NETWORK_TRANSMIT_SET:
        status = mesh_provisioner_process_network_transmit_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_CONFIG_LPN_POLL_TIMEOUT_GET:
        status = mesh_provisioner_process_lpn_poll_timeout_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_GET:
        status = mesh_provisioner_process_health_fault_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_CLEAR:
        status = mesh_provisioner_process_health_fault_clear(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_TEST:
        status = mesh_provisioner_process_health_fault_test(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_PERIOD_GET:
        status = mesh_provisioner_process_health_period_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_PERIOD_SET:
        status = mesh_provisioner_process_health_period_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_ATTENTION_GET:
        status = mesh_provisioner_process_health_attention_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_HEALTH_ATTENTION_SET:
        status = mesh_provisioner_process_health_attention_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_TYPE_SET:
        status = mesh_provisioner_process_proxy_filter_type_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_ADDRESSES_ADD:
    case HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_ADDRESSES_DELETE:
        status = mesh_provisioner_process_proxy_filter_change(opcode == HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_ADDRESSES_ADD, p_data, length);
        break;

    default:
        return WICED_FALSE;
    }
    mesh_provisioner_hci_send_status(status);
    return WICED_TRUE;
}

/*
 * Process command from MCU to setup local device
 */
uint8_t mesh_provisioner_process_set_local_device(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_local_device_set_data_t set;

    STREAM_TO_UINT16(set.addr, p_data);
    STREAM_TO_ARRAY(set.network_key, p_data, 16);
    STREAM_TO_UINT16(set.net_key_idx, p_data);
    STREAM_TO_UINT32(set.iv_idx, p_data);
    STREAM_TO_UINT8(set.key_refresh, p_data);
    STREAM_TO_UINT8(set.iv_update, p_data);

    WICED_BT_TRACE("addr:%x net_key_idx:%x iv_idx:%x key_refresh:%d iv_upd:%d\n", set.addr, set.net_key_idx, set.iv_idx, set.key_refresh, set.iv_update);
    return wiced_bt_mesh_provision_local_device_set(&set) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
* Process command from MCU to set device key.  MCU can set device key once and then perform multiple configuration commands.
*/
uint8_t mesh_provisioner_process_set_dev_key(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_set_dev_key_data_t set;
    STREAM_TO_UINT16(set.dst, p_data);
    STREAM_TO_ARRAY(set.dev_key, p_data, 16);
    wiced_bt_mesh_provision_set_dev_key(&set);
    return HCI_CONTROL_MESH_STATUS_SUCCESS;
}

/*
 * Start Scanning for Unprovisioned devices
 */
uint8_t mesh_provisioner_process_scan_unprovisioned_devices(uint8_t *p_data, uint32_t length)
{
    mesh_gatt_client_scan_unprovisioned_devices(p_data[0]);
    return HCI_CONTROL_MESH_STATUS_SUCCESS;
}

/*
 * Process command from MCU to connect provisioning link
 */
uint8_t mesh_provisioner_process_connect(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_provision_connect_data_t connect;
    uint8_t use_pb_gatt;

    STREAM_TO_UINT16(connect.addr, p_data);
    STREAM_TO_UINT32(connect.conn_id, p_data);
    STREAM_TO_ARRAY(connect.uuid, p_data, 16);
    STREAM_TO_UINT8(connect.identify_duration, p_data);
    STREAM_TO_UINT8(use_pb_gatt, p_data);

    return mesh_gatt_client_provision_connect(&connect, use_pb_gatt)  ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to disconnect provisioning link
 */
uint8_t mesh_provisioner_process_disconnect(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_provision_disconnect_data_t disconnect;

    STREAM_TO_UINT32(disconnect.conn_id, p_data);

    return mesh_gatt_client_provision_disconnect(&disconnect)  ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to start provisioning on the established provisioning link
 */
uint8_t mesh_provisioner_process_start(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_provision_start_data_t start;

    STREAM_TO_UINT32(start.conn_id, p_data);
    STREAM_TO_UINT8(start.algorithm, p_data);
    STREAM_TO_UINT8(start.public_key_type, p_data);
    STREAM_TO_UINT8(start.auth_method, p_data);
    STREAM_TO_UINT8(start.auth_action, p_data);
    STREAM_TO_UINT8(start.auth_size, p_data);

    return wiced_bt_mesh_provision_start(&start) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to with OOB value to be used in the link calculation
 */
uint8_t mesh_provisioner_process_oob_value(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_provision_oob_value_data_t oob;

    STREAM_TO_UINT32(oob.conn_id, p_data);
    STREAM_TO_UINT8(oob.data_size, p_data);
    STREAM_TO_ARRAY(oob.data, p_data, length - 5);

    return wiced_bt_mesh_provision_set_oob(&oob) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Start Search for devices that support GATT Proxy functionality
 */
uint8_t mesh_provisioner_process_search_proxy(uint8_t *p_data, uint32_t length)
{
    mesh_gatt_client_search_proxy(p_data[0]);
    return HCI_CONTROL_MESH_STATUS_SUCCESS;
}

/*
 * Process command from MCU to connect to a GATT Proxy
 */
uint8_t mesh_provisioner_process_proxy_connect(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_proxy_connect_data_t connect;
    uint8_t use_pb_gatt;

    STREAM_TO_BDADDR(connect.bd_addr, p_data);
    STREAM_TO_UINT8(connect.bd_addr_type, p_data);
    return mesh_gatt_client_proxy_connect(&connect)  ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to disconnect GATT Proxy
 */
uint8_t mesh_provisioner_process_proxy_disconnect(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_provision_disconnect_data_t disconnect;

    STREAM_TO_UINT32(disconnect.conn_id, p_data);

    return mesh_gatt_client_proxy_disconnect(&disconnect)  ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}


/*
 * Process command from MCU to Reset Node
 */
uint8_t mesh_provisioner_process_node_reset(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_node_reset_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_node_reset(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Mesh Secure Beacon Status
 */
uint8_t mesh_provisioner_process_beacon_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_beacon_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_beacon_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Mesh Secure Beacon Status
 */
uint8_t mesh_provisioner_process_beacon_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_beacon_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.state, p_data);

    return wiced_bt_mesh_config_beacon_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Mesh Node Composition Data
 */
uint8_t mesh_provisioner_process_composition_data_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_composition_data_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.page_number, p_data);

    return wiced_bt_mesh_config_composition_data_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Value of the Node's Default TTL
 */
uint8_t mesh_provisioner_process_default_ttl_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_default_ttl_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_default_ttl_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Value of the Node's Default TTL
 */
uint8_t mesh_provisioner_process_default_ttl_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_default_ttl_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.ttl, p_data);

    return wiced_bt_mesh_config_default_ttl_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get GATT Proxy State
 */
uint8_t mesh_provisioner_process_gatt_proxy_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_gatt_proxy_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_gatt_proxy_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set GATT Proxy Status
 */
uint8_t mesh_provisioner_process_gatt_proxy_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_gatt_proxy_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.state, p_data);

    return wiced_bt_mesh_config_gatt_proxy_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Relay Status and parameters
 */
uint8_t mesh_provisioner_process_relay_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_relay_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_relay_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Relay Status and parameters
 */
uint8_t mesh_provisioner_process_relay_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_relay_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.state, p_data);
    STREAM_TO_UINT8(data.retransmit_count, p_data);
    STREAM_TO_UINT16(data.retransmit_interval, p_data);

    return wiced_bt_mesh_config_relay_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Friend State 
 */
uint8_t mesh_provisioner_process_friend_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_friend_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);

    return wiced_bt_mesh_config_friend_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Friend State
 */
uint8_t mesh_provisioner_process_friend_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_friend_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT8(data.state, p_data);

    return wiced_bt_mesh_config_friend_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Key Refresh Phase
 */
uint8_t mesh_provisioner_process_key_refresh_phase_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_key_refresh_phase_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);

    return wiced_bt_mesh_config_key_refresh_phase_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Get Key Refresh Phase
 */
uint8_t mesh_provisioner_process_key_refresh_phase_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_key_refresh_phase_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);
    STREAM_TO_UINT8(data.transition, p_data);

    return wiced_bt_mesh_config_key_refresh_phase_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Node Identity State
 */
uint8_t mesh_provisioner_process_node_identity_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_node_identity_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);

    return wiced_bt_mesh_config_node_identity_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Node Identity State
 */
uint8_t mesh_provisioner_process_node_identity_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_node_identity_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);
    STREAM_TO_UINT8(data.identity, p_data);

    return wiced_bt_mesh_config_node_identity_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Model Publication
 */
uint8_t mesh_provisioner_process_model_publication_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_publication_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);

    return wiced_bt_mesh_config_model_publication_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Set Model Publication
 */
uint8_t mesh_provisioner_process_model_publication_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_publication_set_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);
    STREAM_TO_ARRAY(data.publish_addr, p_data, 16);
    STREAM_TO_UINT16(data.app_key_idx, p_data);
    STREAM_TO_UINT8(data.credential_flag, p_data);
    STREAM_TO_UINT8(data.publish_ttl, p_data);
    STREAM_TO_UINT32(data.publish_period, p_data);
    STREAM_TO_UINT8(data.publish_retransmit_count, p_data);
    STREAM_TO_UINT16(data.publish_retransmit_interval, p_data);

    return wiced_bt_mesh_config_model_publication_set(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Add, Delete, Overwrite. or Delete All addresses from a Model Subscription
 */
uint8_t mesh_provisioner_process_model_subscription_change(uint8_t operation, uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_subscription_change_data_t data;

    data.operation = operation;
    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);
    if (operation != OPERATION_DELETE_ALL)
    {
        STREAM_TO_ARRAY(data.addr, p_data, 16);
    }
    return wiced_bt_mesh_config_model_subscription_change(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Model Subscription list
 */
uint8_t mesh_provisioner_process_model_subscription_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_subscription_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);
    return wiced_bt_mesh_config_model_subscription_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Add, Delete, Update Network Key
 */
uint8_t mesh_provisioner_process_netkey_change(uint8_t operation, uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_netkey_change_data_t data;

    data.operation = operation;
    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);
    if (operation != OPERATION_DELETE)
    {
        STREAM_TO_ARRAY(data.net_key, p_data, 16);
    }
    return wiced_bt_mesh_config_netkey_change(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Network Keys list
 */
uint8_t mesh_provisioner_process_netkey_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_netkey_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    return wiced_bt_mesh_config_netkey_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Add, Delete, Update an Application Key
 */
uint8_t mesh_provisioner_process_appkey_change(uint8_t operation, uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_appkey_change_data_t data;

    data.operation = operation;
    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);
    STREAM_TO_UINT16(data.app_key_idx, p_data);
    if (operation != OPERATION_DELETE)
    {
        STREAM_TO_ARRAY(data.app_key, p_data, 16);
    }
    return wiced_bt_mesh_config_appkey_change(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Application Keys list
 */
uint8_t mesh_provisioner_process_appkey_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_appkey_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.net_key_idx, p_data);
    return wiced_bt_mesh_config_appkey_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Bond or Unbind a Model to an Application Key
 */
uint8_t mesh_provisioner_process_model_app_change(uint8_t operation, uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_app_change_data_t data;

    data.operation = operation;
    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);
    STREAM_TO_UINT16(data.app_key_idx, p_data);
    return wiced_bt_mesh_config_model_app_change(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to Get Application Keys bound to a Model
 */
uint8_t mesh_provisioner_process_model_app_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_model_app_get_data_t data;

    STREAM_TO_UINT16(data.dst, p_data);
    STREAM_TO_UINT16(data.element_addr, p_data);
    STREAM_TO_UINT32(data.model_id, p_data);
    return wiced_bt_mesh_config_model_app_get(&data) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to get hearbeat subscription on a device
 */
uint8_t mesh_provisioner_process_heartbeat_subscription_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_heartbeat_subscription_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);

    return wiced_bt_mesh_config_heartbeat_subscription_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to set hearbeat subscription on a device
 */
uint8_t mesh_provisioner_process_heartbeat_subscription_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_heartbeat_subscription_set_data_t set;

    STREAM_TO_UINT16(set.dst, p_data);
    STREAM_TO_UINT16(set.subscription_src, p_data);
    STREAM_TO_UINT16(set.subscription_dst, p_data);
    STREAM_TO_UINT32(set.period, p_data);

    return wiced_bt_mesh_config_heartbeat_subscription_set(&set) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to get hearbeat publication on a device
 */
uint8_t mesh_provisioner_process_heartbeat_publication_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_heartbeat_publication_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);

    return wiced_bt_mesh_config_heartbeat_publication_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}


/*
 * Process command from MCU to set hearbeat publication on a device
 */
uint8_t mesh_provisioner_process_heartbeat_publication_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_heartbeat_publication_set_data_t set;

    STREAM_TO_UINT16(set.dst, p_data);
    STREAM_TO_UINT16(set.publication_dst, p_data);
    STREAM_TO_UINT32(set.count, p_data);
    STREAM_TO_UINT32(set.period, p_data);
    STREAM_TO_UINT8(set.ttl, p_data);
    STREAM_TO_UINT8(set.feature_relay, p_data);
    STREAM_TO_UINT8(set.feature_proxy, p_data);
    STREAM_TO_UINT8(set.feature_friend, p_data);
    STREAM_TO_UINT8(set.feature_low_power, p_data);
    STREAM_TO_UINT16(set.net_key_idx, p_data);

    return wiced_bt_mesh_config_heartbeat_publication_set(&set) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to get network transmit parameters of a device
 */
uint8_t mesh_provisioner_process_network_transmit_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_network_transmit_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);

    return wiced_bt_mesh_config_network_transmit_params_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
* Process command from MCU to set network transmit parameters of a device
*/
uint8_t mesh_provisioner_process_network_transmit_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_config_network_transmit_set_data_t set;

    STREAM_TO_UINT16(set.dst, p_data);
    STREAM_TO_UINT8(set.count, p_data);
    STREAM_TO_UINT16(set.interval, p_data);

    return wiced_bt_mesh_config_network_transmit_params_set(&set) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to get the current Registered Fault state identified by Company ID of an element
 */
uint8_t mesh_provisioner_process_health_fault_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_fault_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);
    STREAM_TO_UINT16(get.app_key_idx, p_data);
    STREAM_TO_UINT16(get.company_id, p_data);

    return wiced_bt_mesh_health_fault_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to clear the current Registered Fault state identified by Company ID of an element
 */
uint8_t mesh_provisioner_process_health_fault_clear(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_fault_clear_data_t clear;

    STREAM_TO_UINT16(clear.dst, p_data);
    STREAM_TO_UINT16(clear.app_key_idx, p_data);
    STREAM_TO_UINT8(clear.reliable, p_data);
    STREAM_TO_UINT16(clear.company_id, p_data);

    return wiced_bt_mesh_health_fault_clear(&clear) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to nvoke a self-test procedure of an element
 */
uint8_t mesh_provisioner_process_health_fault_test(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_fault_test_data_t test;

    STREAM_TO_UINT16(test.dst, p_data);
    STREAM_TO_UINT16(test.app_key_idx, p_data);
    STREAM_TO_UINT8(test.reliable, p_data);
    STREAM_TO_UINT8(test.id, p_data);
    STREAM_TO_UINT16(test.company_id, p_data);

    return wiced_bt_mesh_health_fault_test(&test) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to get the current Health Period state of an element
 */
uint8_t mesh_provisioner_process_health_period_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_period_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);
    STREAM_TO_UINT16(get.app_key_idx, p_data);

    return wiced_bt_mesh_health_period_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

/*
 * Process command from MCU to set the current Health Period state of an element
 */
uint8_t mesh_provisioner_process_health_period_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_period_set_data_t period;

    STREAM_TO_UINT16(period.dst, p_data);
    STREAM_TO_UINT16(period.app_key_idx, p_data);
    STREAM_TO_UINT8(period.reliable, p_data);
    STREAM_TO_UINT8(period.divisor, p_data);

    return wiced_bt_mesh_health_period_set(&period) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

uint8_t mesh_provisioner_process_health_attention_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_attention_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);
    STREAM_TO_UINT16(get.app_key_idx, p_data);

    return wiced_bt_mesh_health_attention_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

uint8_t mesh_provisioner_process_health_attention_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_health_attention_set_data_t attention;

    STREAM_TO_UINT16(attention.dst, p_data);
    STREAM_TO_UINT16(attention.app_key_idx, p_data);
    STREAM_TO_UINT8(attention.reliable, p_data);
    STREAM_TO_UINT8(attention.timer, p_data);

    return wiced_bt_mesh_health_attention_set(&attention) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

uint8_t mesh_provisioner_process_lpn_poll_timeout_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_lpn_poll_timeout_get_data_t get;

    STREAM_TO_UINT16(get.dst, p_data);
    STREAM_TO_UINT16(get.lpn_addr, p_data);

    return wiced_bt_mesh_lpn_poll_timeout_get(&get) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

uint8_t mesh_provisioner_process_proxy_filter_type_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_proxy_filter_set_type_data_t set;

    STREAM_TO_UINT8(set.type, p_data);

    return wiced_bt_mesh_proxy_set_filter_type(&set) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
}

uint8_t mesh_provisioner_process_proxy_filter_change(wiced_bool_t is_add, uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_proxy_filter_change_addr_data_t *p_addr;
    uint16_t addr_num = length / 2;
    uint16_t i;
    uint8_t  res;

    if ((p_addr = (wiced_bt_mesh_proxy_filter_change_addr_data_t *)wiced_bt_get_buffer(sizeof(wiced_bt_mesh_proxy_filter_change_addr_data_t) + (addr_num - 1) * 2)) == NULL)
        return HCI_CONTROL_MESH_STATUS_ERROR;

    p_addr->addr_num = addr_num;
    for (i = 0; i < addr_num; i++)
        STREAM_TO_UINT16(p_addr->addr[i], p_data);

    res = wiced_bt_mesh_proxy_filter_change_addr(is_add, p_addr) ? HCI_CONTROL_MESH_STATUS_SUCCESS : HCI_CONTROL_MESH_STATUS_ERROR;
    wiced_bt_free_buffer(p_addr);
    return res;
}

void mesh_provisioner_hci_send_status(uint8_t status)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, status);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_COMMAND_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send provisioner unprovisioned device event over transport
 */
void mesh_provisioner_hci_event_unprovisioned_device_send(wiced_bt_mesh_provision_unprovisioned_device_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    ARRAY_TO_STREAM(p, p_data->uuid, 16);
    UINT16_TO_STREAM(p, p_data->oob);
    UINT32_TO_STREAM(p, p_data->uri_hash);
    UINT8_TO_STREAM(p, p_data->pb_gatt_supported);
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_UNPPROVISIONED_DEVICE, buffer, (uint16_t)(p - buffer));

}

/*
 * Send Proxy Device Network Data event over transport
 */
void mesh_provisioner_hci_event_proxy_device_send(wiced_bt_mesh_proxy_device_network_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    BDADDR_TO_STREAM(p, p_data->bd_addr);
    UINT8_TO_STREAM(p, p_data->bd_addr_type);
    UINT8_TO_STREAM(p, p_data->rssi);
    UINT16_TO_STREAM(p, p_data->net_key_idx);
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROXY_DEVICE_NETWORK_DATA, buffer, (uint16_t)(p - buffer));

}

/*
 * Send provisioner provisioning end event over transport
 */
void mesh_provisioner_hci_event_provision_link_status_send(wiced_bt_mesh_provision_link_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("provision connection status conn_id:%d addr:%x connected:%d\n", p_data->conn_id, p_data->addr, p_data->is_connected);

    UINT32_TO_STREAM(p, p_data->conn_id);
    UINT16_TO_STREAM(p, p_data->addr);
    UINT8_TO_STREAM(p, p_data->is_connected);
    UINT8_TO_STREAM(p, p_data->is_gatt);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROVISION_LINK_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send provisioner provisioning end event over transport
 */
void mesh_provisioner_hci_event_provision_end_send(wiced_bt_mesh_provision_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("provision end addr:%x result:%d\n", p_data->addr, p_data->result);

    UINT32_TO_STREAM(p, p_data->conn_id);
    UINT16_TO_STREAM(p, p_data->addr);
    UINT8_TO_STREAM(p, p_data->result);
    ARRAY_TO_STREAM(p, p_data->dev_key, MESH_KEY_LEN);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROVISION_END, buffer, (uint16_t)(p - buffer));
}

/*
 * Send to the MCU device capabilities received during provisioning
 */
void mesh_provisioner_hci_event_device_capabilities_send(wiced_bt_mesh_provision_device_capabilities_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("mesh prov caps conn_id:%x key_type:%d\n", p_data->conn_id, p_data->pub_key_type);

    UINT32_TO_STREAM(p, p_data->conn_id);
    UINT8_TO_STREAM(p, p_data->elements_num);
    UINT16_TO_STREAM(p, p_data->algorithms);
    UINT8_TO_STREAM(p, p_data->pub_key_type);
    UINT8_TO_STREAM(p, p_data->static_oob_type);
    UINT8_TO_STREAM(p, p_data->output_oob_size);
    UINT16_TO_STREAM(p, p_data->output_oob_action);
    UINT8_TO_STREAM(p, p_data->input_oob_size);
    UINT16_TO_STREAM(p, p_data->input_oob_action);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROVISION_DEVICE_CAPABITIES, buffer, (uint16_t)(p - buffer));
}

/*
 * Send to the MCU Out of Band data request
 */
void mesh_provisioner_hci_event_device_get_oob_data_send(wiced_bt_mesh_provision_device_oob_request_data_t *p_oob_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("mesh prov oob req conn_id:%x type:%d size:%d action:%d\n", p_oob_data->conn_id, p_oob_data->type, p_oob_data->size, p_oob_data->action);

    UINT32_TO_STREAM(p, p_oob_data->conn_id);
    UINT8_TO_STREAM(p, p_oob_data->type);
    UINT8_TO_STREAM(p, p_oob_data->size);
    UINT8_TO_STREAM(p, p_oob_data->action);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROVISION_OOB_DATA, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_node_reset_status_send(wiced_bt_mesh_config_node_reset_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("node reset status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_NODE_RESET_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_node_identity_status_send(wiced_bt_mesh_config_node_identity_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("node identity status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->net_key_idx);
    UINT8_TO_STREAM(p, p_data->identity);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_NODE_IDENTITY_STATUS, buffer, (uint16_t)(p - buffer));
}


void mesh_provisioner_hci_event_composition_data_status_send(wiced_bt_mesh_config_composition_data_status_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(3 + p_data->data_len)) == NULL)
    {
        WICED_BT_TRACE("model sub list no mem\n");
        return;
    }
    p = p_buffer;

    WICED_BT_TRACE("comp status src:%x page_num:%d len:%d\n", p_data->src, p_data->page_number, p_data->data_len);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->page_number);
    ARRAY_TO_STREAM(p, p_data->data, p_data->data_len);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_COMPOSITION_DATA_STATUS, p_buffer, (uint16_t)(p - p_buffer));
    wiced_bt_free_buffer(p_buffer);
}

void mesh_provisioner_hci_event_friend_status_send(wiced_bt_mesh_config_friend_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("friend status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->state);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_FRIEND_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_key_refresh_phase_status_send(wiced_bt_mesh_config_key_refresh_phase_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("kr status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->net_key_idx);
    UINT8_TO_STREAM(p, p_data->phase);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_KEY_REFRESH_PHASE_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_default_ttl_status_send(wiced_bt_mesh_config_default_ttl_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("default_ttl status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->ttl);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_DEFAULT_TTL_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_relay_status_send(wiced_bt_mesh_config_relay_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("relay status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->state);
    UINT8_TO_STREAM(p, p_data->retransmit_count);
    UINT16_TO_STREAM(p, p_data->retransmit_interval);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_RELAY_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_gatt_proxy_status_send(wiced_bt_mesh_config_gatt_proxy_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("gatt_proxy status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->state);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_GATT_PROXY_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_beacon_status_send(wiced_bt_mesh_config_beacon_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("beacon status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->state);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_BEACON_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_model_publication_status_send(wiced_bt_mesh_config_model_publication_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("model pub status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->publish_addr);
    UINT32_TO_STREAM(p, p_data->model_id);
    UINT16_TO_STREAM(p, p_data->app_key_idx);
    UINT8_TO_STREAM(p, p_data->credential_flag);
    UINT8_TO_STREAM(p, p_data->publish_ttl);
    UINT32_TO_STREAM(p, p_data->publish_period);
    UINT8_TO_STREAM(p, p_data->publish_retransmit_count);
    UINT16_TO_STREAM(p, p_data->publish_retransmit_interval);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_MODEL_PUBLICATION_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_model_subscription_status_send(wiced_bt_mesh_config_model_subscription_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("model sub status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->element_addr);
    UINT32_TO_STREAM(p, p_data->model_id);
    UINT16_TO_STREAM(p, p_data->addr);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_MODEL_SUBSCRIPTION_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_model_subscription_list_send(wiced_bt_mesh_config_model_subscription_list_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;
    int i;

    WICED_BT_TRACE("model sub list src:%x\n", p_data->src);

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(7 + (p_data->num_addr * 2))) == NULL)
    {
        WICED_BT_TRACE("model sub list no mem\n");
        return;
    }
    p = p_buffer;
    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->element_addr);
    UINT32_TO_STREAM(p, p_data->model_id);
    for (i = 0; i < p_data->num_addr; i++)
    {
        UINT16_TO_STREAM(p, p_data->addr[i]);
    }
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_MODEL_SUBSCRIPTION_LIST, p_buffer, (uint16_t)(p - p_buffer));
    wiced_bt_free_buffer(p_buffer);
}

void mesh_provisioner_hci_event_netkey_status_send(wiced_bt_mesh_config_netkey_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("netkey status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->net_key_idx);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_NETKEY_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_netkey_list_send(wiced_bt_mesh_config_netkey_list_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;
    int i;

    WICED_BT_TRACE("netkey list src:%x\n", p_data->src);

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(7 + (p_data->num_keys * 2))) == NULL)
    {
        WICED_BT_TRACE("netkey list no mem\n");
        return;
    }
    p = p_buffer;
    UINT16_TO_STREAM(p, p_data->src);
    for (i = 0; i < p_data->num_keys; i++)
    {
        UINT16_TO_STREAM(p, p_data->net_key_idx[i]);
    }
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_NETKEY_LIST, p_buffer, (uint16_t)(p - p_buffer));
    wiced_bt_free_buffer(p_buffer);
}

void mesh_provisioner_hci_event_appkey_status_send(wiced_bt_mesh_config_appkey_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("appkey status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->net_key_idx);
    UINT16_TO_STREAM(p, p_data->app_key_idx);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_APPKEY_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_appkey_list_send(wiced_bt_mesh_config_appkey_list_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;
    int i;

    WICED_BT_TRACE("appkey list src:%x\n", p_data->src);

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(7 + (p_data->num_keys * 2))) == NULL)
    {
        WICED_BT_TRACE("appkey list no mem\n");
        return;
    }
    p = p_buffer;
    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->net_key_idx);
    for (i = 0; i < p_data->num_keys; i++)
    {
        UINT16_TO_STREAM(p, p_data->app_key_idx[i]);
    }
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_APPKEY_LIST, p_buffer, (uint16_t)(p - p_buffer));
    wiced_bt_free_buffer(p_buffer);
}

void mesh_provisioner_hci_event_model_app_status_send(wiced_bt_mesh_config_model_app_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("model app status src:%x\n", p_data->src);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->element_addr);
    UINT32_TO_STREAM(p, p_data->model_id);
    UINT16_TO_STREAM(p, p_data->app_key_idx);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_MODEL_APP_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_model_app_list_send(wiced_bt_mesh_config_model_app_list_data_t *p_data)
{
    uint8_t *p_buffer;
    uint8_t *p;
    int i;

    WICED_BT_TRACE("model_app list src:%x\n", p_data->src);

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(9 + (p_data->num_keys * 2))) == NULL)
    {
        WICED_BT_TRACE("model_app list no mem\n");
        return;
    }
    p = p_buffer;
    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->element_addr);
    UINT32_TO_STREAM(p, p_data->model_id);
    for (i = 0; i < p_data->num_keys; i++)
    {
        UINT16_TO_STREAM(p, p_data->app_key_idx[i]);
    }
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_MODEL_APP_LIST, p_buffer, (uint16_t)(p - p_buffer));
    wiced_bt_free_buffer(p_buffer);
}

void mesh_provisioner_hci_event_hearbeat_subscription_status_send(wiced_bt_mesh_config_heartbeat_subscription_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("subs status src:%x status:%d subs src/dst:%x/%x period:%d count:%d hops min/max:%d/%d\n",
        p_data->src, p_data->status, p_data->subscription_src, p_data->subscription_dst, p_data->period, p_data->count, p_data->min_hops, p_data->max_hops);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->subscription_src);
    UINT16_TO_STREAM(p, p_data->subscription_dst);
    UINT32_TO_STREAM(p, p_data->period);
    UINT16_TO_STREAM(p, p_data->count);
    UINT8_TO_STREAM(p, p_data->min_hops);
    UINT8_TO_STREAM(p, p_data->max_hops);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEARTBEAT_SUBSCRIPTION_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_hearbeat_publication_status_send(wiced_bt_mesh_config_heartbeat_publication_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("pubs status src:%x status:%d pubs dst:%x period:%d count:%d hops ttl:%d net_key_idx:%d\n",
        p_data->src, p_data->status, p_data->publication_dst, p_data->period, p_data->count, p_data->ttl, p_data->net_key_idx);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->publication_dst);
    UINT32_TO_STREAM(p, p_data->period);
    UINT16_TO_STREAM(p, p_data->count);
    UINT8_TO_STREAM(p, p_data->ttl);
    UINT8_TO_STREAM(p, p_data->feature_relay);
    UINT8_TO_STREAM(p, p_data->feature_proxy);
    UINT8_TO_STREAM(p, p_data->feature_friend);
    UINT8_TO_STREAM(p, p_data->feature_low_power);
    UINT16_TO_STREAM(p, p_data->net_key_idx);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEARTBEAT_PUBLICATION_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_network_transmit_status_send(wiced_bt_mesh_config_network_transmit_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    WICED_BT_TRACE("net xmit params src:%x count:%d interval:%d\n", p_data->src, p_data->count, p_data->interval);

    UINT16_TO_STREAM(p, p_data->src);
    UINT8_TO_STREAM(p, p_data->count);
    UINT32_TO_STREAM(p, p_data->interval);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_NETWORK_TRANSMIT_PARAMS_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_health_current_status_send(wiced_bt_mesh_health_fault_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_data->src);
    UINT16_TO_STREAM(p, p_data->app_key_idx);
    UINT8_TO_STREAM(p, p_data->test_id);
    UINT16_TO_STREAM(p, p_data->company_id);
    ARRAY_TO_STREAM(p, p_data->fault_array, p_data->count);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEALTH_CURRENT_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_health_fault_status_send(wiced_bt_mesh_health_fault_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_data->src);
    UINT16_TO_STREAM(p, p_data->app_key_idx);
    UINT8_TO_STREAM(p, p_data->test_id);
    UINT16_TO_STREAM(p, p_data->company_id);
    ARRAY_TO_STREAM(p, p_data->fault_array, p_data->count);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEALTH_FAULT_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_health_period_status_send(wiced_bt_mesh_health_period_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_data->src);
    UINT16_TO_STREAM(p, p_data->app_key_idx);
    UINT8_TO_STREAM(p, p_data->divisor);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEALTH_PERIOD_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_health_attention_status_send(wiced_bt_mesh_health_attention_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_data->src);
    UINT16_TO_STREAM(p, p_data->app_key_idx);
    UINT8_TO_STREAM(p, p_data->timer);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_HEALTH_ATTENTION_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_lpn_poll_timeout_status_send(wiced_bt_mesh_lpn_poll_timeout_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_data->src);
    UINT16_TO_STREAM(p, p_data->lpn_addr);
    UINT32_TO_STREAM(p, p_data->poll_timeout);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LPN_POLL_TIMEOUT_STATUS, buffer, (uint16_t)(p - buffer));
}

void mesh_provisioner_hci_event_proxy_filter_status_send(wiced_bt_mesh_proxy_filter_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT8_TO_STREAM(p, p_data->type);
    UINT16_TO_STREAM(p, p_data->list_size);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROXY_FILTER_STATUS, buffer, (uint16_t)(p - buffer));
}
