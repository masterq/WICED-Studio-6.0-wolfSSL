/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file manages Provisioning and Proxy Connections over GATT client
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_uuid.h"
#include "wiced_bt_mesh_core.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_mesh_provision.h"
#include "wiced_bt_trace.h"

#define ADV_DATA_LEN_MESH_PROVISIONING_SERVICE  22
#define ADV_DATA_LEN_MESH_PROXY_SERVICE         21

typedef struct
{
#define MESH_GATT_CLIENT_STATE_IDLE                         0
#define MESH_GATT_CLIENT_STATE_SCANNING                     1
#define MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION         2
#define MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION      3
#define MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROVISION       4
#define MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY          5
#define MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROXY           6
    uint8_t  state;

#define MESH_GATT_CLIENT_SCAN_STATE_UNPROVISIONED           1
#define MESH_GATT_CLIENT_SCAN_STATE_PROXY                   2
    uint8_t  scan_state;

#define MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE               0
#define MESH_GATT_CLIENT_DISCOVERY_STATE_SERVICE            1
#define MESH_GATT_CLIENT_DISCOVERY_STATE_CHARACTERISTICS    2
#define MESH_GATT_CLIENT_DISCOVERY_STATE_DESCRIPTORS        3
#define MESH_GATT_CLIENT_DISCOVERY_STATE_WRITE_DESCRIPTORS  4
    uint8_t  discovery_state;

    uint8_t  use_pb_gatt;
    uint16_t conn_id;
    uint16_t mtu;
    uint16_t start_handle;
    uint16_t end_handle;
    uint16_t data_out_handle;
    uint16_t data_out_value_handle;
    uint16_t data_in_handle;
    uint16_t data_in_value_handle;
    uint16_t data_out_cccd_handle;
    uint16_t connecting_addr;
    uint8_t  connecting_uuid[MAX_UUID_SIZE];
    uint8_t  connecting_bdaddr[BD_ADDR_LEN];
    uint8_t  identify_duration;
} mesh_gatt_client_state_t;

mesh_gatt_client_state_t mesh_gatt_client_state;

static void mesh_gatt_client_process_connection_status(wiced_bt_gatt_connection_status_t *p_connection_status);
static wiced_bt_gatt_status_t mesh_gatt_client_gatt_operation_complete(wiced_bt_gatt_operation_complete_t *p_operation_complete);
static wiced_bt_gatt_status_t mesh_gatt_client_gatt_discovery_result(wiced_bt_gatt_discovery_result_t *p_discovery_result);
static wiced_bt_gatt_status_t mesh_gatt_client_gatt_discovery_complete(wiced_bt_gatt_discovery_complete_t *p_discovery_complete);
static wiced_bt_gatt_status_t mesh_gatt_client_gatts_req_callback(wiced_bt_gatt_attribute_request_t *p_attribute_request);
static wiced_bt_gatt_status_t mash_gatt_client_notification_handler(wiced_bt_gatt_operation_complete_t *p_data);
wiced_bt_gatt_status_t mesh_gatt_client_event(wiced_bt_gatt_evt_t event, wiced_bt_gatt_event_data_t *p_data);
static void mesh_adv_report(wiced_bt_ble_scan_results_t *p_scan_result, uint8_t *p_adv_data);
static void mash_gatt_client_write_rsp_handler(wiced_bt_gatt_operation_complete_t *p_data);
static void mesh_gatt_client_link_status_send(wiced_bool_t is_connected);


/*
 * Mesh GATT client initialization.
 * Unlike prety much all other mesh applications, this sample app works as a controller, meaning that
 * it need to be able to create GATT connections to a provisining device and/or GATT proxy. Some logic
 * which is implemented in the mesh_app library is overwritten here.
 */
void mesh_gatt_client_init(void)
{
    wiced_bt_gatt_status_t gatt_status;

    gatt_status = wiced_bt_gatt_register(mesh_gatt_client_event);
    WICED_BT_TRACE("wiced_bt_gatt_register: %d\n", gatt_status);

    // take over advertising reports so that core only receives messages through this app
    wiced_bt_ble_observe(WICED_FALSE, 0, mesh_adv_report);
    wiced_bt_ble_observe(WICED_TRUE, 0, mesh_adv_report);
}

/*
 * Process command from the MCU to start or stop scanning for unprovisioned mesh devices.
 */
void mesh_gatt_client_scan_unprovisioned_devices(uint8_t start)
{
    WICED_BT_TRACE("%s %d\n", __FUNCTION__, start);
    if (start)
    {
        if (mesh_gatt_client_state.scan_state & MESH_GATT_CLIENT_SCAN_STATE_UNPROVISIONED)
        {
            WICED_BT_TRACE("already scanning\n");
            return;
        }
        mesh_gatt_client_state.scan_state |= MESH_GATT_CLIENT_SCAN_STATE_UNPROVISIONED;
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_SCANNING;
    }
    else
    {
        mesh_gatt_client_state.scan_state &= ~MESH_GATT_CLIENT_SCAN_STATE_UNPROVISIONED;
        if (mesh_gatt_client_state.scan_state == 0)
        {
            mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_IDLE;
        }
    }
    wiced_bt_mesh_provision_scan_unprovisioned_devices(start);
}

/*
 * Process command from the MCU to start or stop scanning for GATT Proxy devices.
 */
void mesh_gatt_client_search_proxy(uint8_t start)
{
    WICED_BT_TRACE("%s %d\n", __FUNCTION__, start);
    if (start)
    {
        if (mesh_gatt_client_state.scan_state & MESH_GATT_CLIENT_SCAN_STATE_PROXY)
        {
            WICED_BT_TRACE("already scanning\n");
            return;
        }
        mesh_gatt_client_state.scan_state |= MESH_GATT_CLIENT_SCAN_STATE_PROXY;
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_SCANNING;
    }
    else
    {
        mesh_gatt_client_state.scan_state &= ~MESH_GATT_CLIENT_SCAN_STATE_PROXY;
        if (mesh_gatt_client_state.scan_state == 0)
        {
            mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_IDLE;
        }
    }
}

/*
 * Handle received adv packet
 */
void mesh_adv_report(wiced_bt_ble_scan_results_t *p_adv_report, uint8_t *p_adv_data)
{
    uint8_t *p_service_data;
    uint8_t service_data_len;
    uint8_t *p;

    if (!p_adv_report)
        return;

    if (p_adv_report->ble_evt_type == BTM_BLE_EVT_NON_CONNECTABLE_ADVERTISEMENT)
    {
        // While we are trying to connect over GATT to a device do not give none connectable adverts to core
        if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION) && mesh_gatt_client_state.use_pb_gatt)
        {
            return;
        }
        // pass it to core
        wiced_bt_mesh_core_adv_packet(p_adv_report->rssi, p_adv_data);
        return;
    }
    // While we are trying to connect over PB-ADV ignore connectable adverts
    if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION) && !mesh_gatt_client_state.use_pb_gatt)
    {
        return;
    }
    // handle connectable unprovisioned packet
    if ((p_adv_report->ble_evt_type == BTM_BLE_EVT_CONNECTABLE_ADVERTISEMENT) &&
        ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_SCANNING) ||
         (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION)))
    {
        uint8_t      length;
        uint8_t      adv_type;
        wiced_bool_t flags_present = WICED_FALSE;
        wiced_bool_t proxy_service_present = WICED_FALSE;
        wiced_bool_t provisioning_service_present = WICED_FALSE;

        p = p_adv_data;
        length = *p++;

        // stack combines adv and scan response data.  Full length can be 62 bytes
        while (length && (p - p_adv_data <= 62))
        {
            adv_type = *p++;

            switch (adv_type)
            {
            case BTM_BLE_ADVERT_TYPE_FLAG:
                flags_present = WICED_TRUE;
                break;

            case BTM_BLE_ADVERT_TYPE_16SRV_PARTIAL:
            case BTM_BLE_ADVERT_TYPE_16SRV_COMPLETE:
                if ((p[0] == (WICED_BT_MESH_CORE_UUID_SERVICE_PROXY & 0xff)) && (p[1] == ((WICED_BT_MESH_CORE_UUID_SERVICE_PROXY >> 8) & 0xff)))
                {
                    proxy_service_present = WICED_TRUE;
                }
                else if ((p[0] == (WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING & 0xff)) && (p[1] == ((WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING >> 8) & 0xff)))
                {
                    provisioning_service_present = WICED_TRUE;
                }
                break;

            case BTM_BLE_ADVERT_TYPE_SERVICE_DATA:
                // Service data should be for the same service
                if ((proxy_service_present && (p[0] == (WICED_BT_MESH_CORE_UUID_SERVICE_PROXY & 0xff)) && (p[1] == ((WICED_BT_MESH_CORE_UUID_SERVICE_PROXY >> 8) & 0xff)))
                 || (provisioning_service_present && (p[0] == (WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING & 0xff)) && (p[1] == ((WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING >> 8) & 0xff))))
                {
                    p_service_data   = p + 2;
                    service_data_len = length - 3;
                }
                break;
            }
            p += length - 1; /* skip the length of data */
            length = *p++;
        }
        // advertisement data for unprovisioned device should have UUID and OOB
        if ((((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_SCANNING) && (mesh_gatt_client_state.scan_state & MESH_GATT_CLIENT_SCAN_STATE_UNPROVISIONED))
          ||  (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION))
         && flags_present && provisioning_service_present &&
            (p_service_data != NULL) && (service_data_len == 2 + MAX_UUID_SIZE))
        {
            WICED_BT_TRACE("state:%d\n", mesh_gatt_client_state.state);
            if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_SCANNING)
            {
                wiced_bt_mesh_provision_unprovisioned_device_data_t unrovisioned_device_data;

                memcpy(unrovisioned_device_data.uuid, p_service_data, MAX_UUID_SIZE);
                unrovisioned_device_data.oob = p_service_data[MAX_UUID_SIZE] << 8 + p_service_data[MAX_UUID_SIZE + 1];
                unrovisioned_device_data.uri_hash = 0;
                unrovisioned_device_data.pb_gatt_supported = WICED_TRUE;

                mesh_provisioner_hci_event_unprovisioned_device_send(&unrovisioned_device_data);
            }
            else // if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION)
            {
                if (memcmp(mesh_gatt_client_state.connecting_uuid, p_service_data, MAX_UUID_SIZE) == 0)
                {
                    wiced_bool_t connect_status;

                    mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION;

                    wiced_bt_ble_observe(WICED_FALSE, 0, mesh_adv_report);

                    memcpy(mesh_gatt_client_state.connecting_bdaddr, p_adv_report->remote_bd_addr, BD_ADDR_LEN);
                    connect_status = wiced_bt_gatt_le_connect(p_adv_report->remote_bd_addr, p_adv_report->ble_addr_type, BLE_CONN_MODE_HIGH_DUTY, WICED_TRUE);
                    WICED_BT_TRACE("Connect result:%d\n", connect_status);

                    if (!connect_status)
                    {
                        // something went wrong, restart scanning
                        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION;
                        wiced_bt_ble_observe(WICED_TRUE, 0, mesh_adv_report);
                    }
                }
            }
        }
        if (((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_SCANNING) && (mesh_gatt_client_state.scan_state & MESH_GATT_CLIENT_SCAN_STATE_PROXY))
         && flags_present && proxy_service_present && (p_service_data != NULL))
        {
            uint8_t identification_type;

            p = p_service_data;
            identification_type = *p++;
            WICED_BT_TRACE("Proxy device identification_type:%d\n", identification_type);

            if (identification_type == WICED_BT_MESH_PROXY_IDENTIFICATION_TYPE_NETWORK_ID)
            {
                wiced_bt_mesh_proxy_device_network_data_t proxy_network_data;
                if (wiced_bt_mesh_core_is_valid_network_id(p, &proxy_network_data.net_key_idx))
                {
                    proxy_network_data.rssi         = p_adv_report->rssi;
                    proxy_network_data.bd_addr_type = p_adv_report->ble_addr_type;
                    memcpy(proxy_network_data.bd_addr, p_adv_report->remote_bd_addr, BD_ADDR_LEN);
                    mesh_provisioner_hci_event_proxy_device_send(&proxy_network_data);
                }
                return;
            }
            else if (identification_type == WICED_BT_MESH_PROXY_IDENTIFICATION_TYPE_NODE_IDENTITY)
            {

            }
            else
            {
                WICED_BT_TRACE("invalid identity type:%d ignored\n", identification_type);
                return;
            }
            if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_SCANNING)
            {
            }
        }
    }
}

/*
 * Command from MCU to start connection.  If use PB-GATT we will be scanning for
 * connectable unprovisioned devices until we figure out the BDADDR.  Otherwise
 * pass the connect request to provisioner library
 */
wiced_bool_t mesh_gatt_client_provision_connect(wiced_bt_mesh_provision_connect_data_t *p_connect, wiced_bool_t use_pb_gatt)
{
    WICED_BT_TRACE("%s use_pb_gatt:%d\n", __FUNCTION__, use_pb_gatt);

    mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECT_TO_PROVISION;

    mesh_gatt_client_state.use_pb_gatt     = use_pb_gatt;
    mesh_gatt_client_state.connecting_addr = p_connect->addr;
    memcpy(mesh_gatt_client_state.connecting_uuid, p_connect->uuid, MAX_UUID_SIZE);
    mesh_gatt_client_state.identify_duration = p_connect->identify_duration;

    if (!use_pb_gatt)
    {
        wiced_bt_mesh_provision_connect(p_connect);
    }
    return WICED_TRUE;
}

/*
 * Command from MCU to start connection to a proxy device.
 */
wiced_bool_t mesh_gatt_client_proxy_connect(wiced_bt_mesh_proxy_connect_data_t *p_connect)
{
    wiced_bool_t connect_status;

    // if we are connected to proxy we do not need to scan anymore
    wiced_bt_ble_observe(WICED_FALSE, 0, mesh_adv_report);

    // Stops proxy server advertisement and network secure beacon if they are running
    mesh_stop_advert();

    connect_status = wiced_bt_gatt_le_connect(p_connect->bd_addr, p_connect->bd_addr_type, BLE_CONN_MODE_HIGH_DUTY, WICED_TRUE);
    if (connect_status)
    {
        memcpy(mesh_gatt_client_state.connecting_bdaddr, p_connect->bd_addr, BD_ADDR_LEN);
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY;
    }
    else
    {
        // something went wrong, restart scanning
        wiced_bt_ble_observe(WICED_TRUE, 0, mesh_adv_report);
    }
    WICED_BT_TRACE("%s status:%d\n", __FUNCTION__, connect_status);
    return (connect_status);
}

/*
 * Command from MCU to disconnect a proxy connection.
 */
wiced_bool_t mesh_gatt_client_proxy_disconnect(wiced_bt_mesh_proxy_connect_data_t *p_connect)
{
    if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY) &&
        (mesh_gatt_client_state.conn_id == 0))
    {
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_IDLE;
        return wiced_bt_gatt_cancel_connect(mesh_gatt_client_state.connecting_bdaddr, WICED_TRUE);
    }
    else
    {
        return wiced_bt_gatt_disconnect(mesh_gatt_client_state.conn_id);
    }
}

wiced_bool_t mesh_gatt_client_provision_disconnect(wiced_bt_mesh_provision_disconnect_data_t *p_disconnect)
{
    if (p_disconnect->conn_id != mesh_gatt_client_state.conn_id)
    {
        return wiced_bt_mesh_provision_disconnect(p_disconnect);
    }
    else if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) &&
             (mesh_gatt_client_state.conn_id == 0))
    {
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_IDLE;
        return wiced_bt_gatt_cancel_connect(mesh_gatt_client_state.connecting_bdaddr, WICED_TRUE);
    }
    else
    {
        return wiced_bt_gatt_disconnect(mesh_gatt_client_state.conn_id);
    }
}

/*
 * Callback for various GATT events.  As this application performs only as a GATT client, some of the events are omitted.
 */
wiced_bt_gatt_status_t mesh_gatt_client_event(wiced_bt_gatt_evt_t event, wiced_bt_gatt_event_data_t *p_data)
{
    wiced_bt_gatt_status_t result = WICED_BT_GATT_INVALID_PDU;

    WICED_BT_TRACE("GATT callback event %d\n", event);

    switch(event)
    {
    case GATT_CONNECTION_STATUS_EVT:
        mesh_gatt_client_process_connection_status(&p_data->connection_status);
        break;

    case GATT_OPERATION_CPLT_EVT:
        result = mesh_gatt_client_gatt_operation_complete(&p_data->operation_complete);
        break;

    case GATT_DISCOVERY_RESULT_EVT:
        result = mesh_gatt_client_gatt_discovery_result(&p_data->discovery_result);
        break;

    case GATT_DISCOVERY_CPLT_EVT:
        result = mesh_gatt_client_gatt_discovery_complete(&p_data->discovery_complete);
        break;

    case GATT_ATTRIBUTE_REQUEST_EVT:
        result = mesh_gatt_client_gatts_req_callback(&p_data->attribute_request);
        break;

    default:
        break;
    }
    return result;
}

/*
 * This function will be called when a connection is established or dropped
 */
void mesh_gatt_client_process_connection_status(wiced_bt_gatt_connection_status_t *p_connection_status)
{
    wiced_bt_gatt_status_t result = WICED_BT_GATT_SUCCESS;

    WICED_BT_TRACE("gatt_conn_status conn_id:%04x connected:%d\n", p_connection_status->conn_id, p_connection_status->connected);

    if (p_connection_status->connected)
    {
        if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_IDLE)
        {
            WICED_BT_TRACE("gatt clnt connection not expected\n");
            return;
        }
        mesh_gatt_client_state.conn_id = p_connection_status->conn_id;

        mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE;

        // mesh_gatt_client_state.started         = WICED_FALSE;
        mesh_gatt_client_state.start_handle    = 0;
        mesh_gatt_client_state.end_handle      = 0;
        mesh_gatt_client_state.mtu             = 20;    // ATT MTU - 3 bytes for the GATT header
#if 0
        // ToDo, MTU should come in the wiced_bt_cfg.
        result = wiced_bt_gatt_configure_mtu (p_connection_status->conn_id, 75);
        WICED_BT_TRACE("configure MTU status:%d\n", result);
#else
        // perform primary service search
        mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_SERVICE;
        result = wiced_bt_util_send_gatt_discover(mesh_gatt_client_state.conn_id, GATT_DISCOVER_SERVICES_ALL, UUID_ATTRIBUTE_PRIMARY_SERVICE, 1, 0xffff);
        WICED_BT_TRACE("GATT discover result:%d\n", result);
#endif
    }
    else
    {
        mesh_gatt_client_link_status_send(WICED_FALSE);

        if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROXY)
        {
            wiced_bt_mesh_core_connection_status(mesh_gatt_client_state.conn_id, WICED_FALSE, 0, 0);
        }
        else if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROVISION)
        {
            // Tell provisioner library that the link is closed
            wiced_bt_mesh_core_provision_gatt_disconnected(p_connection_status->conn_id);
        }
        mesh_gatt_client_state.conn_id = 0;
        mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE;

        // restart processing advertisiment reports
        wiced_bt_ble_observe(WICED_TRUE, 0, mesh_adv_report);
    }
}


/*
 * The library needs to find 3 characteristics
 * remote control, entity update, and entity attribute.  The second has client
 * configuration descriptor (CCCD).
 */
wiced_bt_gatt_status_t mesh_gatt_client_gatt_discovery_result(wiced_bt_gatt_discovery_result_t *p_data)
{
    WICED_BT_TRACE("[%s] type:%d state:%d\n", __FUNCTION__, p_data->discovery_type, mesh_gatt_client_state.state);

    if (p_data->discovery_type  == GATT_DISCOVER_SERVICES_ALL)
    {
        if (p_data->discovery_data.group_value.service_type.len == 2)
        {
            WICED_BT_TRACE("s:% %04x e:%04x uuid:%04x\n", p_data->discovery_data.group_value.s_handle, p_data->discovery_data.group_value.e_handle, p_data->discovery_data.group_value.service_type.uu.uuid16);
            if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) &&
                (p_data->discovery_data.group_value.service_type.uu.uuid16 == WICED_BT_MESH_CORE_UUID_SERVICE_PROVISIONING))
            {
                WICED_BT_TRACE("Prov Service found s:%04x e:%04x\n", p_data->discovery_data.group_value.s_handle, p_data->discovery_data.group_value.e_handle);
                mesh_gatt_client_state.start_handle = p_data->discovery_data.group_value.s_handle;
                mesh_gatt_client_state.end_handle   = p_data->discovery_data.group_value.e_handle;
            }
            else if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY) &&
                     (p_data->discovery_data.group_value.service_type.uu.uuid16 == WICED_BT_MESH_CORE_UUID_SERVICE_PROXY))
            {
                WICED_BT_TRACE("Prov Service found s:%04x e:%04x\n", p_data->discovery_data.group_value.s_handle, p_data->discovery_data.group_value.e_handle);
                mesh_gatt_client_state.start_handle = p_data->discovery_data.group_value.s_handle;
                mesh_gatt_client_state.end_handle = p_data->discovery_data.group_value.e_handle;
            }
        }
    }
    else if (p_data->discovery_type == GATT_DISCOVER_CHARACTERISTICS)
    {
        // Result for characteristic discovery.  Save appropriate handle based on the UUID.
        wiced_bt_gatt_char_declaration_t *p_char = &p_data->discovery_data.characteristic_declaration;
        if (p_char->char_uuid.len == 2)
        {
            if (((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) &&
                 (p_char->char_uuid.uu.uuid16  == WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROVISIONING_DATA_IN)) ||
                ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY) &&
                 (p_char->char_uuid.uu.uuid16  == WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROXY_DATA_IN)))
            {
                WICED_BT_TRACE("data in hdl:%04x-%04x\n", p_char->handle, p_char->val_handle);
                mesh_gatt_client_state.data_in_handle       = p_char->handle;
                mesh_gatt_client_state.data_in_value_handle = p_char->val_handle;
            }
            else if (((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) &&
                      (p_char->char_uuid.uu.uuid16  == WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROVISIONING_DATA_OUT)) ||
                     ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY) &&
                      (p_char->char_uuid.uu.uuid16  == WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_PROXY_DATA_OUT)))
            {
                WICED_BT_TRACE("data out hdl:%04x-%04x\n", p_char->handle, p_char->val_handle);
                mesh_gatt_client_state.data_out_handle       = p_char->handle;
                mesh_gatt_client_state.data_out_value_handle = p_char->val_handle;
            }
        }
    }
    else if ((p_data->discovery_type == GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS) &&
             (p_data->discovery_data.char_descr_info.type.len == 2) &&
             (p_data->discovery_data.char_descr_info.type.uu.uuid16 == UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION))
    {
        WICED_BT_TRACE("data out cccd hdl:%04x\n", p_data->discovery_data.char_descr_info.handle);
        mesh_gatt_client_state.data_out_cccd_handle = p_data->discovery_data.char_descr_info.handle;
    }
}

/*
 * Process discovery complete from the stack
 */
wiced_bt_gatt_status_t mesh_gatt_client_gatt_discovery_complete(wiced_bt_gatt_discovery_complete_t *p_data)
{
    wiced_result_t result;
    uint16_t end_handle;

    WICED_BT_TRACE("[%s] conn %d type %d state %d\n", __FUNCTION__, p_data->conn_id, p_data->disc_type, mesh_gatt_client_state.state);

    if (p_data->disc_type == GATT_DISCOVER_SERVICES_ALL)
    {
        if ((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) ||
            (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY))
        {
            if ((mesh_gatt_client_state.start_handle == 0) || (mesh_gatt_client_state.end_handle == 0))
            {
                WICED_BT_TRACE("Serv not found: %04x-%04x\n", mesh_gatt_client_state.start_handle, mesh_gatt_client_state.end_handle);
                mesh_gatt_client_link_status_send(WICED_FALSE);
                return WICED_FALSE;
            }
            else
            {
                mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_CHARACTERISTICS;

                wiced_bt_util_send_gatt_discover(p_data->conn_id, GATT_DISCOVER_CHARACTERISTICS, 0, mesh_gatt_client_state.start_handle, mesh_gatt_client_state.end_handle);
            }
        }
    }
    else if (p_data->disc_type == GATT_DISCOVER_CHARACTERISTICS)
    {
        // done with Service characteristics, start reading descriptor handles
        // make sure that all characteristics are present
        if ((mesh_gatt_client_state.data_in_handle        == 0) ||
            (mesh_gatt_client_state.data_in_value_handle  == 0) ||
            (mesh_gatt_client_state.data_out_handle       == 0) ||
            (mesh_gatt_client_state.data_out_value_handle == 0))
        {
            // something is very wrong
            WICED_BT_TRACE("[%s] failed\n", __FUNCTION__);
            mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE;

            // ToDo notify client that connection failed
            // (*reg.p_discovery_complete_callback)(p_data->conn_id, WICED_FALSE);
            return WICED_BT_GATT_SUCCESS;
        }
        // search for descriptor from the characteristic value handle until the end of the
        // service or until the start of the next characteristic
        end_handle = mesh_gatt_client_state.end_handle;
        if (mesh_gatt_client_state.data_in_handle > mesh_gatt_client_state.data_out_handle)
            end_handle = mesh_gatt_client_state.data_in_handle - 1;

        mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_DESCRIPTORS;

        wiced_bt_util_send_gatt_discover(p_data->conn_id, GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS, UUID_DESCRIPTOR_CLIENT_CHARACTERISTIC_CONFIGURATION,
                mesh_gatt_client_state.data_out_value_handle + 1, end_handle);
    }
    else if (p_data->disc_type == GATT_DISCOVER_CHARACTERISTIC_DESCRIPTORS)
    {
        if (mesh_gatt_client_state.data_out_cccd_handle == 0)
        {
            // something is very wrong
            WICED_BT_TRACE("[%s] failed\n", __FUNCTION__);
            mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE;

            // ToDo notify client that connection failed
            // (*reg.p_discovery_complete_callback)(p_data->conn_id, WICED_FALSE);
            return WICED_BT_GATT_SUCCESS;
        }
        // Last step is to enable notification for Data In characteristic
        mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_WRITE_DESCRIPTORS;
        wiced_bt_util_set_gatt_client_config_descriptor(p_data->conn_id, mesh_gatt_client_state.data_out_cccd_handle, GATT_CLIENT_CONFIG_NOTIFICATION);

    }
    return WICED_BT_GATT_SUCCESS;
}


/*
 * GATT operation started by the client has been completed
 */
wiced_bt_gatt_status_t mesh_gatt_client_gatt_operation_complete(wiced_bt_gatt_operation_complete_t *p_data)
{
    switch (p_data->op)
    {
    case GATTC_OPTYPE_WRITE:
        mash_gatt_client_write_rsp_handler(p_data);
        break;

    case GATTC_OPTYPE_CONFIG:
        WICED_BT_TRACE("peer mtu:%d\n", p_data->response_data.mtu);
        mesh_gatt_client_state.mtu = p_data->response_data.mtu;
        // perform primary service search
        if (((mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) || 
             (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY)) &&
            (mesh_gatt_client_state.discovery_state == MESH_GATT_CLIENT_DISCOVERY_STATE_IDLE))
        {
            wiced_bt_gatt_status_t result;

            mesh_gatt_client_state.discovery_state = MESH_GATT_CLIENT_DISCOVERY_STATE_SERVICE;

            result = wiced_bt_util_send_gatt_discover(mesh_gatt_client_state.conn_id, GATT_DISCOVER_SERVICES_ALL, UUID_ATTRIBUTE_PRIMARY_SERVICE, 1, 0xffff);
            WICED_BT_TRACE("GATT discover result:%d\n", result);
        }
        break;

    case GATTC_OPTYPE_NOTIFICATION:
        mash_gatt_client_notification_handler(p_data);
        break;

    case GATTC_OPTYPE_READ:
    case GATTC_OPTYPE_INDICATION:
        WICED_BT_TRACE("This app does not support op:%d\n", p_data->op);
        break;
    }
    return WICED_BT_GATT_SUCCESS;
}


/*
 * Pass notification to appropriate client based on the attribute handle
 */
wiced_bt_gatt_status_t mash_gatt_client_notification_handler(wiced_bt_gatt_operation_complete_t *p_data)
{
    uint16_t handle = p_data->response_data.att_value.handle;
    uint16_t len    = p_data->response_data.att_value.len;
    uint8_t *data   = p_data->response_data.att_value.p_data;

    WICED_BT_TRACE("mesh gatt client notification handle:%04x len:%d state:%d\n", handle, len, mesh_gatt_client_state.state);

    if (mesh_gatt_client_state.data_out_value_handle == handle)
    {
        if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROXY)
        {
            wiced_bt_mesh_core_proxy_packet(data, len);
            return WICED_BT_GATT_SUCCESS;
        }
        else if (mesh_gatt_client_state.state == MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROVISION)
        {
            wiced_bt_mesh_core_provision_gatt_packet(mesh_gatt_client_state.conn_id, data, len);
            return WICED_BT_GATT_SUCCESS;
        }
    }
    WICED_BT_TRACE("ignored handle:%d state:%d\n", handle, mesh_gatt_client_state.state);
    return WICED_BT_GATT_INVALID_HANDLE;
}

/*
 * GATT write response process
 */
void mash_gatt_client_write_rsp_handler(wiced_bt_gatt_operation_complete_t *p_data)
{
    uint8_t *data = p_data->response_data.att_value.p_data;

    WICED_BT_TRACE("mash_gatt_client_write_rsp_handler: status 0x%02x client state 0x%02x\n", p_data->status, mesh_gatt_client_state.state);

    if ((mesh_gatt_client_state.state           == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROVISION) &&
        (mesh_gatt_client_state.discovery_state == MESH_GATT_CLIENT_DISCOVERY_STATE_WRITE_DESCRIPTORS))
    {
        // Send connection complete to the MCU
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROVISION;

        // Tell provisioner library that the link is already connected
        wiced_bt_mesh_provision_gatt_connected(mesh_gatt_client_state.conn_id, mesh_gatt_client_state.data_in_value_handle, mesh_gatt_client_state.data_out_value_handle, mesh_gatt_client_state.connecting_addr, mesh_gatt_client_state.connecting_uuid, mesh_gatt_client_state.identify_duration);

        mesh_gatt_client_link_status_send(WICED_TRUE);
    }
    else if ((mesh_gatt_client_state.state           == MESH_GATT_CLIENT_STATE_CONNECTING_TO_PROXY) &&
             (mesh_gatt_client_state.discovery_state == MESH_GATT_CLIENT_DISCOVERY_STATE_WRITE_DESCRIPTORS))
    {
        // Send connection complete to the MCU
        mesh_gatt_client_state.state = MESH_GATT_CLIENT_STATE_CONNECTED_TO_PROXY;

        // Tell Core library that the connection to proxy device is established
        wiced_bt_mesh_core_connection_status(mesh_gatt_client_state.conn_id, WICED_TRUE, mesh_gatt_client_state.data_in_value_handle, mesh_gatt_client_state.mtu);

        mesh_gatt_client_link_status_send(WICED_TRUE);
    }
}


/*
 * Typically we do not need to process any GATT requests
 */
wiced_bt_gatt_status_t mesh_gatt_client_gatts_req_callback(wiced_bt_gatt_attribute_request_t *p_data)
{
    WICED_BT_TRACE("GATT req not handled:%x\n", p_data->conn_id);
    return WICED_BT_GATT_INVALID_HANDLE;
}

void mesh_gatt_client_link_status_send(wiced_bool_t is_connected)
{
    wiced_bt_mesh_provision_link_status_data_t link_status;

    link_status.conn_id      = mesh_gatt_client_state.conn_id;
    link_status.addr         = mesh_gatt_client_state.connecting_addr;
    link_status.is_connected = is_connected;
    link_status.is_gatt      = WICED_TRUE;
    mesh_provisioner_hci_event_provision_link_status_send(&link_status);
}
