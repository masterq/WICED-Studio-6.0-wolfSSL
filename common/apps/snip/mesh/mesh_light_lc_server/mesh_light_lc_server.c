/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 * This file shows how to create a device which publishes Generic Power Level Server.
 *
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#include "wiced_timer.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

#define MESH_LIGHT_LIGHTNESS_TICK_IN_MS         100

#define WICED_BT_MESH_LIGHT_LIGHTNESS_DEFAULT   0
#define WICED_BT_MESH_LIGHT_LIGHTNESS_MIN       0
#define WICED_BT_MESH_LIGHT_LIGHTNESS_MAX       0xFFFF

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3106
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_light_lc_server_dev_name[]       = "Light Lightness Server";
uint8_t mesh_light_lc_server_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_light_lc_server_mfr_name[]       = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_light_lc_server_model_num[]      = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_light_lc_server_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };

uint16_t mesh_light_lc_ambinet_lux_level_on         = 65535;
uint16_t mesh_light_lc_ambinet_lux_level_prolong    = 1000;
uint16_t mesh_light_lc_ambinet_lux_level_standby    = 0;
uint16_t mesh_light_lc_lightness_on                 = 0x1000;
uint16_t mesh_light_lc_lightness_prolong            = 0x100;
uint16_t mesh_light_lc_lightness_standby            = 0; // 0x10;
uint8_t  mesh_light_lc_regulator_accuracy           = 50;
uint8_t  mesh_light_lc_regulator_kid                = 1;
uint8_t  mesh_light_lc_regulator_kiu                = 1;
uint8_t  mesh_light_lc_regulator_kpd                = 1;
uint8_t  mesh_light_lc_regulator_kpu                = 1;
uint32_t mesh_light_lc_time_fade                    = 0; // 1000;
uint32_t mesh_light_lc_time_fade_on                 = 0; // 1000;
uint32_t mesh_light_lc_time_fade_standby_auto       = 0; // 2000;
uint32_t mesh_light_lc_time_fade_standby_manual     = 0; // 1000;
uint32_t mesh_light_lc_time_occupany_delay          = 100;
uint32_t mesh_light_lc_time_prolong                 = 0; // 2000;
uint32_t mesh_light_lc_time_run_on                  = 3000;

wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_LIGHT_LIGHTNESS_SETUP_SERVER,
};

wiced_bt_mesh_core_config_model_t   mesh_element2_models[] =
{
    WICED_BT_MESH_MODEL_LIGHT_LC_SETUP_SERVER,
};

#define _USER_ WICED_BT_MESH_PROPERTY_TYPE_USER
#define _RW_   (WICED_BT_MESH_PROPERTY_ID_READABLE | WICED_BT_MESH_PROPERTY_ID_WRITABLE)

wiced_bt_mesh_core_config_property_t mesh_element2_properties[] =
{
    { WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_ON     , _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_ambinet_lux_level_on      },
    { WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_PROLONG, _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_ambinet_lux_level_prolong },
    { WICED_BT_MESH_PROPERTY_AMBIENT_LUX_LEVEL_STANDBY, _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_ambinet_lux_level_standby },
    { WICED_BT_MESH_PROPERTY_LIGHTNESS_ON             , _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_lightness_on              },
    { WICED_BT_MESH_PROPERTY_LIGHTNESS_PROLONG        , _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_lightness_prolong         },
    { WICED_BT_MESH_PROPERTY_LIGHTNESS_STANDBY        , _USER_, _RW_, 2, (uint8_t *)&mesh_light_lc_lightness_standby         },
    { WICED_BT_MESH_PROPERTY_REGULATOR_ACCURACY       , _USER_, _RW_, 1, (uint8_t *)&mesh_light_lc_regulator_accuracy        },
    { WICED_BT_MESH_PROPERTY_REGULATOR_KID            , _USER_, _RW_, 4, (uint8_t *)&mesh_light_lc_regulator_kid             },
    { WICED_BT_MESH_PROPERTY_REGULATOR_KIU            , _USER_, _RW_, 4, (uint8_t *)&mesh_light_lc_regulator_kiu             },
    { WICED_BT_MESH_PROPERTY_REGULATOR_KPD            , _USER_, _RW_, 4, (uint8_t *)&mesh_light_lc_regulator_kpd             },
    { WICED_BT_MESH_PROPERTY_REGULATOR_KPU            , _USER_, _RW_, 4, (uint8_t *)&mesh_light_lc_regulator_kpu             },
    { WICED_BT_MESH_PROPERTY_TIME_FADE                , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_fade                 },
    { WICED_BT_MESH_PROPERTY_TIME_FADE_ON             , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_fade_on              },
    { WICED_BT_MESH_PROPERTY_TIME_FADE_STANDBY_AUTO   , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_fade_standby_auto    },
    { WICED_BT_MESH_PROPERTY_TIME_FADE_STANDBY_MANUAL , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_fade_standby_manual  },
    { WICED_BT_MESH_PROPERTY_TIME_OCCUPANCY_DELAY     , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_occupany_delay       },
    { WICED_BT_MESH_PROPERTY_TIME_PROLONG             , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_prolong              },
    { WICED_BT_MESH_PROPERTY_TIME_RUN_ON              , _USER_, _RW_, 3, (uint8_t *)&mesh_light_lc_time_run_on               },
};
#define MESH_APP_NUM_PROPERTIES (sizeof(mesh_element2_properties) / sizeof(wiced_bt_mesh_core_config_property_t))


#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_LIGHT_LC_SERVER_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_DEFAULT,     // Default element behavior on power up
        .default_level = 0xffff,                                        // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = 0,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 1,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = MESH_APP_NUM_PROPERTIES,                      // Number of properties in the array models
        .properties = mesh_element2_properties,                         // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element2_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};


/******************************************************
 *          Structures
 ******************************************************/
typedef struct
{
#define MESH_TRANSITION_STATE_IDLE               0
#define MESH_TRANSITION_STATE_DELAY        1
#define MESH_TRANSITION_STATE_TRANSITION   2
    uint8_t              transition_state;
    int                  lightness_present;
    int                  lightness_target;
    int                  start_level;
    uint16_t             default_level;
    uint16_t             lightness_min;
    uint16_t             lightness_max;
    uint32_t             transition_start_time;
    uint32_t             transition_remaining_time;
    uint32_t             transition_time;
    uint16_t             delay;
//    wiced_timer_t        timer;
} mesh_light_lc_server_t;

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_light_lc_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_light_lc_server_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_light_lc_server_send_status(wiced_bt_mesh_event_t *p_event);
static void mesh_light_lc_process_set_level(uint8_t element_idx, wiced_bt_mesh_light_lightness_set_t *p_data);
static void mesh_light_lc_app_timer_callback(uint32_t arg);
static void mesh_light_lightness_server_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_light_lightness_server_send_status(wiced_bt_mesh_event_t *p_event);
static void mesh_light_lightness_process_set_level(uint8_t element_idx, wiced_bt_mesh_light_lightness_set_t *p_data);
static void mesh_light_lightness_process_set_range(uint8_t element_idx, wiced_bt_mesh_light_lightness_range_set_data_t *p_data);
static void mesh_light_lightness_server_default_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_light_lightness_server_range_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_light_lightness_server_send_default_status(wiced_bt_mesh_event_t *p_event);
static void mesh_light_lightness_server_send_range_status(wiced_bt_mesh_event_t *p_event, uint8_t status);
static void mesh_light_lc_server_mode_set(uint8_t *p_data, uint32_t length);
static void mesh_light_lc_server_occupancy_mode_set(uint8_t *p_data, uint32_t length);
static void mesh_light_lc_server_occupancy_detected(uint8_t *p_data, uint32_t length);
static void mesh_light_lc_server_onoff_set(uint8_t *p_data, uint32_t length);
static void mesh_light_lc_server_property_set(uint8_t *p_data, uint32_t length);

#ifdef HCI_CONTROL
static void mesh_light_lc_hci_event_send_lightness_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_set_t *p_data);
static void mesh_light_lc_hci_event_send_set_default(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_default_data_t *p_data);
static void mesh_light_lc_hci_event_send_set_lightness_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_range_set_data_t *p_data);
#endif

/******************************************************
 *          Variables Definitions
 ******************************************************/
// Application state
mesh_light_lc_server_t app_state;

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    memset (&app_state, 0, sizeof(app_state));
    app_state.transition_state = MESH_TRANSITION_STATE_IDLE;
    app_state.lightness_min    = mesh_config.elements[MESH_LIGHT_LC_SERVER_ELEMENT_INDEX].range_min;
    app_state.lightness_max    = mesh_config.elements[MESH_LIGHT_LC_SERVER_ELEMENT_INDEX].range_max;
    app_state.lightness_target = app_state.lightness_min;

    wiced_bt_mesh_model_light_lc_setup_server_init(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX, mesh_light_lc_server_message_handler, is_provisioned);
}

/*
 * Process event received from the models library.
 */
void mesh_light_lc_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    WICED_BT_TRACE("light lc srv msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_LIGHT_LIGHTNESS_GET:
        mesh_light_lightness_server_send_status(wiced_bt_mesh_create_reply_event(p_event));
        break;

    case WICED_BT_MESH_LIGHT_LIGHTNESS_SET:
#if defined HCI_CONTROL
        mesh_light_lc_hci_event_send_lightness_set(p_event, (wiced_bt_mesh_light_lightness_set_t *)p_data);
#endif
        mesh_light_lightness_process_set_level(p_event->element_idx, (wiced_bt_mesh_light_lightness_set_t *)p_data);
        if (p_event->reply)
        {
            mesh_light_lightness_server_send_status(wiced_bt_mesh_create_reply_event(p_event));
        }
        else
        {
            wiced_bt_mesh_release_event(p_event);
        }
        break;

    case WICED_BT_MESH_LIGHT_LIGHTNESS_SET_RANGE:
#if defined HCI_CONTROL
        mesh_light_lc_hci_event_send_set_lightness_range(p_event, (wiced_bt_mesh_light_lightness_range_set_data_t *)p_data);
#endif
        mesh_light_lightness_process_set_range(p_event->element_idx, (wiced_bt_mesh_light_lightness_range_set_data_t *)p_data);
        if (p_event->reply)
        {
            mesh_light_lightness_server_send_range_status(wiced_bt_mesh_create_reply_event(p_event), WICED_BT_MESH_STATUS_SUCCESS);
        }
        else
        {
            wiced_bt_mesh_release_event(p_event);
        }
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        wiced_bt_mesh_release_event(p_event);
        break;
    }
}

/*
 * In 2 chip solutions MCU can send commands to indicate that Level has changed.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
#ifdef HCI_CONTROL
    uint16_t dst         = p_data[0] + (p_data[1] << 8);
    uint16_t app_key_idx = p_data[2] + (p_data[3] << 8);

    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_LIGHT_LIGHTNESS_SET:
        mesh_light_lightness_server_status_changed(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LIGHTNESS_DEFAULT_SET:
        mesh_light_lightness_server_default_status_changed(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LIGHTNESS_RANGE_SET:
        mesh_light_lightness_server_range_status_changed(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_ONPOWERUP_SET:
        mesh_power_onoff_server_onpowerup_set(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX, p_data[0]);
        mesh_power_onoff_server_onpowerup_set(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1, p_data[0]);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LC_MODE_SET:
        mesh_light_lc_server_mode_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LC_OCCUPANCY_MODE_SET:
        mesh_light_lc_server_occupancy_mode_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LC_OCCUPANCY_SET:
        mesh_light_lc_server_occupancy_detected(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LC_ONOFF_SET:
        mesh_light_lc_server_onoff_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_LIGHT_LC_PROPERTY_SET:
        mesh_light_lc_server_property_set(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
#endif
    return 0;
}

/*
 * Create mesh event for unsolicited message and send Light Lightness Status event
 */
void mesh_light_lightness_server_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(app_state.lightness_present, p_data);
    STREAM_TO_UINT16(app_state.lightness_target, p_data);
    STREAM_TO_UINT32(app_state.transition_remaining_time, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("level state changed: no mem\n");
        return;
    }
    mesh_light_lc_server_send_status(p_event);
}

/*
 * Create mesh event for unsolicited message and send Power Default Status event
 */
void mesh_light_lightness_server_default_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(app_state.default_level, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh lc def state changed: no mem\n");
        return;
    }
    mesh_light_lightness_server_send_default_status(p_event);
}

/*
 * Create mesh event for unsolicited message and send Light Lightness Range Status event
 */
void mesh_light_lightness_server_range_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(app_state.lightness_min, p_data);
    STREAM_TO_UINT16(app_state.lightness_max, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_LIGHT_LIGHTNESS_SETUP_SRV, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh lc range state changed: no mem\n");
        return;
    }
    mesh_light_lightness_server_send_range_status(p_event, WICED_BT_MESH_STATUS_SUCCESS);
}

/*
 * Command from the level client is received to set the new level
 */
void mesh_light_lightness_process_set_level(uint8_t element_idx, wiced_bt_mesh_light_lightness_set_t *p_set)
{
    wiced_result_t res;

    WICED_BT_TRACE("mesh light srv set level state:%d level:%d trans time:%d delay:%d\n", app_state.transition_state, p_set->lightness_actual, p_set->transition_time, p_set->delay);

    // whenever we receive set, the previous transition is cancelled.
    app_state.transition_state  = MESH_TRANSITION_STATE_IDLE;
//    wiced_stop_timer(&app_state.timer);

    // Special time 0xFFFFFFFF indicates that transition should stop
    if (p_set->transition_time == WICED_BT_MESH_TRANSITION_TIME_DEFAULT)
    {
        app_state.lightness_target          = app_state.lightness_present;
        app_state.transition_remaining_time = 0;
    }
    // If transition time is 0, change the state immediately to the target state.
    else if (p_set->transition_time == 0)
    {
        app_state.lightness_target          = p_set->lightness_actual;
        app_state.lightness_present         = p_set->lightness_actual;
        app_state.transition_remaining_time = 0;
    }
    else
    {
        // Transition time is not 0.  Save parameters and start delay or transition itself.
        app_state.lightness_target      = p_set->lightness_actual;
        app_state.transition_time       = p_set->transition_time;
        app_state.delay                 = p_set->delay;
        app_state.transition_start_time = (uint32_t)wiced_bt_mesh_core_get_tick_count();
        app_state.start_level           = app_state.lightness_present;

        if (p_set->delay != 0)
        {
            app_state.transition_state  = MESH_TRANSITION_STATE_DELAY;
//            res = wiced_start_timer(&app_state.timer, app_state.delay);
        }
        else
        {
            app_state.transition_state          = MESH_TRANSITION_STATE_TRANSITION;
            app_state.transition_remaining_time = app_state.transition_time;
//            res = wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);
        }
        WICED_BT_TRACE("next state:%d start_timer_result:%d\n", app_state.transition_state, res);
    }
}

void mesh_light_lightness_process_set_range(uint8_t element_idx, wiced_bt_mesh_light_lightness_range_set_data_t *p_data)
{
    app_state.lightness_min = p_data->min_level;
    app_state.lightness_max = p_data->max_level;
}


/*
 * Command from the level client is received to set the new level
 */
void mesh_light_lc_process_set_level(uint8_t element_idx, wiced_bt_mesh_light_lightness_set_t *p_set)
{
    wiced_result_t res;

    WICED_BT_TRACE("mesh lc srv set level state:%d level:%d trans time:%d delay:%d\n", app_state.transition_state, p_set->lightness_actual, p_set->transition_time, p_set->delay);

    // whenever we receive set, the previous transition is cancelled.
    app_state.transition_state  = MESH_TRANSITION_STATE_IDLE;
//    wiced_stop_timer(&app_state.timer);

    // Special time 0xFFFFFFFF indicates that transition should stop
    if (p_set->transition_time == WICED_BT_MESH_TRANSITION_TIME_DEFAULT)
    {
        app_state.lightness_target          = app_state.lightness_present;
        app_state.transition_remaining_time = 0;
    }
    // If transition time is 0, change the state immediately to the target state.
    if (p_set->transition_time == 0)
    {
        app_state.lightness_target          = p_set->lightness_actual;
        app_state.lightness_present         = p_set->lightness_actual;
        app_state.transition_remaining_time = 0;
    }
    else
    {
        // Transition time is not 0.  Save parameters and start delay or transition itself.
        app_state.lightness_target      = p_set->lightness_actual;
        app_state.transition_time       = p_set->transition_time;
        app_state.delay                 = p_set->delay;
        app_state.transition_start_time = (uint32_t)wiced_bt_mesh_core_get_tick_count();
        app_state.start_level           = app_state.lightness_present;

        if (p_set->delay != 0)
        {
            app_state.transition_state  = MESH_TRANSITION_STATE_DELAY;
//            res = wiced_start_timer(&app_state.timer, app_state.delay);
        }
        else
        {
            app_state.transition_state          = MESH_TRANSITION_STATE_TRANSITION;
            app_state.transition_remaining_time = app_state.transition_time;
//            res = wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);
        }
        WICED_BT_TRACE("next state:%d start_timer_result:%d\n", app_state.transition_state, res);
    }
}

#if 0
/*
 * Transition timeout
 */
void mesh_light_lc_app_timer_callback(uint32_t arg)
{
    uint8_t element_idx = (uint8_t)arg;

    WICED_BT_TRACE("level timer state:%d remain:%d start:%d target:%d present:%d\n",
            app_state.transition_state, app_state.transition_remaining_time, app_state.start_level, app_state.lightness_target, app_state.lightness_present);

    switch (app_state.transition_state)
    {
    case MESH_TRANSITION_STATE_DELAY:
        // if it was a delay before actual transition, start transition now
        app_state.transition_state = MESH_TRANSITION_STATE_TRANSITION;

        app_state.transition_remaining_time = app_state.transition_time;
        if (app_state.transition_remaining_time != 0)
        {
            wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);
        }
        else
        {
            app_state.transition_state  = MESH_TRANSITION_STATE_IDLE;
            app_state.lightness_present = app_state.lightness_target;
        }
        break;

    case MESH_TRANSITION_STATE_TRANSITION:
        // check if we are done
        if (app_state.transition_remaining_time < MESH_LIGHT_LIGHTNESS_TICK_IN_MS)
        {
            app_state.transition_state          = MESH_TRANSITION_STATE_IDLE;
            app_state.transition_remaining_time = 0;
            app_state.lightness_present         = app_state.lightness_target;
        }
        else
        {
            // still in the transition, adjust remaining time and start timer for another tick
            app_state.transition_remaining_time -= MESH_LIGHT_LIGHTNESS_TICK_IN_MS;
            wiced_start_timer(&app_state.timer, MESH_LIGHT_LIGHTNESS_TICK_IN_MS);

            app_state.lightness_present = app_state.start_level +
                    ((app_state.lightness_target - app_state.start_level) *
                               (int)(app_state.transition_time - app_state.transition_remaining_time) / (int)app_state.transition_time);
        }
        break;
    }
}
#endif

/*
 * Send Light Lightness status message to the Client
 */
void mesh_light_lightness_server_send_status(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_light_lightness_status_data_t event;

    event.present = app_state.lightness_present;
    event.target  = app_state.lightness_target;
    if (app_state.transition_state == MESH_TRANSITION_STATE_DELAY)
        event.remaining_time = app_state.transition_time + app_state.delay - ((uint32_t)wiced_bt_mesh_core_get_tick_count() - app_state.transition_start_time);
    else
        event.remaining_time = app_state.transition_remaining_time;

    wiced_bt_mesh_model_light_lightness_server_send_status(p_event, &event);
}


/*
 * Send Light LC status message to the Client
 */
void mesh_light_lc_server_send_status(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_light_lightness_status_data_t event;

    event.present = app_state.lightness_present;
    event.target  = app_state.lightness_target;
    if (app_state.transition_state == MESH_TRANSITION_STATE_DELAY) 
        event.remaining_time = app_state.transition_time + app_state.delay - ((uint32_t)wiced_bt_mesh_core_get_tick_count() - app_state.transition_start_time);
    else
        event.remaining_time = app_state.transition_remaining_time;

    wiced_bt_mesh_model_light_lc_server_send_status(p_event, &event);
}

/*
 * Send Light Lightness Default status message to the Client
 */
void mesh_light_lightness_server_send_default_status(wiced_bt_mesh_event_t *p_event)
{
    wiced_bt_mesh_light_lightness_default_data_t event;

    event.default_level  = app_state.default_level;
    wiced_bt_mesh_model_light_lightness_server_send_default_status(p_event, &event);
}

/*
 * Send Light Lightness Range status message to the Client
 */
void mesh_light_lightness_server_send_range_status(wiced_bt_mesh_event_t *p_event, uint8_t status)
{
    wiced_bt_mesh_light_lightness_range_status_data_t event;

    event.status    = status;
    event.min_level = app_state.lightness_min;
    event.max_level = app_state.lightness_max;
    wiced_bt_mesh_model_light_lightness_server_send_range_status(p_event, &event);
}

/*
 * Light LC Mode changed notification
 */
void mesh_light_lc_server_mode_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_light_lc_mode_set_data_t set_data;

    p_data += 5; // skip dst, app_inx and reply

    STREAM_TO_UINT8(set_data.mode, p_data);

    wiced_bt_mesh_model_light_lc_mode_changed(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1, &set_data);
}

/*
 * Light LC Occupancy Mode changed notification
 */
void mesh_light_lc_server_occupancy_mode_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_light_lc_occupancy_mode_set_data_t set_data;

    p_data += 5; // skip dst, app_inx and reply

    STREAM_TO_UINT8(set_data.mode, p_data);

    wiced_bt_mesh_model_light_lc_occupancy_mode_changed(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1, &set_data);
}

/*
 * Light LC Occupancy Detected notification
 */
void mesh_light_lc_server_occupancy_detected(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_model_light_lc_occupancy_detected(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1);
}

/*
 * Light LC OnOff changed notification
 */
void mesh_light_lc_server_onoff_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_light_lc_light_onoff_status_data_t set_data;
    uint16_t dst;
    uint16_t app_key_idx;

    p_data += 5; // skip dst, app_inx and reply

    STREAM_TO_UINT8(set_data.present_onoff, p_data);
    set_data.target_onoff   = set_data.present_onoff;
    set_data.remaining_time = 0;

    wiced_bt_mesh_model_light_lc_onoff_changed(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1, &set_data);
}

/*
 * Light LC Property changed notification
 */
void mesh_light_lc_server_property_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_light_lc_property_set_data_t set_data;
    uint16_t dst;
    uint16_t app_key_idx;

    p_data += 5; // skip dst, app_inx and reply

    STREAM_TO_UINT16(set_data.id, p_data);
    set_data.len = length - 7;
    memcpy(set_data.value, p_data, set_data.len);

    wiced_bt_mesh_model_light_lc_property_changed(MESH_LIGHT_LC_SERVER_ELEMENT_INDEX + 1, &set_data);
}

#ifdef HCI_CONTROL
/*
 * Send Light LC Set lightness event over transport
 */
void mesh_light_lc_hci_event_send_lightness_set(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_set_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->lightness_actual);
    UINT32_TO_STREAM(p, p_data->transition_time);
    UINT16_TO_STREAM(p, p_data->delay);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LIGHT_LIGHTNESS_SET, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Light LC Set lightness range event over transport
 */
void mesh_light_lc_hci_event_send_set_lightness_range(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_light_lightness_range_set_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->min_level);
    UINT16_TO_STREAM(p, p_data->max_level);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_LIGHT_LIGHTNESS_RANGE_SET, buffer, (uint16_t)(p - buffer));
}

#endif
