/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 *
 * This file shows how to create a device which implements mesh user sensor client.
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"

#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3006
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

#define SENSOR_PROPERTY_ID_SIZE                     (2)
#define PROPERTY_ID_MOTION_SENSED                   (0x42)
#define PROPERTY_ID_TOTAL_LIGHT_EXPOSURE_TIME       (0x6F)
#define PROPERTY_ID_PRESENT_AMBIENT_LIGHT_LEVEL     (0x4E)
#define SETTING_PROPERTY_ID                         (0x2001)

wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
        WICED_BT_MESH_DEVICE,
        WICED_BT_MESH_MODEL_SENSOR_CLIENT,
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_SENSOR_CLIENT_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_property_t mesh_element1_properties[] =
{
    { PROPERTY_ID_MOTION_SENSED,               0, 0, 1, NULL},
    { PROPERTY_ID_PRESENT_AMBIENT_LIGHT_LEVEL, 0, 0, 3, NULL},
    { PROPERTY_ID_TOTAL_LIGHT_EXPOSURE_TIME,   0, 0, 3, NULL},
    { SETTING_PROPERTY_ID,                     0, 0, 2, NULL},
};

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .properties_num = 4,                                            // Number of properties in the array models
        .properties = mesh_element1_properties,                         // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

uint8_t mesh_num_elements = 1;

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};


/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_sensor_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_sensor_descriptor_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_column_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_cadence_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_series_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_cadence_set(uint8_t *p_data, uint32_t data_len);
static void mesh_sensor_setting_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_setting_set(uint8_t *p_data, uint32_t length);
static void mesh_sensor_settings_get(uint8_t *p_data, uint32_t length);
static void mesh_sensor_series_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_series_status_data_t *p_data);
static void mesh_sensor_desc_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_descriptor_status_data_t *p_data);
static void mesh_sensor_data_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_status_data_t *p_data);
static void mesh_sensor_column_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_column_status_data_t *p_data);
static void mesh_sensor_cadence_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_status_data_t *cadence_status );
static void mesh_sensor_setting_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_status_data_t *p_data);
static void mesh_sensor_settings_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_settings_status_data_t *p_data);
/******************************************************
 *          Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    // register with the library to receive parsed data
    wiced_bt_mesh_model_sensor_client_init(MESH_SENSOR_CLIENT_ELEMENT_INDEX, mesh_sensor_client_message_handler, is_provisioned);
}

/*
 * Process event received from the sensor Server.
 */
void mesh_sensor_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    WICED_BT_TRACE("sensor clt msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_SENSOR_DESCRIPTOR_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_desc_hci_event_send(p_event, (wiced_bt_mesh_sensor_descriptor_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_data_hci_event_send(p_event, (wiced_bt_mesh_sensor_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_COLUMN_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_column_hci_event_send(p_event, (wiced_bt_mesh_sensor_column_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_SERIES_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_series_hci_event_send(p_event, (wiced_bt_mesh_sensor_series_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_CADENCE_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_cadence_hci_event_send(p_event, (wiced_bt_mesh_sensor_cadence_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_SETTINGS_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_settings_hci_event_send(p_event, (wiced_bt_mesh_sensor_settings_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_SENSOR_SETTING_STATUS:
#if defined HCI_CONTROL
        mesh_sensor_setting_hci_event_send(p_event, (wiced_bt_mesh_sensor_setting_status_data_t *)p_data);
#endif
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
    wiced_bt_mesh_release_event(p_event);
}


/*
 * In 2 chip solutions MCU can send commands to change sensor state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
        //sensor server messages
    case HCI_CONTROL_MESH_COMMAND_SENSOR_DESCRIPTOR_GET:
        mesh_sensor_descriptor_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_GET:
        mesh_sensor_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_COLUMN_GET:
        mesh_sensor_column_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_SERIES_GET:
        mesh_sensor_series_get(p_data, length);
        break;
           //sensor setup server messages
    case HCI_CONTROL_MESH_COMMAND_SENSOR_CADENCE_GET:
        mesh_sensor_cadence_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_CADENCE_SET:
        mesh_sensor_cadence_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_SETTING_GET:
        mesh_sensor_setting_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_SETTING_SET:
        mesh_sensor_setting_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_SENSOR_SETTINGS_GET:
        mesh_sensor_settings_get(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }

    return 0;
}

/*
 * Create mesh event for unsolicited message and send sensor descriptor get command
 */
void mesh_sensor_descriptor_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_descriptor_get\n");
    wiced_bt_mesh_event_t *p_event;

    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_get_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    if ((length - 4) == SENSOR_PROPERTY_ID_SIZE)
    {
        STREAM_TO_UINT16(get_data.property_id, p_data);
    }
    else
    {
        get_data.property_id = 0;
    }

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor state changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_descriptor_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send sensor get command
 */
void mesh_sensor_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_get_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(get_data.property_id, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("sensor state changed: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send column get command
 */
void mesh_sensor_column_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_column_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_column_get_data_t get_data;
    int i;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(get_data.property_id, p_data);
    STREAM_TO_UINT8(get_data.prop_value_len, p_data);
    STREAM_TO_ARRAY(get_data.raw_valuex, p_data, get_data.prop_value_len);
    WICED_BT_TRACE("\n-------------------------\n");
    for(i=0; i<get_data.prop_value_len ;i++)
        WICED_BT_TRACE(" %x ",get_data.raw_valuex[i]);
    WICED_BT_TRACE("\n-------------------------\n");

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_column_get : sensor clnt get: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_column_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send series get command
 */
void mesh_sensor_series_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_series_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_series_get_data_t get_data;
    uint8_t header;
	int i;

    memset(&get_data, 0, sizeof(wiced_bt_mesh_sensor_series_get_data_t));

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    header = 2 * sizeof(uint16_t);

    if ((length - header) == SENSOR_PROPERTY_ID_SIZE)
    {
        WICED_BT_TRACE("\n property id only\n");
        STREAM_TO_UINT16(get_data.property_id, p_data);
        get_data.start_index = 0x00;
        get_data.end_index = 0xFF;
    }
    else
    {
        WICED_BT_TRACE("\n property id \n");
        STREAM_TO_UINT16(get_data.property_id, p_data);
        STREAM_TO_UINT8(get_data.prop_value_len, p_data);
        STREAM_TO_ARRAY(get_data.raw_valuex1, p_data, get_data.prop_value_len);
        STREAM_TO_ARRAY(get_data.raw_valuex2, p_data, get_data.prop_value_len);
    }
    WICED_BT_TRACE("\n-------------------------\n");
    for(i=0; i<get_data.prop_value_len ;i++)
        WICED_BT_TRACE(" %x ",get_data.raw_valuex1[i]);
    WICED_BT_TRACE("\n-------------------------\n");
    for(i=0; i<get_data.prop_value_len ;i++)
        WICED_BT_TRACE(" %x ",get_data.raw_valuex2[i]);
    WICED_BT_TRACE("\n-------------------------\n");

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_series_get : sensor clnt get: no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_series_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send cadence get command
 */
void mesh_sensor_cadence_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_cadence_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_get_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(get_data.property_id, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_cadence_get : no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_cadence_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send cadence set command
 */

void mesh_sensor_cadence_set(uint8_t *p_data, uint32_t data_len)
{

    wiced_bt_mesh_sensor_cadence_set_data_t cadence_set;
    uint16_t prop_id;
    uint8_t  buffer[2];
    uint8_t  *p;
    uint8_t  prop_val_len;
    uint8_t  fast_cadence_period_divisor;
    uint8_t trigger_type;
    uint8_t val_len;
    uint16_t trigger_val_down;
    uint16_t trigger_val_up;
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;

    WICED_BT_TRACE("%s\n", __FUNCTION__);



    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_cadence_set : no mem\n");
        return;
    }

    STREAM_TO_UINT8(p_event->reply, p_data);

    STREAM_TO_UINT16(prop_id, p_data);
    STREAM_TO_UINT8(prop_val_len, p_data);

    cadence_set.property_id = prop_id;
    cadence_set.prop_value_len = prop_val_len;
    STREAM_TO_UINT8(fast_cadence_period_divisor, p_data);
    cadence_set.cadence_data.fast_cadence_period_divisor = fast_cadence_period_divisor;
    STREAM_TO_UINT8(trigger_type, p_data);
    cadence_set.cadence_data.trigger_type = (trigger_type) ? WICED_TRUE : WICED_FALSE ;



    if (cadence_set.cadence_data.trigger_type)
    {
       val_len = sizeof(uint16_t);
       STREAM_TO_ARRAY(cadence_set.cadence_data.trigger_delta_down, p_data, val_len);
       STREAM_TO_ARRAY(cadence_set.cadence_data.trigger_delta_up, p_data, val_len);

    }
    else
    {
       //The format shall be defined by the Format Type of the characteristic that the Sensor Property ID
       val_len = prop_val_len;
       STREAM_TO_ARRAY(cadence_set.cadence_data.trigger_delta_down, p_data, val_len);
       STREAM_TO_ARRAY(cadence_set.cadence_data.trigger_delta_up, p_data, val_len);

    }

    STREAM_TO_UINT8(cadence_set.cadence_data.min_interval, p_data);

    val_len = prop_val_len;
    STREAM_TO_ARRAY(cadence_set.cadence_data.fast_cadence_low, p_data, val_len);
    STREAM_TO_ARRAY(cadence_set.cadence_data.fast_cadence_high, p_data, val_len);

    wiced_bt_mesh_model_sensor_client_sensor_cadence_send_set(p_event, &cadence_set);

}

/*
 * Create mesh event for unsolicited message and send setting get command
 */
void mesh_sensor_setting_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_setting_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_setting_get_data_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(get_data.property_id, p_data);
    STREAM_TO_UINT16(get_data.setting_property_id, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_setting_get : no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_setting_send_get(p_event, &get_data);
}

/*
 * Create mesh event for unsolicited message and send setting set command
 */
void mesh_sensor_setting_set(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_setting_set\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_setting_set_data_t set_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("mesh_sensor_setting_get : sensor get: no mem\n");
        return;
    }
    STREAM_TO_UINT8(p_event->reply, p_data);

    STREAM_TO_UINT16(set_data.property_id, p_data);
    STREAM_TO_UINT16(set_data.setting_property_id, p_data);
    STREAM_TO_UINT8(set_data.prop_value_len, p_data);
    STREAM_TO_ARRAY(set_data.setting_raw_val, p_data, set_data.prop_value_len);

    wiced_bt_mesh_model_sensor_client_sensor_setting_send_set(p_event, &set_data);
}

/*
 * Create mesh event for unsolicited message and send settings get command
 */
void mesh_sensor_settings_get(uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("mesh_sensor_settings_get\n");
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    wiced_bt_mesh_sensor_get_t get_data;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);

    STREAM_TO_UINT16(get_data.property_id, p_data);

    p_event = wiced_bt_mesh_create_event(MESH_SENSOR_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_SENSOR_CLNT, dst, app_key_idx);
    if (p_event == NULL)
    {
        WICED_BT_TRACE("no mem\n");
        return;
    }
    wiced_bt_mesh_model_sensor_client_sensor_settings_send_get(p_event, &get_data);
}

#ifdef HCI_CONTROL
/*
 * Send Descriptor Status event over transport
 */
void mesh_sensor_desc_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_descriptor_status_data_t *p_data)
{
    int i;
    uint8_t *p_buffer;
    uint8_t *p;

    if ((p_buffer = (uint8_t *)wiced_bt_get_buffer(5 + p_data->num_descriptors * 9)) != NULL)
    {
        p = p_buffer;

        UINT16_TO_STREAM(p, p_event->src);
        UINT8_TO_STREAM(p, p_event->app_key_idx);
        UINT8_TO_STREAM(p, p_event->element_idx);

        WICED_BT_TRACE("mesh_sensor_desc_hci_event_send: num descriptors %x\n", p_data->num_descriptors);
        UINT8_TO_STREAM(p, p_data->num_descriptors);
        if (p_data->num_descriptors != 0)
        {
            for (i = 0; i < p_data->num_descriptors; i++)
            {
                WICED_BT_TRACE("property_id :  %x\n", p_data->descriptor_list[i].property_id);
                WICED_BT_TRACE("positive_tolerance :  %x\n", p_data->descriptor_list[i].positive_tolerance);
                WICED_BT_TRACE("negative_tolerance :  %x\n", p_data->descriptor_list[i].negative_tolerance);
                WICED_BT_TRACE("sampling_function :  %x\n", p_data->descriptor_list[i].sampling_function);
                WICED_BT_TRACE("measurement_period :  %x\n", p_data->descriptor_list[i].measurement_period);
                WICED_BT_TRACE("update_interval :  %x\n", p_data->descriptor_list[i].update_interval);
                UINT16_TO_STREAM(p, p_data->descriptor_list[i].property_id);
                UINT16_TO_STREAM(p, p_data->descriptor_list[i].positive_tolerance);
                UINT16_TO_STREAM(p, p_data->descriptor_list[i].negative_tolerance);
                UINT8_TO_STREAM(p, p_data->descriptor_list[i].sampling_function);
                UINT8_TO_STREAM(p, p_data->descriptor_list[i].measurement_period);
                UINT8_TO_STREAM(p, p_data->descriptor_list[i].update_interval);
            }
        }
        else
        {
            WICED_BT_TRACE("mesh_sensor_desc_get : no descriptor present for property ID\n");
        }

        wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_DESCRIPTOR_STATUS, p_buffer, (uint16_t)(p - p_buffer));
        wiced_bt_free_buffer(p_buffer);
    }
}

/*
 * Send sensor Status event over transport
 */
void mesh_sensor_data_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_status_data_t *p_data)
{
    WICED_BT_TRACE("mesh_sensor_data_hci_event_send :%d\n",p_data->num_status);

    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    int i, j;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT8_TO_STREAM(p, p_data->num_status);
    WICED_BT_TRACE(" num_status:%x\n",p_data->num_status);
    for (i = 0; i < p_data->num_status; i++)
    {
        WICED_BT_TRACE(" property_id:%x\n",p_data->status_list[i].property_id);
        WICED_BT_TRACE(" prop_value_len:%x\n", p_data->status_list[i].prop_value_len);
        WICED_BT_TRACE("\n Raw val \n");

        for (j = 0; j < p_data->status_list[i].prop_value_len; j++)
            WICED_BT_TRACE(" %x ",p_data->status_list[i].raw_value[j]);

        UINT16_TO_STREAM(p, p_data->status_list[i].property_id);
        UINT8_TO_STREAM(p, p_data->status_list[i].prop_value_len);
        ARRAY_TO_STREAM(p, p_data->status_list[i].raw_value, p_data->status_list[i].prop_value_len);
        WICED_BT_TRACE("\n ---------------- \n");
    }
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send sensor column status event over transport
 */
void mesh_sensor_column_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_column_status_data_t *p_data)
{
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    int i;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->property_id);
    UINT8_TO_STREAM(p, p_data->prop_value_len);

    ARRAY_TO_STREAM(p, p_data->column_data.raw_valuex, p_data->prop_value_len);
    ARRAY_TO_STREAM(p, p_data->column_data.column_width, p_data->prop_value_len);
    ARRAY_TO_STREAM(p, p_data->column_data.raw_valuey, p_data->prop_value_len);

    WICED_BT_TRACE(" property_id:%x\n",p_data->property_id);
    WICED_BT_TRACE(" prop_value_len:%x\n",p_data->prop_value_len);
    WICED_BT_TRACE("\n -----RAW VAL X----------- \n");
    for (i=0; i < p_data->prop_value_len; i++)
        WICED_BT_TRACE(" %x ",p_data->column_data.raw_valuex[i]);
    WICED_BT_TRACE("\n -----COL WIDTH----------- \n");
    for (i=0; i < p_data->prop_value_len; i++)
           WICED_BT_TRACE(" %x ",p_data->column_data.column_width[i]);
    WICED_BT_TRACE("\n -----RAW VAL Y----------- \n");
    for (i=0; i < p_data->prop_value_len; i++)
           WICED_BT_TRACE(" %x ",p_data->column_data.raw_valuey[i]);
    WICED_BT_TRACE("\n ------------------------ \n");
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_COLUMN_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send sensor series status event over transport
 */
void mesh_sensor_series_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_series_status_data_t *p_data)
{
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    int i, j;
    WICED_BT_TRACE(" property_id:%x\n",p_data->property_id);
    WICED_BT_TRACE(" prop_value_len:%x\n",p_data->prop_value_len);
    WICED_BT_TRACE(" no_of_columns:%x\n",p_data->no_of_columns);

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->property_id);
    UINT8_TO_STREAM(p, p_data->prop_value_len);
    UINT8_TO_STREAM(p, p_data->no_of_columns);

    for(i=0; i<p_data->no_of_columns; i++)
    {
        ARRAY_TO_STREAM(p, p_data->column_list->raw_valuex, p_data->prop_value_len);
        ARRAY_TO_STREAM(p, p_data->column_list->column_width, p_data->prop_value_len);
        ARRAY_TO_STREAM(p, p_data->column_list->raw_valuey, p_data->prop_value_len);

        WICED_BT_TRACE("\n -----RAW VAL X----------- \n");
        for (j=0; j <  p_data->prop_value_len; j++)
            WICED_BT_TRACE(" %x ", p_data->column_list[i].raw_valuex[j]);
        WICED_BT_TRACE("\n -----COL WIDTH----------- \n");
        for (j=0; j < p_data->prop_value_len; j++)
               WICED_BT_TRACE(" %x ",p_data->column_list[i].column_width[j]);
        WICED_BT_TRACE("\n -----RAW VAL Y----------- \n");
        for (j=0; j < p_data->prop_value_len; j++)
               WICED_BT_TRACE(" %x ",p_data->column_list[i].raw_valuey[j]);
        WICED_BT_TRACE("\n ------------------------ \n");
    }

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_SERIES_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send sensor cadence status event over transport
 */

void mesh_sensor_cadence_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_cadence_status_data_t *cadence_status )
{
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer ;
    uint8_t fast_cad_period_div;
    uint8_t trigger_type;
    uint16_t trigger_delta_down;
    uint16_t trigger_delta_up;
    int i;

    WICED_BT_TRACE("%s\n", __FUNCTION__);

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, cadence_status->property_id);
    WICED_BT_TRACE(" property_id:%x\n",cadence_status->property_id);

    if (cadence_status->is_data_present)
    {
        UINT8_TO_STREAM(p, cadence_status->prop_value_len);
        WICED_BT_TRACE(" prop_value_len:%x\n",cadence_status->prop_value_len);

       fast_cad_period_div = cadence_status->cadence_data.fast_cadence_period_divisor;
       trigger_type = (cadence_status->cadence_data.trigger_type ? 1 : 0 );

       UINT8_TO_STREAM(p,fast_cad_period_div);
       UINT8_TO_STREAM(p,trigger_type);
       WICED_BT_TRACE(" fast_cad_period_div:%x\n",fast_cad_period_div);
       WICED_BT_TRACE(" trigger_type:%x\n",trigger_type);

       if(cadence_status->cadence_data.trigger_type)
       {
           //unit is �unitless�
           ARRAY_TO_STREAM(p,cadence_status->cadence_data.trigger_delta_down, sizeof(uint16_t));
           ARRAY_TO_STREAM(p,cadence_status->cadence_data.trigger_delta_up,sizeof(uint16_t));
           WICED_BT_TRACE("\trigger_delta_down\n");
  for (i=0; i<sizeof(uint16_t); i++)
               WICED_BT_TRACE(" %x ",cadence_status->cadence_data.trigger_delta_down[i]);
           WICED_BT_TRACE("\trigger_delta_up\n");
           for (i=0; i<sizeof(uint16_t); i++)
               WICED_BT_TRACE(" %x ",cadence_status->cadence_data.trigger_delta_up[i]);
       }
       else
       {
           // format shall be defined by the Format Type of the characteristic that the Sensor Property ID
           ARRAY_TO_STREAM(p,cadence_status->cadence_data.trigger_delta_down, cadence_status->prop_value_len);
           ARRAY_TO_STREAM(p,cadence_status->cadence_data.trigger_delta_up, cadence_status->prop_value_len);
           WICED_BT_TRACE("\trigger_delta_down\n");
           for (i=0; i<cadence_status->prop_value_len; i++)
               WICED_BT_TRACE(" %x ",cadence_status->cadence_data.trigger_delta_down[i]);
           WICED_BT_TRACE("\trigger_delta_up\n");
           for (i=0; i<cadence_status->prop_value_len; i++)
               WICED_BT_TRACE(" %x ",cadence_status->cadence_data.trigger_delta_up[i]);
       }
       UINT8_TO_STREAM(p,cadence_status->cadence_data.min_interval);
       ARRAY_TO_STREAM(p,cadence_status->cadence_data.fast_cadence_high, cadence_status->prop_value_len);
       ARRAY_TO_STREAM(p,cadence_status->cadence_data.fast_cadence_low, cadence_status->prop_value_len);
     }

    WICED_BT_TRACE(" min interval:%x\n",cadence_status->cadence_data.min_interval);
    WICED_BT_TRACE("\nfast_cadence_high\n");
    for (i=0; i<cadence_status->prop_value_len; i++)
           WICED_BT_TRACE(" %x ",cadence_status->cadence_data.fast_cadence_high[i]);
    WICED_BT_TRACE("\nfast_cadence_low\n");
    for (i=0; i<cadence_status->prop_value_len; i++)
              WICED_BT_TRACE(" %x ",cadence_status->cadence_data.fast_cadence_low[i]);


    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_CADENCE_STATUS, buffer, (uint16_t)(p - buffer));

}

/*
 * Send sensor setting status event over transport
 */
void mesh_sensor_setting_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_setting_status_data_t *p_data)
{
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    int i;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->property_id);
    UINT8_TO_STREAM(p, p_data->setting.setting_property_id);
    UINT8_TO_STREAM(p, p_data->setting.access);
    UINT8_TO_STREAM(p, p_data->setting.value_len);
    ARRAY_TO_STREAM(p, p_data->setting.val, p_data->setting.value_len);

    WICED_BT_TRACE(" property_id:%x\n", p_data->property_id);
    WICED_BT_TRACE(" setting_property_id:%x\n",p_data->setting.setting_property_id);
    WICED_BT_TRACE(" access:%x\n",p_data->setting.access);
    WICED_BT_TRACE(" value_len:%x\n", p_data->setting.value_len);
    for (i=0; i<p_data->setting.value_len; i++)
        WICED_BT_TRACE(" %x ",p_data->setting.val[i]);

    WICED_BT_TRACE("\n----------\n");
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_SETTING_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send sensor settings status event over transport
 */
void mesh_sensor_settings_hci_event_send(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_sensor_settings_status_data_t *p_data)
{
    uint8_t buffer[WICED_BT_MESH_MAX_SENSOR_PAYLOAD_LEN];
    uint8_t *p = buffer;
    int i;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->property_id);
    UINT8_TO_STREAM(p, p_data->num_setting_property_id);
    ARRAY_TO_STREAM(p, p_data->setting_property_id_list,sizeof(uint16_t)*p_data->num_setting_property_id);

    WICED_BT_TRACE(" property_id:%x\n", p_data->property_id);
    WICED_BT_TRACE(" num_setting_property_id:%x\n",p_data->num_setting_property_id);

    for (i = 0; i < p_data->num_setting_property_id; i++)
        WICED_BT_TRACE(" %x ",p_data->setting_property_id_list[i]);

    WICED_BT_TRACE("\n----------\n",p_data->num_setting_property_id);
    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_SENSOR_SETTINGS_STATUS, buffer, (uint16_t)(p - buffer));
}

#endif
