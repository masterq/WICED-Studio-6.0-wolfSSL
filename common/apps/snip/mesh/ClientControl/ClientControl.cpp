
// ClientControl.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "stdint.h"
//#include "wiced.h"
#include "ClientControl.h"
#include "ClientControlDlg.h"
#include "resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#if 0
uint64_t TickCountInitValue;
#endif

// CClientControlApp

BEGIN_MESSAGE_MAP( CClientControlApp, CWinApp )
	ON_COMMAND( ID_HELP, &CWinApp::OnHelp )
END_MESSAGE_MAP( )


// CClientControlApp construction

CClientControlApp::CClientControlApp( )
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CClientControlApp object

CClientControlApp theApp;

class CClientControlSheet : public CPropertySheetEx
{
public:
    CClientControlSheet(LPTSTR title);
    virtual BOOL OnInitDialog();
};

CClientControlSheet::CClientControlSheet(LPTSTR title)
    : CPropertySheetEx(title)
{
}


BOOL CClientControlSheet::OnInitDialog()
{
//    CPropertyPage * a = GetActivePage();
//    a->OnInitDialog();
    return TRUE;
}

// CClientControlApp initialization

BOOL CClientControlApp::InitInstance( )
{
    // InitCommonControlsEx( ) is required on Windows XP if an application
    // manifest specifies use of ComCtl32.dll version 6 or later to enable
    // visual styles.  Otherwise, any window creation will fail.
    INITCOMMONCONTROLSEX InitCtrls;
    InitCtrls.dwSize = sizeof( InitCtrls );
    // Set this to include all the common control classes you want to use
    // in your application.
    InitCtrls.dwICC = ICC_WIN95_CLASSES;
    InitCommonControlsEx( &InitCtrls );

    CWinApp::InitInstance( );

    // Create the shell manager, in case the dialog contains
    // any shell tree view or shell list view controls.
    CShellManager *pShellManager = new CShellManager;

    // Activate "Windows Native" visual manager for enabling themes in MFC controls
    CMFCVisualManager::SetDefaultManager( RUNTIME_CLASS( CMFCVisualManagerWindows ) );

    CCommandLineInfo cmdInfo;
    ParseCommandLine( cmdInfo );

    // Change the registry key under which our settings are stored
    SetRegistryKey( _T( "Cypress" ) );

#if 0
    TickCountInitValue = GetTickCount64() / 1000;
#endif

    CClientDialog dlg(_T("Mesh Client Control"));
    m_pMainWnd = &dlg;
    dlg.DoModal();
    /*
    CClientControlDlg dlg;
    m_pMainWnd = &dlg;
    INT_PTR nResponse = dlg.DoModal( );
    if ( nResponse == IDOK )
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with OK
    }
    else if ( nResponse == IDCANCEL )
    {
        // TODO: Place code here to handle when the dialog is
        //  dismissed with Cancel
    }
    else if ( nResponse == -1 )
    {
        TRACE( traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n" );
        TRACE( traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n" );
    }
    */
    // Delete the shell manager created above.
    if ( pShellManager != NULL )
    {
        delete pShellManager;
    }

    // Since the dialog has been closed, return FALSE so that we exit the
    //  application, rather than start the application's message pump.
    return FALSE;
}

IMPLEMENT_DYNAMIC(CClientDialog, CPropertySheet)

CClientDialog::CClientDialog(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
    :CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
    m_psh.dwFlags &= ~PSH_HASHELP;
    AddPage(&pageMain);
    AddPage(&pageConfig);
}

CClientDialog::CClientDialog(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
    :CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
    m_psh.dwFlags &= ~PSH_HASHELP;
    AddPage(&pageMain);
    AddPage(&pageConfig);
}

CClientDialog::~CClientDialog()
{
}


BEGIN_MESSAGE_MAP(CClientDialog, CPropertySheet)
    //{{AFX_MSG_MAP(COptions)
    //ON_COMMAND(ID_APPLY_NOW, OnApplyNow)
    //ON_COMMAND(IDOK, OnOK)
    //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////

#if 0
#include "wiced_result.h"
#include "wiced.h"
#include "wiced_timer.h"
#include "wiced_bt_mesh_cfg.h"

char* szRegKey = "Software\\Cypress\\Mesh";

#define MESH_PID                0x3106
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

wiced_bt_mesh_core_config_t  mesh_config =
{
    MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    MESH_PID,                                 // Vendor-assigned product identifier
    MESH_VID,                                 // Vendor-assigned product version identifier
    MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
    0,   // number of elements on this device
    NULL                                 // Array of elements for this device
};

extern "C" uint16_t wiced_hal_write_nvram(uint16_t vs_id,
    uint16_t         data_length,
    uint8_t        * p_data,
    wiced_result_t * p_status)
{
    HKEY hKey;
    char lpszEntry[10];
    sprintf_s(lpszEntry, "%04x", vs_id);
    if (RegCreateKeyExA(HKEY_CURRENT_USER, szRegKey, 0, NULL, 0, KEY_SET_VALUE, NULL, &hKey, NULL) == ERROR_SUCCESS)
    {
        RegSetValueExA(hKey, lpszEntry, 0, REG_BINARY, p_data, data_length);
        RegCloseKey(hKey);
        *p_status = WICED_SUCCESS;
        return data_length;
    }
    return 0;
}

extern "C" uint16_t wiced_hal_read_nvram(uint16_t vs_id,
    uint16_t         data_length,
    uint8_t        * p_data,
    wiced_result_t * p_status)
{
    HKEY hKey;
    char lpszEntry[10];
    sprintf_s(lpszEntry, "%04x", vs_id);

    if (RegOpenKeyExA(HKEY_CURRENT_USER, szRegKey, 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
    {
        DWORD dwBytes;
        if (RegQueryValueExA(hKey, lpszEntry, 0, NULL, p_data, &dwBytes) == ERROR_SUCCESS)
        {
            RegCloseKey(hKey);
            *p_status = WICED_SUCCESS;
            return data_length;
        }
        RegCloseKey(hKey);
    }
    return 0;
}

extern "C" void* wiced_memory_permanent_allocate(uint32_t size)
{
    return malloc(size);
}

extern "C" void* wiced_bt_get_buffer(uint16_t size)
{
    return malloc(size);
}

extern "C" void wiced_bt_free_buffer(void *p_buf)
{
    return free(p_buf);
}

extern "C" uint64_t wiced_bt_mesh_core_get_tick_count(void)
{
    return GetTickCount64() / 1000 - TickCountInitValue;
}

extern "C" void dump_hex(uint8_t *p, uint32_t len)
{
    uint32_t i;
    char     buff1[100];

    while (len != 0)
    {
        memset(buff1, 0, sizeof(buff1));
        for (i = 0; i < len && i < 32; i++)
        {
            int s1 = (*p & 0xf0) >> 4;
            int s2 = *p & 0x0f;
            buff1[i * 3] = (s1 >= 0 && s1 <= 9) ? s1 + '0' : s1 - 10 + 'A';
            buff1[i * 3 + 1] = (s2 >= 0 && s2 <= 9) ? s2 + '0' : s2 - 10 + 'A';
            buff1[i * 3 + 2] = ' ';
            p++;
        }
        len -= i;
        if (len != 0)
            OutputDebugStringA(buff1);
    }
    OutputDebugStringA(buff1);
}

uint8_t timer_id = 1;

typedef struct
{
    void *p_callback;
    uint32_t params;
    uint8_t type;
    uint8_t id;
} my_timer;

wiced_result_t wiced_init_timer(wiced_timer_t* p_timer, wiced_timer_callback_fp TimerCb, UINT32 cBackparam, wiced_timer_type_t type)
{
    my_timer *timer = (my_timer *)p_timer;

    timer->p_callback = (void *)TimerCb;
    timer->params = cBackparam;
    timer->type = type;
    timer->id = timer_id++;
    return WICED_SUCCESS;
}

VOID CALLBACK TimerCallback(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime)
{
}

wiced_result_t wiced_start_timer(wiced_timer_t* p_timer, uint32_t timeout)
{
    my_timer *timer = (my_timer *)p_timer;
    KillTimer(NULL, timer->id);

    DWORD duration = timeout;
    if ((timer->type == WICED_SECONDS_TIMER) || (timer->type == WICED_MILLI_SECONDS_PERIODIC_TIMER))
        duration *= 1000;

    SetTimer(NULL, timer->id, duration, TimerCallback);
    return WICED_SUCCESS;
}

wiced_result_t wiced_stop_timer(wiced_timer_t* p_timer)
{
    my_timer *timer = (my_timer *)p_timer;
    KillTimer(NULL, timer->id);
    return WICED_SUCCESS;
}
#endif

