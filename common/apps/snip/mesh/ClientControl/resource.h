//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClientControl.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CLIENTCONTROL_DIALOG        102
#define IDS_STRING_GENERICS             102
#define IDS_STRING_CONFIGURATION        103
#define IDR_MAINFRAME                   128
#define IDD_SCHEDULER_ADVANCED          134
#define IDD_CONFIGURATION               137
#define IDD_OOB_OUTPUT                  140
#define IDC_LIGHT_LIGHTNESS_DEFAULT     1001
#define IDC_LIGHT_LIGHTNESS_RANGE_MAX   1002
#define IDC_LIGHT_LIGHTNESS_RANGE_MIN   1003
#define IDC_COM_PORT                    1004
#define IDC_BATTERY_LEVEL               1005
#define IDC_TIME_TO_DISCHARGE           1006
#define IDC_COM_BAUD                    1007
#define IDC_TIME_TO_CHARGE              1008
#define IDC_LIGHT_HUE_RANGE_MAX         1009
#define IDC_TC_NET_LEVEL_TRX_TTL        1010
#define IDC_LIGHT_HUE_RANGE_MIN         1011
#define IDC_LOCAL_NORTH                 1012
#define IDC_LOCAL_EAST                  1013
#define IDC_LOCAL_ALTITUDE              1014
#define IDC_DST                         1015
#define IDC_UPDATE_TIME                 1016
#define IDC_LOCAL_ADDR                  1016
#define IDC_PRECISION                   1017
#define IDC_IV_INDEX                    1017
#define IDC_APP_KEY_IDX                 1018
#define IDC_PROPERTY_ID                 1019
#define IDC_NET_KEY_IDX                 1019
#define IDC_PROPERTY_VALUE              1020
#define IDC_NET_KEY_IDX2                1020
#define IDC_IDENTITY_DURATION           1020
#define IDC_LIGHT_SATURATION_RANGE_MAX  1021
#define IDC_AUTH_SIZE                   1021
#define IDC_LIGHT_SATURATION_RANGE_MIN  1022
#define IDC_BD_ADDR_TYPE                1022
#define IDC_LIGHT_CTL_TEMPERATURE_RANGE_MAX 1023
#define IDC_PROXY_FILTER_TYPE           1023
#define IDC_LIGHT_CTL_TEMPERATURE_RANGE_MIN 1024
#define IDC_PROXY_FILTER_ADDR           1024
#define IDC_LIGHT_XYL_X_MAX             1025
#define IDC_LIGHT_XYL_X_MIN             1026
#define IDC_LIGHT_XYL_Y_MAX             1027
#define IDC_LIGHT_XYL_Y_MIN             1028
#define IDC_COM_BAUD2                   1029
#define IDC_TEST_SELECTION              1029
#define IDC_LIGHT_LC_PROPERTY_VALUE     1030
#define IDC_SCENE_NUMBER                1031
#define IDC_SCHEDULAR_ACTION_NUMBER     1032
#define IDC_TIME_SUBSECONDS             1033
#define IDC_TIME_ZONE_OFFSET            1034
#define IDC_TIME_UNCERTAINTY            1035
#define IDC_TIME_TAI_UTC_DELTA          1036
#define IDC_TRACE                       1040
#define IDC_BATTERY_PRESENSE            1064
#define IDC_BATTERY_INDICATOR           1065
#define IDC_BATTERY_CHARGING            1066
#define IDC_BATTERY_SERVICABILITY       1067
#define IDC_FLOOR                       1068
#define IDC_ON_OFF_TARGET               1069
#define IDC_TIME_AUTHORITY              1070
#define IDC_POWER_ON_OFF_STATE          1071
#define IDC_PROPERTY_TYPE               1072
#define IDC_PROPERTY_ACCESS             1073
#define IDC_LIGHT_LC_ON_OFF             1074
#define IDC_LIGHT_MODE                  1075
#define IDC_LIGHT_LC_MODE               1075
#define IDC_TC_NET_LEVEL_TRX_CTL        1076
#define IDC_LIGHT_LC_OCCUPANCY_MODE     1077
#define IDC_LIGHT_LC_PROPERTY           1078
#define IDC_CLEAR_TRACE                 1079
#define IDC_YEAR_TYPE                   1080
#define IDC_DAY_TYPE                    1081
#define IDC_HOUR_TYPE                   1082
#define IDC_MINUTE_TYPE                 1083
#define IDC_SECOND_TYPE                 1084
#define IDC_DAY_OF_WEEK                 1085
#define IDC_ACTION                      1086
#define IDC_TC_NET_LEVEL_TRX_SEND       1095
#define IDC_BATTERY_LEVEL_GET           1101
#define IDC_LOCATION_GET                1102
#define IDC_ON_OFF_GET                  1103
#define IDC_MOBILE                      1104
#define IDC_LOCATION_LOCAL_GET          1105
#define IDC_BATTERY_LEVEL_SET           1106
#define IDC_LOCATION_SET                1107
#define IDC_GLOBAL_ALTITUDE             1108
#define IDC_GLOBAL_LONGITUDE            1109
#define IDC_GLOBAL_LATITUDE             1110
#define IDC_SCENE_GET                   1111
#define IDC_USE_PUBLICATION_INFO        1112
#define IDC_LEVEL_GET                   1113
#define IDC_TRANSITION_TIME             1114
#define IDC_RELIABLE_SEND               1115
#define IDC_ON_OFF_SET                  1116
#define IDC_KEY_REFRESH_PHASE2          1116
#define IDC_SCENE_STORE                 1117
#define IDC_IV_UPDATE                   1117
#define IDC_DELAY                       1118
#define IDC_RELIABLE_SEND2              1118
#define IDC_USE_GATT                    1118
#define IDC_LEVEL_TARGET                1119
#define IDC_LEVEL_CURRENT               1120
#define IDC_USE_DEFAULT_TRANS_TIME_SEND 1121
#define IDC_LIGHT_LIGHTNESS_LINEAR      1122
#define IDC_LEVEL_SET                   1123
#define IDC_SCHEDULER_ACTION_GET        1124
#define IDC_LEVEL_DELTA                 1125
#define IDC_SCHEDULER_ACTION_SET        1126
#define IDC_SCHEDULER_ADVANCED          1127
#define IDC_POWER_ON_OFF_GET            1128
#define IDC_POWER_ON_OFF_SET            1129
#define IDC_TIME_GET                    1130
#define IDC_DELTA_CONTINUE              1131
#define IDC_DEFAULT_TRANSITION_TIME     1132
#define IDC_SENSOR_CL_TRIG_TYPE         1133
#define IDC_DEFAULT_TRANSITION_TIME_GET 1134
#define IDC_DEFAULT_TRANSITION_TIME_SET 1135
#define IDC_DEFAULT_TRANSITION_TIME_STATUS 1136
#define IDC_LOCATION_LOCAL_SET          1136
#define IDC_LOCATION_LOCAL_STATUS       1137
#define IDC_SCENE_RECALL                1137
#define IDC_POWER_LEVEL_CURRENT         1138
#define IDC_LIGHT_LC_ON_OFF_GET         1138
#define IDC_LIGHT_LC_ON_OFF_SET         1139
#define IDC_LIGHT_LIGHTNESS_TARGET      1140
#define IDC_POWER_LEVEL_GET             1141
#define IDC_SCENE_DELETE                1141
#define IDC_POWER_LEVEL_SET             1142
#define IDC_TIME_SET                    1142
#define IDC_POWER_LEVEL_STATUS          1143
#define IDC_TIMEZONE_GET                1143
#define IDC_POWER_LEVEL_DELAY           1144
#define IDC_LIGHT_CTL_DELTA_UV_TARGET   1144
#define IDC_POWER_LEVEL_REMAINING_TIME  1145
#define IDC_TIME_SET2                   1145
#define IDC_TIMEZONE_SET                1145
#define IDC_PROPERTIES_GET              1146
#define IDC_PROPERTY_GET                1147
#define IDC_TC_NET_LEVEL_TRX_PDU        1148
#define IDC_TC_NET_LEVEL_TRX_DST        1149
#define IDC_TC_TRANSP_LEVEL_SZMIC       1150
#define IDC_TC_TRANSP_LEVEL_SEND        1151
#define IDC_TC_IV_UPDATE_IN_PROGRESS    1152
#define IDC_SCENE_REGISTER_GET          1152
#define IDC_TC_IV_UPDATE_TRANSIT        1153
#define IDC_PROPERTY_SET                1154
#define IDC_PROPERTY_STATUS             1155
#define IDC_LIGHT_LIGHTNESS_CURRENT     1156
#define IDC_LIGHT_CTL_TEMPERATURE_TARGET 1157
#define IDC_LIGHT_LIGHTNESS_GET         1158
#define IDC_LIGHT_LIGHTNESS_SET         1159
#define IDC_SCHEDULER_REGISTER_GET      1160
#define IDC_LIGHT_LIGHTNESS_LAST_GET    1161
#define IDC_TC_LPN_FRND_CLEAR           1163
#define IDC_TAI_UTC_DELTA_GET           1164
#define IDC_LIGHT_CTL_GET               1165
#define IDC_LIGHT_LIGHTNESS_DEFAULT_GET 1166
#define IDC_LIGHT_LIGHTNESS_DEFAULT_SET 1167
#define IDC_TAI_UTC_DELTA_SET           1168
#define IDC_LIGHT_LIGHTNESS_RANGE_GET   1169
#define IDC_LIGHT_LIGHTNESS_RANGE_SET   1170
#define IDC_TIME_AUTHORITY_GET          1171
#define IDC_LIGHT_CTL_SET               1172
#define IDC_LIGHT_HSL_HUE_VALUE         1173
#define IDC_LIGHT_HSL_SATURATION_VALUE  1174
#define IDC_LIGHT_HSL_HUE_GET           1175
#define IDC_LIGHT_HSL_HUE_SET           1176
#define IDC_LIGHT_HSL_SATURATION_GET    1177
#define IDC_LIGHT_HSL_SATURATION_SET    1178
#define IDC_LIGHT_HSL_DEFAULT_GET       1179
#define IDC_LIGHT_HSL_DEFAULT_SET       1180
#define IDC_LIGHT_HSL_GET               1181
#define IDC_LIGHT_HSL_SET               1182
#define IDC_TIME_AUTHORITY_SET          1183
#define IDC_LIGHT_HSL_RANGE_GET         1184
#define IDC_LIGHT_HSL_RANGE_SET         1185
#define IDC_CONFIGURATION               1186
#define IDC_LIGHT_CTL_TEMPERATURE_GET   1188
#define IDC_LIGHT_CTL_TEMPERATURE_SET   1189
#define IDC_LIGHT_CTL_DEFAULT_GET       1190
#define IDC_LIGHT_CTL_DEFAULT_SET       1191
#define IDC_LIGHT_CTL_TEMPERATURE_RANGE_GET 1192
#define IDC_LIGHT_CTL_TEMPERATURE_RANGE_SET 1193
#define IDC_LIGHT_XYL_Y_TARGET          1195
#define IDC_LIGHT_XYL_GET               1196
#define IDC_LIGHT_XYL_SET               1197
#define IDC_LIGHT_XYL_X_TARGET          1198
#define IDC_DELTA_SET                   1200
#define IDC_MOVE_SET                    1201
#define IDC_LIGHT_XYL_DEFAULT_GET       1202
#define IDC_LIGHT_XYL_DEFAULT_SET       1203
#define IDC_LIGHT_XYL_RANGE_GET         1204
#define IDC_LIGHT_XYL_RANGE_SET         1205
#define IDC_LIGHT_LC_PROPERTY_GET       1206
#define IDC_LIGHT_XYL_TARGET_GET        1207
#define IDC_TC_PVNR_ADDR                1208
#define IDC_TC_PVNR_PROVISION           1209
#define IDC_LIGHT_HSL_TARGET_GET        1210
#define IDC_LIGHT_HSL_DEFAULT_GET2      1210
#define IDC_LIGHT_LC_MODE_GET           1211
#define IDC_LIGHT_LC_MODE_SET           1212
#define IDC_LIGHT_XYL_RANGE_SET2        1213
#define IDC_LIGHT_LC_PROPERTY_SET       1213
#define IDC_LIGHT_LC_OCCUPANCY_MODE_GET 1214
#define IDC_LIGHT_LC_OCCUPANCY_MODE_SET 1215
#define IDC_LIGHT_LC_OCCUPANCY_SET      1216
#define IDC_SENSOR_RAW_VAL_X            1221
#define IDC_SENSOR_RAW_VAL_X1           1222
#define IDC_SENSOR_CL_RAW_VAL_X2        1223
#define IDC_SENSOR_SETTING_VAL          1224
#define IDC_SENSOR_FS_CAD_DIV           1225
#define IDC_SENSOR_CL_TRIG_DWN          1226
#define IDC_SENSOR_CL_TRIG_DEL_UP       1227
#define IDC_SENSOR_CL_MIN_INT           1228
#define IDC_SENSOR_CL_FST_CAD_HIGH      1229
#define IDC_SENSOR_CL_FST_CAD_LOW       1230
#define IDC_SENSOR_PROP_ID              1231
#define IDC_SENSOR_SETTING_PROP_ID      1232
#define IDC_SENSOR_PROP_VAL_LEN         1234
#define IDC_SENSOR_MSG                  1235
#define IDC_SENSOR_MSG_SEND             1237
#define IDC_TC_CLEAR_RPL                1240
#define IDC_TC_IV_UPDATE_SET_TEST_MODE  1241
#define IDC_TC_IV_UPDATE                1242
#define IDC_TC_IV_UPDATE_SET_RECOVERY_MODE 1243
#define IDC_STATIC_LIGHTNESS_POWER_LEVEL 1244
#define IDC_SHEDULER_TIME               1245
#define IDC_SHEDULER_DAY                1246
#define IDC_MONDAY                      1247
#define IDC_TUESDAY                     1248
#define IDC_WEDNESDAY                   1249
#define IDC_THURSDAY                    1250
#define IDC_FRIDAY                      1251
#define IDC_SATURDAY                    1252
#define IDC_SUNDAY                      1253
#define IDC_JANUARY                     1254
#define IDC_FABRUARY                    1255
#define IDC_MARCH                       1256
#define IDC_APRIL                       1257
#define IDC_MAY                         1258
#define IDC_JUNE                        1259
#define IDC_JULY                        1260
#define IDC_AUGUST                      1261
#define IDC_SEPTEMBER                   1262
#define IDC_OCTOBER                     1263
#define IDC_NOVEMBER                    1264
#define IDC_DECEMBER                    1265
#define IDC_SCHEDULER_HOUR              1266
#define IDC_SCHEDULER_MINUTE            1267
#define IDC_SCHEDULER_SECOND            1268
#define IDC_SCHEDULER_YEAR              1269
#define IDC_SCHEDULER_DAY               1270
#define IDC_TC_HEALTH_FAULTS            1271
#define IDC_TC_HEALTH_FAULTS_SET        1272
#define IDC_TC_CFG_IDENTITY             1273
#define IDC_TC_CFG_ONE_NETKEY           1274
#define IDC_TC_ACCESS_PDU               1275
#define IDC_TC_LPN_SEND_SUBS_ADD        1276
#define IDC_TC_LPN_SEND_SUBS_DEL        1277
#define IDC_HEARTBEAT_SUBSCRIPTION_SET  1278
#define IDC_HEARTBEAT_SUBSCRIPTION_SRC  1279
#define IDC_HEARTBEAT_SUBSCRIPTION_DST  1280
#define IDC_HEARTBEAT_SUBSCRIPTION_PERIOD 1281
#define IDC_HEARTBEAT_PUBLICATION_DST   1282
#define IDC_HEARTBEAT_PUBLICATION_PERIOD 1283
#define IDC_HEARTBEAT_PUBLICATION_COUNT 1284
#define IDC_HEARTBEAT_PUB_PROXY         1285
#define IDC_HEARTBEAT_PUB_RELAY         1286
#define IDC_HEARTBEAT_PUB_RELAY2        1287
#define IDC_HEARTBEAT_PUB_FRIEND        1287
#define IDC_HEARTBEAT_PUB_LOW_POWER_MODE 1288
#define IDC_HEARTBEAT_PUBLICATION_SET   1289
#define IDC_HEARTBEAT_PUBLICATION_TTL   1290
#define IDC_HEARTBEAT_PUBLICATION_NET_KEY_IDX 1291
#define IDC_NETWORK_TRANSMIT_COUNT      1292
#define IDC_NETWORK_TRANSMIT_INTERVAL   1293
#define IDC_NETWORK_TRANSMIT_INTERVAL_SET 1294
#define IDC_NETWORK_TRANSMIT_INTERVAL_GET 1295
#define IDC_NODE_RESET                  1296
#define IDC_CONFIG_DATA_GET             1297
#define IDC_BEACON_STATE                1298
#define IDC_BEACON_SET                  1299
#define IDC_BEACON_GET                  1300
#define IDC_USE_VIRTUAL_ADDR            1301
#define IDC_MODEL_SUBSCRIPTION_ADD      1302
#define IDC_OOB_NUMERIC                 1302
#define IDC_COMPOSITION_DATA_PAGE       1303
#define IDC_STATIC_OOB                  1303
#define IDC_GATT_PROXY_SET              1304
#define IDC_GATT_PROXY_GET              1305
#define IDC_GATT_PROXY_STATE            1306
#define IDC_RELAY_SET                   1307
#define IDC_RELAY_GET                   1308
#define IDC_RELAY_STATE                 1309
#define IDC_RELAY_TRANSMIT_COUNT        1310
#define IDC_RELAY_TRANSMIT_INTERVAL     1311
#define IDC_DEFAULT_TTL_GET             1312
#define IDC_DEFAULT_TTL                 1313
#define IDC_DEFAULT_TTL_SET             1314
#define IDC_FRIEND_SET                  1315
#define IDC_FRIEND_GET                  1316
#define IDC_FRIEND_STATE                1317
#define IDC_MODEL_PUB_ELEMENT_ADDR      1318
#define IDC_MODEL_PUB_PUBLISH_ADDR      1319
#define IDC_MODEL_PUB_APP_KEY_IDX       1320
#define IDC_MODEL_PUB_CREDENTIAL_FLAG   1321
#define IDC_PUBLISH_TTL                 1322
#define IDC_MODEL_PUB_RETRANSMIT_COUNT  1323
#define IDC_MODEL_PUB_RETRANSMIT_INTERVAL 1324
#define IDC_MODEL_PUB_MODEL_ID          1325
#define IDC_MODEL_PUB_SET               1326
#define IDC_MODEL_PUB_GET               1327
#define IDC_MODEL_PUB_PERIOD            1328
#define IDC_MODEL_PUB_VIRTUAL_ADDR      1329
#define IDC_MODEL_SUB_ELEMENT_ADDR      1330
#define IDC_MODEL_SUB_ADDR              1331
#define IDC_MODEL_SUB_MODEL_ID          1332
#define IDC_NETKEY                      1333
#define IDC_USE_VIRTUAL_ADDR2           1334
#define IDC_MODEL_SUBSCRIPTION_DELETE   1335
#define IDC_MODEL_SUBSCRIPTION_OVERWRITE 1336
#define IDC_MODEL_SUBSCRIPTION_DELETE_ALL 1337
#define IDC_SUBSCRIPTION_GET            1338
#define IDC_NODE_IDENTITY_STATE         1339
#define IDC_NETKEY_IDX                  1340
#define IDC_MODEL_SUB_VIRTUAL_ADDR      1341
#define IDC_NETKEY_ADD                  1342
#define IDC_NETKEY_DELETE               1343
#define IDC_NETKEY_UPDATE               1344
#define IDC_NETKEY_GET                  1345
#define IDC_APPKEY                      1346
#define IDC_APPKEY_ADD                  1347
#define IDC_APPKEY_DELETE               1348
#define IDC_APPKEY_UPDATE               1349
#define IDC_APPKEY_GET                  1350
#define IDC_APPKEY_IDX                  1351
#define IDC_MODEL_ID                    1352
#define IDC_MODEL_APP_BIND              1353
#define IDC_MODEL_APP_UNBIND            1354
#define IDC_MODEL_APP_GET               1355
#define IDC_NODE_IDENTITY_SET           1356
#define IDC_MODEL_BIND_ELEMENT_ADDR     1357
#define IDC_NODE_IDENTITY_GET           1358
#define IDC_NODE_IDENTITY_NETKEY_IDX    1359
#define IDC_HEALTH_FAULT_GET            1360
#define IDC_HEALTH_FAULT_COMPANY_ID     1361
#define IDC_SCAN_UNPROVISIONED          1362
#define IDC_HEALTH_FAULT_CLEAR          1363
#define IDC_HEALTH_FAULT_TEST           1364
#define IDC_HEALTH_PERIOD_GET           1365
#define IDC_HEALTH_PERIOD               1366
#define IDC_HEALTH_PERIOD_SET           1367
#define IDC_HEALTH_ATTENTION_GET        1368
#define IDC_HEALTH_ATTENTION            1369
#define IDC_HEALTH_ATTENTION_SET        1370
#define IDC_AUTH_METHOD                 1371
#define IDC_HEALTH_FAULT_TEST_ID        1372
#define IDC_PROVISION_UUID              1373
#define IDC_PROVISION_CONNECT           1374
#define IDC_NET_KEY                     1375
#define IDC_LOCAL_SET                   1376
#define IDC_PROVISION_DISCONNECT        1377
#define IDC_PROVISION_START             1378
#define IDC_AUTH_ACTION                 1379
#define IDC_PROVISION_UUID2             1380
#define IDC_DEVICE_PUB_KEY              1380
#define IDC_HEARTBEAT_PUBLICATION_GET   1381
#define IDC_HEARTBEAT_SUBSCRIPTION_GET  1382
#define IDC_LPN_ADDR                    1383
#define IDC_LPN_POLL_TIMEOUT_GET        1384
#define IDC_KEY_REFRESH_PHASE_SET       1385
#define IDC_KEY_REFRESH_PHASE_GET       1386
#define IDC_KEY_REFRESH_PHASE           1387
#define IDC_KEY_REFRESH_NET_KEY_INX     1388
#define IDC_CONNECt_PROXY               1389
#define IDC_FIND_PROXY                  1390
#define IDC_BD_ADDR                     1391
#define IDC_DISCONNECt_PROXY            1392
#define IDC_PROXY_FILTER_TYPE_SET       1393
#define IDC_PROXY_FILTER_ADDR_ADD       1394
#define IDC_PROXY_FILTER_ADDR_ADD2      1395
#define IDC_PROXY_FILTER_ADDR_DELETE    1395

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1385
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
