// Config.cpp : implementation file
//

#include "stdafx.h"
#include "Config.h"
#include "ClientControlDlg.h"
#include "ClientControlDlg.h"
#include "afxdialogex.h"
#include "resource.h"
#include "..\..\..\..\common\include\hci_control_api.h"

// CConfig dialog

#define WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_PUB_KEY   1   ///< Provisioner: Enter public key()
#define WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_STATIC    3   ///< Provisioner: Enter static OOB value(size)
#define WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_INPUT   5   ///< Provisioner: Select and display input OOB value(size, action)
#define WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_OUTPUT  6   ///< Provisioning node: Select and display output OOB value(size, action)
#define WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_STOP    7   ///< Provisioner and Provisioning node: Stop displaying OOB value

extern void TraceHciPkt(BYTE type, BYTE *buffer, USHORT length);

// COutputOob dialog
class COutputOob : public CDialogEx
{
    DECLARE_DYNAMIC(COutputOob)

public:
    COutputOob(CWnd* pParent = NULL);   // standard constructor
    virtual ~COutputOob();
    BYTE m_oob_type; 
    BYTE m_oob_size; 
    BYTE m_oob_action;
    char m_output_value[17];

    // Dialog Data
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_OOB_OUTPUT };
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    void OnBnClickedOk();

    virtual BOOL OnInitDialog();
    DECLARE_MESSAGE_MAP()
};

IMPLEMENT_DYNAMIC(COutputOob, CDialogEx)

COutputOob::COutputOob(CWnd* pParent /*=NULL*/)
    : CDialogEx(IDD_OOB_OUTPUT, pParent)
{
}

BOOL COutputOob::OnInitDialog()
{
    CDialogEx::OnInitDialog();
    SetDlgItemText(IDC_STATIC_OOB, L"Input value presented by the device");
    return TRUE;  // return TRUE  unless you set the focus to a control
}

COutputOob::~COutputOob()
{
}

void COutputOob::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(COutputOob, CDialogEx)
    ON_BN_CLICKED(IDOK, &COutputOob::OnBnClickedOk)
END_MESSAGE_MAP()

void COutputOob::OnBnClickedOk()
{
    GetDlgItemTextA(m_hWnd, IDC_OOB_NUMERIC, m_output_value, 17);
    CDialogEx::OnOK();
}

IMPLEMENT_DYNAMIC(CConfig, CPropertyPage)

ULONG conn_id;
BYTE auth_action;
BYTE dev_key[16] = { 0 };
BYTE configured_dev_key[16] = { 0 };
BYTE pub_key_type;

CConfig::CConfig(CWnd* pParent /*=NULL*/)
	: CPropertyPage(IDD_CONFIGURATION, 103)
{

}

CConfig::~CConfig()
{
}

extern void Log(WCHAR *fmt, ...);

BOOL CConfig::OnSetActive()
{
    CPropertyPage::OnSetActive();

    m_trace = (CListBox *)GetDlgItem(IDC_TRACE);
    CClientDialog *pSheet = (CClientDialog *)theApp.m_pMainWnd;
    pSheet->m_active_page = 2;

    CClientControlDlg *pMainDlg = &pSheet->pageMain;
    m_ComHelper = pMainDlg->m_ComHelper;

    SetDlgItemHex(IDC_LOCAL_ADDR, 2);
    SetDlgItemText(IDC_NET_KEY, L"00112233445566778899aabbccddeeff");
    SetDlgItemHex(IDC_NET_KEY_IDX, 0);
    SetDlgItemText(IDC_IV_INDEX, L"00000000");

    SetDlgItemText(IDC_PROVISION_UUID, L"33 11 00 9B 71 20 0F 0F 0F 0F 0F 0F 0F 0F 0F 0F");

    SetDlgItemHex(IDC_DST, 1);
    SetDlgItemHex(IDC_APP_KEY_IDX, 0);
    ((CButton *)(GetDlgItem(IDC_RELIABLE_SEND)))->SetCheck(1);

    SetDlgItemHex(IDC_HEARTBEAT_SUBSCRIPTION_SRC, 1);
    SetDlgItemHex(IDC_HEARTBEAT_SUBSCRIPTION_DST, 2);
    SetDlgItemHex(IDC_HEARTBEAT_SUBSCRIPTION_PERIOD, 0x10000);
    
    SetDlgItemHex(IDC_HEARTBEAT_PUBLICATION_DST, 0x02);
    SetDlgItemHex(IDC_HEARTBEAT_PUBLICATION_COUNT, 0x10000);
    SetDlgItemHex(IDC_HEARTBEAT_PUBLICATION_PERIOD, 0x3a98);
    SetDlgItemHex(IDC_HEARTBEAT_PUBLICATION_NET_KEY_IDX, 0);
    SetDlgItemHex(IDC_HEARTBEAT_PUBLICATION_TTL, 0x7f);
    
    ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_PROXY)))->SetCheck(0);
    ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_RELAY)))->SetCheck(0);
    ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_FRIEND)))->SetCheck(0);
    ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_LOW_POWER_MODE)))->SetCheck(0);
    
    SetDlgItemHex(IDC_COMPOSITION_DATA_PAGE, 1);
    SetDlgItemHex(IDC_NETWORK_TRANSMIT_COUNT, 2);
    SetDlgItemHex(IDC_NETWORK_TRANSMIT_INTERVAL, 0x64);
    ((CComboBox *)(GetDlgItem(IDC_BEACON_STATE)))->SetCurSel(0);
    SetDlgItemHex(IDC_DEFAULT_TTL, 127);
    ((CComboBox *)(GetDlgItem(IDC_GATT_PROXY_STATE)))->SetCurSel(0);
    ((CComboBox *)(GetDlgItem(IDC_RELAY_STATE)))->SetCurSel(0);
    SetDlgItemHex(IDC_RELAY_TRANSMIT_COUNT, 3);
    SetDlgItemHex(IDC_RELAY_TRANSMIT_INTERVAL, 0x64);
    ((CComboBox *)(GetDlgItem(IDC_FRIEND_STATE)))->SetCurSel(0);

    SetDlgItemHex(IDC_MODEL_PUB_ELEMENT_ADDR, 0);
    SetDlgItemHex(IDC_MODEL_PUB_PUBLISH_ADDR, 2);
    SetDlgItemHex(IDC_MODEL_PUB_APP_KEY_IDX, 0);
    ((CComboBox *)(GetDlgItem(IDC_MODEL_PUB_CREDENTIAL_FLAG)))->SetCurSel(0);
    SetDlgItemHex(IDC_PUBLISH_TTL, 0x3f);
    SetDlgItemHex(IDC_MODEL_PUB_PERIOD, 0x3a98);
    SetDlgItemHex(IDC_MODEL_PUB_RETRANSMIT_COUNT, 0);
    SetDlgItemHex(IDC_MODEL_PUB_RETRANSMIT_INTERVAL, 0);
    SetDlgItemHex(IDC_MODEL_PUB_MODEL_ID, 0x1001);
    SetDlgItemText(IDC_MODEL_PUB_VIRTUAL_ADDR, L"00112233445566778899aabbccddeeff");

    SetDlgItemHex(IDC_MODEL_SUB_ELEMENT_ADDR, 1);
    SetDlgItemHex(IDC_MODEL_SUB_ADDR, 2);
    SetDlgItemHex(IDC_MODEL_SUB_MODEL_ID, 0x1001);
    SetDlgItemText(IDC_MODEL_SUB_VIRTUAL_ADDR, L"00112233445566778899aabbccddeeff");
        
    SetDlgItemHex(IDC_NETKEY_IDX, 0);
    SetDlgItemHex(IDC_APPKEY_IDX, 0);
    SetDlgItemHex(IDC_MODEL_ID, 0x1001);
    SetDlgItemHex(IDC_MODEL_BIND_ELEMENT_ADDR, 0x1);
    SetDlgItemText(IDC_NETKEY, L"00112233445566778899aabbccddeeff");
    SetDlgItemText(IDC_APPKEY, L"00112233445566778899aabbccddeeff");

    ((CComboBox *)(GetDlgItem(IDC_NODE_IDENTITY_STATE)))->SetCurSel(0);
    SetDlgItemHex(IDC_NODE_IDENTITY_NETKEY_IDX, 0);

    SetDlgItemHex(IDC_HEALTH_FAULT_COMPANY_ID, 0x131);
    SetDlgItemHex(IDC_HEALTH_FAULT_TEST_ID, 0);
    SetDlgItemHex(IDC_HEALTH_PERIOD, 1);
    SetDlgItemHex(IDC_HEALTH_ATTENTION, 0);

    SetDlgItemHex(IDC_IDENTITY_DURATION, 1);

    ((CComboBox *)GetDlgItem(IDC_AUTH_METHOD))->SetCurSel(0);
    SetDlgItemText(IDC_DEVICE_PUB_KEY, L"F465E43FF23D3F1B9DC7DFC04DA8758184DBC966204796ECCF0D6CF5E16500CC0201D048BCBBD899EEEFC424164E33C201C2B010CA6B4D43A8A155CAD8ECB279");
    SetDlgItemText(IDC_BD_ADDR, L"001bdc08e4e8");
    SetDlgItemInt(IDC_BD_ADDR_TYPE, 0);
    return TRUE;  // return TRUE unless you set the focus to a control
}

void CConfig::ProcessEvent(LPBYTE p_data, DWORD len)
{
}


void CConfig::DoDataExchange(CDataExchange* pDX)
{
    CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CConfig, CPropertyPage)
    ON_BN_CLICKED(IDC_HEARTBEAT_SUBSCRIPTION_SET, &CConfig::OnBnClickedHeartbeatSubscriptionSet)
    ON_BN_CLICKED(IDC_HEARTBEAT_PUBLICATION_SET, &CConfig::OnBnClickedHeartbeatPublicationSet)
    ON_BN_CLICKED(IDC_NETWORK_TRANSMIT_INTERVAL_GET, &CConfig::OnBnClickedNetworkTransmitIntervalGet)
    ON_BN_CLICKED(IDC_NETWORK_TRANSMIT_INTERVAL_SET, &CConfig::OnBnClickedNetworkTransmitIntervalSet)
    ON_BN_CLICKED(IDC_NODE_RESET, &CConfig::OnBnClickedNodeReset)
    ON_BN_CLICKED(IDC_CONFIG_DATA_GET, &CConfig::OnBnClickedConfigDataGet)
    ON_BN_CLICKED(IDC_BEACON_GET, &CConfig::OnBnClickedBeaconGet)
    ON_BN_CLICKED(IDC_BEACON_SET, &CConfig::OnBnClickedBeaconSet)
    ON_BN_CLICKED(IDC_DEFAULT_TTL_GET, &CConfig::OnBnClickedDefaultTtlGet)
    ON_BN_CLICKED(IDC_DEFAULT_TTL_SET, &CConfig::OnBnClickedDefaultTtlSet)
    ON_BN_CLICKED(IDC_GATT_PROXY_GET, &CConfig::OnBnClickedGattProxyGet)
    ON_BN_CLICKED(IDC_GATT_PROXY_SET, &CConfig::OnBnClickedGattProxySet)
    ON_BN_CLICKED(IDC_RELAY_GET, &CConfig::OnBnClickedRelayGet)
    ON_BN_CLICKED(IDC_RELAY_SET, &CConfig::OnBnClickedRelaySet)
    ON_BN_CLICKED(IDC_FRIEND_GET, &CConfig::OnBnClickedFriendGet)
    ON_BN_CLICKED(IDC_FRIEND_SET, &CConfig::OnBnClickedFriendSet)
    ON_BN_CLICKED(IDC_MODEL_PUB_SET, &CConfig::OnBnClickedModelPubSet)
    ON_BN_CLICKED(IDC_MODEL_PUB_GET, &CConfig::OnBnClickedModelPubGet)
    ON_BN_CLICKED(IDC_MODEL_SUBSCRIPTION_ADD, &CConfig::OnBnClickedModelSubscriptionAdd)
    ON_BN_CLICKED(IDC_MODEL_SUBSCRIPTION_DELETE, &CConfig::OnBnClickedModelSubscriptionDelete)
    ON_BN_CLICKED(IDC_MODEL_SUBSCRIPTION_OVERWRITE, &CConfig::OnBnClickedModelSubscriptionOverwrite)
    ON_BN_CLICKED(IDC_MODEL_SUBSCRIPTION_DELETE_ALL, &CConfig::OnBnClickedModelSubscriptionDeleteAll)
    ON_BN_CLICKED(IDC_SUBSCRIPTION_GET, &CConfig::OnBnClickedSubscriptionGet)
    ON_BN_CLICKED(IDC_NETKEY_ADD, &CConfig::OnBnClickedNetkeyAdd)
    ON_BN_CLICKED(IDC_NETKEY_DELETE, &CConfig::OnBnClickedNetkeyDelete)
    ON_BN_CLICKED(IDC_NETKEY_UPDATE, &CConfig::OnBnClickedNetkeyUpdate)
    ON_BN_CLICKED(IDC_NETKEY_GET, &CConfig::OnBnClickedNetkeyGet)
    ON_BN_CLICKED(IDC_APPKEY_ADD, &CConfig::OnBnClickedAppkeyAdd)
    ON_BN_CLICKED(IDC_APPKEY_DELETE, &CConfig::OnBnClickedAppkeyDelete)
    ON_BN_CLICKED(IDC_APPKEY_UPDATE, &CConfig::OnBnClickedAppkeyUpdate)
    ON_BN_CLICKED(IDC_APPKEY_GET, &CConfig::OnBnClickedAppkeyGet)
    ON_BN_CLICKED(IDC_MODEL_APP_BIND, &CConfig::OnBnClickedModelAppBind)
    ON_BN_CLICKED(IDC_MODEL_APP_UNBIND, &CConfig::OnBnClickedModelAppUnbind)
    ON_BN_CLICKED(IDC_MODEL_APP_GET, &CConfig::OnBnClickedModelAppGet)
    ON_BN_CLICKED(IDC_NODE_IDENTITY_GET, &CConfig::OnBnClickedNodeIdentityGet)
    ON_BN_CLICKED(IDC_NODE_IDENTITY_SET, &CConfig::OnBnClickedNodeIdentitySet)
    ON_BN_CLICKED(IDC_HEALTH_FAULT_GET, &CConfig::OnBnClickedHealthFaultGet)
    ON_BN_CLICKED(IDC_HEALTH_FAULT_CLEAR, &CConfig::OnBnClickedHealthFaultClear)
    ON_BN_CLICKED(IDC_HEALTH_FAULT_TEST, &CConfig::OnBnClickedHealthFaultTest)
    ON_BN_CLICKED(IDC_HEALTH_PERIOD_GET, &CConfig::OnBnClickedHealthPeriodGet)
    ON_BN_CLICKED(IDC_HEALTH_PERIOD_SET, &CConfig::OnBnClickedHealthPeriodSet)
    ON_BN_CLICKED(IDC_HEALTH_ATTENTION_GET, &CConfig::OnBnClickedHealthAttentionGet)
    ON_BN_CLICKED(IDC_HEALTH_ATTENTION_SET, &CConfig::OnBnClickedHealthAttentionSet)
    ON_BN_CLICKED(IDC_PROVISION_CONNECT, &CConfig::OnBnClickedProvisionConnect)
    ON_BN_CLICKED(IDC_LOCAL_SET, &CConfig::OnBnClickedLocalSet)
    ON_BN_CLICKED(IDC_SCAN_UNPROVISIONED, &CConfig::OnBnClickedScanUnprovisioned)
    ON_BN_CLICKED(IDC_PROVISION_DISCONNECT, &CConfig::OnBnClickedProvisionDisconnect)
    ON_BN_CLICKED(IDC_PROVISION_START, &CConfig::OnBnClickedProvisionStart)
    ON_CBN_SELCHANGE(IDC_AUTH_METHOD, &CConfig::OnCbnSelchangeAuthMethod)
    ON_BN_CLICKED(IDC_HEARTBEAT_PUBLICATION_GET, &CConfig::OnBnClickedHeartbeatPublicationGet)
    ON_BN_CLICKED(IDC_HEARTBEAT_SUBSCRIPTION_GET, &CConfig::OnBnClickedHeartbeatSubscriptionGet)
    ON_BN_CLICKED(IDC_LPN_POLL_TIMEOUT_GET, &CConfig::OnBnClickedLpnPollTimeoutGet)
    ON_BN_CLICKED(IDC_KEY_REFRESH_PHASE_GET, &CConfig::OnBnClickedKeyRefreshPhaseGet)
    ON_BN_CLICKED(IDC_KEY_REFRESH_PHASE_SET, &CConfig::OnBnClickedKeyRefreshPhaseSet)
    ON_BN_CLICKED(IDC_CONNECt_PROXY, &CConfig::OnBnClickedConnectProxy)
    ON_BN_CLICKED(IDC_FIND_PROXY, &CConfig::OnBnClickedFindProxy)
    ON_BN_CLICKED(IDC_DISCONNECt_PROXY, &CConfig::OnBnClickedDisconnectProxy)
    ON_BN_CLICKED(IDC_PROXY_FILTER_TYPE_SET, &CConfig::OnBnClickedProxyFilterTypeSet)
    ON_BN_CLICKED(IDC_PROXY_FILTER_ADDR_ADD, &CConfig::OnBnClickedProxyFilterAddrAdd)
    ON_BN_CLICKED(IDC_PROXY_FILTER_ADDR_DELETE, &CConfig::OnBnClickedProxyFilterAddrDelete)
END_MESSAGE_MAP()
extern BYTE ProcNibble(char n);
extern DWORD GetHexValue(char *szbuf, LPBYTE buf, DWORD buf_size);

void CConfig::ProcessData(DWORD opcode, LPBYTE p_data, DWORD len)
{
    WCHAR   trace[1024];
    switch (opcode)
    {
    case HCI_CONTROL_EVENT_WICED_TRACE:
        if (len >= 2)
        {
            if ((len > 2) && (p_data[len - 2] == '\n'))
            {
                p_data[len - 2] = 0;
                len--;
            }
            TraceHciPkt(0, p_data, (USHORT)len);
        }
        //MultiByteToWideChar(CP_ACP, 0, (const char *)p_data, len, trace, sizeof(trace) / sizeof(WCHAR));
        //m_trace->SetCurSel(m_trace->AddString(trace));
        break;
    case HCI_CONTROL_EVENT_HCI_TRACE:
        TraceHciPkt(p_data[0] + 1, &p_data[1], (USHORT)(len - 1));
        break;
    case HCI_CONTROL_MESH_EVENT_COMMAND_STATUS:
        wsprintf(trace, L"Mesh Command Status:%d", p_data[0]);
        m_trace->SetCurSel(m_trace->AddString(trace));
        break;
    case HCI_CONTROL_MESH_EVENT_UNPPROVISIONED_DEVICE:
        ProcessUnprovisionedDevice(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROXY_DEVICE_NETWORK_DATA:
        ProcessProxyDeviceNetworkData(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROVISION_END:
        ProcessProvisionEnd(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROVISION_LINK_STATUS:
        ProcessProvisionLinkStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROVISION_DEVICE_CAPABITIES:
        ProcessProvisionDeviceCapabilities(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROVISION_OOB_DATA:
        ProcessProvisionOobDataRequest(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_NODE_RESET_STATUS:
        ProcessNodeResetStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_COMPOSITION_DATA_STATUS:
        ProcessCompositionDataStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_FRIEND_STATUS:
        ProcessFriendStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_KEY_REFRESH_PHASE_STATUS:
        ProcessKeyRefreshPhaseStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_GATT_PROXY_STATUS:
        ProcessGattProxyStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_RELAY_STATUS:
        ProcessRelayStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_DEFAULT_TTL_STATUS:
        ProcessDefaultTtlStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_BEACON_STATUS:
        ProcessBeaconStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_MODEL_PUBLICATION_STATUS:
        ProcessModelPublicationStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_MODEL_SUBSCRIPTION_STATUS:
        ProcessModelSubscriptionStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_MODEL_SUBSCRIPTION_LIST:
        ProcessModelSubscriptionList(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_NETKEY_STATUS:
        ProcessNetKeyStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_NETKEY_LIST:
        ProcessNetKeyList(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_APPKEY_STATUS:
        ProcessAppKeyStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_APPKEY_LIST:
        ProcessAppKeyList(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_MODEL_APP_STATUS:
        ProcessModelAppStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_MODEL_APP_LIST:
        ProcessModelAppList(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_NODE_IDENTITY_STATUS:
        ProcessNodeIdentityStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEARTBEAT_SUBSCRIPTION_STATUS:
        ProcessHearbeatSubscriptionStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEARTBEAT_PUBLICATION_STATUS:
        ProcessHearbeatPublicationStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_NETWORK_TRANSMIT_PARAMS_STATUS:
        ProcessNetworkTransmitParamsStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEALTH_CURRENT_STATUS:
        ProcessHealthCurrentStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEALTH_FAULT_STATUS:
        ProcessHealthFaultStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEALTH_PERIOD_STATUS:
        ProcessHealthPeriodStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_HEALTH_ATTENTION_STATUS:
        ProcessHealthAttentionStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_LPN_POLL_TIMEOUT_STATUS:
        ProcessLpnPollTimeoutStatus(p_data, len);
        break;
    case HCI_CONTROL_MESH_EVENT_PROXY_FILTER_STATUS:
        ProcessProxyFilterStatus(p_data, len);
        break;
    default:
        wsprintf(trace, L"Rcvd Unknown Op Code: 0x%04x", opcode);
        m_trace->SetCurSel(m_trace->AddString(trace));
    }
}

void CConfig::ProcessUnprovisionedDevice(LPBYTE p_data, DWORD len)
{
    WCHAR buf[180];
    wcscpy(buf, L"Unprovisioned Device UUID:");
    for (int i = 0; i < 16; i++)
    {
        wsprintf(&buf[wcslen(buf)], L"%02x ", p_data[i]);
    }
    p_data += 16;
    wsprintf(&buf[wcslen(buf)], L"OOB:%x URI hash:%x PB GATT supported:%d", p_data[0] + ((USHORT)p_data[1] << 8), 
        p_data[2] + ((ULONG)p_data[3] << 8) + ((ULONG)p_data[4] << 16) + ((ULONG)p_data[5] << 24), p_data[6]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProxyDeviceNetworkData(LPBYTE p_data, DWORD len)
{
    WCHAR buf[180] = { 0 };
    wsprintf(buf, L"Proxy Device BD_ADDR:%02x%02x%02x%02x%02x%02x Type:%d NetKeyIdx:%x RSSI:%d",
        p_data[5], p_data[4], p_data[3], p_data[2], p_data[1], p_data[0], p_data[6], p_data[8] + (p_data[9] << 8), p_data[7]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProvisionEnd(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    ULONG conn_id = p_data[0] + ((ULONG)p_data[1] << 8) + ((ULONG)p_data[2] << 16) + ((ULONG)p_data[3] << 24);
    USHORT addr = p_data[4] + ((USHORT)p_data[5] << 8);
    wsprintf(buf, L"Provision: End conn_id:%x Addr:%x result:%x", conn_id, addr, p_data[6]);
    m_trace->SetCurSel(m_trace->AddString(buf));
    memcpy(dev_key, &p_data[7], 16);
    wcscpy(buf, L"DevKey: ");
    for (int i = 0; i < 16; i++)
        wsprintf(&buf[wcslen(buf)], L"%02x ", dev_key[i]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProvisionLinkStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    conn_id = p_data[0] + ((ULONG)p_data[1] << 8) + ((ULONG)p_data[2] << 16) + ((ULONG)p_data[3] << 24);
    USHORT addr = p_data[4] + ((USHORT)p_data[5] << 8);
    wsprintf(buf, L"Provision: Link Status conn_id:%x Addr:%x connected:%x over_gatt:%d", conn_id, addr, p_data[6], p_data[7]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProvisionDeviceCapabilities(LPBYTE p_data, DWORD len)
{
    WCHAR buf[280];
    conn_id = p_data[0] + ((ULONG)p_data[1] << 8) + ((ULONG)p_data[2] << 16) + ((ULONG)p_data[3] << 24);
    BYTE elements_num = p_data[4];
    USHORT algorithms = p_data[5] + ((USHORT)p_data[6] << 8);
    pub_key_type = p_data[7];
    BYTE static_oob_type = p_data[8];
    BYTE output_oob_size = p_data[9];
    USHORT output_oob_action = p_data[10] + ((USHORT)p_data[11] << 8);
    BYTE input_oob_size = p_data[12];
    USHORT input_oob_action = p_data[13] + ((USHORT)p_data[14] << 8);
    wsprintf(buf, L"Provision: Device Capabilities conn_id:%x Num elements:%x algo:%x pub key type:%d oob type:%d oob size:%d oob_action:%d input oob size:%d action:%d",
        conn_id, elements_num, algorithms, pub_key_type, static_oob_type, output_oob_size, output_oob_action, input_oob_size, input_oob_action);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProvisionOobDataRequest(LPBYTE p_data, DWORD len)
{
    WCHAR buf[280];
    conn_id = p_data[0] + ((ULONG)p_data[1] << 8) + ((ULONG)p_data[2] << 16) + ((ULONG)p_data[3] << 24);
    BYTE static_oob_type = p_data[4];
    BYTE oob_size = p_data[5];
    BYTE oob_action = p_data[6];
    BYTE oob_data_size;
    BYTE oob_data[64];
    BYTE oob_static[16] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8};

    wsprintf(buf, L"Provision: Get OOB Data conn_id:%x oob type:%d oob size:%d oob_action:%d", conn_id, static_oob_type, oob_size, oob_action);
    m_trace->SetCurSel(m_trace->AddString(buf));

    if (static_oob_type == WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_STOP)
        return;

    if (static_oob_type == WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_STATIC)
    {
        oob_data_size = 16;
        memcpy(oob_data, oob_static, 16);
    }
    else if (static_oob_type == WICED_BT_MESH_PROVISION_GET_OOB_TYPE_DISPLAY_INPUT)
    {
        oob_data_size = 1;
        oob_data[0] = rand() & 0x07;
        wsprintf(buf, L"Provision: OOB input %d for conn_id:%x", oob_data[0], conn_id);
        if (auth_action == 3)
        {
            // alphanumeric
            oob_data[0] = 0x30 + oob_data[0];
        }
        m_trace->SetCurSel(m_trace->AddString(buf));
    }
    else if (static_oob_type == WICED_BT_MESH_PROVISION_GET_OOB_TYPE_ENTER_PUB_KEY)
    {
        char szbuf[130];
        GetDlgItemTextA(m_hWnd, IDC_DEVICE_PUB_KEY, szbuf, 130);
        DWORD len = GetHexValue(szbuf, oob_data, 64);
        if (len != 64)
        {
            MessageBox(L"Public Key should be 64 octets in length");
            return;
        }
        oob_data_size = 64;
    }
    else
    {
        COutputOob dlg;
        dlg.m_oob_type = static_oob_type;
        dlg.m_oob_size = oob_size;
        dlg.m_oob_action = oob_action;

        if (dlg.DoModal() != IDOK)
            return;

        oob_data_size = (BYTE)((strlen(dlg.m_output_value) + 1) / 2);
        DWORD val;
        for (int i = 0; i < oob_data_size; i++)
        {
            sscanf(&dlg.m_output_value[2 * i], "%02d", &val);
            oob_data[i] = val & 0xff;
        }
        wsprintf(buf, L"Provision: OOB data conn_id:%x", conn_id);
        m_trace->SetCurSel(m_trace->AddString(buf));
    }
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = (BYTE)conn_id;
    *p++ = (BYTE)(conn_id >> 8);
    *p++ = (BYTE)(conn_id >> 16);
    *p++ = (BYTE)(conn_id >> 24);
    *p++ = oob_data_size;
    memcpy(p, oob_data, oob_data_size);
    p += oob_data_size;
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROVISION_OOB_VALUE, buffer, (DWORD)(p - buffer));
}

void CConfig::ProcessNodeResetStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Node Reset Status from:%x", src);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessCompositionDataStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[300];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Composition Data from:%x Page:%d Len:%d ", src, p_data[2], len - 3);
    m_trace->SetCurSel(m_trace->AddString(buf));
    for (DWORD i = 3; i < len; i++)
    {
        wsprintf(&buf[wcslen(buf)], L"%02x ", p_data[i]);
    }
}

void CConfig::ProcessFriendStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Friend Status from:%x state:%d", src, p_data[2]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessKeyRefreshPhaseStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    BYTE status = p_data[2];
    USHORT net_key_inx = p_data[3] + ((USHORT)p_data[4] << 8);
    wsprintf(buf, L"Key Refresh Phase Status from:%x Status:%d NetKeyIdx:%x state:%d", status, src, net_key_inx, p_data[4]);
    m_trace->SetCurSel(m_trace->AddString(buf));

}
void CConfig::ProcessGattProxyStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"GATT Proxy Status from:%x state:%d", src, p_data[2]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessRelayStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Relay Status from:%x state:%d count:%d interval:%d", src, p_data[2], p_data[3], p_data[4] + ((USHORT)p_data[5] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessDefaultTtlStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Default TTL Status from:%x TTL:%d", src, p_data[2]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessNodeIdentityStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Node Identity Status from:%x status:%d net key idx:%d identity:%d", src, p_data[2], p_data[3] + ((USHORT)p_data[4] << 8), p_data[5]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessBeaconStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Beacon Status from:%x state:%d\n", src, p_data[2]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessModelPublicationStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Model Pub Status from:%x status:%d PublishAddr:%x Model ID: %x app_key_idx:%x cred_flag:%d TTL:%d Period:%d Retransmit Count/Interval %d/%d", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8),
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24), 
        p_data[9] + ((ULONG)p_data[10] << 8), p_data[11], p_data[12], 
        p_data[13] + ((ULONG)p_data[14] << 8) + ((ULONG)p_data[15] << 16) + ((ULONG)p_data[16] << 24),
        p_data[17], p_data[18] + ((ULONG)p_data[19] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessModelSubscriptionStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Model Sub Status from:%x status:%d Element Addr:%x Model ID:%x Addr:%x", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8), 
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24),
        p_data[9] + ((SHORT)p_data[10] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessModelSubscriptionList(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Model Sub List from:%x status:%d Element Addr:%x Model ID:%x Addresses:", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8),
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24));
    len -= 9;
    p_data += 9;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data += 2;
        len -= 2;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessNetKeyStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"NetKey Status from:%x status:%d NetKey Index:%x", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessNetKeyList(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"NetKey List from:%x Addresses:", src);
    len -= 2;
    p_data += 2;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data += 2;
        len -= 2;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessAppKeyStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"AppKey Status from:%x status:%d NetKey Index:%x ApptKey Index:%x", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8), p_data[5] + ((ULONG)p_data[6] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessAppKeyList(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"AppKey List from:%x status:%x NetKeyIdx:%x Addresses:", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8));
    len -= 5;
    p_data += 5;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data += 2;
        len -= 2;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessModelAppStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Model App Status from:%x status:%d Element Addr:%x Model ID:%x ApptKey Index:%x", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8),
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24),
        p_data[9] + ((ULONG)p_data[10] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessModelAppList(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Model App List from:%x status:%x Element Addr:%x Model ID:%x App Keys: ", src, p_data[2], p_data[3] + ((ULONG)p_data[4] << 8),
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24));
    len -= 9;
    p_data += 9;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data += 2;
        len -= 2;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHearbeatSubscriptionStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"subs status src:%x status:%d subs src/dst:%x/%x period:%d count:%d hops min/max:%d/%d\n",
        src, p_data[2], p_data[3] + ((USHORT)p_data[4] << 8), p_data[5] + ((USHORT)p_data[6] << 8),
        p_data[7] + ((ULONG)p_data[8] << 8) + ((ULONG)p_data[9] << 16) + ((ULONG)p_data[10] << 24),
        p_data[11] + ((USHORT)p_data[12] << 8), p_data[13], p_data[14]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHearbeatPublicationStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[200];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"pubs status src:%x status:%d pubs dst:%x period:%d count:%d ttl:%d relay:%d proxy:%d friend:%d low power:%d net_key_idx:%d\n",
        src, p_data[2], p_data[3] + ((USHORT)p_data[4] << 8),
        p_data[5] + ((ULONG)p_data[6] << 8) + ((ULONG)p_data[7] << 16) + ((ULONG)p_data[8] << 24),
        p_data[9] + ((USHORT)p_data[10] << 8), p_data[13],
        p_data[14], p_data[15], p_data[16], p_data[17], p_data[18], p_data[19] + ((USHORT)p_data[20] << 8));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessNetworkTransmitParamsStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    wsprintf(buf, L"Network Transmit Params Status from:%x count:%d interval:%d", src, p_data[2],
        p_data[3] + ((ULONG)p_data[4] << 8) + ((ULONG)p_data[5] << 16) + ((ULONG)p_data[6] << 24));
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHealthCurrentStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    USHORT app_key_idx = p_data[2] + ((USHORT)p_data[3] << 8);
    wsprintf(buf, L"Health Current Status from:%x AppKeyIdx:%x Test ID:%d CompanyID:%x Faults: ", src, app_key_idx, p_data[4], p_data[5] + ((USHORT)p_data[6] << 8),
        p_data[3] + ((ULONG)p_data[4] << 8) + ((ULONG)p_data[5] << 16) + ((ULONG)p_data[6] << 24));
    len -= 7;
    p_data += 7;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data ++;
        len --;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHealthFaultStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    USHORT app_key_idx = p_data[2] + ((USHORT)p_data[3] << 8);
    wsprintf(buf, L"Health Fault Status from:%x AppKeyIdx:%x Test ID:%d CompanyID:%x Faults: ", src, app_key_idx, p_data[4], p_data[5] + ((USHORT)p_data[6] << 8),
        p_data[3] + ((ULONG)p_data[4] << 8) + ((ULONG)p_data[5] << 16) + ((ULONG)p_data[6] << 24));
    len -= 7;
    p_data += 7;
    while (len)
    {
        wsprintf(&buf[wcslen(buf)], L"0x%04x ", p_data[0] + ((USHORT)p_data[1] << 8));
        p_data++;
        len--;
    }
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHealthPeriodStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    USHORT app_key_idx = p_data[2] + ((USHORT)p_data[3] << 8);
    wsprintf(buf, L"Health Period Status from:%x AppKeyIdx:%x Divisor:%d", src, app_key_idx, p_data[4]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessHealthAttentionStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    USHORT app_key_idx = p_data[2] + ((USHORT)p_data[3] << 8);
    wsprintf(buf, L"Health Attention Status from:%x AppKeyIdx:%x Timer:%d", src, app_key_idx, p_data[4]);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessLpnPollTimeoutStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    USHORT src = p_data[0] + ((USHORT)p_data[1] << 8);
    USHORT lpn_addr = p_data[2] + ((USHORT)p_data[3] << 8);
    USHORT poll_timeout = p_data[4] + ((ULONG)p_data[5] << 8) + ((ULONG)p_data[6] << 16) + ((ULONG)p_data[7] << 24);
    wsprintf(buf, L"LPN Poll Timeout Status from:%x Addr:%x PollTimeout:%d", src, lpn_addr, poll_timeout);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::ProcessProxyFilterStatus(LPBYTE p_data, DWORD len)
{
    WCHAR buf[80];
    BYTE  type = p_data[0];
    USHORT list_size = p_data[1] + ((USHORT)p_data[2] << 8);
    wsprintf(buf, L"Proxy Filter Status Type:%x List Size:%d", type, list_size);
    m_trace->SetCurSel(m_trace->AddString(buf));
}

void CConfig::SetDlgItemHex(DWORD id, DWORD val)
{
    WCHAR buf[10];
    wsprintf(buf, L"%x", val);
    SetDlgItemText(id, buf);
}

// CConfig message handlers
DWORD CConfig::GetHexValueInt(DWORD id)
{
    DWORD ret = 0;
    BYTE buf[32];
    char szbuf[100];
    char *psz = szbuf;
    BYTE *pbuf = buf;
    DWORD res = 0;

    memset(buf, 0, 32);

    GetDlgItemTextA(m_hWnd, id, szbuf, 100);

    DWORD len = GetHexValue(szbuf, buf, sizeof(buf));
    for (DWORD i = 0; i<len; i++)
    {
        ret = (ret << 8) + buf[i];
    }
    return ret;
}

void CConfig::OnBnClickedLocalSet()
{
    USHORT addr = (USHORT)GetHexValueInt(IDC_LOCAL_ADDR);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NET_KEY_IDX);
    ULONG  iv_idx = (USHORT)GetHexValueInt(IDC_IV_INDEX);
    BYTE   key_refresh = (BYTE)((CButton *)GetDlgItem(IDC_KEY_REFRESH_PHASE2))->GetCheck();
    BYTE   iv_update = (BYTE)((CButton *)GetDlgItem(IDC_IV_UPDATE))->GetCheck();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = addr & 0xff;
    *p++ = (addr >> 8) & 0xff;
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_NET_KEY, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = iv_idx & 0xff;
    *p++ = (iv_idx >> 8) & 0xff;
    *p++ = (iv_idx >> 16) & 0xff;
    *p++ = (iv_idx >> 24) & 0xff;
    *p++ = key_refresh;
    *p++ = iv_update;
    m_trace->SetCurSel(m_trace->AddString(L"Set Local Device"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_SET_LOCAL_DEVICE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedScanUnprovisioned()
{
    static BYTE scanning = FALSE;
    if (!scanning)
    {
        SetDlgItemText(IDC_SCAN_UNPROVISIONED, L"Stop Scanning");
        scanning = TRUE;
    }
    else
    {
        SetDlgItemText(IDC_SCAN_UNPROVISIONED, L"Scan Unprovisioned");
        scanning = FALSE;
    }
    WCHAR buf[128];
    wsprintf(buf, L"scan unprovisioned:%d", scanning);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_SCAN_UNPROVISIONED, &scanning, 1);
}

void CConfig::OnBnClickedProvisionConnect()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    identity_duration = (BYTE)GetHexValueInt(IDC_IDENTITY_DURATION);
    BYTE    use_gatt = (BYTE)((CButton *)GetDlgItem(IDC_USE_GATT))->GetCheck();
    UINT    conn_id = 100;
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = (BYTE)conn_id;
    *p++ = (BYTE)(conn_id >> 8);
    *p++ = (BYTE)(conn_id >> 16);
    *p++ = (BYTE)(conn_id >> 24);
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_PROVISION_UUID, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    *p++ = identity_duration;
    *p++ = use_gatt;
    WCHAR buf[128];
    wsprintf(buf, L"Provision: conn_id:%x addr:%x", conn_id, dst);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROVISION_CONNECT, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedProvisionDisconnect()
{
//    UINT    conn_id = 100;
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = (BYTE)conn_id;
    *p++ = (BYTE)(conn_id >> 8);
    *p++ = (BYTE)(conn_id >> 16);
    *p++ = (BYTE)(conn_id >> 24);
    WCHAR buf[128];
    wsprintf(buf, L"Provision: Disconnect conn_id:%x", conn_id);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROVISION_DISCONNECT, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedProvisionStart()
{
    BYTE algorithm = 0;
    BYTE auth_method = ((CComboBox *)GetDlgItem(IDC_AUTH_METHOD))->GetCurSel();
    auth_action = 0;
    BYTE auth_size = 0;
    if ((auth_method == 2) || (auth_method == 3)) // authentication with Output OOB
    {
        auth_action = ((CComboBox *)GetDlgItem(IDC_AUTH_ACTION))->GetCurSel();
        auth_size = GetDlgItemInt(IDC_AUTH_SIZE);
        if ((auth_size == 0) || (auth_size > 8))
        {
            MessageBox(L"Auth size should be 1-8");
            return;
        }
    }
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = (BYTE)conn_id;
    *p++ = (BYTE)(conn_id >> 8);
    *p++ = (BYTE)(conn_id >> 16);
    *p++ = (BYTE)(conn_id >> 24);
    *p++ = algorithm;
    *p++ = pub_key_type;    // use value passed in the capabilities
    *p++ = auth_method;
    *p++ = auth_action;
    *p++ = auth_size;
    WCHAR buf[128];
    wsprintf(buf, L"Provision: Start conn_id:%x", conn_id);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROVISION_START, buffer, (DWORD)(p - buffer));
}

void CConfig::OnCbnSelchangeAuthMethod()
{
    CComboBox *pCb = (CComboBox *)GetDlgItem(IDC_AUTH_ACTION);
    pCb->ResetContent();
    BYTE auth_method = ((CComboBox *)GetDlgItem(IDC_AUTH_METHOD))->GetCurSel();
    if (auth_method == 2)
    {
        pCb->AddString(L"Blink");
        pCb->AddString(L"Beep");
        pCb->AddString(L"Vibrate");
        pCb->AddString(L"Output Numeric");
        pCb->AddString(L"Output Alphanumeric");
        pCb->SetCurSel(0);
    }
    else if (auth_method == 3)
    {
        pCb->AddString(L"Push");
        pCb->AddString(L"Twist");
        pCb->AddString(L"Input Numeric");
        pCb->AddString(L"Input Alphanumeric");
        pCb->SetCurSel(0);
    }
}

// All Configuration messages are send using device key generated during provisioning
void CConfig::SetDeviceKey()
{
    if (memcmp(dev_key, configured_dev_key, 16) != 0)
    {
        memcpy(configured_dev_key, dev_key, 16);

        USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
        BYTE    buffer[128];
        LPBYTE  p = buffer;
        *p++ = dst & 0xff;
        *p++ = (dst >> 8) & 0xff;
        memcpy(p, dev_key, 16);
        p += 16;
        m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_SET_DEVICE_KEY, buffer, (DWORD)(p - buffer));

    }
}

void CConfig::OnBnClickedHeartbeatSubscriptionGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Heartbeat Subscription Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_SUBSCRIPTION_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHeartbeatSubscriptionSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT subsription_src = (USHORT)GetHexValueInt(IDC_HEARTBEAT_SUBSCRIPTION_SRC);
    USHORT subsription_dst = (USHORT)GetHexValueInt(IDC_HEARTBEAT_SUBSCRIPTION_DST);
    ULONG subsription_period = (ULONG)GetHexValueInt(IDC_HEARTBEAT_SUBSCRIPTION_PERIOD);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = subsription_src & 0xff;
    *p++ = (subsription_src >> 8) & 0xff;
    *p++ = subsription_dst & 0xff;
    *p++ = (subsription_dst >> 8) & 0xff;
    *p++ = subsription_period & 0xff;
    *p++ = (subsription_period >> 8) & 0xff;
    *p++ = (subsription_period >> 16) & 0xff;
    *p++ = (subsription_period >> 24) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Heartbeat Subscription Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_SUBSCRIPTION_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHeartbeatPublicationGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Heartbeat Publication Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_PUBLICATION_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHeartbeatPublicationSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT publication_dst = (USHORT)GetHexValueInt(IDC_HEARTBEAT_PUBLICATION_DST);
    ULONG  publication_count = GetHexValueInt(IDC_HEARTBEAT_PUBLICATION_COUNT);
    ULONG  publication_period = GetHexValueInt(IDC_HEARTBEAT_PUBLICATION_PERIOD);
    BYTE   publication_feature_proxy = ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_PROXY)))->GetCheck();
    BYTE   publication_feature_relay = ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_RELAY)))->GetCheck();
    BYTE   publication_feature_friend = ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_FRIEND)))->GetCheck();
    BYTE   publication_feature_low_power_mode = ((CButton *)(GetDlgItem(IDC_HEARTBEAT_PUB_LOW_POWER_MODE)))->GetCheck();
    USHORT publication_net_key_idx = (USHORT)GetHexValueInt(IDC_HEARTBEAT_PUBLICATION_NET_KEY_IDX);
    BYTE   publication_ttl = (BYTE)GetHexValueInt(IDC_HEARTBEAT_PUBLICATION_TTL);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = publication_dst & 0xff;
    *p++ = (publication_dst >> 8) & 0xff;
    *p++ = publication_count & 0xff;
    *p++ = (publication_count >> 8) & 0xff;
    *p++ = (publication_count >> 16) & 0xff;
    *p++ = (publication_count >> 24) & 0xff;
    *p++ = publication_period & 0xff;
    *p++ = (publication_period >> 8) & 0xff;
    *p++ = (publication_period >> 16) & 0xff;
    *p++ = (publication_period >> 24) & 0xff;
    *p++ = publication_ttl;
    *p++ = publication_feature_relay;
    *p++ = publication_feature_proxy;
    *p++ = publication_feature_friend;
    *p++ = publication_feature_low_power_mode;
    *p++ = publication_net_key_idx & 0xff;
    *p++ = (publication_net_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Heartbeat Publication Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_HEARBEAT_PUBLICATION_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetworkTransmitIntervalGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Network Transmit Interval Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NETWORK_TRANSMIT_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetworkTransmitIntervalSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE network_transmit_count = (BYTE)GetHexValueInt(IDC_NETWORK_TRANSMIT_COUNT);
    USHORT network_transmit_interval = (USHORT)GetHexValueInt(IDC_NETWORK_TRANSMIT_INTERVAL);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = network_transmit_count & 0xff;
    *p++ = network_transmit_interval & 0xff;
    *p++ = (network_transmit_interval >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Network Transmit Interval Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NETWORK_TRANSMIT_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNodeReset()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;

    m_trace->SetCurSel(m_trace->AddString(L"Node Reset"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_RESET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedConfigDataGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE composition_data_page = (BYTE)GetHexValueInt(IDC_COMPOSITION_DATA_PAGE);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = composition_data_page & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Composition Data Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_COMPOSITION_DATA_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedBeaconGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Beacon Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_BEACON_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedBeaconSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE beacon_state = (BYTE)((CComboBox *)(GetDlgItem(IDC_BEACON_STATE)))->GetCurSel();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = beacon_state & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Beacon Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_BEACON_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedDefaultTtlGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Default TTL Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_DEFAULT_TTL_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedDefaultTtlSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE default_ttl = (BYTE)GetHexValueInt(IDC_DEFAULT_TTL);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = default_ttl & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Default TTL Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_DEFAULT_TTL_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedGattProxyGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"GATT Proxy Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_GATT_PROXY_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedGattProxySet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE gatt_proxy_state = (BYTE)((CComboBox *)(GetDlgItem(IDC_GATT_PROXY_STATE)))->GetCurSel();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = gatt_proxy_state & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"GATT Proxy Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_GATT_PROXY_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedRelayGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;

    m_trace->SetCurSel(m_trace->AddString(L"Relay Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_RELAY_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedRelaySet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE relay_state = (BYTE)((CComboBox *)(GetDlgItem(IDC_RELAY_STATE)))->GetCurSel();
    BYTE relay_retransmit_count = (BYTE)GetHexValueInt(IDC_RELAY_TRANSMIT_COUNT);
    USHORT relay_retransmit_interval = (USHORT)GetHexValueInt(IDC_RELAY_TRANSMIT_INTERVAL);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = relay_state & 0xff;
    *p++ = relay_retransmit_count & 0xff;
    *p++ = relay_retransmit_interval & 0xff;
    *p++ = (relay_retransmit_interval >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Relay Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_RELAY_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedFriendGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;

    m_trace->SetCurSel(m_trace->AddString(L"Friend Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_FRIEND_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedFriendSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    BYTE friend_state = (BYTE)((CComboBox *)(GetDlgItem(IDC_FRIEND_STATE)))->GetCurSel();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = friend_state & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Friend Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_FRIEND_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedKeyRefreshPhaseGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_KEY_REFRESH_NET_KEY_INX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Key Refresh Phase Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_KEY_REFRESH_PHASE_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedKeyRefreshPhaseSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_KEY_REFRESH_NET_KEY_INX);
    BYTE phase = (BYTE)GetHexValueInt(IDC_KEY_REFRESH_PHASE);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = phase & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Key Refresh Phase Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_KEY_REFRESH_PHASE_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelPubGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_pub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_PUB_ELEMENT_ADDR);
    ULONG model_pub_model_id = GetHexValueInt(IDC_MODEL_PUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_pub_element_addr & 0xff;
    *p++ = (model_pub_element_addr >> 8) & 0xff;
    *p++ = model_pub_model_id & 0xff;
    *p++ = (model_pub_model_id >> 8) & 0xff;
    *p++ = (model_pub_model_id >> 16) & 0xff;
    *p++ = (model_pub_model_id >> 24) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model Publication Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_PUBLICATION_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelPubSet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_pub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_PUB_ELEMENT_ADDR);
    ULONG model_pub_model_id = GetHexValueInt(IDC_MODEL_PUB_MODEL_ID);
    ULONG model_pub_period = GetHexValueInt(IDC_MODEL_PUB_PERIOD);
    USHORT model_pub_app_key_idx = (USHORT)GetHexValueInt(IDC_MODEL_PUB_APP_KEY_IDX);
    BYTE model_pub_credential_flag = (BYTE)((CComboBox *)(GetDlgItem(IDC_MODEL_PUB_CREDENTIAL_FLAG)))->GetCurSel();
    BYTE model_pub_ttl = (BYTE)GetHexValueInt(IDC_PUBLISH_TTL);
    BYTE model_pub_retransmit_count = (BYTE)GetHexValueInt(IDC_MODEL_PUB_RETRANSMIT_COUNT);
    USHORT model_pub_retransmit_interval = (USHORT)GetHexValueInt(IDC_MODEL_PUB_RETRANSMIT_INTERVAL);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_pub_element_addr & 0xff;
    *p++ = (model_pub_element_addr >> 8) & 0xff;
    *p++ = model_pub_model_id & 0xff;
    *p++ = (model_pub_model_id >> 8) & 0xff;
    *p++ = (model_pub_model_id >> 16) & 0xff;
    *p++ = (model_pub_model_id >> 24) & 0xff;
    if (!((CButton *)GetDlgItem(IDC_USE_VIRTUAL_ADDR))->GetCheck())
    {
        USHORT model_pub_publish_addr = (USHORT)GetHexValueInt(IDC_MODEL_PUB_PUBLISH_ADDR);
        *p++ = model_pub_publish_addr & 0xff;
        *p++ = (model_pub_publish_addr >> 8) & 0xff;
        memset(p, 0, 14);
        p += 14;
    }
    else
    {
        memset(p, 0, 16);
        char szbuf[100];
        GetDlgItemTextA(m_hWnd, IDC_MODEL_PUB_VIRTUAL_ADDR, szbuf, 100);
        DWORD len = GetHexValue(szbuf, p, 16);
        p += 16;
    }
    *p++ = model_pub_app_key_idx & 0xff;
    *p++ = (model_pub_app_key_idx >> 8) & 0xff;
    *p++ = model_pub_credential_flag;
    *p++ = model_pub_ttl;
    *p++ = model_pub_period & 0xff;
    *p++ = (model_pub_period >> 8) & 0xff;
    *p++ = (model_pub_period >> 16) & 0xff;
    *p++ = (model_pub_period >> 24) & 0xff;
    *p++ = model_pub_retransmit_count;
    *p++ = model_pub_retransmit_interval & 0xff;
    *p++ = (model_pub_retransmit_interval >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model Publication Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_PUBLICATION_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelSubscriptionAdd()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_sub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ELEMENT_ADDR);
    ULONG model_sub_model_id = GetHexValueInt(IDC_MODEL_SUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_sub_element_addr & 0xff;
    *p++ = (model_sub_element_addr >> 8) & 0xff;
    *p++ = model_sub_model_id & 0xff;
    *p++ = (model_sub_model_id >> 8) & 0xff;
    *p++ = (model_sub_model_id >> 16) & 0xff;
    *p++ = (model_sub_model_id >> 24) & 0xff;
    if (!((CButton *)GetDlgItem(IDC_USE_VIRTUAL_ADDR2))->GetCheck())
    {
        USHORT model_sub_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ADDR);
        *p++ = model_sub_addr & 0xff;
        *p++ = (model_sub_addr >> 8) & 0xff;
        memset(p, 0, 14);
        p += 14;
    }
    else
    {
        memset(p, 0, 16);
        char szbuf[100];
        GetDlgItemTextA(m_hWnd, IDC_MODEL_SUB_VIRTUAL_ADDR, szbuf, 100);
        DWORD len = GetHexValue(szbuf, p, 16);
        p += 16;
    }
    m_trace->SetCurSel(m_trace->AddString(L"Model Subscription Add"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_ADD, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelSubscriptionDelete()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_sub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ELEMENT_ADDR);
    ULONG model_sub_model_id = GetHexValueInt(IDC_MODEL_SUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_sub_element_addr & 0xff;
    *p++ = (model_sub_element_addr >> 8) & 0xff;
    *p++ = model_sub_model_id & 0xff;
    *p++ = (model_sub_model_id >> 8) & 0xff;
    *p++ = (model_sub_model_id >> 16) & 0xff;
    *p++ = (model_sub_model_id >> 24) & 0xff;
    if (!((CButton *)GetDlgItem(IDC_USE_VIRTUAL_ADDR2))->GetCheck())
    {
        USHORT model_sub_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ADDR);
        *p++ = model_sub_addr & 0xff;
        *p++ = (model_sub_addr >> 8) & 0xff;
        memset(p, 0, 14);
        p += 14;
    }
    else
    {
        memset(p, 0, 16);
        char szbuf[100];
        GetDlgItemTextA(m_hWnd, IDC_MODEL_SUB_VIRTUAL_ADDR, szbuf, 100);
        DWORD len = GetHexValue(szbuf, p, 16);
        p += 16;
    }
    m_trace->SetCurSel(m_trace->AddString(L"Model Subscription Delete"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_DELETE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelSubscriptionOverwrite()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_sub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ELEMENT_ADDR);
    ULONG model_sub_model_id = GetHexValueInt(IDC_MODEL_SUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_sub_element_addr & 0xff;
    *p++ = (model_sub_element_addr >> 8) & 0xff;
    *p++ = model_sub_model_id & 0xff;
    *p++ = (model_sub_model_id >> 8) & 0xff;
    *p++ = (model_sub_model_id >> 16) & 0xff;
    *p++ = (model_sub_model_id >> 24) & 0xff;
    if (!((CButton *)GetDlgItem(IDC_USE_VIRTUAL_ADDR2))->GetCheck())
    {
        USHORT model_sub_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ADDR);
        *p++ = model_sub_addr & 0xff;
        *p++ = (model_sub_addr >> 8) & 0xff;
        memset(p, 0, 14);
        p += 14;
    }
    else
    {
        memset(p, 0, 16);
        char szbuf[100];
        GetDlgItemTextA(m_hWnd, IDC_MODEL_SUB_VIRTUAL_ADDR, szbuf, 100);
        DWORD len = GetHexValue(szbuf, p, 16);
        p += 16;
    }
    m_trace->SetCurSel(m_trace->AddString(L"Model Subscription Overwrite"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_OVERWRITE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelSubscriptionDeleteAll()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_sub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ELEMENT_ADDR);
    ULONG model_sub_model_id = GetHexValueInt(IDC_MODEL_SUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_sub_element_addr & 0xff;
    *p++ = (model_sub_element_addr >> 8) & 0xff;
    *p++ = model_sub_model_id & 0xff;
    *p++ = (model_sub_model_id >> 8) & 0xff;
    *p++ = (model_sub_model_id >> 16) & 0xff;
    *p++ = (model_sub_model_id >> 24) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model Subscription Delete All"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_DELETE_ALL, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedSubscriptionGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT model_sub_element_addr = (USHORT)GetHexValueInt(IDC_MODEL_SUB_ELEMENT_ADDR);
    ULONG model_sub_model_id = GetHexValueInt(IDC_MODEL_SUB_MODEL_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = model_sub_element_addr & 0xff;
    *p++ = (model_sub_element_addr >> 8) & 0xff;
    *p++ = model_sub_model_id & 0xff;
    *p++ = (model_sub_model_id >> 8) & 0xff;
    *p++ = (model_sub_model_id >> 16) & 0xff;
    *p++ = (model_sub_model_id >> 24) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model Subscription Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_SUBSCRIPTION_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetkeyAdd()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_NETKEY, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    m_trace->SetCurSel(m_trace->AddString(L"NetKey Add"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_ADD, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetkeyDelete()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"NetKey Delete"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_DELETE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetkeyUpdate()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_NETKEY, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    m_trace->SetCurSel(m_trace->AddString(L"NetKey Add"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_UPDATE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNetkeyGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"NetKey Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NET_KEY_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedAppkeyAdd()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APPKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_APPKEY, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    m_trace->SetCurSel(m_trace->AddString(L"AppKey Add"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_ADD, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedAppkeyDelete()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APPKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"AppKey Delete"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_DELETE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedAppkeyUpdate()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APPKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    memset(p, 0, 16);
    char szbuf[100];
    GetDlgItemTextA(m_hWnd, IDC_APPKEY, szbuf, 100);
    DWORD len = GetHexValue(szbuf, p, 16);
    p += 16;
    m_trace->SetCurSel(m_trace->AddString(L"AppKey Update"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_UPDATE, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedAppkeyGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"AppKey Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_APP_KEY_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelAppBind()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT element_addr = (USHORT)GetHexValueInt(IDC_MODEL_BIND_ELEMENT_ADDR);
    ULONG model_id = GetHexValueInt(IDC_MODEL_ID);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = element_addr & 0xff;
    *p++ = (element_addr >> 8) & 0xff;
    *p++ = model_id & 0xff;
    *p++ = (model_id >> 8) & 0xff;
    *p++ = (model_id >> 16) & 0xff;
    *p++ = (model_id >> 24) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model App Bind"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_BIND, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelAppUnbind()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT element_addr = (USHORT)GetHexValueInt(IDC_MODEL_BIND_ELEMENT_ADDR);
    ULONG model_id = GetHexValueInt(IDC_MODEL_ID);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = element_addr & 0xff;
    *p++ = (element_addr >> 8) & 0xff;
    *p++ = model_id & 0xff;
    *p++ = (model_id >> 8) & 0xff;
    *p++ = (model_id >> 16) & 0xff;
    *p++ = (model_id >> 24) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model App Unbind"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_UNBIND, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedModelAppGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT element_addr = (USHORT)GetHexValueInt(IDC_MODEL_BIND_ELEMENT_ADDR);
    ULONG model_id = GetHexValueInt(IDC_MODEL_ID);
    USHORT app_key_idx = (USHORT)GetHexValueInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = element_addr & 0xff;
    *p++ = (element_addr >> 8) & 0xff;
    *p++ = model_id & 0xff;
    *p++ = (model_id >> 8) & 0xff;
    *p++ = (model_id >> 16) & 0xff;
    *p++ = (model_id >> 24) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Model App Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_MODEL_APP_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNodeIdentityGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NODE_IDENTITY_NETKEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Node Identity Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_IDENTITY_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedNodeIdentitySet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = 0xffff;
    USHORT net_key_idx = (USHORT)GetHexValueInt(IDC_NODE_IDENTITY_NETKEY_IDX);
    BYTE node_identity_state = (BYTE)((CComboBox *)(GetDlgItem(IDC_NODE_IDENTITY_STATE)))->GetCurSel();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = net_key_idx & 0xff;
    *p++ = (net_key_idx >> 8) & 0xff;
    *p++ = node_identity_state;
    m_trace->SetCurSel(m_trace->AddString(L"Node Identity Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_NODE_IDENTITY_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedLpnPollTimeoutGet()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT lpn_addr = (USHORT)GetHexValueInt(IDC_LPN_ADDR);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = lpn_addr & 0xff;
    *p++ = (lpn_addr >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"LPN Poll Timeout Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_CONFIG_LPN_POLL_TIMEOUT_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthFaultGet()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    USHORT company_id = (USHORT)GetHexValueInt(IDC_HEALTH_FAULT_COMPANY_ID);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    *p++ = company_id & 0xff;
    *p++ = (company_id >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Health Fault Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthFaultClear()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    USHORT company_id = (USHORT)GetHexValueInt(IDC_HEALTH_FAULT_COMPANY_ID);
    BYTE   reliable = (BYTE)((CButton *)GetDlgItem(IDC_RELIABLE_SEND))->GetCheck();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    *p++ = reliable;
    *p++ = company_id & 0xff;
    *p++ = (company_id >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Health Fault Clear"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_CLEAR, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthFaultTest()
{
    SetDeviceKey();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    USHORT company_id = (USHORT)GetHexValueInt(IDC_HEALTH_FAULT_COMPANY_ID);
    BYTE   test_id = (BYTE)GetHexValueInt(IDC_HEALTH_FAULT_TEST_ID);
    BYTE   reliable = (BYTE)((CButton *)GetDlgItem(IDC_RELIABLE_SEND))->GetCheck();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    *p++ = reliable;
    *p++ = test_id;
    *p++ = company_id & 0xff;
    *p++ = (company_id >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Health Fault Test"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_FAULT_TEST, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthPeriodGet()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Health Period Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_PERIOD_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthPeriodSet()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    BYTE   period = (BYTE)GetHexValueInt(IDC_HEALTH_PERIOD);
    BYTE   reliable = (BYTE)((CButton *)GetDlgItem(IDC_RELIABLE_SEND))->GetCheck();
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    *p++ = reliable;
    *p++ = period;
    m_trace->SetCurSel(m_trace->AddString(L"Health Period Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_PERIOD_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthAttentionGet()
{
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    m_trace->SetCurSel(m_trace->AddString(L"Attention Get"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_ATTENTION_GET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedHealthAttentionSet()
{
    BYTE   attention = (BYTE)GetHexValueInt(IDC_HEALTH_ATTENTION);
    BYTE   reliable = (BYTE)((CButton *)GetDlgItem(IDC_RELIABLE_SEND))->GetCheck();
    USHORT dst = (USHORT)GetHexValueInt(IDC_DST);
    USHORT app_key_idx = GetDlgItemInt(IDC_APP_KEY_IDX);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = dst & 0xff;
    *p++ = (dst >> 8) & 0xff;
    *p++ = app_key_idx & 0xff;
    *p++ = (app_key_idx >> 8) & 0xff;
    *p++ = reliable;
    *p++ = attention;
    m_trace->SetCurSel(m_trace->AddString(L"Attention Timer Set"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_HEALTH_ATTENTION_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedFindProxy()
{
    static BYTE scanning = FALSE;
    if (!scanning)
    {
        SetDlgItemText(IDC_FIND_PROXY, L"Stop Scanning");
        scanning = TRUE;
    }
    else
    {
        SetDlgItemText(IDC_FIND_PROXY, L"Find Proxy");
        scanning = FALSE;
    }
    WCHAR buf[128];
    wsprintf(buf, L"Find proxy:%d", scanning);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_SEARCH_PROXY, &scanning, 1);
}

void CConfig::OnBnClickedConnectProxy()
{
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    memset(p, 0, 6);
    char szbuf[100];
    BYTE bd_addr[6];
    GetDlgItemTextA(m_hWnd, IDC_BD_ADDR, szbuf, 100);
    DWORD len = GetHexValue(szbuf, bd_addr, 6);
    for (int i = 0; i < 6; i++)
        *p++ = bd_addr[5 - i];
    *p++ = GetDlgItemInt(IDC_BD_ADDR_TYPE);
    m_trace->SetCurSel(m_trace->AddString(L"Proxy: Connect"));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROXY_CONNECT, buffer, (DWORD)(p - buffer));
}


void CConfig::OnBnClickedDisconnectProxy()
{
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    *p++ = (BYTE)conn_id;
    *p++ = (BYTE)(conn_id >> 8);
    *p++ = (BYTE)(conn_id >> 16);
    *p++ = (BYTE)(conn_id >> 24);
    WCHAR buf[128];
    wsprintf(buf, L"Proxy: Disconnect conn_id:%x", conn_id);
    m_trace->SetCurSel(m_trace->AddString(buf));
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROXY_DISCONNECT, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedProxyFilterTypeSet()
{
    BYTE type = (BYTE)GetHexValueInt(IDC_PROXY_FILTER_TYPE);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    WCHAR buf[128];
    wsprintf(buf, L"Proxy: Filter Type:%x", type);
    m_trace->SetCurSel(m_trace->AddString(buf));
    *p++ = (BYTE)type;
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_TYPE_SET, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedProxyFilterAddrAdd()
{
    USHORT  addr = (USHORT)GetHexValueInt(IDC_PROXY_FILTER_ADDR);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    WCHAR buf[128];
    wsprintf(buf, L"Proxy: Filter Add:%x", addr);
    m_trace->SetCurSel(m_trace->AddString(buf));
    *p++ = (BYTE)addr;
    *p++ = (BYTE)(addr >> 8);
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_ADDRESSES_ADD, buffer, (DWORD)(p - buffer));
}

void CConfig::OnBnClickedProxyFilterAddrDelete()
{
    USHORT  addr = (USHORT)GetHexValueInt(IDC_PROXY_FILTER_ADDR);
    BYTE    buffer[128];
    LPBYTE  p = buffer;
    WCHAR buf[128];
    wsprintf(buf, L"Proxy: Filter Add:%x", addr);
    m_trace->SetCurSel(m_trace->AddString(buf));
    *p++ = (BYTE)addr;
    *p++ = (BYTE)(addr >> 8);
    m_ComHelper->SendWicedCommand(HCI_CONTROL_MESH_COMMAND_PROXY_FILTER_ADDRESSES_DELETE, buffer, (DWORD)(p - buffer));
}

