/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
 *
 *
 * This file shows how to create a device which implements mesh on/off client model.
 */
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3006
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_power_level_dev_name[]       = "Power OnOff Client";
uint8_t mesh_power_level_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_power_level_mfr_name[]       = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_power_level_model_num[]      = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_power_level_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };


wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_POWER_LEVEL_CLIENT,
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

#define MESH_POWER_LEVEL_CLIENT_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 0,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = 0,                                            // Number of properties in the array models
        .properties = NULL,                                             // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};

/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_power_level_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_power_level_client_level_get(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_level_set(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_last_get(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_default_get(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_default_set(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_range_get(uint8_t *p_data, uint32_t length);
static void mesh_power_level_client_range_set(uint8_t *p_data, uint32_t length);
wiced_bt_mesh_event_t *mesh_power_level_client_create_event(uint8_t **p_data, uint32_t *length);

#ifdef HCI_CONTROL
static void mesh_power_level_hci_event_send_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_level_status_data_t *p_data);
static void mesh_power_level_hci_event_send_last_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_level_last_data_t *p_data);
static void mesh_power_level_hci_event_send_default_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_default_data_t *p_data);
static void mesh_power_level_hci_event_send_range_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_range_status_data_t *p_data);
#endif

/******************************************************
 *          Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    wiced_bt_mesh_model_power_level_client_init(MESH_POWER_LEVEL_CLIENT_ELEMENT_INDEX, mesh_power_level_client_message_handler, is_provisioned);
}

/*
 * Process event received from the onoff Server.
 */
void mesh_power_level_client_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    WICED_BT_TRACE("pwr lvl clt msg:%d\n", event);

    switch (event)
    {
    case WICED_BT_MESH_POWER_LEVEL_STATUS:
        WICED_BT_TRACE("present:%d target:%d remain time:%d\n",
                ((wiced_bt_mesh_power_level_status_data_t *)p_data)->present_power,
                ((wiced_bt_mesh_power_level_status_data_t *)p_data)->target_power,
                ((wiced_bt_mesh_power_level_status_data_t *)p_data)->remaining_time);
#if defined HCI_CONTROL
        mesh_power_level_hci_event_send_status(p_event, (wiced_bt_mesh_power_level_status_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_POWER_LEVEL_LAST_STATUS:
        WICED_BT_TRACE("last:%d\n", ((wiced_bt_mesh_power_level_last_data_t *)p_data)->power);
#if defined HCI_CONTROL
        mesh_power_level_hci_event_send_last_status(p_event, (wiced_bt_mesh_power_level_last_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_POWER_LEVEL_DEFAULT_STATUS:
        WICED_BT_TRACE("default:%d\n", ((wiced_bt_mesh_power_default_data_t *)p_data)->power);
#if defined HCI_CONTROL
        mesh_power_level_hci_event_send_default_status(p_event, (wiced_bt_mesh_power_default_data_t *)p_data);
#endif
        break;

    case WICED_BT_MESH_POWER_LEVEL_RANGE_STATUS:
        WICED_BT_TRACE("range min:%d max:%d\n",
                ((wiced_bt_mesh_power_range_status_data_t *)p_data)->power_min,
                ((wiced_bt_mesh_power_range_status_data_t *)p_data)->power_max);
#if defined HCI_CONTROL
        mesh_power_level_hci_event_send_range_status(p_event, (wiced_bt_mesh_power_range_status_data_t *)p_data);
#endif
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
    wiced_bt_mesh_release_event(p_event);
}


/*
 * In 2 chip solutions MCU can send commands to change level state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_GET:
        mesh_power_level_client_level_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_SET:
        mesh_power_level_client_level_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_LAST_GET:
        mesh_power_level_client_last_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_DEFAULT_GET:
        mesh_power_level_client_default_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_DEFAULT_SET:
        mesh_power_level_client_default_set(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_RANGE_GET:
        mesh_power_level_client_range_get(p_data, length);
        break;

    case HCI_CONTROL_MESH_COMMAND_POWER_LEVEL_RANGE_SET:
        mesh_power_level_client_range_set(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        return WICED_FALSE;
    }
    return WICED_TRUE;
}

/*
 * Create mesh event for unsolicited message and send power onoff get command
 */
void mesh_power_level_client_level_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    wiced_bt_mesh_model_power_level_client_send_get(p_event);
}

/*
 * Create mesh event for unsolicited message and send power level set command
 */
void mesh_power_level_client_level_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t        *p_event;
    wiced_bt_mesh_power_level_set_level_t set_data;
    uint16_t dst;
    uint16_t app_key_idx;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    STREAM_TO_UINT8(p_event->reply, p_data);
    STREAM_TO_UINT16(set_data.level, p_data);
    STREAM_TO_UINT32(set_data.transition_time, p_data);
    STREAM_TO_UINT16(set_data.delay, p_data);

    wiced_bt_mesh_model_power_level_client_send_set(p_event, &set_data);
}

/*
 * Create mesh event for unsolicited message and send power Get Last Power Level command
 */
void mesh_power_level_client_last_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    wiced_bt_mesh_model_power_level_client_send_last_get(p_event, NULL);
}

/*
 * Create mesh event for unsolicited message and send power Get Default Power Level command
 */
void mesh_power_level_client_default_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    wiced_bt_mesh_model_power_level_client_send_default_get(p_event);
}

/*
 * Create mesh event for unsolicited message and send power Set Default Power Level command
 */
void mesh_power_level_client_default_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    wiced_bt_mesh_power_default_data_t set_data;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    STREAM_TO_UINT8(p_event->reply, p_data);
    STREAM_TO_UINT16(set_data.power, p_data);

    wiced_bt_mesh_model_power_level_client_send_default_set(p_event, &set_data);
}

/*
 * Create mesh event for unsolicited message and send power Get Default Power Level command
 */
void mesh_power_level_client_range_get(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    wiced_bt_mesh_model_power_level_client_send_range_get(p_event);
}

/*
 * Create mesh event for unsolicited message and send power Set Range Power Level command
 */
void mesh_power_level_client_range_set(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    wiced_bt_mesh_power_level_range_set_data_t set_data;

    if ((p_event = mesh_power_level_client_create_event(&p_data, &length)) == NULL)
        return;

    STREAM_TO_UINT8(p_event->reply, p_data);
    STREAM_TO_UINT16(set_data.power_min, p_data);
    STREAM_TO_UINT16(set_data.power_max, p_data);

    wiced_bt_mesh_model_power_level_client_send_range_set(p_event, &set_data);
}

wiced_bt_mesh_event_t *mesh_power_level_client_create_event(uint8_t **p_data, uint32_t *length)
{
    wiced_bt_mesh_event_t *p_event = NULL;
    uint16_t dst;
    uint16_t app_key_idx;

    if (*length >= 4)
    {
        STREAM_TO_UINT16(dst, *p_data);
        STREAM_TO_UINT16(app_key_idx, *p_data);
        *length -= 4;

        p_event = wiced_bt_mesh_create_event(MESH_POWER_LEVEL_CLIENT_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, WICED_BT_MESH_CORE_MODEL_ID_GENERIC_POWER_LEVEL_CLNT, dst, app_key_idx);
        if (p_event == NULL)
        {
            WICED_BT_TRACE("pwr lvl clnt: no mem dst:%04x app_key_idx:%d\n", dst, app_key_idx);
        }
    }
    return p_event;
}

#ifdef HCI_CONTROL
/*
 * Send Power Level Status event over transport
 */
void mesh_power_level_hci_event_send_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_level_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->present_power);
    UINT16_TO_STREAM(p, p_data->target_power);
    UINT32_TO_STREAM(p, p_data->remaining_time);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_POWER_LEVEL_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Last Power Level Status event over transport
 */
void mesh_power_level_hci_event_send_last_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_level_last_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->power);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_POWER_LEVEL_LAST_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Default Power Level Status event over transport
 */
void mesh_power_level_hci_event_send_default_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_default_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT16_TO_STREAM(p, p_data->power);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_POWER_LEVEL_DEFAULT_STATUS, buffer, (uint16_t)(p - buffer));
}

/*
 * Send Range Power Level Status event over transport
 */
void mesh_power_level_hci_event_send_range_status(wiced_bt_mesh_event_t *p_event, wiced_bt_mesh_power_range_status_data_t *p_data)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_event->src);
    UINT8_TO_STREAM(p, p_event->app_key_idx);
    UINT8_TO_STREAM(p, p_event->element_idx);
    UINT8_TO_STREAM(p, p_data->status);
    UINT16_TO_STREAM(p, p_data->power_min);
    UINT16_TO_STREAM(p, p_data->power_max);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_POWER_LEVEL_RANGE_STATUS, buffer, (uint16_t)(p - buffer));
}
#endif
