/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 *
 * This file shows how to create a device which publishes user_property level.
 */
#include "wiced_bt_uuid.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_mesh_models.h"
#include "wiced_bt_trace.h"
#include "wiced_timer.h"
#ifdef HCI_CONTROL
#include "wiced_transport.h"
#include "hci_control_api.h"
#endif

 // defines Friendship Mode
#define MESH_FRIENDSHIP_NONE  0
#define MESH_FRIENDSHIP_FRND  1
#define MESH_FRIENDSHIP_LPN   2

/******************************************************
 *          Constants
 ******************************************************/
#define MESH_PID                0x3106
#define MESH_VID                0x0001
#define MESH_CACHE_REPLAY_SIZE  0x0008

char    mesh_property_server_dev_name[]       = "Property Server";
uint8_t mesh_property_server_appearance[2]    = { BIT16_TO_8(APPEARANCE_GENERIC_TAG) };
char    mesh_property_server_mfr_name[36]     = { 'C', 'y', 'p', 'r', 'e', 's', 's', 0, };
char    mesh_property_server_model_num[16]    = { '1', '2', '3', '4', 0, 0, 0, 0 };
uint8_t mesh_property_server_system_id[]      = { 0xbb, 0xb8, 0xa1, 0x80, 0x5f, 0x9f, 0x91, 0x71 };
uint8_t mesh_property_motion_threashold[]     = { 50 };

wiced_bt_mesh_core_config_model_t   mesh_element1_models[] =
{
    WICED_BT_MESH_DEVICE,
    WICED_BT_MESH_MODEL_USER_PROPERTY_SERVER,
    WICED_BT_MESH_MODEL_ADMIN_PROPERTY_SERVER,
    WICED_BT_MESH_MODEL_MANUFACTURER_PROPERTY_SERVER,
    WICED_BT_MESH_MODEL_CLIENT_PROPERTY_SERVER,
};
#define MESH_APP_NUM_MODELS  (sizeof(mesh_element1_models) / sizeof(wiced_bt_mesh_core_config_model_t))

wiced_bt_mesh_core_config_property_t mesh_element1_properties[] =
{
    {
        .id          = WICED_BT_MESH_PROPERTY_DEVICE_APPEARANCE,
        .type        = WICED_BT_MESH_PROPERTY_TYPE_MANUFACTURER,
        .user_access = WICED_BT_MESH_PROPERTY_ID_READABLE,
        .max_len     = 2,
        .value       = mesh_property_server_appearance
    },
    { 
        .id          = WICED_BT_MESH_PROPERTY_DEVICE_MANUFACTURER_NAME, 
        .type        = WICED_BT_MESH_PROPERTY_TYPE_MANUFACTURER, 
        .user_access = WICED_BT_MESH_PROPERTY_ID_READABLE, 
        .max_len     = 36,
        .value       = mesh_property_server_mfr_name
    },
    { 
        .id          = WICED_BT_MESH_PROPERTY_DEVICE_SERIAL_NUMBER,     
        .type        = WICED_BT_MESH_PROPERTY_TYPE_MANUFACTURER, 
        .user_access = WICED_BT_MESH_PROPERTY_ID_READABLE,
        .max_len     = 16,
        .value       = mesh_property_server_model_num
    },
    { 
        .id          = WICED_BT_MESH_PROPERTY_MOTION_THRESHOLD,
        .type        = WICED_BT_MESH_PROPERTY_TYPE_ADMIN,        
        .user_access = WICED_BT_MESH_PROPERTY_ID_READABLE | WICED_BT_MESH_PROPERTY_ID_WRITABLE, 
        .max_len     = 1,
        .value       = mesh_property_motion_threashold
    },
};
#define MESH_APP_NUM_PROPERTIES (sizeof(mesh_element1_properties) / sizeof(wiced_bt_mesh_core_config_property_t))

#define MESH_PROPERTY_SERVER_ELEMENT_INDEX   0

wiced_bt_mesh_core_config_element_t mesh_elements[] =
{
    {
        .location = MESH_ELEM_LOC_MAIN,                                 // location description as defined in the GATT Bluetooth Namespace Descriptors section of the Bluetooth SIG Assigned Numbers
        .default_transition_time = MESH_DEFAULT_TRANSITION_TIME_IN_MS,  // Default transition time for models of the element in milliseconds
        .onpowerup_state = WICED_BT_MESH_ON_POWER_UP_STATE_RESTORE,     // Default element behavior on power up
        .default_level = 0,                                             // Default value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_min = 1,                                                 // Minimum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .range_max = 0xffff,                                            // Maximum value of the variable controlled on this element (for example power, lightness, temperature, hue...)
        .move_rollover = 0,                                             // If true when level gets to range_max during move operation, it switches to min, otherwise move stops.
        .properties_num = MESH_APP_NUM_PROPERTIES,                      // Number of properties in the array models
        .properties = mesh_element1_properties,                         // Array of properties in the element.
        .sensors_num = 0,                                               // Number of sensors in the sensor array
        .sensors = NULL,                                                // Array of sensors of that element
        .models_num = MESH_APP_NUM_MODELS,                              // Number of models in the array models
        .models = mesh_element1_models,                                 // Array of models located in that element. Model data is defined by structure wiced_bt_mesh_core_config_model_t
    },
};

wiced_bt_mesh_core_config_t  mesh_config =
{
    .company_id         = MESH_COMPANY_ID_CYPRESS,                  // Company identifier assigned by the Bluetooth SIG
    .product_id         = MESH_PID,                                 // Vendor-assigned product identifier
    .vendor_id          = MESH_VID,                                 // Vendor-assigned product version identifier
    .replay_cache_size  = MESH_CACHE_REPLAY_SIZE,                   // minimum number of replay protection list entries in a device
#if MESH_FRIENDSHIP == MESH_FRIENDSHIP_LPN
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_LOW_POWER, // A bit field indicating the device features. In Low Power mode no Relay, no Proxy and no Friend
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window = 0,                                        // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len  = 0                                         // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 2,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 2,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 3,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 100,                               // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 36000                              // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#elif MESH_FRIENDSHIP == MESH_FRIENDSHIP_FRND
    .features           = WICED_BT_MESH_CORE_FEATURE_BIT_FRIEND | WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,   // In Friend mode support friend, relay
    .friend_cfg         =                                           // Configuration of the Friend Feature(Receive Window in Ms, messages cache)
    {
        .receive_window        = 200,
        .cache_buf_len         = 300                                // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#else
    WICED_BT_MESH_CORE_FEATURE_BIT_RELAY | WICED_BT_MESH_CORE_FEATURE_BIT_GATT_PROXY_SERVER,                           // support relay
    .friend_cfg         =                                           // Empty Configuration of the Friend Feature
    {
        .receive_window         = 0,                                // Receive Window value in milliseconds supported by the Friend node.
        .cache_buf_len          = 0                                 // Length of the buffer for the cache
    },
    .low_power          =                                           // Configuration of the Low Power Feature
    {
        .rssi_factor           = 0,                                 // contribution of the RSSI measured by the Friend node used in Friend Offer Delay calculations.
        .receive_window_factor = 0,                                 // contribution of the supported Receive Window used in Friend Offer Delay calculations.
        .min_cache_size_log    = 0,                                 // minimum number of messages that the Friend node can store in its Friend Cache.
        .receive_delay         = 0,                                 // Receive delay in 1 ms units to be requested by the Low Power node.
        .poll_timeout          = 0                                  // Poll timeout in 100ms unite to bt requested by the Low Power node.
    },
#endif
    .elements_num  = (uint8_t)(sizeof(mesh_elements) / sizeof(mesh_elements[0])),   // number of elements on this device
    .elements      = mesh_elements                                  // Array of elements for this device
};


/******************************************************
 *          Structures
 ******************************************************/

/******************************************************
 *          Function Prototypes
 ******************************************************/
static void mesh_property_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data);
static void mesh_property_server_status_changed(uint8_t *p_data, uint32_t length);
static void mesh_property_process_set(wiced_bt_mesh_property_set_data_t *p_data);

#ifdef HCI_CONTROL
static void mesh_property_hci_event_send_set(wiced_bt_mesh_property_set_data_t *p_set);
#endif

extern wiced_bt_mesh_core_config_t  mesh_config;

/******************************************************
 *          Variables Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void mesh_app_init(wiced_bool_t is_provisioned)
{
    wiced_bt_mesh_model_property_server_init(MESH_PROPERTY_SERVER_ELEMENT_INDEX, mesh_property_server_message_handler, is_provisioned);
}

/*
 * Process event received from the Property Client.
 */
void mesh_property_server_message_handler(uint16_t event, wiced_bt_mesh_event_t *p_event, void *p_data)
{
    WICED_BT_TRACE("user_property srv msg:%d\n", event);
    wiced_bt_mesh_property_get_data_t *p_get;
    wiced_bt_mesh_property_set_data_t *p_set;

    switch (event)
    {
    case WICED_BT_MESH_USER_PROPERTY_SET:
#if defined HCI_CONTROL
        // In some cases we do not know the state of the user_property and need to ask MCU whenever client asks
        mesh_property_hci_event_send_set((wiced_bt_mesh_property_set_data_t *)p_data);
#endif
        mesh_property_process_set((wiced_bt_mesh_property_set_data_t *)p_data);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
    }
}

/*
 * In 2 chip solutions MCU can send commands to change user_property state.
 */
uint32_t mesh_app_proc_rx_cmd(uint16_t opcode, uint8_t *p_data, uint32_t length)
{
    WICED_BT_TRACE("[%s] cmd_opcode 0x%02x\n", __FUNCTION__, opcode);

    switch (opcode)
    {
    case HCI_CONTROL_MESH_COMMAND_PROPERTY_SET:
        mesh_property_server_status_changed(p_data, length);
        break;

    default:
        WICED_BT_TRACE("unknown\n");
        break;
    }
    return 0;
}

/*
 * Create mesh event for unsolicited message and send Property Status event
 */
void mesh_property_server_status_changed(uint8_t *p_data, uint32_t length)
{
    wiced_bt_mesh_event_t *p_event;
    uint16_t dst;
    uint16_t app_key_idx;
    uint16_t model_id;
    uint8_t type;
    uint16_t id;
    uint32_t len;
    int i;

    STREAM_TO_UINT16(dst, p_data);
    STREAM_TO_UINT16(app_key_idx, p_data);
    STREAM_TO_UINT8(type, p_data);
    STREAM_TO_UINT16(id, p_data);
    len = length - 7;

    // If value of the User Property ID field that does not identify any existing User Property, Send Reply without bothering the app. 
    for (i = 0; i < mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties_num; i++)
    {
        if (mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i].id == id)
        {
            STREAM_TO_ARRAY(mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i].value, p_data, length - 7);
            
            if (type == 0)
                model_id = WICED_BT_MESH_CORE_MODEL_ID_GENERIC_ADMIN_PROPERTY_SRV;
            else if (type == 1)
                model_id = WICED_BT_MESH_CORE_MODEL_ID_GENERIC_MANUFACT_PROPERTY_SRV;
            else
                model_id = WICED_BT_MESH_CORE_MODEL_ID_GENERIC_USER_PROPERTY_SRV;

            p_event = wiced_bt_mesh_create_event(MESH_PROPERTY_SERVER_ELEMENT_INDEX, MESH_COMPANY_ID_BT_SIG, model_id, dst, app_key_idx);

            if (p_event == NULL)
            {
                WICED_BT_TRACE("user_property state changed: no mem\n");
                return;
            }
            wiced_bt_mesh_model_property_server_send_property_status(p_event, type, &mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i]);
            return;
        }
    }
}

/*
 * Sample implementation of the command from the Property client to set the state
 */
void mesh_property_process_set(wiced_bt_mesh_property_set_data_t *p_set)
{
    int i;

    WICED_BT_TRACE("user_property srv set id:%04x len:%d\n", p_set->id, p_set->len);

    for (i = 0; i < mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties_num; i++)
    {
        if (mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i].id == p_set->id)
        {
            mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i].user_access = p_set->access;
            if (p_set->len != 0)
            {
                memcpy (mesh_config.elements[MESH_PROPERTY_SERVER_ELEMENT_INDEX].properties[i].value, p_set->value, p_set->len);
            }
            break;
        }
    }
}

#ifdef HCI_CONTROL

/*
 * Send Property Set event over transport
 */
void mesh_property_hci_event_send_set(wiced_bt_mesh_property_set_data_t *p_set)
{
    uint8_t buffer[30];
    uint8_t *p = buffer;

    UINT16_TO_STREAM(p, p_set->id);
    ARRAY_TO_STREAM(p, p_set->value, p_set->len);

    wiced_transport_send_data(HCI_CONTROL_MESH_EVENT_PROPERTY_SET, buffer, (uint16_t)(p - buffer));
}

#endif
