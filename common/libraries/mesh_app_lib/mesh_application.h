/*
* Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
*/

/** @file
*
* mesh definitions
*
*/


#ifndef __MESH_APPLICATION_H__
#define __MESH_APPLICATION_H__

// Definitions for handles used in the GATT database
enum
{
    MESH_HANDLE_GATT_SERVICE = 0x01,

    MESH_HANDLE_GAP_SERVICE = 0x14,
        MESH_HANDLE_GAP_SERVICE_CHARACTERISTIC_DEV_NAME,
        MESH_HANDLE_GAP_SERVICE_CHARACTERISTIC_DEV_NAME_VAL,

        MESH_HANDLE_GAP_SERVICE_CHARACTERISTIC_APPEARANCE,
        MESH_HANDLE_GAP_SERVICE_CHARACTERISTIC_APPEARANCE_VAL,

    HANDLE_MESH_SERVICE_PROVISIONING = 0x28,
        HANDLE_CHAR_MESH_PROVISIONING_DATA_IN,
        HANDLE_CHAR_MESH_PROVISIONING_DATA_IN_VALUE,
        HANDLE_CHAR_MESH_PROVISIONING_DATA_OUT,
        HANDLE_CHAR_MESH_PROVISIONING_DATA_OUT_VALUE,
        HANDLE_DESCR_MESH_PROVISIONING_DATA_CLIENT_CONFIG,

    HANDLE_MESH_SERVICE_PROXY = 0x30,
        HANDLE_CHAR_MESH_PROXY_DATA_IN,
        HANDLE_CHAR_MESH_PROXY_DATA_IN_VALUE,
        HANDLE_CHAR_MESH_PROXY_DATA_OUT,
        HANDLE_CHAR_MESH_PROXY_DATA_OUT_VALUE,
        HANDLE_DESCR_MESH_PROXY_DATA_CLIENT_CONFIG,

    MESH_HANDLE_DEV_INFO_SERVICE = 0x40,
        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_MFR_NAME,
        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_MFR_NAME_VAL,

        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_MODEL_NUM,
        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_MODEL_NUM_VAL,

        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_SYSTEM_ID,
        MESH_HANDLE_DEV_INFO_SERVICE_CHARACTERISTIC_SYSTEM_ID_VAL,
#ifdef _DEB_COMMAND_SERVICE
    HANDLE_MESH_SERVICE_COMMAND = 0x50,
        HANDLE_CHAR_MESH_COMMAND_DATA,
        HANDLE_CHAR_MESH_COMMAND_DATA_VALUE,
        HANDLE_DESCR_MESH_COMMAND_DATA_CLIENT_CONFIG,
#endif
};


#ifdef __cplusplus 
extern "C" 
{ 
#endif 

#define MESH_APP_TIMEOUT_IN_SECONDS   1

// Following definitions shpuld be defined by mesh_model_lib
#define MESH_COMPANY_ID_BT_SIG              0x0000
#define MESH_COMPANY_ID_CYPRESS             0x0131
#define MESH_COMPANY_ID_SPECIAL             0xffff // Used to setup proxy filters

#if MESH_CHIP == 20703
#define NVRAM_ID_NODE_INFO          WICED_NVRAM_VSID_START
#elif MESH_CHIP == 20719
#define NVRAM_ID_NODE_INFO          WICED_NVRAM_VSID_START
#else
#define NVRAM_ID_NODE_INFO  (WICED_NVRAM_VSID_START + 0x100)  // ID of the memory block used for NVRAM access (+0x100 - workaround to get rid of lost NVM IDs)
#endif

#define NVRAM_ID_PAIRED_KEYS        (NVRAM_ID_NODE_INFO + 0)
#define NVRAM_ID_LOCAL_KEYS         (NVRAM_ID_NODE_INFO + 1)
#define NVRAM_ID_GEN_ONOFF          (NVRAM_ID_NODE_INFO + 2)
#define NVRAM_ID_DEF_TT             (NVRAM_ID_NODE_INFO + 3)
#define NVRAM_ID_MESH_START         (NVRAM_ID_NODE_INFO + 4)

#ifdef _DEB_COMMAND_SERVICE
// UUIDs for proprietary COMMAND service and characteristic
#define WICED_BT_MESH_CORE_UUID_SERVICE_COMMAND                 0x7FD3
#define WICED_BT_MESH_CORE_UUID_CHARACTERISTIC_COMMAND_DATA     0x7FCB
#endif

// device UUID length
#define MESH_DEVICE_UUID_LEN                      16

// mesh device ID (address) length
#define MESH_NODE_ID_LEN                          2

#ifdef _DEB_COMMAND_SERVICE
#define MESH_COMMAND_START_SCAN_UNPROVISIONED     1       // <cmd=1>(1 byte). Start sending UUIDs of detected unprovisioned devices
#define MESH_COMMAND_STOP_SCAN_UNPROVISIONED      2       // <cmd=2>(1 byte). Stops sending UUIDs of detected unprovisioned devices
#define MESH_COMMAND_PROVISION                    3       // <cmd=3>(1 byte), <uuid>(16bytes), <addr>(2bytes). Starts provisionning of device with uuid and assigns addrt to it
#define MESH_COMMAND_RESET_NODE                   4       // <cmd=4>(1 byte). Resets node and makes it unprovisioned

#define MESH_COMMAND_EVENT_UNPROVISIONED_DEV      1       // <event=1>(1 byte), <uuid>(16bytes). Detected unprovisioned device
#define MESH_COMMAND_EVENT_PB_END                 2       // <event=2>(1 byte), <conn_id>(4bytes), <reason>(1byte). End of provisioning
#endif

#define BE2TOUINT16(p) ((uint16_t)(p)[1] + (((uint16_t)(p)[0])<<8))
#define UINT16TOBE2(p, ui) (p)[0]=(uint8_t)((ui)>>8); (p)[1]=(uint8_t)(ui)

#ifdef __cplusplus 
} 
#endif

#endif /* __MESH_H__ */
