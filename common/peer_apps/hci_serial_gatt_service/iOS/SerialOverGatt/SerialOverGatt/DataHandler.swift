/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

import CoreBluetooth

protocol DataHandlerDelegate {
    func creditOutOfBounds()
}

class DataHandler: NSObject {

    let BSG_CREDIT_BIT : UInt8 = 0b00000001
    let BSG_MTU_BIT    : UInt8 = 0b00000010
    let BSG_DATA_BIT   : UInt8 = 0b00000100

    var delegate: DataHandlerDelegate?

    internal func getData(_ isCreditPresent : Bool)  -> [UInt8] {
         var data = [UInt8]()
        if(DataManager.sharedInstance().myCredits > 0) {
            Logger.debug("--- DataHandler#getData --- \(DataManager.sharedInstance().dataToSend.count)" as AnyObject)
            if (DataManager.sharedInstance().dataToSend.count == 0) {
                if(DataManager.sharedInstance().dataQueue.peek() != nil) {
                    DataManager.sharedInstance().dataToSend = DataManager.sharedInstance().dataQueue.peek()!
                    print("data is ----- >")
                    let str = String(bytes: DataManager.sharedInstance().dataToSend, encoding: String.Encoding.utf8)
                    print(str ?? "default")
                }
            }
            let sentLen : Int = DataManager.sharedInstance().sentLen
            var toSend  : Int = 0
            var packet  = DataManager.sharedInstance().dataToSend
            let packetLen : Int = packet.count
            var maxlen : Int = Int (DataManager.sharedInstance().myMTU) - 1 //first flag bit

            //var maxlen : Int = peerMtu > DataManager.sharedInstance().myMaxDataLen ? DataManager.sharedInstance().myMaxDataLen : peerMtu

            if isCreditPresent {
                maxlen = maxlen - 1
            }

            Logger.debug("--- current snapshot ---" as AnyObject)
            Logger.debug("sent length = \(sentLen)" as AnyObject)
            Logger.debug("packet length = \(packetLen)" as AnyObject)
            Logger.debug("max data length = \(maxlen)" as AnyObject)
            Logger.debug("my cerdits = \(DataManager.sharedInstance().myCredits)" as AnyObject)

            if ((packetLen - sentLen) > 0) {
                toSend = packetLen < maxlen ? packetLen : maxlen
                Logger.debug("toSend = \(toSend)" as AnyObject)
                Logger.debug("--------------------" as AnyObject)
                if(DataManager.sharedInstance().myCredits > 0) {
                    let temp :Int = sentLen + (toSend - 1)
                    if (temp > packetLen) {
                        Logger.debug("packet has less value than needed" as AnyObject)
                        data = Array(packet[sentLen...packetLen-1])
                        DataManager.sharedInstance().sentLen = 0
                        DataManager.sharedInstance().dataQueue.dequeue()
                        DataManager.sharedInstance().dataToSend = [UInt8]()
                    } else {
                        Logger.debug("packet has enough values" as AnyObject)
                        data = Array(packet[sentLen...sentLen+toSend-1])
                        DataManager.sharedInstance().sentLen += toSend
                        if(DataManager.sharedInstance().sentLen == packetLen) {
                            Logger.debug("dequeuing" as AnyObject)
                            DataManager.sharedInstance().sentLen = 0
                            DataManager.sharedInstance().dataQueue.dequeue()
                            DataManager.sharedInstance().dataToSend = [UInt8]()
                        }
                    }
                    Logger.debug("data selected for this packet is : \(data)" as AnyObject)
                }
            }
        } else {

            Logger.debug("no credits to send data" as AnyObject)
        }
        return data
    }

    internal func makePacket() -> [UInt8]{

        Logger.debug("--- DataHandler#makePacket --- " as AnyObject)
        var flags : UInt8 = 0
        var packet = [UInt8]()
        var data   = [UInt8]()
        var length : Int = 0
        var isCreditPresent : Bool = false

        //initialize flag to 0
        packet.append(0)

        //assign credit
        if (DataManager.sharedInstance().creditsToAssign > 0) {
            flags = flags | BSG_CREDIT_BIT
            packet.append(DataManager.sharedInstance().creditsToAssign)
            isCreditPresent = true
            DataManager.sharedInstance().creditsToAssign = 0
        }

        if (DataManager.sharedInstance().myMTU > 0) {
            //get data
            data = getData(isCreditPresent)
            length = data.count-1

            //fill data
            if (data.count > 0) {
                flags = flags | BSG_DATA_BIT
                for i in 0...length {
                    packet.append(data[i])
                }
            }
        }
        //update flag bit
        packet[0] = flags

        Logger.debug("flag byte is \(packet[0]) " as AnyObject)
        Logger.debug("final data is \(packet)" as AnyObject)
        if(DataManager.sharedInstance().myCredits > 0) {
            DataManager.sharedInstance().myCredits  = DataManager.sharedInstance().myCredits-1
        }
        return packet
    }

    func hasDataToSend() -> Bool {
        //if (DataManager.sharedInstance().sentLen == DataManager.sharedInstance().dataToSend.count)
        if (DataManager.sharedInstance().dataQueue.peek() == nil){
            return false
        } else {
            return true
        }
    }

    func parseIncomingData(_ data : [UInt8]) -> [UInt8] {
        Logger.debug("--- DataHandler#parseIncomingData ---" as AnyObject)
        var credit  : UInt8
        var mtuSize : UInt16
        var packet  = [UInt8]()
        var index   : Int = 1

        //check credit
        if ((data[0] & BSG_CREDIT_BIT) > 0) {
            credit = data[index]
            index += 1
            Logger.debug("DataHandler#parseIncomingData : bit 0 set credit field is present : \(credit)" as AnyObject)
            DataManager.sharedInstance().myCredits += credit
            if(credit > 255) {
                delegate?.creditOutOfBounds()
            }
        }

        //check MTU
        if ((data[0] & BSG_MTU_BIT) > 0) {
            let MTU : [UInt8] = [data[index],data[index+1]]
            mtuSize = UnsafePointer(MTU).withMemoryRebound(to: UInt16.self, capacity: 1) {
                $0.pointee
            }
            Logger.debug("DataHandler#parseIncomingData : bit 1 set MTU Size field is present: \(mtuSize)" as AnyObject)
            DataManager.sharedInstance().myMTU = mtuSize - 3 //3 bytes for GATT header
            index += 2
        }

        //check data
        if ((data[0] & BSG_DATA_BIT) > 0) {
            Logger.debug("DataHandler#parseIncomingData : bit 2 set Data field is present : \(data.count)" as AnyObject)
            for i in index...data.count-1 {
                packet.append(data[i])
            }

            print("data after parsing is : ")
            let str = String(bytes: packet, encoding: String.Encoding.utf8)
            print(str ?? "data")

            //peer has sent data, assign credits to peer
            DataManager.sharedInstance().creditsToAssign += 1
            DataManager.sharedInstance().incomingQueue.enqueue(str!)

        }

        return packet

    }
}
