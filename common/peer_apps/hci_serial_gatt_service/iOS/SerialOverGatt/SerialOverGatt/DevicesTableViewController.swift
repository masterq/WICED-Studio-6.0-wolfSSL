/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

import UIKit
import CoreBluetooth


protocol DevicesTableViewControllerDelegate {
    func deviceSelectionComplete(_ peripheral : Peripheral)
}

class DevicesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ReadPeripheralProtocol, ConnectPeripheralProtocol{

    var serviceUUIDString:String        = "695293B2-059D-47E0-A63B-7EBEF2FA607E"
    var characteristicUUIDString:String = "614146E4-EF00-42BC-8727-902D3CFE5E8B"

    let BSG_ServiceUUID                 = CBUUID(string: "695293B2-059D-47E0-A63B-7EBEF2FA607E") // Service UUID
    let BSGConfigCharUUID               = CBUUID(string: "614146E4-EF00-42BC-8727-902D3CFE5E8B")  // Characteristic UUIDs

    var peripheralSelected : Peripheral?
    var delegate: DevicesTableViewControllerDelegate?

    @IBOutlet weak var scanButton: UIButton!
    var devicearray = [Peripheral]()
    var discoveredPeripherals : Dictionary<CBPeripheral, Peripheral> = [:]
    var connectionTimer = Timer()

    @IBAction func stopScanning(_ sender: AnyObject) {

        if(scanButton.currentTitle == "STOP SCANNING") {
            stopScanning()
            scanButton.setTitle("START SCANNING", for: UIControlState())
        } else {
            startScanning()
            scanButton.setTitle("STOP SCANNING", for: UIControlState())

        }


    }

    @IBOutlet weak var list: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()


        if CentralManager.sharedInstance().connectPeripheralDelegate == nil {
            initBluetooth()

        }
        else {
            bluetoothBecomeAvailable()
        }
        self.list.dataSource = self
        self.list.delegate = self
        CentralManager.sharedInstance().connectPeripheralDelegate = self
        DataManager.sharedInstance().discoveredPeripherals = discoveredPeripherals;
    }

    func initBluetooth() {

        CentralManager.sharedInstance().connectPeripheralDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devicearray.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "devices", for: indexPath)

        cell.textLabel?.text = devicearray[indexPath.row].name
        cell.textLabel?.font =  UIFont(name :"Avenir Book", size: 16)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        CentralManager.sharedInstance().connectPeripheralDelegate = self
        peripheralSelected = devicearray[indexPath.row]
        stopScanning()
        connect()

    }

    func connect() {
        print("DevicesTableViewController#connect ")
        CentralManager.sharedInstance().connectPeripheral(peripheralSelected!)
        connectionTimer = Timer.scheduledTimer(timeInterval: 2, target:self, selector: #selector(DevicesTableViewController.timeout), userInfo: nil, repeats: false)

    }

    func disconnect() {
        print("DevicesTableViewController#disconnect ")
        CentralManager.sharedInstance().cancelPeripheralConnection(peripheralSelected!,userClickedCancel: true)

    }


    func didDiscoverCharacteristicsofPeripheral(_ cbservice : CBService!) {
        print("DevicesTableViewController#didDiscoverCharacteristicsofPeripheral \(cbservice)")

        var characteristicFound : Bool = false

        for charateristic in cbservice.characteristics! {
            let thisCharacteristic = charateristic as CBCharacteristic
            print("UUID == \(thisCharacteristic.uuid)")
            switch thisCharacteristic.uuid {
            case BSGConfigCharUUID:
                print("BSGConfigCharUUID")
                DataManager.sharedInstance().bsggattCharacteristic = thisCharacteristic
                characteristicFound = true
                sleep(2)
                break

            default:
                _ = 0

            }

        }

        if(characteristicFound == false) {
            //show pop up
            DispatchQueue.main.async(execute: {
                self.showCharacteristicNotFoundPopUp()
            })
            disconnect()
        } else {
            notifyCallingViewController()
        }

    }

    func timeout() {
        print("DevicesTableViewController#timeout ")
        disconnect()
        connectionTimer.invalidate()
        showServiceNotFoundPopUp()
    }

    func didDiscoverServices(_ peripheral:CBPeripheral, error:NSError?) {
        print("DevicesTableViewController#didDiscoverServices \(String(describing: peripheral.services))")
        connectionTimer.invalidate()
        var serviceFound : Bool = false
        if (error == nil) {
            DataManager.sharedInstance().numberOfServices = (peripheral.services?.count)!

            for service:CBService in peripheral.services as [CBService]! {
                if(service.uuid == BSG_ServiceUUID) {
                    serviceFound = true
                    peripheral.discoverCharacteristics(nil, for: service)
                    break
                }

            }

            if(serviceFound == false) {
                //pop up showing no services
                DispatchQueue.main.async(execute: {
                    self.showServiceNotFoundPopUp()
                })

                disconnect()
            }

        } else {
            DispatchQueue.main.async(execute: {
                self.showServiceNotFoundPopUp()
            })

        }
    }

    func showServiceNotFoundPopUp ( ) {
        let refreshAlert = UIAlertController(title: "No Service Found!", message: "Please retry with other device", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            //print("Handle Ok logic here")
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)

    }

    func showCharacteristicNotFoundPopUp ( ) {
        let refreshAlert = UIAlertController(title: "No Characteristic Found!", message: "Please retry with other device", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            //print("Handle Ok logic here")
        }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //print("Handle Cancel Logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }


    func didWriteValueForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic: CBCharacteristic!, error: NSError!) {

    }


    func didUpdateValueForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic: CBCharacteristic!, error: NSError!) {
        print("DevicesTableViewController#didUpdateValueForCharacteristic  characteristic.UUID = \(characteristic.uuid)  value = \(String(describing: characteristic.value))")

    }

    func didUpdateNotificationStateForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic:CBCharacteristic!, error:NSError!) {
        print("DevicesTableViewController#didUpdateNotificationStateForCharacteristic  characteristic.UUID = \(characteristic.uuid)  value = \(String(describing: characteristic.value))")
    }

    func didConnectPeripheral(_ cbPeripheral: CBPeripheral!) {
        Logger.debug("DevicesTableViewController#didConnectPeripheral \(String(describing: cbPeripheral.name))" as AnyObject)

        if let peripheral = DataManager.sharedInstance().discoveredPeripherals[cbPeripheral] {
            peripheral.discoverServices(nil, delegate: self)
        }
    }

    func didDisconnectPeripheral(_ cbPeripheral: CBPeripheral!, error: NSError!, userClickedCancel: Bool) {
        Logger.debug("DevicesTableViewController#didDisconnectPeripheral \(String(describing: cbPeripheral.name))" as AnyObject)
    }

    func didRestorePeripheral(_ peripheral: Peripheral) {
        Logger.debug("DevicesTableViewController#didRestorePeripheral \(peripheral.name)" as AnyObject)
    }

    func bluetoothBecomeAvailable() {
        print("DevicesTableViewController#bluetoothBecomeAvailable")
        self.startScanning()
    }

    func bluetoothBecomeUnavailable() {
        print("DevicesTableViewController#bluetoothBecomeUnavailable")
        self.stopScanning()
        DispatchQueue.main.async(execute: {
            self.bluetoothTurnedOffpopUp()
        })
    }

    func notifyCallingViewController() {
        print("DevicesTableViewController#notifyCallingViewController scanComplete")
        self.delegate?.deviceSelectionComplete(peripheralSelected!)
    }

    func startScanning() {
        print("DevicesTableViewController#startScanning")

        DataManager.sharedInstance().discoveredPeripherals = [:]
        devicearray = [Peripheral]()

        for peripheral:Peripheral in DataManager.sharedInstance().discoveredPeripherals.values {
            peripheral.isNearby = false
        }
        CentralManager.sharedInstance().startScanning(afterPeripheralDiscovered, allowDuplicatesKey: false)
    }

    func stopScanning() {
        print("DevicesTableViewController#stopScanning")
        CentralManager.sharedInstance().stopScanning()
    }

    func afterPeripheralDiscovered(_ cbPeripheral:CBPeripheral, advertisementData:NSDictionary, RSSI:NSNumber) {

        Logger.debug("DevicesTableViewController#afterPeripheralDiscovered: \(cbPeripheral)" as AnyObject)

        var peripheral : Peripheral

        if let p = DataManager.sharedInstance().discoveredPeripherals[cbPeripheral] {

            peripheral = p

        }
        else {

            peripheral = Peripheral(cbPeripheral:cbPeripheral, advertisements:advertisementData as Dictionary<NSObject, AnyObject>, rssi:RSSI.intValue)
            DataManager.sharedInstance().discoveredPeripherals[peripheral.cbPeripheral] = peripheral
            devicearray.append(peripheral)

            DispatchQueue.main.async(execute: {

                if CentralManager.sharedInstance().isScanning == true {
                    self.list.reloadData()
                }

            })

        }

        NotificationCenter.default.post(name: Notification.Name(rawValue: "afterPeripheralDiscovered"), object: nil)
    }

    func bluetoothTurnedOffpopUp ( ) {
        let refreshAlert = UIAlertController(title: "Bluetooth turned off!", message: "Please turn on Bluetooth", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            //print("Handle Ok logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)

    }

}
