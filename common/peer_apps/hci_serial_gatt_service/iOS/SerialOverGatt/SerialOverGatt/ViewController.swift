/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

import UIKit

class ViewController: UIViewController , DevicesTableViewControllerDelegate, BSGViewControllerDelegate{

    @IBOutlet weak var viewcontainer: UIView!
    weak var currentViewController: UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()
        let tempController: DevicesTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "ComponentE") as! DevicesTableViewController
        tempController.delegate = self
        self.currentViewController = tempController
        self.addChildViewController(self.currentViewController!)
        self.currentViewController?.view.frame = CGRect(x: 0,y: 0,width: viewcontainer.frame.size.width, height: viewcontainer.frame.size.height)
        self.addSubview(self.currentViewController!.view, toView: self.viewcontainer)
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func scanningComplete(_ data: Bool) {

        print("scanningComplete")
        showComponent(1)

    }

    func addSubview(_ subView:UIView, toView parentView:UIView) {
        parentView.addSubview(subView)

        var viewBindingsDict = [String: AnyObject]()
        viewBindingsDict["subView"] = subView
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
        parentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[subView]|",
            options: [], metrics: nil, views: viewBindingsDict))
    }

    func showComponent(_ value: NSInteger) {

         if  value == 1 {
            DispatchQueue.main.async(execute: {
                let newViewController:DevicesTableViewController = self.storyboard?.instantiateViewController(withIdentifier: "ComponentE") as! DevicesTableViewController
                self.cycleFromViewController(self.currentViewController!, toViewController: newViewController)
                newViewController.delegate = self;
                self.currentViewController = newViewController
            })

        } else if  value == 2 {
             DispatchQueue.main.async(execute: {
            let newViewController:BSGViewController = self.storyboard?.instantiateViewController(withIdentifier: "ComponentD") as! BSGViewController
            self.cycleFromViewController(self.currentViewController!, toViewController: newViewController)
            newViewController.delegate = self;
            self.currentViewController = newViewController
            })
        }
    }

    func cycleFromViewController(_ oldViewController: UIViewController, toViewController newViewController: UIViewController) {
        oldViewController.willMove(toParentViewController: nil)
        self.addChildViewController(newViewController)
        newViewController.view.frame = CGRect(x: 0,y: 0,width: viewcontainer.frame.size.width, height: viewcontainer.frame.size.height)
        self.addSubview(newViewController.view, toView:self.viewcontainer!)
        newViewController.view.alpha = 0
        newViewController.view.layoutIfNeeded()

        UIView.animate(withDuration: 2, animations: {
            newViewController.view.alpha = 1
            oldViewController.view.alpha = 0
            },
            completion: { finished in
                oldViewController.view.removeFromSuperview()
                oldViewController.removeFromParentViewController()
                newViewController.didMove(toParentViewController: self)
        })
    }

    func deviceSelectionComplete(_ peripheral : Peripheral) {
        Logger.debug("ViewController#deviceSelectionComplete" as AnyObject)
        DataManager.sharedInstance().peripheralSelected = peripheral
        showComponent(2)
    }

    func back() {
        Logger.debug("back" as AnyObject)
        showComponent(1)
    }

}

