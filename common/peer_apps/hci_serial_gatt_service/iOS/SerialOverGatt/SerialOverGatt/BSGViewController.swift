/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

import UIKit
import CoreBluetooth

protocol BSGViewControllerDelegate {
    func back()
}

class BSGViewController: UIViewController , ReadPeripheralProtocol, ConnectPeripheralProtocol, DataHandlerDelegate {

    var serviceUUIDString:String        = "695293B2-059D-47E0-A63B-7EBEF2FA607E"
    var characteristicUUIDString:String = "614146E4-EF00-42BC-8727-902D3CFE5E8B"

    var bsgCharacteristic : CBCharacteristic =  DataManager.sharedInstance().bsggattCharacteristic
    var delegate: BSGViewControllerDelegate?
    var incomingbytes : Int = 0
    var dataHandler : DataHandler = DataHandler()


    @IBOutlet weak var devicename: UILabel!


    @IBOutlet weak var messageTextBox: UITextField!

    @IBAction func BtnSend(_ sender: AnyObject) {
        if( messageTextBox.hasText) {

            let data: [UInt8] = Array(messageTextBox.text!.utf8)
            DataManager.sharedInstance().dataQueue.enqueue(data)

            print("my data is =\(DataManager.sharedInstance().dataQueue.peek()!)  count = \(DataManager.sharedInstance().dataQueue)")

            DataManager.sharedInstance().dataToSend = DataManager.sharedInstance().dataQueue.peek()!
            while (dataHandler.hasDataToSend() == true && DataManager.sharedInstance().myCredits > 0) {
                let pkt : [UInt8] = dataHandler.makePacket()
                sendPacket(pkt)
            }
            messageTextBox.text = ""

        }
    }

    @IBAction func BtnSendMB(_ sender: AnyObject) {
        var bytes = [UInt8]()
        var x : UInt8 = 0
        for _ in 1...1048576 {
            bytes.append(x)
            x = (x+1)%255;
        }

        DataManager.sharedInstance().dataQueue.enqueue(bytes)
        DataManager.sharedInstance().dataToSend = DataManager.sharedInstance().dataQueue.peek()!

        while (dataHandler.hasDataToSend() == true && DataManager.sharedInstance().myCredits > 0) {
            let pkt : [UInt8] = dataHandler.makePacket()
            sendPacket(pkt)
        }

    }

    @IBOutlet weak var bytesText: UILabel!

    @IBAction func BtnBack(_ sender: AnyObject) {
        //disable notification
        bsgCharacteristic.service.peripheral.setNotifyValue(false, for: bsgCharacteristic)
        disconnect()
    }


    @IBAction func BtnExit(_ sender: AnyObject) {
        bsgCharacteristic.service.peripheral.setNotifyValue(false, for: bsgCharacteristic)
        disconnect()
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
    }

    func dissmissKeyboard() {
        view.endEditing(true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        DataManager.sharedInstance().dataToSend = [UInt8]()
        DataManager.sharedInstance().creditsToAssign = DataManager.sharedInstance().MAX_CREDITS
        DataManager.sharedInstance().myCredits = 0
        DataManager.sharedInstance().myMTU = 0
        DataManager.sharedInstance().sentLen = 0
        DataManager.sharedInstance().peripheralSelected.setDelegate(self)

        bsgCharacteristic.service.peripheral.setNotifyValue(true, for: bsgCharacteristic)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BSGViewController.dissmissKeyboard));
        view.addGestureRecognizer(tap)

        CentralManager.sharedInstance().connectPeripheralDelegate = self
        devicename.text = DataManager.sharedInstance().peripheralSelected.name
        dataHandler.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        disconnect()
        // Dispose of any resources that can be recreated.
    }


    func didDiscoverCharacteristicsofPeripheral(_ cbservice : CBService!) {
        print("didDiscoverCharacteristicsofPeripheral")

    }

    func didDiscoverServices(_ peripheral:CBPeripheral, error:NSError?) {
        print("didDiscoverServices ")

    }

    func didWriteValueForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic: CBCharacteristic!, error: NSError!) {
         print("didWriteValueForCharacteristic \(error)")
        if(dataHandler.hasDataToSend() && DataManager.sharedInstance().myCredits > 0) {
            let pkt : [UInt8] = dataHandler.makePacket()
            sendPacket(pkt)
        } else if(DataManager.sharedInstance().creditsToAssign > 0){
            let pkt : [UInt8] = dataHandler.makePacket()
            sendPacket(pkt)
        }
    }


    func didUpdateValueForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic: CBCharacteristic!, error: NSError!) {
        print("didUpdateValueForCharacteristic  characteristic.UUID = \(characteristic.uuid)  value = \(characteristic.value)")
        if characteristic.uuid == BSGConfigCharUUID {
            print(characteristic.value)
            let dataBytes = characteristic.value
            let dataArray = Array(UnsafeBufferPointer(start: (dataBytes! as NSData).bytes.bindMemory(to: UInt8.self, capacity: dataBytes!.count), count: dataBytes!.count))

            print("didUpdateValueForCharacteristic   value = \(dataArray)")
            incomingbytes = incomingbytes + dataArray.count
            print(" count = \(incomingbytes)")

            //displayIncomingPacket()
            DispatchQueue.main.async(execute: {
                self.bytesText.text = "\(self.incomingbytes)"
            })

            let packet = dataHandler.parseIncomingData(dataArray)
            print("value =  \(packet )")

                if(dataHandler.hasDataToSend() == true) {
                    while (dataHandler.hasDataToSend() == true && DataManager.sharedInstance().myCredits > 0) {
                        let pkt : [UInt8] = dataHandler.makePacket()
                        sendPacket(pkt)
                    }
                } else if(DataManager.sharedInstance().creditsToAssign > 0){
                    let pkt : [UInt8] = dataHandler.makePacket()
                    sendPacket(pkt)
                }
        }
    }

    func didUpdateNotificationStateForCharacteristic(_ cbPeripheral: CBPeripheral!, characteristic:CBCharacteristic!, error:NSError!) {
        print("didUpdateNotificationStateForCharacteristic  characteristic.UUID = \(characteristic.uuid)  value = \(characteristic.value)")


        if characteristic.uuid == BSGConfigCharUUID {
            print(characteristic.value)

            if(characteristic.value != nil) {

                let dataBytes = characteristic.value
                let dataArray = Array(UnsafeBufferPointer(start: (dataBytes! as NSData).bytes.bindMemory(to: UInt8.self, capacity: dataBytes!.count), count: dataBytes!.count))

                print("didUpdateNotificationStateForCharacteristic   value = \(dataArray)")
                let packet = dataHandler.parseIncomingData(dataArray)

                incomingbytes = incomingbytes + dataArray.count
                print("value =  \(packet )  ")
                DispatchQueue.main.async(execute: {
                    self.bytesText.text = "\(self.incomingbytes)"
                })

            }

            let pkt : [UInt8] = dataHandler.makePacket()
            print("sending packet after enable notification \(pkt)")
            sendPacket(pkt)
        }

    }


    func notifyCallingViewController() {
        delegate?.back()
    }
    func sendPacket(_ packet : [UInt8]) {
        let data : Data = Data(bytes: UnsafePointer<UInt8>(packet), count: packet.count )
        print("data = \(data)")
        bsgCharacteristic.service.peripheral.writeValue(data, for: bsgCharacteristic, type: CBCharacteristicWriteType.withoutResponse)
    }

    func didConnectPeripheral(_ cbPeripheral: CBPeripheral!) {
        Logger.debug("AppDelegate#didConnectPeripheral \(cbPeripheral.name)" as AnyObject)

    }

    func didDisconnectPeripheral(_ cbPeripheral: CBPeripheral!, error: NSError!, userClickedCancel: Bool) {
        Logger.debug("AppDelegate#didDisconnectPeripheral \(cbPeripheral.name)" as AnyObject)
        DataManager.sharedInstance().dataToSend = [UInt8]()
        DataManager.sharedInstance().creditsToAssign = DataManager.sharedInstance().MAX_CREDITS
        DataManager.sharedInstance().myCredits = 0
        DataManager.sharedInstance().myMTU = 0
        DataManager.sharedInstance().peripheralSelected = nil
        DataManager.sharedInstance().sentLen = 0

        showdevicedisconnectedpopup()

        while(DataManager.sharedInstance().dataQueue.peek() != nil) {
            DataManager.sharedInstance().dataQueue.dequeue()
        }

        while(DataManager.sharedInstance().incomingQueue.peek() != nil) {
            DataManager.sharedInstance().incomingQueue.dequeue()
        }

        notifyCallingViewController()
    }
    func didRestorePeripheral(_ peripheral: Peripheral) {
        Logger.debug("AppDelegate#didRestorePeripheral \(peripheral.name)" as AnyObject)
    }

    func disconnect() {
        print("disconnect ")
        CentralManager.sharedInstance().cancelPeripheralConnection(DataManager.sharedInstance().peripheralSelected!,userClickedCancel: true)

    }
    func bluetoothBecomeAvailable() {
        print("calling start scan -- not implemented here")
    }

    func bluetoothBecomeUnavailable() {
        print("calling stop scan -- not implemented here")
        DispatchQueue.main.async(execute: {
            self.bluetoothTurnedOffpopUp()
        })
    }

    func creditOutOfBounds() {
        print("creditOutOfBounds")
        disconnect()
        notifyCallingViewController()
    }

    func showdevicedisconnectedpopup ( ) {
        let refreshAlert = UIAlertController(title: "Device Disconnected!", message: "Please retry with other device", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            //print("Handle Ok logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }

    func bluetoothTurnedOffpopUp ( ) {
        let refreshAlert = UIAlertController(title: "Bluetooth turned off!", message: "Please turn on Bluetooth", preferredStyle: UIAlertControllerStyle.alert)

        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            //print("Handle Ok logic here")
        }))

        present(refreshAlert, animated: true, completion: nil)

    }


}
