/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */
protocol QueueType {
    associatedtype Element
    mutating func enqueue(_ element: Element)
    mutating func dequeue() -> Element?
    func peek() -> Element?
}

final class Storage<Element> {

    fileprivate var pointer: UnsafeMutablePointer<Element>
    fileprivate let capacity: Int

    init(capacity: Int) {
        pointer = UnsafeMutablePointer<Element>.allocate(capacity: capacity)
        self.capacity = capacity
    }

    static func copy(_ storage: Storage) -> Storage<Element> {
        let storageNew = Storage<Element>(capacity: storage.capacity)
        storageNew.pointer.initialize(from: storage.pointer, count: storage.capacity)
        return storageNew
    }

    func add(_ element: Element, at index: Int) {
        (pointer + index).initialize(to: element)
    }

    func removeAt(_ index: Int) {
        (pointer + index).deinitialize()
    }

    func itemAt(_ index: Int) -> Element {
        return (pointer + index).pointee
    }

    deinit {
        pointer.deinitialize(count: capacity)
        pointer.deallocate(capacity: capacity)
    }

}

struct Queue<Element> : QueueType {

    fileprivate var storage: Storage<Element>
    fileprivate var rear: Int = 0
    fileprivate var front: Int = 0
    fileprivate var count: Int = 0
    fileprivate let capacity: Int

    init(capacity: Int) {
        self.capacity = capacity
        storage = Storage<Element>(capacity: capacity)
    }

    fileprivate mutating func makeUnique() {
        if !isKnownUniquelyReferenced(&storage) {
            storage = Storage.copy(storage)
        }
    }

    mutating func enqueue(_ element: Element) {
        guard count < capacity else {
            print("Queue is full.")
            return
        }
        makeUnique()
        storage.add(element, at: rear)
        rear = (rear + 1) % capacity
        count = count + 1
    }

    mutating func dequeue() -> Element? {
        guard count > 0 else {
            print("Queue is empty.")
            return nil
        }

        makeUnique()
        let item = storage.itemAt(front)
        storage.removeAt(front)
        front = (front + 1) % capacity
        count = count - 1
        return item
    }

    func peek() -> Element? {
        guard count > 0 else {
            print("Queue is empty.")
            return nil
        }
        return storage.itemAt(front)
    }

}

extension Queue : CustomStringConvertible {
    internal var description: String {
        guard count > 0 else {
            return "[]"
        }
        var desc = "["
        for idx in front..<rear {
            desc = desc + "\(storage.itemAt(idx)), "
        }
        desc.remove(at: desc.characters.index(before: desc.endIndex))
        desc.remove(at: desc.characters.index(before: desc.endIndex))
        desc = desc + "]"
        return desc
    }
}


struct QueueGenerator<Element> : IteratorProtocol {
    var queue: Queue<Element>
    mutating func next() -> Element? {
        return queue.dequeue()
    }
}

extension Queue: Sequence {
    func makeIterator() -> QueueGenerator<Element> {
        return QueueGenerator<Element>(queue: self)
    }
}
