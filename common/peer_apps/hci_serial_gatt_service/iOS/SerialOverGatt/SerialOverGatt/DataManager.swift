/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */
import CoreBluetooth


var thisDataManager : DataManager?

class DataManager {

    var serviceUUIDString:String        = "695293B2-059D-47E0-A63B-7EBEF2FA607E"
    var characteristicUUIDString:String = "614146E4-EF00-42BC-8727-902D3CFE5E8B"

    var discoveredPeripherals : Dictionary<CBPeripheral, Peripheral> = [:]
    var peripheralInProximity : Bool = false

    var creditsToAssign: UInt8 = 0
    var myCredits: UInt8 = 0
    var creditOutOfBounds : Bool = false


    var myMTU            : UInt16 = 0
    var myMaxDataLen     : Int = 0 // first byte is a flag

    var dataToSend       : [UInt8] = []
    var sentLen          : Int = 0
    var currDataLen      :UInt16 = 0
    var fullDataLen      : UInt16 = 0
    var MAX_CREDITS      : UInt8 = 6
    var numberOfServices : Int = 0

    var dataQueue = Queue<[UInt8]>(capacity: 255)
    var incomingQueue = Queue<String>(capacity: 255)

    var bsggattCharacteristic : CBCharacteristic!
    var peripheralSelected : Peripheral!

    // MARK: Singleton
    internal class func sharedInstance() -> DataManager {
        if thisDataManager == nil {
            thisDataManager = DataManager()
        }
        return thisDataManager!
    }

}
