var modules =
[
    [ "Bluetooth", "group__wicedbt.html", "group__wicedbt" ],
    [ "WICED trace utils", "group__wiced__utils.html", "group__wiced__utils" ],
    [ "WICED Firmware Upgrade", "group__wiced___firmware__upgrade.html", "group__wiced___firmware__upgrade" ],
    [ "Hardware Drivers", "group___hardware_drivers.html", "group___hardware_drivers" ],
    [ "WICED result", "group___result.html", "group___result" ],
    [ "Crypto functions", "group__crypto.html", "group__crypto" ]
];