var group__wiced__bt__ble__hidh__api__functions =
[
    [ "wiced_bt_ble_hidh_connected_t", "structwiced__bt__ble__hidh__connected__t.html", [
      [ "bdaddr", "structwiced__bt__ble__hidh__connected__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "handle", "structwiced__bt__ble__hidh__connected__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__ble__hidh__connected__t.html#ac0ba4465a1ce109ecc670bf74a7f68df", null ]
    ] ],
    [ "wiced_bt_ble_hidh_disconnected_t", "structwiced__bt__ble__hidh__disconnected__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__disconnected__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "reason", "structwiced__bt__ble__hidh__disconnected__t.html#a53a9e6e1b68d477c173d07e156ca4f7b", null ]
    ] ],
    [ "wiced_bt_ble_hidh_descriptor_t", "structwiced__bt__ble__hidh__descriptor__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__descriptor__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__ble__hidh__descriptor__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_descriptor", "structwiced__bt__ble__hidh__descriptor__t.html#a9b260d0b772b173d041c32f430cd0c82", null ],
      [ "status", "structwiced__bt__ble__hidh__descriptor__t.html#ac0ba4465a1ce109ecc670bf74a7f68df", null ]
    ] ],
    [ "wiced_bt_ble_hidh_report_t", "structwiced__bt__ble__hidh__report__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__ble__hidh__report__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_data", "structwiced__bt__ble__hidh__report__t.html#a8304c4af5da6e830b86d7199dc9a22e6", null ],
      [ "report_id", "structwiced__bt__ble__hidh__report__t.html#a725a13d8d43f65c0f5e8adc271f71573", null ]
    ] ],
    [ "wiced_bt_ble_hidh_gatt_char_t", "structwiced__bt__ble__hidh__gatt__char__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__gatt__char__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "properties", "structwiced__bt__ble__hidh__gatt__char__t.html#ad55938872fd47086c38b264f22b0eb65", null ],
      [ "uuid16", "structwiced__bt__ble__hidh__gatt__char__t.html#ae4ba2923b4d364d3472acf5ee2a7752c", null ],
      [ "val_handle", "structwiced__bt__ble__hidh__gatt__char__t.html#a13d14e316098f34a69b6e4f3c29b9b3c", null ]
    ] ],
    [ "wiced_bt_ble_hidh_gatt_report_t", "structwiced__bt__ble__hidh__gatt__report__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__gatt__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "rpt_id", "structwiced__bt__ble__hidh__gatt__report__t.html#a70d5de92826151be54874adc90f04166", null ],
      [ "rpt_type", "structwiced__bt__ble__hidh__gatt__report__t.html#a2fea089c29d03fa43ca04a8f1fdffcce", null ],
      [ "val_handle", "structwiced__bt__ble__hidh__gatt__report__t.html#a13d14e316098f34a69b6e4f3c29b9b3c", null ]
    ] ],
    [ "wiced_bt_ble_hidh_gatt_cache_t", "structwiced__bt__ble__hidh__gatt__cache__t.html", [
      [ "bdaddr", "structwiced__bt__ble__hidh__gatt__cache__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "characteristics", "structwiced__bt__ble__hidh__gatt__cache__t.html#a0ea414bee683d9597297d91b2734d842", null ],
      [ "characteristics_nb", "structwiced__bt__ble__hidh__gatt__cache__t.html#acc331d0484e7f918482880226215f29b", null ],
      [ "report_descs", "structwiced__bt__ble__hidh__gatt__cache__t.html#a10e08f20773be7b647aff239a185c837", null ],
      [ "report_descs_nb", "structwiced__bt__ble__hidh__gatt__cache__t.html#a12811ad8206103b2cd6afd189629c130", null ]
    ] ],
    [ "wiced_bt_ble_hidh_set_report_t", "structwiced__bt__ble__hidh__set__report__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__set__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__ble__hidh__set__report__t.html#ac0ba4465a1ce109ecc670bf74a7f68df", null ]
    ] ],
    [ "wiced_bt_ble_hidh_get_report_t", "structwiced__bt__ble__hidh__get__report__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__get__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__ble__hidh__get__report__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_data", "structwiced__bt__ble__hidh__get__report__t.html#a8304c4af5da6e830b86d7199dc9a22e6", null ],
      [ "status", "structwiced__bt__ble__hidh__get__report__t.html#ac0ba4465a1ce109ecc670bf74a7f68df", null ]
    ] ],
    [ "wiced_bt_ble_hidh_set_protocol_t", "structwiced__bt__ble__hidh__set__protocol__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__set__protocol__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__ble__hidh__set__protocol__t.html#ac0ba4465a1ce109ecc670bf74a7f68df", null ]
    ] ],
    [ "wiced_bt_ble_hidh_virtual_unplug_t", "structwiced__bt__ble__hidh__virtual__unplug__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__virtual__unplug__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ]
    ] ],
    [ "wiced_bt_ble_hidh_event_data_t", "unionwiced__bt__ble__hidh__event__data__t.html", [
      [ "connected", "unionwiced__bt__ble__hidh__event__data__t.html#a8c435ce0404161ebd27b219f19af14ae", null ],
      [ "descriptor", "unionwiced__bt__ble__hidh__event__data__t.html#aa82e18a1dc7d6db0077d59b4d876ecd5", null ],
      [ "disconnected", "unionwiced__bt__ble__hidh__event__data__t.html#ab7ebd7edabec741e7ea09ae8b56edaa7", null ],
      [ "gatt_cache", "unionwiced__bt__ble__hidh__event__data__t.html#acd13899a4ab4217cce99e44992650b96", null ],
      [ "get_report", "unionwiced__bt__ble__hidh__event__data__t.html#ab3a511aa16390e7c086696d35fef4372", null ],
      [ "report", "unionwiced__bt__ble__hidh__event__data__t.html#a59ab4f448fbe97da7981bf89a1c3eded", null ],
      [ "set_protocol", "unionwiced__bt__ble__hidh__event__data__t.html#a6827a1ba3547538801b03a40a8ae5f5c", null ],
      [ "set_report", "unionwiced__bt__ble__hidh__event__data__t.html#a023cdcb30629cdd6f640c85b05a50f4a", null ],
      [ "virtual_unplug", "unionwiced__bt__ble__hidh__event__data__t.html#aa8640711b0fb6ceed9f48b02d2e4200f", null ]
    ] ],
    [ "WICED_BT_BLE_HIDH_DEV_CHAR_MAX", "group__wiced__bt__ble__hidh__api__functions.html#gafaeb8d61bc8d24c60e057831cf490b38", null ],
    [ "WICED_BT_BLE_HIDH_DEV_MAX", "group__wiced__bt__ble__hidh__api__functions.html#gac8b4c4bf65fd12a983feb22212090fb5", null ],
    [ "WICED_BT_BLE_HIDH_EVENT_FILTER_NB_MAX", "group__wiced__bt__ble__hidh__api__functions.html#ga1eba0d180daaec29ab925ae54bbde91e", null ],
    [ "WICED_BT_BLE_HIDH_HANDLE_OFFSET", "group__wiced__bt__ble__hidh__api__functions.html#gad11b8fbeb8795cb2a5f96821bd5fb75a", null ],
    [ "WICED_BT_BLE_HIDH_REPORT_DESC_MAX", "group__wiced__bt__ble__hidh__api__functions.html#gaa420d29c830a462dbbe612cef73f6969", null ],
    [ "WICED_BT_BLE_HIDH_WAKEUP_PATTERN_LEN_MAX", "group__wiced__bt__ble__hidh__api__functions.html#ga7263d244b42630a308aad0db219b5706", null ],
    [ "WICED_BT_BLE_HIDH_WAKEUP_PATTERN_NB_MAX", "group__wiced__bt__ble__hidh__api__functions.html#gaf2ee3a7a8c53dc2c7bf20c5c39cdd34b", null ],
    [ "wiced_bt_ble_hidh_cback_t", "group__wiced__bt__ble__hidh__api__functions.html#ga28438ef601e35f082c7d6111684b8af8", null ],
    [ "wiced_bt_ble_hidh_filter_cback_t", "group__wiced__bt__ble__hidh__api__functions.html#ga945cfe253e5b4edffa3514bb5f864a4c", null ],
    [ "wiced_bt_ble_hidh_event_t", "group__wiced__bt__ble__hidh__api__functions.html#ga597edb0b00c91c8f74fa3f67eb258a7a", [
      [ "WICED_BT_BLE_HIDH_OPEN_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aae54d43e3c03e1be04f409fd696264ad7", null ],
      [ "WICED_BT_BLE_HIDH_CLOSE_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa2cd5f75f454eb377f33d8cf3931495b8", null ],
      [ "WICED_BT_BLE_HIDH_DESCRIPTOR_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa89ef0782b35663d303369ebc842f38fc", null ],
      [ "WICED_BT_BLE_HIDH_GATT_CACHE_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa477ebd4b85306be600224a812d62629d", null ],
      [ "WICED_BT_BLE_HIDH_REPORT_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa5b18f0ff45529027e15fc426fe567139", null ],
      [ "WICED_BT_BLE_HIDH_SET_REPORT_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa745d396b8a6ef4010fc5cfad5142ba34", null ],
      [ "WICED_BT_BLE_HIDH_GET_REPORT_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa9b66f48a94744ec3f7344d5f46878a81", null ],
      [ "WICED_BT_BLE_HIDH_VIRTUAL_UNPLUG_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa1351d97263061a0b7c18df745d5484b1", null ],
      [ "WICED_BT_BLE_HIDH_SET_PROTOCOL_EVT", "group__wiced__bt__ble__hidh__api__functions.html#gga597edb0b00c91c8f74fa3f67eb258a7aa7e21255443299a3d7dec128bbc969f5d", null ]
    ] ],
    [ "wiced_bt_ble_hidh_protocol_t", "group__wiced__bt__ble__hidh__api__functions.html#ga37cfda10abf5ecfbaaa238fd932b66c4", [
      [ "WICED_BT_BLE_HIDH_PROTOCOL_REPORT", "group__wiced__bt__ble__hidh__api__functions.html#gga37cfda10abf5ecfbaaa238fd932b66c4a5fec992b629de6aa9b0f73f2ee8853f0", null ],
      [ "WICED_BT_BLE_HIDH_PROTOCOL_BOOT", "group__wiced__bt__ble__hidh__api__functions.html#gga37cfda10abf5ecfbaaa238fd932b66c4a2bcaef4849496cd0615872957690d4f2", null ]
    ] ],
    [ "wiced_bt_ble_hidh_report_type_t", "group__wiced__bt__ble__hidh__api__functions.html#gaf002849724a26c7b7f532afc61b416d0", null ],
    [ "wiced_bt_ble_hidh_status_t", "group__wiced__bt__ble__hidh__api__functions.html#gaea816aab6e1b352ec4c6e1abdd220973", [
      [ "WICED_BT_BLE_HIDH_STATUS_SUCCESS", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a080b46a5bbe495977d52a350c641b706", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_ERROR", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a62f33f3e509483b91d7f55b7c91fec44", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_GATT_ERROR", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973af91ffbfbe9cb0a1037b782b048185810", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_INVALID_PARAM", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a10068b60003d688dfd87139d6dbe7dc5", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_MEM_FULL", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a1b99a391fe470023a52fa367a1e84411", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_CONNECTION_FAILED", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a61527d3199b78b8331551eecc95da436", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_UNSUPPORTED", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a60c8e9ffdb649b11145b36912cee74fc", null ],
      [ "WICED_BT_BLE_HIDH_STATUS_NOT_YET_IMPLEMENTED", "group__wiced__bt__ble__hidh__api__functions.html#ggaea816aab6e1b352ec4c6e1abdd220973a26d502f207d3e155a062a6fd8d96996f", null ]
    ] ],
    [ "wiced_bt_ble_hidh_wakeup_pattern_cmd_t", "group__wiced__bt__ble__hidh__api__functions.html#ga75e962ea62cfc8ff581e53cd05fff375", null ],
    [ "wiced_bt_ble_hidh_add", "group__wiced__bt__ble__hidh__api__functions.html#ga74ec745681aa6e0b7da6f3ab137c2807", null ],
    [ "wiced_bt_ble_hidh_connect", "group__wiced__bt__ble__hidh__api__functions.html#gae9fc8e9705c84bc5c2f66efce852269a", null ],
    [ "wiced_bt_ble_hidh_disconnect", "group__wiced__bt__ble__hidh__api__functions.html#gabd928d7cb7b8b1be1cbc442dbd1d3d7e", null ],
    [ "wiced_bt_ble_hidh_down", "group__wiced__bt__ble__hidh__api__functions.html#ga4d6861974295b0907ea324251125726d", null ],
    [ "wiced_bt_ble_hidh_encryption_changed", "group__wiced__bt__ble__hidh__api__functions.html#ga3d5a564c492a413d4d0bb7ebbe727377", null ],
    [ "wiced_bt_ble_hidh_filter_register", "group__wiced__bt__ble__hidh__api__functions.html#ga6a4c0f69c8c0789355f5f954e3bc8b0f", null ],
    [ "wiced_bt_ble_hidh_gatt_discovery_complete", "group__wiced__bt__ble__hidh__api__functions.html#gaf4d09c6ff7c16c73a957fccbcce13321", null ],
    [ "wiced_bt_ble_hidh_gatt_discovery_result", "group__wiced__bt__ble__hidh__api__functions.html#ga068246b00e27bfc404594130e19127ab", null ],
    [ "wiced_bt_ble_hidh_gatt_operation_complete", "group__wiced__bt__ble__hidh__api__functions.html#ga3b7c94362f74f5a89b1246b52575357e", null ],
    [ "wiced_bt_ble_hidh_get_report", "group__wiced__bt__ble__hidh__api__functions.html#gab70d835bfb808ef03dcbbf8a7f345342", null ],
    [ "wiced_bt_ble_hidh_init", "group__wiced__bt__ble__hidh__api__functions.html#gad60b8ef64ca734b141c2c722674f15da", null ],
    [ "wiced_bt_ble_hidh_remove", "group__wiced__bt__ble__hidh__api__functions.html#gab9a8e5388238e20c70e6e521c117c197", null ],
    [ "wiced_bt_ble_hidh_set_protocol", "group__wiced__bt__ble__hidh__api__functions.html#ga2cd7cc0a0250dfb0cbd1b1f7ae4f0bd9", null ],
    [ "wiced_bt_ble_hidh_set_report", "group__wiced__bt__ble__hidh__api__functions.html#ga995e536c2cfe25babe855ccbdb80ddb5", null ],
    [ "wiced_bt_ble_hidh_up", "group__wiced__bt__ble__hidh__api__functions.html#gadd0018cf65d577574b5f957b6f1a148e", null ],
    [ "wiced_bt_ble_hidh_wakeup_pattern_control", "group__wiced__bt__ble__hidh__api__functions.html#gaa702c48440d641e0748f15f6aa30bc44", null ],
    [ "wiced_bt_ble_hidh_wakeup_pattern_set", "group__wiced__bt__ble__hidh__api__functions.html#gacf66d9c19d8a5e1f7ab209edd6370243", null ]
];