var group__wiced__bt__mesh__onoff__server =
[
    [ "wiced_bt_mesh_onoff_server_callback_t", "group__wiced__bt__mesh__onoff__server.html#ga38b7cd2f950fc314bdb99fab0ff3f6f1", null ],
    [ "wiced_bt_mesh_model_onoff_server_init", "group__wiced__bt__mesh__onoff__server.html#ga890770f46b1519facb51b73607101ff3", null ],
    [ "wiced_bt_mesh_model_onoff_server_message_handler", "group__wiced__bt__mesh__onoff__server.html#gae8407b3533ba16b322606715ecc95421", null ],
    [ "wiced_bt_mesh_model_onoff_server_scene_recall_handler", "group__wiced__bt__mesh__onoff__server.html#ga695d902ada448311745f293913bdfa2d", null ],
    [ "wiced_bt_mesh_model_onoff_server_scene_store_handler", "group__wiced__bt__mesh__onoff__server.html#ga183decc30f376b4a051c5a5a1752aa8d", null ],
    [ "wiced_bt_mesh_model_onoff_server_send_status", "group__wiced__bt__mesh__onoff__server.html#ga33f65f9b527f1090708f1a69e9441c16", null ]
];