var group__wiced__bt__ancs__api__functions =
[
    [ "ancs_event_t", "structancs__event__t.html", [
      [ "category", "structancs__event__t.html#ad32641b3997d65c33b13514e10676e29", null ],
      [ "command", "structancs__event__t.html#a1a5aaa930940857f68f245eeb89506b5", null ],
      [ "flags", "structancs__event__t.html#aa2585d779da0ab21273a8d92de9a0ebe", null ],
      [ "message", "structancs__event__t.html#a62f0fd3e52d61e2b4292284eb588c897", null ],
      [ "negative_action_label", "structancs__event__t.html#ae4e6e3dc48c41385e79486c1cd068a4b", null ],
      [ "notification_uid", "structancs__event__t.html#ae42f69f6c5e626585ff1d8659302024c", null ],
      [ "p_next", "structancs__event__t.html#a46b3b8db2420ef5ea1b26f43b172a9ad", null ],
      [ "positive_action_label", "structancs__event__t.html#a2bb032e5fa1cb5fb70f8b03aadf07200", null ],
      [ "title", "structancs__event__t.html#a0b9e4627203f814e26eb305c1cd121a1", null ]
    ] ],
    [ "wiced_bt_ancs_reg_t", "structwiced__bt__ancs__reg__t.html", [
      [ "p_discovery_complete_callback", "structwiced__bt__ancs__reg__t.html#affb4cde85ca864d54fb0f671e3db4337", null ],
      [ "p_notification_callback", "structwiced__bt__ancs__reg__t.html#a634fa2b3b59f970ff79b5b1b85ae7dfd", null ],
      [ "p_start_complete_callback", "structwiced__bt__ancs__reg__t.html#ab6187683c0b3f679402783556782abb4", null ],
      [ "p_stop_complete_callback", "structwiced__bt__ancs__reg__t.html#a8f4e40d301da8374b31107ba4763da5e", null ]
    ] ],
    [ "ANCS_MAX_QUEUED_NOTIFICATIONS", "group__wiced__bt__ancs__api__functions.html#ga6df221d6074f5afcce89e1f3e085fb84", null ],
    [ "wiced_bt_ancs_discovery_complete_callback_t", "group__wiced__bt__ancs__api__functions.html#ga62c6ad124c2e776bc2933b37de0a8fa1", null ],
    [ "wiced_bt_ancs_notification_callback_t", "group__wiced__bt__ancs__api__functions.html#ga5ea04ba4ed325dd2c64b308c52011f89", null ],
    [ "wiced_bt_ancs_start_complete_callback_t", "group__wiced__bt__ancs__api__functions.html#ga8ea9503b83502851bbdf5dddc696e195", null ],
    [ "wiced_bt_ancs_stop_complete_callback_t", "group__wiced__bt__ancs__api__functions.html#gaf1e44e449a0a229598eadcd21533bea7", null ],
    [ "wiced_bt_ancs_client_connection_down", "group__wiced__bt__ancs__api__functions.html#ga3e419b75fb0dd4d72adc15ca4a0ca477", null ],
    [ "wiced_bt_ancs_client_connection_up", "group__wiced__bt__ancs__api__functions.html#gaf37c96a32ee0bb334e75c7be62f1c01c", null ],
    [ "wiced_bt_ancs_client_discover", "group__wiced__bt__ancs__api__functions.html#ga5a58f9112b758c4c42bb0ecf43ca7f4c", null ],
    [ "wiced_bt_ancs_client_discovery_complete", "group__wiced__bt__ancs__api__functions.html#ga34ee36f2dd5c74e9fe48f5683082da97", null ],
    [ "wiced_bt_ancs_client_discovery_result", "group__wiced__bt__ancs__api__functions.html#gaf7c258f067d3e9f3a9248c9272cb1254", null ],
    [ "wiced_bt_ancs_client_initialize", "group__wiced__bt__ancs__api__functions.html#ga14fa480f52a4a1970092308def8c3f90", null ],
    [ "wiced_bt_ancs_client_start", "group__wiced__bt__ancs__api__functions.html#gab6ce861a4e0986275a96dcdec99c0811", null ],
    [ "wiced_bt_ancs_client_stop", "group__wiced__bt__ancs__api__functions.html#ga560d3e2d17bbc2389865af527ab329c0", null ],
    [ "wiced_bt_ancs_client_write_rsp", "group__wiced__bt__ancs__api__functions.html#gae1505ef25832ab9b400c81592891cd49", null ],
    [ "wiced_bt_ancs_perform_action", "group__wiced__bt__ancs__api__functions.html#gaa09ef49d3893b89fae7e6146a23549a8", null ],
    [ "ancs_client_notification_attribute", "group__wiced__bt__ancs__api__functions.html#gaf05de2849730bcc65bd1ba843ec44168", null ],
    [ "ancs_client_notification_attribute_length", "group__wiced__bt__ancs__api__functions.html#gae9e1f7c21278daa342c324c49cd8c3c3", null ],
    [ "ANCS_CONTROL_POINT", "group__wiced__bt__ancs__api__functions.html#gafe8e27b90c8433b36191407c0e00fb0f", null ],
    [ "ANCS_DATA_SOURCE", "group__wiced__bt__ancs__api__functions.html#gafdbfa1960012aaf2d2bb41fc50802081", null ],
    [ "ANCS_NOTIFICATION_SOURCE", "group__wiced__bt__ancs__api__functions.html#ga95f3abe0b60f82274e0c3b145f4eba9b", null ],
    [ "ANCS_SERVICE", "group__wiced__bt__ancs__api__functions.html#ga75f1700319bb68ab0841f7661213b1b5", null ]
];