var group__wiced__bt__mesh__location__client =
[
    [ "wiced_bt_mesh_location_client_callback_t", "group__wiced__bt__mesh__location__client.html#gad94159585c5506bd897d4dc11f43e4c4", null ],
    [ "wiced_bt_mesh_model_location_client_init", "group__wiced__bt__mesh__location__client.html#gaa010b446ab2b4c0b83bf8a15c5266111", null ],
    [ "wiced_bt_mesh_model_location_client_message_handler", "group__wiced__bt__mesh__location__client.html#gaadf4255b6ade28b4f994af0190f1df38", null ],
    [ "wiced_bt_mesh_model_location_client_send_global_get", "group__wiced__bt__mesh__location__client.html#gaec30117531a0cf01c0197d9226f7e826", null ],
    [ "wiced_bt_mesh_model_location_client_send_global_set", "group__wiced__bt__mesh__location__client.html#ga53bbe7045e3850df821b9085ee3ccaad", null ],
    [ "wiced_bt_mesh_model_location_client_send_local_set", "group__wiced__bt__mesh__location__client.html#gae32eca053e6663a4d2ddee0f30a1bc78", null ]
];