var group__wicedbt =
[
    [ "AMS Library API", "group__wiced__bt__ams__api__functions.html", "group__wiced__bt__ams__api__functions" ],
    [ "ANCS Library API", "group__wiced__bt__ancs__api__functions.html", "group__wiced__bt__ancs__api__functions" ],
    [ "Audio / Video", "group__wicedbt__av.html", "group__wicedbt__av" ],
    [ "Battery Client", "group__wiced__bt__ble__battc__api__functions.html", "group__wiced__bt__ble__battc__api__functions" ],
    [ "FindMe Locator", "group__wicedbt___findmel__api__functions.html", "group__wicedbt___findmel__api__functions" ],
    [ "Device Management", "group__wicedbt___device_management.html", "group__wicedbt___device_management" ],
    [ "Generic Attribute (GATT)", "group__wicedbt__gatt.html", "group__wicedbt__gatt" ],
    [ "Human Interface Device (HID)", "group__wiced__bt__hid.html", "group__wiced__bt__hid" ],
    [ "Logical Link Control and Adaptation Protocol (L2CAP)", "group__l2cap.html", "group__l2cap" ],
    [ "OBEX", "group__wicedbt__obex.html", "group__wicedbt__obex" ],
    [ "RFCOMM", "group__rfcomm__api__functions.html", "group__rfcomm__api__functions" ],
    [ "Synchronous Connection Oriented (SCO) Channel", "group__sco__api__functions.html", "group__sco__api__functions" ],
    [ "Service Discovery (SDP)", "group__sdp__api__functions.html", "group__sdp__api__functions" ],
    [ "SPP Protocol Library API", "group__wiced__bt__spp__api__functions.html", "group__wiced__bt__spp__api__functions" ],
    [ "Framework", "group__wicedbt___framework.html", "group__wicedbt___framework" ],
    [ "Memory Management", "group__wiced__mem.html", "group__wiced__mem" ],
    [ "BLE Mesh", "group__wiced__bt__mesh.html", "group__wiced__bt__mesh" ]
];