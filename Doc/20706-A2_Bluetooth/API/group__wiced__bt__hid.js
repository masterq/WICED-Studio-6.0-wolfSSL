var group__wiced__bt__hid =
[
    [ "HID Host Role (HIDH) over BLE", "group__wiced__bt__ble__hidh__api__functions.html", "group__wiced__bt__ble__hidh__api__functions" ],
    [ "HID Host over BLE Audio", "group__wiced__bt__ble__hidh__audio__api__functions.html", "group__wiced__bt__ble__hidh__audio__api__functions" ],
    [ "HID Host Role (HIDH) over BR/EDR", "group__wiced__bt__hidh__api__functions.html", "group__wiced__bt__hidh__api__functions" ]
];