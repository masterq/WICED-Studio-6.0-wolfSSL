var group__wiced__bt__ble__hidh__audio__api__functions =
[
    [ "wiced_bt_ble_hidh_audio_start_t", "structwiced__bt__ble__hidh__audio__start__t.html", [
      [ "channel_nb", "structwiced__bt__ble__hidh__audio__start__t.html#a9080cc0b84ce76e61f3d54b22f49ee30", null ],
      [ "format", "structwiced__bt__ble__hidh__audio__start__t.html#a387df52d7ea4bc587f01b067e559e379", null ],
      [ "frequency", "structwiced__bt__ble__hidh__audio__start__t.html#aea762e0e67fcafaf5b3cd61201769926", null ],
      [ "handle", "structwiced__bt__ble__hidh__audio__start__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ]
    ] ],
    [ "wiced_bt_ble_hidh_audio_stop_t", "structwiced__bt__ble__hidh__audio__stop__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__audio__stop__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ]
    ] ],
    [ "wiced_bt_ble_hidh_audio_rx_data_t", "structwiced__bt__ble__hidh__audio__rx__data__t.html", [
      [ "handle", "structwiced__bt__ble__hidh__audio__rx__data__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__ble__hidh__audio__rx__data__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_data", "structwiced__bt__ble__hidh__audio__rx__data__t.html#a8304c4af5da6e830b86d7199dc9a22e6", null ]
    ] ],
    [ "wiced_bt_ble_hidh_audio_event_data_t", "unionwiced__bt__ble__hidh__audio__event__data__t.html", [
      [ "rx_data", "unionwiced__bt__ble__hidh__audio__event__data__t.html#ad9acdb9f035dc2637fa897f879da8249", null ],
      [ "start", "unionwiced__bt__ble__hidh__audio__event__data__t.html#acf894faa3455863a2b4d2d4a4b2c8490", null ],
      [ "stop", "unionwiced__bt__ble__hidh__audio__event__data__t.html#a2801bfcbdce839bc928e7ba182d8b7de", null ]
    ] ],
    [ "wiced_bt_ble_hidh_audio_cback_t", "group__wiced__bt__ble__hidh__audio__api__functions.html#ga505d4a0e8b360321124aa24de763c226", null ],
    [ "wiced_bt_ble_hidh_audio_event_t", "group__wiced__bt__ble__hidh__audio__api__functions.html#ga62ebf0ec3a7b4eeee95fbf5acc68ac1a", [
      [ "WICED_BT_BLE_HIDH_AUDIO_START_EVT", "group__wiced__bt__ble__hidh__audio__api__functions.html#gga62ebf0ec3a7b4eeee95fbf5acc68ac1aa8908a223febcebadca0b14afbdb4e32e", null ],
      [ "WICED_BT_BLE_HIDH_AUDIO_STOP_EVT", "group__wiced__bt__ble__hidh__audio__api__functions.html#gga62ebf0ec3a7b4eeee95fbf5acc68ac1aa1fa5367007f2cea77de995cb3903ddec", null ],
      [ "WICED_BT_BLE_HIDH_AUDIO_RX_DATA_EVT", "group__wiced__bt__ble__hidh__audio__api__functions.html#gga62ebf0ec3a7b4eeee95fbf5acc68ac1aa738e21fb01162fc845fe16715375dd2f", null ]
    ] ],
    [ "wiced_bt_ble_hidh_audio_format_t", "group__wiced__bt__ble__hidh__audio__api__functions.html#ga4477439df15438dea263653e59f1968b", [
      [ "WICED_BT_BLE_HIDH_AUDIO_FORMAT_PCM_16", "group__wiced__bt__ble__hidh__audio__api__functions.html#gga4477439df15438dea263653e59f1968bac93961e47613954d236cb197e7d86727", null ]
    ] ]
];