var group__wiced__bt__hidh__api__functions =
[
    [ "wiced_bt_hidh_open_t", "structwiced__bt__hidh__open__t.html", [
      [ "bdaddr", "structwiced__bt__hidh__open__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "handle", "structwiced__bt__hidh__open__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__hidh__open__t.html#ac79f5926fe9d5697246ddc80f79b8ea6", null ]
    ] ],
    [ "wiced_bt_hidh_close_t", "structwiced__bt__hidh__close__t.html", [
      [ "handle", "structwiced__bt__hidh__close__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "reason", "structwiced__bt__hidh__close__t.html#ada17c117060406a3a2871c6856dd0205", null ]
    ] ],
    [ "wiced_bt_hidh_descriptor_t", "structwiced__bt__hidh__descriptor__t.html", [
      [ "handle", "structwiced__bt__hidh__descriptor__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__hidh__descriptor__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_descriptor", "structwiced__bt__hidh__descriptor__t.html#a9b260d0b772b173d041c32f430cd0c82", null ],
      [ "status", "structwiced__bt__hidh__descriptor__t.html#ac79f5926fe9d5697246ddc80f79b8ea6", null ]
    ] ],
    [ "wiced_bt_hidh_set_protocol_t", "structwiced__bt__hidh__set__protocol__t.html", [
      [ "handle", "structwiced__bt__hidh__set__protocol__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__hidh__set__protocol__t.html#ac79f5926fe9d5697246ddc80f79b8ea6", null ]
    ] ],
    [ "wiced_bt_hidh_set_report_t", "structwiced__bt__hidh__set__report__t.html", [
      [ "handle", "structwiced__bt__hidh__set__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__hidh__set__report__t.html#ac79f5926fe9d5697246ddc80f79b8ea6", null ]
    ] ],
    [ "wiced_bt_hidh_get_report_t", "structwiced__bt__hidh__get__report__t.html", [
      [ "handle", "structwiced__bt__hidh__get__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__hidh__get__report__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_data", "structwiced__bt__hidh__get__report__t.html#a8304c4af5da6e830b86d7199dc9a22e6", null ],
      [ "status", "structwiced__bt__hidh__get__report__t.html#ac79f5926fe9d5697246ddc80f79b8ea6", null ]
    ] ],
    [ "wiced_bt_hidh_report_t", "structwiced__bt__hidh__report__t.html", [
      [ "handle", "structwiced__bt__hidh__report__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "length", "structwiced__bt__hidh__report__t.html#a1892eba2086d12ac2b09005aeb09ea3b", null ],
      [ "p_data", "structwiced__bt__hidh__report__t.html#a8304c4af5da6e830b86d7199dc9a22e6", null ],
      [ "report_id", "structwiced__bt__hidh__report__t.html#a725a13d8d43f65c0f5e8adc271f71573", null ]
    ] ],
    [ "wiced_bt_hidh_virtual_unplug_t", "structwiced__bt__hidh__virtual__unplug__t.html", [
      [ "handle", "structwiced__bt__hidh__virtual__unplug__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ]
    ] ],
    [ "wiced_bt_hidh_data_t", "unionwiced__bt__hidh__data__t.html", [
      [ "close", "unionwiced__bt__hidh__data__t.html#a74331ef6e4ac742f6518c3b400f2d5b0", null ],
      [ "descriptor", "unionwiced__bt__hidh__data__t.html#a6c240e93fe878ae7644cccb7871867ce", null ],
      [ "get_report", "unionwiced__bt__hidh__data__t.html#aadbf64dd5fc203b161aa4e1cd98cb593", null ],
      [ "open", "unionwiced__bt__hidh__data__t.html#a6c3a59c1438e9175b221e6f14f8894db", null ],
      [ "report", "unionwiced__bt__hidh__data__t.html#a5922e1e790cc1b42dde452f82800a88b", null ],
      [ "set_protocol", "unionwiced__bt__hidh__data__t.html#a53d32d54158e6b8a35bf026e0e6a897f", null ],
      [ "set_report", "unionwiced__bt__hidh__data__t.html#a09d6be808d54a761d82beda7c1985dcd", null ],
      [ "virtual_unplug", "unionwiced__bt__hidh__data__t.html#a937147f1ca43709ff800808ebe994b55", null ]
    ] ],
    [ "wiced_bt_hidh_cback_t", "group__wiced__bt__hidh__api__functions.html#gac2c4ce4d2837322442800f42372e4215", null ],
    [ "wiced_bt_hidh_channel_t", "group__wiced__bt__hidh__api__functions.html#ga887db02513b4bc3f4f90aceb96fc880b", [
      [ "WICED_BT_HIDH_CHANNEL_CONTROL", "group__wiced__bt__hidh__api__functions.html#gga887db02513b4bc3f4f90aceb96fc880ba7950a75136bd58b6c25df46bf1ca9b1d", null ],
      [ "WICED_BT_HIDH_CHANNEL_INTERRUPT", "group__wiced__bt__hidh__api__functions.html#gga887db02513b4bc3f4f90aceb96fc880bab546486202c0ffc31afaf2227661db80", null ]
    ] ],
    [ "wiced_bt_hidh_event_t", "group__wiced__bt__hidh__api__functions.html#ga2ba7091a2e89571fa68081e1b33e3071", [
      [ "WICED_BT_HIDH_OPEN_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071aad1255f64dc7d784d9ae0ce60ed2c19b", null ],
      [ "WICED_BT_HIDH_CLOSE_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071a69e7dc7992e26edee5b7503ab7d4b61c", null ],
      [ "WICED_BT_HIDH_DESCRIPTOR_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071a9a82f9f8e5a8407bf5655dfe813e7ee7", null ],
      [ "WICED_BT_HIDH_SET_PROTOCOL_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071aff58e2bfaa5383a43eaa22bdfc54dc7a", null ],
      [ "WICED_BT_HIDH_REPORT_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071aba7d0c44c4c76c435f6a3993afab740f", null ],
      [ "WICED_BT_HIDH_SET_REPORT_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071aeb6876b8d4605fe5bf59b73a94e382e0", null ],
      [ "WICED_BT_HIDH_GET_REPORT_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071aff84a0ddbd91ad916fbb956edb8b633e", null ],
      [ "WICED_BT_HIDH_VIRTUAL_UNPLUG_EVT", "group__wiced__bt__hidh__api__functions.html#gga2ba7091a2e89571fa68081e1b33e3071af92a207ed0eb4e597459746740da40b5", null ]
    ] ],
    [ "wiced_bt_hidh_protocol_t", "group__wiced__bt__hidh__api__functions.html#gada575523b6161d87169c39e486f92182", [
      [ "WICED_BT_HIDH_PROTOCOL_REPORT", "group__wiced__bt__hidh__api__functions.html#ggada575523b6161d87169c39e486f92182a6eda3205c707e68ebb2993fa161949ee", null ],
      [ "WICED_BT_HIDH_PROTOCOL_BOOT", "group__wiced__bt__hidh__api__functions.html#ggada575523b6161d87169c39e486f92182aaf590ca68572415e7ce4e7ba552adac1", null ]
    ] ],
    [ "wiced_bt_hidh_report_type_t", "group__wiced__bt__hidh__api__functions.html#ga0f4dc76ac11c2326bfcf36cca6e8eb4a", [
      [ "WICED_BT_HIDH_REPORT_TYPE_RESERVED", "group__wiced__bt__hidh__api__functions.html#gga0f4dc76ac11c2326bfcf36cca6e8eb4aa6abadc766223624f11713973ff9a2bd4", null ],
      [ "WICED_BT_HIDH_REPORT_TYPE_INPUT", "group__wiced__bt__hidh__api__functions.html#gga0f4dc76ac11c2326bfcf36cca6e8eb4aac55d38664d37efc7ff044504b3cf98e2", null ],
      [ "WICED_BT_HIDH_REPORT_TYPE_OUTPUT", "group__wiced__bt__hidh__api__functions.html#gga0f4dc76ac11c2326bfcf36cca6e8eb4aa0a4c7d42afc16499331e8e9287896d08", null ],
      [ "WICED_BT_HIDH_REPORT_TYPE_FEATURE", "group__wiced__bt__hidh__api__functions.html#gga0f4dc76ac11c2326bfcf36cca6e8eb4aad75a963f95e87dc519a88a23920a89a7", null ]
    ] ],
    [ "wiced_bt_hidh_status_t", "group__wiced__bt__hidh__api__functions.html#ga7da589d0870568964006a03897229c76", [
      [ "WICED_BT_HIDH_STATUS_OK", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76ae1d117b4d74e1484ead93d34d457a2c7", null ],
      [ "WICED_BT_HIDH_STATUS_ERROR", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a360f36ad2f934077827e00d7086f1f2b", null ],
      [ "WICED_BT_HIDH_STATUS_PAGE_TIMEOUT", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a0250df02b31adbe6149b3cd348faa99e", null ],
      [ "WICED_BT_HIDH_STATUS_CON_LOST", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a1a894ebd71cd6cad8cc0a0c8dd8528d5", null ],
      [ "WICED_BT_HIDH_STATUS_NOT_SUPPORTED", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a0f2ec0d88c59ca724f99f62eab6deaf9", null ],
      [ "WICED_BT_HIDH_STATUS_MEM_FULL", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a7e49efcc29198b8191dd67ff029ac7f1", null ],
      [ "WICED_BT_HIDH_STATUS_BUSY", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a17802707c5619ba1d2f338f159096582", null ],
      [ "WICED_BT_HIDH_STATUS_INVALID_REP_ID", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a72c1020ac88ca5d273d8bb6366b3f1cb", null ],
      [ "WICED_BT_HIDH_STATUS_UNSUPPORTED_REQ", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a8a30ccfe2fca55a9ff8c89407c29640a", null ],
      [ "WICED_BT_HIDH_STATUS_INVALID_PARAM", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a638efb8250d8c9d12ba18b78888d1770", null ],
      [ "WICED_BT_HIDH_STATUS_UNKNOWN", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a55af6225092eef725dfb99ae694d43f5", null ],
      [ "WICED_BT_HIDH_STATUS_FATAL", "group__wiced__bt__hidh__api__functions.html#gga7da589d0870568964006a03897229c76a6e8680552d5e60f10da00725696441e8", null ]
    ] ],
    [ "wiced_bt_hidh_add", "group__wiced__bt__hidh__api__functions.html#ga7d6c0a262077268f0d9864aec39047f3", null ],
    [ "wiced_bt_hidh_close", "group__wiced__bt__hidh__api__functions.html#gaf97a5a19168e13c72f9033d4296e99c5", null ],
    [ "wiced_bt_hidh_enable", "group__wiced__bt__hidh__api__functions.html#ga0812b0978d462e1ae2a2a7e7b65c90c7", null ],
    [ "wiced_bt_hidh_get_descriptor", "group__wiced__bt__hidh__api__functions.html#ga896e905c6649bdba78f6e6f1fd2fc94f", null ],
    [ "wiced_bt_hidh_get_report", "group__wiced__bt__hidh__api__functions.html#ga8f372a342dc480a351cfd4607ca9ea93", null ],
    [ "wiced_bt_hidh_init", "group__wiced__bt__hidh__api__functions.html#ga1ace81a8ee5efe80330cb5bf595d7bdf", null ],
    [ "wiced_bt_hidh_open", "group__wiced__bt__hidh__api__functions.html#gad5753e1a9742e99626833449b1201848", null ],
    [ "wiced_bt_hidh_remove", "group__wiced__bt__hidh__api__functions.html#gac54db6d5a07c41977d87163948721cc7", null ],
    [ "wiced_bt_hidh_set_protocol", "group__wiced__bt__hidh__api__functions.html#gae0630cda83aed50cc5faafa500ab29cf", null ],
    [ "wiced_bt_hidh_set_report", "group__wiced__bt__hidh__api__functions.html#ga1f749f65337691546eebf7137cae938d", null ]
];