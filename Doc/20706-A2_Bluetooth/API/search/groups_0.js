var searchData=
[
  ['analog_2dto_2ddigital_20converter_20_28adc_29',['Analog-to-Digital Converter (ADC)',['../group___adc_driver.html',1,'']]],
  ['aes',['AES',['../group__aes.html',1,'']]],
  ['arc4',['ARC4',['../group__arc4.html',1,'']]],
  ['auxiliary_20clock_20_28aclk_29',['Auxiliary Clock (ACLK)',['../group___aux_clk_driver.html',1,'']]],
  ['api_20functions',['API Functions',['../group__l2cap__api__functions.html',1,'']]],
  ['ams_20library_20api',['AMS Library API',['../group__wiced__bt__ams__api__functions.html',1,'']]],
  ['ancs_20library_20api',['ANCS Library API',['../group__wiced__bt__ancs__api__functions.html',1,'']]],
  ['advanced_20audio_20distribution_20profile_20_28a2dp_29_20sink',['Advanced Audio Distribution Profile (A2DP) Sink',['../group__wicedbt__a2dp.html',1,'']]],
  ['a2dp_20sbc_20support',['A2DP SBC Support',['../group__wicedbt__a2dp__sbc.html',1,'']]],
  ['audio_20_2f_20video',['Audio / Video',['../group__wicedbt__av.html',1,'']]],
  ['audio_20_2f_20video_20distribution_20_28avdtp_29',['Audio / Video Distribution (AVDTP)',['../group__wicedbt__avdt.html',1,'']]],
  ['audio_20_2f_20video_20remote_20control_20_28avrcp_29',['Audio / Video Remote Control (AVRCP)',['../group__wicedbt__avrc.html',1,'']]],
  ['avrcp_20common',['AVRCP Common',['../group__wicedbt__avrc__common.html',1,'']]],
  ['avrcp_20controller_20role_20_28ct_29',['AVRCP Controller Role (CT)',['../group__wicedbt__avrc__ct.html',1,'']]],
  ['avrcp_20target_20role_20_28tg_29',['AVRCP Target Role (TG)',['../group__wicedbt__avrc__tg.html',1,'']]]
];
