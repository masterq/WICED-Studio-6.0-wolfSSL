var group___transport =
[
    [ "wiced_spi_transport_cfg_t", "group___transport.html#gaf5fd9c4aa541faa690634024c063f038", null ],
    [ "wiced_tranport_data_handler_t", "group___transport.html#gac18db9b6503a1f1fd4ff237c5d4fccca", null ],
    [ "wiced_transport_buffer_pool_t", "group___transport.html#ga4c42181f8444932c1076e589e4c7feef", null ],
    [ "wiced_transport_cfg_t", "group___transport.html#gadd817eec38e772ef616e61dc15d3708e", null ],
    [ "wiced_transport_interface_cfg_t", "group___transport.html#gabd8d2c75a647214868e084439f66e8a2", null ],
    [ "wiced_transport_rx_buff_pool_cfg_t", "group___transport.html#ga068c39738f139bec421e911a4579964c", null ],
    [ "wiced_transport_status_handler_t", "group___transport.html#ga7b9d02d5fdbf9d3dd635d92be25d3372", null ],
    [ "wiced_transport_tx_complete_t", "group___transport.html#gaa346b15f3bee3902b565a13ab0f82815", null ],
    [ "wiced_uart_transport_cfg_t", "group___transport.html#ga5ab168f5dace84cfe34571e59bb91b29", null ],
    [ "wiced_transport_type_t", "group___transport.html#gaa3204840aade2c90bfa311c48beabcb3", null ],
    [ "wiced_transport_uart_mode_t", "group___transport.html#ga6d833ecd8b1499a47a89c49dfe8d60f1", null ],
    [ "wiced_trans_spi_sleep_config", "group___transport.html#ga943b97d93c7b25bee81320f6406d715a", null ],
    [ "wiced_transport_allocate_buffer", "group___transport.html#gaae7d6f384840fc9f8cd8f875a91b5387", null ],
    [ "wiced_transport_create_buffer_pool", "group___transport.html#ga9b8d8e0693a290ee79e26a76bf24292e", null ],
    [ "wiced_transport_free_buffer", "group___transport.html#gadaac5ec0edb31d250165df53425fbe78", null ],
    [ "wiced_transport_get_buffer_count", "group___transport.html#ga0f5f39c7f944016219f9259dbc619b57", null ],
    [ "wiced_transport_get_buffer_size", "group___transport.html#gad81af311451ffe89e90022c9636e526e", null ],
    [ "wiced_transport_init", "group___transport.html#gacdec4255fa64baf4937295afdd26cb7c", null ],
    [ "wiced_transport_send_buffer", "group___transport.html#ga0e0c4dd9bf07c0e1ef695807c34e0fbc", null ],
    [ "wiced_transport_send_data", "group___transport.html#ga0d22fe05e0462ab0a56b6878d3174d79", null ],
    [ "wiced_transport_send_hci_trace", "group___transport.html#ga593dffa16f0976ef9f813934c0147f11", null ]
];