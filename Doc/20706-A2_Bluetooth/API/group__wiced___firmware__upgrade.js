var group__wiced___firmware__upgrade =
[
    [ "wiced_fw_upgrade_nv_loc_len_t", "structwiced__fw__upgrade__nv__loc__len__t.html", [
      [ "ds1_len", "structwiced__fw__upgrade__nv__loc__len__t.html#a42ef3e026142314850a4a5cb8b0c15d4", null ],
      [ "ds1_loc", "structwiced__fw__upgrade__nv__loc__len__t.html#a9c149575aa9c7cdfac01a74ccb9dd31c", null ],
      [ "ds2_len", "structwiced__fw__upgrade__nv__loc__len__t.html#a01fe700a094cc2035cee67a4b132bb10", null ],
      [ "ds2_loc", "structwiced__fw__upgrade__nv__loc__len__t.html#a8c7fe1357aed26f771a91aef4a205f16", null ],
      [ "ss_loc", "structwiced__fw__upgrade__nv__loc__len__t.html#ab52b36ae75c03870671f474272297cdf", null ],
      [ "vs1_len", "structwiced__fw__upgrade__nv__loc__len__t.html#af37de341cd05f3eddd9d469a21f61957", null ],
      [ "vs1_loc", "structwiced__fw__upgrade__nv__loc__len__t.html#a06637dbd0c2500392a394611f9311195", null ],
      [ "vs2_len", "structwiced__fw__upgrade__nv__loc__len__t.html#a21f47f1d2b19a9939e8a995cb2223fc5", null ],
      [ "vs2_loc", "structwiced__fw__upgrade__nv__loc__len__t.html#a1392f0ac6254aa68ef05d299abe5d7de", null ]
    ] ],
    [ "wiced_firmware_upgrade_pre_reboot_callback_t", "group__wiced___firmware__upgrade.html#ga7fcf07f860b67cc9ea003eac6f0c8227", null ],
    [ "wiced_firmware_upgrade_finish", "group__wiced___firmware__upgrade.html#ga0503e75b0d97d7b7b00940cfa472861a", null ],
    [ "wiced_firmware_upgrade_init", "group__wiced___firmware__upgrade.html#ga22f57be793eff4382f54f0bc0fdfb953", null ],
    [ "wiced_firmware_upgrade_init_nv_locations", "group__wiced___firmware__upgrade.html#ga27fc916ae66e079b3459f1e553999f09", null ],
    [ "wiced_firmware_upgrade_retrieve_from_nv", "group__wiced___firmware__upgrade.html#gafc75a6dab6803a4ea2ec5418f696b8c3", null ],
    [ "wiced_firmware_upgrade_store_to_nv", "group__wiced___firmware__upgrade.html#ga4433b3bf0f43473881496851ba1fdf9d", null ],
    [ "wiced_ota_fw_upgrade_connection_status_event", "group__wiced___firmware__upgrade.html#ga54229d204646bb588ac2d89b741967fe", null ],
    [ "wiced_ota_fw_upgrade_indication_cfm_handler", "group__wiced___firmware__upgrade.html#gae7baca56ad74eead02f0eb38a8ad0db8", null ],
    [ "wiced_ota_fw_upgrade_init", "group__wiced___firmware__upgrade.html#ga7c565cf2dcb415de1ddf629183e20694", null ],
    [ "wiced_ota_fw_upgrade_read_handler", "group__wiced___firmware__upgrade.html#ga212afd28ddaf5c594d8cf52f2607d995", null ],
    [ "wiced_ota_fw_upgrade_write_handler", "group__wiced___firmware__upgrade.html#gace605108600db50d65304e59b531b77d", null ]
];