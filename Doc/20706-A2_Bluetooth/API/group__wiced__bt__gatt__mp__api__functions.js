var group__wiced__bt__gatt__mp__api__functions =
[
    [ "WICED_BT_GATT_MP_CONN_MAX", "group__wiced__bt__gatt__mp__api__functions.html#gaf56684d028d1f325ad4bc3c2b1d17ea6", null ],
    [ "WICED_BT_GATT_MP_PID_MAX", "group__wiced__bt__gatt__mp__api__functions.html#ga1f359bd4e9aaf3677b78057ff50d6457", null ],
    [ "wiced_bt_gatt_mp_pid_t", "group__wiced__bt__gatt__mp__api__functions.html#ga1a44de46ab713f42bcd84943fe2ea9d9", null ],
    [ "wiced_bt_gatt_mp_configure_mtu", "group__wiced__bt__gatt__mp__api__functions.html#ga8eb73761281f60859294388c228fafec", null ],
    [ "wiced_bt_gatt_mp_deregister", "group__wiced__bt__gatt__mp__api__functions.html#gafb6d882a24ffdf4d91f685fa55463230", null ],
    [ "wiced_bt_gatt_mp_disconnect", "group__wiced__bt__gatt__mp__api__functions.html#gac369d30f38afa7c7455464fa4b300b1e", null ],
    [ "wiced_bt_gatt_mp_encryption_changed", "group__wiced__bt__gatt__mp__api__functions.html#gae32b6db9a0d8a173f64fa4e4185395ea", null ],
    [ "wiced_bt_gatt_mp_init", "group__wiced__bt__gatt__mp__api__functions.html#gadd91f60ac2765bfa368058b6682e4479", null ],
    [ "wiced_bt_gatt_mp_is_encrypted", "group__wiced__bt__gatt__mp__api__functions.html#gadce9e2844e4cb6bd8f4cb61a18ce52e3", null ],
    [ "wiced_bt_gatt_mp_le_connect", "group__wiced__bt__gatt__mp__api__functions.html#ga9309d81dcde8257cfcfa50a037a359be", null ],
    [ "wiced_bt_gatt_mp_register", "group__wiced__bt__gatt__mp__api__functions.html#ga67c9f23ed85761842cbe850797d28072", null ],
    [ "wiced_bt_gatt_mp_register_handles", "group__wiced__bt__gatt__mp__api__functions.html#ga68e9f41120074e23bbd9f669e65280f1", null ],
    [ "wiced_bt_gatt_mp_send_discover", "group__wiced__bt__gatt__mp__api__functions.html#gaaf8f9281e034f8c222cfb53349e3397c", null ],
    [ "wiced_bt_gatt_mp_send_discover16", "group__wiced__bt__gatt__mp__api__functions.html#ga4dbce8e7b8b4d0a5a45ab1a7476670df", null ],
    [ "wiced_bt_gatt_mp_send_execute_write", "group__wiced__bt__gatt__mp__api__functions.html#gac526114c744d68bf191dc5070533af79", null ],
    [ "wiced_bt_gatt_mp_send_indication", "group__wiced__bt__gatt__mp__api__functions.html#ga35e2ded5da51e293ff89a687cb0d7de8", null ],
    [ "wiced_bt_gatt_mp_send_indication_confirm", "group__wiced__bt__gatt__mp__api__functions.html#ga950cf3c639bf39d76bf82bf0f58e32f0", null ],
    [ "wiced_bt_gatt_mp_send_notification", "group__wiced__bt__gatt__mp__api__functions.html#gac00b88940b04e5c31ddc04bf892a8cf9", null ],
    [ "wiced_bt_gatt_mp_send_read", "group__wiced__bt__gatt__mp__api__functions.html#ga2d88bfa29601483da67ce59fa3625b41", null ],
    [ "wiced_bt_gatt_mp_send_write", "group__wiced__bt__gatt__mp__api__functions.html#ga4370df4ebede2c32305e5e7c07f47572", null ]
];