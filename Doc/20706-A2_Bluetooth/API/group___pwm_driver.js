var group___pwm_driver =
[
    [ "pwm_config_t", "structpwm__config__t.html", [
      [ "init_count", "structpwm__config__t.html#a1404b86fa341304b95cec52838678b29", null ],
      [ "toggle_count", "structpwm__config__t.html#a7a7be229d612c85de36da94404026dca", null ]
    ] ],
    [ "wiced_hal_pwm_change_values", "group___pwm_driver.html#gaba341b56af89de7a1b8fde54b48652df", null ],
    [ "wiced_hal_pwm_disable", "group___pwm_driver.html#ga0b0b9c0dc78a1b6688ac5f7421a9f0af", null ],
    [ "wiced_hal_pwm_enable", "group___pwm_driver.html#ga6052f99e8a880652f573c10398a386b6", null ],
    [ "wiced_hal_pwm_get_init_value", "group___pwm_driver.html#ga7bd5732b8733e5982dbfb2b315aaadc0", null ],
    [ "wiced_hal_pwm_get_params", "group___pwm_driver.html#gac00190e9039965c6b93b9a023929b770", null ],
    [ "wiced_hal_pwm_get_toggle_count", "group___pwm_driver.html#ga0ab996c38b5e28d22b9f96b9b36a0ddf", null ],
    [ "wiced_hal_pwm_start", "group___pwm_driver.html#gac70a499f9615364611d4e322ef75b0e1", null ]
];