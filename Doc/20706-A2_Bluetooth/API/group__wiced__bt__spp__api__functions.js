var group__wiced__bt__spp__api__functions =
[
    [ "wiced_bt_spp_reg_t", "structwiced__bt__spp__reg__t.html", [
      [ "p_connection_down_callback", "structwiced__bt__spp__reg__t.html#a7d5ed2c0a8254d9fab032e0cf369d079", null ],
      [ "p_connection_failed_callback", "structwiced__bt__spp__reg__t.html#a3608799c7db9e4732e4f88eb5aaedfec", null ],
      [ "p_connection_up_callback", "structwiced__bt__spp__reg__t.html#a28841e27fe9efc3bda5393e595ead4e3", null ],
      [ "p_rx_data_callback", "structwiced__bt__spp__reg__t.html#a0b40df9091359d66f82157697ee79d05", null ],
      [ "p_service_not_found_callback", "structwiced__bt__spp__reg__t.html#a9738b3c2f44532f4c2c9d62838594cb1", null ],
      [ "rfcomm_mtu", "structwiced__bt__spp__reg__t.html#aefa2be089fd56821b92fe080d17c8f53", null ],
      [ "rfcomm_scn", "structwiced__bt__spp__reg__t.html#a94f2abff10f23a3bfc9751facfe9651d", null ]
    ] ],
    [ "wiced_bt_spp_connection_down_callback_t", "group__wiced__bt__spp__api__functions.html#ga8571fb855fd7139dd611863041339980", null ],
    [ "wiced_bt_spp_connection_failed_callback_t", "group__wiced__bt__spp__api__functions.html#ga254b2410dbd3d8c9037649489e6b8203", null ],
    [ "wiced_bt_spp_connection_up_callback_t", "group__wiced__bt__spp__api__functions.html#ga5c5c70bb21b99504927f619833b091f5", null ],
    [ "wiced_bt_spp_rx_data_callback_t", "group__wiced__bt__spp__api__functions.html#ga842fc79d956a41f5cbdaada0f1753690", null ],
    [ "wiced_bt_spp_service_not_found_callback_t", "group__wiced__bt__spp__api__functions.html#ga85c984a551ff8f1e0bd0d0f3a641620b", null ],
    [ "wiced_bt_spp_tx_complete_callback_t", "group__wiced__bt__spp__api__functions.html#ga991d89c51fd6d6e5ac2dd40f2441ba53", null ],
    [ "wiced_bt_spp_can_send_more_data", "group__wiced__bt__spp__api__functions.html#ga9a66c9eac81cd4f741232aeb3699040f", null ],
    [ "wiced_bt_spp_connect", "group__wiced__bt__spp__api__functions.html#gacccfbf6c98289fb891fae749295679f9", null ],
    [ "wiced_bt_spp_disconnect", "group__wiced__bt__spp__api__functions.html#ga84ec427b0f7d6a3c323b0fce4556c476", null ],
    [ "wiced_bt_spp_rx_flow_enabled", "group__wiced__bt__spp__api__functions.html#ga960acfc309b12fbd1304368031049cef", null ],
    [ "wiced_bt_spp_send_session_data", "group__wiced__bt__spp__api__functions.html#ga02b58d1fc649c861de5d05260f848b12", null ],
    [ "wiced_bt_spp_startup", "group__wiced__bt__spp__api__functions.html#gaa68300b37b72ab44fa74df1194e7596a", null ]
];