var group__wicedbt___findmel__api__functions =
[
    [ "wiced_bt_ble_findmel_connected_t", "structwiced__bt__ble__findmel__connected__t.html", [
      [ "bdaddr", "structwiced__bt__ble__findmel__connected__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "handle", "structwiced__bt__ble__findmel__connected__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "status", "structwiced__bt__ble__findmel__connected__t.html#a9e79c62fe55ce2264e63fa2cc64b8741", null ]
    ] ],
    [ "wiced_bt_ble_findmel_disconnected_t", "structwiced__bt__ble__findmel__disconnected__t.html", [
      [ "handle", "structwiced__bt__ble__findmel__disconnected__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "reason", "structwiced__bt__ble__findmel__disconnected__t.html#a53a9e6e1b68d477c173d07e156ca4f7b", null ]
    ] ],
    [ "wiced_bt_ble_findmel_gatt_char_t", "structwiced__bt__ble__findmel__gatt__char__t.html", [
      [ "handle", "structwiced__bt__ble__findmel__gatt__char__t.html#af242d6e3b6108ea75e1eb236e94c8240", null ],
      [ "properties", "structwiced__bt__ble__findmel__gatt__char__t.html#ad55938872fd47086c38b264f22b0eb65", null ],
      [ "uuid16", "structwiced__bt__ble__findmel__gatt__char__t.html#ae4ba2923b4d364d3472acf5ee2a7752c", null ],
      [ "val_handle", "structwiced__bt__ble__findmel__gatt__char__t.html#a13d14e316098f34a69b6e4f3c29b9b3c", null ]
    ] ],
    [ "wiced_bt_ble_findmel_gatt_cache_t", "structwiced__bt__ble__findmel__gatt__cache__t.html", [
      [ "bdaddr", "structwiced__bt__ble__findmel__gatt__cache__t.html#a17f123b687ff9e7b1ff6f55af5fe0865", null ],
      [ "characteristic", "structwiced__bt__ble__findmel__gatt__cache__t.html#a6eae68fbaf572baf13669876677912ed", null ]
    ] ],
    [ "wiced_bt_ble_findmel_event_data_t", "unionwiced__bt__ble__findmel__event__data__t.html", [
      [ "connected", "unionwiced__bt__ble__findmel__event__data__t.html#a5ca662f78054f11666aa2aab4387d27a", null ],
      [ "disconnected", "unionwiced__bt__ble__findmel__event__data__t.html#a2a221adfaaa385b6bd020345309feb98", null ],
      [ "gatt_cache", "unionwiced__bt__ble__findmel__event__data__t.html#ad83995ad598f46bc018648f1f2b00947", null ]
    ] ],
    [ "WICED_BT_BLE_FINDMEL_DEV_MAX", "group__wicedbt___findmel__api__functions.html#ga7a0d752b04bf17f8a06a2e44ae9f24f5", null ],
    [ "wiced_bt_ble_findmel_cback_t", "group__wicedbt___findmel__api__functions.html#gabecccdbf6455d0887d2ebe259cb97bc6", null ],
    [ "wiced_bt_ble_findmel_event_t", "group__wicedbt___findmel__api__functions.html#gafca925e9cf0060dea3a6a1eb2212294e", [
      [ "WICED_BT_BLE_FINDMEL_OPEN_EVT", "group__wicedbt___findmel__api__functions.html#ggafca925e9cf0060dea3a6a1eb2212294ea9992c56f77bf79ad559c7b7e91a744d4", null ],
      [ "WICED_BT_BLE_FINDMEL_CLOSE_EVT", "group__wicedbt___findmel__api__functions.html#ggafca925e9cf0060dea3a6a1eb2212294ea64da6dad370349df6b70454e8e872600", null ],
      [ "WICED_BT_BLE_FINDMEL_GATT_CACHE_EVT", "group__wicedbt___findmel__api__functions.html#ggafca925e9cf0060dea3a6a1eb2212294ea25c98e55e68d97b4e0f096be84ae3237", null ]
    ] ],
    [ "wiced_bt_ble_findmel_level_t", "group__wicedbt___findmel__api__functions.html#gaeccffb3f48ecedf4f824a242039fd143", [
      [ "WICED_BT_BLE_FINDMEL_LEVEL_NO_ALERT", "group__wicedbt___findmel__api__functions.html#ggaeccffb3f48ecedf4f824a242039fd143a9507c7427c51f940ffc7aef7a89d7ede", null ],
      [ "WICED_BT_BLE_FINDMEL_LEVEL_MILD_ALERT", "group__wicedbt___findmel__api__functions.html#ggaeccffb3f48ecedf4f824a242039fd143a210d7d5d8294a4e77beae973f422911f", null ],
      [ "WICED_BT_BLE_FINDMEL_LEVEL_HIGH_ALERT", "group__wicedbt___findmel__api__functions.html#ggaeccffb3f48ecedf4f824a242039fd143afe06ed62f02342bf66fb184895c6a223", null ]
    ] ],
    [ "wiced_bt_ble_findmel_status_t", "group__wicedbt___findmel__api__functions.html#ga42f7757a8079a05d2e3971a45780f138", [
      [ "WICED_BT_BLE_FINDMEL_STATUS_SUCCESS", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138a2b190854be9694ac8b06dc9e801cdaaf", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_UNSUPPORTED", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138ad169f44e1965fabcff3f7e0475e3e2fd", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_ERROR", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138a4347c9e2dd040ef8305e5e635a12ca9b", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_GATT_ERROR", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138a9adf63c9c7701728234198476f8f6541", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_INVALID_PARAM", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138a81285e0b9ba9b30f4a2fc4ee1120e6bf", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_MEM_FULL", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138aca614ce640486540604c1118a3a9f74b", null ],
      [ "WICED_BT_BLE_FINDMEL_STATUS_CONNECTION_FAILED", "group__wicedbt___findmel__api__functions.html#gga42f7757a8079a05d2e3971a45780f138ab438254054bf143b3db86a9b88765426", null ]
    ] ],
    [ "wiced_bt_ble_findmel_add", "group__wicedbt___findmel__api__functions.html#gaf600682065fe6fcd843bb5eb0b6b63e4", null ],
    [ "wiced_bt_ble_findmel_connect", "group__wicedbt___findmel__api__functions.html#gae62b04656cb71009504d1fa92c13074c", null ],
    [ "wiced_bt_ble_findmel_disconnect", "group__wicedbt___findmel__api__functions.html#ga17b98d2158e2bdd1f98f9553ed231ebf", null ],
    [ "wiced_bt_ble_findmel_encryption_changed", "group__wicedbt___findmel__api__functions.html#ga1cc6685eebb3fe076cc53f1a49ce15b6", null ],
    [ "wiced_bt_ble_findmel_init", "group__wicedbt___findmel__api__functions.html#ga8f5a59748528bbc4d7230395c91df128", null ],
    [ "wiced_bt_ble_findmel_remove", "group__wicedbt___findmel__api__functions.html#ga3a38bc6e863e3d714519714bf71ce99f", null ],
    [ "wiced_bt_ble_findmel_write_alert", "group__wicedbt___findmel__api__functions.html#gac1c308db2e808e536bafc763ebdd86bf", null ]
];