var group__wiced__bt__mesh__models =
[
    [ "mesh_battery_event_t", "structmesh__battery__event__t.html", [
      [ "battery_level", "structmesh__battery__event__t.html#a3c0aee7bfb6e464e575c6fe45ebf9b86", null ],
      [ "charging", "structmesh__battery__event__t.html#ae8ab857fadc83af7d7931c0874e1ab96", null ],
      [ "level_inidicator", "structmesh__battery__event__t.html#a3e901fd985daa36b7813eefdc5499018", null ],
      [ "presence", "structmesh__battery__event__t.html#a0fb4e2d117d1a1a4f6e6190170bc35ab", null ],
      [ "servicability", "structmesh__battery__event__t.html#ae72554ff008ab0fdded369df877657b5", null ],
      [ "time_to_charge", "structmesh__battery__event__t.html#a4033d2d4da842718b4f4abb7f3a6f5d5", null ],
      [ "time_to_discharge", "structmesh__battery__event__t.html#a53b528b3e7b9e50a7af8b8880bcfa032", null ]
    ] ],
    [ "wiced_bt_mesh_location_global_data_t", "structwiced__bt__mesh__location__global__data__t.html", [
      [ "global_altitude", "structwiced__bt__mesh__location__global__data__t.html#a321fbb281452ac3b9f2c4e6f7b6786a7", null ],
      [ "global_latitude", "structwiced__bt__mesh__location__global__data__t.html#a15fb15d1c0ba48ac264c80e36cc63f57", null ],
      [ "global_longitude", "structwiced__bt__mesh__location__global__data__t.html#a29cf5a10ba0bcce46865b560791c8bc4", null ]
    ] ],
    [ "wiced_bt_mesh_location_local_data_t", "structwiced__bt__mesh__location__local__data__t.html", [
      [ "floor_number", "structwiced__bt__mesh__location__local__data__t.html#a83005d78b4bf9436ff02d5b937ad6b2a", null ],
      [ "is_mobile", "structwiced__bt__mesh__location__local__data__t.html#a2e9de5cd2408c606fc609a790fa561b4", null ],
      [ "local_altitude", "structwiced__bt__mesh__location__local__data__t.html#ab0b5777419c58cfdde04a14721e3e49e", null ],
      [ "local_east", "structwiced__bt__mesh__location__local__data__t.html#a073482916db003790e5106c1c16cedda", null ],
      [ "local_north", "structwiced__bt__mesh__location__local__data__t.html#af05181b83f66aaef80f69782f8fd5db9", null ],
      [ "precision", "structwiced__bt__mesh__location__local__data__t.html#ace557f86a8fd710ca0a257d7d47a3560", null ],
      [ "update_time", "structwiced__bt__mesh__location__local__data__t.html#a23889ca4a3f67a8d2ba2edc7a14e9ad1", null ]
    ] ],
    [ "wiced_bt_mesh_onoff_set_data_t", "structwiced__bt__mesh__onoff__set__data__t.html", [
      [ "delay", "structwiced__bt__mesh__onoff__set__data__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "onoff", "structwiced__bt__mesh__onoff__set__data__t.html#ab846f82707d4c23e43f5208dab504c77", null ],
      [ "transition_time", "structwiced__bt__mesh__onoff__set__data__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_onoff_status_data_t", "structwiced__bt__mesh__onoff__status__data__t.html", [
      [ "present_onoff", "structwiced__bt__mesh__onoff__status__data__t.html#a09c7da8c6da38a06ee60cb9bb5c5476b", null ],
      [ "remaining_time", "structwiced__bt__mesh__onoff__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_onoff", "structwiced__bt__mesh__onoff__status__data__t.html#a646fa87b42187b149f6d8badab574ec0", null ]
    ] ],
    [ "wiced_bt_mesh_level_set_level_t", "structwiced__bt__mesh__level__set__level__t.html", [
      [ "delay", "structwiced__bt__mesh__level__set__level__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "level", "structwiced__bt__mesh__level__set__level__t.html#a2da91f0a0f8a8cd77e01c7617dfe58e3", null ],
      [ "transition_time", "structwiced__bt__mesh__level__set__level__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_level_set_delta_t", "structwiced__bt__mesh__level__set__delta__t.html", [
      [ "continuation", "structwiced__bt__mesh__level__set__delta__t.html#ac822e6d5f7ae5e33ce0ee0deb060e040", null ],
      [ "delay", "structwiced__bt__mesh__level__set__delta__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "delta", "structwiced__bt__mesh__level__set__delta__t.html#a682a609ad7209580739aa2589f49dc04", null ],
      [ "transition_time", "structwiced__bt__mesh__level__set__delta__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_level_set_move_t", "structwiced__bt__mesh__level__set__move__t.html", [
      [ "continuation", "structwiced__bt__mesh__level__set__move__t.html#ac822e6d5f7ae5e33ce0ee0deb060e040", null ],
      [ "delay", "structwiced__bt__mesh__level__set__move__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "delta", "structwiced__bt__mesh__level__set__move__t.html#a70d4fc7788f641b6913d1a8280206a9b", null ],
      [ "transition_time", "structwiced__bt__mesh__level__set__move__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_level_status_data_t", "structwiced__bt__mesh__level__status__data__t.html", [
      [ "present_level", "structwiced__bt__mesh__level__status__data__t.html#af8083705068d0784296c235eaf82cec8", null ],
      [ "remaining_time", "structwiced__bt__mesh__level__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_level", "structwiced__bt__mesh__level__status__data__t.html#ad504c111bee4bf7646d9f661b58de2f0", null ]
    ] ],
    [ "wiced_bt_mesh_default_transition_time_data_t", "structwiced__bt__mesh__default__transition__time__data__t.html", [
      [ "time", "structwiced__bt__mesh__default__transition__time__data__t.html#ae73654f333e4363463ad8c594eca1905", null ]
    ] ],
    [ "wiced_bt_mesh_power_onoff_data_t", "structwiced__bt__mesh__power__onoff__data__t.html", [
      [ "on_power_up", "structwiced__bt__mesh__power__onoff__data__t.html#af9effca8886dc00141a32b604cac88f6", null ]
    ] ],
    [ "wiced_bt_mesh_power_level_status_data_t", "structwiced__bt__mesh__power__level__status__data__t.html", [
      [ "present_power", "structwiced__bt__mesh__power__level__status__data__t.html#a0a526d102d32386ba578e4b456aef8c3", null ],
      [ "remaining_time", "structwiced__bt__mesh__power__level__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_power", "structwiced__bt__mesh__power__level__status__data__t.html#a689e89976eddabcb6ba0dff84924e51b", null ]
    ] ],
    [ "wiced_bt_mesh_power_level_last_data_t", "structwiced__bt__mesh__power__level__last__data__t.html", [
      [ "power", "structwiced__bt__mesh__power__level__last__data__t.html#a9bb8314656e9fc5911338903d902d1a9", null ]
    ] ],
    [ "wiced_bt_mesh_power_default_data_t", "structwiced__bt__mesh__power__default__data__t.html", [
      [ "power", "structwiced__bt__mesh__power__default__data__t.html#a9bb8314656e9fc5911338903d902d1a9", null ]
    ] ],
    [ "wiced_bt_mesh_power_level_range_set_data_t", "structwiced__bt__mesh__power__level__range__set__data__t.html", [
      [ "power_max", "structwiced__bt__mesh__power__level__range__set__data__t.html#a6c0997d77cd914eb8fa53f54d0d1343b", null ],
      [ "power_min", "structwiced__bt__mesh__power__level__range__set__data__t.html#af56c44a29742765d2d6bab9c8fcd8b01", null ]
    ] ],
    [ "wiced_bt_mesh_power_range_status_data_t", "structwiced__bt__mesh__power__range__status__data__t.html", [
      [ "power_max", "structwiced__bt__mesh__power__range__status__data__t.html#a6c0997d77cd914eb8fa53f54d0d1343b", null ],
      [ "power_min", "structwiced__bt__mesh__power__range__status__data__t.html#af56c44a29742765d2d6bab9c8fcd8b01", null ],
      [ "status", "structwiced__bt__mesh__power__range__status__data__t.html#ade818037fd6c985038ff29656089758d", null ]
    ] ],
    [ "wiced_bt_mesh_power_level_set_level_t", "structwiced__bt__mesh__power__level__set__level__t.html", [
      [ "delay", "structwiced__bt__mesh__power__level__set__level__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "level", "structwiced__bt__mesh__power__level__set__level__t.html#a864fde14aaec72616f08a10c4a3a07ad", null ],
      [ "transition_time", "structwiced__bt__mesh__power__level__set__level__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_status_data_t", "structwiced__bt__mesh__light__lightness__status__data__t.html", [
      [ "present", "structwiced__bt__mesh__light__lightness__status__data__t.html#a0aeb19c8e9ac98318e93f84e69d3243f", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__lightness__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target", "structwiced__bt__mesh__light__lightness__status__data__t.html#af27d9627f4a2f2839ae2c3500301b87e", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_set_t", "structwiced__bt__mesh__light__lightness__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__lightness__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "lightness_actual", "structwiced__bt__mesh__light__lightness__set__t.html#a17312bb6480549142f7cfbcc728a75b9", null ],
      [ "lightness_linear", "structwiced__bt__mesh__light__lightness__set__t.html#ace23c6ab066de48c058b1793a19ad41a", null ],
      [ "transition_time", "structwiced__bt__mesh__light__lightness__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_actual_set_t", "structwiced__bt__mesh__light__lightness__actual__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__lightness__actual__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "lightness_actual", "structwiced__bt__mesh__light__lightness__actual__set__t.html#a17312bb6480549142f7cfbcc728a75b9", null ],
      [ "transition_time", "structwiced__bt__mesh__light__lightness__actual__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_linear_set_t", "structwiced__bt__mesh__light__lightness__linear__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__lightness__linear__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "lightness_linear", "structwiced__bt__mesh__light__lightness__linear__set__t.html#ace23c6ab066de48c058b1793a19ad41a", null ],
      [ "transition_time", "structwiced__bt__mesh__light__lightness__linear__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_last_data_t", "structwiced__bt__mesh__light__lightness__last__data__t.html", [
      [ "last_level", "structwiced__bt__mesh__light__lightness__last__data__t.html#a8fe92e45aae40b038b7d22d3a976db84", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_default_data_t", "structwiced__bt__mesh__light__lightness__default__data__t.html", [
      [ "default_level", "structwiced__bt__mesh__light__lightness__default__data__t.html#a9496a26bb12f665ca514f4e482ccf88f", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_range_set_data_t", "structwiced__bt__mesh__light__lightness__range__set__data__t.html", [
      [ "max_level", "structwiced__bt__mesh__light__lightness__range__set__data__t.html#af3640e972f2c3e009773c3037a376b64", null ],
      [ "min_level", "structwiced__bt__mesh__light__lightness__range__set__data__t.html#a7c109bb4723cc86bf32d05a61218b9e2", null ]
    ] ],
    [ "wiced_bt_mesh_light_lightness_range_status_data_t", "structwiced__bt__mesh__light__lightness__range__status__data__t.html", [
      [ "max_level", "structwiced__bt__mesh__light__lightness__range__status__data__t.html#af3640e972f2c3e009773c3037a376b64", null ],
      [ "min_level", "structwiced__bt__mesh__light__lightness__range__status__data__t.html#a7c109bb4723cc86bf32d05a61218b9e2", null ],
      [ "status", "structwiced__bt__mesh__light__lightness__range__status__data__t.html#ade818037fd6c985038ff29656089758d", null ]
    ] ],
    [ "wiced_bt_mesh_properties_get_data_t", "structwiced__bt__mesh__properties__get__data__t.html", [
      [ "starting_id", "structwiced__bt__mesh__properties__get__data__t.html#a18d3664574ad7a6267b78de27043d5d7", null ],
      [ "type", "structwiced__bt__mesh__properties__get__data__t.html#a1d127017fb298b889f4ba24752d08b8e", null ]
    ] ],
    [ "wiced_bt_mesh_property_get_data_t", "structwiced__bt__mesh__property__get__data__t.html", [
      [ "id", "structwiced__bt__mesh__property__get__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
      [ "type", "structwiced__bt__mesh__property__get__data__t.html#a1d127017fb298b889f4ba24752d08b8e", null ]
    ] ],
    [ "wiced_bt_mesh_properties_status_data_t", "structwiced__bt__mesh__properties__status__data__t.html", [
      [ "id", "structwiced__bt__mesh__properties__status__data__t.html#aacce74ab4e266853217783b13acad523", null ],
      [ "properties_num", "structwiced__bt__mesh__properties__status__data__t.html#a33bd439954516567235fc257725b42f3", null ],
      [ "type", "structwiced__bt__mesh__properties__status__data__t.html#a1d127017fb298b889f4ba24752d08b8e", null ]
    ] ],
    [ "wiced_bt_mesh_property_set_data_t", "structwiced__bt__mesh__property__set__data__t.html", [
      [ "access", "structwiced__bt__mesh__property__set__data__t.html#a8b0d6200bc639dd37ff68847a0adde5f", null ],
      [ "id", "structwiced__bt__mesh__property__set__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
      [ "len", "structwiced__bt__mesh__property__set__data__t.html#a8aed22e2c7b283705ec82e0120515618", null ],
      [ "type", "structwiced__bt__mesh__property__set__data__t.html#a1d127017fb298b889f4ba24752d08b8e", null ],
      [ "value", "structwiced__bt__mesh__property__set__data__t.html#a2c18aaad4bff3be18292e343c3b28dc8", null ]
    ] ],
    [ "wiced_bt_mesh_property_status_data_t", "structwiced__bt__mesh__property__status__data__t.html", [
      [ "access", "structwiced__bt__mesh__property__status__data__t.html#a8b0d6200bc639dd37ff68847a0adde5f", null ],
      [ "id", "structwiced__bt__mesh__property__status__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
      [ "len", "structwiced__bt__mesh__property__status__data__t.html#a8aed22e2c7b283705ec82e0120515618", null ],
      [ "type", "structwiced__bt__mesh__property__status__data__t.html#a1d127017fb298b889f4ba24752d08b8e", null ],
      [ "value", "structwiced__bt__mesh__property__status__data__t.html#a1ed5b151a90f7e99af8cca2e6875ddf4", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_data_t", "structwiced__bt__mesh__light__ctl__data__t.html", [
      [ "delta_uv", "structwiced__bt__mesh__light__ctl__data__t.html#aa89cdf6c8632f721c3033cc5904e8087", null ],
      [ "lightness", "structwiced__bt__mesh__light__ctl__data__t.html#a968eb4438426da63f07e28bfa3578bda", null ],
      [ "temperature", "structwiced__bt__mesh__light__ctl__data__t.html#a6bd95140398cc8f4b44157b17d6ba31e", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_status_data_t", "structwiced__bt__mesh__light__ctl__status__data__t.html", [
      [ "present", "structwiced__bt__mesh__light__ctl__status__data__t.html#ac9f524d68a7ad8c9bebe6e4f77b5f299", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__ctl__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target", "structwiced__bt__mesh__light__ctl__status__data__t.html#a70cca36b6f723c28e65089bff78138a2", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_set_t", "structwiced__bt__mesh__light__ctl__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__ctl__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "target", "structwiced__bt__mesh__light__ctl__set__t.html#a70cca36b6f723c28e65089bff78138a2", null ],
      [ "transition_time", "structwiced__bt__mesh__light__ctl__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_temperature_set_t", "structwiced__bt__mesh__light__ctl__temperature__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__ctl__temperature__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "target_delta_uv", "structwiced__bt__mesh__light__ctl__temperature__set__t.html#a86c00786b720f77f2dbad34721a0d541", null ],
      [ "target_temperature", "structwiced__bt__mesh__light__ctl__temperature__set__t.html#a8d635c050452c20c44ba058443ad5543", null ],
      [ "transition_time", "structwiced__bt__mesh__light__ctl__temperature__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_default_data_t", "structwiced__bt__mesh__light__ctl__default__data__t.html", [
      [ "default_status", "structwiced__bt__mesh__light__ctl__default__data__t.html#a025b245a42d698d3b5a0bbb01f6dd2a1", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_temperature_range_status_data_t", "structwiced__bt__mesh__light__ctl__temperature__range__status__data__t.html", [
      [ "max_level", "structwiced__bt__mesh__light__ctl__temperature__range__status__data__t.html#af3640e972f2c3e009773c3037a376b64", null ],
      [ "min_level", "structwiced__bt__mesh__light__ctl__temperature__range__status__data__t.html#a7c109bb4723cc86bf32d05a61218b9e2", null ],
      [ "status", "structwiced__bt__mesh__light__ctl__temperature__range__status__data__t.html#ade818037fd6c985038ff29656089758d", null ]
    ] ],
    [ "wiced_bt_mesh_light_ctl_temperature_range_data_t", "structwiced__bt__mesh__light__ctl__temperature__range__data__t.html", [
      [ "max_level", "structwiced__bt__mesh__light__ctl__temperature__range__data__t.html#af3640e972f2c3e009773c3037a376b64", null ],
      [ "min_level", "structwiced__bt__mesh__light__ctl__temperature__range__data__t.html#a7c109bb4723cc86bf32d05a61218b9e2", null ]
    ] ],
    [ "wiced_bt_mesh_light_delta_uv_set_level_t", "structwiced__bt__mesh__light__delta__uv__set__level__t.html", [
      [ "delay", "structwiced__bt__mesh__light__delta__uv__set__level__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "level", "structwiced__bt__mesh__light__delta__uv__set__level__t.html#a864fde14aaec72616f08a10c4a3a07ad", null ],
      [ "transition_time", "structwiced__bt__mesh__light__delta__uv__set__level__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_delta_uv_last_data_t", "structwiced__bt__mesh__light__delta__uv__last__data__t.html", [
      [ "last_level", "structwiced__bt__mesh__light__delta__uv__last__data__t.html#a8fe92e45aae40b038b7d22d3a976db84", null ]
    ] ],
    [ "wiced_bt_mesh_light_delta_uv_default_data_t", "structwiced__bt__mesh__light__delta__uv__default__data__t.html", [
      [ "default_level", "structwiced__bt__mesh__light__delta__uv__default__data__t.html#a9496a26bb12f665ca514f4e482ccf88f", null ]
    ] ],
    [ "wiced_bt_mesh_light_delta_uv_range_data_t", "structwiced__bt__mesh__light__delta__uv__range__data__t.html", [
      [ "max_level", "structwiced__bt__mesh__light__delta__uv__range__data__t.html#af3640e972f2c3e009773c3037a376b64", null ],
      [ "min_level", "structwiced__bt__mesh__light__delta__uv__range__data__t.html#a7c109bb4723cc86bf32d05a61218b9e2", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_data_t", "structwiced__bt__mesh__light__hsl__data__t.html", [
      [ "hue", "structwiced__bt__mesh__light__hsl__data__t.html#a2d3850bf87522a9e917e8a33cf4201a5", null ],
      [ "lightness", "structwiced__bt__mesh__light__hsl__data__t.html#a968eb4438426da63f07e28bfa3578bda", null ],
      [ "saturation", "structwiced__bt__mesh__light__hsl__data__t.html#a6bc230d6374c45c9ce52286adb1d8832", null ],
      [ "x", "structwiced__bt__mesh__light__hsl__data__t.html#a4dde988b1b2adba65ae3efa69f65d960", null ],
      [ "y", "structwiced__bt__mesh__light__hsl__data__t.html#ab0580f504a7428539be299fa71565f30", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_status_data_t", "structwiced__bt__mesh__light__hsl__status__data__t.html", [
      [ "present", "structwiced__bt__mesh__light__hsl__status__data__t.html#aaa5e1fd2b6a88bc1a10e019fd71ea100", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__hsl__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_target_status_data_t", "structwiced__bt__mesh__light__hsl__target__status__data__t.html", [
      [ "remaining_time", "structwiced__bt__mesh__light__hsl__target__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target", "structwiced__bt__mesh__light__hsl__target__status__data__t.html#acc14229c6c6e49aad6ec90498f63506b", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_set_t", "structwiced__bt__mesh__light__hsl__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__hsl__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "target", "structwiced__bt__mesh__light__hsl__set__t.html#acc14229c6c6e49aad6ec90498f63506b", null ],
      [ "transition_time", "structwiced__bt__mesh__light__hsl__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_hue_set_t", "structwiced__bt__mesh__light__hsl__hue__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__hsl__hue__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "level", "structwiced__bt__mesh__light__hsl__hue__set__t.html#a864fde14aaec72616f08a10c4a3a07ad", null ],
      [ "transition_time", "structwiced__bt__mesh__light__hsl__hue__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_saturation_set_t", "structwiced__bt__mesh__light__hsl__saturation__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__hsl__saturation__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "level", "structwiced__bt__mesh__light__hsl__saturation__set__t.html#a864fde14aaec72616f08a10c4a3a07ad", null ],
      [ "transition_time", "structwiced__bt__mesh__light__hsl__saturation__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_default_data_t", "structwiced__bt__mesh__light__hsl__default__data__t.html", [
      [ "default_status", "structwiced__bt__mesh__light__hsl__default__data__t.html#a92dde920f351819d74f256069f06f459", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_hue_status_data_t", "structwiced__bt__mesh__light__hsl__hue__status__data__t.html", [
      [ "present_hue", "structwiced__bt__mesh__light__hsl__hue__status__data__t.html#a0ea8719f9c03dd88a1e516e8d6d04cac", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__hsl__hue__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_hue", "structwiced__bt__mesh__light__hsl__hue__status__data__t.html#a096207fdbd87829043fe9ec2c1f8ebad", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_saturation_status_data_t", "structwiced__bt__mesh__light__hsl__saturation__status__data__t.html", [
      [ "present_saturation", "structwiced__bt__mesh__light__hsl__saturation__status__data__t.html#a05080ae9a892614340166b7231b4f505", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__hsl__saturation__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_saturation", "structwiced__bt__mesh__light__hsl__saturation__status__data__t.html#a3d7e351ac623a0fa5bee7455d7ae85f4", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_range_set_data_t", "structwiced__bt__mesh__light__hsl__range__set__data__t.html", [
      [ "hue_max", "structwiced__bt__mesh__light__hsl__range__set__data__t.html#ad1d0b7aeaf52154060bb19946434a646", null ],
      [ "hue_min", "structwiced__bt__mesh__light__hsl__range__set__data__t.html#acc4bde8ecfb2c24668e99b08702c7429", null ],
      [ "saturation_max", "structwiced__bt__mesh__light__hsl__range__set__data__t.html#a4d3e60a035cbfc8e273ea4c65de48f2b", null ],
      [ "saturation_min", "structwiced__bt__mesh__light__hsl__range__set__data__t.html#a0d63a922b117c408afe623159cef14c9", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_range_status_data_t", "structwiced__bt__mesh__light__hsl__range__status__data__t.html", [
      [ "hue_max", "structwiced__bt__mesh__light__hsl__range__status__data__t.html#ad1d0b7aeaf52154060bb19946434a646", null ],
      [ "hue_min", "structwiced__bt__mesh__light__hsl__range__status__data__t.html#acc4bde8ecfb2c24668e99b08702c7429", null ],
      [ "saturation_max", "structwiced__bt__mesh__light__hsl__range__status__data__t.html#a4d3e60a035cbfc8e273ea4c65de48f2b", null ],
      [ "saturation_min", "structwiced__bt__mesh__light__hsl__range__status__data__t.html#a0d63a922b117c408afe623159cef14c9", null ],
      [ "status", "structwiced__bt__mesh__light__hsl__range__status__data__t.html#ade818037fd6c985038ff29656089758d", null ]
    ] ],
    [ "wiced_bt_mesh_light_hsl_default_status_data_t", "structwiced__bt__mesh__light__hsl__default__status__data__t.html", [
      [ "default_status", "structwiced__bt__mesh__light__hsl__default__status__data__t.html#a92dde920f351819d74f256069f06f459", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_data_t", "structwiced__bt__mesh__light__xyl__data__t.html", [
      [ "lightness", "structwiced__bt__mesh__light__xyl__data__t.html#a968eb4438426da63f07e28bfa3578bda", null ],
      [ "x", "structwiced__bt__mesh__light__xyl__data__t.html#a4dde988b1b2adba65ae3efa69f65d960", null ],
      [ "y", "structwiced__bt__mesh__light__xyl__data__t.html#ab0580f504a7428539be299fa71565f30", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_xy_settings_t", "structwiced__bt__mesh__light__xyl__xy__settings__t.html", [
      [ "x_default", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#a575fc6dc681570f4e5a50bf737486116", null ],
      [ "x_max", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#a85cc0f4334633f7f998e2d55e15aa948", null ],
      [ "x_min", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#ab5379478017ff6051cdd90b108dca2d8", null ],
      [ "y_default", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#a6ab632893e03ab0f8162c6241d2f236c", null ],
      [ "y_max", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#a6c2ea0e2f3ad699f6b5bb36f4ada9abc", null ],
      [ "y_min", "structwiced__bt__mesh__light__xyl__xy__settings__t.html#a86e6872ec9440240ea8d9ab089525d2b", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_status_data_t", "structwiced__bt__mesh__light__xyl__status__data__t.html", [
      [ "present", "structwiced__bt__mesh__light__xyl__status__data__t.html#a4e819217c30e10149d563c3c0f40a67d", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__xyl__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_target_status_data_t", "structwiced__bt__mesh__light__xyl__target__status__data__t.html", [
      [ "remaining_time", "structwiced__bt__mesh__light__xyl__target__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target", "structwiced__bt__mesh__light__xyl__target__status__data__t.html#a0f1bf111da0755d7b5f9ef52f61e966e", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_set_t", "structwiced__bt__mesh__light__xyl__set__t.html", [
      [ "delay", "structwiced__bt__mesh__light__xyl__set__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "target", "structwiced__bt__mesh__light__xyl__set__t.html#a0f1bf111da0755d7b5f9ef52f61e966e", null ],
      [ "transition_time", "structwiced__bt__mesh__light__xyl__set__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_default_data_t", "structwiced__bt__mesh__light__xyl__default__data__t.html", [
      [ "default_status", "structwiced__bt__mesh__light__xyl__default__data__t.html#ab5bae9353df67146de6a493d0569bdd0", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_range_set_data_t", "structwiced__bt__mesh__light__xyl__range__set__data__t.html", [
      [ "x_max", "structwiced__bt__mesh__light__xyl__range__set__data__t.html#a85cc0f4334633f7f998e2d55e15aa948", null ],
      [ "x_min", "structwiced__bt__mesh__light__xyl__range__set__data__t.html#ab5379478017ff6051cdd90b108dca2d8", null ],
      [ "y_max", "structwiced__bt__mesh__light__xyl__range__set__data__t.html#a6c2ea0e2f3ad699f6b5bb36f4ada9abc", null ],
      [ "y_min", "structwiced__bt__mesh__light__xyl__range__set__data__t.html#a86e6872ec9440240ea8d9ab089525d2b", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_range_status_data_t", "structwiced__bt__mesh__light__xyl__range__status__data__t.html", [
      [ "status", "structwiced__bt__mesh__light__xyl__range__status__data__t.html#ade818037fd6c985038ff29656089758d", null ],
      [ "x_max", "structwiced__bt__mesh__light__xyl__range__status__data__t.html#a85cc0f4334633f7f998e2d55e15aa948", null ],
      [ "x_min", "structwiced__bt__mesh__light__xyl__range__status__data__t.html#ab5379478017ff6051cdd90b108dca2d8", null ],
      [ "y_max", "structwiced__bt__mesh__light__xyl__range__status__data__t.html#a6c2ea0e2f3ad699f6b5bb36f4ada9abc", null ],
      [ "y_min", "structwiced__bt__mesh__light__xyl__range__status__data__t.html#a86e6872ec9440240ea8d9ab089525d2b", null ]
    ] ],
    [ "wiced_bt_mesh_light_xyl_default_status_data_t", "structwiced__bt__mesh__light__xyl__default__status__data__t.html", [
      [ "default_status", "structwiced__bt__mesh__light__xyl__default__status__data__t.html#ab5bae9353df67146de6a493d0569bdd0", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_mode_set_data_t", "structwiced__bt__mesh__light__lc__mode__set__data__t.html", [
      [ "mode", "structwiced__bt__mesh__light__lc__mode__set__data__t.html#a37e90f5e3bd99fac2021fb3a326607d4", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_occupancy_mode_set_data_t", "structwiced__bt__mesh__light__lc__occupancy__mode__set__data__t.html", [
      [ "mode", "structwiced__bt__mesh__light__lc__occupancy__mode__set__data__t.html#a37e90f5e3bd99fac2021fb3a326607d4", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_light_onoff_set_data_t", "structwiced__bt__mesh__light__lc__light__onoff__set__data__t.html", [
      [ "delay", "structwiced__bt__mesh__light__lc__light__onoff__set__data__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "light_onoff", "structwiced__bt__mesh__light__lc__light__onoff__set__data__t.html#afd6fcc3eda9937182c8881e6fbf7ff8f", null ],
      [ "transition_time", "structwiced__bt__mesh__light__lc__light__onoff__set__data__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_light_onoff_status_data_t", "structwiced__bt__mesh__light__lc__light__onoff__status__data__t.html", [
      [ "present_onoff", "structwiced__bt__mesh__light__lc__light__onoff__status__data__t.html#a09c7da8c6da38a06ee60cb9bb5c5476b", null ],
      [ "remaining_time", "structwiced__bt__mesh__light__lc__light__onoff__status__data__t.html#abb94f89f6e6e799c5ae0076cc03f251e", null ],
      [ "target_onoff", "structwiced__bt__mesh__light__lc__light__onoff__status__data__t.html#a646fa87b42187b149f6d8badab574ec0", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_linear_out_set_data_t", "structwiced__bt__mesh__light__lc__linear__out__set__data__t.html", [
      [ "delay", "structwiced__bt__mesh__light__lc__linear__out__set__data__t.html#adc4674f6ea53803d98fa2ec36759e77d", null ],
      [ "linear_out", "structwiced__bt__mesh__light__lc__linear__out__set__data__t.html#a56282d8f945484706e00e7ebcd67b9a3", null ],
      [ "transition_time", "structwiced__bt__mesh__light__lc__linear__out__set__data__t.html#a284a81478e29d96532d13ebd737d6b82", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_property_get_data_t", "structwiced__bt__mesh__light__lc__property__get__data__t.html", [
      [ "id", "structwiced__bt__mesh__light__lc__property__get__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_property_set_data_t", "structwiced__bt__mesh__light__lc__property__set__data__t.html", [
      [ "id", "structwiced__bt__mesh__light__lc__property__set__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
      [ "len", "structwiced__bt__mesh__light__lc__property__set__data__t.html#a8aed22e2c7b283705ec82e0120515618", null ],
      [ "value", "structwiced__bt__mesh__light__lc__property__set__data__t.html#a9f1e5c8d8323a21cd866ba859b1d7a44", null ]
    ] ],
    [ "wiced_bt_mesh_light_lc_property_status_data_t", "structwiced__bt__mesh__light__lc__property__status__data__t.html", [
      [ "id", "structwiced__bt__mesh__light__lc__property__status__data__t.html#a4fc3a0c58dfbd1e68224521185cb9384", null ],
      [ "len", "structwiced__bt__mesh__light__lc__property__status__data__t.html#a8aed22e2c7b283705ec82e0120515618", null ],
      [ "value", "structwiced__bt__mesh__light__lc__property__status__data__t.html#a9f1e5c8d8323a21cd866ba859b1d7a44", null ]
    ] ],
    [ "Mesh Battery Server", "group__wiced__bt__mesh__battery__server.html", "group__wiced__bt__mesh__battery__server" ],
    [ "Mesh Battery Client", "group__wiced__bt__mesh__battery__client.html", "group__wiced__bt__mesh__battery__client" ],
    [ "Mesh Location Server", "group__wiced__bt__mesh__location__server.html", "group__wiced__bt__mesh__location__server" ],
    [ "Mesh Location Client", "group__wiced__bt__mesh__location__client.html", "group__wiced__bt__mesh__location__client" ],
    [ "Mesh OnOff Server", "group__wiced__bt__mesh__onoff__server.html", "group__wiced__bt__mesh__onoff__server" ],
    [ "Mesh OnOff Client", "group__wiced__bt__mesh__onoff__client.html", null ],
    [ "WICED_BT_BATTERY_LEVEL_UNKNOWN", "group__wiced__bt__mesh__models.html#ga3daa5b0817f9552901ee39f38cc00621", null ],
    [ "WICED_BT_BATTERY_TIME_TO_CHARGE_UNKNOWN", "group__wiced__bt__mesh__models.html#ga2b4c1569fc411dc57d64a5f84b41b8f5", null ],
    [ "WICED_BT_BATTERY_TIME_TO_DISCHARGE_UNKNOWN", "group__wiced__bt__mesh__models.html#gac795a2a9ea8f003c58113c23d12e7ceb", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_CHARGING", "group__wiced__bt__mesh__models.html#gad1f64d6cb990d50e97aeef2018da01b9", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_CHARGING_UNKNOWN", "group__wiced__bt__mesh__models.html#gaace0a7540b5776e02ea61416ae216806", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_LEVEL_CRITICALLY_LOW", "group__wiced__bt__mesh__models.html#ga4bbe21fefcaf82fd1f25f10a41e961bd", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_LEVEL_GOOD", "group__wiced__bt__mesh__models.html#gaa4c2f2786c0317866dea430fdd5f0a44", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_LEVEL_LOW", "group__wiced__bt__mesh__models.html#gaa265c8c08f849f33c5f6dd4a8cf7e713", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_LEVEL_UNKNOWN", "group__wiced__bt__mesh__models.html#ga7f1c5c83f780b62c0ca3f3cae8b971d3", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_NOT_CHARGABLE", "group__wiced__bt__mesh__models.html#gae202cddf29465ddbc705bd7b0427f68e", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_NOT_CHARGING", "group__wiced__bt__mesh__models.html#gaf5a568abe6992a2e8011d6abc11a9684", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_NOT_PRESENT", "group__wiced__bt__mesh__models.html#ga002171e5549f1b9a78712220855aa1cb", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_PRESENCE_UNKNOWN", "group__wiced__bt__mesh__models.html#ga6f24c71bb630240b88f18d4850660611", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_PRESENT_NON_REMOVABLE", "group__wiced__bt__mesh__models.html#ga1aa887025e1b911749c316b2849d6b29", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_PRESENT_REMOVABLE", "group__wiced__bt__mesh__models.html#ga7015703a57e889f92762469949f249ae", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_SERVICABILITY_UNKNOWN", "group__wiced__bt__mesh__models.html#gaf980d755e93121f51b3608c08a799e60", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_SERVICE_NOT_REQUIRED", "group__wiced__bt__mesh__models.html#ga52c5e46f53da270dcf8cfe96a92cad91", null ],
    [ "WICED_BT_MESH_BATTERY_FLAG_SERVICE_REQUIRED", "group__wiced__bt__mesh__models.html#ga27d3431a5f79f54141ad3b086343c1bb", null ],
    [ "WICED_BT_MESH_LEVEL_MAX_RETRANSMIT_TIME", "group__wiced__bt__mesh__models.html#ga92a3d92c21fdea3daa5d84bdfff882fa", null ],
    [ "WICED_BT_MESH_LEVEL_MOVE_MAX_TRANSITION_TIME", "group__wiced__bt__mesh__models.html#gacc2cbd21b1c211887afc09ac3f03b3aa", null ],
    [ "WICED_BT_MESH_ONOFF_MAX_RETRANSMIT_TIME", "group__wiced__bt__mesh__models.html#gafbdf505bc971ee9057220c4eade174f1", null ],
    [ "WICED_BT_MESH_TRANSITION_TIME_DEFAULT", "group__wiced__bt__mesh__models.html#gabbefa81d7ef47be9593ed93c80221946", null ],
    [ "wiced_bt_mesh_battery_charging_t", "group__wiced__bt__mesh__models.html#gae191641b773db5b28a85cb2aec02981e", null ],
    [ "wiced_bt_mesh_battery_indicator_t", "group__wiced__bt__mesh__models.html#gac290185d2284f327e37042878522094c", null ],
    [ "wiced_bt_mesh_battery_presence_t", "group__wiced__bt__mesh__models.html#gaa67243af9f7a0d1ee2413f0a60ee66a8", null ],
    [ "wiced_bt_mesh_battery_servicibility_t", "group__wiced__bt__mesh__models.html#ga55746a04db5ebfea32b24bd837e9dc5f", null ],
    [ "wiced_bt_mesh_onoff_client_callback_t", "group__wiced__bt__mesh__models.html#ga1090dd9f3579d12c4cd8ced741560ce6", null ],
    [ "wiced_model_scene_recall_handler_t", "group__wiced__bt__mesh__models.html#ga14ca9e69ea94ccfbd44c574b786440c1", null ],
    [ "wiced_model_scene_store_handler_t", "group__wiced__bt__mesh__models.html#gaaf3c246bde4017cf5fcc0c04cb005e45", [
      [ "WICED_BT_MESH_PROPERTY_TYPE_CLIENT", "group__wiced__bt__mesh__models.html#gga394b3903fbf00ba2b6243f60689a5a5fa4c76c0583e075a2b8fccc8b906b1d1f5", null ],
      [ "WICED_BT_MESH_PROPERTY_TYPE_ADMIN", "group__wiced__bt__mesh__models.html#gga394b3903fbf00ba2b6243f60689a5a5fa6d7ebbe61cb9af161ff40f38c72e8982", null ],
      [ "WICED_BT_MESH_PROPERTY_TYPE_MANUFACTURER", "group__wiced__bt__mesh__models.html#gga394b3903fbf00ba2b6243f60689a5a5fa11e0791723f51739f0dc946e86a5ba40", null ],
      [ "WICED_BT_MESH_PROPERTY_TYPE_USER", "group__wiced__bt__mesh__models.html#gga394b3903fbf00ba2b6243f60689a5a5fafa02dfe39ccd6b2e4c3e5fd611be81c4", null ]
    ] ],
    [ "wiced_bt_mesh_model_onoff_client_init", "group__wiced__bt__mesh__models.html#ga05dc5ec90d2ad8a1e9ce1d64c4fc85fa", null ],
    [ "wiced_bt_mesh_model_onoff_client_message_handler", "group__wiced__bt__mesh__models.html#ga673750835549336e8128e8c6d630441f", null ],
    [ "wiced_bt_mesh_model_onoff_client_send_get", "group__wiced__bt__mesh__models.html#ga04f62dd6c859127633342f55b9682c6b", null ],
    [ "wiced_bt_mesh_model_onoff_client_send_set", "group__wiced__bt__mesh__models.html#ga9c927bc79f6050009a4680d7f4736422", null ],
    [ "wiced_bt_mesh_property_type", "group__wiced__bt__mesh__models.html#ga2c6f322561cee107c4d5fb6cdcd84a70", null ]
];