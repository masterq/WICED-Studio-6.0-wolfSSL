Linux drivers for FTDI
======================

Driver in this folder is installed by the WICED Studio installer.
To manually install the driver, run the following commands (requires sudo):

> sudo cp 67-wiced-JTAG.rules /etc/udev/rules.d/
> sudo chmod a-x /etc/udev/rules.d/67-wiced-JTAG.rules
> sudo mkdir -p /opt/cypress
> sudo cp cypress_ftdi.sh /opt/cypress/
> sudo chmod a+x /opt/cypress/cypress_ftdi.sh
