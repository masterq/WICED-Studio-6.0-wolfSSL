--------------------------------------------
BCM943907AEVAL2F - README
--------------------------------------------

Provider    : Cypress
Website     : http://cypress.com/wiced
Description : Cypress BCM943907AEVAL2F

Module
  Mfr       : Cypress
  P/N       : BCM943907AEVAL2F
  MCU       : BCM943907 Cortex-R4 320MHz  (Apps Core) 
  WLAN      : BCM943907 Cortex-R4 (WLAN Core)
  WLAN Antennas : Diversity with two printed antennae (and in-line switched AMP A-1JB connectors)

--------------------------------------------
Board Revisions
--------------------------------------------

P103        ChipRevision : B1
