--------------------------------------------
BCM943907WAE2_1 - README
--------------------------------------------

Provider    : Cypress
Website     : http://cypress.com/wiced
Description : Cypress BCM943907WCD1 module mounted on a BCM943907WAE2_1 Cypress evaluation board

Module
  Mfr       : Cypress
  P/N       : BCM943907WCD1
  MCU       : BCM943907 Cortex-R4 320MHz  (Apps Core) 
  WLAN      : BCM943907 Cortex-R4 (WLAN Core)
  Bluetooth : BCM20707A1
  WLAN Antennas : Diversity with two printed antennae (and in-line switched AMP A-1JB connectors)
  BT   Antennas : Printed antenna (and in-line switched Murata MM8130-2600 RF connector)

--------------------------------------------
Board Revisions
--------------------------------------------

P101 - Latest     ChipRevision : B1
