/** @file
 *
 * wolfSSL test Application
 *
 */

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/logging.h>
#include <wolfcrypt/test/test.h>
#include <stdio.h>
#include "wiced.h"

#define WAIT_SEC 3

typedef struct func_args {
    int    argc;
    char** argv;
    int    return_code;
} func_args;

static func_args args = { 0 } ;

void application_start( )
{
	wiced_init();
	wolfCrypt_Init();

	while(TRUE) {
		printf("\nCrypt Test\n");
		wolfcrypt_test(&args);
		printf("Crypt Test: Return code %d\n\n", args.return_code);

		printf("Sleep %d seconds to restart...\n", WAIT_SEC);
		wiced_rtos_delay_milliseconds(WAIT_SEC * 1000);
	}

	wolfCrypt_Cleanup();
}
