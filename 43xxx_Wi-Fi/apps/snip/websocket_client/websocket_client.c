/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * Websocket Client Application
 *
 * This application snippet demonstrates how to use a simple Websocket Client protocol
 *
 * Features demonstrated
 *  - Wi-Fi client mode
 *  - DNS lookup
 *  - Secure/unsecure websocket( wiced_websocket_secure_connect/wiced_websocket_connect) client connection
 *
 * Application Instructions
 * 1. Modify the CLIENT_AP_SSID/CLIENT_AP_PASSPHRASE Wi-Fi credentials
 *    in the wifi_config_dct.h header file to match your Wi-Fi access point
 * 2. Connect a PC terminal to the serial port of the WICED Eval board,
 *    then build and download the application as described in the WICED
 *    Quick Start Guide
 *
 * After the download completes, the application :
 *  - Connects to the Wi-Fi network specified
 *  - Resolves the websocket server ip address
 *  - Connects to the websocket server and sends two test frames
 *  --1. character set 0-9
 *  --2. websocket ping request/response (not icmp)
 *
 *  The snippet also shows how the websocket API can be used.
 *  i.e. wiced_websocket_t API has the following callbacks:
 *   on open callback       : called once websocket handshake is complete
 *   on close callback      : called on socket close
 *   on error callback      : called on websocket error defined by wiced_websocket_error_t
 *   on message callback    : called whenever a new arrives on the websocket
 *
 *   Also note when sending frames through the wiced_websocket_frame_t structure,
 *   the application must define if this is last frame in message or not
 *   Frames are currently limited to 1024 bytes
 *
 *Limitations:
 *   Does not yet handle fragmentation across multiple packets
 *
 *   Note: The default websocket server used with this snippet is from echo.websocket.org
 *   This is a publicly available server, and is not managed by Cypress
 */

#include <stdlib.h>
#include "wiced.h"
#include "websocket.h"
#include "wiced_tls.h"

/******************************************************
 *                      Macros
 ******************************************************/

#define WEBSOCKET_SNIPPET_VERIFY(x)  {wiced_result_t res = (x); if (res != WICED_SUCCESS){ goto exit;}}
#define FINAL_FRAME WICED_TRUE
#define USE_WEBSOCKET_SECURE_CLIENT

/******************************************************
 *                    Constants
 ******************************************************/
#define BUFFER_LENGTH     (1024)
#define PING_MESSAGE      "This is a PING message from the websocket client"

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

wiced_result_t on_open_websocket_callback   ( void* websocket );
wiced_result_t on_close_websocket_callback  ( void* websocket );
wiced_result_t on_error_websocket_callback  ( void* websocket );
wiced_result_t on_message_websocket_callback( void* websocket );

/******************************************************
 *               Variables Definitions
 ******************************************************/
static wiced_websocket_frame_t tx_frame;
static wiced_websocket_frame_t rx_frame;

static char rx_buffer[ BUFFER_LENGTH ];
static char tx_buffer[ BUFFER_LENGTH ] = { '0','1','2','3','4','5','6','7','8','9','\0' };

static wiced_semaphore_t received_message_event;

/* Godaddy Root CA certificate(for connecting to echo.websocket.org) + securedemo.wiced.cypress.com certificate */
static const char httpbin_root_ca_certificate[] =
    "-----BEGIN CERTIFICATE-----\n"                                      \
    "MIIEfTCCA2WgAwIBAgIDG+cVMA0GCSqGSIb3DQEBCwUAMGMxCzAJBgNVBAYTAlVT\n" \
    "MSEwHwYDVQQKExhUaGUgR28gRGFkZHkgR3JvdXAsIEluYy4xMTAvBgNVBAsTKEdv\n" \
    "IERhZGR5IENsYXNzIDIgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTQwMTAx\n" \
    "MDcwMDAwWhcNMzEwNTMwMDcwMDAwWjCBgzELMAkGA1UEBhMCVVMxEDAOBgNVBAgT\n" \
    "B0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxGjAYBgNVBAoTEUdvRGFkZHku\n" \
    "Y29tLCBJbmMuMTEwLwYDVQQDEyhHbyBEYWRkeSBSb290IENlcnRpZmljYXRlIEF1\n" \
    "dGhvcml0eSAtIEcyMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv3Fi\n" \
    "CPH6WTT3G8kYo/eASVjpIoMTpsUgQwE7hPHmhUmfJ+r2hBtOoLTbcJjHMgGxBT4H\n" \
    "Tu70+k8vWTAi56sZVmvigAf88xZ1gDlRe+X5NbZ0TqmNghPktj+pA4P6or6KFWp/\n" \
    "3gvDthkUBcrqw6gElDtGfDIN8wBmIsiNaW02jBEYt9OyHGC0OPoCjM7T3UYH3go+\n" \
    "6118yHz7sCtTpJJiaVElBWEaRIGMLKlDliPfrDqBmg4pxRyp6V0etp6eMAo5zvGI\n" \
    "gPtLXcwy7IViQyU0AlYnAZG0O3AqP26x6JyIAX2f1PnbU21gnb8s51iruF9G/M7E\n" \
    "GwM8CetJMVxpRrPgRwIDAQABo4IBFzCCARMwDwYDVR0TAQH/BAUwAwEB/zAOBgNV\n" \
    "HQ8BAf8EBAMCAQYwHQYDVR0OBBYEFDqahQcQZyi27/a9BUFuIMGU2g/eMB8GA1Ud\n" \
    "IwQYMBaAFNLEsNKR1EwRcbNhyz2h/t2oatTjMDQGCCsGAQUFBwEBBCgwJjAkBggr\n" \
    "BgEFBQcwAYYYaHR0cDovL29jc3AuZ29kYWRkeS5jb20vMDIGA1UdHwQrMCkwJ6Al\n" \
    "oCOGIWh0dHA6Ly9jcmwuZ29kYWRkeS5jb20vZ2Ryb290LmNybDBGBgNVHSAEPzA9\n" \
    "MDsGBFUdIAAwMzAxBggrBgEFBQcCARYlaHR0cHM6Ly9jZXJ0cy5nb2RhZGR5LmNv\n" \
    "bS9yZXBvc2l0b3J5LzANBgkqhkiG9w0BAQsFAAOCAQEAWQtTvZKGEacke+1bMc8d\n" \
    "H2xwxbhuvk679r6XUOEwf7ooXGKUwuN+M/f7QnaF25UcjCJYdQkMiGVnOQoWCcWg\n" \
    "OJekxSOTP7QYpgEGRJHjp2kntFolfzq3Ms3dhP8qOCkzpN1nsoX+oYggHFCJyNwq\n" \
    "9kIDN0zmiN/VryTyscPfzLXs4Jlet0lUIDyUGAzHHFIYSaRt4bNYC8nY7NmuHDKO\n" \
    "KHAN4v6mF56ED71XcLNa6R+ghlO773z/aQvgSMO3kwvIClTErF0UZzdsyqUvMQg3\n" \
    "qm5vjLyb4lddJIGvl5echK1srDdMZvNhkREg5L4wn3qkKQmw4TRfZHcYQFHfjDCm\n" \
    "rw==\n"                                                             \
    "-----END CERTIFICATE-----\n"                                        \
    "-----BEGIN CERTIFICATE-----\n" \
    "MIICTzCCAbgCCQDpIVfd7XLcTTANBgkqhkiG9w0BAQsFADBsMQswCQYDVQQGEwJJ\n" \
    "TjETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQgV2lkZ2l0\n" \
    "cyBQdHkgTHRkMSUwIwYDVQQDDBxzZWN1cmVkZW1vLndpY2VkLmN5cHJlc3MuY29t\n" \
    "MB4XDTE3MDcxMjEwMDQzMVoXDTIzMDEwMjEwMDQzMVowbDELMAkGA1UEBhMCSU4x\n" \
    "EzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoMGEludGVybmV0IFdpZGdpdHMg\n" \
    "UHR5IEx0ZDElMCMGA1UEAwwcc2VjdXJlZGVtby53aWNlZC5jeXByZXNzLmNvbTCB\n" \
    "nzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAwFec9pyXjZ1IIQpl4H3hL+L994iC\n" \
    "ADE31oZHS+jERINxmw12AO3CRZR5yNUmjRUP4HMFjBJWI4qNBHGhkQ1dWG45VOhl\n" \
    "GlYBT8Ch1TU+hwzqYCsjjbRTuNjU+Mrj+ykz65ZjqLvtsJATNJX2e8evQSq8saCK\n" \
    "8qwNXAgLZc6uiG0CAwEAATANBgkqhkiG9w0BAQsFAAOBgQArp7Cu/LzSHXLR9azO\n" \
    "CRCWo8NkZTJ/QbOZJpu6eGU5x10Y+SMJAUBQ5P7OQLnK5yumnCIdmA+OEFKfstym\n" \
    "a77DNL/jd/RqhJ/vvkEsBkwDmrVMX9JEzcE+MKuvLdMjb767SL9jiee5hFo1HGoI\n" \
    "xZfeZ3Y1ENfAb/gWeuENecyCAg==\n" \
    "-----END CERTIFICATE-----\n";

/* Following are the standard header fields in use
 * by the websoket protocol
 * Additional header fields may be added
 *  GET /chat HTTP/1.1
    Host: server.example.com
    Upgrade: websocket
    Connection: Upgrade
    Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==
    Origin: http://example.com
    Sec-WebSocket-Protocol: chat, superchat
    Sec-WebSocket-Version: 13
 */

static const wiced_websocket_client_url_protocol_t websocket_header =
{
   .request_uri            = "?encoding=text",
   .host                   = "echo.websocket.org", // connection to hosts with an ip-address(like internal servers) will also work.
   //.host                   = "192.168.1.49",
   .origin                 = "172.16.61.228",
   .sec_websocket_protocol =  NULL,
};

static wiced_websocket_t    websocket;
#ifdef USE_WEBSOCKET_SECURE_CLIENT
static wiced_tls_identity_t  tls_identity;
#endif

/******************************************************
 *               Function Definitions
 ******************************************************/

void application_start( )
{

    wiced_init( );

    wiced_rtos_init_semaphore( &received_message_event );

    if ( wiced_network_up( WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL ) != WICED_SUCCESS )
    {
        WPRINT_APP_INFO( ( "\r\nFailed to bring up network\r\n" ) );
        return;
    }

#ifdef USE_WEBSOCKET_SECURE_CLIENT
    wiced_result_t result = WICED_ERROR;
    /* Initialize the root CA certificate */
    result = wiced_tls_init_root_ca_certificates( httpbin_root_ca_certificate, strlen(httpbin_root_ca_certificate) );
    if ( result != WICED_SUCCESS )
    {
        WPRINT_APP_INFO( ( "Error: Root CA certificate failed to initialize: %u\n", result) );
        return;
    }
#endif

    /* Initialise the websocket */
    wiced_websocket_initialise( &websocket );

    /* Set up websocket transmit and receive buffers */
    wiced_websocket_initialise_tx_frame( &tx_frame, FINAL_FRAME, WEBSOCKET_TEXT_FRAME, strlen(tx_buffer), tx_buffer, BUFFER_LENGTH );

    wiced_websocket_initialise_rx_frame( &rx_frame, rx_buffer, BUFFER_LENGTH );

    /*register websocket callbacks */
    wiced_websocket_register_callbacks( &websocket, on_open_websocket_callback, on_close_websocket_callback, on_message_websocket_callback, on_error_websocket_callback  );

    WPRINT_APP_INFO( ( "\r\nConnecting to server...\r\n\r\n" ) );

    /* Establish a websocket connection with the server */
    WPRINT_APP_INFO(("Trying to connect\n"));
#ifdef USE_WEBSOCKET_SECURE_CLIENT
    WEBSOCKET_SNIPPET_VERIFY( wiced_websocket_secure_connect( &websocket, &websocket_header, &tls_identity ) );
#else
    WEBSOCKET_SNIPPET_VERIFY( wiced_websocket_connect( &websocket, &websocket_header ) );
#endif

    WPRINT_APP_INFO( ( "\r\nConnected to server \r\n\r\n" ) );

    WPRINT_APP_INFO( ( "Sending text frame with data 0123456789. Expecting echo response...\r\n\r\n" ) );

    /* Send the text data to the server */
    WEBSOCKET_SNIPPET_VERIFY( wiced_websocket_send( &websocket, &tx_frame ) );

    wiced_rtos_get_semaphore( &received_message_event, WICED_WAIT_FOREVER );

    /* prepare PING frame */
    wiced_websocket_initialise_tx_frame( &tx_frame, FINAL_FRAME, WEBSOCKET_PING, strlen( PING_MESSAGE ), PING_MESSAGE, strlen( PING_MESSAGE ) );

    WPRINT_APP_INFO( ( "Sending ping frame. Expecting PONG response...\r\n\r\n" ) );

    /* send PING frame */
    WEBSOCKET_SNIPPET_VERIFY( wiced_websocket_send( &websocket, &tx_frame ) );

    wiced_rtos_get_semaphore( &received_message_event, WICED_WAIT_FOREVER );

    WPRINT_APP_INFO( ( "\r\nWebsocket client demo complete. Closing connection.\r\n") );

exit:

    wiced_websocket_close( &websocket, WEBSOCKET_CLOSE_STATUS_CODE_NORMAL, "Closing session\r\n" );

    wiced_websocket_uninitialise( &websocket );

}

wiced_result_t on_open_websocket_callback( void* websocket )
{
    UNUSED_PARAMETER( websocket );

    WPRINT_APP_INFO( ( "\r\nConnection open received\r\n " ) );

    return WICED_SUCCESS;
}

wiced_result_t on_close_websocket_callback( void* websocket )
{
    UNUSED_PARAMETER( websocket );

    WPRINT_APP_INFO( ( "\r\nConnection closed received\r\n " ) );

    return WICED_SUCCESS;
}

wiced_result_t on_message_websocket_callback( void* websocket )
{

    WPRINT_APP_INFO( ( "[Message received]\r\n ") );

    wiced_websocket_receive( (wiced_websocket_t*)websocket, &rx_frame );

    WPRINT_APP_INFO( ( "Server returned:\r\n") );

    switch( rx_frame.payload_type )
    {
        case WEBSOCKET_TEXT_FRAME :
        {
            WPRINT_APP_INFO( ( "\tFrame Type: TEXT FRAME\r\n" ) );
            WPRINT_APP_INFO( ( "\tFrame Data: %s\r\n", (char*)rx_frame.payload ) );
            break;
        }

        case WEBSOCKET_PONG:
        {
            WPRINT_APP_INFO( ( "\tFrame Type: PONG\r\n" ) );
            WPRINT_APP_INFO( ( "\tFrame Data: %s\r\n", (char*)rx_frame.payload ) );
            break;
        }

        case WEBSOCKET_PING:
        {
            char* pong_payload = NULL;
            uint8_t nr_bytes = 0;
            WPRINT_APP_INFO( ( "\tFrame Type: PING\r\n" ) );
            WPRINT_APP_INFO( ( "\tFrame Data: %s\r\n", (char*)rx_frame.payload ) );
            nr_bytes = strlen(rx_frame.payload) + 1;
            pong_payload = malloc(nr_bytes);
            if(pong_payload == NULL)
            {
                WPRINT_APP_INFO(("[App] Error sending PONG\n"));
                break;
            }

            memcpy( pong_payload, rx_frame.payload, nr_bytes );
            wiced_websocket_initialise_tx_frame( &tx_frame, FINAL_FRAME, WEBSOCKET_PONG, (nr_bytes- 1), pong_payload, nr_bytes );
            WICED_VERIFY( wiced_websocket_send(websocket, &tx_frame) );
            WPRINT_APP_INFO((" \tSending PONG done\r\n"));
            free(pong_payload);
            break;
        }

        case WEBSOCKET_CONNECTION_CLOSE:
        {
            WPRINT_APP_INFO( ( "\tFrame Type: CONNECTION CLOSE\r\n" ) );

            wiced_websocket_unregister_callbacks( (wiced_websocket_t*)websocket );
            break;
        }

        case WEBSOCKET_BINARY_FRAME:
        {
            WPRINT_APP_INFO( ( "\tFrame Type: BINARY\r\n" ) );
            break;
        }

        case WEBSOCKET_CONTINUATION_FRAME:
        {
            WPRINT_APP_INFO( ( "\tFrame Type: CONTINUATION\r\n" ) );
            break;
        }

        default:
            WPRINT_APP_INFO( ( "\tFrame Type: RESERVED\r\n" ) );
            break;
    }

    printf("\r\n");

    wiced_rtos_set_semaphore( &received_message_event );

    return WICED_SUCCESS;
}

wiced_result_t on_error_websocket_callback( void* websocket )
{
    WPRINT_APP_INFO( ( "\r\nError number received = %d\r\n", ((wiced_websocket_t*)websocket)->error_type ) );

    return WICED_SUCCESS;
}
