/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * HTTPS Webclient Application
 *
 */

#include "wiced.h"
#include "wiced_tcpip.h"
#include "wiced_tls.h"
#include "http.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define HTTPS_PORT           443
#define HTTPS_SERVER_ADDRESS "google.com"
#define HTTP_GET_REQ_LENGTH  100
#define BUFFER_LENGTH        1000

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

static uint8_t resp_buffer_get[ BUFFER_LENGTH ];

/******************************************************
 *               Function Definitions
 ******************************************************/

void application_start( void )
{
    char *http_get_req = "GET / HTTP/1.1\r\n\r\n";
    wiced_ip_address_t ip_address;
    wiced_result_t result;

    /* Initialise the device */
    wiced_init( );

    /* Configure the device */
    /* wiced_configure_device( app_config ); */ /* Config bypassed in local makefile and wifi_config_dct.h */

    /* Bring up the network on the STA interface */
    WPRINT_APP_INFO(( "Connect to the network\n" ));
    result = wiced_network_up( WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL );
    if ( result != WICED_SUCCESS )
    {
        WPRINT_APP_INFO(("Unable to bring up network connection\n"));
        return;
    }

    WPRINT_APP_INFO( ( "Resolving IP address of https server...\n" ) );
    result = wiced_hostname_lookup( HTTPS_SERVER_ADDRESS, &ip_address, 10000, WICED_STA_INTERFACE );
    if ( result == WICED_ERROR || ip_address.ip.v4 == 0 )
    {
        WPRINT_APP_INFO(("Error in resolving DNS\n"));
        return;
    }
    WPRINT_APP_INFO(("Resolved Server IP: %u.%u.%u.%u\n\n", (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 24),
                    (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 16),
                    (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 8),
                    (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 0)));

    /* HTTPS Request */
    result = wiced_https_get( &ip_address, http_get_req, resp_buffer_get, BUFFER_LENGTH, NULL );
    if ( result == WICED_SUCCESS )
    {
        WPRINT_APP_INFO (( "\nhttp_get: Get returned:\n" ));
        WPRINT_APP_INFO (( "%s\n", resp_buffer_get ));
    }
    else
    {
        WPRINT_APP_INFO (( "\nhttp_get: Get failed: %u\n", result ));
    }
}
