/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * Handles http proxy data and sends to cloud server using HTTP and HTTPS protocol stack
 *
 */

#include "wiced.h"
#include "wiced_network.h"
#include "wiced_tcpip.h"
#ifdef CLOUD_PROTO_HTTPS
#include "wiced_tls.h"
#endif
#include "gateway.h"
#include "cloudconfig.h"
#include "httpproxy.h"
#include "http.h"

extern wiced_ip_address_t ip_address;

static uint8_t resp_buffer_post[ BUFFER_LENGTH ];
static uint8_t resp_buffer_observe[ BUFFER_LENGTH ];
static uint8_t hostname[ 50 ];
static wiced_thread_t httpobserve_handler;
static iot_gateway_device_t obsrv_msg;

static wiced_result_t sensors_http_ovbserve( uint16_t connid, char *urioption, char *header, char *payload );
static wiced_result_t sensors_http_post( uint16_t connid, char *urioption, char *header, char *payload );
static wiced_result_t create_http_observe_thread( iot_gateway_device_t *device );
static wiced_result_t http_observe( iot_gateway_device_t *device );

wiced_result_t http_process_request( iot_gateway_device_t *device );

void http_observe_thread( uint32_t arg )
{

    WPRINT_APP_DEBUG (("http_observe_thread created\n"));

    while ( 1 )
    {
        /* check for network link */
        if ( wiced_network_is_ip_up( WICED_STA_INTERFACE ) != WICED_TRUE )
        {
            wiced_rtos_delay_milliseconds( 100 );
            continue;
        }

        for ( int i = 0; i < IOT_GATEWAY_GATTS_MAX_CONN; i++ )
        {
            if ( iot_gateway_devices[ i ].conn_id == obsrv_msg.conn_id )
            {
                break;
            }
        }

        sensors_http_ovbserve( obsrv_msg.conn_id, (char*) obsrv_msg.http_proxy_info.http_proxy_uri, (char*) obsrv_msg.http_proxy_info.http_proxy_header, (char*) obsrv_msg.http_proxy_info.http_proxy_body );

    }
}

wiced_result_t create_http_observe_thread( iot_gateway_device_t *device )
{
    uint32_t stack_size = 2 * 1024;
    wiced_result_t result;

    result = wiced_rtos_create_thread( &httpobserve_handler, 1, "Http Obsrv Thrd", http_observe_thread, stack_size, device );
    if ( result )
    {
        WPRINT_APP_INFO( ( "thread init failed !!!!!!!!!\n" ) );
    }

    return result;
}

wiced_result_t http_observe( iot_gateway_device_t *device )
{
    wiced_result_t ret;

    if ( httpobserve_handler.thread != NULL )
    {
        WPRINT_APP_INFO( ("Already one observe is running !!!\n") );
        return WICED_ERROR;
    }

    /* update the new observe value. but dont create any extra thread*/
    memcpy( &obsrv_msg, device, sizeof(iot_gateway_device_t) );

    /* check for duplicate call*/
    /* for each client there will be a one observe thread*/
    ret = create_http_observe_thread( device );

    return ret;
}

wiced_result_t sensors_http_ovbserve( uint16_t connid, char *urioption, char *header, char *payload )
{
    wiced_result_t result;
    char http_post_req[ HTTP_POST_REQ_LENGTH ];
    char *observe;
    char *statusline = "\"status\":\"ok\"";
    char *pattern_start = ",\"";
    char *pattern_end = "\"]}";
    char *lineend;

    sprintf( http_post_req, "POST %s\r\n%s\r\n\r\n%s\r\n\r\n", urioption, header, payload );

    WPRINT_APP_DEBUG (("http post request:length=%d\n%s\r\n",strlen(http_post_req),http_post_req));

    result = wiced_http_get( &ip_address, http_post_req, resp_buffer_observe, BUFFER_LENGTH );
    if ( result == WICED_SUCCESS )
    {
        WPRINT_APP_DEBUG (( "Observe: Server returned\n%s", resp_buffer_observe )) ;

        /*parse response*/
        observe = strnstr( (char*) resp_buffer_observe, strlen( (char*) resp_buffer_observe ), statusline, strlen( statusline ) );
        if ( observe != NULL )
        {
            observe = strnstr( (char*) resp_buffer_observe, strlen( (char*) resp_buffer_observe ), "result", strlen( "result" ) );
            if ( observe == NULL )
            {
                WPRINT_APP_INFO( ("Invalid payload: Not found [%s]\n","result") );
                return WICED_ERROR;
            }
            observe = strnstr( observe, strlen( observe ), pattern_start, strlen( pattern_start ) );
            if ( observe == NULL )
            {
                WPRINT_APP_INFO( ("Invalid payload: Not found [%s]\n",pattern_start) );
                return WICED_ERROR;
            }
            observe = observe + strlen( pattern_start );
            lineend = strnstr( observe, strlen( observe ), pattern_end, strlen( pattern_end ) );
            if ( lineend == NULL )
            {
                WPRINT_APP_INFO( ("Invalid payload: Not found [%s]\n",pattern_end) );
                return WICED_ERROR;
            }
            *lineend = '\0';
            WPRINT_APP_INFO( ("\nobserve = %s\n",observe) );
            gateway_gatt_send_notification( connid, HANDLE_HTTP_PROXY_SERVICE_OBSERVE_VAL, strlen( observe ) + 1, (uint8_t*) observe );
        }
    }
    else
    {
        WPRINT_APP_INFO (("Observe: Get failed: %u\n", result ));
    }

    return WICED_SUCCESS;
}

wiced_result_t sensors_http_post( uint16_t connid, char *urioption, char *header, char *payload )
{
    wiced_result_t result;
    char http_post_req[ HTTP_POST_REQ_LENGTH ];
    char *status_code;
    char *lineend;
#ifdef CLOUD_PROTO_HTTPS
    static const char geo_trust_g3_ca_certificate[] =
    "-----BEGIN CERTIFICATE-----\n"
    "MIIETzCCAzegAwIBAgIDAjpvMA0GCSqGSIb3DQEBCwUAMEIxCzAJBgNVBAYTAlVT\n"
    "MRYwFAYDVQQKEw1HZW9UcnVzdCBJbmMuMRswGQYDVQQDExJHZW9UcnVzdCBHbG9i\n"
    "YWwgQ0EwHhcNMTMxMTA1MjEzNjUwWhcNMjIwNTIwMjEzNjUwWjBEMQswCQYDVQQG\n"
    "EwJVUzEWMBQGA1UEChMNR2VvVHJ1c3QgSW5jLjEdMBsGA1UEAxMUR2VvVHJ1c3Qg\n"
    "U1NMIENBIC0gRzMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDjvn4K\n"
    "hqPPa209K6GXrUkkTdd3uTR5CKWeop7eRxKSPX7qGYax6E89X/fQp3eaWx8KA7UZ\n"
    "U9ulIZRpY51qTJEMEEe+EfpshiW3qwRoQjgJZfAU2hme+msLq2LvjafvY3AjqK+B\n"
    "89FuiGdT7BKkKXWKp/JXPaKDmJfyCn3U50NuMHhiIllZuHEnRaoPZsZVP/oyFysx\n"
    "j0ag+mkUfJ2fWuLrM04QprPtd2PYw5703d95mnrU7t7dmszDt6ldzBE6B7tvl6QB\n"
    "I0eVH6N3+liSxsfQvc+TGEK3fveeZerVO8rtrMVwof7UEJrwEgRErBpbeFBFV0xv\n"
    "vYDLgVwts7x2oR5lAgMBAAGjggFKMIIBRjAfBgNVHSMEGDAWgBTAephojYn7qwVk\n"
    "DBF9qn1luMrMTjAdBgNVHQ4EFgQU0m/3lvSFP3I8MH0j2oV4m6N8WnwwEgYDVR0T\n"
    "AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAQYwNgYDVR0fBC8wLTAroCmgJ4Yl\n"
    "aHR0cDovL2cxLnN5bWNiLmNvbS9jcmxzL2d0Z2xvYmFsLmNybDAvBggrBgEFBQcB\n"
    "AQQjMCEwHwYIKwYBBQUHMAGGE2h0dHA6Ly9nMi5zeW1jYi5jb20wTAYDVR0gBEUw\n"
    "QzBBBgpghkgBhvhFAQc2MDMwMQYIKwYBBQUHAgEWJWh0dHA6Ly93d3cuZ2VvdHJ1\n"
    "c3QuY29tL3Jlc291cmNlcy9jcHMwKQYDVR0RBCIwIKQeMBwxGjAYBgNVBAMTEVN5\n"
    "bWFudGVjUEtJLTEtNTM5MA0GCSqGSIb3DQEBCwUAA4IBAQCg1Pcs+3QLf2TxzUNq\n"
    "n2JTHAJ8mJCi7k9o1CAacxI+d7NQ63K87oi+fxfqd4+DYZVPhKHLMk9sIb7SaZZ9\n"
    "Y73cK6gf0BOEcP72NZWJ+aZ3sEbIu7cT9clgadZM/tKO79NgwYCA4ef7i28heUrg\n"
    "3Kkbwbf7w0lZXLV3B0TUl/xJAIlvBk4BcBmsLxHA4uYPL4ZLjXvDuacu9PGsFj45\n"
    "SVGeF0tPEDpbpaiSb/361gsDTUdWVxnzy2v189bPsPX1oxHSIFMTNDcFLENaY9+N\n"
    "QNaFHlHpURceA1bJ8TCt55sRornQMYGbaLHZ6PPmlH7HrhMvh+3QJbBo+d4IWvMp\n"
    "zNSS\n"
    "-----END CERTIFICATE-----\n";

    result = wiced_tls_init_root_ca_certificates( geo_trust_g3_ca_certificate,(uint32_t)strlen(geo_trust_g3_ca_certificate) );
    if ( result != WICED_SUCCESS )
    {
        WPRINT_APP_INFO (("Error: Root CA certificate failed to initialize: %u\n", result));
        return -1;
    }
    else
    {
        WPRINT_APP_INFO (("#### ROOT CA INSTALLED: %u\n", result));
    }

#endif

    sprintf( http_post_req, "POST %s\r\n%s\r\n\r\n%s\r\n\r\n", urioption, header, payload );

    WPRINT_APP_DEBUG (("http post request:length=%d\n%s\r\n",strlen(http_post_req),http_post_req));
#ifdef CLOUD_PROTO_HTTPS
    result = wiced_https_get( &ip_address, http_post_req, resp_buffer_post, BUFFER_LENGTH, NULL);
#else
    result = wiced_http_get( &ip_address, http_post_req, resp_buffer_post, BUFFER_LENGTH );
#endif
    if ( result == WICED_SUCCESS )
    {
        WPRINT_APP_DEBUG (( "Server returned\n%s\n", resp_buffer_post )) ;
        status_code = strnstr( (char*) resp_buffer_post, strlen( (char*) resp_buffer_post ), "HTTP/1.1 ", strlen( "HTTP/1.1 " ) );
        status_code = status_code + strlen( "HTTP/1.1 " );
        lineend = strnstr( status_code, strlen( status_code ), " ", strlen( " " ) );
        *lineend = '\0';

        WPRINT_APP_INFO( ("\nhttp_post : status code = %s\n", status_code) );

        gateway_gatt_send_notification( connid, HANDLE_HTTP_PROXY_SERVICE_HTTP_STATUS_CODE_VAL, strlen( status_code ) + 1, (uint8_t*) status_code );
    }
    else
    {
        WPRINT_APP_INFO (( "\nhttp_post: Get failed: %u\n", result ));
    }

    return WICED_SUCCESS;
}

wiced_result_t http_process_request( iot_gateway_device_t *device )
{
    uint8_t *host;
    uint8_t *lineend = NULL;
    uint8_t name[ 100 ];
    http_proxy_info_t *msg = &( device->http_proxy_info );
    uint8_t retry_count = 0;

    if ( msg->http_proxy_control != HTTP_REQ_NULL )
    {
        /*extract host name from header*/
        host = (uint8_t*) strnstr( (char*) msg->http_proxy_header, strlen( (char*) msg->http_proxy_header ), "Host:", strlen( "Host:" ) );
        host = host + strlen( "Host:" );
        if ( host != NULL )
        {
            memcpy( name, host, strlen( (char*) host ) );
            lineend = (uint8_t*) strnstr( (char*) name, strlen( (char*) name ), "\r\n", strlen( "\r\n" ) );
        }
        if ( lineend != NULL )
        {
            *lineend = '\0';
        }

        WPRINT_APP_DEBUG (("Host name : %s\n",name));

        if ( strcmp( (char*) hostname, (char*) name ) )
        {
            memcpy( hostname, name, strlen( (char*) name ) );

            while ( (retry_count < MAX_RETRY_COUNT_FOR_DNS_LOOKUP) && wiced_hostname_lookup( (char*) hostname, &ip_address, 5000, WICED_STA_INTERFACE ) != WICED_SUCCESS ) //NX_WAIT_FOREVER
            {
                retry_count++;
            }

            if(retry_count == MAX_RETRY_COUNT_FOR_DNS_LOOKUP)
            {
                WICED_BT_TRACE( "DNS Lookup failed..\n" );
                return WICED_TIMEOUT;
            }

            WPRINT_APP_INFO( ( "1-->Server is at %u.%u.%u.%u\n", (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 24), (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 16), (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 8), (uint8_t)(GET_IPV4_ADDRESS(ip_address) >> 0) ) );

        }
    }

    WPRINT_APP_DEBUG (("msg->http_proxy_control = %d\n",msg->http_proxy_control));

    switch ( msg->http_proxy_control )
    {
        case HTTP_REQ_POST:
            sensors_http_post( device->conn_id, (char*) msg->http_proxy_uri, (char*) msg->http_proxy_header, (char*) msg->http_proxy_body );
            break;
        case HTTP_REQ_OBSERVE:
            http_observe( device );
            break;
        default:
            WPRINT_APP_DEBUG (("http_proxy_control event %d not handled\n", msg->http_proxy_control));
            break;
    }

    return WICED_SUCCESS;

}
