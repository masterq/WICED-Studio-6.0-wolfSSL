/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary oF
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */


/** @file
 *
 * adc Measure Application
 *
 * This application measures the adc (Channel 1) of the WICED evaluation
 * board and displays voltage values on a local webpage.
 *
 * The application demonstrates the following features ...
 *  - Wi-Fi client mode to display adc measurements on the webpage
 *  - Webserver
 *  - Gedday mDNS / DNS-SD Network Discovery
 *
 * Application Operation
 * This section provides a description of the application flow and usage.
 * The app runs in a thread, the entry point is application_start()
 *
 *    Startup
 *      - Initialise the device
 *      - Check the device has a valid configuration in the DCT
 *      - Setup peripheral ADC 
 *      - Start the network interface to connect the device to the network
 *      - Set the local time from a time server on the internet
 *      - Setup a timer to take adc measurements
 *      - Start a webserver to display adc & setpoint values
 *      - Start Gedday to advertise the webserver on the network
 *
 *    Usage
 *
 *        The current time, date, adc are published to
 *        a webpage by a local webserver. 
 *
 *
 */


#include "adc_measure.h"
#include <math.h>
#include "wiced.h"
#include "http_server.h"
#include "sntp.h"
#include "gedday.h"
#include "resources.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define MAX_NTP_ATTEMPTS        (3)


/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    float         adc;
    wiced_mutex_t mutex;
} setpoint_t;

typedef struct
{
    adc_reading_t adc_readings[MAX_ADC_READINGS];
    uint16_t              adc_reading_index;
    uint16_t              last_sent_adc_index;
    int16_t               last_sample;
    wiced_mutex_t         mutex;
} temp_data_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

static wiced_result_t take_adc_reading       ( void* arg );


static int32_t        process_adc_update     ( const char* url_path, const char* url_parameters, wiced_http_response_stream_t* stream, void* arg, wiced_http_message_body_t* http_data );

/******************************************************
 *               Variable Definitions
 ******************************************************/

static temp_data_t         adc_data;
static wiced_http_server_t http_server;
static wiced_timed_event_t adc_timed_event;
static START_OF_HTTP_PAGE_DATABASE(web_pages)
    ROOT_HTTP_PAGE_REDIRECT("/apps/adc_measure/main.html"),
    { "/apps/adc_measure/main.html",    "text/html",                WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_apps_DIR_adc_measure_DIR_main_html, },
    { "/temp_report.html",               "text/html",                WICED_DYNAMIC_URL_CONTENT,   .url_content.dynamic_data   = {process_adc_update, 0 }, },
    { "/images/favicon.ico",             "image/vnd.microsoft.icon", WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_images_DIR_favicon_ico, },
    { "/scripts/general_ajax_script.js", "application/javascript",   WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_scripts_DIR_general_ajax_script_js, },
    { "/images/cypresslogo.png",         "image/png",                WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_images_DIR_cypresslogo_png, },
    { "/images/cypresslogo_line.png",    "image/png",                WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_images_DIR_cypresslogo_line_png, },
    { "/styles/buttons.css",             "text/css",                 WICED_RESOURCE_URL_CONTENT,  .url_content.resource_data  = &resources_styles_DIR_buttons_css, },
END_OF_HTTP_PAGE_DATABASE();

wiced_worker_thread_t   button_worker_thread;

/******************************************************
 *               Function Definitions
 ******************************************************/
extern void wiced_adc_start(void);
extern wiced_result_t wiced_adc_sample(int16_t *);
/*
 * Application start/main function.It initializes, brings up network and starts web page
 */
void application_start( void )
{
    uint16_t max_sockets = 10;

    wiced_init( );

    /* Initialise adc data. adc data are shared among multiple threads; therefore a mutex is required */
    memset( &adc_data, 0, sizeof( adc_data ) );
    wiced_rtos_init_mutex( &adc_data.mutex );

    wiced_adc_start();

    /* Disable roaming to other access points */
    wiced_wifi_set_roam_trigger( -99 ); /* -99dBm ie. extremely low signal level */

    /* Bringup the network interface */
    if(wiced_network_up( WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL )==WICED_SUCCESS)
    {

     /* Start automatic time synchronisation and synchronise once every day. */
    sntp_start_auto_time_sync( 1 * DAYS );

    /* Setup a timed event that will take a measurement */
    wiced_rtos_register_timed_event( &adc_timed_event, WICED_HARDWARE_IO_WORKER_THREAD, take_adc_reading, 1 * SECONDS, 0 );

    /* Start web server to display current adc & setpoint */
    wiced_http_server_start( &http_server, 80, max_sockets, web_pages, WICED_STA_INTERFACE, DEFAULT_URL_PROCESSOR_STACK_SIZE );

    /* Advertise webpage services using Gedday */
    gedday_init( WICED_STA_INTERFACE, "wiced-adc-measurement" );
    gedday_add_service( "adc_measurement web server", "_http._tcp.local", 80, 300, "" );
    }
    else
    {
    	WPRINT_APP_INFO(("\n\rError connecting to Wi-Fi access point\n\r"));
    }
}




/*
 * Takes a adc reading
 */
static wiced_result_t take_adc_reading( void* arg )
{
    UNUSED_PARAMETER(arg);

    wiced_rtos_lock_mutex( &adc_data.mutex );
    if ( wiced_adc_sample(  &adc_data.last_sample ) != WICED_SUCCESS )
    {
        wiced_rtos_unlock_mutex( &adc_data.mutex );
        return WICED_ERROR;
    }


    /* Get the current ISO8601 time */
    wiced_time_get_iso8601_time( &adc_data.adc_readings[adc_data.adc_reading_index].timestamp );

    /* Create sample string */
    xively_u16toa( adc_data.last_sample / 10, adc_data.adc_readings[adc_data.adc_reading_index].sample, 2 );

    xively_u16toa( adc_data.last_sample % 10, &adc_data.adc_readings[adc_data.adc_reading_index].sample[2], 1 );

    if ( ( ++adc_data.adc_reading_index ) == MAX_ADC_READINGS )
    {
        adc_data.adc_reading_index = 0;
    }

    wiced_rtos_unlock_mutex( &adc_data.mutex );

    return WICED_SUCCESS;
}


/*
 * Updates adc information in the web page
 */
static int32_t process_adc_update( const char* url_path, const char* url_parameters, wiced_http_response_stream_t* stream, void* arg, wiced_http_message_body_t* http_data )
{
    wiced_iso8601_time_t* curr_time;
    float temp_count;
    float temp_volt;
    char  temp_char_buffer[6];
    int   temp_char_buffer_length;

    UNUSED_PARAMETER( url_path );
    UNUSED_PARAMETER( http_data );

    wiced_rtos_lock_mutex( &adc_data.mutex );

    if ( adc_data.adc_reading_index == 0 )
    {
        curr_time = &adc_data.adc_readings[MAX_ADC_READINGS - 1].timestamp;
    }
    else
    {
        curr_time = &adc_data.adc_readings[adc_data.adc_reading_index - 1].timestamp;
    }

    /* Update adc report with the most recent adc reading */
    temp_count        = (float) adc_data.last_sample;
    temp_volt     = temp_count / 4096.0*3.3;


    wiced_rtos_unlock_mutex( &adc_data.mutex );

    /* Write the time */
    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_time_start );
    wiced_http_response_stream_write( stream, curr_time->hour, sizeof(curr_time->hour)   +
                                                     sizeof(curr_time->colon1) +
                                                     sizeof(curr_time->minute) +
                                                     sizeof(curr_time->colon2) +
                                                     sizeof(curr_time->second) );
    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_time_end );

    /* Write the date */
    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_date_start );
    wiced_http_response_stream_write(stream, curr_time->year, sizeof(curr_time->year)  +
                                                    sizeof(curr_time->dash1) +
                                                    sizeof(curr_time->month) +
                                                    sizeof(curr_time->dash2) +
                                                    sizeof(curr_time->day) );

    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_date_end );

    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_temp_start );
    temp_char_buffer_length = sprintf(temp_char_buffer, "%.0f", temp_count);
    wiced_http_response_stream_write(stream, temp_char_buffer, temp_char_buffer_length );
    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_temp_mid );
    temp_char_buffer_length = sprintf(temp_char_buffer, "%.3f", temp_volt);
    wiced_http_response_stream_write(stream, temp_char_buffer, temp_char_buffer_length );
    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_temp_end );

    wiced_http_response_stream_write_resource( stream, &resources_apps_DIR_adc_measure_DIR_data_html_page_end );

    return 0;
}
