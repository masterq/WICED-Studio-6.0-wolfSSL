/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary oF
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *
 * A simple application that scans, stores AP & pass phrase in DCT, joins an AP based on configuration in Device Configuration Table (DCT).
 * config <STA name and pass key>
 *       - adds AP settings to DCT
 *   join
 *       - Joins station set in DCT
 *   print_config
 *       - prints current wifi configuration
 *   scan
 *       - prints current wifi configuration
 *   ping <ip address>
 *       - pings wifi configuration
 *   disconnect
 *       - disconnects from Access Point (AP)
 */

#include "wiced.h"
#include "command_console.h"
#include "wiced_management.h"
#include "command_console_ping.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define MAX_LINE_LENGTH  (128)
#define MAX_HISTORY_LENGTH (20)
#define MAX_NUM_COMMAND_TABLE  (8)


static char line_buffer[MAX_LINE_LENGTH];
static char history_buffer_storage[MAX_LINE_LENGTH * MAX_HISTORY_LENGTH];

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/

static wiced_result_t print_wifi_config_dct ( void );
wiced_result_t scan_result_handler( wiced_scan_handler_result_t* malloced_scan_result );

/******************************************************
 *               Variable Definitions
 ******************************************************/

typedef struct
{
    wiced_semaphore_t   semaphore;      /* Semaphore used for signaling scan complete */
    uint32_t            result_count;   /* Count to measure the total scan results    */
} app_scan_data_t;

#define DIAGNOSTICS_COMMANDS \
{ "config",  wifi_config,  1, NULL, NULL, "<STA name and pass key>", "adds AP settings to DCT" }, \
{ "join",  join,  0, NULL, NULL,"", "Joins station set in DCT" }, \
{ "print_config",  print_wifi,  0, NULL, NULL,"",  "prints current wifi configuration" }, \
{ "scan",  scan_wifi,  0, NULL, NULL,"",  "prints current wifi configuration" }, \
{ "ping",  ping,  1, NULL, NULL,"<ip address>",  "pings wifi configuration" }, \
{ "disconnect", disconnect, 0, NULL, NULL, "",             "Dis-connects from AP" },

/*
 * Holder function to call printing wifi configuration in dct..
 */
int print_wifi(int argc, char* argv[])
{
print_wifi_config_dct();
return 0;
}

/*
 * Holder function to bring up client inerface
 */
int join( int argc, char* argv[])
{

	/* Bring up the STA (client) interface ------------------------------------------------------- */
        wiced_network_up(WICED_STA_INTERFACE, WICED_USE_EXTERNAL_DHCP_SERVER, NULL);
        return 0;

}

/*
 * Holder function to disconnect network
 */
int disconnect( int argc, char* argv[])
{
	wiced_network_down(WICED_STA_INTERFACE);
    return 0;
}

/*
 * Holder function to store AP name and Pass phrase in DCT
 */
int wifi_config( int argc, char* argv[])
{
    char* ap_name = argv[1];
    char* ap_passkey = argv[2];
    platform_dct_wifi_config_t*     dct_wifi_config          = NULL;

    /* get the read lock */
    wiced_dct_read_lock( (void**) &dct_wifi_config, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *dct_wifi_config ) );

    /* store AP name and Pass phrase in DCT*/
    strcpy((char *)&dct_wifi_config->stored_ap_list[0].details.SSID.value[0],ap_name);
    dct_wifi_config->stored_ap_list[0].details.SSID.length = strlen(ap_name);
    strcpy((char *)&dct_wifi_config->stored_ap_list[0].security_key[0],ap_passkey);
    dct_wifi_config->stored_ap_list[0].security_key_length = strlen(ap_passkey);
    wiced_dct_write( (const void*) dct_wifi_config, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );

    /* release the read lock */
    wiced_dct_read_unlock( dct_wifi_config, WICED_TRUE );
    return WICED_SUCCESS;
}

/*
 * Holder function to scan for Wi-Fi Access Points (AP)
 */
int scan_wifi(int argc, char *argv[]){
        wiced_time_t    scan_start_time;
        wiced_time_t    scan_end_time;
        app_scan_data_t scan_data;

        /* Initialize the semaphore that will tell us when the scan is complete */
        wiced_rtos_init_semaphore(&scan_data.semaphore);
        scan_data.result_count = 0;
        WPRINT_APP_INFO( ( "Waiting for scan results...\n" ) );
        WPRINT_APP_INFO( ("  # Type  BSSID             RSSI  Rate Chan  Security         SSID\n" ) );
        WPRINT_APP_INFO( ("----------------------------------------------------------------------------------------------\n" ) );

        /* Start the scan */
        wiced_time_get_time(&scan_start_time);
        wiced_wifi_scan_networks(scan_result_handler, &scan_data );

        /* Wait until scan is complete */
        wiced_rtos_get_semaphore(&scan_data.semaphore, WICED_WAIT_FOREVER);
        wiced_time_get_time(&scan_end_time);

        WPRINT_APP_INFO( ("\nScan complete in %lu milliseconds\n", (unsigned long )(scan_end_time - scan_start_time) ) );

        /* Clean up */
        wiced_rtos_deinit_semaphore(&scan_data.semaphore);


        return 0;
}

/*
 * Callback function to handle scan results
 */
wiced_result_t scan_result_handler( wiced_scan_handler_result_t* malloced_scan_result )
{
    /* Validate the input arguments */
    wiced_assert("Bad args", malloced_scan_result != NULL);

    if ( malloced_scan_result != NULL )
    {
        app_scan_data_t* scan_data  = (app_scan_data_t*)malloced_scan_result->user_data;

        malloc_transfer_to_curr_thread( malloced_scan_result );

        if ( malloced_scan_result->status == WICED_SCAN_INCOMPLETE )
        {
            wiced_scan_result_t* record = &malloced_scan_result->ap_details;

            WPRINT_APP_INFO( ( "%3ld ", scan_data->result_count ) );
            print_scan_result(record);
            scan_data->result_count++;
        }
        else
        {
            wiced_rtos_set_semaphore( &scan_data->semaphore );
        }

        free( malloced_scan_result );
    }
    return WICED_SUCCESS;
}




static const command_t init_commands[] =
{
    DIAGNOSTICS_COMMANDS
    CMD_TABLE_END
};


/*
 * Application start/main function.
 */
void application_start( )
{

    /* Initialise the device */
    wiced_init( );

    WPRINT_APP_INFO( ( "\r\nType help to know more about commands ...\r\n" ) );
    command_console_init( STDIO_UART, MAX_LINE_LENGTH, line_buffer, MAX_HISTORY_LENGTH, history_buffer_storage, " " );
    console_add_cmd_table( init_commands );

}

/*
 * Print's Wi-Fi configuration in DCT
 */
static wiced_result_t print_wifi_config_dct( void )
{
    platform_dct_wifi_config_t* dct_wifi_config = NULL;

    if ( wiced_dct_read_lock( (void**) &dct_wifi_config, WICED_FALSE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *dct_wifi_config ) ) != WICED_SUCCESS )
    {
        return WICED_ERROR;
    }


    WPRINT_APP_INFO( ( "\r\n----------------------------------------------------------------\r\n\r\n") );

    /* Wi-Fi Config Section */
    WPRINT_APP_INFO( ( "Wi-Fi Config Section \r\n") );
    WPRINT_APP_INFO( ( "    device_configured               : %d \r\n", dct_wifi_config->device_configured ) );
    WPRINT_APP_INFO( ( "    stored_ap_list[0]  (SSID)       : %s \r\n", dct_wifi_config->stored_ap_list[0].details.SSID.value ) );
    WPRINT_APP_INFO( ( "    stored_ap_list[0]  (Passphrase) : %s \r\n", dct_wifi_config->stored_ap_list[0].security_key ) );
    WPRINT_APP_INFO( ( "    soft_ap_settings   (SSID)       : %s \r\n", dct_wifi_config->soft_ap_settings.SSID.value ) );
    WPRINT_APP_INFO( ( "    soft_ap_settings   (Passphrase) : %s \r\n", dct_wifi_config->soft_ap_settings.security_key ) );
    WPRINT_APP_INFO( ( "    config_ap_settings (SSID)       : %s \r\n", dct_wifi_config->config_ap_settings.SSID.value ) );
    WPRINT_APP_INFO( ( "    config_ap_settings (Passphrase) : %s \r\n", dct_wifi_config->config_ap_settings.security_key ) );
    WPRINT_APP_INFO( ( "    country_code                    : %c%c%d \r\n", ((dct_wifi_config->country_code) >>  0) & 0xff,
                                                                            ((dct_wifi_config->country_code) >>  8) & 0xff,
                                                                            ((dct_wifi_config->country_code) >> 16) & 0xffff));
    WPRINT_APP_INFO( ( "    DCT mac_address                 : ") );
    print_mac_address( (wiced_mac_t*) &dct_wifi_config->mac_address );
    WPRINT_APP_INFO( ("\r\n") );

    wiced_dct_read_unlock( dct_wifi_config, WICED_FALSE );

    return WICED_SUCCESS;
}

