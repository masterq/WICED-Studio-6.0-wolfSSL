#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 # Cypress Semiconductor Corporation. All Rights Reserved.
 # This software, including source code, documentation and related
 # materials ("Software"), is owned by Cypress Semiconductor Corporation
 # or one of its subsidiaries ("Cypress") and is protected by and subject to
 # worldwide patent protection (United States and foreign),
 # United States copyright laws and international treaty provisions.
 # Therefore, you may use this Software only as provided in the license
 # agreement accompanying the software package from which you
 # obtained this Software ("EULA").
 # If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 # non-transferable license to copy, modify, and compile the Software
 # source code solely for use in connection with Cypress's
 # integrated circuit products. Any reproduction, modification, translation,
 # compilation, or representation of this Software except as specified
 # above is prohibited without the express written permission of Cypress.
 #
 # Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 # EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 # WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 # reserves the right to make changes to the Software without notice. Cypress
 # does not assume any liability arising out of the application or use of the
 # Software or any product or circuit described in the Software. Cypress does
 # not authorize its products for use in any products where a malfunction or
 # failure of the Cypress product may reasonably be expected to result in
 # significant property damage, injury or death ("High Risk Product"). By
 # including Cypress's product in a High Risk Product, the manufacturer
 # of such system or application assumes all risk of such use and in doing
 # so agrees to indemnify Cypress against all liability.
#

#$(info Processing wiced_cgs.mk LINK_OUTPUT_FILE: $(LINK_OUTPUT_FILE))
#$(info LINK_OUTPUT_SUFFIX: $(LINK_OUTPUT_SUFFIX))
#$(LINK_OUTPUT_FILE:.elf=.cgs): $(LINK_OUTPUT_FILE)
#	$(info Processed CGS ... )

default: help

# cgs recipe
.PHONY:

# Include all toolchain makefiles - one of them will handle the architecture
include $(MAKEFILES_PATH)/wiced_toolchain_$(TOOLCHAIN_NAME).mk
#include $(MAKEFILES_PATH)/wiced_elf.mk

include $(SOURCE_ROOT)WICED/platform/MCU/$(HOST_MCU_FAMILY)/makefiles/tools.mk
include platforms/$(subst .,/,$(PLATFORM))/$(PLATFORM).mk
# Basic environment for SPAR builds
SDK_ROOT_DIR    := ../../..

ELF_OUT := $(LINK_OUTPUT_FILE)
BIN_OUT_DIR = $(OUTPUT_DIR)

PLATFORM_DIR := $(SOURCE_ROOT)platforms/$(PLATFORM)
PATCH_ROOT_DIR  = $(SOURCE_ROOT)WICED/platform/MCU/$(HOST_MCU_FAMILY)
PATCH_DIR       = $(PATCH_ROOT_DIR)/patches/$(BT_CHIP_REVISION)
PATCH_LIBS_DIR  = $(PATCH_ROOT_DIR)/libraries
CGS_LIST       += $(PATCH_DIR)/patch.cgs $(PLATFORM_DIR)/$(PLATFORM_CONFIGS)
PATCH_ELF       = $(OUTPUT_DIR)/binary/patch_$(HOST_MCU_FAMILY).elf
ELF_LIST       += $(PATCH_ELF)
PATCH_LST		= $(PATCH_DIR)/patch.lst
PATCH_SYMDEF    = $(PATCH_DIR)/patch.symdefs
ifeq ($(BUILD_TYPE),debug)
APP_LD_SCRIPT   = $(PATCH_ROOT_DIR)/GCC/$(BT_CHIP_REVISION)/20739$(BT_CHIP_REVISION)_$(SPAR_IN)_debug.ld
else
APP_LD_SCRIPT   = $(PATCH_ROOT_DIR)/GCC/$(BT_CHIP_REVISION)/20739$(BT_CHIP_REVISION)_$(SPAR_IN).ld
endif

ifdef PLATFORM_SUPPORTS_OC_FLASH
FS_IMAGE           := $(OUTPUT_DIR)/filesystem.bin
STAGING_DIR          := $(OUTPUT_DIR)/resources/Staging/
endif
ifdef PLATFORM_SUPPORTS_OC_FLASH
EXTRA_POST_BUILD_TARGETS += $(FS_IMAGE) $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).hex
#EXTRA_POST_BUILD_TARGETS += $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).hex
else
EXTRA_POST_BUILD_TARGETS += $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).hex
endif
EXTRA_PRE_BUILD_TARGETS += $(PATCH_ELF)
BROADCOM_INTERNAL :=no

#$(info CGS_LIST: $(CGS_LIST))

# Now add the optional libraries and patches this app needs to the final objs to link in.
#APP_PATCHES_AND_LIBS :=btstack_lib.a
#$(info PATCH_LIBS_DIR: $(PATCH_LIBS_DIR))
#$(info APP_PATCHES_AND_LIBS: $(APP_PATCHES_AND_LIBS))
PATCHES_LIBS += $(addprefix $(PATCH_LIBS_DIR)/,$(APP_PATCHES_AND_LIBS))
#$(info PATCHES_LIBS: $(PATCHES_LIBS))

# Include the platform/chip specific makefile
#include $(PATCH_ROOT_DIR)/$(CHIP)$(REVNUM).mk
include $(PATCH_ROOT_DIR)/20739$(BT_CHIP_REVISION).mk



# Include generic ARM core options
#include $(TC)/make_$(CHIP_CORE)_$(TC).mk
include $(SOURCE_ROOT)WICED/platform/MCU/$(HOST_MCU_FAMILY)/makefiles/gcc/make_cortex-m4_gcc.mk



################################################################################
# CGS generation, foo.elf.text.hex etc. -> spar.cgs
################################################################################
SPAR_CGS_TARGET = $(ELF_OUT:.elf=.cgs)

# Make the spar-setup call CGS element
SPAR_SETUP_CALL_CGS = $(ELF_OUT:.elf=.elf.spar_setup_call.cgs)
# cgs file for each section we need to extract
SECTIONS_CGS_LIST += $(patsubst %, $(ELF_OUT)%.cgs, $(UNCOMPRESSED_SECTIONS))
SECTIONS_CGS_LIST += $(patsubst %, $(ELF_OUT)%.ccgs, $(COMPRESSED_SECTIONS))
SECTIONS_CGS_LIST += $(patsubst %, $(ELF_OUT)%.ocgs, $(OVERLAID_SECTIONS))



# Include defalt platform.cgs from the current directory if
# nothing is specified. This may come in from WICED SDK platform directory.
PLATFORM_CGS_PATH ?= platform.cgs


#$(BUILD_STRING): $(OBJS) $(OUTPUT_DIR)/Modules/lib_installer.o #$(SPAR_CGS_TARGET)
#	$(info CLEANED_BUILD_STRING cgs.mk: $(OBJS))


OUTPUT_NAME:=$(BUILD_STRING)
PLATFORM_BOOTP:= 20739_OCF_$(BT_CHIP_REVISION).btp
PLATFORM_MINIDRIVER  := uart_main_$(BT_CHIP_REVISION).hex
PLATFORM_BAUDRATE  := 115200
# DCT build takes the COMPILER_SPECIFIC_DEBUG_CFLAGS flag even in release build,
# DCT files needs to be built with always "-O0" option, where as other .c files
# should be built with "-Os" option,so when -debug is enabled  in order to fix
# this we have used the order in which the COMPILER_SPECIFIC_DEBUG_CFLAGS and
# COMPILER_SPECIFIC_STANDARD_CFLAGS are used while building the DCT files and
# other .c file in wiced_elf.mk, since the compiler takes more latest -O option
# in the command, so now for DCT it will be "-Os -O0" and for other .c files it
# will be "-O0 -Os" in the command.So we need to take care here when there is some
# compiler flags order change in wiced_elf.mk.
ifeq ($(BUILD_TYPE),debug)
COMPILER_SPECIFIC_DEBUG_CFLAGS += -Os
COMPILER_SPECIFIC_STANDARD_CFLAGS += -O0
endif

# DCT build takes the COMPILER_SPECIFIC_DEBUG_CFLAGS flag even in release build,
# DCT files needs to be built with always "-O0" option, where as other .c files
# should be built with "-Os" option,so when -debug is enabled  in order to fix
# this we have used the order in which the COMPILER_SPECIFIC_DEBUG_CFLAGS and
# COMPILER_SPECIFIC_STANDARD_CFLAGS are used while building the DCT files and
# other .c file in wiced_elf.mk, since the compiler takes more latest -O option
# in the command, so now for DCT it will be "-Os -O0" and for other .c files it
# will be "-O0 -Os" in the command.So we need to take care here when there is some
# compiler flags order change in wiced_elf.mk.
ifeq ($(BUILD_TYPE),debug)
COMPILER_SPECIFIC_DEBUG_CFLAGS += -Os
COMPILER_SPECIFIC_STANDARD_CFLAGS += -O0
endif

#include platforms/$(subst .,/,$(PLATFORM))/$(PLATFORM).mk

$(if $(WLAN_CHIP),,$(error No WLAN_CHIP has been defined))
$(if $(WLAN_CHIP_REVISION),,$(error No WLAN_CHIP_REVISION has been defined))

WIFI_IMAGE_NAME		:= $(WLAN_CHIP)$(WLAN_CHIP_REVISION).bin
DCT_IMAGE_NAME		:= $(OUTPUT_DIR)/DCT.bin
DOWNLOAD_DCT_HEX	:= $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING)_append_dct.hex
DOWNLOAD_APP_HEX    := $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING).hex
ifdef PLATFORM_SUPPORTS_OC_FLASH
DOWNLOAD_FILESYSTEM_HEX	:= $(OUTPUT_DIR)/binary/$(CLEANED_BUILD_STRING)_append_filesystem.hex
endif

ifdef PLATFORM_SUPPORTS_OC_FLASH
DOWNLOAD_FILE_NAME := $(DOWNLOAD_FILESYSTEM_HEX)
else
DOWNLOAD_FILE_NAME := $(DOWNLOAD_DCT_HEX)
endif


# Override the BD_ADDR parameter if provided on the command line.
ifeq ($(BT_DEVICE_ADDRESS),random)
BT_DEVICE_ADDRESS_OVERRIDE := -O DLConfigBD_ADDRBase:$(shell $(call CONV_SLASHES,$(PERL)) -e '$(PERL_ESC_DOLLAR)r=int(rand(32768));printf "$(CHIP)$(CHIP_REV)0%04X",$(PERL_ESC_DOLLAR)r;')
else
ifneq ($(BT_DEVICE_ADDRESS),)
BT_DEVICE_ADDRESS_OVERRIDE := -O DLConfigBD_ADDRBase:$(BT_DEVICE_ADDRESS)
endif
endif

 $(PATCH_ELF) : $(PATCH_DIR)/patch.elf
	$(OBJCOPY) --strip-symbols=$(PATCH_DIR)/sym_remove.txt --redefine-syms=$(PATCH_DIR)/sym_rename.txt $< $(PATCH_ELF)

$(DOWNLOAD_APP_HEX): $(SPAR_CGS_TARGET)
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Converting ELF to ASM...
	$(QUIET)$(OBJDUMP)  --disassemble $(@:.hex=.elf) > $(@:.hex=.asm)
	$(QUIET)$(ECHO) Converting CGS to HEX...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)platforms/$(PLATFORM)/*.hdf $(OUTPUT_DIR)/binary/
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM) $(BT_DEVICE_ADDRESS_OVERRIDE) -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I $@ --cgs-files $< > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log

$(DOWNLOAD_DCT_HEX): $(DOWNLOAD_APP_HEX)
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(call CONV_SLASHES,$(APPEND_TO_INTEL_HEX)) -I $(DCT_OCF_START_ADDR) $(DCT_IMAGE_NAME) $< $@ > $(OUTPUT_DIR)/binary/append_dct.log 2>&1 && $(ECHO) $(QUOTES_FOR_ECHO)dct append complete$(QUOTES_FOR_ECHO)  || $(ECHO) $(QUOTES_FOR_ECHO)**** dct append failed ****$(QUOTES_FOR_ECHO)

ifdef PLATFORM_SUPPORTS_OC_FLASH
$(DOWNLOAD_FILESYSTEM_HEX): $(DOWNLOAD_DCT_HEX)
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(call CONV_SLASHES,$(APPEND_TO_INTEL_HEX)) -I $(FILESYSTEM_OCF_START_ADDR) $(FS_IMAGE) $< $@ > $(OUTPUT_DIR)/binary/append_filesystem.log 2>&1 && $(ECHO) $(QUOTES_FOR_ECHO)filesystem append complete$(QUOTES_FOR_ECHO)  || $(ECHO) $(QUOTES_FOR_ECHO)**** filesystem append failed ****$(QUOTES_FOR_ECHO)

endif

platform_ota:
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(ECHO) Creating OTA images...
	$(QUIET)$(COPY) -f $(SOURCE_ROOT)platforms/$(PLATFORM)/*.hdf build/$(OUTPUT_NAME)/
	$(QUIET)$(call CONV_SLASHES,$(CGS_FULL_NAME)) -D $(SOURCE_ROOT)platforms/$(PLATFORM) -O DLConfigFixedHeader:0 -O ConfigDSLocation:0 -B $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -I build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex --cgs-files build/$(OUTPUT_NAME)/binay/$(OUTPUT_NAME).cgs > build/$(OUTPUT_NAME)/cgs2hex.log 2>&1 && $(ECHO) Conversion complete || $(ECHO) $(QUOTES_FOR_ECHO)**** Conversion failed ****$(QUOTES_FOR_ECHO)  && $(call CONV_SLASHES,$(PERL)) $(AFT) 5 build/$(OUTPUT_NAME)/cgs2hex.log
	$(QUIET)$(call CONV_SLASHES,$(HEX_TO_BIN_FULL_NAME)) build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.hex build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin
	$(QUIET)$(call CONV_SLASHES,$(PERL)) -e '$$s=-s "build/$(OUTPUT_NAME)/$(OUTPUT_NAME).ota.bin";printf "OTA image footprint in NV is %d bytes",$$s;'
	$(QUIET)$(ECHO_BLANK_LINE)
	$(QUIET)$(call CONV_SLASHES,$(APPEND_TO_INTEL_HEX)) -I 0x543000 $(SOURCE_ROOT)resources/firmware/$(WLAN_CHIP)/$(WIFI_IMAGE_NAME) $@  $(OUTPUT_DIR)/binary/$(OUTPUT_NAME)_append_wifi.hex > $(OUTPUT_DIR)/binary/append_wifi.log 2>&1 && $(ECHO) wifi append complete  || $(ECHO) $(QUOTES_FOR_ECHO)**** wifi append failed ****
	$(QUIET)$(ECHO_BLANK_LINE)

download: $(DOWNLOAD_FILE_NAME)
	$(QUIET)$(ECHO_BLANK_LINE)
#	$(QUIET)$(eval UART:=$(shell $(CAT) < com_port.txt))
	$(QUIET)$(if $(UART), \
	        $(ECHO) Downloading application... && $(call CONV_SLASHES,$(CHIPLOAD_FULL_NAME)) -BLUETOOLMODE -PORT $(UART) -BAUDRATE $(PLATFORM_BAUDRATE) -MINIDRIVER $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_MINIDRIVER) -BTP $(SOURCE_ROOT)platforms/$(PLATFORM)/$(PLATFORM_BOOTP) -CONFIG $< -LOGTO build/$(OUTPUT_NAME)/logto.log > build/$(OUTPUT_NAME)/download.log 2>&1 \
	        		&& $(ECHO) Download complete && $(ECHO_BLANK_LINE) && $(ECHO) $(QUOTES_FOR_ECHO)Application running$(QUOTES_FOR_ECHO) \
	        		|| $(ECHO) $(QUOTES_FOR_ECHO)****Download failed ****$(QUOTES_FOR_ECHO) && $(call CONV_SLASHES,$(PERL)) $(AFT) 4 build/$(OUTPUT_NAME)/download.log , \
	        	$(ECHO) Download failed. This version of the SDK only supports download to BCM20735 and BCM20739 family of devices)

run:
	$(QUIET)$(ECHO) Ignoring 'run' - Target will reset after download...

$(OUTPUT_DIR)/Modules/lib_installer.o: $(OUTPUT_DIR)/Modules/lib_installer.c
#	$(info Generated lib_installer.c)
	$(CC) $(COMPILER_SPECIFIC_COMP_ONLY_FLAG) $(COMPILER_SPECIFIC_DEPS_FLAG) $(COMPILER_SPECIFIC_STANDARD_CFLAGS)  $(COMPILER_SPECIFIC_RELEASE_CFLAGS) -o $@ $< $(COMPILER_SPECIFIC_STDOUT_REDIRECT)

$(OUTPUT_DIR)/Modules/lib_installer.c_opts: #$(CONFIG_FILE)
#	$(QUIET)$$(call WRITE_FILE_CREATE, $$@, $(subst $(COMMA),$$(COMMA), $(COMPILER_SPECIFIC_COMP_ONLY_FLAG) $(COMPILER_SPECIFIC_DEPS_FLAG) $(COMPILER_SPECIFIC_STANDARD_CFLAGS) $(if $(findstring debug,$($(1)_BUILD_TYPE)), $(COMPILER_SPECIFIC_DEBUG_CFLAGS), $(COMPILER_SPECIFIC_RELEASE_CFLAGS)) $($(1)_CFLAGS) $($(1)_INCLUDES) $($(1)_DEFINES) $(WICED_SDK_INCLUDES) $(WICED_SDK_DEFINES)))
#	$(QUIET)$$(call WRITE_FILE_CREATE, $$@, $(subst $(COMMA),$$(COMMA), $(COMPILER_SPECIFIC_COMP_ONLY_FLAG) $(COMPILER_SPECIFIC_DEPS_FLAG) $(COMPILER_SPECIFIC_STANDARD_CFLAGS)  $(COMPILER_SPECIFIC_RELEASE_CFLAGS) ))

#Final CGS Target: All the CGS files are merged here
$(SPAR_CGS_TARGET): $(CGS_LIST) $(SECTIONS_CGS_LIST) $(SPAR_SETUP_CALL_CGS)
	$(QUIET)-$(XMD) $(@D)
ifeq ($(HOST_OS),Win32)
	$(QUIET)$(ECHO) Making combined CGS image on $(HOST_OS)
	$(call PERL_FIX_QUOTES, $(PERL) -e "foreach(@ARGV){open(FL,$$_);while(<FL>){print;}close(FL);}" $^) > $@
else
	$(QUIET)$(ECHO) Making combined CGS image on $(HOST_OS)
	$(PERL) -e 'foreach(@ARGV){open(FL,$$_);while(<FL>){print;}close(FL);}'  $^ > $@
endif
	$(QUIET)$(ECHO) OK, made $(CURDIR)/$@. MD5 sum is:
	$(QUIET)$(MD5SUM) $@
#	$(QUIET)$(XRM) $(ELF_OUT)*.cgs $(ELF_OUT_SYM) $(ELF_OUT_BIN)
	$(QUIET)$(ECHO)




#include $(TC)/rules_$(CHIP_CORE)_$(TC).mk
include $(SOURCE_ROOT)WICED/platform/MCU/$(HOST_MCU_FAMILY)/makefiles/gcc/rules_cortex-m4_gcc.mk
#$(info OBJS: $(OBJS))
