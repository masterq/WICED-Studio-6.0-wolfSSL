/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 * Cypress Semiconductor Corporation. All Rights Reserved.
 * 
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

/** @file
 *  Deep-sleep support functions.
 *
 */

#pragma once

#include <stdint.h>
#include "platform_config.h"
#include "platform_mcu_peripheral.h"
#include "spar_utils.h"

#ifdef __cplusplus
extern "C"
{
#endif

/******************************************************
 *             Macros
 ******************************************************/

#ifndef PLATFORM_LOW_POWER_HEADER_INCLUDED
#error "Platform header file must not be included directly, Please use wiced_deep_sleep.h instead."
#endif

#define WICED_SLEEP_STR_EXPAND( name )                      #name

#define WICED_SLEEP_SECTION_NAME_EVENT_HANDLER( name )      ".sleep_event_handlers."WICED_SLEEP_STR_EXPAND( name )

#define WICED_SLEEP_SECTION_NAME_EVENT_REGISTRATION( name ) ".sleep_event_registrations."WICED_SLEEP_STR_EXPAND( name )

/* Definition of MACROs from wiced_sleep.h */

#define WICED_DEEP_SLEEP_IS_WARMBOOT( ) \
    platform_mcu_powersave_is_warmboot( )

#ifdef APPS_DEEP_SLEEP_READY
/* Include these MACRO definition only when app is deep sleep ready.
 * Else default definition would be picked from wiced_deep_sleep.h
 */

#define WICED_DEEP_SLEEP_IS_ENABLED( )                           1

#define WICED_DEEP_SLEEP_SAVED_VAR( var ) \
        PLACE_DATA_IN_RETENTION_RAM var

#endif


/******************************************************
 *             Constants
 ******************************************************/

/******************************************************
 *             Enumerations
 ******************************************************/

/******************************************************
 *             Structures
 ******************************************************/

/******************************************************
 *             Variables
 ******************************************************/

/******************************************************
 *             Function declarations
 ******************************************************/

//void SECTION( WICED_DEEP_SLEEP_SECTION_NAME_EVENT_HANDLER( wiced_deep_sleep_call_event_handlers ) ) wiced_deep_sleep_call_event_handlers( wiced_deep_sleep_event_type_t event );

wiced_bool_t SECTION( WICED_SLEEP_SECTION_NAME_EVENT_HANDLER( platform_deep_sleep_copy_to_fw_memory ) ) platform_deep_sleep_copy_to_fw_memory( uint8_t* fw_mem_ptr, uint32_t aon_size );
wiced_bool_t SECTION( WICED_SLEEP_SECTION_NAME_EVENT_HANDLER( platform_deep_sleep_copy_from_fw_memory ) ) platform_deep_sleep_copy_from_fw_memory( uint8_t* fw_mem_ptr, uint32_t aon_size );
uint32_t SECTION( WICED_SLEEP_SECTION_NAME_EVENT_HANDLER( platform_deep_sleep_required_mem_size ) ) platform_deep_sleep_required_mem_size(void);


#ifdef __cplusplus
} /* extern "C" */
#endif
