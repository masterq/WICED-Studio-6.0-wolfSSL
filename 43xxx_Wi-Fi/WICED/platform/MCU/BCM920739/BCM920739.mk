#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of 
 # Cypress Semiconductor Corporation. All Rights Reserved.
 # This software, including source code, documentation and related
 # materials ("Software"), is owned by Cypress Semiconductor Corporation
 # or one of its subsidiaries ("Cypress") and is protected by and subject to
 # worldwide patent protection (United States and foreign),
 # United States copyright laws and international treaty provisions.
 # Therefore, you may use this Software only as provided in the license
 # agreement accompanying the software package from which you
 # obtained this Software ("EULA").
 # If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 # non-transferable license to copy, modify, and compile the Software
 # source code solely for use in connection with Cypress's
 # integrated circuit products. Any reproduction, modification, translation,
 # compilation, or representation of this Software except as specified
 # above is prohibited without the express written permission of Cypress.
 #
 # Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 # EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 # WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 # reserves the right to make changes to the Software without notice. Cypress
 # does not assume any liability arising out of the application or use of the
 # Software or any product or circuit described in the Software. Cypress does
 # not authorize its products for use in any products where a malfunction or
 # failure of the Cypress product may reasonably be expected to result in
 # significant property damage, injury or death ("High Risk Product"). By
 # including Cypress's product in a High Risk Product, the manufacturer
 # of such system or application assumes all risk of such use and in doing
 # so agrees to indemnify Cypress against all liability.
#

NAME = BCM920739

# Host architecture is ARM Cortex M4
HOST_ARCH := ARM_CM4

HEX_OUTPUT_SUFFIX := .hex
CGS_OUTPUT_SUFFIX := .cgs
SPAR_CRT_SETUP := $(notdir $(APP))_spar_crt_setup
SPAR_APP_SETUP := application_setup
GLOBAL_DEFINES += SPAR_CRT_SETUP=$(SPAR_CRT_SETUP)
GLOBAL_DEFINES += SPAR_APP_SETUP=$(SPAR_APP_SETUP)
NO_BUILD_BOOTLOADER:=1
NO_BOOTLOADER_REQUIRED:=1
# Host MCU alias for OpenOCD
HOST_OPENOCD := BCM920739
# Only ThredX RTOS and NetX_Duo supported
VALID_OSNS_COMBOS := ThreadX-NetX_Duo

USE_LIBC_PRINTF ?= 1

GLOBAL_INCLUDES += . \
                   .. \
                   ../.. \
                   ../../include \
                   ../../$(HOST_ARCH) \
                   ../../$(HOST_ARCH)/CMSIS \
                   peripherals \
                      ../../../../libraries/utilities/ring_buffer \
                   ./inc/$(BT_CHIP_REVISION) \
                   WAF \
                   WWD \
                   ../../../../libraries/drivers/spi_flash

#GLOBAL_INCLUDES  += ../../../../libraries/drivers/bluetooth/BTE
#GLOBAL_INCLUDES  += ../../../../libraries/drivers/bluetooth/BTE/WICED
#GLOBAL_INCLUDES  += ../../../../libraries/drivers/bluetooth/BTE/Components/stack/include
#GLOBAL_INCLUDES  += ../../../../libraries/drivers/bluetooth/BTE/Projects/bte/main
#GLOBAL_INCLUDES  += ../../../../libraries/drivers/bluetooth/include
GLOBAL_INCLUDES  += inc/$(BT_CHIP_REVISION)/stack/include_wiced
GLOBAL_INCLUDES  += inc/$(BT_CHIP_REVISION)/stack/include
GLOBAL_INCLUDES  += inc/$(BT_CHIP_REVISION)/stack

GLOBAL_INCLUDES +=  inc/$(BT_CHIP_REVISION)/hal \
                    inc/$(BT_CHIP_REVISION)/internal \
                    inc/$(BT_CHIP_REVISION)/internal/chip \


# Global defines
GLOBAL_DEFINES  := USE_STDPERIPH_DRIVER \
                   _$(HOST_MCU_PART_NUMBER)_ \
                   CPU_CM4 \

           __CORE__=__ARM7EM__ \
                   __FPU_PRESENT=0 \
                   __FPU_USED=0

ifneq ($(TOOLCHAIN_NAME),IAR)
GLOBAL_DEFINES    += __ARM7EM__ \
                  __VER__=5000000
endif

#ThreadX defines when debug build is enabled
ifeq ($(BUILD_TYPE),debug)
include $(SOURCE_ROOT)WICED/platform/MCU/BCM920739/ThreadX_Debug.mk
endif


ifeq ($(TOOLCHAIN_NAME),GCC)
GLOBAL_CXXFLAGS += $$(CPU_CXXFLAGS)  $$(ENDIAN_CXXFLAGS_LITTLE)
GLOBAL_ASMFLAGS += $$(CPU_ASMFLAGS)  $$(ENDIAN_ASMFLAGS_LITTLE)
GLOBAL_CFLAGS   += $$(CPU_CFLAGS)    $$(ENDIAN_CFLAGS_LITTLE)
GLOBAL_CFLAGS   += -DSPAR_CRT_SETUP=$(APP)_spar_crt_setup -DSPAR_APP_SETUP=$(SPAR_APP_SETUP) -DUSE_BOOL_UINT32
GLOBAL_CFLAGS   += -Dsprintf=__2sprintf -Dsnprintf=__2snprintf
ifneq ($(USE_LIBC_PRINTF),1)
GLOBAL_CFLAGS += -Dprintf=WICED_BT_TRACE
endif
GLOBAL_DEFINES += PLATFORM_TRACE
endif
GLOBAL_DEFINES += WICED_USE_PLATFORM_ALLOCATED_POOL

GLOBAL_DEFINES += NO_SUPPORT_FOR_HIGHSPEED_MODE

# Components
ifneq ($(TOOLCHAIN_NAME),)
ifeq ($(TOOLCHAIN_NAME),GCC)
#libc related platform specific implementation
$(NAME)_COMPONENTS += MCU/BCM920739/GCC_libc
else
#platform common GCC libc related implementation
$(NAME)_COMPONENTS += $(TOOLCHAIN_NAME)
endif
endif

$(NAME)_COMPONENTS += MCU/BCM920739/peripherals
$(NAME)_COMPONENTS += inputs/gpio_button
$(NAME)_COMPONENTS += utilities/ring_buffer

ifdef PLATFORM_SUPPORTS_SPI_FLASH
GLOBAL_DEFINES += WICED_PLATFORM_INCLUDES_SPI_FLASH
$(NAME)_COMPONENTS += MCU/BCM920739/peripherals/spi_flash
endif

ifdef PLATFORM_FILESYSTEM_FILEX
GLOBAL_DEFINES += FILEX_FIX
endif

# Source files
$(NAME)_SOURCES := ../wiced_platform_common.c \
                  platform_init.c \
                  platform_bt_cfg.c \
                  spar_setup.c \
                   ../platform_stdio.c \
                ../platform_resource.c \
                  ../wwd_resources.c \
                  ../platform_button.c \
                 DCT/wiced_dct_ocf.c \
                 ../wiced_dct_update.c

ifdef PLATFORM_SUPPORTS_DEEP_SLEEP
GLOBAL_DEFINES += PLATFORM_SUPPORTS_LOW_POWER_MODES
endif

ifdef PLATFORM_SUPPORTS_FILESYSTEM
GLOBAL_DEFINES += WICED_FILESYSTEM_SUPPORT
ifeq ($(PLATFORM_SUPPORTS_RESSOURCE_GENFS),1)
GLOBAL_DEFINES += USES_RESOURCE_GENERIC_FILESYSTEM
endif
ifdef PLATFORM_SUPPORTS_SPI_FLASH
$(NAME)_SOURCES += platform_filesystem.c
else
ifdef PLATFORM_SUPPORTS_OC_FLASH
GLOBAL_DEFINES += WICED_PLATFORM_INCLUDES_OCF_FS
GLOBAL_DEFINES += WICED_FILESYSTEM_OCF_START=0x45000
GLOBAL_DEFINES += WICED_FILESYSTEM_OCF_SIZE=0xBB000
GLOBAL_DEFINES += WICED_FILESYSTEM_OCF_END=WICED_FILESYSTEM_OCF_START+WICED_FILESYSTEM_OCF_SIZE
$(NAME)_SOURCES += platform_filesystem_ocf.c
endif #PLATFORM_SUPPORTS_OC_FLASH
endif #PLATFORM_SUPPORTS_SPI_FLASH
endif #PLATFORM_SUPPORTS_FILESYSTEM

$(NAME)_SOURCES += threadx_stub.c

ifeq ($(WICED_NETTYPE),ROM)
$(NAME)_SOURCES += network/Netx_Duo_5.7/nxd_rom_port.c
endif

ifneq ($(WLAN_CHIP),NONE)
$(NAME)_SOURCES += WWD/wwd_platform.c \
                 WWD/wwd_$(BUS).c
endif
#                  ../../$(HOST_ARCH)/crt0_$(TOOLCHAIN_NAME).c \
#                   ../../$(HOST_ARCH)/host_cm4.c \
#                   ../../$(HOST_ARCH)/hardfault_handler.c \
#                  strlcpy.c \
#                  strcasestr.c \
#                  gnu_getopt.c \
#                  gnu_getopt_long.c \
#                    ram_dct.c \
#                  wiced_dct_external_ram_common.c \
#                  platform_stubs.c \
#                  WWD/wwd_platform.c
#                  WWD/wwd_SPI.c
#                   ../platform_resource.c \


  #                 ../wwd_platform_separate_mcu.c \
   #                ../wwd_resources.c \
  #                 ../wiced_apps_common.c    \
  #                 ../wiced_waf_common.c    \
  #                 ../wiced_dct_internal_common.c \
  #                 platform_vector_table.c \
  #                 platform_unhandled_isr.c \
  #                 platform_filesystem.c \
  #                 WWD/wwd_platform.c \
  #                 WWD/wwd_$(BUS).c \
  #                 WAF/waf_platform.c \


# These need to be forced into the final ELF since they are not referenced otherwise
#$(NAME)_LINK_FILES := ../../$(HOST_ARCH)/crt0_$(TOOLCHAIN_NAME).o \
#                      ../../$(HOST_ARCH)/hardfault_handler.o \
#                      platform_vector_table.o

#for DCT with crc checking
$(NAME)_COMPONENTS  += utilities/crc

$(NAME)_CFLAGS = $(COMPILER_SPECIFIC_PEDANTIC_CFLAGS)
$(NAME)_CFLAGS += -Wno-unused-function

# Add maximum and default watchdog timeouts to definitions. Warning: Do not change MAX_WATCHDOG_TIMEOUT_SECONDS
MAX_WATCHDOG_TIMEOUT_SECONDS = 22
GLOBAL_DEFINES += MAX_WATCHDOG_TIMEOUT_SECONDS=$(MAX_WATCHDOG_TIMEOUT_SECONDS)

# DCT linker script
DCT_LINK_SCRIPT += $(TOOLCHAIN_NAME)/$(BT_CHIP_REVISION)/dct$(LINK_SCRIPT_SUFFIX)
GLOBAL_DEFINES += USE_BOOL_UINT32

ifeq ($(APP),bootloader)
####################################################################################
# Building bootloader
####################################################################################

#DEFAULT_LINK_SCRIPT += $(TOOLCHAIN_NAME)/bootloader$(LINK_SCRIPT_SUFFIX)
#$(NAME)_SOURCES     += WAF/waf_platform.c
#$(NAME)_LINK_FILES  += WAF/waf_platform.o
$(error ERROR: BT_CHIP is undefined1!)
else
#ifneq ($(filter sflash_write, $(APP)),)
ifneq ($(filter VALID_RAM_APPS, $(APP)),)
####################################################################################
# Building VALID_RAM_APPS
####################################################################################
#$(info  HB_IMPORTANT NOTES)
#PRE_APP_BUILDS      += bootloader
DEFAULT_LINK_SCRIPT := $(TOOLCHAIN_NAME)/app_ram$(LINK_SCRIPT_SUFFIX)
#GLOBAL_INCLUDES     += WAF ../../../../../apps/waf/bootloader/
GLOBAL_DEFINES      += __JTAG_FLASH_WRITER_DATA_BUFFER_SIZE__=16384
ifeq ($(TOOLCHAIN_NAME),IAR)
GLOBAL_LDFLAGS      += --config_def __JTAG_FLASH_WRITER_DATA_BUFFER_SIZE__=16384
endif

else
ifeq ($(USES_BOOTLOADER_OTA),1)
####################################################################################
# Building standard application to run with bootloader
####################################################################################

#PRE_APP_BUILDS      += bootloader
#DEFAULT_LINK_SCRIPT := $(TOOLCHAIN_NAME)/app_with_bootloader$(LINK_SCRIPT_SUFFIX)
#GLOBAL_INCLUDES     += WAF ../../../../../apps/waf/bootloader/
else
####################################################################################
# Building a WWD application (standalone app without bootloader and DCT)
####################################################################################

BASE_IN                           ?= rom
SPAR_IN                           ?= ram
#DEFAULT_LINK_SCRIPT := $(TOOLCHAIN_NAME)/app_no_bootloader$(LINK_SCRIPT_SUFFIX)
GLOBAL_LINK_SCRIPT := $(OUTPUT_DIR)/$(PLATFORM)_$(SPAR_IN)_proc.ld
GLOBAL_DEFINES      += WICED_DISABLE_BOOTLOADER
NO_BUILD_BOOTLOADER:=1
NO_BOOTLOADER_REQUIRED:=1
USES_BOOTLOADER_OTA:=0
endif # USES_BOOTLOADER_OTA = 1
endif # APP= sflash_write
endif # APP=bootloader
# Using a precompiled Library
#$(NAME)_PREBUILT_LIBRARY := Chinookpatch.a libnano_IAR66.a libmicronavKE_all4774IAR66.a ChinookCohoEvk.sym patch_4774_A1_RTOS.a service_system_app20.a os7m_tlv_r.a mp3decctrl/lib/mp3dec_iar.a
#endif
EXTRA_PLATFORM_MAKEFILES +=  $(SOURCE_ROOT)/WICED/platform/MCU/$(HOST_MCU_FAMILY)/makefiles/wiced_cgs.mk
EXTRA_PLATFORM_MAKEFILES +=  $(SOURCE_ROOT)/WICED/platform/MCU/$(HOST_MCU_FAMILY)/BCM920739_targets.mk
