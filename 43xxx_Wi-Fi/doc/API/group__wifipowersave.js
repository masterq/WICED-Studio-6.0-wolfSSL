var group__wifipowersave =
[
    [ "wiced_wifi_disable_powersave", "group__wifipowersave.html#gab3f0bfdd53168dd21af3182c54244b61", null ],
    [ "wiced_wifi_disable_powersave_interface", "group__wifipowersave.html#ga096caf679a3411560cc579496a53e6b3", null ],
    [ "wiced_wifi_enable_powersave", "group__wifipowersave.html#ga261fe02752907ee6366c032690f2041f", null ],
    [ "wiced_wifi_enable_powersave_interface", "group__wifipowersave.html#gacfed335cd0ea5d6baa07ed827e452013", null ],
    [ "wiced_wifi_enable_powersave_with_throughput", "group__wifipowersave.html#ga6d75a5852c8b691fa6a3fc332b2ba629", null ],
    [ "wiced_wifi_enable_powersave_with_throughput_interface", "group__wifipowersave.html#ga21e4eebd9fbab39b857b513819c06f07", null ]
];