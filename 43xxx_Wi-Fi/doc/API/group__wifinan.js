var group__wifinan =
[
    [ "wiced_nan_config_disable", "group__wifinan.html#ga797add78b2c8f1d6e3d3f3a371520060", null ],
    [ "wiced_nan_config_enable", "group__wifinan.html#gaafa64e6c007896f29c12d48bcc445f3d", null ],
    [ "wiced_wifi_register_nan_event_handler", "group__wifinan.html#gafa93ec98adb533a5339bc0cc4f3a0907", null ],
    [ "wiced_wifi_unregister_nan_event_handler", "group__wifinan.html#ga2fb347d77bcd735b393187e3c0b5bd78", null ]
];