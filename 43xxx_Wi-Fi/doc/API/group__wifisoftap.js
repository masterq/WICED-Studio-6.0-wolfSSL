var group__wifisoftap =
[
    [ "wiced_stop_ap", "group__wifisoftap.html#ga563657210072e0630139c89960fe0a0d", null ],
    [ "wiced_wifi_get_ap_client_rssi", "group__wifisoftap.html#gacb2f44ab5aeb790d5a1c0116e86db20c", null ],
    [ "wiced_wifi_get_ap_info", "group__wifisoftap.html#gaa49b6363af17e9cfebd61960a3512ce7", null ],
    [ "wiced_wifi_get_associated_client_list", "group__wifisoftap.html#gae4431e4f32510a05b23443ad1b16fec7", null ],
    [ "wiced_wifi_is_sta_link_up", "group__wifisoftap.html#ga6e83b8570122c8dd36a31c1fb5d05001", null ],
    [ "wiced_wifi_register_softap_event_handler", "group__wifisoftap.html#gab16dcf459642169520ecf50a38870f3e", null ],
    [ "wiced_wifi_start_ap_with_custom_ie", "group__wifisoftap.html#gab65820715c9c79172d978e938e3bba26", null ],
    [ "wiced_wifi_unregister_softap_event_handler", "group__wifisoftap.html#gad482437dc22c49670d054d8b45d600d9", null ]
];