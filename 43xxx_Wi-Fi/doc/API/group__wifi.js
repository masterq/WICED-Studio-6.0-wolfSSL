var group__wifi =
[
    [ "WiFi Connectivity initialization and de-initialization", "group__wificonn.html", "group__wificonn" ],
    [ "WiFi Join, Scan and Halt Functions", "group__wifijoin.html", "group__wifijoin" ],
    [ "WiFi Protected Setup", "group__wifiwps.html", "group__wifiwps" ],
    [ "WiFi Utility Functions", "group__wifiultilities.html", "group__wifiultilities" ],
    [ "WiFi Soft AP", "group__wifisoftap.html", "group__wifisoftap" ],
    [ "WiFi Radio Resource Management", "group__wifirrm.html", "group__wifirrm" ],
    [ "WiFi Neighbor Awareness Networking", "group__wifinan.html", "group__wifinan" ],
    [ "WiFi (Preferred Network Offload)", "group__wifipno.html", "group__wifipno" ],
    [ "WiFi Power Saving functions", "group__wifipowersave.html", "group__wifipowersave" ],
    [ "Packet Filter functions", "group__packet-filter.html", "group__packet-filter" ],
    [ "Keep-Alive functions", "group__keep-alive.html", "group__keep-alive" ],
    [ "WiFi Deep Sleep Functions", "group__wifideepsleep.html", "group__wifideepsleep" ],
    [ "WiFi (802.11) P2P connection functions", "group__connection__manager.html", "group__connection__manager" ]
];