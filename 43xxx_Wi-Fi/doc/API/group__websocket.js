var group__websocket =
[
    [ "wiced_websocket_close", "group__websocket.html#ga094527ae5be34863ae04c24fb0c08f7f", null ],
    [ "wiced_websocket_connect", "group__websocket.html#gac46415daf2d41c5e1611e121497a536d", null ],
    [ "wiced_websocket_initialise", "group__websocket.html#gad1fdd66e009c969d898cf91b6e7193ee", null ],
    [ "wiced_websocket_initialise_rx_frame", "group__websocket.html#gae9fbe9ab4d90eb9c57c11f0278d25878", null ],
    [ "wiced_websocket_initialise_tx_frame", "group__websocket.html#ga06610ac463edffd408b7e381b2e402de", null ],
    [ "wiced_websocket_receive", "group__websocket.html#ga06f3f5e17e7ab81c96291042e1c5547a", null ],
    [ "wiced_websocket_register_callbacks", "group__websocket.html#ga73810956a26f1e5e4de5436a542c5883", null ],
    [ "wiced_websocket_secure_connect", "group__websocket.html#ga9f8b4b066bfdf387e7e006a07b2b2096", null ],
    [ "wiced_websocket_send", "group__websocket.html#gab8cec7ae1e6ec950251749fcfc7c1851", null ],
    [ "wiced_websocket_server_start", "group__websocket.html#ga345a44a4542f77c850a5d3e05a5528fb", null ],
    [ "wiced_websocket_server_stop", "group__websocket.html#gaa23ff6160d661cf8341a61837dd40000", null ],
    [ "wiced_websocket_uninitialise", "group__websocket.html#ga44083877cb30326591cbd8c771089241", null ],
    [ "wiced_websocket_unregister_callbacks", "group__websocket.html#gaa7fbf72fb2560352d523e00ea54befbe", null ]
];