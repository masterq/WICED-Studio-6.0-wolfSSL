=====================================================================
Cypress WICED Studio
WICED Platform 20706A2 - README
=====================================================================

WICED Studio provides systems and APIs needed to build, design and
implement applications for Classic Bluetooth (BR/EDR) and Bluetooth
Low Energy (BLE) devices.

Major features of this WICED Studio platform include:

- Bluetooth stack included the ROM.
- BT stack and profile level APIs for embedded BT application development.
- WICED HCI protocol to simplify host/MCU application development.
- APIs and drivers to access on board peripherals like SPI, UART, 
  ADC, PWM, Keyscan and IR HW blocks.
- Bluetooth protocols include GAP, GATT, SMP, RFCOMM, SDP,
  AVDTP, AVTCP and OBEX.
- Bluetooth profiles include A2DP source and sink, AVRCP controller
  and target, HF and AG, HID device and host, HOGP, PBAP, SPP.
- BR/EDR embedded sample apps for BT profiles.
- BLE embedded sample apps for BLE client and server, beacon, AMS, ANCS, 
  HID over GATT, Serial port profile over GATT.
- Support for Apple HomeKit API, library and sample applications.
- Support for Apple iAP2 protocol and sample applications.
- Support for BLE Mesh (Bluetooth SIG Certified QDID 35920)
- MCU sample application running on host OS for controlling embedded
  BT applications over WICED HCI protocol.
- Sample apps for hardware interfaces including ADC, PUART, 
  GPIO, PWM.
- Support for Over-The-Air (OTA) upgrade.
- BTSpy application for viewing embedded BT application and 
  HCI protocol traces.
- Manufacturing tools to verify RF performance (mbt and wmbt). 
- Support for 20706A2 platform.
- Segger J-Link debugger using J-Link GDB server and GCC GDB client.
- Test tool for automated testing. 

 
The WICED Studio release is structured as follows:
apps               : Example & Test Applications
doc                : API & Reference Documentation, Eval Board &
                     Module Schematics
drivers            : Drivers for the USB serial converter
include            : WICED API, constants, and defaults
libraries          : Bluetooth profile and protocol libraries
platforms          : Configuration files and information for supported 
                     hardware platforms
test               : Tools provided for automation testing
tools, wiced_tools : Build tools, compilers, programming tools etc.
WICED              : Core WICED components
README.txt         : This file
version.txt        : Version of WICED Studio
 
Getting Started
---------------------------------------------------------------------
If you are unfamiliar with WICED Studio, please refer to the WICED
Studio Kit Guide located here:
20706-A2_Bluetooth/doc/CYW92070xV3_EVAL-Kit-Guide.pdf.  
The WICED Studio Kit Guide documents the process to setup a
computer for use with WICED Studio and a WICED Evaluation Board. 

For Mac OS, make sure to download the latest FTDI drivers from
ftdichip.com (especially for Mac OS 10.10 or below).

WICED Studio includes lots of sample applications in the
20706-A2_Bluetooth/apps directory.  See the README.txt in the apps
directory for more description of the apps.

To obtain a complete list of build commands and options double click
the "help" item in the Make Target pane.

To compile, download and run an application on the CYW920706WCDEVAL
platform, connect the 20706 board to the computer with a USB cable, and 
double click the Make Target for the application, for example
"demo.hello_sensor-CYW920706WCDEVAL download". 

Platform implementations are available in the 20706-A2_Bluetooth/
Platforms directory.

Supported Features
---------------------------------------------------------------------
Application Features
 * Bluetooth application
   * RFCOMM/SPP
   * A2DP source and sink
   * AVRCP controller and target
   * Hands-free AG and HF
   * BT and BLE HID Device (HOGP)
   * BT and BLE HID Host (HOGP)
   * BLE Client and Server
   * BLE Mesh   
   * BLE beacon
   * Apple ANCS, AMS   
   * Apple HomeKit and iAP2 support (requires add-in package, contact
     Cypress support for details)
   * OBEX library and PBAP client profile.
   * Serial over GATT profile.
 * Peripheral interfaces
   * GPIO
   * Timer (Software)
   * PWM
   * UART (two instances - one for application download and another
     for application use).
   * SPI (two instances - one for serial flash and another for 
     application use).
   * I2C (master only).
   * RTC (Real Time Clock)
   * Keyscan
   * ADC (12 bit)
 * Generic profile level abstraction API
 * API to access NV storage areas.

* WICED BT Application Framework
   * OTA firmware upgrade
   * Overlay support to load code from NV storage on demand (NV storage
     dependent latency and power).

Toolchains
 * GNU make
 * ARM RealView debugger
 * Segger J-Link debugger
 * GNU ARM tool chain

Hardware Platforms
 * CYW920706WCDEVAL: Cypress 20706A2 based evaluation board.


Known Limitations & Notes
---------------------------------------------------------------------
  * File Permissions
      In Linux, WICED Studio files are installed using the default
      permissions for the current user. Users may wish to change the
      access permissions on the WICED Studio files. This can be done
      either on a one-time basis using 'chmod', or more permanently
      for all user programs using the 'umask' command in a shell
      startup script such as .bashrc or /etc/launchd-user.conf
         eg. At a prompt  : $WICED-Studio> chmod -R g+w
         eg. In ~/.bashrc : umask u=rwx,g=rwx,o=
  * Programming and Debugging
      Programming is currently enabled with Cypress download tools
      included with WICED Studio. Debugging is enabled by ARM
      RealView with Serial Wire Debug interface. Debugging is also
      enabled with Segger J-Link debug probes.
  * Application download via USB-serial/serial port and application
      mode are mutually exclusive. The serial port must be
      disconnected from the board for the application to initialize
  * ARM RealView is not currently supported out of the box with
      WICED Studio.  


Tools
---------------------------------------------------------------------
The GNU ARM toolchain is from Yagarto, http://www.yagarto.org/

The standard WICED BT Evaluation board (CYW920706WCDEVAL) provides
single USB-serial port for programming.

The debug interface is ARM Serial Wire Debug (SWD) and shares pins 
with download serial lines TXd (SWDCLK) and RXd (SWDIO).

Building, programming and debugging of applications is achieved
using either a command line interface or the WICED Studio IDE as
described in the Kit Guide.

                     
WICED Technical Support
---------------------------------------------------------------------
WICED support is available on the Cypress forum at 
https://community.cypress.com/welcome
Access to the WICED forum is restricted to bona-fide WICED customers
only.

Cypress provides customer access to a wide range of additional 
information, including technical documentation, schematic diagrams, 
product bill of materials, PCB layout information, and software 
updates. Please contact your Cypress Sales or Engineering support 
representative or Cypress support at http://www.cypress.com/support.

                 
Further Information
---------------------------------------------------------------------
Further information about WICED and the WICED Studio Development
System is available on the WICED website at 
http://www.cypress.com/products/wireless-connectivity or by
contacting Cypress support at http://www.cypress.com/support


