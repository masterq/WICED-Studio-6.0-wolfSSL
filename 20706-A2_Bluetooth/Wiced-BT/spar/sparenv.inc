##############################################################################
# THIS INFORMATION IS PROPRIETARY TO CYPRESS SEMICONDUCTOR                   #
#                                                                            #
# Copyright (c) 2009 Cypress Semiconductor.                                  #
#                                                                            #
#          ALL RIGHTS RESERVED                                               #
#                                                                            #
##############################################################################

# Basic environment for SPAR builds
# Commands need to be in variables since they contain commas which would confuse the Call function
CHIP_CMD     := $(PERL) -e '$$s=$(BLD);$$s=~m/^([AF])_(\d{5})([A-Z])(\d)/;print $$2;'
REVNUM_CMD   := $(PERL) -e '$$s=$(BLD);$$s=~m/^([AF])_(\d{5})([A-Z])(\d)/;print $$3.$$4;'
PLATFORM_CMD := $(PERL) -e '$$s=$(BLD);$$s=~m/^([AF])_(\d{5})([A-Z])(\d)/;print $$1;'

CHIP_CMD     := $(call PERL_FIX_QUOTES,$(CHIP_CMD))
REVNUM_CMD   := $(call PERL_FIX_QUOTES,$(REVNUM_CMD))
PLATFORM_CMD := $(call PERL_FIX_QUOTES,$(PLATFORM_CMD))

CHIP       := $(call QUIET_SHELL,$(CHIP_CMD))
REVNUM     := $(call QUIET_SHELL,$(REVNUM_CMD))
PLATFORM := $(call QUIET_SHELL,$(PLATFORM_CMD))
VARIANT  :=


