# Spar make file overrides and definitions for *_2070x

# This is a CM3 target.
override    COMPILER            := ARM
override    CPU                 := Cortex-M3
override    RTOS                := RTOS_TX_CM3
override    COMPILER_ARM		:= y

# build 591 is broken, use 902 instead
override RVDS_COMPILER_VERSION = arm/rvds40_build902
override RVDS_COMPILER_BUILD   = 902

ifeq ($(SPAR_IN), rom)
# The X0 is what it is, no CRT.
override    SPAR_CRT_SETUP      = $(SPAR_APP_SETUP)
endif

# We dont want USB transport
override 	USB_TRANSPORT 		:= n

APP_PATCH_DIR ?= brcm/mandatory

LMPREV                = 8453

# Define the base chip family name
ifeq ($(BASE_IN),flash)
	BASE_NAME = 20703_flash
else
	BASE_NAME = 20703
endif

# Define 20703 common limits
# Begin address of ROM
IROM_BEGIN_ADDR		= 0x00000000
# Available ROM = 800K
IROM_LENGTH			= (800 * 1024)
# Begin address of RAM
SRAM_BEGIN_ADDR		= 0x00200000
# Available RAM = 284K
SRAM_LENGTH			= (284 * 1024)
# Length of patch table = 512 bytes
PATCH_TABLE_LENGTH	= 512
# Reusable area for spar init - 12K
SPAR_APP_INIT_LENGTH = 12288
# Begin address of flash0
FLASH0_BEGIN_ADDR	= 0x00100000
# Available flash = 320K
#FLASH0_LENGTH		= 0x00050000
# increase size
FLASH0_LENGTH		= 0x00060000

# Patch directory
TIER2_DIR = ../tier2/$(APP_PATCH_DIR)

SYMBOL_TABLE_EXTENSION ?= .st


# If BASE is in ROM, this has to be a patch
ifeq ($(BASE_IN),rom)
  TIER2_ELF := $(TIER2_DIR)/bld/$(PLATFORM)_$(CHIP)$(REVNUM)/$(CHIP)_ram_ext.elf
  ELF_LIST  += $(TIER2_ELF)
ifeq ($(APP_PATCH_DIR),)
    $(error APP_PATCH_DIR is not defined in Application makefile)
endif
else
  TIER2_ELF:=
  ELF_LIST += ../$(BLD_DIR)/$(BASE_NAME).elf
endif

# Path to the patch symbol table
TIER2_SYMDEFS_FILE := $(TIER2_DIR)/bld/$(PLATFORM)_$(CHIP)$(REVNUM)/$(CHIP)_ram_ext.symdefs

# Now add the optional libraries and patches this app needs to the final objs to link in.
LIBS += $(addprefix ../tier2/brcm/libraries/lib/$(CHIP)/,$(APP_PATCHES_AND_LIBS))
LIBS +=  $(addprefix ../tier2/brcm/libraries/lib/$(CHIP)/,$(SPAR_APP_PATCHES_AND_LIBS))

# Include base chip family definitions
CHIP_FLAGS += BCM20703 BB_20703A1 CHIP_FLAVOR=$(CHIP_FLAVOR)

C_FLAGS  += $(addprefix -D,$(CHIP_FLAGS))

ifeq ($(BLEAPP), y)

# Add a few includes: bleapp incs
# arvinds - need to fix these includes - we don't need to include
# all these in the SDK. TODO.
INCS += ../hidd_drivers/$(CHIP)
INCS += ../inc ../cfa ../bsp ../bsp/inc ../hal/boot
INCS += ../bleapp ../bleapp/app ../hidd_drivers  ../bleapp/utils
INCS += ../ulp ../misc ../boot ../bm ../hci ../hal/afh
INCS += ../hidd_drivers ../hidd_drivers/$(CHIP)
INCS += ../mpaf/inc ../mpaf/app
INCS += ../adva/bec_crc ../adva/msbc/lib ../hal/pwrcntl

# OS incs
INCS += ../rtos/threadx ../rtos/threadx/cm3

# LE includes
INCS += ../hci ../ulp ../bm ../misc
C_FLAGS += -DBLE_LRK_LIST_SIZE=4
C_FLAGS += -DBLEAPP

# Stack incs
STACK_DIR		= ../bleapp/lestack
STACK_COMPONENTS = att l2cap blecm gatt profile smp

INCS  += $(addprefix $(STACK_DIR)/, $(STACK_COMPONENTS))

else

include ../config/$(BLD).inc

# ADK should include HIDD src
INCLUDE_HIDD_SRC = y

# Add a few includes: HIDD incs
INCS += . ../inc ../cfa ../bsp ../bsp/inc ../hal/boot
INCS += ../ulp ../misc ../boot ../bm ../hci ../hal/afh
INCS += ../hidd_drivers ../hidd_drivers/$(CHIP)
INCS += ../mpaf/inc ../mpaf/app
INCS += ../adva/bec_crc ../adva/msbc/lib ../hal/pwrcntl

# OS incs
INCS += ../rtos/threadx ../rtos/threadx/cm3

# Stack incs
STACK_DIR		= ../mpaf/btstack/stack
INCS += $(STACK_DIR)/include $(STACK_DIR)/hid $(STACK_DIR)/porting 
INCS += $(STACK_DIR)/l2cap $(STACK_DIR)/btm 

endif

INCS += ../tier2/patch/common
INCS += ../tier2/patch/inc

ifeq ($(DEBUG),1)
    C_FLAGS += -DDEBUG
endif

APP_PATCH_DIR_CGS = $(if $(wildcard ../tier2/$(APP_PATCH_DIR)/bld/$(CHIP)/$(notdir $(APP_PATCH_DIR)).cgs) , "cgs" , "not_found" )
APP_PATCH_DIR_MAKEFILE_INC = $(if $(wildcard ../tier2/$(APP_PATCH_DIR)/makefile.inc) , "makefile.inc" , "not_found" )

### Teach this spar how to make its dependent
# Everything in base dir depends on base elf
../$(BLD_DIR)/%: ../$(BLD_DIR)/$(BASE_NAME).elf
# Base dir depends on base elf
../$(BLD_DIR): ../$(BLD_DIR)/$(BASE_NAME).elf
# this is how you build base elf
../$(BLD_DIR)/$(BASE_NAME).elf:
	$(QUIET)$(MAKE) -C .. MAKEFLAGS=  -j6 BLD=$(BLD) DEBUG=$(DEBUG) LMPREV=$(LMPREV) \
                OS_TARGET_DIR= TOOLSBIN_DOXYGEN_PATH= TOOLSBIN_GRAPHVIZ_PATH= $(BUILD_TOOL_DIR)= TOOLSBIN=


# If we are linking against patches, ensure that patches are there first and then link against them
ifeq ($(SPAR_IN),ram)
# Everything in the patch directory depends on the patch elf
$(TIER2_DIR)/bld/%: $(TIER2_SYMDEFS_FILE)

$(CGS_LIST): $(TIER2_SYMDEFS_FILE)

$(TIER2_ELF): $(TIER2_SYMDEFS_FILE)

$(TIER2_ELF:.elf=.lst): $(TIER2_SYMDEFS_FILE)

$(TIER2_SYMDEFS_FILE):
	# $@ : $+
	if [ ! -f $@ ]; then echo "FATAL: missing: $@"; false; fi
endif

../bootsector/obj-A_$(CHIP)$(REVNUM)/bootsector.cgs:
ifeq "$(wildcard ../bootsector/obj-A_$(CHIP)$(REVNUM)/bootsector.cgs)" ""
	$(QUIET)( cd ../bootsector && \
	 $(MAKE) BLD=$(BLD) cgs )
endif
