/********************************************************************************
* ------------------------------------------------------------------------------
*
* Copyright (c), 2007 Cypress Semiconductor.
*
*          ALL RIGHTS RESERVED
*
********************************************************************************/

#ifndef __BT_HID_TRANSPORT_FIXES_H__
#define __BT_HID_TRANSPORT_FIXES_H__

#include "bthidtransport.h"




// Forward declarations to break cyclic dependencies
class BtHidConn;

class BtHidTransportFixes : public BtHidTransport
{
/*******************************************************************************
* Public methods
*******************************************************************************/
public:
    BtHidTransportFixes(BtHidConn            *btHidConn = NULL, 
                        BtPairingList        *hostList = NULL, 
                        const tBTM_APPL_INFO *btmSecCallbacks = &defaultBtmSecCallbacks,
                        const tBTM_LINK_EVT_CALLBACKS *btmLinkEvtCb = &defaultBtmLinkEvtCallbacks);

    tBTM_STATUS bfcSetParameters(tBTM_BFC_SET_PARAMS *pdata);


    bool isIdled(void) const;
    
#if defined(FIX_CQ_3042497) && defined(DUAL_HIDD)
    void configure(void);
#endif


};
#endif
