
/********************************************************************
* THIS INFORMATION IS PROPRIETARY TO
* Cypress Semiconductor.
*-------------------------------------------------------------------
*
*           Copyright (c) 2007 2010 Cypress Semiconductor.
*                      ALL RIGHTS RESERVED
*
********************************************************************

********************************************************************
*    File Name: btlpm_fixes.h
*
*    Abstract: The concrete implementation of the LPM for a Bluetooth link
*
*    Functions:
*            --
*
*    $History:$ 
*            created  Haixia    03/16/2014
*
*********************************************************************/
#ifndef __BT_LPM_FIXES_H__
#define __BT_LPM_FIXES_H__

#include "btlpm.h"


class BtLpmFixes : public BtLpm
{
public:

    /// Constructor of BTLpmFixes
    BtLpmFixes(BtHidTransport* transportParam,
               const BtLowPowerState* const sniffOnlyParams,
               UINT8 numSniffOnlyParams, 
               const BtLowPowerState* const ssrparams, 
               UINT8 numssrParams);

    void sniffSubrateInd(BD_ADDR bdAddr, UINT8 status, UINT16 maxTxLat, 
                                 UINT16 maxRxLat, UINT16 minRemTo, UINT16 minLocTo);
    void setUpEarlyWakeSources(void);
};
#endif
