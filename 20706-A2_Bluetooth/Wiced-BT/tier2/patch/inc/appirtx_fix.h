/*
********************************************************************
* THIS INFORMATION IS PROPRIETARY TO
* Cypress Semiconductor.
*-------------------------------------------------------------------
*                                                                        
*           Copyright (c) 2014 Cypress Semiconductor.
*                      ALL RIGHTS RESERVED                              
*                                                                       
********************************************************************/
#ifndef __APPIR_TX_FIX_H__
#define __APPIR_TX_FIX_H__

#include "appirtx.h"

class appIRtx_fix : public appIRtx
{
public:
    static appIRtx_fix* getInstance(UINT8 port=2, UINT8 pin=6); //by default P38 is IR tx
    virtual void SendIR(BYTE code, BOOL8 repeat, BYTE repeatcount);

protected:        
    appIRtx_fix(UINT8 port, UINT8 pin); 
    static appIRtx_fix* (*createInstance)(UINT8 port, UINT8 pin);
    static appIRtx_fix* defaultCreateInstance(UINT8 port, UINT8 pin); 
    
    virtual void irTxTimerCallback(void);

    void irBtClockEnable(BOOL enable);
    
    UINT32 mia_clock_1M_restore;

};

#endif
