/*******************************************************************************
* ------------------------------------------------------------------------------
*
* Copyright (c), 2014 Cypress Semiconductor.
*
*          ALL RIGHTS RESERVED
*
********************************************************************************/

#ifndef __BT_HID_CONN_FIXES_H__
#define __BT_HID_CONN_FIXES_H__

#include "bt_types.h"
#include "bthidconn.h"

class BtHidConnFixes : public BtHidConn
{

/*******************************************************************************
* Public methods
*******************************************************************************/
public:
    BtHidConnFixes(unsigned roleSwitchTimeoutInSecs = 0,
              UINT8 maxRoleSwitchRetryCount = 0,
              HidCtrlChannel *ctrlCh = NULL, 
              HidIntrChannel *intrCh = NULL,
              HidSdpChannel  *sdpCh  = NULL, 
              HidConnectionlessCtrlChannel *clCCh = NULL,
              HidConnectionlessIntrChannel *clICh = NULL);

	BOOLEAN connect(const BD_ADDR bdAddr, unsigned timeoutInSecs = 0, 
                            unsigned ctrlChSetupTimeoutInSecs = 0, bool isUcd = false, UINT16 l2cDelayPeriod = 0);

	BOOLEAN incomingAcl(const BD_ADDR bdAddr, unsigned timeoutInSecs = 0);

	void    aclConnected(const BD_ADDR bdAddrOfConnectedDevice, bool isUcd = false, bool isResurrectedAcl = false);

	void clCtrlChDisconnectInd(void);

  
protected:
/*******************************************************************************
* Protected methods
*******************************************************************************/
#ifdef FIX_CQ_2857690
    static BOOLEAN incoming_acl;
#else
    BOOLEAN sdp_requested;
#endif
};

#endif // __BT_HID_CONN_H__

