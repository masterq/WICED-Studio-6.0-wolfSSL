/*
********************************************************************
* THIS INFORMATION IS PROPRIETARY TO
* Cypress Semiconductor.
*-------------------------------------------------------------------
*                                                                        
*           Copyright (c) 2014 Cypress Semiconductor.
*                      ALL RIGHTS RESERVED                              
*                                                                       
********************************************************************/
#ifndef __KEYSCAN_FIXES_H__
#define __KEYSCAN_FIXES_H__

#ifdef __cplusplus
extern "C" {
#endif

BOOL32 keyscan_fixes_allKeysIdle(void);

#ifdef __cplusplus
}
#endif

#endif


