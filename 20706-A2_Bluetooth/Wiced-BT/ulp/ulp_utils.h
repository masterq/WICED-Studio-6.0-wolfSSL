/******************************************************************************
* THIS INFORMATION IS PROPRIETARY TO Cypress Semiconductor 
*
* -----------------------------------------------------------------------------
*
* Copyright (c) 2004 Cypress Semiconductor.
*
*          ALL RIGHTS RESERVED
*
*******************************************************************************
*
* File Name: ulp_utils.h
*
* Abstract:  This file implements the all ULP utility functions.
*            Can be split into different files in future development if it gets
*            too big.
* Functions:
*
******************************************************************************/
#ifndef _ULP_UTILS_H
#define _ULP_UTILS_H

#include "types.h"

extern INT32 ulp_randomNum;
extern const UINT8 btBaseUUID[16];

extern INT32 ulp_getRand (void);
extern INT32 ulp_initRandomSeed(void);
extern void ulp_rand(UINT32* randNumber, UINT32 length);
extern void ulp_upconvertUUID(UINT8 *pUUID_1, MU8 size_1, UINT8 *pUUID_2, MU8 size_2);
#endif
