/*
 * $ Copyright Broadcom Corporation $
 */

/** @file
 *
 * Handsfree AT command response interpreter.
 */

#include <string.h>
#include "wiced_bt_hfp_hf_int.h"

/*****************************************************************************
**  Constants
*****************************************************************************/
extern const uint8_t wiced_bt_hfp_hf_trans_cmd[];

/* AT command interpreter states */
enum
{
    WICED_BT_HFP_HF_PARSE_INIT_ST, /* Init */
    WICED_BT_HFP_HF_PARSE_CMD_ST,  /* Processing command */
    WICED_BT_HFP_HF_PARSE_ERR_ST   /* Error */
};

/*******************************************************************************
** Function         wiced_bt_hfp_hf_util_strucmp
** Description      This utility function compares two strings in uppercase.
**                  String p_s must be uppercase.  String p_t is converted to
**                  uppercase if lowercase.  If p_s ends first, the substring
**                  match is counted as a match.
** Returns          0 if strings match, nonzero otherwise.
*******************************************************************************/
static int wiced_bt_hfp_hf_util_strucmp(const char *p_s, const char *p_t)
{
    char c;

    while (*p_s && *p_t)
    {
        c = *p_t++;
        if (c >= 'a' && c <= 'z')
        {
            c -= 0x20;
        }
        if (*p_s++ != c)
        {
            return -1;
        }
    }
    /* if p_t hit null first, no match */
    if (*p_t == 0 && *p_s != 0)
    {
        return 1;
    }
    /* else p_s hit null first, count as match */
    else
    {
        return 0;
    }
}

/*****************************************************************************
** Function         wiced_bt_hfp_hf_at_init
** Description      Initialize the AT command parser control block.
** Returns          void
*****************************************************************************/
void wiced_bt_hfp_hf_at_init(wiced_bt_hfp_hf_at_cb_t *p_cb)
{
    p_cb->p_res_buf = NULL;
    p_cb->res_pos = 0;
    p_cb->state = WICED_BT_HFP_HF_PARSE_INIT_ST;
}

/*****************************************************************************
** Function         wiced_bt_hfp_hf_at_reinit
** Description      Re-initialize the AT command parser control block.  This
**                  function resets the AT command parser state and frees
**                  any GKI buffer.
** Returns          void
*****************************************************************************/
void wiced_bt_hfp_hf_at_reinit(wiced_bt_hfp_hf_at_cb_t *p_cb)
{
    if (p_cb->p_res_buf != NULL)
    {
        GKI_freebuf(p_cb->p_res_buf);
        p_cb->p_res_buf = NULL;
    }
    p_cb->res_pos = 0;
    p_cb->state = WICED_BT_HFP_HF_PARSE_INIT_ST;
}

/*****************************************************************************
** Function         wiced_bt_hfp_hf_at_parse
** Description      Parse AT commands.  This function will take the input
**                  character string and parse it for AT commands according to
**                  the AT command table passed in the control block.
** Returns          BOOLEAN TRUE if done with string. Flase if waiting for new data
*****************************************************************************/
wiced_bool_t wiced_bt_hfp_hf_at_parse(wiced_bt_hfp_hf_at_cb_t *p_cb,
    char *p_buf, uint16_t len)
{
    int           i;
    UINT16        idx;
    wiced_bool_t  found_return = FALSE;
    char         *p_arg;

    for(i = 0; i < len-1;)
    {
        switch(p_cb->state)
        {
            case WICED_BT_HFP_HF_PARSE_INIT_ST:
                /* Scan through looking for '\n' */
                for (; ((i<len) && (p_buf[i] < ' ')); i++);

                if(i < len-1)
                {
                    p_cb->state = WICED_BT_HFP_HF_PARSE_CMD_ST;
                    p_cb->p_res_buf = (char *) GKI_getbuf(p_cb->res_max_len);
                }
                break;


            case WICED_BT_HFP_HF_PARSE_CMD_ST:
                /* Scan through, copying string looking for return */
                while ((i < len) && (p_cb->res_pos < p_cb->res_max_len))
                {
                    if ((p_cb->p_res_buf[p_cb->res_pos] = p_buf[i++]) == '\r')
                    {
                        p_cb->p_res_buf[p_cb->res_pos] = 0;
                        found_return = TRUE;
                        break;
                    }
                    p_cb->res_pos++;
                }

                /* If we found return, we're done, parse string */
                if (found_return)
                {
                    WICED_BTHFP_TRACE("%s: rcvd AT cmd: %s\n",
                        __FUNCTION__, p_cb->p_res_buf);
                    found_return = FALSE;

                    /* Loop through at command table looking for match */
                    for (idx = 0; wiced_bt_hfp_hf_res[idx].p_res[0] != 0; idx++)
                    {
                        if (!wiced_bt_hfp_hf_util_strucmp(wiced_bt_hfp_hf_res[idx].p_res,
                                p_cb->p_res_buf))
                        {
                            p_arg = p_cb->p_res_buf + strlen(wiced_bt_hfp_hf_res[idx].p_res);
                            wiced_bt_hfp_hf_at_cback(p_cb->p_user, idx, p_arg);
                            wiced_bt_hfp_hf_at_reinit(p_cb);
                            break;
                        }
                    }
                    /* If no match call error callback */
                    if (wiced_bt_hfp_hf_res[idx].p_res[0] == 0)
                    {
                        /* Go to idle if this is an unknown AT command */
                        wiced_bt_hfp_hf_at_err_cback(p_cb->p_user, TRUE, p_cb->p_res_buf);
                        wiced_bt_hfp_hf_at_reinit(p_cb);
                    }
                }
                /* Else if string too long skip command */
                else if (p_cb->res_pos == p_cb->res_max_len)
                {
                    WICED_BTHFP_ERROR("%s: AT Parser reached max length: %d\n",
                        __FUNCTION__, p_cb->res_max_len );
                    wiced_bt_hfp_hf_at_reinit(p_cb);
                    return FALSE;
                }
                break;

            case WICED_BT_HFP_HF_PARSE_ERR_ST:
                /* Skip data until we get a return */
                for(; (i < len) && (p_buf[i] != '\n'); i++);

                if(i < len)
                {
                    wiced_bt_hfp_hf_at_reinit(p_cb);
                    i++;
                }
                break;
        }
    }

    return TRUE;
}

/*******************************************************************************
** Function         wiced_bt_hfp_hf_at_timer_cback
** Description      Handsfree timer callback.
*******************************************************************************/
void wiced_bt_hfp_hf_at_timer_cback(void *p)
{
/* TODO: Timer implementation */
#if 0
    BT_HDR          *p_buf;
    TIMER_LIST_ENT  *p_tle = (TIMER_LIST_ENT *) p;

    if ((p_buf = (BT_HDR *) GKI_getbuf(sizeof(BT_HDR))) != NULL)
    {
        //p_buf->event = p_tle->event;
        //p_buf->layer_specific = bta_hs_scb_to_idx((wiced_bt_hfp_hf_scb_t *)p_tle->param);
        //bta_sys_sendmsg(p_buf);
    }
#endif
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_init
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_init(wiced_bt_hfp_hf_scb_t *p_scb)
{
    WICED_BTHFP_TRACE("%s: AT command send queue initialized\n", __FUNCTION__);
    GKI_init_q (&p_scb->wiced_bt_hfp_hf_at_cmd_queue);
    p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth = 0;
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_free
** Description      Function to delete all array elements + array list
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_free(wiced_bt_hfp_hf_scb_t *p_scb)
{
    BT_HDR *p_buf = NULL;

    /*TODO: Timer implementation */
    //bta_sys_stop_timer(&p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer);

    while((p_buf = (BT_HDR *)GKI_dequeue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue)) != NULL)
    {
        GKI_freebuf(p_buf);
    }

    p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth = 0;
    WICED_BTHFP_TRACE("%s: AT command send queue freed\n", __FUNCTION__);
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_enqueue
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_enqueue(wiced_bt_hfp_hf_scb_t *p_scb,
    uint8_t cmd, char *buf, uint16_t len)
{
    BT_HDR *p_data = (BT_HDR *)GKI_getbuf((UINT16)(len+sizeof(BT_HDR)));

    if(p_data != NULL)
    {
        p_data->len = len;
        p_data->layer_specific = cmd;
        memcpy((UINT8 *)(p_data+1), buf, len);
        GKI_enqueue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue, (void *)p_data);
        p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth++;

        /*send if 1st (and only) command in the queue*/
        if(p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth == 1)
        {
            wiced_bt_hfp_hf_at_cmd_queue_send(p_scb);
        }
    }
    else
    {
        WICED_BTHFP_ERROR("%s: No GKI buffer for cmd %s\n", __FUNCTION__, buf);
    }
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_cmd_queue_handle_res
** Description      If OK/ERROR, returns the command id that this ok/error belongs to
******************************************************************************/
uint8_t wiced_bt_hfp_hf_at_cmd_queue_handle_res(wiced_bt_hfp_hf_scb_t *p_scb)
{
    BT_HDR *p_data;
    uint8_t at_cmd_id;
    uint8_t api_cmd_id = 0xFF;

    /*TODO: Timer implementation */
    //bta_sys_stop_timer(&p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer);

    p_data = (BT_HDR *)GKI_dequeue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue);

    if(p_data)
    {
        p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth--;

        /* api_cmd_id, not at_cmd_id, should be passed to the application. */
        at_cmd_id = (UINT8)p_data->layer_specific;
        /* TODO: This needs to be resolved for translation of AT cmds to the application */
        at_cmd_id = at_cmd_id;
#if 0
        for (api_cmd_id = 0 ; api_cmd_id < WICED_BT_HFP_HF_MAX_CMD; api_cmd_id++)
        {
            if (at_cmd_id == wiced_bt_hfp_hf_trans_cmd[api_cmd_id])
                break;
        }

        if (api_cmd_id >= WICED_BT_HFP_HF_MAX_CMD)
            api_cmd_id = 0xFF;
#endif
        GKI_freebuf(p_data);
    }
    else
    {
        WICED_BTHFP_TRACE("%s: Received AT response, but no AT command pending\n",
            __FUNCTION__);
    }

    if (!GKI_queue_is_empty(&p_scb->wiced_bt_hfp_hf_at_cmd_queue))
        wiced_bt_hfp_hf_at_cmd_queue_send(p_scb);

    return api_cmd_id;
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_flush
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_flush(wiced_bt_hfp_hf_scb_t *p_scb)
{
    BT_HDR *p_buf = NULL;

    /*TODO: Timer implementation */
    //bta_sys_stop_timer( &p_scb->wiced_bt_hfp_hf_cmd_queue_timer );

    while((p_buf = (BT_HDR *)GKI_dequeue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue)) != NULL)
    {
        GKI_freebuf(p_buf);
    }

    p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth = 0;
    WICED_BTHFP_TRACE("%s: AT command send queue flushed\n", __FUNCTION__);
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_send
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_send(wiced_bt_hfp_hf_scb_t *p_scb)
{
    char      data[WICED_BT_HFP_HF_AT_MAX_LEN + 16] = {'\0'};
    uint16_t  len;
    BT_HDR   *p_buf = (BT_HDR *)GKI_getfirst(&p_scb->wiced_bt_hfp_hf_at_cmd_queue);

    if(p_buf == NULL)
    {
        WICED_BTHFP_ERROR("%s NULL SCB\n", __FUNCTION__);
        return;
    }

    memcpy(data, (UINT8 *)(p_buf+1),p_buf->len);

    /* Send to RFCOMM */
    WICED_BTHFP_TRACE("%s: Sending AT cmd: %s\n", __FUNCTION__, data);
    wiced_bt_rfcomm_write_data(p_scb->rfcomm_handle, data, p_buf->len ,&len);

    /* TODO: Timer implementation */
    //bta_sys_stop_timer(&p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer);
    //memset( &p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer, 0,
    //    sizeof(p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer));
    //p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer.param = (TIMER_PARAM_TYPE) p_scb;
    //p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer.p_cback = wiced_bt_hfp_hf_at_timer_cback;
    //bta_sys_start_timer( &p_scb->wiced_bt_hfp_hf_at_cmd_queue_timer, WICED_BT_HFP_HF_CMD_TIMEOUT_EVT,
    //    WICED_BT_HFP_HF_CMD_TIMEOUT_VALUE);

}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_queue_timeout
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_queue_timeout(wiced_bt_hfp_hf_scb_t *p_scb)
{
    BT_HDR *p_data;

    WICED_BTHFP_TRACE("%s: No response received for previous AT command, sending next\n",
        __FUNCTION__);

    p_data = (BT_HDR *)GKI_dequeue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue);

    if(p_data)
    {
        p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth--;
        GKI_freebuf(p_data);
    }

    if (!GKI_queue_is_empty(&p_scb->wiced_bt_hfp_hf_at_cmd_queue))
        wiced_bt_hfp_hf_at_cmd_queue_send(p_scb);
}

/******************************************************************************
** Function         wiced_bt_hfp_hf_at_cmd_flush_cmd_in_queue
** Description
******************************************************************************/
void wiced_bt_hfp_hf_at_cmd_flush_cmd_in_queue(wiced_bt_hfp_hf_scb_t *p_scb, uint8_t cmd)
{
    /* Ignore the first element, as we might have already sent it over the air */
    BT_HDR *p_buf = (BT_HDR *)GKI_getfirst(&p_scb->wiced_bt_hfp_hf_at_cmd_queue);


    while(p_buf)
    {
        p_buf = (BT_HDR *)GKI_getnext(p_buf);
        if(p_buf)
        {
            if(cmd == p_buf->layer_specific)
            {
                WICED_BTHFP_TRACE("Removing AT cmd %d from queue\n", cmd);
                GKI_remove_from_queue(&p_scb->wiced_bt_hfp_hf_at_cmd_queue, p_buf);
                p_scb->wiced_bt_hfp_hf_at_cmd_queue_depth--;
                GKI_freebuf(p_buf);
                return;
            }
        }
    }

    return;
}


