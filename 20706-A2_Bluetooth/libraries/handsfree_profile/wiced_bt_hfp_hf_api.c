/*
 * $ Copyright Broadcom Corporation $
 */

/** @file
 *
 * This file contains implementation of the handsfree interface.
 */

#include "wiced_bt_hfp_hf_int.h"
#include <string.h>

#define GKI_send_msg(a,b,p_msg) wiced_bt_hfp_hf_hdl_event((BT_HDR *)p_msg)
#define WICED_ALREADY_INITIALIZED 1000
/*******************************************************************************
** Function         wiced_bt_hfp_hf_utils_bdcpy
** Description      Copy bd addr b to a
** Returns          void
*******************************************************************************/
void wiced_bt_hfp_hf_utils_bdcpy(wiced_bt_device_address_t a,
    const wiced_bt_device_address_t b)
{
    int i;
    for(i = BD_ADDR_LEN; i!=0; i--)
    {
        *a++ = *b++;
    }
}

/*******************************************************************************
** Function         wiced_bt_hfp_hf_utils_bdcmp
** Description      Compare bd addr b to a
** Returns          Zero if b==a, nonzero otherwise (like memcmp)
*******************************************************************************/
int wiced_bt_hfp_hf_utils_bdcmp(const wiced_bt_device_address_t a,
    const wiced_bt_device_address_t b)
{
    int i;
    for(i = BD_ADDR_LEN; i!=0; i--)
    {
        if( *a++ != *b++ )
        {
            return -1;
        }
    }
    return 0;
}

wiced_result_t wiced_bt_hfp_hf_init(wiced_bt_hfp_hf_config_data_t *p_config_data,
    wiced_bt_hfp_hf_event_cb_t event_cb)
{
    uint8_t        i = 0;
    wiced_result_t ret = WICED_SUCCESS;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init == TRUE)
    {
        WICED_BTHFP_ERROR("%s: Already initialized\n", __FUNCTION__);
        return WICED_ALREADY_INITIALIZED;
    }

    if(p_config_data == NULL)
    {
        WICED_BTHFP_ERROR("%s: Config data is not present\n", __FUNCTION__);
        return WICED_BADARG;
    }

    if(p_config_data->num_server > WICED_BT_HFP_HF_MAX_CONN)
    {
        WICED_BTHFP_ERROR("%s: Max number of servers exceeded\n", __FUNCTION__);
        return WICED_BADARG;
    }

    /* Check if Init of the state machine can be done. If not, then bail-out */
    if(wiced_bt_hfp_hf_init_state_machine() != WICED_SUCCESS)
    {
        return WICED_BADARG;
    }

    /* Initialize main control block */
    memset(&wiced_bt_hfp_hf_cb, 0, sizeof(wiced_bt_hfp_hf_cb_t));

    /* Copy the configuration information */
    memcpy(&wiced_bt_hfp_hf_cb.config_data, p_config_data,
        sizeof(wiced_bt_hfp_hf_config_data_t));

    /* Copy the callback information */
    wiced_bt_hfp_hf_cb.p_event_cback = event_cb;

    /* Register with RFCOMM */
    for(i=0; i<p_config_data->num_server; i++) {
        if( (ret = wiced_bt_hfp_hf_register(p_config_data->scn[i],p_config_data->uuid[i])) != WICED_SUCCESS)
        {
            /*TODO: Stop the already started server in case any of the server registration fails */
            WICED_BTHFP_ERROR("%s: Register with RFCOMM failed\n", __FUNCTION__);
            return ret;
        }
    }

    wiced_bt_hfp_hf_cb.is_init = TRUE;
    return WICED_SUCCESS;

}

wiced_result_t wiced_bt_hfp_hf_deinit(void)
{
    BT_HDR *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (BT_HDR *) GKI_getbuf(sizeof(BT_HDR))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->event = WICED_BT_HFP_HF_API_DEINIT_EVT;
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_hfp_hf_connect(wiced_bt_device_address_t bd_address)
{
    wiced_bt_hfp_hf_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_hfp_hf_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_hfp_hf_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    wiced_bt_hfp_hf_cb.ag_profile_uuid = UUID_SERVCLASS_AG_HANDSFREE;
    p_buf->hdr.event = WICED_BT_HFP_HF_API_CONNECT_EVT;
    wiced_bt_hfp_hf_utils_bdcpy(p_buf->bd_address, bd_address);
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_hfp_hf_disconnect(uint16_t handle)
{
    wiced_bt_hfp_hf_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_hfp_hf_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_hfp_hf_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_HFP_HF_API_DISCONNECT_EVT;
    p_buf->handle = handle;
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_hfp_hf_perform_call_action(uint16_t handle,
    wiced_bt_hfp_hf_call_action_t action, char* number)
{
    wiced_bt_hfp_hf_api_call_action_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_hfp_hf_api_call_action_t *)
        GKI_getbuf(sizeof(wiced_bt_hfp_hf_api_call_action_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_HFP_HF_API_CALL_ACTION_EVT;
    p_buf->handle = handle;
    p_buf->action = action;
    if(number != NULL)
    {
        if(strlen(number) >= WICED_BT_HFP_HF_MAX_PHONE_NUMBER_LEN)
        {
            WICED_BTHFP_ERROR("%s: phone number exceeds max\n", __FUNCTION__);
            GKI_freebuf(p_buf);
            return WICED_BADARG;
        }
        utl_strcpy(p_buf->number, number);
    }
    else
    {
        memset(p_buf->number, 0, sizeof(p_buf->number));
    }
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_hfp_hf_notify_volume( uint16_t handle,
    wiced_bt_hfp_hf_volume_type_t volume_type, uint8_t volume_level)
{
    wiced_bt_hfp_hf_api_notify_vol_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_hfp_hf_api_notify_vol_t *)
        GKI_getbuf(sizeof(wiced_bt_hfp_hf_api_notify_vol_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_HFP_HF_API_NOTIFY_VOLUME_EVT;
    p_buf->handle = handle;
    p_buf->volume_type = volume_type;
    p_buf->volume_level = volume_level;
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_hfp_hf_send_at_cmd(uint16_t handle, char* at_cmd)
{
    wiced_bt_hfp_hf_api_send_at_cmd_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_hfp_hf_cb.is_init != WICED_TRUE)
    {
        WICED_BTHFP_ERROR("%s: Not initialized\n", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_hfp_hf_api_send_at_cmd_t *)
        GKI_getbuf(sizeof(wiced_bt_hfp_hf_api_send_at_cmd_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_HFP_HF_API_SEND_AT_CMD_EVT;
    p_buf->handle = handle;
    if(at_cmd != NULL)
    {
        utl_strcpy(p_buf->at_cmd, at_cmd);
    }
    GKI_send_msg(WICED_BT_HFP_HF_TASK_ID, WICED_BT_HFP_HF_TASK_MBOX, p_buf);

    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

