/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file contains implementation of the audio sink interface.
 */

#include <string.h>
#include "wiced_gki.h"
#include "wiced_bt_avdt.h"
#include "wiced_bt_a2dp_sink_int.h"
#include "wiced_audio_sink.h"

#define GKI_send_msg(a,b,p_msg) wiced_bt_a2dp_sink_hdl_event((BT_HDR *)p_msg)

/*******************************************************************************
**
** Function         wiced_bt_a2dp_sink_utils_bdcpy
**
** Description      Copy bd addr b to a.
**
** Returns          void
**
*******************************************************************************/
void wiced_bt_a2dp_sink_utils_bdcpy(wiced_bt_device_address_t a,
    const wiced_bt_device_address_t b)
{
    int i;

    for(i = BD_ADDR_LEN; i!=0; i--)
    {
        *a++ = *b++;
    }
}

/*******************************************************************************
**
** Function         wiced_bt_a2dp_sink_utils_bdcmp
**
** Description          Compare bd addr b to a.
**
** Returns              Zero if b==a, nonzero otherwise (like memcmp).
**
*******************************************************************************/
int wiced_bt_a2dp_sink_utils_bdcmp(const wiced_bt_device_address_t a,
    const wiced_bt_device_address_t b)
{
    int i;

    for(i = BD_ADDR_LEN; i!=0; i--)
    {
        if( *a++ != *b++ )
        {
            return -1;
        }
    }
    return 0;
}

wiced_result_t wiced_bt_a2dp_sink_init(wiced_bt_a2dp_config_data_t *p_config_data,
    wiced_bt_a2dp_sink_control_cb_t control_cb )
{
    wiced_result_t ret = WICED_SUCCESS;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init == WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Already initialized", __FUNCTION__);
        return WICED_ALREADY_INITIALIZED;
    }

    if(p_config_data == NULL)
    {
        APPL_TRACE_ERROR1("%s: Config data is not present", __FUNCTION__);
        return WICED_BADARG;
    }

    /* Check if Init of the state machine can be done. If not, then bail-out */
    if(wiced_bt_a2dp_sink_init_state_machine() != WICED_SUCCESS)
    {
        return WICED_BADARG;
    }

    /* Initialize main control block */
    memset(&wiced_bt_a2dp_sink_cb, 0, sizeof(wiced_bt_a2dp_sink_cb_t));

    /* Copy the configuration information */
    wiced_bt_a2dp_sink_cb.p_config_data = p_config_data;

    /* Copy the callback information */
    wiced_bt_a2dp_sink_cb.control_cb = control_cb;
    //wiced_bt_a2dp_sink_cb.data_cb = data_cb;

    /* Register with AVDT */
    if( (ret = wiced_bt_a2dp_sink_register()) != WICED_SUCCESS)
    {
        APPL_TRACE_ERROR1("%s: Register with AVDT failed", __FUNCTION__);
        return ret;
    }

    wiced_bt_a2dp_sink_cb.is_init = WICED_TRUE;

    return wiced_audio_sink_config_init(&p_config_data->p_param);
}

wiced_result_t wiced_bt_a2dp_sink_deinit(void)
{
    BT_HDR *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (BT_HDR *) GKI_getbuf(sizeof(BT_HDR))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->event = WICED_BT_A2DP_SINK_API_DEINIT_EVT;
    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_connect(wiced_bt_device_address_t bd_address)
{
    wiced_bt_a2dp_sink_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_a2dp_sink_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_a2dp_sink_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_A2DP_SINK_API_CONNECT_EVT;
    wiced_bt_a2dp_sink_utils_bdcpy(p_buf->bd_address, bd_address);

    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_disconnect(uint16_t handle)
{
    wiced_bt_a2dp_sink_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_a2dp_sink_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_a2dp_sink_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_A2DP_SINK_API_DISCONNECT_EVT;
    p_buf->handle    = handle;

    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_start(uint16_t handle)
{
    wiced_bt_a2dp_sink_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_a2dp_sink_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_a2dp_sink_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_A2DP_SINK_API_START_EVT;
    p_buf->handle    = handle;

    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_send_start_response( uint16_t handle, uint8_t label, uint8_t status )
{
    wiced_bt_a2dp_sink_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_a2dp_sink_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_a2dp_sink_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event    = WICED_BT_A2DP_SINK_API_START_RESP_EVT;
    p_buf->handle       = handle;
    p_buf->label        = label;
    p_buf->status       = status;

    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_suspend(uint16_t handle)
{
    wiced_bt_a2dp_sink_api_data_t *p_buf = NULL;

    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    if((p_buf = (wiced_bt_a2dp_sink_api_data_t *)
        GKI_getbuf(sizeof(wiced_bt_a2dp_sink_api_data_t))) == NULL)
    {
        return WICED_OUT_OF_HEAP_SPACE;
    }

    p_buf->hdr.event = WICED_BT_A2DP_SINK_API_SUSPEND_EVT;
    p_buf->handle    = handle;

    GKI_send_msg(WICED_BT_A2DP_SINK_TASK_ID, WICED_BT_A2DP_SINK_TASK_MBOX, p_buf);
    GKI_freebuf(p_buf);
    return WICED_SUCCESS;
}

wiced_result_t wiced_bt_a2dp_sink_send_delay_report(uint16_t handle, uint16_t delay )
{
    /* Check if already initialized, then just return with error */
    if(wiced_bt_a2dp_sink_cb.is_init != WICED_TRUE)
    {
        APPL_TRACE_ERROR1("%s: Not initialized", __FUNCTION__);
        return WICED_NOTUP;
    }

    APPL_TRACE_ERROR1("%s: Not supporetd", __FUNCTION__);
    return WICED_UNSUPPORTED;
}

/*******************************************************************************
**
** Function         wiced_bt_a2dp_sink_mute_audio
**
** Description      To mute/unmute the audio while streaming
**
** Returns          wiced_result_t
**
*******************************************************************************/
wiced_result_t  wiced_bt_a2dp_sink_mute_audio( wiced_bool_t enable, uint16_t ramp_ms )
{
    return wiced_audio_sink_mute( enable, ramp_ms );
}

/*******************************************************************************
**
** Function         wiced_bt_a2dp_sink_register_data_cback
**
** Description      To register the callback function to receive the encoded audio data
**
** Returns          none
**
*******************************************************************************/
void wiced_bt_a2dp_sink_register_data_cback( wiced_bt_a2dp_sink_data_cb_t p_cback )
{
    wiced_audio_sink_register_data_cback( p_cback );
}
