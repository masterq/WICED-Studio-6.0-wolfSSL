/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
*
* WICED Bluetooth OTA Upgrade
*
* This file provides function required to support Over the Air WICED Upgrade.
*
* To download host sends command to download with length of the patch to be
* transmitted.  GATT Write Requests are used to send commands and portions of
* data.  In case of an error Error Response indicates failure to the host.
* Host sends fixed chunks of data.  After all the bytes has been downloaded
* and acknowledged host sends verify command that includes CRC32 of the
* whole patch.  During the download device saves data directly to the 
* serial flash.  At the verification stage device reads data back from the
* NVRAM and calculates checksum of the data stored there.  Result of the
* verification is indicated in the Write Response or Error Response GATT message.
*
*/
#include "bt_types.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_firmware_upgrade.h"
#include "wiced_bt_fw_upgrade.h"
#include "wiced_bt_trace.h"
#include "wiced_timer.h"

/******************************************************
 *                      Constants
 ******************************************************/
#define OTA_FW_UPGRADE_READ_CHUNK                   128
#define OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT         128

#if defined(USE_256K_SECTOR_SIZE)
  #define FLASH_SECTOR_SIZE         (256*1024)
  #define FW_UPGRADE_FLASH_SIZE     (256*FLASH_SECTOR_SIZE)
#else
  #define FLASH_SECTOR_SIZE         (4*1024)
  #define FW_UPGRADE_FLASH_SIZE     0x80000 // 512  kbyte/4M Bit Sflash for new tag boards
#endif

typedef struct
{
// device states during OTA FW upgrade
#define OTA_STATE_IDLE                   0
#define OTA_STATE_READY_FOR_DOWNLOAD     1
#define OTA_STATE_DATA_TRANSFER          2
#define OTA_STATE_VERIFICATION           3
#define OTA_STATE_VERIFIED               4
#define OTA_STATE_ABORTED                5
    int32_t         state;
    uint8_t         bdaddr[6];               // BDADDR of connected device
    uint16_t        client_configuration;    // characteristic client configuration descriptor
    uint8_t         status;                  // Current status
    uint16_t        current_offset;          // Offset in the image to store the data
    int32_t         total_len;               // Total length expected from the host
    int32_t         current_block_offset;
    int32_t         total_offset;
    uint32_t        crc32;
    uint32_t        recv_crc32;
    uint8_t         indication_sent;
    wiced_timer_t   reset_timer;
    uint8_t         read_buffer[OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT];
} ota_fw_upgrade_state_t;


/******************************************************
 *               Variables Definitions
 ******************************************************/
ota_fw_upgrade_state_t                        ota_fw_upgrade_state;
uint16_t                                      ota_client_config_descriptor = 0;
wiced_firmware_upgrade_pre_reboot_callback_t *ota_pre_reboot_callback = NULL;

static void                   ota_fw_upgrade_set_client_configuration(uint16_t client_config);
static wiced_bool_t           ota_fw_upgrade_handle_data(uint16_t conn_id, uint8_t *data, int32_t len);
static wiced_bool_t           ota_fw_upgrade_handle_command(uint16_t conn_id, uint8_t *data, int32_t len);
static wiced_bt_gatt_status_t ota_fw_upgrade_send_notification(uint16_t conn_id, uint16_t attr_handle, uint16_t val_len, uint8_t *p_val);
static void                   ota_fw_upgrade_reset_timeout(uint32_t param);

/*
 * Initializes global data
 */
void ota_fw_upgrade_init_data()
{
    ota_fw_upgrade_state.state           = OTA_STATE_IDLE;
    ota_fw_upgrade_state.indication_sent = WICED_FALSE;
}

/*
 * Initialize peripheral UART upgrade procedure
 */
wiced_bool_t wiced_ota_fw_upgrade_init(wiced_firmware_upgrade_pre_reboot_callback_t *p_reboot_callback)
{
    wiced_fw_upgrade_nv_loc_len_t nv_loc_len;

    /* In Serial flash first 8K i.e (addr range 0x0000 to 0x2000)reserved for static section and for fail safe OTA block.
    Caution!!: Application should not modify these sections. */
    /* Remaining portion can be used for Vendor specific data followed by firmware
    It is applications choice to choose size for Vendor specific data and firmware.
    Apart from vendor specific portion, remaining area equally divided for active firmware and upgradable firmware.
    Below are the example offsets for 4M bit Serial flash 
    Note: below configuration should not conflict with ConfigDSLocation, DLConfigVSLocation and DLConfigVSLength configured in 
    platform sflash .btp file */

    // 4 MB Serial flash offsets for firmware upgrade.
    nv_loc_len.ss_loc   = 0x0000;
    nv_loc_len.vs1_loc  = (FLASH_SECTOR_SIZE + FLASH_SECTOR_SIZE); // one sector after SS for fail safe
    nv_loc_len.vs1_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.vs2_loc  = nv_loc_len.vs1_loc + nv_loc_len.vs1_len;
    nv_loc_len.vs2_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.ds1_loc  = nv_loc_len.vs2_loc + nv_loc_len.vs2_len;
#if defined(USE_256K_SECTOR_SIZE)
    nv_loc_len.ds1_len  = FLASH_SECTOR_SIZE;
    nv_loc_len.ds2_loc  = nv_loc_len.ds1_loc + nv_loc_len.ds1_len;
    nv_loc_len.ds2_len  = FLASH_SECTOR_SIZE;
#else
    {
        uint32_t ds1_ds2_length = (FW_UPGRADE_FLASH_SIZE - nv_loc_len.ds1_loc);
        nv_loc_len.ds1_len  = ds1_ds2_length/2;
        nv_loc_len.ds2_loc  = nv_loc_len.ds1_loc + nv_loc_len.ds1_len;
        nv_loc_len.ds2_len  = ds1_ds2_length/2;
    }
#endif

    WICED_BT_TRACE("ds1:0x%08x, len:0x%08x\n", nv_loc_len.ds1_loc, nv_loc_len.ds1_len);
    WICED_BT_TRACE("ds2:0x%08x, len:0x%08x\n", nv_loc_len.ds2_loc, nv_loc_len.ds2_len);

    if (wiced_firmware_upgrade_init(&nv_loc_len, FW_UPGRADE_FLASH_SIZE) == WICED_FALSE)
    {
        WICED_BT_TRACE("WARNING: Upgrade will fail - active DS is not one of the expected locations\n");
        WICED_BT_TRACE("WARNING: Please build with OTA_FW_UPGRADE=1 and download \n");
        return WICED_FALSE;
    }
    ota_fw_upgrade_init_data();
    ota_pre_reboot_callback = p_reboot_callback;

    return WICED_TRUE;
}

/*
 * Connection with peer device is established
 */
void wiced_ota_fw_upgrade_connection_status_event(wiced_bt_gatt_connection_status_t *p_status)
{
    if (p_status->connected)
    {
        memcpy(ota_fw_upgrade_state.bdaddr, p_status->bd_addr, BD_ADDR_LEN);
    }
}

/*
 * Process GATT Read request
 */
wiced_bt_gatt_status_t wiced_ota_fw_upgrade_read_handler(uint16_t conn_id, wiced_bt_gatt_read_t * p_read_data)
{
    switch (p_read_data->handle)
    {
    case HANDLE_OTA_FW_UPGRADE_CLIENT_CONFIGURATION_DESCRIPTOR:
        if (p_read_data->offset >= 2)
            return WICED_BT_GATT_INVALID_OFFSET;

        if (*p_read_data->p_val_len < 2)
            return WICED_BT_GATT_INVALID_ATTR_LEN;

        if (p_read_data->offset == 1)
        {
            p_read_data->p_val[0] = ota_client_config_descriptor >> 8;
            *p_read_data->p_val_len = 1;
        }
        else
        {
            p_read_data->p_val[0] = ota_client_config_descriptor & 0xff;
            p_read_data->p_val[1] = ota_client_config_descriptor >> 8;
            *p_read_data->p_val_len = 2;
        }
        return WICED_BT_GATT_SUCCESS;
    }
    return WICED_BT_GATT_INVALID_HANDLE;
}

/*
 * Process GATT Write request
 */
wiced_bt_gatt_status_t wiced_ota_fw_upgrade_write_handler(uint16_t conn_id, wiced_bt_gatt_write_t *p_write_data)
{
    switch (p_write_data->handle)
    {
    case HANDLE_OTA_FW_UPGRADE_CONTROL_POINT:
        if (!ota_fw_upgrade_handle_command(conn_id, p_write_data->p_val, p_write_data->val_len))
        {
            WICED_BT_TRACE("ota_handle_command failed.\n");
            return WICED_BT_GATT_ERROR;
        }
        break;

    case HANDLE_OTA_FW_UPGRADE_CLIENT_CONFIGURATION_DESCRIPTOR:
        if (p_write_data->val_len != 2)
        {
            WICED_BT_TRACE("ota client config wrong len %d\n", p_write_data->val_len);
            return WICED_BT_GATT_INVALID_ATTR_LEN;
        }
        ota_fw_upgrade_set_client_configuration(p_write_data->p_val[0] + (p_write_data->p_val[1] << 8));
        break;

    case HANDLE_OTA_FW_UPGRADE_DATA:
        if (!ota_fw_upgrade_handle_data(conn_id, p_write_data->p_val, p_write_data->val_len))
        {
            WICED_BT_TRACE("ota_handle_data failed.\n");
            return WICED_BT_GATT_INTERNAL_ERROR;
        }
        break;

    default:
        return WICED_BT_GATT_INVALID_HANDLE;
        break;
    }
    return WICED_BT_GATT_SUCCESS;
}

/*
 * Process GATT indication confirmation
 */
wiced_bt_gatt_status_t wiced_ota_fw_upgrade_indication_cfm_handler(uint16_t conn_id, uint16_t handle)
{
    ota_fw_upgrade_state_t *p_state = &ota_fw_upgrade_state;

    WICED_BT_TRACE("wsr_upgrade_indication_confirm state:%d \n", p_state->state);

    WICED_BT_TRACE("ota_fw_upgrade_indication_cfm_handler, conn %d hdl %d\n", conn_id, handle);

    if (handle == HANDLE_OTA_FW_UPGRADE_CONTROL_POINT)
    {
        if (p_state->state == OTA_STATE_VERIFIED)
        {
            if (ota_pre_reboot_callback)
            {
                (*ota_pre_reboot_callback)();
            }
            wiced_firmware_upgrade_finish();
        }
        return WICED_BT_GATT_SUCCESS;
    }
    return WICED_BT_GATT_INVALID_HANDLE;
}

void dump_hex(uint8_t *p, uint32_t len)
{
    uint32_t i, j;
    char     buff1[100];

    while (len != 0)
    {
        memset(buff1, 0, sizeof(buff1));
        for (i = 0; i < len && i < 32; i++)
        {
            int s1 = (*p & 0xf0) >> 4;
            int s2 = *p & 0x0f;
            buff1[i * 3]     = (s1 >= 0 && s1 <= 9) ? s1 + '0' : s1 - 10 + 'A';
            buff1[i * 3 + 1] = (s2 >= 0 && s2 <= 9) ? s2 + '0' : s2 - 10 + 'A';
            buff1[i * 3 + 2] = ' ';
            p++;
        }
        len -= i;
        if (len != 0)
            WICED_BT_TRACE("%s\n", buff1);
    }
    WICED_BT_TRACE("%s\n", buff1);
}

/*
 * verify function is called after all the data has been received and stored
 * in the NV.  The function reads back data from the NV and calculates the checksum.
 * Function returns TRUE if calculated CRC matches the one calculated by the host
 */
int32_t ota_fw_upgrade_verify()
{
    uint32_t offset;
    uint32_t crc32 = 0xffffffff;

    for (offset = 0; offset < ota_fw_upgrade_state.total_len; offset += OTA_FW_UPGRADE_READ_CHUNK)
    {
        uint8_t memory_chunk[OTA_FW_UPGRADE_READ_CHUNK];
        int32_t bytes_to_read = ((offset + OTA_FW_UPGRADE_READ_CHUNK) < ota_fw_upgrade_state.total_len) ?
                                        OTA_FW_UPGRADE_READ_CHUNK : ota_fw_upgrade_state.total_len - offset;

        wiced_firmware_upgrade_retrieve_from_nv(offset, memory_chunk, bytes_to_read);
        crc32 = update_crc(crc32, memory_chunk, bytes_to_read);
        // dump_hex(memory_chunk, bytes_to_read);
        // WICED_BT_TRACE("crc:%x\n", crc32);
    }
    crc32 = crc32 ^ 0xffffffff;

    WICED_BT_TRACE("stored crc:%4x received bytes crc:%4x recvd crc:%4x\n", crc32, ota_fw_upgrade_state.recv_crc32, ota_fw_upgrade_state.crc32);

    return (crc32 == ota_fw_upgrade_state.crc32);
}

/*
 * handle commands received over the control point
 */
wiced_bool_t ota_fw_upgrade_handle_command(uint16_t conn_id, uint8_t *data, int32_t len)
{
    uint8_t command = data[0];
    ota_fw_upgrade_state_t *p_state = &ota_fw_upgrade_state;
    uint8_t value = WICED_OTA_UPGRADE_STATUS_OK;

    WICED_BT_TRACE("OTA handle cmd:%d, state:%d\n", command, p_state->state);
    if (command == WICED_OTA_UPGRADE_COMMAND_PREPARE_DOWNLOAD)
    {
        wiced_bt_l2cap_update_ble_conn_params(ota_fw_upgrade_state.bdaddr, 6, 6, 0, 200);

        p_state->state = OTA_STATE_READY_FOR_DOWNLOAD;
        ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
        return (TRUE);
    }
    if (command == WICED_OTA_UPGRADE_COMMAND_ABORT)
    {
        p_state->state = OTA_STATE_ABORTED;
        ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
        return FALSE;
    }

    switch (p_state->state)
    {
    case OTA_STATE_IDLE:
        return (TRUE);

    case OTA_STATE_READY_FOR_DOWNLOAD:
        if (command == WICED_OTA_UPGRADE_COMMAND_DOWNLOAD)
        {
            // command to start upgrade should be accompanied by 4 bytes with the image size
            if (len < 5)
            {
                WICED_BT_TRACE("Bad Download len: %d \n", len);
                return (FALSE);
            }

            if (!wiced_firmware_upgrade_init_nv_locations())
            {
                WICED_BT_TRACE("failed init nv locations\n");
                value = WICED_OTA_UPGRADE_STATUS_INVALID_IMAGE;
                ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
                return (FALSE);
            }

            p_state->current_offset       = 0;
            p_state->current_block_offset = 0;
            p_state->total_offset         = 0;
            p_state->total_len            = data[1] + (data[2] << 8) + (data[3] << 16) + (data[4] << 24);
            p_state->recv_crc32           = 0xffffffff;
            p_state->state                = OTA_STATE_DATA_TRANSFER;

            WICED_BT_TRACE("state %d total_len %d \n", p_state->state, p_state->total_len);
            ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
            return (TRUE);
        }
        break;

    case OTA_STATE_DATA_TRANSFER:
        if (command == WICED_OTA_UPGRADE_COMMAND_VERIFY)
        {
            // command to start upgrade should be accompanied by 2 bytes with the image size
            if (len < 5)
            {
                WICED_BT_TRACE("Bad Verify len %d \n", len);
                return (FALSE);
            }
            if (p_state->total_len != p_state->total_offset)
            {
                WICED_BT_TRACE("Verify failed received:%d out of %d\n", p_state->total_offset, p_state->total_len);
                p_state->state = OTA_STATE_ABORTED;
                value = WICED_OTA_UPGRADE_STATUS_VERIFICATION_FAILED;
                ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
                return TRUE;
            }
            else
            {
                p_state->crc32 = data[1] + (data[2] << 8) + (data[3] << 16) + (data[4] << 24);
                if (ota_fw_upgrade_verify())
                {
                    WICED_BT_TRACE("Verify success\n");
                    p_state->state = OTA_STATE_VERIFIED;

                    // if we are able to send indication (good host) wait for the confirmation before the reboot
                    if (ota_client_config_descriptor & GATT_CLIENT_CONFIG_INDICATION)
                    {
                        if (wiced_bt_gatt_send_indication(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value) == WICED_BT_GATT_SUCCESS)
                        {
                            return TRUE;
                        }
                    }
                    // if we are unable to send indication, try to send notification and start 1 sec timer
                    if (ota_client_config_descriptor & GATT_CLIENT_CONFIG_NOTIFICATION)
                    {
                        if (wiced_bt_gatt_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value) == WICED_BT_GATT_SUCCESS)
                        {
                            // notify application that we are going down
                            if (ota_pre_reboot_callback)
                            {
                                (*ota_pre_reboot_callback)();
                            }
                            // init timer for detect packet retransmission
                            wiced_init_timer(&ota_fw_upgrade_state.reset_timer, ota_fw_upgrade_reset_timeout, 0, WICED_SECONDS_TIMER);
                            wiced_start_timer(&ota_fw_upgrade_state.reset_timer, 1);
                            return TRUE;
                        }
                    }
                    WICED_BT_TRACE("failed to notify the app\n");
                    return FALSE;
                }
                else
                {
                    WICED_BT_TRACE("Verify failed\n");
                    p_state->state = OTA_STATE_ABORTED;
                    value = WICED_OTA_UPGRADE_STATUS_VERIFICATION_FAILED;
                    ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
                    return TRUE;
                }
            }
        }
        break;

    case OTA_STATE_ABORTED:
    default:
        break;
    }

    value = WICED_OTA_UPGRADE_STATUS_ILLEGAL_STATE;
    ota_fw_upgrade_send_notification(conn_id, HANDLE_OTA_FW_UPGRADE_CONTROL_POINT, 1, &value);
    return FALSE;
}

/*
 * Process data chunk received from the host.  If received num of bytes equals to
 * OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT, save the data to NV.
 *
 */
wiced_bool_t ota_fw_upgrade_handle_data(uint16_t conn_id, uint8_t *data, int32_t len)
{
    ota_fw_upgrade_state_t *p_state = &ota_fw_upgrade_state;
    uint8_t *p = data;
    p_state->recv_crc32 = update_crc(p_state->recv_crc32, data, len);

    while (len)
    {
        int bytes_to_copy = 
            (p_state->current_block_offset + len) < OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT ? len: (OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT - p_state->current_block_offset);

        if ((p_state->total_offset + p_state->current_block_offset + bytes_to_copy > p_state->total_len))
        {
            WICED_BT_TRACE("Too much data. size of the image %d \n", p_state->total_len);
            WICED_BT_TRACE("offset %d, block offset %d len rcvd %d \n",
                            p_state->total_offset, p_state->current_block_offset, len);
            return (FALSE);
        }

        memcpy (&(p_state->read_buffer[p_state->current_block_offset]), p, bytes_to_copy);
        p_state->current_block_offset += bytes_to_copy;

        if ((p_state->current_block_offset == OTA_FW_UPGRADE_CHUNK_SIZE_TO_COMMIT) ||
            (p_state->total_offset + p_state->current_block_offset == p_state->total_len))
        {
            wiced_firmware_upgrade_store_to_nv(p_state->total_offset, p_state->read_buffer, p_state->current_block_offset);
            p_state->total_offset        += p_state->current_block_offset;
            p_state->current_block_offset = 0;

            if (p_state->total_offset == p_state->total_len)
            {
                p_state->recv_crc32 = p_state->recv_crc32 ^ 0xffffffff;
            }
        }

        len = len - bytes_to_copy;
        p = p + bytes_to_copy;

        //WICED_BT_TRACE("remaining len: %d \n", len);
    }
    return (TRUE);
}

/* 
 * Set new value for client configuration descriptor
 */
void ota_fw_upgrade_set_client_configuration(uint16_t client_config)
{
    ota_client_config_descriptor = client_config;
}

/*
 * Send Notification if allowed, or indication if allowed, or return error
 */
wiced_bt_gatt_status_t ota_fw_upgrade_send_notification(uint16_t conn_id, uint16_t attr_handle, uint16_t val_len, uint8_t *p_val)
{
    if (ota_client_config_descriptor & GATT_CLIENT_CONFIG_NOTIFICATION)
        return wiced_bt_gatt_send_notification(conn_id, attr_handle, val_len, p_val);
    else if (ota_client_config_descriptor & GATT_CLIENT_CONFIG_INDICATION)
        return wiced_bt_gatt_send_indication(conn_id, attr_handle, val_len, p_val);
    return WICED_BT_GATT_ERROR;
}

/*
 * Process timeout started after the last notification to perform restart
 */
void ota_fw_upgrade_reset_timeout(uint32_t param)
{
    wiced_firmware_upgrade_finish();
}
