#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
#  Corporation. All rights reserved. This software, including source code, documentation and  related 
# materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
#  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
# (United States and foreign), United States copyright laws and international treaty provisions. 
# Therefore, you may use this Software only as provided in the license agreement accompanying the 
# software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
# hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
# compile the Software source code solely for use in connection with Cypress's  integrated circuit 
# products. Any reproduction, modification, translation, compilation,  or representation of this 
# Software except as specified above is prohibited without the express written permission of 
# Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
# the Software without notice. Cypress does not assume any liability arising out of the application 
# or use of the Software or any product or circuit  described in the Software. Cypress does 
# not authorize its products for use in any products where a malfunction or failure of the 
# Cypress product may reasonably be expected to result  in significant property damage, injury 
# or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
#  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
# to indemnify Cypress against all liability.
#

NAME := pbap_lib


########################################################################
# Add Application sources here.
########################################################################
LIB_SOURCES := wiced_bt_pbc_act.c
LIB_SOURCES += wiced_bt_pbc_api.c
LIB_SOURCES += wiced_bt_pbc_cfg.c
LIB_SOURCES += wiced_bt_pbc_main.c
LIB_SOURCES += wiced_bt_pbc_utils.c
LIB_SOURCES += wiced_bt_pbc_co.c

LIB_SOURCES += bd.c
LIB_SOURCES += utl.c


LIB_OBJS := wiced_bt_pbc_act.o
LIB_OBJS += wiced_bt_pbc_api.o
LIB_OBJS += wiced_bt_pbc_cfg.o
LIB_OBJS += wiced_bt_pbc_main.o
LIB_OBJS += wiced_bt_pbc_utils.o
LIB_OBJS += wiced_bt_pbc_co.o

LIB_OBJS += bd.o
LIB_OBJS += utl.o


$(NAME)_SOURCES := $(LIB_SOURCES)

$(NAME)_OBJS := $(LIB_OBJS)


########################################################################
# C flags
########################################################################
#C_FLAGS += -DOBEX_LIB_L2CAP_INCLUDED

########################################################################
################ DO NOT MODIFY FILE BELOW THIS LINE ####################
########################################################################
include ../../Wiced-BT/spar/library.mk
