/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * Definitions used in the iAP2 protocol.  See Accessories Interface
 * Specifications from Apple for details
 */

#ifndef IAP2_DEFS__H
#define IAP2_DEFS__H

#define IAP2_VERSION                        1
#define IAP2_MAX_OUTSTANDING                5
#define IAP2_MAX_PACKET_SIZE                1007
#define IAP2_ZERO_ACK_RE_TX_TIMEOUT         0
#define IAP2_ZERO_ACK_CUM_ACK_TIMEOUT       0
#define IAP2_ZERO_ACK_MAX_RETRANSMISSIONS   0
#define IAP2_ZERO_ACK_MAX_CUM_ACKS          0
#define IAP2_RE_TX_TIMEOUT                  1500
#define IAP2_CUM_ACK_TIMEOUT                200 // 73
#define IAP2_MAX_RETRANSMISSIONS            30
#define IAP2_MAX_CUM_ACKS                   3

#define IAP2_PKT_OFFSET_LEN                 2
#define IAP2_PKT_OFFSET_CONTROL             4
#define IAP2_PKT_OFFSET_TX_SEQ              5
#define IAP2_PKT_OFFSET_RX_SEQ              6
#define IAP2_PKT_OFFSET_SESSION             7

#define kIAP2PacketSynDataBaseLen           10
#define kIAP2PacketSessionDataLen           3

#define kIAP2PacketHeaderLen                9
#define kIAP2PacketChksumLen                1

#define kIAP2PacketReservedSessionID        0

/* BitMask defines for bits in control byte */
#define kIAP2PacketControlMaskSYN           0x80    /* synchronization */
#define kIAP2PacketControlMaskACK           0x40    /* acknowledgement */
#define kIAP2PacketControlMaskEAK           0x20    /* extended acknowledgement */
#define kIAP2PacketControlMaskRST           0x10    /* reset */
#define kIAP2PacketControlMaskSUS           0x08    /* suspend (sleep) */

#define kIAP2PacketServiceTypeControl       0       /* Control & Authentication */
#define kIAP2PacketServiceTypeBuffer        1       /* Buffer (ie. Artwork, Workout) */
#define kIAP2PacketServiceTypeEA            2       /* EA stream */

#define kIAP2PacketSYNC                     0xFF
#define kIAP2PacketSOP                      0x5A
#define kIAP2PacketSOPOrig                  0x55

/* iAP 1.0/2.0 packet detect: FF 55 02 00 EE 10 */
#define kIAP2PacketDetectLEN                0x0200
#define kIAP2PacketDetectCTRL               0xEE
#define kIAP2PacketDetectSEQ                0x10

/* iAP 1.0 request identity: (FF) 55 02 00 EE 10 */
#define kIAP2PacketDetectLEN                0x0200
#define kIAP1PacketDetectNackCTRL           0x00
#define kIAP1PacketDetectNACKSEQ            0xFE

/* iAP 1.0/2.0 packet detect BAD ACK: FF 55 04 00 02 04 EE 08 */
#define kIAP2PacketDetectNACKLEN            0x0400
#define kIAP2PacketDetectNACKCTRL           0x02
#define kIAP2PacketDetectNACKSEQ            0x04
#define kIAP2PacketDetectNACKACK            0xEE
#define kIAP2PacketDetectNACKSESSID         0x08

#define IAP2_CONTROL_SESSION_HEADER_SIZE            6
#define IAP2_CONTROL_SESSION_PARAMETER_HEADER_SIZE  4

#define IAP2_EA_HEADER_SIZE                         2

//
// Control session messages (authentication)
//
#define RequestAuthenticationCertificate                                    0xAA00 /* from device */

#define AuthenticationCertificate                                           0xAA01 /* from accessory */
#define AuthenticationCertificate_id_AuthenticationCertificate              0

#define RequestAuthenticationChallengeResponse                              0xAA02 /* from device */
#define RequestAuthenticationChallengeResponse_id_AuthenticationChallenge   0

#define AuthenticationResponse                                              0xAA03 /* from accessory */
#define AuthenticationFailed                                                0xAA04 /* from device */
#define AuthenticationSucceeded                                             0xAA05 /* from device */

//
// Control session messages (identification)
//
#define StartIdentification                                                 0x1D00 /* from device */

#define IdentificationInformation                                           0x1D01 /* from accessory */
#define IdentificationInformation_id_Name                                           0
#define IdentificationInformation_id_ModelIdentifier                                1
#define IdentificationInformation_id_Manufacturer                                   2
#define IdentificationInformation_id_SerialNumber                                   3
#define IdentificationInformation_id_FirmwareVersion                                4
#define IdentificationInformation_id_HardwareVersion                                5
#define IdentificationInformation_id_MessagesSentByAccessory                        6
#define IdentificationInformation_id_MessagesReceivedFromDevice                     7
#define IdentificationInformation_id_PowerProvidingCapability                       8
#define IdentificationInformation_id_MaximumCurrentDrawnFromDevice                  9
#define IdentificationInformation_id_SupportedExternalAccessoryProtocol             10

#define ExternalAccessoryProtocolGroup_id_ExternalAccessoryProtocolIdentifier       0
#define ExternalAccessoryProtocolGroup_id_ExternalAccessoryProtocolName             1
#define ExternalAccessoryProtocolGroup_id_ExternalAccessoryMatchAction              2
#define ExternalAccessoryProtocolGroup_id_NativeTransportComponentIdentifier        3

#define IdentificationInformation_id_AppMatchTeamID                                 11
#define IdentificationInformation_id_CurrentLanguage                                12
#define IdentificationInformation_id_SupportedLanguage                              13
#define IdentificationInformation_id_BluetoothTransportComponent                    17

#define BluetoothTransportComponent_id_TransportComponentIdentifier                 0
#define BluetoothTransportComponent_id_TransportComponentName                       1
#define BluetoothTransportComponent_id_TransportSupportsiAP2Connection              2
#define BluetoothTransportComponent_id_BluetoothTransportMediaAccessControlAddress  3

#define IdentificationInformation_id_LocationInformationComponent                   22
#define IdentificationInformation_id_BluetoothHIDComponent                          22

#define IdentificationAccepted                                              0x1D02 /* from device */

// a subset of IdentificationInformation parameters are used as is */
#define IdentificationRejected                                              0x1D03 /* from device */

#define CancelIdentification                                                0x1D05 /* from accessory */

// a subset of IdentificationInformation parameters are used as is */
#define IdentificationInformationUpdate                                     0x1D06 /* from accessory */

//
// Control session message (app launch)
//
#define RequestAppLaunch                                                    0xEA02 /* from accessory */

//
// Control session messages (Bluetooth connection)
//
#define BluetoothComponentInformation                                       0x4E01 /* from accessory */
#define BluetoothComponentInformation_id_BluetoothComponentStatus           0

#define StartBluetoothConnectionUpdate                                      0x4E03 /* from accessory */
#define StartBluetoothConnectionUpdate_id_BluetoothTransportComponentIdentifier     0

#define BluetoothConnectionUpdate                                           0x4E04 /* from device */
#define BluetoothConnectionUpdate_id_BluetoothTransportComponentIdentifier          0
#define BluetoothConnectionUpdate_id_ConnectedBluetoothProfiles                     1

#define StopBluetoothConnectionUpdate                                       0x4E05 /* from accessory */

//
// Control session messages (EA protocol)
//
#define StartExternalAccessoryProtocolSession                               0xEA00 /* from device */
#define StartExternalAccessoryProtocolSession_id_ExternalAccessoryProtocolIdentifier        0
#define StartExternalAccessoryProtocolSession_id_ExternalAccessoryProtocolSessionIdentifier 1

#define StopExternalAccessoryProtocolSession                                0xEA01 /* from device */
#define StopExternalAccessoryProtocolSession_id_ExternalAccessoryProtocolSessionIdentifier  0

#define StatusExternalAccessoryProtocolSession                              0xEA03 /* from accessory */
#define StatusExternalAccessoryProtocolSession_id_ExternalAccessoryProtocolSessionIdentifier 0
#define StatusExternalAccessoryProtocolSession_id_ExternalAccessoryProtocolSessionStatus 1

#endif
