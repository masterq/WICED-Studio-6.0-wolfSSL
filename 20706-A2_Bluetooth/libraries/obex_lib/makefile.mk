#
# Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
#  Corporation. All rights reserved. This software, including source code, documentation and  related 
# materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
#  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
# (United States and foreign), United States copyright laws and international treaty provisions. 
# Therefore, you may use this Software only as provided in the license agreement accompanying the 
# software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
# hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
# compile the Software source code solely for use in connection with Cypress's  integrated circuit 
# products. Any reproduction, modification, translation, compilation,  or representation of this 
# Software except as specified above is prohibited without the express written permission of 
# Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
# OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
# AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
# the Software without notice. Cypress does not assume any liability arising out of the application 
# or use of the Software or any product or circuit  described in the Software. Cypress does 
# not authorize its products for use in any products where a malfunction or failure of the 
# Cypress product may reasonably be expected to result  in significant property damage, injury 
# or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
#  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
# to indemnify Cypress against all liability.
#

NAME := obex_lib

########################################################################
# Add Application sources here.
########################################################################
LIB_SOURCES := obx_cact.c
LIB_SOURCES += obx_capi.c
LIB_SOURCES += obx_csm.c
LIB_SOURCES += obx_hdrs.c
LIB_SOURCES += obx_l2c.c
LIB_SOURCES += obx_main.c
LIB_SOURCES += obx_md5.c
LIB_SOURCES += obx_rfc.c
LIB_SOURCES += obx_sact.c
LIB_SOURCES += obx_sapi.c
LIB_SOURCES += obx_ssm.c
LIB_SOURCES += obx_utils.c
LIB_SOURCES += utfc.c

LIB_OBJS := obx_cact.o
LIB_OBJS += obx_capi.o
LIB_OBJS += obx_csm.o
LIB_OBJS += obx_hdrs.o
LIB_OBJS += obx_l2c.o
LIB_OBJS += obx_main.o
LIB_OBJS += obx_md5.o
LIB_OBJS += obx_rfc.o
LIB_OBJS += obx_sact.o
LIB_OBJS += obx_sapi.o
LIB_OBJS += obx_ssm.o
LIB_OBJS += obx_utils.o
LIB_OBJS += utfc.o

$(NAME)_SOURCES := $(LIB_SOURCES)

$(NAME)_OBJS := $(LIB_OBJS)

########################################################################
# C flags
########################################################################
C_FLAGS += -DOBEX_CLIENT_INCLUDED=TRUE
C_FLAGS += -DOBEX_SERVER_INCLUDED=TRUE
#C_FLAGS += -DOBEX_LIB_L2CAP_INCLUDED
C_FLAGS += -DOBEX_LIB_SESSION_SUPPORTED

########################################################################
################ DO NOT MODIFY FILE BELOW THIS LINE ####################
########################################################################
include ../../Wiced-BT/spar/library.mk
