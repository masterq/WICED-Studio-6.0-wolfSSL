#
# Copyright 2016, Cypress Semiconductor
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Cypress Semiconductor.
#

NAME := hidh_lib

$(NAME)_SOURCES := wiced_bt_hidh_api.c
$(NAME)_SOURCES += wiced_bt_hidh_con.c
$(NAME)_SOURCES += wiced_bt_hidh_core.c
$(NAME)_SOURCES += wiced_bt_hidh_utils.c
$(NAME)_SOURCES += wiced_bt_hidh_sdp.c

########################################################################
################ DO NOT MODIFY FILE BELOW THIS LINE ####################
########################################################################
include ../../Wiced-BT/spar/library.mk
