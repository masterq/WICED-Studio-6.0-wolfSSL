Host app
++++++++
A Host application is the end user application running on the host MCU and 
contains the customer business logic. The Host application controls the embedded
application via WICED HCI commands. See the \apps\host folder.

Client Control
    This is multi-platform sample UI application for controlling various 
    embedded applications. The application is developed using QT IDE. 
    See apps\host\client_control\README.txt. 
