/*
 * Copyright 2016, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * This file implements the entry point of the Wiced Application
 */
#include "wiced.h"
#include "wiced_bt_dev.h"
#include "wiced_app.h"
#include "sparcommon.h"
#include "wiced_bt_trace.h"
#include "hci_control_api.h"

/*
 *  Application Start, ie, entry point to the application.
 */
APPLICATION_START( )
{
    // Initialize HCI
    hci_control_init();

#if (WICED_APP_LE_INCLUDED == TRUE)
    hci_control_le_init();
#endif

    WICED_BT_TRACE( "Wiced Headset Application Started...\n" );
    /* first and foremost send DEVICE_INIT event */
    //wiced_transport_send_data(HCI_CONTROL_EVENT_DEVICE_INIT, NULL, 0);

}
