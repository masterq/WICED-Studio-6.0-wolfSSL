/*
 * Copyright 2016, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * This file implement BTLE application controlled over UART.
 * The GATT database is defined in this file and is not changed by the MCU.
 *
 */


#ifndef _HCI_CONTROL_LE_H_
#define _HCI_CONTROL_LE_H_

#include "wiced_app.h"

void hci_control_le_init( void );
void hci_control_le_enable( void );


#endif /* _HCI_CONTROL_LE_H_ */
