#
# Copyright 2016, Cypress Semiconductor
# All Rights Reserved.
#
# This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
# the contents of this file may not be disclosed to third parties, copied
# or duplicated in any form, in whole or in part, without the prior
# written permission of Cypress Semiconductor.
#
NAME := headset

########################################################################
# Add Application sources here.
########################################################################

$(NAME)_COMPONENTS := a2dp_sink_profile.a
$(NAME)_COMPONENTS += handsfree_profile.a
$(NAME)_COMPONENTS += avrc_controller.a
APP_PATCHES_AND_LIBS += wiced_audio_sink.a
APP_PATCHES_AND_LIBS += wiced_voice_path.a

APP_SRC += wiced_app.c
APP_SRC += hci_control.c
APP_SRC += hci_control_le.c
APP_SRC += hci_control_audio.c
APP_SRC += wiced_app_cfg.c
APP_SRC += remote_controller/hci_control_rc.c

APP_SRC += handsfree/handsfree.c
APP_SRC += handsfree/handsfree_utils.c
APP_SRC += handsfree/handsfree_wiced_hci.c

INCS += ../../Apps/headset/handsfree


########################################################################
# C flags
# To use SPI transport, append TRANSPORT=SPI to the make target, 
# for example "demo.headset-CYW920706WCDEVAL TRANSPORT=SPI download"
########################################################################

#FLAG CHECK FOR STAND-ALONE/MCU-DRIVEN
C_FLAGS += -DSTANDALONE_HEADSET_APP

C_FLAGS += -DWICED_BT_TRACE_ENABLE
C_FLAGS += -DAVRC_ADV_CTRL_INCLUDED
C_FLAGS += -DAVRC_METADATA_INCLUDED
# Max Number of HF connections supported.
# Default value is 2, please update this macro based on requirement
C_FLAGS += -DWICED_BT_HFP_HF_MAX_CONN=2

########################################################################
################ DO NOT MODIFY FILE BELOW THIS LINE ####################
########################################################################
