/*
 * Copyright 2016, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * Runtime Bluetooth stack configuration parameters
 *
 */

#ifndef _WICED_APP_CFG_H_
#define _WICED_APP_CFG_H_


#include "wiced_bt_cfg.h"
#include "wiced_bt_audio.h"

extern const const wiced_bt_cfg_settings_t wiced_bt_cfg_settings;
extern const wiced_bt_cfg_buf_pool_t wiced_app_cfg_buf_pools[];
extern const wiced_bt_audio_config_buffer_t wiced_bt_audio_buf_config;
extern int wiced_app_cfg_get_num_buf_pools(void);

#endif /* _WICED_APP_CFG_H_ */
