/*
 * Copyright 2016, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * Runtime Bluetooth stack configuration parameters
 *
 */
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_cfg.h"
#include "wiced_bt_sdp.h"
#include "wiced_memory.h"
#include "handsfree.h"
#include "wiced_bt_audio.h"

/*****************************************************************************
 * wiced_bt core stack configuration
 ****************************************************************************/

uint8_t uuid_list[] =
{
    0x08, 0x11, /* Headset */
    0x1E, 0x11, /* Handsfree */
};

const wiced_bt_cfg_settings_t handsfree_cfg_settings =
{
    ( uint8_t* )HANDS_FREE_DEVICE_NAME,                                     /**< Local device name ( NULL terminated ) */
    {0x24, 0x04, 0x18},                                           /**< Local device class */
    ( BTM_SEC_IN_AUTHENTICATE | BTM_SEC_OUT_AUTHENTICATE | BTM_SEC_ENCRYPT ),
    /**< Security requirements mask ( BTM_SEC_NONE, or combination of BTM_SEC_IN_AUTHENTICATE, BTM_SEC_OUT_AUTHENTICATE, BTM_SEC_ENCRYPT ( see #wiced_bt_sec_level_e ) ) */
    /**< Security requirements mask ( BTM_SEC_NONE, or combinination of BTM_SEC_IN_AUTHENTICATE, BTM_SEC_OUT_AUTHENTICATE, BTM_SEC_ENCRYPT ( see #wiced_bt_sec_level_e ) ) */

    3,                                                              /**< Maximum number simultaneous links to different devices */

    /* BR/EDR scan config */
    {
        BTM_SCAN_TYPE_STANDARD,                                     /**< Inquiry scan type ( BTM_SCAN_TYPE_STANDARD or BTM_SCAN_TYPE_INTERLACED ) */
        WICED_BT_CFG_DEFAULT_INQUIRY_SCAN_INTERVAL,                 /**< Inquiry scan interval  ( 0 to use default ) */
        WICED_BT_CFG_DEFAULT_INQUIRY_SCAN_WINDOW,                   /**< Inquiry scan window ( 0 to use default ) */

        BTM_SCAN_TYPE_STANDARD,                                     /**< Page scan type ( BTM_SCAN_TYPE_STANDARD or BTM_SCAN_TYPE_INTERLACED ) */
        WICED_BT_CFG_DEFAULT_PAGE_SCAN_INTERVAL,                    /**< Page scan interval  ( 0 to use default ) */
        WICED_BT_CFG_DEFAULT_PAGE_SCAN_WINDOW                       /**< Page scan window ( 0 to use default ) */
    },

    /* BLE scan settings  */
    {
        BTM_BLE_SCAN_MODE_ACTIVE,                                   /**< BLE scan mode ( BTM_BLE_SCAN_MODE_PASSIVE, BTM_BLE_SCAN_MODE_ACTIVE, or BTM_BLE_SCAN_MODE_NONE ) */

        /* Advertisement scan configuration */
        96,                 /**< High duty scan interval */
        48,                 /**< High duty scan window */
        30,                                                         /**< High duty scan duration in seconds ( 0 for infinite ) */

        2048,                /**< Low duty scan interval  */
        48,                  /**< Low duty scan window */
        30,                                                         /**< Low duty scan duration in seconds ( 0 for infinite ) */

        /* Connection scan configuration */
        96,            /**< High duty cycle connection scan interval */
        48,            /**< High duty cycle connection scan window */
        30,                                                         /**< High duty cycle connection duration in seconds ( 0 for infinite ) */

        2048,           /**< Low duty cycle connection scan interval */
        48,             /**< Low duty cycle connection scan window */
        30,                                                         /**< Low duty cycle connection duration in seconds ( 0 for infinite ) */

        /* Connection configuration */
        WICED_BT_CFG_DEFAULT_CONN_MIN_INTERVAL,                     /**< Minimum connection interval */
        WICED_BT_CFG_DEFAULT_CONN_MAX_INTERVAL,                     /**< Maximum connection interval */
        WICED_BT_CFG_DEFAULT_CONN_LATENCY,                          /**< Connection latency */
        WICED_BT_CFG_DEFAULT_CONN_SUPERVISION_TIMEOUT,              /**< Connection link supervision timeout */
    },

    /* BLE advertisement settings */
    {
        BTM_BLE_ADVERT_CHNL_37 |                                    /**< Advertising channel map ( mask of BTM_BLE_ADVERT_CHNL_37, BTM_BLE_ADVERT_CHNL_38, BTM_BLE_ADVERT_CHNL_39 ) */
        BTM_BLE_ADVERT_CHNL_38 |
        BTM_BLE_ADVERT_CHNL_39,

        WICED_BT_CFG_DEFAULT_HIGH_DUTY_ADV_MIN_INTERVAL,            /**< High duty undirected connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_HIGH_DUTY_ADV_MAX_INTERVAL,            /**< High duty undirected connectable maximum advertising interval */
        30,                                                         /**< High duty undirected connectable advertising duration in seconds ( 0 for infinite ) */

        WICED_BT_CFG_DEFAULT_LOW_DUTY_ADV_MIN_INTERVAL,             /**< Low duty undirected connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_LOW_DUTY_ADV_MAX_INTERVAL,             /**< Low duty undirected connectable maximum advertising interval */
        60,                                                         /**< Low duty undirected connectable advertising duration in seconds ( 0 for infinite ) */

        WICED_BT_CFG_DEFAULT_HIGH_DUTY_DIRECTED_ADV_MIN_INTERVAL,   /**< High duty directed connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_HIGH_DUTY_DIRECTED_ADV_MAX_INTERVAL,   /**< High duty directed connectable maximum advertising interval */

        WICED_BT_CFG_DEFAULT_LOW_DUTY_DIRECTED_ADV_MIN_INTERVAL,    /**< Low duty directed connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_LOW_DUTY_DIRECTED_ADV_MAX_INTERVAL,    /**< Low duty directed connectable maximum advertising interval */
        30,                                                         /**< Low duty directed connectable advertising duration in seconds ( 0 for infinite ) */

        WICED_BT_CFG_DEFAULT_HIGH_DUTY_NONCONN_ADV_MIN_INTERVAL,    /**< High duty non-connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_HIGH_DUTY_NONCONN_ADV_MAX_INTERVAL,    /**< High duty non-connectable maximum advertising interval */
        30,                                                         /**< High duty non-connectable advertising duration in seconds ( 0 for infinite ) */

        WICED_BT_CFG_DEFAULT_LOW_DUTY_NONCONN_ADV_MIN_INTERVAL,     /**< Low duty non-connectable minimum advertising interval */
        WICED_BT_CFG_DEFAULT_LOW_DUTY_NONCONN_ADV_MAX_INTERVAL,     /**< Low duty non-connectable maximum advertising interval */
        0                                                           /**< Low duty non-connectable advertising duration in seconds ( 0 for infinite ) */
    },

    /* GATT configuration */
    {
        APPEARANCE_GENERIC_TAG,                                     /**< GATT appearance ( see gatt_appearance_e ) */
        3,                                                          /**< Client config: maximum number of servers that local client can connect to  */
        3,                                                          /**< Server config: maximum number of remote clients connections allowed by the local */
        512                                                         /**< Clinet config: maximum number of bytes that local client can reciver over LE link  */
    },

    /* RFCOMM configuration */
    {
        7,                                                          /**< Maximum number of simultaneous RFCOMM ports */
        7                                                           /**< Maximum number of simultaneous RFCOMM connections */
    },

    /* Application managed l2cap protocol configuration */
    {
        2,                                                          /**< Maximum number of application-managed l2cap links (BR/EDR and LE) */

        /* BR EDR l2cap configuration */
        7,                                                          /**< Maximum number of application-managed BR/EDR PSMs */
        7,                                                          /**< Maximum number of application-managed BR/EDR channels  */

        /* LE L2cap connection-oriented channels configuration */
        0,                                                          /**< Maximum number of application-managed LE PSMs */
        0,                                                          /**< Maximum number of application-managed LE channels */
    },

    /* Audio/Video Distribution configuration */
    {
        1,                                                          /**< Maximum simultaneous audio/video links */
    },

    /* Audio/Video Remote Control configuration */
    {
        1,                                                           /**< 1 if AVRC_CONN_ACCEPTOR is supported */
        1,                                                           /**< Maximum simultaneous remote control links */
    },
    5,                                                               /**< LE Address Resolution DB settings - effective only for pre 4.2 controller*/
    517,                                                              /**< Maximum MTU size for GATT connections, should be between 23 and (max_attr_len + 5 )*/
    12
};


/*****************************************************************************
 * SDP database for the handsfree application
 ****************************************************************************/

const uint8_t handsfree_sdp_db[] =
{
#if (!defined WICED_ENABLE_BT_HSP_PROFILE)
    SDP_ATTR_SEQUENCE_1(77),
#else
    SDP_ATTR_SEQUENCE_1(154),
#endif
        // SDP Record for Hands-Free Unit
        SDP_ATTR_SEQUENCE_1(75),
            SDP_ATTR_RECORD_HANDLE(HDLR_HANDS_FREE_UNIT),
            SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST), SDP_ATTR_SEQUENCE_1(6),
                SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),
                SDP_ATTR_UUID16(UUID_SERVCLASS_GENERIC_AUDIO),
            SDP_ATTR_RFCOMM_PROTOCOL_DESC_LIST(HANDS_FREE_SCN),
            SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),
                SDP_ATTR_SEQUENCE_1(6),
                    SDP_ATTR_UUID16(UUID_SERVCLASS_HF_HANDSFREE),
                    SDP_ATTR_VALUE_UINT2(0x0107),
            SDP_ATTR_SERVICE_NAME(15),
                'W', 'I', 'C', 'E', 'D', ' ', 'H', 'F', ' ', 'D', 'E', 'V', 'I', 'C', 'E',
            SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x0016),
#if (defined WICED_ENABLE_BT_HSP_PROFILE)

        // SDP Record for Hands-Free Unit
        SDP_ATTR_SEQUENCE_1(75),
            SDP_ATTR_RECORD_HANDLE(HDLR_HEADSET_UNIT),
            SDP_ATTR_ID(ATTR_ID_SERVICE_CLASS_ID_LIST), SDP_ATTR_SEQUENCE_1(6),
                SDP_ATTR_UUID16(UUID_SERVCLASS_HEADSET),
                SDP_ATTR_UUID16(UUID_SERVCLASS_GENERIC_AUDIO),
            SDP_ATTR_RFCOMM_PROTOCOL_DESC_LIST(HEADSET_SCN),
            SDP_ATTR_ID(ATTR_ID_BT_PROFILE_DESC_LIST), SDP_ATTR_SEQUENCE_1(8),
                SDP_ATTR_SEQUENCE_1(6),
                    SDP_ATTR_UUID16(UUID_SERVCLASS_HEADSET_HS),
                    SDP_ATTR_VALUE_UINT2(0x0102),
            SDP_ATTR_SERVICE_NAME(15),
                'W', 'I', 'C', 'E', 'D', ' ', 'H', 'S', ' ', 'D', 'E', 'V', 'I', 'C', 'E',
            SDP_ATTR_UINT2(ATTR_ID_SUPPORTED_FEATURES, 0x0016),
#endif
};

/*****************************************************************************
 * wiced_bt  buffer pool configuration
 *
 * Configure buffer pools used by the stack  according to application's requirement
 *
 * Pools must be ordered in increasing buf_size.
 * If a pool runs out of buffers, the next  pool will be used
 *****************************************************************************/

const wiced_bt_cfg_buf_pool_t handsfree_cfg_buf_pools[] =
{
/*  { buf_size, buf_count } */
    { 64,      12  },      /* Small Buffer Pool */
    { 272,      6  },      /* Medium Buffer Pool (used for HCI & RFCOMM control messages, min recommended size is 360) */
    { 1056,     6  },      /* Large Buffer Pool  (used for HCI ACL messages) */
    { 1056,     1  },      /* Extra Large Buffer Pool - Used for avdt media packets and miscellaneous (if not needed, set buf_count to 0) */
};

/**  Audio buffer configuration configuration */
const wiced_bt_audio_config_buffer_t handsfree_audio_buf_config = {
    .role                       =   WICED_HF_ROLE,
    .audio_tx_buffer_size       =   0,
    .audio_codec_buffer_size    =   0x3400
};

/*
 * wiced_app_cfg_sdp_record_get_size
 */
uint16_t wiced_app_cfg_sdp_record_get_size(void)
{
    return (uint16_t)sizeof(handsfree_sdp_db);
}
