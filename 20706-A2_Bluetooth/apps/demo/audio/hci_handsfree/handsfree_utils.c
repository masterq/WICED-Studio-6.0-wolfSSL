#include <stdlib.h>
#include <string.h>

/*
 * This utility copies a character string to another
 */
char *utl_strcpy( char *p_dst, char *p_src )
{
    register char *pd = p_dst;
    register char *ps = p_src;

    while ( *ps )
        *pd++ = *ps++;

    *pd++ = 0;
    return ( p_dst );
}

/*
 * This utility counts the characteers in a string
 * Returns  number of characters ( excluding the terminating '0' )
 */
int utl_strlen( char *p_str )
{
    register int  xx = 0;

    while ( *p_str++ != 0 )
        xx++;

    return ( xx );
}

/*
 * This utility function converts a UINT16 to a string.  The string is NULL-terminated.
 * Returns Length of string.
 */
int util_itoa(int i, char *p_s)
{
    int         j, k;
    char        *p = p_s;
    int         fill = 0;

    if (i == 0)
    {
        /* take care of zero case */
        *p++ = '0';
    }
    else
    {
        for(j = 10000; j > 0; j /= 10)
        {
            k = i / j;
            i %= j;
            if (k > 0 || fill)
            {
              *p++ = k + '0';
              fill = 1;
            }
        }
    }
    *p = 0;
    return (int) (p - p_s);
}
