/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file provides the private interface definitions for hci_control app
 *
 */
#ifndef HCI_CONTROL_SPP_H
#define HCI_CONTROL_SPP_H

/*****************************************************************************
**  Constants that define the capabilities and configuration
*****************************************************************************/
#define SPP_RFCOMM_SCN                  1
#define SPP_DEVICE_MTU                  1024
#define WICED_BUFF_MAX_SIZE             264
#define SPP_TRANS_MAX_BUFFERS           10
#define TRANS_UART_BUFFER_SIZE          1024

/*****************************************************************************
**  Data types
*****************************************************************************/

/* SPP control block */
typedef struct
{
#define     HCI_CONTROL_SPP_STATE_IDLE       0
#define     HCI_CONTROL_SPP_STATE_OPENING    1
#define     HCI_CONTROL_SPP_STATE_OPEN       2
#define     HCI_CONTROL_SPP_STATE_CLOSING    3

    uint8_t             state;                  /* state machine state */

    uint8_t             b_is_initiator;         /* initiator of the connection ( true ) or acceptor ( false ) */

    uint16_t            rfc_serv_handle;        /* RFCOMM server handle */
    uint16_t            rfc_conn_handle;        /* RFCOMM handle of connected service */
    uint8_t             server_scn;             /* server's scn */
    BD_ADDR             server_addr;            /* server's bd address */

    uint16_t            pending_bytes;          /* number of bytes waiting for transmission */
    uint8               pending_buffer[HCI_CONTROL_SPP_MAX_TX_BUFFER];   /* pending buffer */

    wiced_bt_sdp_discovery_db_t *p_sdp_discovery_db;                     /* pointer to discovery database */

} hci_control_spp_session_cb_t;

extern hci_control_spp_session_cb_t hci_control_spp_scb;

/*****************************************************************************
**  Global data
*****************************************************************************/

/*****************************************************************************
**  Function prototypes
*****************************************************************************/
/* main functions */
extern void     hci_control_send_command_status_evt( uint16_t code, uint8_t status );

/* SPP RFCOMM functions */
extern void     hci_control_spp_handle_command( uint16_t cmd_opcode, uint8_t* p, uint32_t data_len );
extern void     hci_control_spp_rfcomm_start_server( hci_control_spp_session_cb_t *p_scb );
extern void     hci_control_spp_rfcomm_do_close( hci_control_spp_session_cb_t *p_scb );
extern void     hci_control_spp_connect( BD_ADDR bd_addr );
extern void     hci_control_spp_disconnect( uint16_t handle );
extern void     hci_control_spp_send_data( uint16_t handle, uint8_t *p_data, uint32_t length );

#endif /* BTA_HS_INT_H */



