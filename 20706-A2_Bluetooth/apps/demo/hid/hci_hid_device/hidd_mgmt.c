/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * This file contains the HID Device Management logic
 *
 *  NOTE: In the interest of keeping code small, there is an
 *        inherent assumption that L2CAP always runs a timer,
 *        and so the HID management never needs a timer on L2CAP
 *        actions like connect and disconnect.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "hidd_int.h"

/********************************************************************************/
/*              L O C A L    F U N C T I O N     P R O T O T Y P E S            */
/********************************************************************************/
MAY_BE_STATIC tHID_STATUS hidd_no_conn_proc_evt( uint8_t event, void *p_data );
MAY_BE_STATIC tHID_STATUS hidd_connecting_proc_evt( uint8_t event, void *p_data );
MAY_BE_STATIC tHID_STATUS hidd_connected_proc_evt( uint8_t event, void *p_data );
MAY_BE_STATIC tHID_STATUS hidd_disc_ing_proc_evt( uint8_t event, void *p_data );


/* State machine jump table.
*/
tHIDD_MGMT_EVT_HDLR  * const hidd_sm_proc_evt[] =
{
    hidd_no_conn_proc_evt,
    hidd_connecting_proc_evt,
    hidd_connected_proc_evt,
    hidd_disc_ing_proc_evt
};

/* Main HID control block */
tHIDDEV_CB   hidd_cb;

/*******************************************************************************
**
** Function         hidd_mgmt_process_evt
**
** Description      This function is called by other modules to report an event
**
** Returns          tHID_STATUS
**
*******************************************************************************/
tHID_STATUS hidd_mgmt_process_evt( uint8_t event, void *p_data )
{
    HIDD_TRACE_DBG("hidd_mgmt_process_evt known:%d, s:%d, e:%d", hidd_cb.host_known,
            hidd_cb.dev_state, event);

    if( hidd_cb.host_known )
    {
        return ((hidd_sm_proc_evt[hidd_cb.dev_state]) (event, p_data)); /* Event is passed to main State Machine */
    }

    if( event == HOST_CONN_OPEN )
    {
        hidd_cb.host_known = WICED_TRUE;
        memcpy( hidd_cb.host_addr, (BD_ADDR *) p_data, BD_ADDR_LEN ) ;
        hidd_cb.dev_state = HID_DEV_ST_CONNECTED ;
        /* Call-Back the Application with this information */
        hidd_cb.callback(WICED_BT_HIDD_EVT_OPEN, 0, (wiced_bt_hidd_event_data_t *) hidd_cb.host_addr ) ;
        return( HID_SUCCESS );
    }

    return( HID_ERR_HOST_UNKNOWN );
}

/*******************************************************************************
**
** Function         hidd_no_conn_proc_evt
**
** Description      NO-CONN state event handler.
**
** Returns          tHID_STATUS
**
*******************************************************************************/
MAY_BE_STATIC tHID_STATUS hidd_no_conn_proc_evt( uint8_t event, void *p_data )
{
    tHID_STATUS st = HID_SUCCESS;

    switch( event )
    {
    case HOST_CONN_OPEN:
        hidd_cb.dev_state = HID_DEV_ST_CONNECTED ;
        hidd_cb.callback(WICED_BT_HIDD_EVT_OPEN, 0, (wiced_bt_hidd_event_data_t *) hidd_cb.host_addr ) ;
        break;
    case HID_API_CONNECT:
        hidd_cb.conn_tries = 1;
        hidd_cb.dev_state = HID_DEV_ST_CONNECTING ;
        if( (st = hidd_conn_initiate()) != HID_SUCCESS )
        {
#if HID_DEV_MAX_CONN_RETRY > 0
            wiced_start_timer(&hidd_cb.conn.retry_timer, HID_DEV_REPAGE_WIN);
            return HID_SUCCESS;
#else
            hidd_cb.dev_state = HID_DEV_ST_NO_CONN ;
#endif
        }
        break;
        /* when HID connection not fully opened, still allow sending data in control channel */
    case HID_API_SEND_DATA: /* Send Input reports, handshake or virtual-unplug */
        st = hidd_conn_snd_data( (tHID_SND_DATA_PARAMS *) p_data );
        break;

    default:
        st = HID_ERR_NO_CONNECTION;
    }

    return st;
}

/*******************************************************************************
**
** Function         hidd_proc_repage_timeout
**
** Description      function to handle timeout.
**
** Returns          void
**
*******************************************************************************/
void hidd_proc_repage_timeout (uint32_t arg)
{
    HIDD_TRACE_DBG("hidd_proc_repage_timeout");

    hidd_cb.conn_tries++;
    if( hidd_conn_initiate() != HID_SUCCESS )
    {
        // retry 2 times
        if( hidd_cb.conn_tries > 1 )
        {
            hidd_cb.dev_state = HID_DEV_ST_NO_CONN ;
            hidd_cb.callback(WICED_BT_HIDD_EVT_CLOSE, 0, NULL );
        }
        else
            wiced_start_timer(&hidd_cb.conn.retry_timer, HID_DEV_REPAGE_WIN);
    }
    else
        hidd_cb.callback( WICED_BT_HIDD_EVT_RETRYING, hidd_cb.conn_tries, NULL );
}

/*******************************************************************************
**
** Function         hidd_connecting_proc_evt
**
** Description      CONNECTING state event handler
**
** Returns          tHID_STATUS
**
*******************************************************************************/
MAY_BE_STATIC tHID_STATUS hidd_connecting_proc_evt( uint8_t event, void *p_data )
{
    switch( event )
    {
    case HOST_CONN_OPEN:
        hidd_cb.dev_state = HID_DEV_ST_CONNECTED ;
        hidd_cb.callback(WICED_BT_HIDD_EVT_OPEN, 0, (wiced_bt_hidd_event_data_t *) hidd_cb.host_addr ) ;
        break;

    case HOST_CONN_FAIL:
        // retry 2 times
        if( hidd_cb.conn_tries > 1 )
        {
            uint16_t reason = *( (uint16_t *) p_data);

            hidd_cb.dev_state = HID_DEV_ST_NO_CONN;
            hidd_cb.callback(WICED_BT_HIDD_EVT_CLOSE, reason, NULL );
        }
#if HID_DEV_MAX_CONN_RETRY > 0
        else
        {
            wiced_start_timer(&hidd_cb.conn.retry_timer, HID_DEV_REPAGE_WIN);
        }
#endif
        break;

    case HOST_CONN_CLOSE:
    case HOST_CONN_LOST:
        hidd_cb.dev_state = HID_DEV_ST_NO_CONN;
        hidd_cb.callback(WICED_BT_HIDD_EVT_CLOSE, *((uint16_t *) p_data), NULL );
        break;

    case HID_API_DISCONNECT:
        hidd_cb.dev_state = HID_DEV_ST_NO_CONN ;
        wiced_stop_timer(&hidd_cb.conn.retry_timer);
        hidd_conn_disconnect();
        break;

    /* When HID connection not fully opened, still allow sending data in control channel */
    /* This is an HID 1.1 feature, but is needed for some HID Host */
    case HID_API_SEND_DATA:
        return hidd_conn_snd_data( (tHID_SND_DATA_PARAMS *) p_data );

    default:
        return( HID_ERR_CONN_IN_PROCESS ) ;
    }

    return (HID_SUCCESS);
}

/*******************************************************************************
**
** Function         hidd_mgmt_conn_closed
**
** Description      Called when l2cap channels have been released
**
** Returns          void
**
*******************************************************************************/
void hidd_mgmt_conn_closed( uint16_t reason )
{
    if( hidd_cb.unplug_on )
    {
        hidd_cb.host_known = WICED_FALSE; /* This allows the device to accept connection from other hosts */
    }

    hidd_cb.dev_state = HID_DEV_ST_NO_CONN;
    hidd_cb.callback(WICED_BT_HIDD_EVT_CLOSE, reason, NULL );
}

/*******************************************************************************
**
** Function         hidd_connected_proc_evt
**
** Description      CONNECTED state event handler
**
** Returns          tHID_STATUS
**
*******************************************************************************/
MAY_BE_STATIC tHID_STATUS hidd_connected_proc_evt( uint8_t event, void *p_data )
{
    switch( event )
    {
    case HOST_CONN_LOST:
#if HID_DEV_RECONN_INITIATE == TRUE
        hidd_cb.dev_state = HID_DEV_ST_CONNECTING ;
        hidd_cb.conn_tries = 0;
        wiced_start_timer(&hidd_cb.conn.retry_timer, HID_DEV_REPAGE_WIN);
#else
        hidd_mgmt_conn_closed( *((uint16_t *) p_data) ) ;
#endif
        break;
    case HOST_CONN_CLOSE:
        hidd_mgmt_conn_closed( *((uint16_t *) p_data) ) ;
        break;
    case HID_API_DISCONNECT:
        hidd_cb.dev_state = HID_DEV_ST_DISC_ING ;
        hidd_conn_disconnect();
        break;
    case HID_API_SEND_DATA: /*Send Input reports, handshake or virtual-unplug */
        return hidd_conn_snd_data( (tHID_SND_DATA_PARAMS *) p_data );
    default:
        return( HID_ERR_ALREADY_CONN ) ;
    }

    return( HID_SUCCESS );
}

/*******************************************************************************
**
** Function         hidd_disc_ing_proc_evt
**
** Description      DISCONNECTING state event handler
**
** Returns          tHID_STATUS
**
*******************************************************************************/
MAY_BE_STATIC tHID_STATUS hidd_disc_ing_proc_evt( uint8_t event, void *p_data )
{
    switch( event )
    {
    case HOST_CONN_LOST:
    case HOST_CONN_FAIL:
    case HOST_CONN_CLOSE:
        hidd_mgmt_conn_closed( *((uint16_t *) p_data) ) ;
        break;
    default:
        return( HID_ERR_DISCONNECTING ) ;
    }

    return( HID_SUCCESS );
}


