/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
 *
 * HID Device Sample Application for 20706 devices.
 *
 * This application demonstrates two chip HID implementation.  The standard
 * Bluetooth controller, core Bluetooth host stack and HID profile
 * implementation fully implemented on the 20706 device.  The role of the
 * second MCU is to implement application specific logic.  This sample
 * assume host MCU to handle NVRAM access.  At the startup the MCU should
 * download the 20706 configuration file, and if appropriate paired host data.
 * This application assumes MCU to be connected over the UART although minor
 * changes are required to support different interface, for example SPI.
 *
 * The sample app performs as a Bluetooth keyboard.  To modify the behavior
 * change HID descriptor in the wiced_bt_sdp_db.c file.
 *
 * The sample Windows ClientControl application is provided to show sample
 * MCU implementation on Windows platform.
 *
 * Features demonstrated
 *  - WICED BT HID Device (HIDD) APIs
 *  - Handling of the UART WICED protocol
 *  - SDP and HID descriptor configuration
 *  - Setting of the Local Bluetooth Device address from the host MCU
 *
 * On startup this demo:
 *  - Initializes the Bluetooth sub system
 *  - Receive NVRAM information from the host
 *
 * Application Instructions
 *  - Build application to produce a downloadable hcd file.  For example
 *    demo.hci_hid_device-CYW920706WCDEVAL DIRECT_LOAD=1 build
 *  - Connect a PC terminal to the serial port of the WICED Eval board.
 *  - Start ClientControl application.
 *  - Select the COM port assigned to the WICED Eval board.
 *  - Modify Local Bluetooth address if necessary.
 *  - Enter the full path of the HCD file to download, for example
 *    C:\Users\<username>\Documents\WICED-Studio-X.X\
 *      20706-A2_Bluetooth\build\
 *      hci_hid_device-CYW920706WCDEVAL-rom-ram-Wiced-release\
 *      hci_hid_device-CYW920706WCDEVAL-rom-ram-Wiced-release.hcd
 *  - Click on the Download button to push the HCD file, Local Address and
 *    peer host information if it was saved before.  Note: User input as well
 *    as peer host information is saved in the Windows registry under
 *    HKCU\Software\Broadcom\HciHidControl hive.  This is the place to clean
 *    up, for example if information about the paired should be empty.
 *  - Click on the Enter Pairing Mode button.
 *  - From the peer device, search and pair with the discovered WICED device.
 *  - After initial pairing, if connection is down, selecting the Connect
 *    button, or sending the report will retry the connection to the previously
 *    paired host.
 *  - Select Interrupt channel, Input report, enter the contents of the report
 *    and click on the Send button, to send the report.  For example to send
 *    button down event when key '1' is pushed, report should be
 *    01 00 00 1e 00 00 00 00 00.  All buttons up 01 00 00 00 00 00 00 00 00.
 */

#include <string.h>
#include <stdio.h>
#include "wiced.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_sdp.h"
#include "wiced_bt_cfg.h"
#include "wiced_bt_hidd.h"

#include "wiced_bt_trace.h"
#include "sparcommon.h"
#include "wiced_memory.h"
#include "wiced_gki.h"
#include "wiced_timer.h"
#include "hci_hid_device.h"
#include "hci_control_api.h"
#include "hci_control_spp.h"
#include "wiced_transport.h"
#include "wiced_hal_platform.h"
#include "wiced_hal_nvram.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
#define WICED_HID_EIR_BUF_MAX_SIZE      264
#define HCI_CONTROL_KEY_STORAGE_VS_ID   0x10
#define HCI_HID_SIZE_QUEUE_BUFFER       100     // Set to 0 not to queue data of host's request received before connection is established

#define KEY_INFO_POOL_BUFFER_SIZE   145 //Size of the buffer used for holding the peer device key info
#define KEY_INFO_POOL_BUFFER_COUNT  10  //Correspond's to the number of peer devices

#define BCM920706 20706
#define BCM920707 20707

#ifdef HIDD_QOS
/* QoS settings */
const wiced_bt_hidd_qos_info_t hci_hid_qos =
{
    {
        0,          /* qos_flags */
        1,          /* service type */
        800,        /* token rate (bytes/second) */
        8,          /* token_bucket_size (bytes) */
        0,          /* peak_bandwidth (bytes/second) */
        0xffffffff, /* latency(microseconds) */
        0xffffffff  /* delay_variation(microseconds) */
    },  /* ctrl_ch */
    {
        0,          /* qos_flags */
        2,          /* service type */
        300,        /* token rate (bytes/second) */
        4,          /* token_bucket_size (bytes) */
        300,        /* peak_bandwidth (bytes/second) */
        11250,      /* latency(microseconds) */
        11250       /* delay_variation(microseconds) */
    },  /* int_ch */
    {
        0,          /* qos_flags */
        2,          /* service type */
        400,        /* token rate (bytes/second) */
        8,          /* token_bucket_size (bytes) */
        800,        /* peak_bandwidth (bytes/second) */
        11250,      /* latency(microseconds) */
        11250       /* delay_variation(microseconds) */
    }   /* hci */
};
#endif

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct
{
    void     *p_next;
    uint16_t nvram_id;
    uint8_t  chunk_len;
    uint8_t  data[1];
} hci_hid_nvram_chunk_t;


/******************************************************
 *               Function Declarations
 ******************************************************/
static int      hci_hid_init( void *param );
static wiced_bt_dev_status_t hci_hid_management_cback( wiced_bt_management_evt_t event,
        wiced_bt_management_evt_data_t *p_event_data );
static void     hci_hid_device_write_eir( void );
static uint32_t hci_hid_handle_command( uint8_t *p_data, uint32_t length );
static void     hci_hid_handle_reset_cmd( void );
static void     hci_hid_handle_set_local_bda( uint8_t *p_bda);
static void     hci_hid_handle_accept_pairing_cmd( BOOLEAN enable );
static void     hci_hid_handle_host_connect( );
static void     hci_hid_handle_host_disconnect( void );
static void     hci_hid_handle_send_virtual_unplug(void);
static void     hci_hid_handle_send_report( uint8_t channel, uint8_t type, uint8_t *p_data, uint16_t length );
static void     hci_hid_process_get_report( uint32_t data, wiced_bt_hidd_event_data_t *p_event_data );
static void     hci_hid_process_set_report( uint32_t data, wiced_bt_hidd_event_data_t *p_event_data );
static uint8_t  hci_hid_write_nvram( uint16_t nvram_id, uint8_t data_len, uint8_t *p_data, BOOLEAN from_host );
static uint8_t  hci_hid_read_nvram( uint16_t nvram_id, uint8_t data_len, uint8_t *p_data );
static void     hci_hid_delete_nvram( uint16_t nvram_id , wiced_bool_t unbond );
static void     hci_hid_send_pairing_complete( uint8_t result, uint8_t *p_bda);
static void     hci_hid_send_hid_opened( void );
static void     hci_hid_send_hid_closed( uint32_t reason );
static void     hci_hid_send_virtual_cable_unplugged( void );
static void     hci_hid_process_output_report( uint32_t rpt_type, wiced_bt_hidd_data_t *p_hidd_data );
static void     hci_hid_queue_data( uint8_t channel, uint8_t type, uint8_t *p_data, uint16_t length );
static int      hci_hid_send_queued_data( void *param );
static void     hci_hid_handle_pairing_host_info( uint8_t *p_data, uint16_t length );
static void     hci_hid_hci_trace_cback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data );
static void     hci_control_misc_handle_get_version( void );

extern uint8_t BTM_SetDeviceClass (uint8_t *p_dev_class);

/******************************************************
 *               Variable Definitions
 ******************************************************/
hci_hid_nvram_chunk_t   *p_nvram_first = NULL;
wiced_bt_device_link_keys_t  paired_device;

uint8_t pincode[] = { 0x30, 0x30, 0x30, 0x30 };

#define HCI_HID_DEVICE_STATE_IDLE       0
#define HCI_HID_DEVICE_STATE_PAIRING    1
#define HCI_HID_DEVICE_STATE_PAIRED     2
#define HCI_HID_DEVICE_STATE_CONNECTING 3
#define HCI_HID_DEVICE_STATE_CONNECTED  4

uint8_t hci_hid_device_state = HCI_HID_DEVICE_STATE_IDLE;
wiced_bool_t hci_hid_device_virtual_unplug = WICED_FALSE;

void *p_hci_hid_pool;
wiced_transport_buffer_pool_t *rfcomm_trans_pool;   // Trans pool for sending the RFCOMM data to host
wiced_timer_t rfcomm_trans_data_timer;
uint16_t rfcomm_trans_data_len = 0;
uint8_t* p_rfcomm_trans_buf = NULL;
wiced_timer_t rfcomm_flow_control_timer;

wiced_bt_buffer_pool_t* p_key_info_pool;//Pool for storing the  key info

uint8_t         hci_hid_report_queue[HCI_HID_SIZE_QUEUE_BUFFER];
uint32_t        hci_hid_report_queue_size = 0;

const wiced_transport_cfg_t  transport_cfg =
{
    WICED_TRANSPORT_UART,
    { WICED_TRANSPORT_UART_HCI_MODE, HCI_UART_DEFAULT_BAUD },
    { 0, 0},
    NULL,
    hci_hid_handle_command,
    NULL
};


/******************************************************
 *               Function Definitions
 ******************************************************/
/*
 *  Application Start, the entry point to the application.
 */

APPLICATION_START( )
{
    wiced_result_t result;

    wiced_transport_init( &transport_cfg );

#ifdef WICED_BT_TRACE_ENABLE

    // WICED_ROUTE_DEBUG_TO_WICED_UART to send debug strings over the WICED debug interface */
    wiced_set_debug_uart(WICED_ROUTE_DEBUG_TO_WICED_UART);
#endif

    WICED_BT_TRACE( "HIDD APP START\n" );

    /* Register the dynamic configurations */
    wiced_bt_stack_init( hci_hid_management_cback, &hci_hid_cfg_settings, hci_hid_cfg_buf_pools );

    rfcomm_trans_pool = wiced_transport_create_buffer_pool( SPP_DEVICE_MTU, SPP_TRANS_MAX_BUFFERS );

    WICED_BT_TRACE( "wiced_trans_uart_buffer_init result %d\n", result );

//    p_hci_hid_pool  = wiced_bt_create_pool(1024, 10);
//
//    WICED_BT_TRACE( "wiced_bt_create_pool %x %x\n", p_hci_hid_pool );

//    wiced_sleep_config( WICED_TRUE, WICED_FALSE, WICED_TRUE ); //Enable sleep
}

/*
 *  Pass protocol traces up through the UART
 */
void hci_hid_hci_trace_cback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data )
{
    //WICED_BT_TRACE( "HCI event type %d len %d\n", type, length );
    //WICED_BT_TRACE_ARRAY(  p_data, length, "event data" );

    //send the trace
    wiced_transport_send_hci_trace( NULL, type, length, p_data  );
}

/*
 * Bluetooth management event handler
 */
static wiced_bt_dev_status_t hci_hid_management_cback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data )
{
    wiced_bt_dev_status_t       status = WICED_BT_SUCCESS;
    wiced_bt_device_address_t   bda;
    uint8_t                     bytes_written, bytes_read;
    wiced_bt_dev_pairing_cplt_t *p_pairing_complete;

    switch ( event )
    {
    case BTM_ENABLED_EVT:
        /* Bluetooth controller and host stack enabled */
        WICED_BT_TRACE( "BT Enable status: 0x%02x\n", p_event_data->enabled.status);

        if ( p_event_data->enabled.status == WICED_BT_SUCCESS )
        {
            wiced_bt_dev_read_local_addr( bda );
            WICED_BT_TRACE( "Local Bluetooth Address: [%02X:%02X:%02X:%02X:%02X:%02X]\n", bda[0], bda[1], bda[2], bda[3], bda[4], bda[5]);

           /* Workaround to prevent the Stack to overwrite the COD (when Limited Discovery us used) */
           BTM_SetDeviceClass((uint8_t *)hci_hid_cfg_settings.device_class);

            /* Register callback for receiving hci traces */
            wiced_bt_dev_register_hci_trace( hci_hid_hci_trace_cback );

            hci_control_spp_startup( );

            /* Initialize the HID device */
            hci_hid_init( NULL );

            //Creating a buffer pool for holding the peer devices's key info
            p_key_info_pool = wiced_bt_create_pool( KEY_INFO_POOL_BUFFER_SIZE, KEY_INFO_POOL_BUFFER_COUNT );
            WICED_BT_TRACE( "wiced_bt_create_pool %x\n", p_key_info_pool );

            wiced_transport_send_data( HCI_CONTROL_EVENT_DEVICE_STARTED, NULL, 0 );
        }
        break;
        
    case BTM_PIN_REQUEST_EVT:
        {
            WICED_BT_TRACE("remote address= %B\n", p_event_data->pin_request.bd_addr);
            wiced_bt_dev_pin_code_reply(*p_event_data->pin_request.bd_addr, WICED_BT_SUCCESS ,4, &pincode[0]);
        }
        break;

    case BTM_PAIRING_IO_CAPABILITIES_BR_EDR_REQUEST_EVT:
        /* Use the default security for BR/EDR*/
        WICED_BT_TRACE("\n\n BTM_PAIRING_IO_CAPABILITIES_REQUEST_EVT bda %B\n",
                        p_event_data->pairing_io_capabilities_br_edr_request.bd_addr);
        p_event_data->pairing_io_capabilities_br_edr_request.local_io_cap = BTM_IO_CAPABILITIES_NONE;
        p_event_data->pairing_io_capabilities_br_edr_request.auth_req = BTM_AUTH_SINGLE_PROFILE_GENERAL_BONDING_NO;
        break;

    case BTM_USER_CONFIRMATION_REQUEST_EVT:
        /* User confirmation request for pairing (sample app always accepts) */
        wiced_bt_dev_confirm_req_reply( WICED_BT_SUCCESS, p_event_data->user_confirmation_request.bd_addr );
        break;

    case BTM_PAIRING_COMPLETE_EVT:
        p_pairing_complete = &p_event_data->pairing_complete;

        WICED_BT_TRACE( "Pairing Complete status:%d addr:%B\n", p_pairing_complete->pairing_complete_info.br_edr.status,
                        p_pairing_complete->bd_addr);

        hci_hid_send_pairing_complete( p_pairing_complete->pairing_complete_info.br_edr.status, p_pairing_complete->bd_addr );
        break;

    case BTM_LOCAL_IDENTITY_KEYS_REQUEST_EVT:
        /* Request to restore local identity keys from NVRAM (requested during Bluetooth start up) */
        /* (sample app does not store keys to NVRAM. New local identity keys will be generated).   */
        status = WICED_BT_NO_RESOURCES;
        break;

    case BTM_PAIRED_DEVICE_LINK_KEYS_UPDATE_EVT:
        WICED_BT_TRACE( "LinkKey Update addr:%B\n", p_event_data->paired_device_link_keys_update.bd_addr);
        bytes_written = hci_hid_write_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID, sizeof(wiced_bt_device_link_keys_t),
                        ( uint8_t* ) &p_event_data->paired_device_link_keys_update, FALSE );
        WICED_BT_TRACE( "NVRAM write: %d\n", bytes_written);
        break;

    case BTM_PAIRED_DEVICE_LINK_KEYS_REQUEST_EVT:
        bytes_read = hci_hid_read_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID,
                sizeof(wiced_bt_device_link_keys_t), ( uint8_t* ) &paired_device );

        if ( ( bytes_read == sizeof(wiced_bt_device_link_keys_t) ) &&
             ( memcmp( p_event_data->paired_device_link_keys_request.bd_addr, paired_device.bd_addr, BD_ADDR_LEN ) == 0 ) )
        {
            status = WICED_BT_SUCCESS;
            memcpy( &p_event_data->paired_device_link_keys_request, &paired_device, sizeof(wiced_bt_device_link_keys_t) );
            WICED_BT_TRACE( "Key retrieval success\n" );
        }
        else
        {
            status = WICED_BT_ERROR;
            WICED_BT_TRACE( "Key retrieval failure\n" );
        }
        break;

    case BTM_ENCRYPTION_STATUS_EVT:
        WICED_BT_TRACE( "Encryption status:%d\n", p_event_data->encryption_status.result );
        break;

    case BTM_LOCAL_IDENTITY_KEYS_UPDATE_EVT:
        /* Request to store newly generated local identity keys to NVRAM */
        /* (sample app does not store keys to NVRAM) */
        break;

    case BTM_POWER_MANAGEMENT_STATUS_EVT:
        hidd_pm_proc_mode_change( p_event_data->power_mgmt_notification.hci_status, p_event_data->power_mgmt_notification.status, p_event_data->power_mgmt_notification.value );
        break;

    default:
        WICED_BT_TRACE( "BT management callback: unhandled event (%d)\n", event);
        break;
    }

    return ( status );
}

/*
 * This function handles events received from the HID stack
 */
static void hci_hid_cback( wiced_bt_hidd_cback_event_t event, uint32_t data, wiced_bt_hidd_event_data_t *p_event_data )
{
    uint8_t res_code;

    switch ( event )
    {
    case WICED_BT_HIDD_EVT_OPEN:                                    /**< Connected to host with Interrupt and Control  Data = 1 if Virtual Cable */
        hci_hid_device_state = HCI_HID_DEVICE_STATE_CONNECTED;

        /* Disable discoverability */
        wiced_bt_dev_set_discoverability( BTM_NON_DISCOVERABLE, BTM_DEFAULT_DISC_WINDOW, BTM_DEFAULT_DISC_INTERVAL );

        /* Disable connectability  */
        wiced_bt_dev_set_connectability( WICED_FALSE, BTM_DEFAULT_CONN_WINDOW, BTM_DEFAULT_CONN_INTERVAL );

        WICED_BT_TRACE( "HID connection opened\n" );

        wiced_app_event_serialize( hci_hid_send_queued_data, NULL );

        /* Allow only Pairing device to connect (Virtual Plug) */
        wiced_bt_set_pairable_mode(WICED_TRUE, WICED_TRUE);

        hci_hid_send_hid_opened( );
        break;

    case WICED_BT_HIDD_EVT_CLOSE:                                   /**< Connection with host is closed.            Data=Reason Code. */
        hci_hid_send_hid_closed( data );
        if (hci_hid_device_virtual_unplug)
        {
            hci_hid_device_virtual_unplug = WICED_FALSE;

            /* Delete saved information from the NVRAM */
            hci_hid_delete_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID ,WICED_TRUE );

            /* deregister with HID Host */
            wiced_bt_hidd_deregister( );

            hci_hid_device_state = HCI_HID_DEVICE_STATE_IDLE;
        }
        else if ( ( hci_hid_device_state == HCI_HID_DEVICE_STATE_CONNECTING ) ||
                ( hci_hid_device_state == HCI_HID_DEVICE_STATE_CONNECTED ) )
        {
            hci_hid_device_state = HCI_HID_DEVICE_STATE_PAIRED;

            /* For some PTS Tests, the device must be connectable */
            WICED_BT_TRACE( "Enable PageScan mode (for PTS test)\n" );
            wiced_bt_dev_set_connectability( WICED_TRUE, BTM_DEFAULT_CONN_WINDOW, BTM_DEFAULT_CONN_INTERVAL );
        }
        WICED_BT_TRACE( "HID connection closed (reason code = 0x%x)\n", data);
        break;

    case WICED_BT_HIDD_EVT_RETRYING:                                /**< Lost connection is being re-connected.     Data=Retrial number */
        break;

    case WICED_BT_HIDD_EVT_MODE_CHG:                                /**< Device changed power mode.                 Data=new power mode */
        WICED_BT_TRACE( "HID power mode change (new mode = 0x%x)\n", data );
        break;

    case WICED_BT_HIDD_EVT_PM_FAILED:                               /**< Device power mode change failed */
        WICED_BT_TRACE( "HID power mode change failed\n" );
        break;

    case WICED_BT_HIDD_EVT_CONTROL:                                 /**< Host sent HID_CONTROL                      Data=Control Operation */
        if ( data == HID_PAR_CONTROL_VIRTUAL_CABLE_UNPLUG )
        {
            hci_hid_device_virtual_unplug = WICED_TRUE;
            hci_hid_send_virtual_cable_unplugged( );
        }
        break;

    case WICED_BT_HIDD_EVT_GET_REPORT:                              /**< Host sent GET_REPORT                       Data=Length,  pdata=get_report details */
        WICED_BT_TRACE( "HID GET_REPORT received (len=%d)\n", (int)data );
        hci_hid_process_get_report( data, p_event_data );
        break;

    case WICED_BT_HIDD_EVT_SET_REPORT:                              /**< Host sent SET_REPORT                       Data=Length pdata=details.*/
        WICED_BT_TRACE( "HID SET_REPORT received (len=%d)\n", (int)data );
        hci_hid_process_set_report( data, p_event_data );
        break;

    case WICED_BT_HIDD_EVT_GET_PROTO:                               /**< Host sent GET_PROTOCOL                     Data=NA*/
        WICED_BT_TRACE( "HID GET_PROTOCOL received (len=%d)\n", (int)data );
        wiced_bt_hidd_hand_shake (HID_PAR_HANDSHAKE_RSP_ERR_UNSUPPORTED_REQ);
        break;

    case WICED_BT_HIDD_EVT_SET_PROTO:                               /**< Host sent SET_PROTOCOL         Data=1 for Report, 0 for Boot*/
        WICED_BT_TRACE( "HID SET_PROTOCOL received (protocol = %s)\n", ( data ? "REPORT_MODE" : "BOOT_MODE" ) );
        wiced_bt_hidd_hand_shake (HID_PAR_HANDSHAKE_RSP_ERR_UNSUPPORTED_REQ);
        break;


    case WICED_BT_HIDD_EVT_GET_IDLE:                                /**< Host sent GET_IDLE                         Data=NA */
        WICED_BT_TRACE( "HID GET_IDLE received\n" );
        wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_SUCCESS );
        break;

    case WICED_BT_HIDD_EVT_SET_IDLE:                                /**< Host sent SET_IDLE                         Data=Idle Rate */
        WICED_BT_TRACE( "HID SET_IDLE received (idle rate:%d)\n", (int)data );
        wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_SUCCESS );
        break;

    case WICED_BT_HIDD_EVT_DATA:
        WICED_BT_TRACE( "HID DATA received (len:%d)\n", p_event_data->data.len );
        hci_hid_process_output_report( data, &p_event_data->data );
        break;

    case WICED_BT_HIDD_EVT_DATC:
        WICED_BT_TRACE( "HID DATC received\n" );
        break;

    case WICED_BT_HIDD_EVT_L2CAP_CONGEST:                           /**< L2CAP channel congested */
        WICED_BT_TRACE( "HID Congestion\n" );
        break;

    default:
        WICED_BT_TRACE( "HID event 0x%x\n", event );
        break;
    }
}

/*
 * Initialize the HID Device
 */
int hci_hid_init( void * param )
{
    /* Initialize SDP server database for rfcble_app */
    wiced_bt_sdp_db_init( ( uint8_t * ) wiced_bt_sdp_db, wiced_bt_sdp_db_size );

    hci_hid_device_write_eir( );

    wiced_bt_hidd_init( );

    WICED_BT_TRACE( "HIDD initialized\n" );
}

/*
 *  Prepare extended inquiry response data.  Current version HID service.
 */
void hci_hid_device_write_eir( void )
{
    uint8_t *pBuf = NULL;
    uint8_t *p = NULL;
    uint8_t length = strlen( ( char * ) hci_hid_cfg_settings.device_name );

    /* Allocate buffer of sufficient size to hold the EIR data */
    pBuf = ( uint8_t* ) wiced_bt_get_buffer( WICED_HID_EIR_BUF_MAX_SIZE );
    if ( NULL == pBuf )
    {
        WICED_BT_TRACE( "ERROR: Out of memory for EIR ...\n");
        return;
    }

    p = pBuf;

    /* Update the length of the name (Account for the type field(1 byte) as well) */
    UINT8_TO_STREAM(p, length + 1);                         /* Tag length */
    UINT8_TO_STREAM(p, HCI_EIR_COMPLETE_LOCAL_NAME_TYPE);   /* Full Name Tag */

    /* Copy the device name */
    memcpy( p, hci_hid_cfg_settings.device_name, length );
    p += length;

    /* Write the Complete UUID List Tag */
    UINT8_TO_STREAM(p, sizeof(uint16_t) + 1);               /* Tag length */
    UINT8_TO_STREAM(p, HCI_EIR_COMPLETE_16BITS_UUID_TYPE);  /* Complete Service UUID List */
    UINT16_TO_STREAM(p, UUID_SERVCLASS_HUMAN_INTERFACE);    /* HID Service UUID */

    UINT8_TO_STREAM(p, 0);                                  /* Last Tag */

    // print EIR data (debug)
    wiced_bt_trace_array( "EIR :", ( uint8_t* ) ( pBuf + 1 ), MIN( p - ( uint8_t* ) pBuf, 100 ) );
    wiced_bt_dev_write_eir( pBuf, ( uint16_t ) ( p - pBuf ) );

    /* Allocated buffer not anymore needed. Free it */
    wiced_bt_free_buffer( pBuf );

    return;
}

/*
 * process get report from the host
 */
void hci_hid_process_get_report( uint32_t data, wiced_bt_hidd_event_data_t *p_event_data )
{
    uint8_t report_type = p_event_data->get_rep.rep_type;
    uint8_t report_id   = p_event_data->get_rep.rep_id;

    WICED_BT_TRACE( "%s: Get Report Type:%d ReportId:0x%x\n", __FUNCTION__, report_type, report_id );

    if ( report_type == HID_PAR_REP_TYPE_INPUT )
    {
        switch ( report_id )
        {
        case HCI_HID_REPORT_ID_KEYBOARD:
            wiced_bt_hidd_send_data( WICED_TRUE, HID_PAR_REP_TYPE_INPUT, hci_hid_last_keyboard_report, HCI_HID_KEYBOARD_REPORT_SIZE );
            break;

        case HCI_HID_REPORT_ID_MOUSE:
            wiced_bt_hidd_send_data( WICED_TRUE, HID_PAR_REP_TYPE_INPUT, hci_hid_last_mouse_report, HCI_HID_MOUSE_REPORT_SIZE );
            break;

        case HCI_HID_REPORT_ID_CONSUMER:
            wiced_bt_hidd_send_data( WICED_TRUE, HID_PAR_REP_TYPE_INPUT, hci_hid_last_consumer_report, HCI_HID_CONSUMER_REPORT_SIZE );
            break;

        default:
            wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_ERR_INVALID_REP_ID );
            break;
        }
    }
    else if (report_type == HID_PAR_REP_TYPE_OUTPUT)
    {
        switch ( report_id )
        {
        case HCI_HID_REPORT_ID_KEYBOARD:
            wiced_bt_hidd_send_data( WICED_TRUE, HID_PAR_REP_TYPE_OUTPUT, hci_hid_last_keyboard_out_report, HCI_HID_KEYBOARD_OUT_REPORT_SIZE );
            break;

        default:
            wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_ERR_INVALID_REP_ID );
            break;
        }
    }
    else
    {
        // no (report_id == HID_PAR_REP_TYPE_FEATURE)
        wiced_bt_hidd_hand_shake( HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM );
    }
}

/*
 * process set report from the host
 */
void hci_hid_process_set_report( uint32_t data, wiced_bt_hidd_event_data_t *p_event_data )
{
    uint8_t res_code    = HID_PAR_HANDSHAKE_RSP_SUCCESS;
    uint8_t report_type = data;
    uint8_t report_id   = p_event_data->data.p_data[0];
    uint8_t report_len  = p_event_data->data.len;

    WICED_BT_TRACE( "%s: Set Report Type:%d ReportId:0x%x len:%d\n",
            __FUNCTION__, report_type, report_id, report_len );

    if ( report_type == HID_PAR_REP_TYPE_INPUT )
    {
        switch ( report_id )
        {
        case HCI_HID_REPORT_ID_KEYBOARD:
            if ( report_len != HCI_HID_KEYBOARD_REPORT_SIZE )
            {
                res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM;
            }
            break;

        case HCI_HID_REPORT_ID_MOUSE:
            if ( report_len != HCI_HID_MOUSE_REPORT_SIZE )
            {
                res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM;
            }
            break;

        case HCI_HID_REPORT_ID_CONSUMER:
            if ( p_event_data->data.len != HCI_HID_CONSUMER_REPORT_SIZE )
            {
                res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM;
            }
            break;

        default:
            res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_REP_ID;
            break;
        }
    }
    else if ( report_type == HID_PAR_REP_TYPE_OUTPUT )
    {
        if ( report_id == HCI_HID_REPORT_ID_KEYBOARD )
        {
            // PTS Tester sends a SetReport command containing 9 or 2 bytes...
            // Accept any length.
        }
        else
        {
            res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_REP_ID;
        }
    }
    else
    {
        res_code = HID_PAR_HANDSHAKE_RSP_ERR_INVALID_PARAM;
    }
    wiced_bt_hidd_hand_shake( res_code );
}

/*
 * Handle command received over the UART.  First buffer of the command is the opcode
 * of the operation.  Rest are parameters specific for particular command.
 *
 */
uint32_t hci_hid_handle_command( uint8_t *p_data, uint32_t length )
{
    uint16_t handle;
    uint8_t  hs_cmd;
    uint8_t  bytes_written;
    uint16_t opcode;
    uint16_t payload_len;
    uint8_t* p_rx_buf = p_data;

    if ( !p_rx_buf )
    {
        return HCI_CONTROL_STATUS_INVALID_ARGS;
    }
    //Expected minimum 4 byte as the wiced header
    if( length < 4 )
    {
        WICED_BT_TRACE("invalid params\n");
        wiced_transport_free_buffer( p_rx_buf );
        return HCI_CONTROL_STATUS_INVALID_ARGS;
    }


    opcode = p_data[0] + ( p_data[1] << 8 );     // Get opcode
    payload_len = p_data[2] + ( p_data[3] << 8 );     // Get len
    p_data += 4;
    length -= 4;

    WICED_BT_TRACE( "hci_hid_handle_command 0x%04x\n", opcode );

    switch ( opcode )
    {
    case HCI_CONTROL_COMMAND_RESET:
        hci_hid_handle_reset_cmd( );
        break;

    case HCI_CONTROL_COMMAND_SET_LOCAL_BDA:
        hci_hid_handle_set_local_bda( p_data );
        break;

    case HCI_CONTROL_COMMAND_PUSH_NVRAM_DATA:
        bytes_written = hci_hid_write_nvram( p_data[0] | ( p_data[1] << 8 ), length - 2, &p_data[2], TRUE );
        WICED_BT_TRACE( "NVRAM write: %d\n", bytes_written );
        break;

    case HCI_CONTROL_HIDD_COMMAND_ACCEPT_PAIRING:
        hci_hid_handle_accept_pairing_cmd( *p_data );
        break;

    case HCI_CONTROL_HID_COMMAND_PUSH_PAIRING_HOST_INFO:
        hci_hid_handle_pairing_host_info( p_data, length );
        break;

    case HCI_CONTROL_HIDD_COMMAND_CONNECT:
        hci_hid_handle_host_connect( );
        break;

    case HCI_CONTROL_HIDD_COMMAND_DISCONNECT:
        hci_hid_handle_host_disconnect( );
        break;

    case HCI_CONTROL_HIDD_COMMAND_VIRTUAL_UNPLUG:
        hci_hid_handle_send_virtual_unplug( );
        break;

    case HCI_CONTROL_HID_COMMAND_SEND_REPORT:
        hci_hid_handle_send_report( p_data[0], p_data[1], &p_data[2], ( uint16_t ) ( length - 2 ) );
        break;

    case HCI_CONTROL_MISC_COMMAND_GET_VERSION:
        hci_control_misc_handle_get_version();
        break;

    default:
        WICED_BT_TRACE( "Err: unhandled opcode:0x%04X\n" , opcode );
        break;
    }

    //Freeing the buffer in which data is received
    wiced_transport_free_buffer( p_rx_buf );
    return ( 0 );
}

/*
 * handle reset command from UART
 */
void hci_hid_handle_reset_cmd( void )
{
    // trip watch dog now.
    wdog_generate_hw_reset( );
}

/*
 * Handle host command to set device pairable.  This is typically a HID device button push.
 */
void hci_hid_handle_accept_pairing_cmd( BOOLEAN enable )
{
    wiced_bt_hidd_reg_info_t hidd_reg_info = { { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }, /* Host bd address (FF:FF:FF:FF:FF:FF = any host) */

    // Removing this for now. Broadcom's BTW on Win7 does not negotiate QoS parameters correctly.
    // Which causes connection failure. We may want to restore when BTW is not
    // supported anymore.  Most of the HID devices do not use it regardless of the spec recommendation.
    //( wiced_bt_hidd_qos_info_t * ) &hci_hid_qos, /* QoS Information */
    NULL, /* QoS Information */

    hci_hid_cback /* Event callback  */
    };

    WICED_BT_TRACE( "accept_pairing_cmd %d\n", enable );

    if ( !enable )
    {
        /* Disable discoverability */
        wiced_bt_dev_set_discoverability( BTM_NON_DISCOVERABLE, BTM_DEFAULT_DISC_WINDOW, BTM_DEFAULT_DISC_INTERVAL );

        /* Disable connectability  */
        wiced_bt_dev_set_connectability( WICED_FALSE, BTM_DEFAULT_CONN_WINDOW, BTM_DEFAULT_CONN_INTERVAL );

        /* do not allow peer to pair */
        wiced_bt_set_pairable_mode(WICED_FALSE, WICED_TRUE);

        return;
    }

    /* disconnect any connections if active */
    wiced_bt_hidd_disconnect( );

    /* Host instructed us to start pairing.  Delete saved information from the NVRAM */
    /* If pairing fails, host better restore the NVRAM */
    hci_hid_delete_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID, WICED_TRUE );

    /*
     * We will deregister HIDD without waiting the result of the HIDD Disconnection above.
     * Call wiced_bt_hidd_init to reset HIDD state.
     */
    wiced_bt_hidd_init();

    /* deregister with HIDD and send new registration accepting connections from all devices */
    wiced_bt_hidd_deregister( );

    hci_hid_device_state = HCI_HID_DEVICE_STATE_PAIRING;

    wiced_bt_hidd_register( &hidd_reg_info );

    /* Enable connectability (use default connectability window/interval) */
    wiced_bt_dev_set_connectability( WICED_TRUE, BTM_DEFAULT_DISC_WINDOW, BTM_DEFAULT_DISC_INTERVAL );

    /* Enable discoverability (use default discoverability window/interval) */
    wiced_bt_dev_set_discoverability( BTM_LIMITED_DISCOVERABLE, BTM_DEFAULT_DISC_WINDOW, BTM_DEFAULT_DISC_INTERVAL );

    /* Allow peer to pair */
    wiced_bt_set_pairable_mode(WICED_TRUE, WICED_FALSE);
}

/*
 * Handle command to establish connection to a known host
 */
void hci_hid_handle_host_connect( void )
{
    WICED_BT_TRACE( "HIDD Connect state:%d\n", hci_hid_device_state );

    switch ( hci_hid_device_state )
    {
    case HCI_HID_DEVICE_STATE_IDLE:
    case HCI_HID_DEVICE_STATE_PAIRING:
    case HCI_HID_DEVICE_STATE_CONNECTING:
    case HCI_HID_DEVICE_STATE_CONNECTED:
        WICED_BT_TRACE( "Err: Wrong state:%d\n", hci_hid_device_state );
        break;

    case HCI_HID_DEVICE_STATE_PAIRED:
        hci_hid_device_state = HCI_HID_DEVICE_STATE_CONNECTING;
        wiced_bt_hidd_connect( );
        break;
    }
}

/*
 * Handle command to release HID connection
 */
void hci_hid_handle_host_disconnect( void )
{
    WICED_BT_TRACE( "HIDD Disconnect state:%d\n", hci_hid_device_state );

    switch ( hci_hid_device_state )
    {
    case HCI_HID_DEVICE_STATE_IDLE:
    case HCI_HID_DEVICE_STATE_PAIRING:
    case HCI_HID_DEVICE_STATE_CONNECTING:
    case HCI_HID_DEVICE_STATE_PAIRED:
        WICED_BT_TRACE( "Err: HIDD Disconnect wrong state:%d\n", hci_hid_device_state );
        break;

    case HCI_HID_DEVICE_STATE_CONNECTED:
        wiced_bt_hidd_disconnect( );
        break;
    }
}

/*
 * Handle command to release HID connection
 */
void hci_hid_handle_send_virtual_unplug(void)
{
    WICED_BT_TRACE( "HIDD Send Virtual Unplug state:%d\n", hci_hid_device_state );

    switch ( hci_hid_device_state )
    {
    case HCI_HID_DEVICE_STATE_IDLE:
    case HCI_HID_DEVICE_STATE_PAIRING:
        WICED_BT_TRACE( "Err: HIDD Disconnect wrong state:%d\n", hci_hid_device_state );
        break;

    /* Paired but not connected */
    case HCI_HID_DEVICE_STATE_PAIRED:
        /* Delete saved information from the NVRAM */
        hci_hid_delete_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID, WICED_TRUE );

        /* deregister with HID Host */
        wiced_bt_hidd_deregister( );

        hci_hid_device_state = HCI_HID_DEVICE_STATE_IDLE;
        break;

    case HCI_HID_DEVICE_STATE_CONNECTING:
    case HCI_HID_DEVICE_STATE_CONNECTED:
        /* Send the HID Virtual Unplug command. The HID Host will disconnect */
        hci_hid_device_virtual_unplug = WICED_TRUE;
        wiced_bt_hidd_virtual_unplug( );
        break;
    }
}


/*
 * Handle host command to send HID report.
 */
void hci_hid_handle_send_report( uint8_t channel, uint8_t type, uint8_t *p_data, uint16_t length )
{
    WICED_BT_TRACE( "send report %d bytes state:%d\n", length, hci_hid_device_state );

    switch ( hci_hid_device_state )
    {
    case HCI_HID_DEVICE_STATE_IDLE:
    case HCI_HID_DEVICE_STATE_PAIRING:
        break;

    case HCI_HID_DEVICE_STATE_CONNECTING:
        hci_hid_queue_data( channel, type, p_data, length );
        break;

    case HCI_HID_DEVICE_STATE_PAIRED:
        hci_hid_device_state = HCI_HID_DEVICE_STATE_CONNECTING;
        wiced_bt_hidd_connect( );
        hci_hid_queue_data( channel, type, p_data, length );
        break;

    case HCI_HID_DEVICE_STATE_CONNECTED:
        wiced_bt_hidd_send_data( channel == HCI_CONTROL_HID_REPORT_CHANNEL_CONTROL, type, p_data, length );

        switch ( *p_data )
        {
        case HCI_HID_REPORT_ID_KEYBOARD:
            memcpy( hci_hid_last_keyboard_report, p_data, HCI_HID_KEYBOARD_REPORT_SIZE < length ? HCI_HID_KEYBOARD_REPORT_SIZE : length );
            break;

        case HCI_HID_REPORT_ID_MOUSE:
            memcpy( hci_hid_last_mouse_report, p_data, HCI_HID_MOUSE_REPORT_SIZE < length ? HCI_HID_MOUSE_REPORT_SIZE : length );
            break;

        case HCI_HID_REPORT_ID_CONSUMER:
            memcpy( hci_hid_last_consumer_report, p_data, HCI_HID_CONSUMER_REPORT_SIZE < length ? HCI_HID_CONSUMER_REPORT_SIZE : length );
            break;
        }
    }
}

/*
 * Queue HID report if request from the host received before connection is established.
 */
void hci_hid_queue_data( uint8_t channel, uint8_t type, uint8_t *p_data, uint16_t length )
{
#if HCI_HID_SIZE_QUEUE_BUFFER
    int i;

    // report too long to save
    if ( 4 + length >= HCI_HID_SIZE_QUEUE_BUFFER )
        return;

    // if queue is full, it is probably a good idea to save more recent reports
    while ( hci_hid_report_queue_size + 4 + length >= HCI_HID_SIZE_QUEUE_BUFFER )
    {
        // drop first report
        uint16_t report_len = hci_hid_report_queue[2] + ( hci_hid_report_queue[3] << 8 );
        for ( i = 0; i < hci_hid_report_queue_size - ( report_len + 4 ); i++ )
        {
            hci_hid_report_queue[i] = hci_hid_report_queue[report_len + 4 + i];
        }
        hci_hid_report_queue_size -= ( report_len + 4 );
    }
    hci_hid_report_queue[hci_hid_report_queue_size++] = channel;
    hci_hid_report_queue[hci_hid_report_queue_size++] = type;
    hci_hid_report_queue[hci_hid_report_queue_size++] = length & 0xff;
    hci_hid_report_queue[hci_hid_report_queue_size++] = (length >> 8 ) & 0xff;
    memcpy( &hci_hid_report_queue[hci_hid_report_queue_size], p_data, length );
    hci_hid_report_queue_size += length;

    WICED_BT_TRACE( "report queued total len:%d\n", hci_hid_report_queue_size );
#endif
}

/*
 * Send queued reports when connection is established
 */
int hci_hid_send_queued_data( void *param )
{
    int i = 0;
    while ( hci_hid_report_queue_size != 0 )
    {
        uint16_t report_len = hci_hid_report_queue[i + 2] + ( hci_hid_report_queue[i + 3] >> 8 );
        wiced_bt_hidd_send_data( hci_hid_report_queue[i] == HCI_CONTROL_HID_REPORT_CHANNEL_CONTROL, hci_hid_report_queue[i + 1],
                        &hci_hid_report_queue[i + 4], report_len );

        hci_hid_report_queue_size -= ( report_len + 4 );
        i                         += ( report_len + 4 );
    }
    return 0;
}

/*
 * handle command to send local Bluetooth device address
 */
void hci_hid_handle_set_local_bda( uint8_t *bda )
{
    int result;
    BD_ADDR bd_addr;
    STREAM_TO_BDADDR(bd_addr,bda);
    wiced_bt_set_local_bdaddr( bd_addr, BLE_ADDR_PUBLIC );
    WICED_BT_TRACE( "Local Bluetooth Address: [%02X:%02X:%02X:%02X:%02X:%02X]\n", bda[5], bda[4], bda[3], bda[2], bda[1], bda[0] );

    L2CA_SetDesireRole(HCI_ROLE_SLAVE);

    WICED_BT_TRACE( "Set Name %s result:%d\n", hci_hid_cfg_settings.device_name, result );
}

/*
 * Send notification to the host that pairing has been completed
 */
void hci_hid_send_pairing_complete( uint8_t result, uint8_t *p_bda )
{
    uint8_t tx_buf[12];
    uint8_t *p = tx_buf;
    int i;

    *p++ = result;
    for ( i = 0; i < 6; i++ )
        *p++ = p_bda[5 - i];

    wiced_transport_send_data( HCI_CONTROL_EVENT_PAIRING_COMPLETE, tx_buf, ( int ) ( p - tx_buf ) );
}

/*
 * Send HID OPENED event to the host
 */
void hci_hid_send_hid_opened( void )
{
    wiced_transport_send_data( HCI_CONTROL_HID_EVENT_OPENED, NULL, 0 );
}

/*
 * Send HID CLOSED event to the host
 */
void hci_hid_send_hid_closed( uint32_t reason )
{
    uint8_t u8 = ( uint8_t ) reason;
    wiced_transport_send_data( HCI_CONTROL_HID_EVENT_CLOSED, &u8, 1 );
}

/*
 * Send Virtual Cable Unplugged event to the host
 */
void hci_hid_send_virtual_cable_unplugged( void )
{
    wiced_transport_send_data( HCI_CONTROL_HID_EVENT_VIRTUAL_CABLE_UNPLUGGED, NULL, 0 );
}

/*
 * Send Data event to the host
 */
void hci_hid_process_output_report( uint32_t rpt_type, wiced_bt_hidd_data_t *p_hidd_data )
{
    uint8_t tx_buf[202];

    if ( ( p_hidd_data->p_data[0] == HCI_HID_REPORT_ID_KEYBOARD ) && ( p_hidd_data->len == HCI_HID_KEYBOARD_OUT_REPORT_SIZE ) )
    {
        WICED_BT_TRACE( "%s type:%d len:%d\n", __FUNCTION__, rpt_type, p_hidd_data->len );

        tx_buf[0] = ( uint8_t ) rpt_type;
        memcpy( &tx_buf[1], p_hidd_data->p_data, p_hidd_data->len < 200 ? p_hidd_data->len : 200 );
        memcpy( hci_hid_last_keyboard_out_report, p_hidd_data->p_data, p_hidd_data->len < HCI_HID_KEYBOARD_OUT_REPORT_SIZE ? p_hidd_data->len : HCI_HID_KEYBOARD_OUT_REPORT_SIZE );
        wiced_transport_send_data( HCI_CONTROL_HID_EVENT_DATA, tx_buf, ( int ) ( p_hidd_data->len < 200 ? 1 + p_hidd_data->len : 200 ) );
    }
    else
    {
        WICED_BT_TRACE( "%s ignored type:%d len:%d\n", __FUNCTION__, rpt_type, p_hidd_data->len );
    }
}

/*
 * Host sends information about previously paired host
 */
void hci_hid_handle_pairing_host_info( uint8_t *p_data, uint16_t length )
{
    wiced_bt_device_link_keys_t *p_host_info = ( wiced_bt_device_link_keys_t * ) p_data;
    uint8_t bytes_written;
    wiced_bt_hidd_reg_info_t hidd_reg_info = { { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff }, /* Host bd address (FF:FF:FF:FF:FF:FF = any host) */
#ifdef HIDD_QOS
    ( wiced_bt_hidd_qos_info_t * ) &hci_hid_qos, /* QoS Information */
#else
        NULL,
#endif
    hci_hid_cback /* Event callback  */
    };

    if ( length < sizeof(wiced_bt_device_link_keys_t) )
    {
        WICED_BT_TRACE( " pairing_host_info bad length:%d\n", length );
        return;
    }
    bytes_written = hci_hid_write_nvram( HCI_CONTROL_KEY_STORAGE_VS_ID, sizeof(wiced_bt_device_link_keys_t), p_data, TRUE );

    wiced_bt_hidd_deregister( );

    hci_hid_device_state = HCI_HID_DEVICE_STATE_PAIRED;

    memcpy( hidd_reg_info.host_addr, p_host_info->bd_addr, BD_ADDR_LEN );

    wiced_bt_hidd_register( &hidd_reg_info );

    wiced_bt_set_pairable_mode(WICED_TRUE, WICED_TRUE);
}

/*
 * Write NVRAM function is called to store information in the RAM.  This can be called when
 * stack requires persistent storage, for example to save link keys.  In this case
 * data is also formatted and send to the host for real NVRAM storage.  The same function is
 * called when host pushes NVRAM chunks during the startup.  Parameter from_host in this
 * case is set to FALSE indicating that data does not need to be forwarded.
 */
uint8_t hci_hid_write_nvram( uint16_t nvram_id, uint8_t data_len, uint8_t *p_data, BOOLEAN from_host )
{
    uint8_t tx_buf[257];
    uint8_t *p = tx_buf;
    hci_hid_nvram_chunk_t *p1, *p2;

    /* first check if this ID is being reused and release the memory chunk */
    hci_hid_delete_nvram( nvram_id , WICED_FALSE);

    if ( ( p1 = ( hci_hid_nvram_chunk_t * ) wiced_bt_get_buffer_from_pool( p_key_info_pool ) ) == NULL )
    {
        WICED_BT_TRACE( "Failed to alloc:%d\n", data_len );
        return ( 0 );
    }
    if ( wiced_bt_get_buffer_size( p1 ) < ( sizeof( hci_hid_nvram_chunk_t ) + data_len - 1 ) )
    {
        WICED_BT_TRACE( "Insufficient buffer size, Buff Size %d, Len %d  \n",
                        wiced_bt_get_buffer_size( p1 ), ( sizeof( hci_hid_nvram_chunk_t ) + data_len - 1 ) );
        wiced_bt_free_buffer( p1 );
        return ( 0 );
    }
    p1->p_next = p_nvram_first;
    p1->nvram_id = nvram_id;
    p1->chunk_len = data_len;
    memcpy( p1->data, p_data, data_len );

    p_nvram_first = p1;

    /* If NVRAM chunk arrived from host, no need to send it back, otherwise send over transport */
    if ( !from_host )
    {
        *p++ = nvram_id & 0xff;
        *p++ = ( nvram_id >> 8 ) & 0xff;
        memcpy( p, p_data, data_len );

        wiced_transport_send_data( HCI_CONTROL_EVENT_NVRAM_DATA, tx_buf, ( int ) ( data_len + 2 ) );
    }
    return ( data_len );
}

/*
 * Read NVRAM actually finds the memory chunk in the RAM
 */
uint8_t hci_hid_read_nvram( uint16_t nvram_id, uint8_t data_len, uint8_t *p_data )
{
    hci_hid_nvram_chunk_t *p1;
    uint8_t data_read = 0;

    /* Go through the linked list of chunks */
    for ( p1 = p_nvram_first; p1 != NULL; p1 = ( hci_hid_nvram_chunk_t * ) p1->p_next )
    {
        if ( p1->nvram_id == nvram_id )
        {
            data_read = ( data_len < p1->chunk_len ) ? data_len : p1->chunk_len;
            memcpy( p_data, p1->data, data_read );
        }
    }
    return ( data_read );
}

/*
 * Delete NVRAM function is called when host deletes NVRAM chunk from the persistent storage.
 */
void hci_hid_delete_nvram( uint16_t nvram_id , wiced_bool_t unbond)
{
    hci_hid_nvram_chunk_t *p1, *p2;

    if ( p_nvram_first == NULL )
        return;

    /* Special case when need to remove the first chunk */
    if ( ( p_nvram_first != NULL ) && ( p_nvram_first->nvram_id == nvram_id ) )
    {
        p1 = p_nvram_first;
        p_nvram_first = ( hci_hid_nvram_chunk_t * ) p_nvram_first->p_next;
        if (unbond)
        {
            WICED_BT_TRACE( "Removing device from bonded list\n" );
            wiced_bt_dev_delete_bonded_device(p1->data);
        }
        wiced_bt_free_buffer( p1 );
        return;
    }

    /* Go through the linked list of chunks */
    for ( p1 = p_nvram_first; p1 != NULL; p1 = ( hci_hid_nvram_chunk_t * ) p1->p_next )
    {
        p2 = ( hci_hid_nvram_chunk_t * ) p1->p_next;
        if ( ( p2 != NULL ) && ( p2->nvram_id == nvram_id ) )
        {
            p1->p_next = p2->p_next;
            if (unbond)
            {
                WICED_BT_TRACE( "Removing device from bonded list(2)\n" );
                wiced_bt_dev_delete_bonded_device(p2->data);
            }
            wiced_bt_free_buffer( p2 );
            return;
        }
    }
}

/* Handle get version command */
void hci_control_misc_handle_get_version( void )
{
    uint8_t   tx_buf[15];
    uint8_t   cmd = 0;

    uint32_t  chip = (PLATFORM) ? BCM920707 : BCM920706;

    tx_buf[cmd++] = WICED_SDK_MAJOR_VER;
    tx_buf[cmd++] = WICED_SDK_MINOR_VER;
    tx_buf[cmd++] = WICED_SDK_REV_NUMBER;
    tx_buf[cmd++] = WICED_SDK_BUILD_NUMBER & 0xFF;
    tx_buf[cmd++] = (WICED_SDK_BUILD_NUMBER>>8) & 0xFF;
    tx_buf[cmd++] = chip & 0xFF;
    tx_buf[cmd++] = (chip>>8) & 0xFF;
    tx_buf[cmd++] = (chip>>24) & 0xFF;
    tx_buf[cmd++] = POWER_CLASS;

    /* Send MCU app the supported features */
    tx_buf[cmd++] = HCI_CONTROL_GROUP_HIDD;

    wiced_transport_send_data( HCI_CONTROL_MISC_EVENT_VERSION, tx_buf, cmd );
}
