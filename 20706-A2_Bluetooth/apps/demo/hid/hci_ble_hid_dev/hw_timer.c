
#include <stdio.h>
#include "hw_timer.h"
#include "wiced_bt_trace.h"

/*******************************************************************
 * Constant Definitions
 ******************************************************************/

/* HW Timer2 Register address definitions */
#define timer2intclr_adr                    0x0032a02c
#define timer2load_adr                      0x0032a020
#define timer2value_adr                     0x0032a024
#define timer2control_adr                   0x0032a028

/* HW Timer Control (timer2control_adr) definitions */
#define TIMER_ENABLE_BIT                    0x80
#define TIMER_MODE_PERIODIC_BIT             0x40
#define TIMER_INT_ENABLE_BIT                0x20
#define TIMER_PRE_SCALE_BITS_256            0x08
#define TIMER_PRE_SCALE_BITS_16             0x04
#define TIMER_PRE_SCALE_BITS_1              0x00
#define TIMER_COUNTERSIZE_MODE              0x02        // 1 = 32 bits, 0 = 16 bits
#define TIMER_1_SHOT_BIT                    0x01

/* Timer 2 interrupt */
#define   INTCTL_TIMER_2_MASK               ((uint64_t) 0x0000000000040000)

/* ARM CM3 Interrupt Set registers */
#define intSetEnable_adr0                   0xE000E100
#define intSetEnable_adr1                   0xE000E104
#define intSetEnable_adr2                   0xE000E108
#define intSetEnable_adr3                   0xE000E10C

/* ARM CM3 Interrupt Clear registers */
#define intClrEnable_adr0                   0xE000E180
#define intClrEnable_adr1                   0xE000E184
#define intClrEnable_adr2                   0xE000E188
#define intClrEnable_adr3                   0xE000E18C

#ifndef REG32
#define REG32(ADDR)                         *((volatile uint32_t*)(ADDR))
#endif

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct
{
    hw_timer_cback_t *p_callback;
} hw_timer_cb_t;

/******************************************************
 *               Variable Definitions
 ******************************************************/
static hw_timer_cb_t hw_timer_cb;


/******************************************************
 *               External Definitions
 ******************************************************/
extern void patch_install(uint32_t old_address, uint32_t new_adress);
extern void Timer2DoneInt(void);


/******************************************************
 *               Local Functions
 ******************************************************/
static void hw_timer_cpu_intctl_enable(uint64_t mask);
static void hw_timer_cpu_intctl_disable(uint64_t mask);
static void hw_timer_Timer2DoneInt_addin(void);
void Timer2DoneInt_C_patch(void);

/*
 * hw_timer_init
 */
void hw_timer_init(hw_timer_cback_t *p_callback)
{
    memset(&hw_timer_cb, 0, sizeof(hw_timer_cb));

    hw_timer_cb.p_callback = p_callback;

    patch_install((uint32_t)Timer2DoneInt + 6, (uint32_t)hw_timer_Timer2DoneInt_addin);
}

/*
 * hw_timer_start
 */
void hw_timer_start(uint32_t micro_sec)
{
    /* Clear any pending timer2 interrupt */
    REG32(timer2intclr_adr) = 0;

    // Set countdown interval in microseconds
    REG32(timer2load_adr) = micro_sec;

    /* Enable timer2 as a Periodic timer that generates interrupt */
    REG32(timer2control_adr) = TIMER_ENABLE_BIT |
                               TIMER_MODE_PERIODIC_BIT |
                               TIMER_INT_ENABLE_BIT |
                               TIMER_PRE_SCALE_BITS_1 | /* No prescale, count CPU clocks */
                               TIMER_COUNTERSIZE_MODE;

    /* Enable the interrupt controller */
    hw_timer_cpu_intctl_enable(INTCTL_TIMER_2_MASK);
}

/*
 * hw_timer_stop
 */
void hw_timer_stop(void)
{
    /* disable the interrupt controller */
    hw_timer_cpu_intctl_disable(INTCTL_TIMER_2_MASK);

    /* Clear any pending timer2 interrupt */
    REG32(timer2intclr_adr) = 0;

    /* disable timer2 */
    REG32(timer2control_adr) = 0;
}

/*
 * hw_timer_cpu_intctl_enable
 * Low Level to enable Interrupt at CPU level
 */
static void hw_timer_cpu_intctl_enable(uint64_t mask)
{
    uint32_t *pData = (uint32_t *)&mask;

    if  (!(pData[1] & 0x80000000))
    {
        REG32(intSetEnable_adr0) = pData[0];
        REG32(intSetEnable_adr1) = pData[1];
    }
    else
    {
        REG32(intSetEnable_adr2) = pData[0];
        REG32(intSetEnable_adr3) = pData[1] & 0x7FFFFFFF;
    }
}

/*
 * hw_timer_cpu_intctl_enable
 * Low Level to disable Interrupt at CPU level
 */
static void hw_timer_cpu_intctl_disable(uint64_t mask)
{
    uint32_t *pData = (uint32_t *)&mask;
    if (!(pData[1] & 0x80000000))
    {
        REG32(intClrEnable_adr0) = pData[0];
        REG32(intClrEnable_adr1) = pData[1];
    }
    else
    {
        REG32(intClrEnable_adr2) = pData[0];
        REG32(intClrEnable_adr3) = pData[1];
    }
}

/*
 * inthw_timer_callback_serialized
 * This function is called from the Wiced context. Call the application's callback function.
 */
static void inthw_timer_callback_serialized(void *p_data)
{
    if (hw_timer_cb.p_callback != NULL)
    {
        hw_timer_cb.p_callback();
    }
}

/*
 * Timer2DoneInt_C_patch
 * This is a the FW patch function
 */
void Timer2DoneInt_C_patch(void)
{
    /* We are in CPU interrupt context. Let's call serialize the event (to be in Wiced context) */
    wiced_app_event_serialize(inthw_timer_callback_serialized, 0);
    timer_clearTimer2Int();
}

static void hw_timer_Timer2DoneInt_addin(void)
{
    asm("BL       Timer2DoneInt_C_patch");
    asm("B.W      Timer2DoneInt + 0xA");    //        ; 0x0001a7e0
}

