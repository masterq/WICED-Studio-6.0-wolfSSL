Demo apps
+++++++++
These applications demonstrate the use of one or more Classic Bluetooth (BR/EDR)
and Bluetooth Low Energy (BLE) profiles, and/or the WICED HCI interface.

Most of the applications here (except where noted) have corresponding sample
implementations of MCU functionality executing on a PC, in the Client Control
application, demonstrating WICED HCI control of the device over UART.  See
apps\host\client_control\README.txt

Some applications also have specific peer sample applications provided to
interact with the embedded application, as noted below.

Details on how to build and run the applications, as well as interact and test
them with the client_control application, can be found in the referenced source
file listed for each application.

Hello Sensor application
    This application shows an example of a BLE vendor specific GATT device and
    service.  See hello_sensor\hello_sensor.c.
    The application can be exercised with the Hello Sensor peer application on
    a PC or phone, or with the Hello Client application on another WICED device,
    Note: This application is not controlled by client_control app.
- Hello Sensor peer application
    This application shows an example of a BLE vendor specific GATT client
    running on Windows, Android or iOS. It sends data to Hello Sensor
    application.  See hello_sensor\peer_apps.

Hello Client application
    This application shows an example implementation of a BLE vendor specific
    GATT client profile.  See hello_client\hello_client.c.
    This application can be used with the Hello Sensor application.
    Note: This application is not controlled by client_control app.

Watch Reference application
    The watch reference application combines multiple BR/EDR and BLE services
    and clients commonly used in the watches including Audio Source, AVRCP CT
    and TG, GATT, and Apple's vendor specific ANCS and AMS services.  The
    application can be controlled over UART or SPI.  See watch\hci_control.c.

HCI Audio Gateway application
    This application demonstrtes the BR/EDR Handsfree Audio Gateway profile.
    It also supports SPP functionality.  See audio\hci_ag_plus\hci_control.c.

HCI Audio Remote Control application
    This BR/EDR application is a reference app to show usage of the AVRCP
    Controller and Target profile.  See audio\audio_remote_control\hci_control.c.

HCI Handsfree application
    This BR/EDR application provides a sample Handsfree device implementation.
    See audio\hci_handsfree_plus\hci_control.c.

HCI PBAP Client
   This BR/EDR application implements the Phonebook Acccess Profile Client
   using the WICED PBAP and OBEX libraries. The application can connect to a
   phone and download phonebook or call history.
   See hci_pbap_client\pbc_main.c.

HCI BLE HID Device application
    This application implements the BLE HID over GATT profile (HOGP). See
    hid\hci_ble_hid_dev\hci_ble_hid_dev.c.

HCI HID Device application
    This application provides a sample BR/EDR HID device implementation.  The
    application expects control from a MCU.  See hid\hci_hid_device\hci_hid_device.c

HCI HID Host application
    This application provides a sample BR/EDR HID host implementation. See
    hid\hci_hid_host\hci_control.c

HomeKit Light Bulb and Lock
    Note: These applications and libraries are available for Apple MFI licensees
    only.  The HomeKit application, together with the WICED BT HomeKit library,
    provide examples of Apple HomeKit BLE accessory (light bulb and door lock)
    that can be accessed and controlled by iOS device.

HCI iAP2 SPP application
    Note: This application and library is available for Apple MFI licensees only.
    This application implements a passthrough serial application. The app can
    use standard BR/EDR SPP over RFCOMM if peer supports it, or External
    Accessory iAP2 protocol if connection is established with a iOS device.

BLE Serial Over GATT application
    This application demonstrates a BLE Serial Gatt (BSG) server, a Cypress
    vendor specific service.  See serial_gatt_service\hci_control.c.
- BLE Serial Over GATT peer application
    This application demonstrates how to implement Windows/Android/iOS based
    BLE Serial Gatt (BSG) client, a Cypress vendor specific service.  It sends
    data to and receives data from the HCI BLE Serial Over GATT application.
    See hci_serial_gatt_service\peer_apps.

HCI UART-SPI bridge application
    This application implements the SPI Master and acts as a UART-SPI Bridge.
    The hci_uart_spi_bridge sample application demonstrates the signaling
    protocol to be implemented on the customer MCU to communicate over SPI using
    the WICED_HCI messages. See hci_uart_spi_bridge\hci_bridge.c.
    Note: This application is not controlled by the client_control app.

Headset application
    This application demonstrates a sample BR/EDR Headset device implementation,
    including A2DP Sink, AVRCP Controller and Target roles, and HFP
    functionality.  It also demonstrates BR/EDR Inquiry and SDP as well as BLE 
    GATT Discovery and Advertisement.  See audio\headset\hci_control.c.

