/*
 * Copyright 2015, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * WICED SPI Driver
 */
 
#define WICED_HCI_HEADER_LENGTH 5

typedef enum
{
    SPI_MASTER_MODE=1,
    SPI_SLAVE_MODE
}spi_mode_t;

typedef enum
{
    SPI_RX_DATA_EVENT        = ( 1 << 0 ),
    SPI_TX_DATA_EVENT        = ( 1 << 1 ),
    SPI_RX_DMA_DONE_EVENT    = ( 1 << 2 ),
    SPI_TX_DMA_DONE_EVENT    = ( 1 << 3 ),
    SPI_INIT_EVENT           = ( 1 << 4 ),
    SPI_RX_DATA_START_EVENT  = ( 1 << 5 ),
    SPI_TX_AUDIO_DATA_EVENT  = ( 1 << 6 ),
    SPI_DEV_READY_EVENT      = ( 1 << 7 ),
    SPI_WAKEUP_EVENT         = ( 1 << 8 ),
}spi_events_t;

typedef uint8_t (*spi_tx_rx_handler_t)( uint32_t event );
/**
  * SPI Data received callback
  *
  * @param p_data          : Received data
  *
  * @return void
  */
typedef void (*wiced_trans_spi_data_rx_cback_t)( uint8_t* p_data, uint32_t length );

void spi_register_tx_rx_handler( spi_tx_rx_handler_t p_handler );

void spi_set_event( uint32_t event );