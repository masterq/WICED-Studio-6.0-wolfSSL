/*
 * Copyright 2015, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

/** @file
 *
 * WICED PWM sample application.
 *
 * This application demonstrates how to configure and use
 * PWM in WICED Eval. boards to control the brightness of the
 * LED's onboard
 *
 * Features demonstrated
 * - PWM WICED API's
 *
 * Application Instructions
 * - Connect a PC terminal to the serial port of the WICED Eval board,
 *   then build and download the application as described in the WICED
 *   Studio Kit Guide.
 *
 */
#include "sparcommon.h"
#include "wiced_bt_dev.h"
#include "wiced_hal_gpio.h"
#include "wiced_hal_aclk.h"
#include "wiced_hal_pwm.h"
#include "wiced_hal_platform.h"
#include "wiced_timer.h"

#include "wiced_bt_trace.h"

/******************************************************************************
 *                                Constants
 ******************************************************************************/
#define PWM_CHANNEL         PWM0

#define PWM_INP_CLK_IN_HZ   (512*1000)
#define PWM_FREQ_IN_HZ      (10000)
#define PWM_DUTY_CYCLE      40

/******************************************************************************
 *                                Structures
 ******************************************************************************/

/******************************************************************************
 *                                Variables Definitions
 ******************************************************************************/
static pwm_running = WICED_FALSE;

/******************************************************************************
 *                          Function Declarations
 ******************************************************************************/
void pwm_sample_app_init(void);

/******************************************************************************
 *                          Function Definitions
 ******************************************************************************/

void pwm_sample_app_button_interrupt_handler(void* data, uint8_t pin)
{
    /* Toggle the PWM on/off */
    if (pwm_running)
    {
        WICED_BT_TRACE("Stopping PWM... \n\r");
        wiced_hal_pwm_disable(PWM_CHANNEL);
        pwm_running = WICED_FALSE;
    }
    else
    {
        WICED_BT_TRACE("Starting PWM... \n\r");
        wiced_hal_pwm_enable(PWM_CHANNEL);
        pwm_running = WICED_TRUE;
    }
}

wiced_timer_t seconds_timer;               /* wiced bt app seconds timer */

/* The function invoked on timeout of app seconds timer. */
void seconds_app_timer_cb( uint32_t arg )
{
    pwm_config_t pwm_config;
    static uint32_t idx=0;

    /* vary the PWM duty cycle in steps of 5% to control the brightness of the onboard LED */
    uint32_t duty_cycle[] = { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65,
            70, 75, 80, 85, 90, 95, 100, 95, 90, 85, 80, 75, 70, 65, 60, 55, 50,
            45, 40, 35, 30, 25, 20, 15, 10, 5 };

    if( (sizeof(duty_cycle)/sizeof(uint32_t) == idx) )
        idx = 0;

    /* calculate and update the PWM parameters to reflect the new duty cycle */
    wiced_hal_pwm_get_params(PWM_INP_CLK_IN_HZ, duty_cycle[idx++], PWM_FREQ_IN_HZ, &pwm_config);
    wiced_hal_pwm_change_values(PWM_CHANNEL, pwm_config.toggle_count, pwm_config.init_count);
}

void pwm_sample_app_init(void)
{
    pwm_config_t pwm_config;

    /* Initialize wiced app */
    wiced_bt_app_init();

    /* Configure buttons available on the platform (pin should be configured before registering interrupt handler ) */
    wiced_hal_gpio_configure_pin( WICED_GPIO_BUTTON, WICED_GPIO_BUTTON_SETTINGS( GPIO_EN_INT_RISING_EDGE ), WICED_GPIO_BUTTON_DEFAULT_STATE );
    wiced_hal_gpio_register_pin_for_interrupt( WICED_GPIO_BUTTON, pwm_sample_app_button_interrupt_handler, NULL );

    /* configure PWM */
    wiced_hal_aclk_enable(PWM_INP_CLK_IN_HZ, ACLK1, ACLK_FREQ_24_MHZ);
    wiced_hal_pwm_get_params(PWM_INP_CLK_IN_HZ, PWM_DUTY_CYCLE, PWM_FREQ_IN_HZ, &pwm_config);
    wiced_hal_pwm_start(PWM_CHANNEL, PMU_CLK, pwm_config.toggle_count, pwm_config.init_count, 0);
    pwm_running = WICED_TRUE;

    if ( wiced_init_timer( &seconds_timer, &seconds_app_timer_cb, 0, WICED_MILLI_SECONDS_PERIODIC_TIMER )== WICED_SUCCESS )
    {
        if ( wiced_start_timer( &seconds_timer, 100 ) !=  WICED_SUCCESS )
    {
            WICED_BT_TRACE( "Seconds Timer Error\n\r" );
        }
    }
}

/*
 *  Management callback receives various notifications from the stack
 */
wiced_result_t app_management_callback(wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data)
{

    wiced_result_t result = WICED_BT_SUCCESS;

    WICED_BT_TRACE("app_management_callback %d\n", event);

    switch (event)
    {
        /* Bluetooth  stack enabled */
        case BTM_ENABLED_EVT:
            pwm_sample_app_init();
        break;

        default:
            result = WICED_BT_USE_DEFAULT_SECURITY;
        break;
    }
    return result;
}

/*
 *  Entry point to the application.
 */
APPLICATION_START( )
{

#ifdef WICED_BT_TRACE_ENABLE
     wiced_set_debug_uart( WICED_ROUTE_DEBUG_TO_PUART );
     wiced_hal_puart_select_uart_pads( WICED_PUART_RXD, WICED_PUART_TXD, 0, 0);
#endif

     WICED_BT_TRACE( "\n--------------------------------------------------------- \n"
                     "              PWM Sample Application \n"
                     "---------------------------------------------------------\n");

     wiced_bt_stack_init(app_management_callback, NULL, NULL);
}

