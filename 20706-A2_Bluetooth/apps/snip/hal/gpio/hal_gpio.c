/*
 * Copyright 2014, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

 /** @file
 *
 * WICED sample application for GPIO usage
 *
 * This application demonstrates how to use WICED GPIO APIs to
 * configure GPIO's as Input(with interrupt enabled) or Output pins.
 * The GPIO's defiend using LED_GPIO_1 and LED_GPIO_2 are configured
 * as output pins and toggled at predefined frequency(APP_TIMEOUT_IN_SECONDS_A).
 * The GPIO defined using BUTTON_GPIO is configured as an interrupt
 * enabled input pin, upon pressing this button the blink rate
 * of LED's is toggled between 1 and 5 sec(both intervals are configurable).
 *
 * Features demonstrated
 * - GPIO WICED APIs
 *
 * Application Instructions
 *   Connect a PC terminal to the serial port of the WICED Eval board,
 *   then build and download the application as described in the WICED
 *   Studio Kit Guide
 *
 * Usage
 * Follow the prompts printed on the terminal
 */

#include "wiced_bt_dev.h"
#include "sparcommon.h"

#include "wiced_hal_gpio.h"
#include "wiced_hal_mia.h"
#include "wiced_gki.h"
#include "wiced_hal_platform.h"
#include "wiced_timer.h"
#include "wiced_bt_trace.h"

/******************************************************************************
 *                                Constants
 ******************************************************************************/

/******************************************************************************
 *                                Structures
 ******************************************************************************/

/******************************************************************************
 *                                Variables Definitions
 ******************************************************************************/
#define LED_GPIO_1                              WICED_P26


/* Delay timer for LED blinking */
#define APP_TIMEOUT_IN_SECONDS_A                  1       /* Seconds timer */
#define APP_TIMEOUT_IN_SECONDS_B                  5       /* Seconds timer */

wiced_timer_t seconds_timer;               /* wiced bt app seconds timer */
uint32_t wiced_timer_count       = 0;          /* number of seconds elapsed */

static wiced_result_t sample_gpio_app_management_cback( wiced_bt_management_evt_t event,
                                                        wiced_bt_management_evt_data_t *p_event_data );
static void gpio_set_input_interrupt( void );
static void gpio_test_led( );

/******************************************************************************
 *                          Function Definitions
 ******************************************************************************/

/*
 *  Entry point to the application. Set device configuration and start BT
 *  stack initialization.  The actual application initialization will happen
 *  when stack reports that BT device is ready.
 */
APPLICATION_START( )
{
    wiced_set_debug_uart( WICED_ROUTE_DEBUG_TO_PUART );
    wiced_hal_puart_select_uart_pads( WICED_PUART_RXD, WICED_PUART_TXD, 0, 0);

    WICED_BT_TRACE( "GPIO application start\n\r" );

    wiced_bt_stack_init( sample_gpio_app_management_cback,NULL,NULL);
}

wiced_result_t sample_gpio_app_management_cback( wiced_bt_management_evt_t event,
                                                 wiced_bt_management_evt_data_t *p_event_data)
{
    wiced_result_t result = WICED_SUCCESS;

    WICED_BT_TRACE( "sample_gpio_app_management_cback %d\n\r", event );

    switch( event )
    {
        /* Bluetooth  stack enabled */
        case BTM_ENABLED_EVT:
            /* Initializes the GPIO driver */
            wiced_hal_mia_init( );
            wiced_hal_gpio_init( );
            wiced_hal_mia_enable_mia_interrupt( TRUE );
            wiced_hal_mia_enable_lhl_interrupt( TRUE );

            /* Sample function configures LED pin as output
             * sends a square wave on it
             * if testing pin 31 then disable puart*/
            gpio_test_led( );

            /* Sample function configures GPIO as input. Enable interrupt.
             * Register a call back function to handle on interrupt*/
            gpio_set_input_interrupt( );

            break;
        default:
            break;
    }
    return result;
}

void gpio_interrrupt_handler(void *data, uint8_t port_pin)
{
    static uint32_t prev_blink_interval = APP_TIMEOUT_IN_SECONDS_A;
    uint32_t curr_blink_interval = 0;

     /* Get the status of interrupt on P# */
    if ( wiced_hal_gpio_get_pin_interrupt_status( WICED_GPIO_BUTTON ) )
    {
        /* Clear the gpio interrupt */
        wiced_hal_gpio_clear_pin_interrupt_status( WICED_GPIO_BUTTON );
    }

    /* stop the led blink timer */
    wiced_stop_timer( &seconds_timer );

    /* toggle the blink time interval */
    curr_blink_interval = (APP_TIMEOUT_IN_SECONDS_A == prev_blink_interval)?APP_TIMEOUT_IN_SECONDS_B:APP_TIMEOUT_IN_SECONDS_A;

    wiced_start_timer( &seconds_timer, curr_blink_interval );

    WICED_BT_TRACE("gpio_interrupt_handler : %d\n\r", curr_blink_interval);

    /* update the previous blink interval */
    prev_blink_interval = curr_blink_interval;
}

void gpio_set_input_interrupt( )
{
    uint16_t pin_config;

    /* Configure GPIO PIN# as input, pull up and interrupt on rising edge and output value as high
     *  (pin should be configured before registering interrupt handler )*/
    wiced_hal_gpio_configure_pin( WICED_GPIO_BUTTON, WICED_GPIO_BUTTON_SETTINGS( GPIO_EN_INT_RISING_EDGE ), WICED_GPIO_BUTTON_DEFAULT_STATE );
    wiced_hal_gpio_register_pin_for_interrupt( WICED_GPIO_BUTTON, gpio_interrrupt_handler, NULL );

    /* Get the pin configuration set above */
    pin_config = wiced_hal_gpio_get_pin_config( WICED_GPIO_BUTTON );
    WICED_BT_TRACE( "Pin config of P%d is %d\n\r", WICED_GPIO_BUTTON, pin_config );
}

/* The function invoked on timeout of app seconds timer. */
void seconds_app_timer_cb( uint32_t arg )
{
    wiced_timer_count++;
    WICED_BT_TRACE( "seconds periodic timer count: %d s\n", wiced_timer_count );

    if(wiced_timer_count & 1)
    {
        wiced_hal_gpio_set_pin_output( LED_GPIO_1, GPIO_PIN_OUTPUT_LOW);
    }
    else
    {
        wiced_hal_gpio_set_pin_output( LED_GPIO_1, GPIO_PIN_OUTPUT_HIGH);
    }
}

void gpio_test_led( )
{
    WICED_BT_TRACE( "gpio_test_led\n\r" );

    /* Configure LED PIN as input and initial outvalue as high */
    wiced_hal_gpio_configure_pin( LED_GPIO_1, GPIO_OUTPUT_ENABLE, GPIO_PIN_OUTPUT_HIGH );

    if ( wiced_init_timer( &seconds_timer, &seconds_app_timer_cb, 0, WICED_SECONDS_PERIODIC_TIMER )== WICED_SUCCESS )
    {
        if ( wiced_start_timer( &seconds_timer, APP_TIMEOUT_IN_SECONDS_A ) !=  WICED_SUCCESS )
    {
            WICED_BT_TRACE( "Seconds Timer Error\n\r" );
        }
    }
}
