/*
 * Copyright 2014, Cypress Semiconductor
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Cypress Semiconductor;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Cypress Semiconductor.
 */

 /** @file
 *
 * WICED sample application for PUART usage
 *
 * This application demonstrates how to use WICED PUART driver interface
 * to send and receive bytes or a stream of bytes over the UART hardware.
 *
 * Features demonstrated
 * - PUART WICED APIs
 *
 * Application Instructions
 *   Connect a PC terminal to the serial port of the WICED Eval board,
 *   then build and download the application as described in the WICED
 *   Studio Kit Guide
 *
 * Usage
 * Follow the prompts printed on the terminal
 */

#include "wiced_bt_dev.h"
#include "sparcommon.h"
#include "wiced_hal_puart.h"
#include "wiced_hal_platform.h"
#include "wiced_bt_trace.h"
#include "wiced_hal_gpio.h"
extern void wiced_hal_puart_register_interrupt( void (*puart_rx_cbk)(void*) );

/******************************************************************************
 *                                Constants
 ******************************************************************************/

/******************************************************************************
 *                                Structures
 ******************************************************************************/

/******************************************************************************
 *                                Variables Definitions
 ******************************************************************************/
 static wiced_result_t  sample_puart_app_management_cback( wiced_bt_management_evt_t event,
                                                           wiced_bt_management_evt_data_t *p_event_data);
 static void            test_puart_driver( void );
 /******************************************************************************
 *                                Function Definitions
 ******************************************************************************/
/*
 *  Entry point to the application. Set device configuration and start BT
 *  stack initialization.  The actual application initialization will happen
 *  when stack reports that BT device is ready.
 */
APPLICATION_START( )
{
    wiced_set_debug_uart( WICED_ROUTE_DEBUG_NONE );

    wiced_bt_stack_init( sample_puart_app_management_cback, NULL, NULL );
}

wiced_result_t sample_puart_app_management_cback( wiced_bt_management_evt_t event,
                                                  wiced_bt_management_evt_data_t *p_event_data)
{
    wiced_result_t result = WICED_SUCCESS;

    WICED_BT_TRACE("sample_puart_app_management_cback %d", event);

    switch( event )
    {
        /* Bluetooth  stack enabled */
        case BTM_ENABLED_EVT:
            test_puart_driver( );
            break;
        default:
            break;
    }
    return result;
}

void puar_rx_interrupt_callback(void* unused)
{
    // There can be at most 16 bytes in the HW FIFO.
    uint8_t  readbyte;

    wiced_hal_puart_read( &readbyte );

    /* send one byte via the TX line. */
    wiced_hal_puart_write( readbyte+1 );

    if( readbyte == 'S' )
    {
        /* send a string of characters via the TX line */
        wiced_hal_puart_print( "\nYou typed 'S'.\n" );
    }
    wiced_hal_puart_reset_puart_interrupt( );

}

/* Sample code to test puart driver. Initialises puart, selects puart pads,
 * turn off flow control, and enables Tx and Rx.
 * Echoes the input byte with increment by 1.
 */
void test_puart_driver( void )
{
    wiced_hal_puart_init( );

    // Please see the User Documentation to reference the valid pins.
    wiced_hal_puart_select_uart_pads( WICED_PUART_RXD, WICED_PUART_TXD, 0, 0);

    /* Turn off flow control */
    wiced_hal_puart_flow_off( );  // call wiced_hal_puart_flow_on(); to turn on flow control

    // BEGIN - puart interrupt
    wiced_hal_puart_register_interrupt(puar_rx_interrupt_callback);

    /* Turn on Tx */
    wiced_hal_puart_enable_tx( ); // call wiced_hal_puart_disable_tx to disable transmit capability.
    wiced_hal_puart_print( "Hello World!\r\nType something! Keystrokes are echoed to the terminal ...\r\n");
}

