/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of Cypress Semiconductor 
 *  Corporation. All rights reserved. This software, including source code, documentation and  related 
 * materials ("Software"), is owned by Cypress Semiconductor  Corporation or one of its 
 *  subsidiaries ("Cypress") and is protected by and subject to worldwide patent protection  
 * (United States and foreign), United States copyright laws and international treaty provisions. 
 * Therefore, you may use this Software only as provided in the license agreement accompanying the 
 * software package from which you obtained this Software ("EULA"). If no EULA applies, Cypress 
 * hereby grants you a personal, nonexclusive, non-transferable license to  copy, modify, and 
 * compile the Software source code solely for use in connection with Cypress's  integrated circuit 
 * products. Any reproduction, modification, translation, compilation,  or representation of this 
 * Software except as specified above is prohibited without the express written permission of 
 * Cypress. Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO  WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING,  BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE. Cypress reserves the right to make changes to 
 * the Software without notice. Cypress does not assume any liability arising out of the application 
 * or use of the Software or any product or circuit  described in the Software. Cypress does 
 * not authorize its products for use in any products where a malfunction or failure of the 
 * Cypress product may reasonably be expected to result  in significant property damage, injury 
 * or death ("High Risk Product"). By including Cypress's product in a High Risk Product, the 
 *  manufacturer of such system or application assumes  all risk of such use and in doing so agrees 
 * to indemnify Cypress against all liability.
 */

/** @file
*
* BLE beacon sample
*
* During initialization the app configures stack to send advertisement packets.
* Non-connectable undirected advertisements (High duty cycle, infinite duration).
*
* Features demonstrated
*  - configuring vendor specific advertisements
*
* To demonstrate the app, work through the following steps.
* 1. Plug the WICED eval board into your computer
* 2. Build and download the application (to the WICED board)
* 3. Monitor advertisement packets on over the air sniffer
*
*/
#include "sparcommon.h"
#include "wiced_bt_dev.h"
#include "wiced_bt_ble.h"
#include "wiced_bt_gatt.h"
#include "wiced_bt_cfg.h"
#include "wiced_hal_gpio.h"
#include "wiced_hal_platform.h"

#ifdef  WICED_BT_TRACE_ENABLE
#include "wiced_transport.h"
#include "wiced_bt_trace.h"
#endif

/******************************************************************************
 *                                Structures
 ******************************************************************************/


/******************************************************************************
 *                              Variables Definitions
 ******************************************************************************/

const wiced_transport_cfg_t  transport_cfg =
{
    WICED_TRANSPORT_UART,
    { WICED_TRANSPORT_UART_HCI_MODE, HCI_UART_DEFAULT_BAUD },
    { 0 , 0},
    NULL,
    NULL,
    NULL
};

static void            mybeacon_start_advertisement( void );
static wiced_result_t  mybeacon_management_cback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data );
static void            mybeacon_set_advertisement_data( void );

extern const wiced_bt_cfg_settings_t my_beacon_cfg_settings;
extern const wiced_bt_cfg_buf_pool_t my_beacon_cfg_buf_pools[];

/******************************************************************************
 *                             Function Definitions
 ******************************************************************************/

/*
 *  Entry point to the application. Set device configuration and start BT
 *  stack initialization.  The actual application initialization will happen
 *  when stack reports that BT device is ready.
 */
APPLICATION_START( )
{
#ifdef WICED_BT_TRACE_ENABLE
    wiced_transport_init( &transport_cfg );
    /*
     * For disabling the traces set the UART type as WICED_ROUTE_DEBUG_NONE, and
     * For sending debug strings over the WICED debug interface set the UART type as WICED_ROUTE_DEBUG_TO_WICED_UART
     */
    wiced_set_debug_uart( WICED_ROUTE_DEBUG_TO_WICED_UART );
#endif

    WICED_BT_TRACE( "mybeacon_application_start\n" );

    /* Register the dynamic configurations */
    /* initialize the stack */
    wiced_bt_stack_init(mybeacon_management_cback, &my_beacon_cfg_settings, my_beacon_cfg_buf_pools);
}

/*
 * This function Can be called to configure advertisement data and start advertisements.
 */
void mybeacon_start_advertisement( void )
{
    wiced_result_t result;

    /* Set advertisement data */
    mybeacon_set_advertisement_data();

    /* The device starts sending non-connectable advertisements
     * for an infinite duration. Infinite duration is set by making high duty non-connectable advertising
     * duration (high_duty_nonconn_duration) to 0 in wiced_bt_cfg_settings */
    result =  wiced_bt_start_advertisements( BTM_BLE_ADVERT_NONCONN_HIGH, 0, NULL );
    WICED_BT_TRACE( "wiced_bt_start_advertisements %d\n", result );
}

#ifdef WICED_BT_TRACE_ENABLE
/*
 * The function invoked on timeout of app seconds timer.
 */
void mybeacon_timeout( uint32_t count )
{
    WICED_BT_TRACE("mybeacon_timeout\n");
}
#endif

/*
 *  Pass protocol traces up through the UART
 */
void mybeacon_hci_trace_cback( wiced_bt_hci_trace_type_t type, uint16_t length, uint8_t* p_data )
{
    //send the trace
    wiced_transport_send_hci_trace( NULL, type, length, p_data  );
}

/*
 * my beacon  bt/ble link management callbacks
 */
wiced_result_t mybeacon_management_cback( wiced_bt_management_evt_t event, wiced_bt_management_evt_data_t *p_event_data )
{
    wiced_result_t result = WICED_BT_SUCCESS;

    WICED_BT_TRACE( "mybeacon_management_cback evt: %d\n", event );

    switch( event )
    {
        /* Bluetooth  stack enabled . Start mybeacon application*/
        case BTM_ENABLED_EVT:

            /* Fill the adv data and start advertisements */
            mybeacon_start_advertisement();

#ifdef WICED_BT_TRACE_ENABLE
            /* Register callback for receiving hci traces */
            wiced_bt_dev_register_hci_trace( mybeacon_hci_trace_cback );

            /* Starting the app timer 1 second timer.  This is used just to send some traces out for debugging, */
            wiced_bt_app_start_timer( 1, 0, mybeacon_timeout, NULL );
#endif
            break;

        case BTM_DISABLED_EVT:
            break;

        default:
            break;
    }
    return result;
}

/*
 * This function is called to setup the advertisement data.
 */
void mybeacon_set_advertisement_data( void )
{
    /* TX power for advertisement packets , 10dBm */
    uint8_t tx_power_lcl = 10;
    wiced_bt_ble_advert_elem_t adv_elem[4];
    uint8_t num_elem = 0;
    uint8_t flag = BTM_BLE_GENERAL_DISCOVERABLE_FLAG | BTM_BLE_BREDR_NOT_SUPPORTED;

    adv_elem[num_elem].advert_type  = BTM_BLE_ADVERT_TYPE_FLAG;
    adv_elem[num_elem].len          = sizeof(uint8_t);
    adv_elem[num_elem].p_data       = &flag;
    num_elem++;

    adv_elem[num_elem].advert_type  = BTM_BLE_ADVERT_TYPE_TX_POWER;
    adv_elem[num_elem].len          = 1;
    adv_elem[num_elem].p_data       = &tx_power_lcl;
    num_elem++;

    adv_elem[num_elem].advert_type  = BTM_BLE_ADVERT_TYPE_NAME_COMPLETE;
    adv_elem[num_elem].len          = strlen( (char *)my_beacon_cfg_settings.device_name );
    adv_elem[num_elem].p_data       = ( uint8_t* )my_beacon_cfg_settings.device_name ;
    num_elem++;
    wiced_bt_ble_set_raw_advertisement_data(num_elem, adv_elem);

}

